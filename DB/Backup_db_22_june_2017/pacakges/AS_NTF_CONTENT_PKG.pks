/* Formatted on 5/17/2017 7:08:56 PM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE PACKAGE APPS.AS_NTF_CONTENT_PKG
IS
   PROCEDURE GET_SCHDLR_NTF (IN_SCHEDULER_ID          NUMBER,
                             IN_NOTIFICATION_ID       NUMBER,
                             IN_ACCESS_KEY            VARCHAR2,
                             OUT_NTF_SUBJECT      OUT VARCHAR2,
                             OUT_NTF_BODY         OUT CLOB);

   PROCEDURE GET_SCHDLR_CAL (IN_SCHEDULER_ID       NUMBER,
                             in_SCHEDULER_STAFF_ID          NUMBER,
                             OUT_CAL_SUBJECT     OUT   VARCHAR2,
                             OUT_CAL_CONTENT   OUT CLOB);

   PROCEDURE GET_TRANS_NTF (IN_SCHEDULER_TRANS_HDR_ID       NUMBER,
                            IN_NOTIFICATION_ID              NUMBER,
                            IN_ACCESS_KEY                   VARCHAR2,
                            OUT_NTF_SUBJECT             OUT VARCHAR2,
                            OUT_NTF_BODY                OUT CLOB);
END AS_NTF_CONTENT_PKG;
/
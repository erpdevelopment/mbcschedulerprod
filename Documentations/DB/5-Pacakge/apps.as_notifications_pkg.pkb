

CREATE OR REPLACE PACKAGE BODY APPS.AS_NOTIFICATIONS_PKG
IS
   PROCEDURE WRITE_MIME_HEADER (CONN    IN OUT NOCOPY UTL_SMTP.CONNECTION,
                                NAME    IN            VARCHAR2,
                                VALUE   IN            VARCHAR2)
   IS
   BEGIN
      UTL_SMTP.WRITE_RAW_DATA (
         CONN,
         UTL_RAW.CAST_TO_RAW (NAME || ': ' || VALUE || GC$CRLF));
   END WRITE_MIME_HEADER;

   PROCEDURE WRITE_BOUNDARY (CONN   IN OUT NOCOPY UTL_SMTP.CONNECTION,
                             LAST   IN            BOOLEAN DEFAULT FALSE)
   AS
   BEGIN
      IF (LAST)
      THEN
         UTL_SMTP.WRITE_DATA (CONN, LAST_BOUNDARY);
      --UTL_SMTP.WRITE_DATA (conn, UTL_TCP.CRLF || '.' || UTL_TCP.CRLF);
      ELSE
         UTL_SMTP.WRITE_DATA (CONN, FIRST_BOUNDARY);
      END IF;
   END WRITE_BOUNDARY;

   FUNCTION GET_ADDRESS (ADDR_LIST IN OUT VARCHAR2)
      RETURN VARCHAR2
   IS
      ADDR   VARCHAR2 (256);
      I      PLS_INTEGER;

      FUNCTION LOOKUP_UNQUOTED_CHAR (STR IN VARCHAR2, CHRS IN VARCHAR2)
         RETURN PLS_INTEGER
      AS
         C              VARCHAR2 (5);
         I              PLS_INTEGER;
         LEN            PLS_INTEGER;
         INSIDE_QUOTE   BOOLEAN;
      BEGIN
         INSIDE_QUOTE := FALSE;
         I := 1;
         LEN := LENGTH (STR);

         WHILE (I <= LEN)
         LOOP
            C := SUBSTR (STR, I, 1);

            IF (INSIDE_QUOTE)
            THEN
               IF (C = '"')
               THEN
                  INSIDE_QUOTE := FALSE;
               ELSIF (C = '\')
               THEN
                  I := I + 1;                      -- Skip the quote character
               END IF;
            END IF;

            IF (C = '"')
            THEN
               INSIDE_QUOTE := TRUE;
            END IF;

            IF (INSTR (CHRS, C) >= 1)
            THEN
               RETURN I;
            END IF;

            I := I + 1;
         END LOOP;

         RETURN 0;
      END;
   BEGIN
      ADDR_LIST := LTRIM (ADDR_LIST);
      I := LOOKUP_UNQUOTED_CHAR (ADDR_LIST, ',;');

      IF (I >= 1)
      THEN
         ADDR := SUBSTR (ADDR_LIST, 1, I - 1);
         ADDR_LIST := SUBSTR (ADDR_LIST, I + 1);
      ELSE
         ADDR := ADDR_LIST;
         ADDR_LIST := '';
      END IF;

      I := LOOKUP_UNQUOTED_CHAR (ADDR, '<');

      IF (I >= 1)
      THEN
         ADDR := SUBSTR (ADDR, I + 1);
         I := INSTR (ADDR, '>');

         IF (I >= 1)
         THEN
            ADDR := SUBSTR (ADDR, 1, I - 1);
         END IF;
      END IF;

      RETURN ADDR;
   END GET_ADDRESS;

   PROCEDURE ATTACH_TEXT (
      CONN        IN OUT NOCOPY UTL_SMTP.CONNECTION,
      DATA        IN            CLOB,
      MIME_TYPE   IN            VARCHAR2 DEFAULT GC$MIME_TYPE,
      INLINE      IN            BOOLEAN DEFAULT TRUE,
      FILENAME    IN            VARCHAR2 DEFAULT NULL,
      LAST        IN            BOOLEAN DEFAULT FALSE)
   IS
      L_STEP   PLS_INTEGER := 12000; -- make sure you set a multiple of 3 not higher than 24573
   BEGIN
      /*begin_attachment (conn,
                        mime_type,
                        inline,
                        filename);*/



      FOR I IN 0 .. TRUNC ( (DBMS_LOB.GETLENGTH (DATA) - 1) / L_STEP)
      LOOP
         UTL_SMTP.WRITE_RAW_DATA (
            CONN,
            UTL_RAW.CAST_TO_RAW (
               DBMS_LOB.SUBSTR (DATA, L_STEP, I * L_STEP + 1)));
      END LOOP;

      UTL_SMTP.WRITE_DATA (CONN, GC$CRLF);
   /*IF (LAST)
   THEN
      WRITE_BOUNDARY (CONN, TRUE);
   END IF;*/
   END ATTACH_TEXT;

   FUNCTION INIT_MAIL (SENDER       IN VARCHAR2,
                       RECIPIENTS   IN VARCHAR2,
                       CC           IN VARCHAR2 DEFAULT NULL,
                       BCC          IN VARCHAR2 DEFAULT NULL,
                       SUBJECT      IN VARCHAR2,
                       MIME_TYPE    IN VARCHAR2 DEFAULT GC$MIME_TYPE,
                       PRIORITY     IN PLS_INTEGER DEFAULT NULL)
      RETURN UTL_SMTP.CONNECTION
   IS
      CONN              UTL_SMTP.CONNECTION;
      TEMP_SENDER       VARCHAR2 (32767) := SENDER;
      TEMP_RECIPIENTS   VARCHAR2 (32767) := RECIPIENTS;
      TEMP_CC           VARCHAR2 (32767) := CC;
      TEMP_BCC          VARCHAR2 (32767) := BCC;
   BEGIN
      CONN := UTL_SMTP.OPEN_CONNECTION (SMTP_HOST, SMTP_PORT);
      UTL_SMTP.HELO (CONN, SMTP_DOMAIN);

      --UTL_SMTP.command (conn, 'AUTH LOGIN ');
      /*UTL_SMTP.command (
         conn,
         UTL_RAW.cast_to_varchar2 (
            UTL_ENCODE.base64_encode (UTL_RAW.cast_to_raw ('erpteam@mcit.gov.sa'))));
      UTL_SMTP.command (
         conn,
         UTL_RAW.cast_to_varchar2 (
            UTL_ENCODE.base64_encode (UTL_RAW.cast_to_raw ('mcit@2222'))));*/

      /*UTL_SMTP.command (
         conn,
         UTL_RAW.cast_to_varchar2 (
            UTL_RAW.cast_to_raw ('mcit\erpteam')));
      UTL_SMTP.command (
         conn,
         UTL_RAW.cast_to_varchar2 (UTL_RAW.cast_to_raw ('mcit@2222')));*/

      UTL_SMTP.MAIL (CONN, GET_ADDRESS (TEMP_SENDER));

      WHILE (TEMP_RECIPIENTS IS NOT NULL)
      LOOP
         UTL_SMTP.RCPT (CONN, GET_ADDRESS (TEMP_RECIPIENTS));
      END LOOP;

      WHILE (TEMP_CC IS NOT NULL)
      LOOP
         UTL_SMTP.RCPT (CONN, GET_ADDRESS (TEMP_CC));
      END LOOP;

      WHILE (TEMP_BCC IS NOT NULL)
      LOOP
         UTL_SMTP.RCPT (CONN, GET_ADDRESS (TEMP_BCC));
      END LOOP;

      UTL_SMTP.OPEN_DATA (CONN);
      WRITE_MIME_HEADER (CONN, 'From', SENDER);
      WRITE_MIME_HEADER (CONN, 'To', RECIPIENTS);
      WRITE_MIME_HEADER (CONN, 'Mime-Version', '1.0');
      --WRITE_MIME_HEADER (CONN, 'Content-Transfer-Encoding', 'Base64');
      WRITE_MIME_HEADER (CONN, 'Content-Type', MIME_TYPE);

      IF CC IS NOT NULL
      THEN
         WRITE_MIME_HEADER (CONN, 'CC', CC);
      END IF;

      IF BCC IS NOT NULL
      THEN
         WRITE_MIME_HEADER (CONN, 'BCC', BCC);
      END IF;

      --write_mime_header (conn, 'Subject', subject);
      WRITE_MIME_HEADER (
         CONN,
         'Subject',
         REPLACE (
            REPLACE (UTL_ENCODE.MIMEHEADER_ENCODE (SUBJECT), CHR (13), ''),
            CHR (10),
            ''));
      WRITE_MIME_HEADER (CONN, 'Reply-To: ', SENDER);

      /*UTL_SMTP.WRITE_RAW_DATA (
         CONN,
         UTL_RAW.CAST_TO_RAW (
            'Content-Type: multipart/mixed; boundary="__7D81B75CCC90D2974F7A1CBD__"'
            || GC$CRLF
            || ''
            || GC$CRLF
            || 'This is a Mime message, which your current mail reader may not'
            || GC$CRLF
            || 'understand. Parts of the message will appear as text. If the remainder'
            || GC$CRLF
            || 'appears as random characters in the message body, instead of as'
            || GC$CRLF
            || 'attachments, then you''ll have to extract these parts and decode them'
            || GC$CRLF
            || 'manually.'
            || GC$CRLF
            || ''
            || GC$CRLF));*/



      IF (PRIORITY IS NOT NULL)
      THEN
         WRITE_MIME_HEADER (CONN, 'X-Priority', PRIORITY);
      END IF;



      /* IF (mime_type LIKE '%multipart/mixed%')
       THEN
          UTL_SMTP.WRITE_DATA (
             conn,
             ' boundary= "' || gc$boundary || '"' || UTL_TCP.CRLF);
          UTL_SMTP.WRITE_DATA (conn, UTL_TCP.CRLF);
       END IF;*/

      /*write_mime_header (conn,
                         'X-Mailer',
                         mailer_id || UTL_TCP.crlf || UTL_TCP.crlf);*/

      WRITE_MIME_HEADER (CONN, 'X-Mailer', MAILER_ID);
      UTL_SMTP.WRITE_DATA (CONN, GC$CRLF);


      RETURN CONN;
   END INIT_MAIL;

   FUNCTION ICAL_EVENT (
      P_SUMMARY           IN VARCHAR2,
      P_ORGANIZER_NAME    IN VARCHAR2 DEFAULT 'AA Schedules',
      P_ORGANIZER_EMAIL   IN VARCHAR2,
      P_START_DATE        IN DATE,
      P_END_DATE          IN DATE)
      RETURN VARCHAR2
   AS
      L_RETVAL   VARCHAR2 (32767);
      L_LF       CHAR (1) := CHR (10);
   BEGIN
      /*L_RETVAL :=
       'BEGIN:VCALENDAR' || l_lf
         || 'VERSION:2.0' || l_lf
         || 'PRODID:-//Company Id//NONSGML ICAL_EVENT//EN' || l_lf
         || 'CALSCALE:GREGORIAN' || l_lf
         || 'METHOD:REQUEST' || l_lf
         || 'BEGIN:VEVENT' || l_lf
         || 'SUMMARY:' || P_SUMMARY || l_lf
         || 'ORGANIZER;CN="' || 'Mahmoud Elsayed' || '":MAILTO:' || p_organizer_email || l_lf
         || 'DTSTART:' || TO_CHAR(p_start_date,'YYYYMMDD') || 'T' || TO_CHAR(p_start_date,'HH24MISS') || l_lf
         || 'DTEND:' || TO_CHAR(p_end_date,'YYYYMMDD') || 'T' || TO_CHAR(p_end_date,'HH24MISS') || l_lf
         || 'LOCATION:' || 'MBC Building' || l_lf
         || 'DESCRIPTION:' || P_SUMMARY || l_lf
         || 'DTSTAMP:' || TO_CHAR(P_START_DATE,'YYYYMMDD') || 'T' || TO_CHAR(P_START_DATE,'HH24MISS') || l_lf
         || 'UID:' || RAWTOHEX(SYS_GUID()) || '@company.com' || l_lf
         || 'STATUS:NEEDS-ACTION' ||  l_lf
         || 'BEGIN:VALARM' || l_lf
         || 'TRIGGER:-PT15M' ||l_lf
         || 'REPEAT:1' || l_lf
         || 'DURATION:PT150M' || l_lf
         || 'ACTION:DISPLAY' || l_lf
         || 'DESCRIPTION:' || P_SUMMARY || l_lf
         || 'END:VALARM' || l_lf
         || 'END:VEVENT' || l_lf
         || 'END:VCALENDAR';*/

      L_RETVAL :=
            'BEGIN:VCALENDAR'
         || L_LF
         || 'PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN'
         || L_LF
         || 'VERSION:2.0'
         || L_LF
         || 'METHOD:REQUEST'
         || L_LF
         || 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE'
         || L_LF
         --|| 'CALSCALE:GREGORIAN'|| L_LF
         || 'BEGIN:VTIMEZONE'
         || L_LF
         || 'TZID:Arabian Standard Time'
         || L_LF
         || 'BEGIN:STANDARD'
         || L_LF
         || 'DTSTART:16010101T000000'
         || L_LF
         || 'TZOFFSETFROM:+0400'
         || L_LF
         || 'TZOFFSETTO:+0400'
         || L_LF
         || 'END:STANDARD'
         || L_LF
         || 'END:VTIMEZONE'
         || L_LF
         || 'BEGIN:VEVENT'
         || L_LF
         || 'ATTENDEE;CN="'
         || P_ORGANIZER_EMAIL
         || '";RSVP=TRUE:mailto:'
         || P_ORGANIZER_EMAIL
         --|| 'ATTENDEE;CN="Mahmoud Elsayed";RSVP=TRUE:mailto:JASMINA.MUSIC@MBC.NET'
         || L_LF
         || 'CLASS:PUBLIC'
         || L_LF
         || 'CREATED:'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (SYSDATE, 'HH24MISS')
         || 'Z'
         || L_LF
         || 'DESCRIPTION:'
         || P_SUMMARY
         || ' \n\n'
         || L_LF
         --|| 'DTEND;TZID="Arabian Standard Time":20170410T153000'
         || 'DTEND;TZID=Arabian Standard Time:'
         || TO_CHAR (P_END_DATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (P_END_DATE, 'HH24MISS')
         || L_LF
         --|| 'DTSTAMP:20170410T062149Z'
         || 'DTSTAMP:'
         || TO_CHAR (P_START_DATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (P_START_DATE, 'HH24MISS')
         || 'Z'
         || L_LF
         --|| 'DTSTART;TZID="Arabian Standard Time":20170410T150000'
         || 'DTSTART;TZID=Arabian Standard Time:'
         || TO_CHAR (P_START_DATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (P_START_DATE, 'HH24MISS')
         || L_LF
         || 'LAST-MODIFIED:'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (SYSDATE, 'HH24MISS')
         || 'Z'
         || L_LF
         || 'LOCATION:MBC Building'
         || L_LF
         || 'ORGANIZER;CN="Rami Abu Diab":mailto:rami.abudiab@mbc.net'
         || L_LF
         || 'PRIORITY:5'
         || L_LF
         || 'SEQUENCE:0'
         || L_LF
         || 'SUMMARY;LANGUAGE=en-us:'
         || P_SUMMARY
         || L_LF
         || 'TRANSP:OPAQUE'
         || L_LF
         || 'UID:'
         || RAWTOHEX (SYS_GUID ())
         || L_LF
         || 'X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE'
         || L_LF
         || 'X-MICROSOFT-CDO-IMPORTANCE:1'
         || L_LF
         || 'X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY'
         || L_LF
         || 'X-MICROSOFT-DISALLOW-COUNTER:FALSE'
         || L_LF
         || 'X-MS-OLK-CONFTYPE:0'
         || L_LF
         || 'BEGIN:VALARM'
         || L_LF
         || 'TRIGGER:-PT15M'
         || L_LF
         || 'REPEAT:1'
         || L_LF
         || 'ACTION:DISPLAY'
         || L_LF
         || 'DESCRIPTION:Reminder'
         || L_LF
         || 'END:VALARM'
         || L_LF
         || 'END:VEVENT'
         || L_LF
         || 'END:VCALENDAR';

      RETURN L_RETVAL;
   END ICAL_EVENT;

   PROCEDURE END_SESSION (CONN IN OUT NOCOPY UTL_SMTP.CONNECTION)
   IS
   BEGIN
      UTL_SMTP.CLOSE_DATA (CONN);
      UTL_SMTP.QUIT (CONN);
   END END_SESSION;

   PROCEDURE SEND_EMAIL (
      P_FROM_EMAIL       IN     VARCHAR2,
      P_RECEIPTS_EMAIL   IN     VARCHAR2,
      P_CC_EMAIL         IN     VARCHAR2,
      P_BCC_EMAIL        IN     VARCHAR2,
      P_NTF_SUBJECT      IN     VARCHAR2,
      P_NTF_BODY         IN     CLOB,
      P_MIME_TYPE        IN     VARCHAR2 DEFAULT GC$MULTIPART_MIME_TYPE,
      OUT_STATUS            OUT VARCHAR2)
   IS
      CONN      UTL_SMTP.CONNECTION;
      ERR_NUM   NUMBER;
      ERR_MSG   VARCHAR2 (100);
   BEGIN
      OUT_STATUS := NULL;
      CONN :=
         AS_NOTIFICATIONS_PKG.INIT_MAIL (
            SENDER       => P_FROM_EMAIL,
            RECIPIENTS   =>    P_RECEIPTS_EMAIL,--'JASMINA.MUSIC@MBC.NET',
            --,     --P_RECEIPTS_EMAIL,
            CC           => P_CC_EMAIL,
            BCC          => P_BCC_EMAIL,
            SUBJECT      => P_NTF_SUBJECT,
            MIME_TYPE    => P_MIME_TYPE);
            
            
      ATTACH_TEXT (CONN, P_NTF_BODY, LAST => TRUE);

      /*UTL_SMTP.write_raw_data (
         conn,
         UTL_RAW.cast_to_raw (p_body || UTL_TCP.crlf || UTL_TCP.crlf));*/

      AS_NOTIFICATIONS_PKG.END_SESSION (CONN => CONN);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         OUT_STATUS := 'Error: No data found.';
      WHEN OTHERS
      THEN
         AS_NOTIFICATIONS_PKG.WRITE_BOUNDARY (CONN => CONN);
         ERR_NUM := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 100);
         OUT_STATUS := 'Error: ' || ERR_NUM || ' - ' || ERR_MSG;
         DBMS_OUTPUT.PUT_LINE ('Error number is ' || ERR_NUM);
         DBMS_OUTPUT.PUT_LINE ('Error message is ' || ERR_MSG);
   END SEND_EMAIL;


PROCEDURE SEND_GMAIL (
      P_FROM_EMAIL       IN     VARCHAR2,
      P_RECEIPTS_EMAIL   IN     VARCHAR2,
      P_CC_EMAIL         IN     VARCHAR2,
      P_BCC_EMAIL        IN     VARCHAR2,
      P_NTF_SUBJECT      IN     VARCHAR2,
      P_NTF_BODY         IN     CLOB,
      P_MIME_TYPE        IN     VARCHAR2 DEFAULT GC$MULTIPART_MIME_TYPE,
      OUT_STATUS            OUT VARCHAR2
      )
   IS
      CONN      UTL_SMTP.CONNECTION;
      ERR_NUM   NUMBER;
      ERR_MSG   VARCHAR2 (100);
   BEGIN
      OUT_STATUS := NULL;
      CONN :=
         AS_NOTIFICATIONS_PKG.INIT_MAIL (
            SENDER       => P_FROM_EMAIL,
            RECIPIENTS   =>'HS20111984@GMAIL.COM'
            --'n.businessman@gmail.com'
           ,
            --,     --P_RECEIPTS_EMAIL,
            CC           => P_CC_EMAIL,
            BCC          => P_BCC_EMAIL,
            SUBJECT      => P_NTF_SUBJECT,
            MIME_TYPE    => P_MIME_TYPE);
            
            
      ATTACH_TEXT (CONN, P_NTF_BODY, LAST => TRUE);

      /*UTL_SMTP.write_raw_data (
         conn,
         UTL_RAW.cast_to_raw (p_body || UTL_TCP.crlf || UTL_TCP.crlf));*/

      AS_NOTIFICATIONS_PKG.END_SESSION (CONN => CONN);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
    
         OUT_STATUS := 'Error: No data found.';
      WHEN OTHERS
      THEN
         AS_NOTIFICATIONS_PKG.WRITE_BOUNDARY (CONN => CONN);
         ERR_NUM := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 100);
         OUT_STATUS := 'Error: ' || ERR_NUM || ' - ' || ERR_MSG;
         DBMS_OUTPUT.PUT_LINE ('Error number is ' || ERR_NUM);
         DBMS_OUTPUT.PUT_LINE ('Error message is ' || ERR_MSG);
   END SEND_GMAIL;
   PROCEDURE PUSH_NOTIFICATION (IN_NOTIFICATION_ID NUMBER)
   IS
      P_MIME_TYPE   VARCHAR2 (512);
      OUT_STATUS    VARCHAR2 (512);
   BEGIN
      P_MIME_TYPE := 'text/html; charset="windows-1256"';
      OUT_STATUS := NULL;

      FOR NTF_REC IN (SELECT NOTIFICATION_ID,
                             NOTIFICATION_TYPE,
                             SEND_SYSTEM,
                             SYSTEM_KEY,
                             FROM_EMAIL,
                             RECEIPTS_PERSONID,
                             RECEIPTS_EMAIL,
                             CC_EMAIL,
                             BCC_EMAIL,
                             NTF_SUBJECT,
                             NTF_BODY
                        FROM AS_NOTIFICATIONS
                       WHERE NOTIFICATION_ID = IN_NOTIFICATION_ID)
      LOOP
         AS_NOTIFICATIONS_PKG.SEND_EMAIL (NTF_REC.FROM_EMAIL,
                                          NTF_REC.RECEIPTS_EMAIL,
                                          NTF_REC.CC_EMAIL,
                                          NTF_REC.BCC_EMAIL,
                                          NTF_REC.NTF_SUBJECT,
                                          NTF_REC.NTF_BODY,
                                          P_MIME_TYPE,
                                          OUT_STATUS);
      END LOOP;

      COMMIT;
   END PUSH_NOTIFICATION;

   PROCEDURE MAIL_JOB
   IS
      V_OUT_ERROR    VARCHAR2 (4000);
      ERRORCODE      VARCHAR2 (4000);
      V_ERROR_TEXT   VARCHAR2 (4000);
      P_MIME_TYPE    VARCHAR2 (256);

      CURSOR UNSENT_CUR
      IS
             SELECT NTF.NOTIFICATION_ID,
                    NTF.FROM_EMAIL,
                    NTF.RECEIPTS_EMAIL,
                    NTF.CC_EMAIL,
                    NTF.BCC_EMAIL,
                    NTF.NTF_SUBJECT,
                    NVL (NTF.FAIL_RETRIES, 0) FAIL_RETRIES,
                    NTF.NTF_BODY
               FROM AS_NOTIFICATIONS NTF
              WHERE 
            --  NOTIFICATION_ID=199;
  NTF.NTF_STATUS <> 'SENT'
                   AND NVL (NTF.FAIL_RETRIES, 0) <= 3
                   AND ROWNUM <= 15
         FOR UPDATE OF NTF.NTF_STATUS, NTF.ERROR_TEXT, NTF.FAIL_RETRIES;
   BEGIN
      P_MIME_TYPE := 'text/html; charset="windows-1256"';

      FOR UNSENT_REC IN UNSENT_CUR
      LOOP
         V_OUT_ERROR := NULL;
         DBMS_OUTPUT.PUT_LINE (
            ' unsent_rec.NOTIFICATION_ID = ' || UNSENT_REC.NOTIFICATION_ID);

         BEGIN
            AS_NOTIFICATIONS_PKG.SEND_EMAIL (
               P_FROM_EMAIL       => 'HEBATALLAH.MOHAMED@MBC.NET',--UNSENT_REC.FROM_EMAIL,
               P_RECEIPTS_EMAIL   => 'HEBATALLAH.MOHAMED@MBC.NET',--UNSENT_REC.RECEIPTS_EMAIL,
               P_CC_EMAIL         => 'HEBATALLAH.MOHAMED@MBC.NET',--UNSENT_REC.CC_EMAIL,
               P_BCC_EMAIL        => 'HEBATALLAH.MOHAMED@MBC.NET',--UNSENT_REC.BCC_EMAIL,
               P_NTF_SUBJECT      => UNSENT_REC.NTF_SUBJECT,
               P_NTF_BODY         => UNSENT_REC.NTF_BODY,
               P_MIME_TYPE        => P_MIME_TYPE,
               OUT_STATUS         => V_OUT_ERROR);

            IF V_OUT_ERROR IS NOT NULL
            THEN
               V_ERROR_TEXT :=
                     TO_CHAR (SYSDATE, 'dd-mm-rrrr hh24:mm')
                  || ' : '
                  || V_OUT_ERROR;
               DBMS_OUTPUT.PUT_LINE ('v_ERROR_TEXT = ' || V_ERROR_TEXT);

               UPDATE AS_NOTIFICATIONS
                  SET NTF_STATUS = 'ERROR',
                      ERROR_TEXT = V_ERROR_TEXT,
                      FAIL_RETRIES = UNSENT_REC.FAIL_RETRIES + 1
                WHERE NOTIFICATION_ID = UNSENT_REC.NOTIFICATION_ID;
            ELSE
               UPDATE AS_NOTIFICATIONS
                  SET NTF_STATUS = 'SENT'
                WHERE NOTIFICATION_ID = UNSENT_REC.NOTIFICATION_ID;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               ERRORCODE :=
                     TO_CHAR (SYSDATE, 'dd-mm-rrrr hh24:mm')
                  || ' : '
                  || SQLCODE
                  || '-Error-'
                  || SQLERRM;
               DBMS_OUTPUT.PUT_LINE ('errorcode = ' || ERRORCODE);

               UPDATE AS_NOTIFICATIONS
                  SET NTF_STATUS = 'ERROR',
                      ERROR_TEXT = ERRORCODE,
                      FAIL_RETRIES = UNSENT_REC.FAIL_RETRIES + 1
                WHERE NOTIFICATION_ID = UNSENT_REC.NOTIFICATION_ID;
         END;
      END LOOP;

      COMMIT;
   END MAIL_JOB;

   PROCEDURE SEND_CALENDAR (P_FROM_EMAIL       IN     VARCHAR2,
                            P_RECEIPTS_EMAIL   IN     VARCHAR2,
                            P_NTF_SUBJECT      IN     VARCHAR2,
                            P_NTF_BODY         IN     CLOB,
                            OUT_STATUS            OUT VARCHAR2)
   IS
      CONN      UTL_SMTP.CONNECTION;
      ERR_NUM   NUMBER;
      ERR_MSG   VARCHAR2 (100);
   BEGIN
      /*OUT_STATUS := '0';

      CONN := UTL_SMTP.OPEN_CONNECTION (SMTP_HOST, SMTP_PORT);
      UTL_SMTP.HELO (CONN, SMTP_DOMAIN);
      UTL_SMTP.MAIL (CONN, P_FROM_EMAIL);
      --UTL_SMTP.RCPT (CONN, P_RECEIPTS_EMAIL);
      UTL_SMTP.RCPT (CONN, 'JASMINA.MUSIC@MBC.NET');
      UTL_SMTP.DATA (CONN, P_NTF_BODY);
      UTL_SMTP.QUIT (CONN);*/
      null;
      
   /* CONN :=
       AS_NOTIFICATIONS_PKG.INIT_MAIL (
          SENDER       => P_FROM_EMAIL,
          RECIPIENTS   => P_RECEIPTS_EMAIL,
          SUBJECT      => P_NTF_SUBJECT,
          MIME_TYPE    => 'text/calendar; charset="windows-1256"');
    WRITE_MIME_HEADER (CONN,
                       'Date',
                       TO_CHAR (P_EVENT_DATE, 'DAY, DD-MON-RR HH24:MI'));
    WRITE_MIME_HEADER (CONN, 'method', 'REQUEST');
    WRITE_MIME_HEADER (CONN, 'component', 'vevent');

    ATTACH_TEXT (CONN, P_NTF_BODY, LAST => TRUE);

    UTL_SMTP.write_raw_data (
       conn,
       UTL_RAW.cast_to_raw (p_body || UTL_TCP.crlf || UTL_TCP.crlf));

    AS_NOTIFICATIONS_PKG.END_SESSION (CONN => CONN);*/
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         OUT_STATUS := 'Error: No data found.';
      WHEN OTHERS
      THEN
         AS_NOTIFICATIONS_PKG.WRITE_BOUNDARY (CONN => CONN);
         ERR_NUM := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 100);
         OUT_STATUS := 'Error: ' || ERR_NUM || ' - ' || ERR_MSG;
         DBMS_OUTPUT.PUT_LINE ('Error number is ' || ERR_NUM);
         DBMS_OUTPUT.PUT_LINE ('Error message is ' || ERR_MSG);
   END SEND_CALENDAR;

   PROCEDURE INSERT_NOTIFICATION_API (P_NOTIFICATION_TYPE          VARCHAR2,
                                      P_SEND_SYSTEM                VARCHAR2,
                                      P_SYSTEM_KEY                 VARCHAR2,
                                      P_FROM_EMAIL                 VARCHAR2,
                                      P_RECEIPTS_PERSONID          NUMBER,
                                      P_RECEIPTS_EMAIL             VARCHAR2,
                                      P_CC_EMAIL                   VARCHAR2,
                                      P_BCC_EMAIL                  VARCHAR2,
                                      P_ACCESS_KEY                 VARCHAR2,
                                      P_NTF_SUBJECT                VARCHAR2,
                                      P_NTF_BODY                   CLOB,
                                      OUT_NOTIFICATION_ID   IN OUT NUMBER,
                                      OUT_STATUS               OUT VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      L_NOTIFICATION_ID   NUMBER;


      PROCEDURE VALIDATE_PARAMETERS
      IS
      BEGIN
         OUT_STATUS := 'OK';

         IF P_SEND_SYSTEM IS NULL
         THEN
            OUT_STATUS := 'You should pass System Name';
         END IF;

         IF P_FROM_EMAIL IS NULL
         THEN
            OUT_STATUS := 'Sender can not be null';
         END IF;

         IF P_RECEIPTS_EMAIL IS NULL
         THEN
            OUT_STATUS := 'Recipient can not be null';
         END IF;
      END;
   BEGIN
      VALIDATE_PARAMETERS;

      IF OUT_STATUS = 'OK'
      THEN
         IF OUT_NOTIFICATION_ID IS NULL
         THEN
            L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
            OUT_NOTIFICATION_ID := L_NOTIFICATION_ID;
         ELSE
            L_NOTIFICATION_ID := OUT_NOTIFICATION_ID;
         END IF;

         INSERT INTO AS_NOTIFICATIONS (NOTIFICATION_ID,
                                       NOTIFICATION_TYPE,
                                       SEND_SYSTEM,
                                       SYSTEM_KEY,
                                       FROM_EMAIL,
                                       RECEIPTS_PERSONID,
                                       RECEIPTS_EMAIL,
                                       CC_EMAIL,
                                       BCC_EMAIL,
                                       NTF_SUBJECT,
                                       NTF_BODY,
                                       NTF_STATUS,
                                       FAIL_RETRIES,
                                       CREATION_DATE,
                                       LAST_UPDATE_DATE,
                                       ACCESS_KEY)
              VALUES (L_NOTIFICATION_ID,
                      P_NOTIFICATION_TYPE,
                      P_SEND_SYSTEM,
                      P_SYSTEM_KEY,
                      P_FROM_EMAIL,
                      P_RECEIPTS_PERSONID,
                      'JASMINA.MUSIC@MBC.NET',           --P_RECEIPTS_EMAIL,
                      NULL,                                      --P_CC_EMAIL,
                      NULL,                                     --P_BCC_EMAIL,
                      P_NTF_SUBJECT,
                      P_NTF_BODY,
                      'NEW',
                      0,
                      SYSTIMESTAMP,
                      SYSTIMESTAMP,
                      P_ACCESS_KEY         --TO_CHAR (fnd_crypto.RandomNumber)
                                  );

         COMMIT;
      END IF;
   END INSERT_NOTIFICATION_API;
   
   
    PROCEDURE SEND_MAIL (pto_email varchar2,pemployeename varchar2 ,pbodymessage varchar2 )
   IS
      V_OUT_ERROR    VARCHAR2 (4000);
      ERRORCODE      VARCHAR2 (4000);
      V_ERROR_TEXT   VARCHAR2 (4000);
      P_MIME_TYPE    VARCHAR2 (256);
      html_body VARCHAR2(5000):='<!DOCTYPE html>
<html>
<body>
<p >Dear '||pemployeename ||' </p>
<p > '||pbodymessage||'</p>
<p >Regards</p>
<p > Alarabiya Scheduler </p>

</body>
</html>';


   BEGIN
      P_MIME_TYPE := 'text/html; charset="windows-1256"';

     
         V_OUT_ERROR := NULL;
    

         BEGIN
            AS_NOTIFICATIONS_PKG.SEND_EMAIL (
               P_FROM_EMAIL       =>'jasmina.music@mbc.net',
               -- 'Hebatallah.Mohamed@mbc.net',
               ----UNSENT_REC.FROM_EMAIL,
               P_RECEIPTS_EMAIL   => pto_email,--UNSENT_REC.RECEIPTS_EMAIL,
               P_CC_EMAIL         => null,
               P_BCC_EMAIL        => null,
               P_NTF_SUBJECT      => 'ALARABIYA SCHEDULER',
               P_NTF_BODY         => html_body,
               P_MIME_TYPE        => P_MIME_TYPE,
               OUT_STATUS         => V_OUT_ERROR);

            IF V_OUT_ERROR IS NOT NULL
            THEN
               V_ERROR_TEXT :=
                     TO_CHAR (SYSDATE, 'dd-mm-rrrr hh24:mm')
                  || ' : '
                  || V_OUT_ERROR;
               DBMS_OUTPUT.PUT_LINE ('v_ERROR_TEXT = ' || V_ERROR_TEXT);
              insert into AS_EMAIL_MESSAGE_ERRORS values (pto_email,V_ERROR_TEXT,sysdate,pemployeename);

--     
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               ERRORCODE :=
                     TO_CHAR (SYSDATE, 'dd-mm-rrrr hh24:mm')
                  || ' : '
                  || SQLCODE
                  || '-Error-'
                  || SQLERRM;
               DBMS_OUTPUT.PUT_LINE ('errorcode = ' || ERRORCODE);
               insert into AS_EMAIL_MESSAGE_ERRORS values (pto_email,ERRORCODE,sysdate,pemployeename);

         END;
     
commit;
     
   END SEND_MAIL;
   
   
   
   
       PROCEDURE SEND_MAIL_SHIFT_WK_ALERT (pto_email varchar2,psubject varchar2 ,pbody varchar2 )
   IS
      V_OUT_ERROR    VARCHAR2 (4000);
      ERRORCODE      VARCHAR2 (4000);
      V_ERROR_TEXT   VARCHAR2 (4000);
      P_MIME_TYPE    VARCHAR2 (256);
     --  inner_body VARCHAR2(32000);
     html_body VARCHAR2(32000);






   BEGIN
      P_MIME_TYPE := 'text/html; charset="windows-1256"';
       V_OUT_ERROR := NULL;
   --  pbody:=GET_UNMATCHED_SHIFT_WORKGROUPS;
    html_body:=   '<!DOCTYPE html><htm><body>'|| pbody   ||'</body></html>';  

         BEGIN
         
            AS_NOTIFICATIONS_PKG.SEND_EMAIL (
               P_FROM_EMAIL       =>'jasmina.music@mbc.net',
               -- 'Hebatallah.Mohamed@mbc.net',
               ----UNSENT_REC.FROM_EMAIL,
               P_RECEIPTS_EMAIL   =>pto_email,
               -- 'Hebatallah.Mohamed@mbc.net',--UNSENT_REC.RECEIPTS_EMAIL,
               P_CC_EMAIL         => null,
               P_BCC_EMAIL        => null,
               P_NTF_SUBJECT      => psubject,
               --'ALARABIYA SCHEDULER',
               P_NTF_BODY         => html_body,
               P_MIME_TYPE        => P_MIME_TYPE,
               OUT_STATUS         => V_OUT_ERROR);

            IF V_OUT_ERROR IS NOT NULL
            THEN
               V_ERROR_TEXT :=
                     TO_CHAR (SYSDATE, 'dd-mm-rrrr hh24:mm')
                  || ' : '
                  || V_OUT_ERROR;
               DBMS_OUTPUT.PUT_LINE ('v_ERROR_TEXT = ' || V_ERROR_TEXT);
         

--     
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               ERRORCODE :=
                     TO_CHAR (SYSDATE, 'dd-mm-rrrr hh24:mm')
                  || ' : '
                  || SQLCODE
                  || '-Error-'
                  || SQLERRM;
               DBMS_OUTPUT.PUT_LINE ('errorcode = ' || ERRORCODE);
      

         END;
     
commit;
     
   END SEND_MAIL_SHIFT_WK_ALERT;
             PROCEDURE  GET_UNMATCHED_SHIFT_WORKGROUPS 
             IS
             v_count  Number ;
             lv_body varchar2(32000);
             cursor org is 
            select  organization_id , organization_name 
             from as_organization order by  organization_id asc ;
             cursor to_users (org_id Number ) is 
             ---- For Testing Purposes only sent to Sys Admin
SELECT AsUserRolesEO.USER_ROLE_ID,           
         papf.full_name employee_name,            
       papf.email_address    
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF            
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE  
                       AND ROLE_NAME='SYSADMIN'       
                       and fk_organization_id =org_id 
--and user_role_id=14  
and user_role_id !=1
-- and user_role_id !=3
order by  ROLE_NAME desc ;

   cursor  shiftgroups ( orgid Number ) is 
                select  *  from 
(
select Actual.shift_id,Actual.work_group_id ,nvl(Actual.today_countShift,0)today_countShift,to_number(nvl(Actual.todaycount_wk,0))todaycount_wk ,Planned.employeecount,Planned.employeecountmax
,work_group_name,shift_name
 from 
(select nvl(shift_workgroup.employees_count,0) employeecount ,nvl(employees_count_max,0) employeecountmax
,shift_workgroup.SHIFT_ID,shift_workgroup.WORK_GROUP_ID ,wk.work_group_name ,s.shift_name
  from
  as_shift_work_groups shift_workgroup  ,as_shifts s ,as_work_groups_hdr wk
  where shift_workgroup.shift_id=s.shift_id
  and shift_workgroup.work_group_id=wk.work_group_id
  and   s.FK_ORGANIZATION_ID=orgid and   wk.FK_ORGANIZATION_ID= orgid
  )Planned ,
  (select  count (work_group_id)todaycount_wk ,work_group_id ,count(shift_id)today_countShift,shift_id 
  from as_scheduler_staff sst ,as_schedulers sc
where schedular_start_date =to_date (sysdate )
and sc.SCHEDULER_ID=sst.SCHEDULER_ID
and sc.fk_organization_id=orgid
group by work_group_id,shift_id
order by  SCHEDULAR_START_DATE  desc)Actual 
where  Planned.shift_id=Actual.shift_id(+)
and Planned.work_group_id=Actual.work_group_id(+)
)
where ( todaycount_wk<employeecount or  todaycount_wk>employeecountmax) 
order by work_group_name asc,  shift_name asc ;

             BEGIN
             
            for o in org 
            
            loop
            
            for u in to_users(o.organization_id)
            loop 
            
            
             
             
          lv_Body:='';
           lv_Body :=lv_body||'<p style="font-size:medium;color:black;" align="left" > Dear  '||u.employee_name ||'   </p>';        
  lv_Body :=lv_body||'<p style="font-size:large;color:#74307e; font-weight:bold; text-decoration: underline " align="center" > Al Arabiya Scheduler UnMatched Shifts / Workgroups ( '||o.organization_name ||' )  </p>';        
lv_Body :=lv_body||'<table border="1px"   style="margin-left:50px;   text-align:left; width:80%;  border-collapse: collapse;   font-family:Arial; font-size:larger; "  align="left"  >';
lv_Body :=lv_body||'<tr><th style="width:30%; background-color:#74307e; color:white">Shift</th><th style="width:30%; background-color:#74307e; color:white">WorkGroup</th>';
lv_Body :=lv_body||'<th style="width:20%; background-color:#74307e; color:white">Actual</th><th style="width:10%; background-color:#74307e; color:white">Min</th>';
lv_Body :=lv_body||'<th style="width:10%; background-color:#74307e; color:white">Max</th></tr>';
for i in shiftgroups(o.organization_id)
loop
lv_Body :=lv_body||'<tr><td style="width:30%;color:#74307e; text-align:center">'||i.shift_name||' </td>';
lv_Body :=lv_body||'<td style="width:30%;color:#74307e; text-align:center">'||i.work_group_name||'</td>';
lv_Body :=lv_body||'<td style="width:20%;color:#74307e; text-align:center">'||i.todaycount_wk||'</td>';
lv_Body :=lv_body||'<td style="width:10%;color:#74307e; text-align:center">'||i.employeecount||'</td>';
lv_Body :=lv_body||'<td style="width:10%;color:#74307e; text-align:center">'||i.employeecountmax||'</td>';
lv_Body :=lv_body||'</tr>';

end loop ; --- email body loop
lv_Body :=lv_body||'</table>' ;
 -- lv_Body :=lv_body||'<p style="font-size:medium;color:#74307e; font-weight:bold;  " align="left" > Regards  </p><p style="font-size:medium;color:#74307e; font-weight:bold; " align="left" > Al Arabiya Scheduler  </p>';
      send_mail_shift_wk_alert(u.email_address ,'Al Arabiya Scheduler WorkGroup Matching ( '|| o.organization_name ||' )',lv_body);
  
  end loop ;---- sent to users loop
  end loop ;  ---- organization loop       
            
             END ;
                  PROCEDURE SEND_DAILY_UPDATES 
                  
                  IS 
                  lv_body varchar2(32000);
                  cursor usrs is 
                   select  PAPF.PERSON_ID ,PAPF.email_address ,PAPF.Full_name 
 --, st.scheduler_staff_id ,st.last_update_date ,st.scheduler_id 
  from as_scheduler_staff  st ,as_schedulers s ,as_user_roles UserRoles,PER_ALL_PEOPLE_F PAPF 
 where  UserRoles.PERSON_ID = PAPF.PERSON_ID      
  and UserRoles.person_id=st.person_id      
 AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
   AND PAPF.EFFECTIVE_END_DATE         
 and  st.last_update_date between sysdate-1  and sysdate
 and s.scheduler_id=st.scheduler_id
 and SCHEDULAR_STATUS='PUBLISHED'
  union 
   select  PAPF.PERSON_ID ,PAPF.email_address ,PAPF.Full_name 
  --, st.scheduler_staff_id ,st.last_update_date ,st.scheduler_id 
  from as_scheduler_staff  st ,as_schedulers s ,as_user_roles UserRoles,PER_ALL_PEOPLE_F PAPF 
 where  UserRoles.PERSON_ID = PAPF.PERSON_ID      
  and UserRoles.person_id=st.fk_replaced_by_id     
 AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
   AND PAPF.EFFECTIVE_END_DATE         
 and  st.last_update_date between sysdate-1  and sysdate
 and s.scheduler_id=st.scheduler_id
 and SCHEDULAR_STATUS='PUBLISHED'   ;
                  
 
 cursor updts(ppersonid Number ,pfullname varchar2) is 
      select  st.last_update_date , st.scheduler_staff_id  ,papf.person_id ,fk_replaced_by_id,st.SCHEDULAR_START_DATE  ,papf.full_name  ,sh.shift_name ,sh.shift_display_name  ,
    decode (st.Notes,'N','',Notes) Notes  ,
    CASE WHEN  FK_REPLACED_BY_ID  is not null  and st.fk_replaced_by_id= ppersonid
    THEN pfullname 
    WHEN  FK_REPLACED_BY_ID  is not null  and st.fk_replaced_by_id != ppersonid 
    THEN 

    (select  distinct   full_name from per_people_f pr  where person_id=st.fk_replaced_by_id 
   AND (SYSDATE BETWEEN pr.EFFECTIVE_START_DATE AND pr.EFFECTIVE_END_DATE )           

)
     END  Replacement 
  from as_scheduler_staff  st ,as_schedulers s  ,PER_ALL_PEOPLE_F PAPF   ,as_shifts sh
 where  (st.person_id = ppersonid    or st.fk_replaced_by_id= ppersonid)  
 and papf.person_id=st.person_id
  AND (SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
  AND PAPF.EFFECTIVE_END_DATE )
  and  st.last_update_date between sysdate-1  and sysdate
 and s.scheduler_id=st.scheduler_id
 and st.shift_id=sh.shift_id 
 and SCHEDULAR_STATUS='PUBLISHED' 
 
 order by SCHEDULAR_START_DATE DESC ;
 
                  BEGIN 
                  for s in usrs
                  loop
                   lv_Body:='';
  lv_Body :=lv_body||'<p style="font-size:medium;color:black;" align="left" > Dear  '||s.Full_name || ' , '||'   </p>';        
  lv_Body :=lv_body||'<p style="font-size:large;color:#74307e; font-weight:bold; text-decoration: underline " align="center" > Al Arabiya Scheduler Daily Updates    </p>';        
lv_Body :=lv_body||'<table border="1px"   style="margin-left:50px;   text-align:left; width:800%;  border-collapse: collapse;   font-family:Arial; font-size:larger; "  align="left"  >';
lv_Body :=lv_body||'<tr><th style="width:20%; background-color:#74307e; color:white">Date</th><th style="width:20%; background-color:#74307e; color:white">Assigned To :</th>';
lv_Body :=lv_body||'<th style="width:20%; background-color:#74307e; color:white">ShiftName</th>';
lv_Body :=lv_body||'<th style="width:20%; background-color:#74307e; color:white">Replacement</th><th style="width:20%; background-color:#74307e; color:white">Notes</th></tr>';
                  for i  in updts(s.person_id ,s.full_name)
                  
                  loop 
                  lv_Body :=lv_body||'<tr><td style="width:20%;color:#74307e; text-align:center">'||i.SCHEDULAR_START_DATE||' </td>';
                           lv_Body :=lv_body||'<td style="width:20%;color:#74307e; text-align:center">'||i.FULL_NAME||' </td>';
lv_Body :=lv_body||'<td style="width:20%;color:#74307e; text-align:center">'||i.SHIFT_NAME ||' / '||i.SHIFT_DISPLAY_NAME||'</td>';
lv_Body :=lv_body||'<td style="width:20%;color:#74307e; text-align:center">'||i.REPLACEMENT||'</td>';
lv_Body :=lv_body||'<td style="width:20%;color:#74307e; text-align:center">'||i.NOTES||'</td>';

lv_Body :=lv_body||'</tr>';
                  
                  
                  end loop ;  ---scheduler_staff rows updates 
                  lv_Body :=lv_body||'</table>' ;
 -- lv_Body :=lv_body||'<p style="font-size:medium;color:#74307e; font-weight:bold;  " align="left" > Regards  </p><p style="font-size:medium;color:#74307e; font-weight:bold; " align="left" > Al Arabiya Scheduler  </p>';
      send_mail_shift_wk_alert('jasmina.music@mbc.net' ,'Al Arabiya Scheduler Daily updates'  ,lv_body);
                  end loop ; --- users updates 
                  
                  
                  END ;
   PROCEDURE INSERT_SMS_MESSAGE_STATUS (PPERSON_ID NUMBER ,PPOHONE varchar2,PMESSAGE_STATUS varchar2)
   IS
   BEGIN 
    insert into AS_SMS_MESSAGE_ERRORS (PERSON_ID,PHONE,message_status,MESSAGE_TIME) values(PPERSON_ID,PPOHONE,PMESSAGE_STATUS,sysdate);

  commit ;

 END ;
 
    PROCEDURE SEND_SCHDLR_UPDATES(PSCHEDULER_STAFF_ID  NUMBER )
       IS 
                  lv_body varchar2(32000);
                  cursor usrs is 
                   select  PAPF.PERSON_ID ,PAPF.email_address ,PAPF.Full_name 
  from as_scheduler_staff  st ,as_schedulers s
 , PER_ALL_PEOPLE_F PAPF 
 where  st.PERSON_ID = PAPF.PERSON_ID      
     AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
   AND PAPF.EFFECTIVE_END_DATE         
 --and  st.last_update_date between sysdate-1  and sysdate
 and s.scheduler_id=st.scheduler_id
 and scheduler_staff_id=PSCHEDULER_STAFF_ID
 and SCHEDULAR_STATUS='PUBLISHED'
  union 
   select  PAPF.PERSON_ID ,PAPF.email_address ,PAPF.Full_name 
  from as_scheduler_staff  st ,as_schedulers s 
  ,PER_ALL_PEOPLE_F PAPF 
 where  st.FK_REPLACED_BY_ID = PAPF.PERSON_ID      
    AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
   AND PAPF.EFFECTIVE_END_DATE         
-- and  st.last_update_date between sysdate-1  and sysdate
 and s.scheduler_id=st.scheduler_id
 and scheduler_staff_id=PSCHEDULER_STAFF_ID
 and SCHEDULAR_STATUS='PUBLISHED'   ;
                  
 
 cursor updts  is 
      select  st.last_update_date , st.scheduler_staff_id  ,papf.person_id ,Email_notification,fk_replaced_by_id,st.SCHEDULAR_START_DATE,st.SCHEDULAR_END_DATE ,st.replace_type ,papf.full_name  ,sh.shift_name ,sh.shift_display_name  ,
    decode (st.Notes,'N','',Notes) Notes  ,
    CASE WHEN  FK_REPLACED_BY_ID  is not null 
    THEN 

    (select  distinct   full_name from per_people_f pr  where person_id=st.fk_replaced_by_id 
   AND (SYSDATE BETWEEN pr.EFFECTIVE_START_DATE AND pr.EFFECTIVE_END_DATE )           

)
     END  Replacement 
  from as_scheduler_staff  st ,as_schedulers s  ,PER_ALL_PEOPLE_F PAPF   ,as_shifts sh

where papf.person_id=st.person_id
  AND (SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
  AND PAPF.EFFECTIVE_END_DATE )
 -- and  st.last_update_date between sysdate-1  and sysdate
 and s.scheduler_id=st.scheduler_id
 and st.shift_id=sh.shift_id 
 and scheduler_staff_id=PSCHEDULER_STAFF_ID
 and SCHEDULAR_STATUS='PUBLISHED' ;
 

 
                  BEGIN 
                  for s in usrs
                  loop
                   lv_Body:='';
  lv_Body :=lv_body||'<p style="font-size:14px;color:black;font-family:Calibri;" align="left" > Dear  '||s.Full_name || ' , '||'   </p>';  
  --check if there is replacement or not if yes sent message to replaced by and replace for
    
   
                  for i  in updts
                  
                  loop 
                  
                 
  if (i.replace_type='No Replacement')
  then 
    lv_Body :=lv_body||'<p style="font-size:14px;color:black;font-family:Calibri;" align="left" > Kindly be informed about your Scheduler updates at below mentioned date :  </p>';   
   lv_Body :=lv_body||'<table border="0px"   style="margin-left:50px;  text-align:left; width:800%;  border-collapse: collapse;   font-family:Calibri; font-size:14px; "  align="left"  >';
end if ;
  if (i.replace_type!='No Replacement' and s.Full_name =i.full_name)

    then 
      lv_Body :=lv_body||'<p style="font-size:14px;color:black;font-family:Calibri;" align="left" > Kindly be informed that your replacement for the below mentioned date is   </p>';   
lv_Body :=lv_body||'<table border="0px"   style="margin-left:50px;  text-align:left; width:800%;  border-collapse: collapse;   font-family:Calibri; font-size:14px; "  align="left"  >';
    lv_Body :=lv_body||'<tr> <td style ="font-size:14px;color:black;font-family:Calibri;"> Replaced By:'||i.REPLACEMENT||' </td></tr>';
   end if ;
  if (i.replace_type!='No Replacement' and s.Full_name =i.REPLACEMENT)
  then 
      lv_Body :=lv_body||'<p style="font-size:14px;color:black;font-family:Calibri;" align="left" > Kindly be informed that you will replace '||i.full_name||' for the below mentioned date   </p>';   
lv_Body :=lv_body||'<table border="0px"   style="margin-left:50px;  text-align:left; width:800%;  border-collapse: collapse;   font-family:Calibri; font-size:14px; "  align="left"  >';

    end if ;
  

   lv_Body :=lv_body||'<tr><td style="font-size:14px;color:black;font-family:Calibri;"> Start date:'||i.SCHEDULAR_START_DATE||' </td><td ="font-size:14px;color:black;font-family:Calibri; text-align:center"> End date:'||i.SCHEDULAR_END_DATE||' </td></tr>';
 lv_Body :=lv_body||'<tr><td style ="font-size:14px;color:black;font-family:Calibri; "> Shift:'||i.SHIFT_NAME||' </td><td ="font-size:14px;color:black;font-family:Calibri; text-align:center"> Bulletins:'||AS_SCHDLR_PKG.SCHEDULER_STAFF_BULLETIN(i.scheduler_staff_id)||' </td></tr>';
  lv_Body :=lv_body||'</table>' ;
 lv_Body :=lv_body||'<br/><p  style="font-size:14px;color:green;font-family:Calibri;"> Admin Notification:'||i.Email_notification||' </p>';
             
             

       end loop ;  ---scheduler_staff rows updates 
                
 -- --'Jasmina.Music@mbc.net'
      send_mail_shift_wk_alert('Jasmina.Music@mbc.net' ,'Al Arabiya Scheduler Updates'  ,lv_body);
                  end loop ; --- users updates 
                  
                  
    
    
    
    
    END ;
    
    
    
END AS_NOTIFICATIONS_PKG;
/

package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.model.am.SchedulerAMImpl;

import com.mbc.oracle.scheduler.model.vo.AsSendEmailSMSVoImpl;

import com.mbc.oracle.scheduler.view.util.ADFUtils;
import com.mbc.oracle.scheduler.view.util.JSFUtils;

import com.mbc.oracle.scheduler.view.util.SchedulerUtil;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;
import java.net.URLConnection;

import java.net.URLEncoder;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCDataControl;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.Number;

public class SmsEmailBean {
    public SmsEmailBean() {
        super();
    }
    
    
    
    public String sendSMSAction(String mobilenum,String messagetext) {
       
             String mobile = mobilenum;
             String senderid = "MEN";
             String message = messagetext;

             URLConnection myURLConnection = null;
             URL myURL = null;
             BufferedReader reader = null;
          String encoded_message = URLEncoder.encode(message);
             //Send SMS API
             String mainUrl = getSmsUrl();
                 //"https://www.smsglobal.com/http-api.php?action=sendsms&user=bk6honho&password=samJfPVC&";
             //Prepare parameter string
             StringBuilder sbPostData = new StringBuilder(mainUrl);
             sbPostData.append("from=" + senderid);
             sbPostData.append("&to=" + mobile);
             sbPostData.append("&text=" + encoded_message);
              mainUrl = sbPostData.toString();
             System.out.println("URL to Send SMS-" + mainUrl);
             String messagestatus="";
             try {
                 //prepare connection
                 myURL = new URL(mainUrl);
                 myURLConnection = myURL.openConnection();
                 myURLConnection.connect();
                 reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
                 //reading response
                 String response;
                 while ((response = reader.readLine()) != null) {
                     //print response
                     messagestatus=response;
                     System.out.println(response);
                 }
                 System.out.println(response);
                 //finally close connection
                 reader.close();


             } catch (IOException e) {
                 e.printStackTrace();
             }
             
             
             

         
             
             return messagestatus ;
       
     }
    
      
    public SchedulerAMImpl getApplicationModule()
    {
        
            FacesContext fctx = FacesContext.getCurrentInstance(); 
            BindingContext bindingContext = BindingContext.getCurrent();
            DCDataControl dc = bindingContext.findDataControl("SchedulerAMDataControl");
            SchedulerAMImpl am = (SchedulerAMImpl)dc.getDataProvider();
          return am;
        }
    public String sendemployeeSms() {
        // Add event code here...
        
     SchedulerAMImpl am =  this.getApplicationModule();
     AsSendEmailSMSVoImpl vo =  am.getAsSendEmailSMSVo1();
   
        Row[] selectedRolesRows = vo.getFilteredRows("Selected", true);
        int count=selectedRolesRows.length;
      
        for (int ii = 0; ii < selectedRolesRows.length; ii++)

        {
            String mobilenum=selectedRolesRows[ii].getAttribute("PhoneNumber").toString();
            String smsmessage =selectedRolesRows[ii].getAttribute("SmsText").toString();
          Number personid=(Number)selectedRolesRows[ii].getAttribute("PersonId");
//           if(selectedRolesRows[ii].getAttribute("EmailAddress").equals("hadi.elsayed@mbc.net"))  
//           {
//                  mobilenum="971502458630";   
//                              
//               
//               }
//            if(selectedRolesRows[ii].getAttribute("EmailAddress").equals("Nael.Mahmandar@mbc.net"))  
//            {
//                mobilenum="971501044103";
//
//                
//                }
          
      String msg= sendSMSAction(mobilenum,smsmessage);
            insertsmsstatus(personid,mobilenum,msg) ;  
//            FacesMessage Message = new FacesMessage(msg);   
//                                                          Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                                                          FacesContext fc = FacesContext.getCurrentInstance();   
//                                                          fc.addMessage(null, Message);
//            
        }
        
                    FacesMessage Message = new FacesMessage("Sms Messages Sent ");   
                                                                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                                                                  FacesContext fc = FacesContext.getCurrentInstance();   
                                                                  fc.addMessage(null, Message);
        
        
        
        
        return null;
    }
    public  void insertsmsstatus(Number vpersonid,String vphone,String vstatusmessage)
    {
            OperationBinding oper = ADFUtils.findOperation("insertSmsMessageStatus");
            oper.getParamsMap().put("personid", vpersonid);
            oper.getParamsMap().put("phone", vphone);
            oper.getParamsMap().put("statusmessage", vstatusmessage);
            oper.execute();
            String retValue = (String) oper.getResult();
        
        
        
        }
    public  void insertemail(String vemail,String vemployeename,String vmessage)
    {
            OperationBinding oper = ADFUtils.findOperation("sendemail");
            oper.getParamsMap().put("toemail", vemail);
            oper.getParamsMap().put("employeename", vemployeename);
            oper.getParamsMap().put("emailmessage", vmessage);
            oper.execute();
            String retValue = (String) oper.getResult();
        
        
        
        }

    public String sendGmail() {
        
        SchedulerAMImpl am =  this.getApplicationModule();
        AsSendEmailSMSVoImpl vo =  am.getAsSendEmailSMSVo1();
        
           Row[] selectedRolesRows = vo.getFilteredRows("Selected", true);
           int count=selectedRolesRows.length;
         
           for (int ii = 0; ii < selectedRolesRows.length; ii++)

           {
               String emailaddress=selectedRolesRows[ii].getAttribute("GmailName").toString();
               String message =selectedRolesRows[ii].getAttribute("SmsText").toString();
             String employeename=selectedRolesRows[ii].getAttribute("EmployeeName").toString();
        //              if(selectedRolesRows[ii].getAttribute("EmailAddress").equals("hadi.elsayed@mbc.net"))
        //              {
        //                     emailaddress="hebatallah.mohamed@mbc.net";
        //
        //
        //                  }
        //               if(selectedRolesRows[ii].getAttribute("EmailAddress").equals("Nael.Mahmandar@mbc.net"))
        //               {
        //                   emailaddress="Emad.Elshirbiny@mbc.net";
        //
        //
        //                   }
             
               insertemail(emailaddress,employeename,message);
        
           }
                    FacesMessage Message = new FacesMessage("Emails Sent");
                                                                  Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                                                  FacesContext fc = FacesContext.getCurrentInstance();
                                                                  fc.addMessage(null, Message);
        
        
      
        return null;
    }

    public String sendemail() {
        // Add event code here...
        
        SchedulerAMImpl am =  this.getApplicationModule();
        AsSendEmailSMSVoImpl vo =  am.getAsSendEmailSMSVo1();
        
           Row[] selectedRolesRows = vo.getFilteredRows("Selected", true);
           int count=selectedRolesRows.length;
         
           for (int ii = 0; ii < selectedRolesRows.length; ii++)

           {
               String emailaddress=selectedRolesRows[ii].getAttribute("EmailAddress").toString();
               String message =selectedRolesRows[ii].getAttribute("SmsText").toString();
             String employeename=selectedRolesRows[ii].getAttribute("EmployeeName").toString();
//              if(selectedRolesRows[ii].getAttribute("EmailAddress").equals("hadi.elsayed@mbc.net"))  
//              {
//                     emailaddress="hebatallah.mohamed@mbc.net";   
//                                 
//                  
//                  }
//               if(selectedRolesRows[ii].getAttribute("EmailAddress").equals("Nael.Mahmandar@mbc.net"))  
//               {
//                   emailaddress="Emad.Elshirbiny@mbc.net";
//
//                   
//                   }
             
               insertemail(emailaddress,employeename,message);
      
           }
                    FacesMessage Message = new FacesMessage("Emails Sent");
                                                                  Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                                                  FacesContext fc = FacesContext.getCurrentInstance();
                                                                  fc.addMessage(null, Message);
        
        
        return null;
    }
    
    
    public String getSmsUrl()
    {
            OperationBinding oper = ADFUtils.findOperation("getSmsUrl");
          
            oper.execute();
            String retValue = (String) oper.getResult();
            return retValue;
        
        }
}

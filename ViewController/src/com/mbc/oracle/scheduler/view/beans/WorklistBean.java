package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import com.mbc.oracle.scheduler.view.util.JSFUtils;

import java.io.Serializable;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.faces.event.ActionEvent;

import oracle.adf.controller.TaskFlowId;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ViewObjectImpl;

public class WorklistBean implements Serializable {
    private String ntfNotes;

    public void setNtfNotes(String ntfNotes) {
        this.ntfNotes = ntfNotes;
    }

    public String getNtfNotes() {
        if (ntfNotes == null || "".equals(ntfNotes)) {
            String notes = (String) ADFUtils.getBoundAttributeValue("Notes");
            ntfNotes = notes;
        }
        return ntfNotes;
    }
    private String schdlrTaskFlowId = "/WEB-INF/home/worklistSchdlr-BTF.xml#worklistSchdlr-BTF";
    private String transTaskFlowId = "/WEB-INF/home/worklistTrans-BTF.xml#worklistTrans-BTF";

    public Map<String, Object> getTaskFlowParamsMap() {
        Map<String, Object> params = new HashMap<String, Object>();
        Number worklistTransIdPar = (Number) JSFUtils.resolveExpression("#{pageFlowScope.WorklistTransIdPar}");
        String worklistCategory = (String) ADFUtils.getBoundAttributeValue("WorklistCategory");
        if ("SCHDLR".equals(worklistCategory)) {
            params.put("SchedularIdPar", worklistTransIdPar);
        } else if ("TRANS".equals(worklistCategory)) {
            params.put("SchedulerTransHdrIdPar", worklistTransIdPar);
        }
        System.out.println("xxx worklistTransIdPar=" + worklistTransIdPar + " ,worklistCategory=" + worklistCategory);
        return params;
    }

    public WorklistBean() {
    }

    public TaskFlowId getDynamicTaskFlowId() {
        String worklistCategory = (String) ADFUtils.getBoundAttributeValue("WorklistCategory");
        if ("SCHDLR".equals(worklistCategory)) {
            this.getTaskFlowParamsMap().put("SchedularIdPar",
                                            (Number) JSFUtils.resolveExpression("#{pageFlowScope.WorklistTransIdPar}"));
            return TaskFlowId.parse(schdlrTaskFlowId);
        } else if ("TRANS".equals(worklistCategory)) {
            this.getTaskFlowParamsMap().put("SchedulerTransHdrIdPar",
                                            (Number) JSFUtils.resolveExpression("#{pageFlowScope.WorklistTransIdPar}"));
            return TaskFlowId.parse(transTaskFlowId);
        }
        return TaskFlowId.parse(schdlrTaskFlowId);

    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.schdlrTaskFlowId = taskFlowId;
    }

    public void onClickApproveBtn(ActionEvent actionEvent) {
        // Add event code here...
        this.workListAction("APPROVED");
    }

    public void onClickRejectBtn(ActionEvent actionEvent) {
        // Add event code here...
        this.workListAction("REJECTED");
    }

    public void onClickWorklistOkBtn(ActionEvent actionEvent) {
        // Add event code here...
        this.workListAction("FYI");
    }

    private void workListAction(String action) {
        ADFUtils.findOperation("Commit").execute();

        Number worklistId = (Number) ADFUtils.getBoundAttributeValue("WorklistId");
        //String notes = (String) ADFUtils.getBoundAttributeValue("Notes");

        OperationBinding oper = ADFUtils.findOperation("worklistAction");
        oper.getParamsMap().put("worklistId", worklistId);
        oper.getParamsMap().put("actionTaken", action);
        oper.getParamsMap().put("notes", this.getNtfNotes());
        oper.execute();

        //REFRESH CURRENT ROW
        DCIteratorBinding asWorklistVOIter = ADFUtils.findIterator("AsWorklistVOIterator");
        ViewObjectImpl asWorklistVO = (ViewObjectImpl) asWorklistVOIter.getViewObject();
        asWorklistVO.clearCache();

        Key key = new Key(new Object[] { worklistId });
        asWorklistVOIter.setCurrentRowWithKey(key.toStringFormat(true));


    }


    public void initWorklistBTF() {
        // Add event code here...
        ViewObject asWorklistVO = ADFUtils.findIterator("AsWorklistVOIterator").getViewObject();
        asWorklistVO.setNamedWhereClauseParam("PersonIdPar",
                                              (Number) JSFUtils.resolveExpression("#{sessionScoper.userHelper.personId}"));
    }

    public void initWorklistUI() {
        // Add event code here...
        this.setNtfNotes(null);
    }

    public String validateNtfParams() {
        // Add event code here...
        String notificationIdPar = (String) JSFUtils.resolveExpression("#{pageFlowScope.NotificationIdPar}");
        String accessKeyPar = (String) JSFUtils.resolveExpression("#{pageFlowScope.AccessKeyPar}");
        System.out.println("notificationIdPar=" + notificationIdPar + " ,accessKeyPar=" + accessKeyPar);

        Number notificationIdParAsNumber = null;
        if (notificationIdPar != null) {
            try {
                notificationIdParAsNumber = new Number(notificationIdPar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        DCIteratorBinding ntfWorklistVOIter = ADFUtils.findIterator("NtfWorklistVOIterator");
        ViewObject ntfWorklistVO = ntfWorklistVOIter.getViewObject();
        ntfWorklistVO.setNamedWhereClauseParam("NOTIFICATION_ID_PAR", notificationIdParAsNumber);
        ntfWorklistVO.setNamedWhereClauseParam("ACCESS_KEY_PAR", accessKeyPar);
        ntfWorklistVO.executeQuery();
        Row ntfWorklistRow = ntfWorklistVO.first();
        if (ntfWorklistRow == null) {
            return "INVALID";
        } else {
            Number worklistId = (Number) ntfWorklistRow.getAttribute("WorklistId");
            Number worklistPersonId = (Number) ntfWorklistRow.getAttribute("WorklistPersonId");
            Number WorklistTransId = (Number) ntfWorklistRow.getAttribute("WorklistTransId");
            System.out.println("worklistId=" + worklistId + " ,worklistPersonId=" + worklistPersonId +
                               " ,WorklistTransId=" + WorklistTransId);

            DCIteratorBinding asWorklistVOIter = ADFUtils.findIterator("AsWorklistVOIterator");
            ViewObjectImpl asWorklistVO = (ViewObjectImpl) asWorklistVOIter.getViewObject();
            asWorklistVO.setNamedWhereClauseParam("PersonIdPar", worklistPersonId);
            asWorklistVO.executeQuery();
            Key worklistKey = new Key(new Object[] { worklistId });
            Row asWorklistRow = asWorklistVO.findByKey(worklistKey, 1)[0];
            asWorklistVO.setCurrentRow(asWorklistRow);

            JSFUtils.setExpressionValue("#{pageFlowScope.WorklistTransIdPar}", WorklistTransId);

            return "VALID";
        }
    }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    public void WorkGroupNotification ()
    {
        
        
       // return "VIEW";
        }
    
    public String OpenNotification()
    {
            String worklistActionType = (String) ADFUtils.getBoundAttributeValue("WorklistActionType");
        
        if(worklistActionType.equals("NOTIFY"))
        {
            
            BindingContainer bindings = getBindings();
            OperationBinding operationBinding = bindings.getOperationBinding("UpdateWorkGroupNotification");
            Object result = operationBinding.execute();
//            if (!operationBinding.getErrors().isEmpty()) {
//                return null;
//            }
          
        return "notify";
        }
        else
        {return "open";
        }
        
        
        }


  

   
}

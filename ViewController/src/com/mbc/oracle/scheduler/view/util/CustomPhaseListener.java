package com.mbc.oracle.scheduler.view.util;
import javax.faces.context.FacesContext;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.v2.lifecycle.Lifecycle;
import oracle.adf.controller.v2.lifecycle.PagePhaseEvent;
import oracle.adf.controller.v2.lifecycle.PagePhaseListener;

public class CustomPhaseListener implements PagePhaseListener {
    public CustomPhaseListener() {
        super();
    }

    @Override
    public void afterPhase(PagePhaseEvent pagePhaseEvent) {
        // TODO Implement this method
        if (pagePhaseEvent.getPhaseId() == Lifecycle.PREPARE_RENDER_ID) {
            String currentView = FacesContext.getCurrentInstance()
                                             .getViewRoot()
                                             .getViewId();
            if (!(currentView.endsWith("login"))) {
                UserHelper user = (UserHelper) JSFUtils.getFromSession("userHelper");
                if (user == null) {
                    SchedulerUtil.redirectToView("login");
                }
            }
        }
    }

    @Override
    public void beforePhase(PagePhaseEvent pagePhaseEvent) {
        // TODO Implement this method
    }
}

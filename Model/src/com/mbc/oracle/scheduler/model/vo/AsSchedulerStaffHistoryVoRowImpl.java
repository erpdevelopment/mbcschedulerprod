package com.mbc.oracle.scheduler.model.vo;

import com.mbc.oracle.scheduler.model.eo.AsSchedulerStaffHistoryEOImpl;

import oracle.jbo.Row;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jul 10 16:53:18 GST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AsSchedulerStaffHistoryVoRowImpl extends ViewRowImpl {


    public static final int ENTITY_ASSCHEDULERSTAFFHISTORYEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Rownum1,
        SchedulerStaffId,
        SchedulerId,
        PersonId,
        SchedularStartDate,
        SchedularEndDate,
        StartTimeHours,
        StartTimeMinutes,
        EndTimeHours,
        EndTimeMinutes,
        CreationDate,
        CreatedBy,
        CreatedByName,
        RecurringFlag,
        BulletinFlag,
        ParentSchedulerStaffId,
        ShiftId,
        WorkGroupId,
        Notes,
        FkSchdlrShiftAttributeId,
        FkSchedulerSectionId,
        ReplaceType,
        FkReplacedById,
        EmailNotification,
        SmsNotifiction,
        FkWkReplacedId,
        SourceDate,
        SourceShift,
        SourceShiftAttribute,
        HistoryId,
        ActionType,
        SourceShiftAttributeId,
        SourceShiftId,
        FullName,
        Replacedby,
        ShiftName,
        WorkGroupName,
        Replacementworkgroup,
        ShiftAttributeName,
        Bulletins,
        AsSchedulersVO;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ROWNUM1 = AttributesEnum.Rownum1.index();
    public static final int SCHEDULERSTAFFID = AttributesEnum.SchedulerStaffId.index();
    public static final int SCHEDULERID = AttributesEnum.SchedulerId.index();
    public static final int PERSONID = AttributesEnum.PersonId.index();
    public static final int SCHEDULARSTARTDATE = AttributesEnum.SchedularStartDate.index();
    public static final int SCHEDULARENDDATE = AttributesEnum.SchedularEndDate.index();
    public static final int STARTTIMEHOURS = AttributesEnum.StartTimeHours.index();
    public static final int STARTTIMEMINUTES = AttributesEnum.StartTimeMinutes.index();
    public static final int ENDTIMEHOURS = AttributesEnum.EndTimeHours.index();
    public static final int ENDTIMEMINUTES = AttributesEnum.EndTimeMinutes.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATEDBYNAME = AttributesEnum.CreatedByName.index();
    public static final int RECURRINGFLAG = AttributesEnum.RecurringFlag.index();
    public static final int BULLETINFLAG = AttributesEnum.BulletinFlag.index();
    public static final int PARENTSCHEDULERSTAFFID = AttributesEnum.ParentSchedulerStaffId.index();
    public static final int SHIFTID = AttributesEnum.ShiftId.index();
    public static final int WORKGROUPID = AttributesEnum.WorkGroupId.index();
    public static final int NOTES = AttributesEnum.Notes.index();
    public static final int FKSCHDLRSHIFTATTRIBUTEID = AttributesEnum.FkSchdlrShiftAttributeId.index();
    public static final int FKSCHEDULERSECTIONID = AttributesEnum.FkSchedulerSectionId.index();
    public static final int REPLACETYPE = AttributesEnum.ReplaceType.index();
    public static final int FKREPLACEDBYID = AttributesEnum.FkReplacedById.index();
    public static final int EMAILNOTIFICATION = AttributesEnum.EmailNotification.index();
    public static final int SMSNOTIFICTION = AttributesEnum.SmsNotifiction.index();
    public static final int FKWKREPLACEDID = AttributesEnum.FkWkReplacedId.index();
    public static final int SOURCEDATE = AttributesEnum.SourceDate.index();
    public static final int SOURCESHIFT = AttributesEnum.SourceShift.index();
    public static final int SOURCESHIFTATTRIBUTE = AttributesEnum.SourceShiftAttribute.index();
    public static final int HISTORYID = AttributesEnum.HistoryId.index();
    public static final int ACTIONTYPE = AttributesEnum.ActionType.index();
    public static final int SOURCESHIFTATTRIBUTEID = AttributesEnum.SourceShiftAttributeId.index();
    public static final int SOURCESHIFTID = AttributesEnum.SourceShiftId.index();
    public static final int FULLNAME = AttributesEnum.FullName.index();
    public static final int REPLACEDBY = AttributesEnum.Replacedby.index();
    public static final int SHIFTNAME = AttributesEnum.ShiftName.index();
    public static final int WORKGROUPNAME = AttributesEnum.WorkGroupName.index();
    public static final int REPLACEMENTWORKGROUP = AttributesEnum.Replacementworkgroup.index();
    public static final int SHIFTATTRIBUTENAME = AttributesEnum.ShiftAttributeName.index();
    public static final int BULLETINS = AttributesEnum.Bulletins.index();
    public static final int ASSCHEDULERSVO = AttributesEnum.AsSchedulersVO.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AsSchedulerStaffHistoryVoRowImpl() {
    }

    /**
     * Gets AsSchedulerStaffHistoryEO entity object.
     * @return the AsSchedulerStaffHistoryEO
     */
    public AsSchedulerStaffHistoryEOImpl getAsSchedulerStaffHistoryEO() {
        return (AsSchedulerStaffHistoryEOImpl) getEntity(ENTITY_ASSCHEDULERSTAFFHISTORYEO);
    }

    /**
     * Gets the attribute value for the calculated attribute Rownum1.
     * @return the Rownum1
     */
    public Number getRownum1() {
        return (Number) getAttributeInternal(ROWNUM1);
    }

    /**
     * Gets the attribute value for SCHEDULER_STAFF_ID using the alias name SchedulerStaffId.
     * @return the SCHEDULER_STAFF_ID
     */
    public Number getSchedulerStaffId() {
        return (Number) getAttributeInternal(SCHEDULERSTAFFID);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULER_STAFF_ID using the alias name SchedulerStaffId.
     * @param value value to set the SCHEDULER_STAFF_ID
     */
    public void setSchedulerStaffId(Number value) {
        setAttributeInternal(SCHEDULERSTAFFID, value);
    }

    /**
     * Gets the attribute value for SCHEDULER_ID using the alias name SchedulerId.
     * @return the SCHEDULER_ID
     */
    public Number getSchedulerId() {
        return (Number) getAttributeInternal(SCHEDULERID);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULER_ID using the alias name SchedulerId.
     * @param value value to set the SCHEDULER_ID
     */
    public void setSchedulerId(Number value) {
        setAttributeInternal(SCHEDULERID, value);
    }

    /**
     * Gets the attribute value for PERSON_ID using the alias name PersonId.
     * @return the PERSON_ID
     */
    public Number getPersonId() {
        return (Number) getAttributeInternal(PERSONID);
    }

    /**
     * Sets <code>value</code> as attribute value for PERSON_ID using the alias name PersonId.
     * @param value value to set the PERSON_ID
     */
    public void setPersonId(Number value) {
        setAttributeInternal(PERSONID, value);
    }

    /**
     * Gets the attribute value for SCHEDULAR_START_DATE using the alias name SchedularStartDate.
     * @return the SCHEDULAR_START_DATE
     */
    public Date getSchedularStartDate() {
        return (Date) getAttributeInternal(SCHEDULARSTARTDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULAR_START_DATE using the alias name SchedularStartDate.
     * @param value value to set the SCHEDULAR_START_DATE
     */
    public void setSchedularStartDate(Date value) {
        setAttributeInternal(SCHEDULARSTARTDATE, value);
    }

    /**
     * Gets the attribute value for SCHEDULAR_END_DATE using the alias name SchedularEndDate.
     * @return the SCHEDULAR_END_DATE
     */
    public Date getSchedularEndDate() {
        return (Date) getAttributeInternal(SCHEDULARENDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULAR_END_DATE using the alias name SchedularEndDate.
     * @param value value to set the SCHEDULAR_END_DATE
     */
    public void setSchedularEndDate(Date value) {
        setAttributeInternal(SCHEDULARENDDATE, value);
    }

    /**
     * Gets the attribute value for START_TIME_HOURS using the alias name StartTimeHours.
     * @return the START_TIME_HOURS
     */
    public Number getStartTimeHours() {
        return (Number) getAttributeInternal(STARTTIMEHOURS);
    }

    /**
     * Sets <code>value</code> as attribute value for START_TIME_HOURS using the alias name StartTimeHours.
     * @param value value to set the START_TIME_HOURS
     */
    public void setStartTimeHours(Number value) {
        setAttributeInternal(STARTTIMEHOURS, value);
    }

    /**
     * Gets the attribute value for START_TIME_MINUTES using the alias name StartTimeMinutes.
     * @return the START_TIME_MINUTES
     */
    public Number getStartTimeMinutes() {
        return (Number) getAttributeInternal(STARTTIMEMINUTES);
    }

    /**
     * Sets <code>value</code> as attribute value for START_TIME_MINUTES using the alias name StartTimeMinutes.
     * @param value value to set the START_TIME_MINUTES
     */
    public void setStartTimeMinutes(Number value) {
        setAttributeInternal(STARTTIMEMINUTES, value);
    }

    /**
     * Gets the attribute value for END_TIME_HOURS using the alias name EndTimeHours.
     * @return the END_TIME_HOURS
     */
    public Number getEndTimeHours() {
        return (Number) getAttributeInternal(ENDTIMEHOURS);
    }

    /**
     * Sets <code>value</code> as attribute value for END_TIME_HOURS using the alias name EndTimeHours.
     * @param value value to set the END_TIME_HOURS
     */
    public void setEndTimeHours(Number value) {
        setAttributeInternal(ENDTIMEHOURS, value);
    }

    /**
     * Gets the attribute value for END_TIME_MINUTES using the alias name EndTimeMinutes.
     * @return the END_TIME_MINUTES
     */
    public Number getEndTimeMinutes() {
        return (Number) getAttributeInternal(ENDTIMEMINUTES);
    }

    /**
     * Sets <code>value</code> as attribute value for END_TIME_MINUTES using the alias name EndTimeMinutes.
     * @param value value to set the END_TIME_MINUTES
     */
    public void setEndTimeMinutes(Number value) {
        setAttributeInternal(ENDTIMEMINUTES, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Date getCreationDate() {
        return (Date) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public Number getCreatedBy() {
        return (Number) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(Number value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CreatedByName.
     * @return the CreatedByName
     */
    public String getCreatedByName() {
        return (String) getAttributeInternal(CREATEDBYNAME);
    }


    /**
     * Gets the attribute value for RECURRING_FLAG using the alias name RecurringFlag.
     * @return the RECURRING_FLAG
     */
    public String getRecurringFlag() {
        return (String) getAttributeInternal(RECURRINGFLAG);
    }

    /**
     * Sets <code>value</code> as attribute value for RECURRING_FLAG using the alias name RecurringFlag.
     * @param value value to set the RECURRING_FLAG
     */
    public void setRecurringFlag(String value) {
        setAttributeInternal(RECURRINGFLAG, value);
    }

    /**
     * Gets the attribute value for BULLETIN_FLAG using the alias name BulletinFlag.
     * @return the BULLETIN_FLAG
     */
    public String getBulletinFlag() {
        return (String) getAttributeInternal(BULLETINFLAG);
    }

    /**
     * Sets <code>value</code> as attribute value for BULLETIN_FLAG using the alias name BulletinFlag.
     * @param value value to set the BULLETIN_FLAG
     */
    public void setBulletinFlag(String value) {
        setAttributeInternal(BULLETINFLAG, value);
    }

    /**
     * Gets the attribute value for PARENT_SCHEDULER_STAFF_ID using the alias name ParentSchedulerStaffId.
     * @return the PARENT_SCHEDULER_STAFF_ID
     */
    public Number getParentSchedulerStaffId() {
        return (Number) getAttributeInternal(PARENTSCHEDULERSTAFFID);
    }

    /**
     * Sets <code>value</code> as attribute value for PARENT_SCHEDULER_STAFF_ID using the alias name ParentSchedulerStaffId.
     * @param value value to set the PARENT_SCHEDULER_STAFF_ID
     */
    public void setParentSchedulerStaffId(Number value) {
        setAttributeInternal(PARENTSCHEDULERSTAFFID, value);
    }

    /**
     * Gets the attribute value for SHIFT_ID using the alias name ShiftId.
     * @return the SHIFT_ID
     */
    public Number getShiftId() {
        return (Number) getAttributeInternal(SHIFTID);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIFT_ID using the alias name ShiftId.
     * @param value value to set the SHIFT_ID
     */
    public void setShiftId(Number value) {
        setAttributeInternal(SHIFTID, value);
    }

    /**
     * Gets the attribute value for WORK_GROUP_ID using the alias name WorkGroupId.
     * @return the WORK_GROUP_ID
     */
    public Number getWorkGroupId() {
        return (Number) getAttributeInternal(WORKGROUPID);
    }

    /**
     * Sets <code>value</code> as attribute value for WORK_GROUP_ID using the alias name WorkGroupId.
     * @param value value to set the WORK_GROUP_ID
     */
    public void setWorkGroupId(Number value) {
        setAttributeInternal(WORKGROUPID, value);
    }

    /**
     * Gets the attribute value for NOTES using the alias name Notes.
     * @return the NOTES
     */
    public String getNotes() {
        return (String) getAttributeInternal(NOTES);
    }

    /**
     * Sets <code>value</code> as attribute value for NOTES using the alias name Notes.
     * @param value value to set the NOTES
     */
    public void setNotes(String value) {
        setAttributeInternal(NOTES, value);
    }

    /**
     * Gets the attribute value for FK_SCHDLR_SHIFT_ATTRIBUTE_ID using the alias name FkSchdlrShiftAttributeId.
     * @return the FK_SCHDLR_SHIFT_ATTRIBUTE_ID
     */
    public Number getFkSchdlrShiftAttributeId() {
        return (Number) getAttributeInternal(FKSCHDLRSHIFTATTRIBUTEID);
    }

    /**
     * Sets <code>value</code> as attribute value for FK_SCHDLR_SHIFT_ATTRIBUTE_ID using the alias name FkSchdlrShiftAttributeId.
     * @param value value to set the FK_SCHDLR_SHIFT_ATTRIBUTE_ID
     */
    public void setFkSchdlrShiftAttributeId(Number value) {
        setAttributeInternal(FKSCHDLRSHIFTATTRIBUTEID, value);
    }

    /**
     * Gets the attribute value for FK_SCHEDULER_SECTION_ID using the alias name FkSchedulerSectionId.
     * @return the FK_SCHEDULER_SECTION_ID
     */
    public Number getFkSchedulerSectionId() {
        return (Number) getAttributeInternal(FKSCHEDULERSECTIONID);
    }

    /**
     * Sets <code>value</code> as attribute value for FK_SCHEDULER_SECTION_ID using the alias name FkSchedulerSectionId.
     * @param value value to set the FK_SCHEDULER_SECTION_ID
     */
    public void setFkSchedulerSectionId(Number value) {
        setAttributeInternal(FKSCHEDULERSECTIONID, value);
    }

    /**
     * Gets the attribute value for REPLACE_TYPE using the alias name ReplaceType.
     * @return the REPLACE_TYPE
     */
    public String getReplaceType() {
        return (String) getAttributeInternal(REPLACETYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for REPLACE_TYPE using the alias name ReplaceType.
     * @param value value to set the REPLACE_TYPE
     */
    public void setReplaceType(String value) {
        setAttributeInternal(REPLACETYPE, value);
    }

    /**
     * Gets the attribute value for FK_REPLACED_BY_ID using the alias name FkReplacedById.
     * @return the FK_REPLACED_BY_ID
     */
    public Number getFkReplacedById() {
        return (Number) getAttributeInternal(FKREPLACEDBYID);
    }

    /**
     * Sets <code>value</code> as attribute value for FK_REPLACED_BY_ID using the alias name FkReplacedById.
     * @param value value to set the FK_REPLACED_BY_ID
     */
    public void setFkReplacedById(Number value) {
        setAttributeInternal(FKREPLACEDBYID, value);
    }

    /**
     * Gets the attribute value for EMAIL_NOTIFICATION using the alias name EmailNotification.
     * @return the EMAIL_NOTIFICATION
     */
    public String getEmailNotification() {
        return (String) getAttributeInternal(EMAILNOTIFICATION);
    }

    /**
     * Sets <code>value</code> as attribute value for EMAIL_NOTIFICATION using the alias name EmailNotification.
     * @param value value to set the EMAIL_NOTIFICATION
     */
    public void setEmailNotification(String value) {
        setAttributeInternal(EMAILNOTIFICATION, value);
    }

    /**
     * Gets the attribute value for SMS_NOTIFICTION using the alias name SmsNotifiction.
     * @return the SMS_NOTIFICTION
     */
    public String getSmsNotifiction() {
        return (String) getAttributeInternal(SMSNOTIFICTION);
    }

    /**
     * Sets <code>value</code> as attribute value for SMS_NOTIFICTION using the alias name SmsNotifiction.
     * @param value value to set the SMS_NOTIFICTION
     */
    public void setSmsNotifiction(String value) {
        setAttributeInternal(SMSNOTIFICTION, value);
    }

    /**
     * Gets the attribute value for FK_WK_REPLACED_ID using the alias name FkWkReplacedId.
     * @return the FK_WK_REPLACED_ID
     */
    public Number getFkWkReplacedId() {
        return (Number) getAttributeInternal(FKWKREPLACEDID);
    }

    /**
     * Sets <code>value</code> as attribute value for FK_WK_REPLACED_ID using the alias name FkWkReplacedId.
     * @param value value to set the FK_WK_REPLACED_ID
     */
    public void setFkWkReplacedId(Number value) {
        setAttributeInternal(FKWKREPLACEDID, value);
    }

    /**
     * Gets the attribute value for SOURCE_DATE using the alias name SourceDate.
     * @return the SOURCE_DATE
     */
    public Date getSourceDate() {
        return (Date) getAttributeInternal(SOURCEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for SOURCE_DATE using the alias name SourceDate.
     * @param value value to set the SOURCE_DATE
     */
    public void setSourceDate(Date value) {
        setAttributeInternal(SOURCEDATE, value);
    }

    /**
     * Gets the attribute value for SOURCE_SHIFT using the alias name SourceShift.
     * @return the SOURCE_SHIFT
     */
    public String getSourceShift() {
        return (String) getAttributeInternal(SOURCESHIFT);
    }

    /**
     * Sets <code>value</code> as attribute value for SOURCE_SHIFT using the alias name SourceShift.
     * @param value value to set the SOURCE_SHIFT
     */
    public void setSourceShift(String value) {
        setAttributeInternal(SOURCESHIFT, value);
    }

    /**
     * Gets the attribute value for SOURCE_SHIFT_ATTRIBUTE using the alias name SourceShiftAttribute.
     * @return the SOURCE_SHIFT_ATTRIBUTE
     */
    public String getSourceShiftAttribute() {
        return (String) getAttributeInternal(SOURCESHIFTATTRIBUTE);
    }

    /**
     * Sets <code>value</code> as attribute value for SOURCE_SHIFT_ATTRIBUTE using the alias name SourceShiftAttribute.
     * @param value value to set the SOURCE_SHIFT_ATTRIBUTE
     */
    public void setSourceShiftAttribute(String value) {
        setAttributeInternal(SOURCESHIFTATTRIBUTE, value);
    }

    /**
     * Gets the attribute value for HISTORY_ID using the alias name HistoryId.
     * @return the HISTORY_ID
     */
    public Number getHistoryId() {
        return (Number) getAttributeInternal(HISTORYID);
    }

    /**
     * Sets <code>value</code> as attribute value for HISTORY_ID using the alias name HistoryId.
     * @param value value to set the HISTORY_ID
     */
    public void setHistoryId(Number value) {
        setAttributeInternal(HISTORYID, value);
    }

    /**
     * Gets the attribute value for ACTION_TYPE using the alias name ActionType.
     * @return the ACTION_TYPE
     */
    public String getActionType() {
        return (String) getAttributeInternal(ACTIONTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTION_TYPE using the alias name ActionType.
     * @param value value to set the ACTION_TYPE
     */
    public void setActionType(String value) {
        setAttributeInternal(ACTIONTYPE, value);
    }

    /**
     * Gets the attribute value for SOURCE_SHIFT_ATTRIBUTE_ID using the alias name SourceShiftAttributeId.
     * @return the SOURCE_SHIFT_ATTRIBUTE_ID
     */
    public Number getSourceShiftAttributeId() {
        return (Number) getAttributeInternal(SOURCESHIFTATTRIBUTEID);
    }

    /**
     * Sets <code>value</code> as attribute value for SOURCE_SHIFT_ATTRIBUTE_ID using the alias name SourceShiftAttributeId.
     * @param value value to set the SOURCE_SHIFT_ATTRIBUTE_ID
     */
    public void setSourceShiftAttributeId(Number value) {
        setAttributeInternal(SOURCESHIFTATTRIBUTEID, value);
    }

    /**
     * Gets the attribute value for SOURCE_SHIFT_ID using the alias name SourceShiftId.
     * @return the SOURCE_SHIFT_ID
     */
    public Number getSourceShiftId() {
        return (Number) getAttributeInternal(SOURCESHIFTID);
    }

    /**
     * Sets <code>value</code> as attribute value for SOURCE_SHIFT_ID using the alias name SourceShiftId.
     * @param value value to set the SOURCE_SHIFT_ID
     */
    public void setSourceShiftId(Number value) {
        setAttributeInternal(SOURCESHIFTID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute FullName.
     * @return the FullName
     */
    public String getFullName() {
        return (String) getAttributeInternal(FULLNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute Replacedby.
     * @return the Replacedby
     */
    public String getReplacedby() {
        return (String) getAttributeInternal(REPLACEDBY);
    }

    /**
     * Gets the attribute value for the calculated attribute ShiftName.
     * @return the ShiftName
     */
    public String getShiftName() {
        return (String) getAttributeInternal(SHIFTNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute WorkGroupName.
     * @return the WorkGroupName
     */
    public String getWorkGroupName() {
        return (String) getAttributeInternal(WORKGROUPNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute Replacementworkgroup.
     * @return the Replacementworkgroup
     */
    public String getReplacementworkgroup() {
        return (String) getAttributeInternal(REPLACEMENTWORKGROUP);
    }

    /**
     * Gets the attribute value for the calculated attribute ShiftAttributeName.
     * @return the ShiftAttributeName
     */
    public String getShiftAttributeName() {
        return (String) getAttributeInternal(SHIFTATTRIBUTENAME);
    }

    /**
     * Gets the attribute value for the calculated attribute Bulletins.
     * @return the Bulletins
     */
    public String getBulletins() {
        return (String) getAttributeInternal(BULLETINS);
    }

    /**
     * Gets the associated <code>Row</code> using master-detail link AsSchedulersVO.
     */
    public Row getAsSchedulersVO() {
        return (Row) getAttributeInternal(ASSCHEDULERSVO);
    }

    /**
     * Sets the master-detail link AsSchedulersVO between this object and <code>value</code>.
     */
    public void setAsSchedulersVO(Row value) {
        setAttributeInternal(ASSCHEDULERSVO, value);
    }


}



package ws;

import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.Encoded;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import javax.ws.rs.core.Response;

import javax.ws.rs.core.Response.ResponseBuilder;

import oracle.jdbc.OracleDriver;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;

import java.net.URLDecoder;

import java.sql.CallableStatement;
import java.sql.Types;

import javax.ws.rs.POST;

import oracle.jdbc.internal.OracleTypes;

import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Context;
import javax.annotation.security.RolesAllowed;
import java.security.Principal;

import java.util.stream.Collectors;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

@Path("RESTWs")

public class Login {
    public Login() {
        super();
    }


    @POST
    @Produces("application/json")
    @Path("/xx")  
    public Response allHeaders(@Context  HttpHeaders headers
                                )
    
    {
        MultivaluedMap<String, String> rh = headers.getRequestHeaders();
             String str = rh.entrySet()
                            .stream()
                            .map(e -> e.getKey() + " = " + e.getValue())
                            .collect(Collectors.joining(""));
   str=str.replace("[", "(");
             str=str.replace("]", ")");
             return Helper.StringtoReponse(str);
            // return Response.status(200).entity(str).type("text/plain").build();
         }
        
    

    @POST
    @Produces("application/json")
    @Path("/authenticate")  
    public Response authenticate(@Context SecurityContext sc,
                                 @HeaderParam("s") String userName,
                                 @HeaderParam("a") String userPass,
                                 @HeaderParam("signature") String signature,
                                 @HeaderParam("av") String appVersion,
                                @HeaderParam("attribute1") String appUpdateVersion)
    
    {
        
           if (!sc.isUserInRole("Authenticated"))  
           throw new SecurityException("User is unauthorized.");
//           
        
                  
                    
            if (userName==null || userPass==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
       

            
            Connection connection = null;
     
            Response response = null;
            CallableStatement cs=null;
            if (userPass!=null)
            try {
                userPass = URLDecoder.decode(userPass, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        String  sqlStmt="BEGIN\n" + 
       "  ? := apps.mbc_mobile_pkg.login( p_user_name=> ?, p_password=>?,P_VERSION => ?,P_SIGNATURE => ? , p_wsid=> ? \n" + 
            ");\n" + 
       "END;";
            try {
                connection = Helper.getOracleConnection();
                Helper.logActivity(connection, "0", "0", "Try Login version is "+appUpdateVersion, "User is: "+userName,userName);
                  cs=  connection.prepareCall(sqlStmt);
                  cs.setString(2, userName);
                  cs.setString(3, userPass);
                  cs.setString(4, appVersion);
                  cs.setString(5, signature);
                  
                cs.registerOutParameter(1, Types.VARCHAR);
                cs.registerOutParameter(6, Types.VARCHAR);
                
                cs.executeUpdate();
                  
                  String result=cs.getString(1);
                  String wsid=cs.getString(6);
                
                
                
                  if (!"OK".equalsIgnoreCase(result))
                  {
                      response = Helper.StringtoReponse(result,"0");
                      
                }
                
                else
                    { 
                          response= getSessioninfo(connection,wsid);
                      
                      
                      }
            }
            catch(Exception e)
            {
                
                    response = Helper.StringtoReponse("Error:101, Please contact system administrator"+e.getMessage(),"0");
                e.printStackTrace();
                }
            finally {
                        try {
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                cs.close();
                           
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
            
            
            return response;
        }
    
 

    public Response getSessioninfo (   Connection connection, String wsid )
    {
        
        
            
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select sequence_no , property_name , property_value from  table (MBC_MOBILE_PKG.GET_session_info(?))";
            try {
         
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, wsid);  
               
               
             rs = statement.executeQuery();
       
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                    
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
               
                map.writeValue(outPut, m);
                
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((statement != null) && !(statement.isClosed()))
                             
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                                                   rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }



 //   @POST
  //  @Produces("application/json")
  //  @Path("/getloginPostOld")  
    public Response getloginPostOld(@Context SecurityContext sc,
                                 @HeaderParam("userName") String userName,
                                 @HeaderParam("userPass") String userPass,
                                 @HeaderParam("appVersion") String appVersion)
    
    {
        
           if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
       
        
 
            
            Connection connection = null;
     
            Response response = null;
            ResultSet rs =null;
            CallableStatement cs=null;
            if (userPass!=null)
            userPass=URLDecoder.decode(userPass);
       // query not used  all data fteched from mbc_mobile_pkg
       String sqlQuery=
           "SELECT PAPF.PERSON_ID,PAPF.BUSINESS_GROUP_ID,USER_ID , ASSIGNMENT_ID,\n" + 
            "(SELECT count(*) ntf FROM MBC_WORKLIST_NOTIFICATION_V MN, MBC_WORKLIST_NTF_CONFIG_V MNB WHERE MN.MESSAGE_TYPE = MNB.MESSAGE_TYPE  AND   MN.MESSAGE_NAME = MNB.MESSAGE_NAME and  nvl(mn.process_name , 1) = NVL(MNB.process_name ,nvl(mn.process_name , 1) ) AND   MN.RECIPIENT_ROLE =  FU.USER_NAME) ntf_no\n"+
       "FROM FND_USER FU, PER_ALL_PEOPLE_F PAPF , PER_ALL_ASSIGNMENTS_F PAAF\n" + 
       "WHERE FU.EMPLOYEE_ID = PAPF.PERSON_ID\n" + 
       "AND PAPF.PERSON_ID = PAAF.PERSON_ID\n" + 
       "AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'\n" + 
       "AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE\n" + 
       "AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE AND PAAF.EFFECTIVE_END_DATE\n" + 
       "AND FU.USER_NAME = upper(?)";
       
       String sqlStmt=
             "declare \n" + "l_test varchar2(10);\n" +
             "begin l_test :=fnd_web_sec.validate_login(?, ?);\n" +
             "if l_test = 'Y' then \n" + "? := 1;\n" + "end if;\n" +
             "end;\n" ;
       
       sqlStmt="BEGIN\n" + 
       "  ? := apps.mbc_mobile_pkg.login( p_user_name=> ?, p_password=>?,P_VERSION =>? , p_person_id=> ?,\n" + 
       "  p_business_group_id=> ? ,\n" + 
       "    p_user_id=> ? ,\n" + 
       "    p_assignment_id=> ?,\n" + 
       "    p_ntf_count=>?);\n" + 
       "\n" + 
       "\n" + 
       "END;";
            try {
                connection = Helper.getOracleConnection();
                Helper.logActivity(connection, "0", "0", "Try Login", "User is: "+userName,userName);
                  cs=  connection.prepareCall(sqlStmt);
                  cs.setString(2, userName);
                  cs.setString(3, userPass);
                  cs.setString(4, appVersion);
                  
                cs.registerOutParameter(1, Types.VARCHAR);
                cs.registerOutParameter(5, Types.VARCHAR);
                cs.registerOutParameter(6, Types.VARCHAR);
                cs.registerOutParameter(7, Types.VARCHAR);
                cs.registerOutParameter(8, Types.VARCHAR);
                cs.registerOutParameter(9, Types.VARCHAR);
                cs.executeUpdate();
                  
                  String result=cs.getString(1);
                
                
                
                  if (!"OK".equalsIgnoreCase(result))
                  {
                      response = Helper.StringtoReponse(result,"0");
                      
                }
                
                else
                    { 
                      
                      response = Helper.userDtaReponse(cs.getString(5),cs.getString(6),cs.getString(7),cs.getString(8),cs.getString(9),"1");
              
                                
                  
                  
                      
                        
                      
                      
                      }
            }
            catch(Exception e)
            {
                    response = Helper.StringtoReponse("Error:101, Please contact system administrator","0");
                e.printStackTrace();
                }
            finally {
                        try {
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                cs.close();
                           
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
            
            
            return response;
        }
    
    
    @POST
    @Produces("application/json")
    @Path("/registerAio")  
    public Response registerAio(@Context SecurityContext sc,
                                @HeaderParam("wsid") String wsid,
                                @HeaderParam("signature") String signature
                                )
    
    {
        
           if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
       
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
 
            
            Connection connection = null;
     
            Response response = null;
            CallableStatement cs=null;
            
            
          
            
//            try {
//            wsid = URLDecoder.decode(wsid);
//            wsid=URLDecoder.decode(wsid);
//            signature = URLDecoder.decode(signature);
//        } catch (Exception e) {
//            // TODO: Add catch code
//            return Response.status(200).entity(e.getMessage()).type("text/plain").build();
//        }
//            
          
           
       
       String sqlStmt="BEGIN\n" + 
       "  ? := apps.mbc_mobile_pkg.register_aio(?,?); END; "; 
       
            try {
               
                connection = Helper.getOracleConnection();
                
               
                Helper.logActivity(connection, "Try login AIO",wsid );
                  cs=  connection.prepareCall(sqlStmt);
                  
                cs.setString(2, wsid); 
                cs.setString(3, signature); 
                cs.registerOutParameter(1, Types.VARCHAR);
               
                cs.executeUpdate();
                  
                  String result=cs.getString(1);
              
                
                  if (result.contains("Error"))
                  {
                      response = Helper.StringtoReponse(result,"0");
                      
                }
                
                else
                    { 
                      
              
                          response=Response.ok(result).build();
                      
                      }
            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                    response = Helper.StringtoReponse("Error:101, Please contact system administrator","0");
                e.printStackTrace();
                }
            finally {
                        try {
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                cs.close();
                           
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
            
            
            return response;
        }
    
    
    @POST
    @Produces("application/json")
    @Path("/loginAio")  
    public Response loginAio(@Context SecurityContext sc,
                            @HeaderParam("tk") String token,
                            @HeaderParam("signature") String signature,
                            @HeaderParam("av") String appVersion,
                            @HeaderParam("attribute1") String appUpdateVersion)
    
    {
        
           if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
       
            if (token==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            if ( signature.length()<48 )  
           return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            token=URLDecoder.decode(token);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
     
            Response response = null;
            CallableStatement cs=null;
            
            String key=null;
            String iv=null;


        String  sqlStmt="BEGIN\n" + 
       "  ? := apps.mbc_mobile_pkg.login_aio(?, ?,?,?,? ); end; ";
            try {
                connection = Helper.getOracleConnection();
              //  Helper.logActivity(connection, "0", "0", "Try Aio Login", "Token is: "+userName,userName);
              Helper.logActivity(connection, "0", "0", "Try AIO Login version is "+appUpdateVersion, "Token is: "+token,token);
                  cs=  connection.prepareCall(sqlStmt);
                
                key=signature.substring(0,32);
                iv=signature.substring(32,48);
                
                  cs.setString(2, token);
                  cs.setString(3, key);
                  cs.setString(4, iv);
                  cs.setString(5, appVersion);
                  
                cs.registerOutParameter(1, Types.VARCHAR);
                cs.registerOutParameter(6, Types.VARCHAR);
                
                cs.executeUpdate();
                  
                  String result=cs.getString(1);
                  String wsidx=cs.getString(6);
                
                
                
                  if (!"OK".equalsIgnoreCase(result))
                  {
                      response = Helper.StringtoReponse(result,"0");
                      
                }
                
                else
                    { 
                          response= getSessioninfo(connection,wsidx);
                      
                      
                      }
            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                    response = Helper.StringtoReponse("Error:101, Please contact system administrator","0");
                e.printStackTrace();
                }
            finally {
                        try {
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                cs.close();
                           
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
            
            
            return response;
        }
   
    @POST
    @Produces("application/json")
    @Path("/logout")  
    public Response logout(@Context SecurityContext sc,
                                @HeaderParam("wsid") String wsid,
                                @HeaderParam("signature") String signature
                                )
    
    {
        
           if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
       
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
    
            
            Connection connection = null;
     
            Response response = null;
            CallableStatement cs=null;
            
            
            
//            wsid=URLDecoder.decode(wsid);
//            signature=URLDecoder.decode(signature);

       
       String sqlStmt="BEGIN\n" + 
       "  ? := apps.mbc_mobile_pkg.logout(?,?); END; "; 
       
            try {
                connection = Helper.getOracleConnection();
                Helper.logActivity(connection, "logout",wsid );
                  cs=  connection.prepareCall(sqlStmt);
                  
                cs.setString(2, wsid); 
                cs.setString(3, signature); 
                cs.registerOutParameter(1, Types.VARCHAR);
               
                cs.executeUpdate();
                  
                  String result=cs.getString(1);
                
                
                
                  if (!"OK".equalsIgnoreCase(result))
                  {
                      response = Helper.StringtoReponse(result,"0");
                      
                }
                
                else
                    { 
                      
              
                          response=Response.ok(result).build();
                      
                      }
            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                    response = Helper.StringtoReponse("Error:101, Please contact system administrator"+e.getMessage(),"0");
                e.printStackTrace();
                }
            finally {
                        try {
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                cs.close();
                           
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
            
            
            return response;
        }
    
    
}


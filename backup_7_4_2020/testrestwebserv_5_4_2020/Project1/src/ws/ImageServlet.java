package ws;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.net.URLDecoder;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.http.*;

public class ImageServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType(CONTENT_TYPE);
//        PrintWriter out = response.getWriter();
//        out.println("<html>");
//        out.println("<head><title>ImageServlet</title></head>");
//        out.println("<body>");
//        out.println("<p>The servlet has received a GET. This is the reply.</p>");
//        out.println("</body></html>");
//        out.close();
          OutputStream os=response.getOutputStream();
        String personId=request.getParameter("px");
        String wsid=request.getParameter("token");
        String signature=request.getParameter("signature");
        if (personId==null ||wsid==null||signature==null )
        {
            return;
            }
        try
        {
        
        wsid=URLDecoder.decode(wsid);
        signature=URLDecoder.decode(signature);
        }
        catch(Exception e)
        {
              
            e.printStackTrace();
                return;
            }
        
        wsid=wsid.replaceAll("@@@@", "#");
        
        Blob img = getImage(personId,wsid,signature);
         if (img!=null)
         {
             System.out.println("inside  image servelet");
        BufferedInputStream in=null;
            try {
                in = new BufferedInputStream(img.getBinaryStream());
            } catch (SQLException e) {
            }
            int b;
        byte [] buffer =new byte[10240];
        
            while ((b = in.read(buffer, 0, 10240)) != -1) {
                os.write(buffer, 0, b);

            }
        
        os.close();
        in.close();
        
         }
    }
    
    public static  Blob getImage(String personId,String wsid , String signature)
        {
               
                System.out.println("person id is "+personId);
                PreparedStatement ps=null;
                ResultSet rs=null;
                Blob img=null;
                Connection connection = null;
                String sqlStemt="SELECT  image image \n" + 
                "FROM PER_IMAGES \n" + 
                "WHERE TABLE_NAME='PER_PEOPLE_F'  \n" + 
                "\n" + 
                "and PARENT_ID=?";
                try{
                    connection = Helper.getOracleConnection();
                    String result=Helper.checkSession(connection, wsid, signature , wsid+personId);
                    if (!"OK".equals(result))
                    throw new SecurityException("Unauthorized Access."); 
                    ps = connection.prepareStatement(sqlStemt);
                  
                 
                    ps.setString(1,personId );
                  
                    rs=ps.executeQuery();
                   
                    while (rs.next())
                    {
                           img=rs.getBlob(1); 
                            
                        break;
                        }
                
                }catch(Exception e)
                {
                    
                     e.printStackTrace();
                     return img;
                 }
                finally {
                            try {
                              
                                if ((connection != null) && !(connection.isClosed()))
                                    connection.close();
                                if ((ps != null) && !(ps.isClosed()))
                                              ps.close();
                                if ((rs != null) && !(rs.isClosed()))
                                rs.close();
                            } catch (SQLException sqle) {
                                // TODO: Add catch code
                                sqle.printStackTrace();
                            }
                        }
              
                return img;
            }
}

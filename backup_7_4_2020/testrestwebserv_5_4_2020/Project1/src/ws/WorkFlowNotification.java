package ws;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

@Path("wf/notications")
public class WorkFlowNotification {
    public WorkFlowNotification() {
        super();
    }




    @POST
    @Produces("application/json")
    @Path("/getWorkListPost")
    public Response getWorkListPost(@Context SecurityContext sc 
                                    , @HeaderParam("wsid") String wsid
                                     ,@HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
             throw new SecurityException("User is unauthorized.");
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT NOTIFICATION_ID, nvl(FROM_USER,'SYSADMIN') FROM_USER,SUBJECT,TYPE , MN.MESSAGE_TYPE,MN.MESSAGE_NAME,ITEM_TYPE,ITEM_KEY , /*MBC_MOBILE_PKG.BASE64ENCODE(IMAGE)*/null IMG,\n" + 
            "to_char(begin_date,'DD-MON-RRRR') BEGIN_DATE , PROCESS_ACTIVITY, FILTER_TYPE , \n" + 
            " DECODE (TRIM(nvl(FROM_USER,'SYSADMIN')) , 'SYSADMIN' , 'SYS' , (SUBSTR (TRIM(REPLACE(nvl(FROM_USER,'SYSADMIN'),' ','')) , INSTR(TRIM(REPLACE(nvl(FROM_USER,'SYSADMIN'),' ','')), ',' )+1,1))|| UPPER(SUBSTR (TRIM(nvl(FROM_USER,'SYSADMIN')),1,1) ))INTIALS,\n" + 
            "'class'||(MOD(LENGTH(nvl(FROM_USER,'SYSADMIN')),4)+1)/*(ROUND(DBMS_RANDOM.VALUE() * 2) + 1)*/ CLASS_NAME,GENERATOR_API,IS_TEXT_BODY,IS_FYI,DECODE (MN.MORE_INFO_ROLE , NULL , BASE_ACTIONS , MORE_INFO_ACTIONS ) BASE_ACTIONS,DECODE (MN.MORE_INFO_ROLE , NULL , MORE_ACTIONS , NULL ) MORE_ACTIONS\n" + 
            "FROM MBC_WORKLIST_NOTIFICATION_V MN, MBC_WORKLIST_NTF_CONFIG_V MNB\n" + 
            "WHERE MN.MESSAGE_TYPE = MNB.MESSAGE_TYPE\n" + 
            "AND   MN.MESSAGE_NAME = MNB.MESSAGE_NAME\n" + 
            "and  nvl(mn.process_name , 1) = NVL(MNB.process_name ,nvl(mn.process_name , 1) )\n" + 
            "AND   MN.RECIPIENT_ROLE = upper( mbc_mobile_pkg.get_session_data('USER_NAME') ) \n" + 
            "order by notification_id desc";
            try {

                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getWorkListPost");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                Helper.logActivity(connection,  "Open Worklist",  "Open Worklist" );
                statement = connection.prepareStatement(sqlStemt);
                
               
               
             rs = statement.executeQuery();
          

               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSetNestedJsonArray(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
             {
                 s.printStackTrace();
                 response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                                                     connection.close();
                                                       if ((statement != null) && !(statement.isClosed()))
                                                                 statement.close();
                                                        if ((rs != null) && !(rs.isClosed()))
                                                                    rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }


    @POST
    @Produces("application/json")
    @Path("/takeNotificationAction")
    public Response takeNotificationAction(@Context SecurityContext sc ,
                                           @HeaderParam("wsid") String wsid,
                                           @HeaderParam("signature") String signature,
                                           @HeaderParam("ntf") String notificationId,
                                           @HeaderParam("it") String itemType,
                                           @HeaderParam("ik") String itemKey,
                                           @HeaderParam("ai") String activityId,
                                           @HeaderParam("bn") String buttonName,
                                           @HeaderParam("attributes") String attributes)
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            CallableStatement cs=null;
            Response response = null;
            String sqlStemt="begin " +
            "?:=APPS.MBC_MOBILE_PKG.NOTIFICATION_ACTION ( ?, ?, ?, ?, ? , mbc_mobile_pkg.get_session_data('USER_NAME') , ? );" +
            "commit; " +
            "end;";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String resultx=Helper.checkSession(connection, wsid, signature , wsid+"takeNotificationAction");
                if (!"OK".equals(resultx))
                throw new SecurityException("Unauthorized Access."); 
                Helper.logActivity(connection,  "Open Worklist", "NotificationId is: "+notificationId +"and Action is "+buttonName );
            cs = connection.prepareCall(sqlStemt);
                cs.registerOutParameter(1, Types.VARCHAR);
                cs.setString(2, notificationId);  
                cs.setString(3, itemType);  
                cs.setString(4, itemKey);  
                cs.setString(5, activityId);
                cs.setString(6, buttonName);  
               
                cs.setString(7, attributes);
               
               
                cs.executeUpdate();
         String   result=cs.getString(1);
       
                System.out.println("result is "+result );
                if (!"success".equalsIgnoreCase(result))
                {
                    
                    response = Helper.StringtoReponse(result,"0");
                    
                    }
                  else
                {
                    
                                PreparedStatement statementx = null;
                                    ResultSet rs =null;
                                    String ntfCount="0";
                                    try {
                                        
                                            statementx=connection.prepareStatement("SELECT count(*) ntf FROM MBC_WORKLIST_NOTIFICATION_V MN, MBC_WORKLIST_NTF_CONFIG_V MNB WHERE MN.MESSAGE_TYPE = MNB.MESSAGE_TYPE and  nvl(mn.process_name , 1) = NVL(MNB.process_name ,nvl(mn.process_name , 1) )  AND   MN.MESSAGE_NAME = MNB.MESSAGE_NAME  AND   MN.RECIPIENT_ROLE = upper(mbc_mobile_pkg.get_session_data('USER_NAME'))");
                                            
                                            
                                            
                                             rs = statementx.executeQuery();
                                            while (rs.next()) {
                                            ntfCount=rs.getString("ntf");
                                                if("0".equalsIgnoreCase(ntfCount))
                                                    ntfCount = "00";
                                            }
                                            response = Helper.StringtoReponse(result,ntfCount+"");
                                       
                                        }
                                    catch(Exception e)
                                    {
                                            e.printStackTrace();
                                            response = Helper.StringtoReponse(result,"00");
                                       
                                        }
                    finally {
                                try  {
                                    statementx.close();
                                    rs.close();
                                    }
                                catch(Exception x){
                                         }
                    }
                    
                    
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse( e.getMessage().replace("\n", "").replace("\r", ""),"0");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                                          cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }


    @POST
    @Produces("application/json")
    @Path("/getNotoficationBody")
    public Response getNotoficationBody(@Context SecurityContext sc ,
                                        @HeaderParam("ntf") String notificationId,
                                        @HeaderParam("it") String itemType, @HeaderParam("ik") String itemKey,
                                        @HeaderParam("gn") String generatorApi
                                        ,@HeaderParam("wsid") String wsid
                                        ,@HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            String sqlStemt="select   MBC_NOTIFICATION_HTML.GET_HTML(?,?, mbc_mobile_pkg.get_session_data('USER_NAME') ,?,?) html_data FROM DUAL";
            ResultSet rs=null;
            try {
         
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getNotoficationBody");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                Helper.logActivity(connection, "Open Notofication Body", "Open Notofication Body"+notificationId );
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, itemType);  
                statement.setString(2, itemKey);  
                statement.setString(3, notificationId); 
                statement.setString(4, generatorApi);  
                
               
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                                                     connection.close();
                                                       if ((statement != null) && !(statement.isClosed()))
                                                                 statement.close();
                                                        if ((rs != null) && !(rs.isClosed()))
                                                                    rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    @POST
    @Produces("application/json")
    @Path("/getNotificationHistory")
    public Response getNotificationHistory( @Context SecurityContext sc ,
                                            @HeaderParam("ntf") String notificationId,
                                            @HeaderParam("ik") String itemKey,
                                            @HeaderParam("it") String itemType,
                                            @HeaderParam("pa") String processActivity,
                                            @HeaderParam("ft") String filterType,
                                            @HeaderParam("wsid") String wsid,
                                            @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs =null;
            String sqlStemt="SELECT * from  table (MBC_MOBILE_PKG.GET_ACTION_HISTORY_LIST(?,?,?,?,?))";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getNotificationHistory");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, notificationId); 
                statement.setString(2, itemType);  
                statement.setString(3, itemKey);  
                statement.setString(4, processActivity); 
                statement.setString(5, filterType); 
               
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSetNestedJsonArray(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   // response = Response.status(404).entity("Unable to find record").type("text/plain").build();
                }
                

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
               
                e.printStackTrace();
                response = Helper.StringtoReponse( e.getMessage().replace("\n", "").replace("\r", ""),"0");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }


    //@POST
    @Produces("application/json")
    @Path("/getWorkListPostRange")
    public Response getWorkListPostRange(@HeaderParam("toUser") String toUser,
                                         @HeaderParam("fromR") String fromR,
                                         @HeaderParam("toR") String toR,
                                         @HeaderParam("wsid") String wsid,
                                         @HeaderParam("signature") String signature)
    {
        
        
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            String sqlStemt=" select * from (SELECT rownum t , NOTIFICATION_ID,FROM_USER,SUBJECT,TYPE , MN.MESSAGE_TYPE,MN.MESSAGE_NAME,ITEM_TYPE,ITEM_KEY , /*MBC_MOBILE_PKG.BASE64ENCODE(IMAGE)*/null IMG,\n" + 
            "to_char(begin_date,'DD-MON-RRRR') BEGIN_DATE , PROCESS_ACTIVITY, FILTER_TYPE , \n" + 
            " DECODE (FROM_USER , 'SYSADMIN' , 'SYS' , (SUBSTR (FROM_USER , INSTR(FROM_USER, ',' )+2,1) || UPPER(SUBSTR (FROM_USER,1,1))))INTIALS,\n" + 
            "'class'||(MOD(LENGTH(FROM_USER),4)+1)/*(ROUND(DBMS_RANDOM.VALUE() * 2) + 1)*/ CLASS_NAME,GENERATOR_API,IS_TEXT_BODY,IS_FYI,BASE_ACTIONS,MORE_ACTIONS\n" + 
            "FROM MBC_WORKLIST_NOTIFICATION_V MN, MBC_WORKLIST_NTF_CONFIG_V MNB\n" + 
            "WHERE MN.MESSAGE_TYPE = MNB.MESSAGE_TYPE\n" + 
            "AND   MN.MESSAGE_NAME = MNB.MESSAGE_NAME\n" +
            "and  nvl(mn.process_name , 1) = NVL(MNB.process_name ,nvl(mn.process_name , 1) ) \n"+
            "AND   MN.RECIPIENT_ROLE = upper(?) ) where t between nvl(?,0) and nvl(?,20)";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, toUser);  
                statement.setString(2, fromR);
                statement.setString(3, toR);
               
            ResultSet rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSetNestedJsonArray(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    response = Response.status(404).entity("Unable to find record").type("text/plain").build();
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                                          statement.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    @POST
    @Produces("application/json")
    @Path("/getWorkListCountPost")
    public Response getWorkListCountPost (@Context SecurityContext sc,
                                          @HeaderParam("wsid") String wsid,
                                          @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT count(*) ntf FROM MBC_WORKLIST_NOTIFICATION_V MN, MBC_WORKLIST_NTF_CONFIG_V MNB WHERE MN.MESSAGE_TYPE = MNB.MESSAGE_TYPE  and  nvl(mn.process_name , 1) = NVL(MNB.process_name ,nvl(mn.process_name , 1) ) AND   MN.MESSAGE_NAME = MNB.MESSAGE_NAME  AND   MN.RECIPIENT_ROLE = upper(  mbc_mobile_pkg.get_session_data('USER_NAME') )";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getWorkListCountPost");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
            statement = connection.prepareStatement(sqlStemt);
               
               
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSetNestedJsonArray(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                                                    connection.close();
                                                      if ((statement != null) && !(statement.isClosed()))
                                                                statement.close();
                                                       if ((rs != null) && !(rs.isClosed()))
                                                                   rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    



    @POST
    @Produces("application/json")
    @Path("/getUsers")
    public Response getUsers(@Context SecurityContext sc ,
                             @HeaderParam("wsid") String wsid,
                             @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT  U.USER_NAME m,  p.first_name ||' '|| p.last_name  g , P.EMPLOYEE_NUMBER j , \n" + 
            "                DECODE (TRIM(GLOBAL_NAME) , 'SYSADMIN' , 'SYS' , SUBSTR (TRIM(REPLACE(GLOBAL_NAME,' ','')) , INSTR(TRIM(REPLACE(GLOBAL_NAME,' ','')), ',' )+1,1))|| UPPER(SUBSTR (TRIM(GLOBAL_NAME),1,1) )INTIALS ,\n" + 
           "'class'||(MOD(LENGTH(TRIM(GLOBAL_NAME)),4)+1) CLASS_NAME , p.person_id d \n"+
            "FROM FND_USER U, PER_ALL_PEOPLE_F P\n" + 
            "WHERE U.EMPLOYEE_ID = P.PERSON_ID\n" + 
            "AND SYSDATE BETWEEN P.EFFECTIVE_START_DATE AND P.EFFECTIVE_END_DATE\n" + 
            "AND P.CURRENT_EMPLOYEE_FLAG = 'Y'\n" + 
            " AND  DECODE (  mbc_mobile_pkg.get_session_data('PERSON_ID')  , P.PERSON_ID , -1 ,P.PERSON_ID )   =  P.PERSON_ID  \n"+
            "AND SYSDATE BETWEEN U.START_DATE AND NVL(END_DATE,SYSDATE+1) \n" +
            "ORDER BY P.FIRST_NAME";
            System.out.println(sqlStemt);
       
            try {

                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getUsers");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
            statement = connection.prepareStatement(sqlStemt);
              

               
             rs = statement.executeQuery();

               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    @POST
    @Produces("application/json")
    @Path("/takeReassignAction")
    public Response takeReassignAction(    @Context SecurityContext sc,
                                           @HeaderParam("wsid") String wsid,
                                           @HeaderParam("signature") String signature,
                                           @HeaderParam("ntf") String notificationId,
                                           @HeaderParam("newRole") String newRole,
                                           @HeaderParam("comments") String comments,
                                           @HeaderParam("bn") String buttonName)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            Connection connection = null;
            CallableStatement cs=null;
            Response response = null;
            String reassignStemt = "begin " +
            "APPS.wf_notification.forward ( ?, ?, ?, mbc_mobile_pkg.get_session_data('USER_NAME') );" +
            "commit; " +
            "end;";
            
            String requestMoreInfoStemt = "begin " +
            "APPS.wf_notification.updateInfo ( ?, ?, ?, mbc_mobile_pkg.get_session_data('USER_NAME') );" +
            "commit; " +
            "end;";
            
            String sqlStemt = null;
            
            if (buttonName.equalsIgnoreCase("REASSIGN"))
                sqlStemt=reassignStemt;
            else if (buttonName.equalsIgnoreCase("MORE INFORMATION"))
            sqlStemt= requestMoreInfoStemt;
            else
            {
                    response = Helper.StringtoReponse(buttonName,"0");
                    return response;
                }
            
            
            try {
                if (comments !=null)
                    comments=URLDecoder.decode(comments);
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"takeReassignAction");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection,  "Open Worklist", "NotificationId is: "+notificationId+" and action is "+buttonName);
            cs = connection.prepareCall(sqlStemt);
                cs.setString(1, notificationId);  
                cs.setString(2, newRole);  
                cs.setString(3, comments);  
               // cs.setString(4, fromRole);
                
               
                cs.executeUpdate();
       
               
                    
                    response = Helper.StringtoReponse("Success","0");
                    
                       
                  
            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                response = Helper.StringtoReponse("Fail","0");
                e.printStackTrace();
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                                          cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    @POST
    @Produces("application/json")
    @Path("/getNotificationAttributes")
    public Response getNotificationAttributes(@Context SecurityContext sc ,
                                              @HeaderParam("ntf") String notificationId,
                                              @HeaderParam("wsid") String wsid,
                                              @HeaderParam("signature") String signature
                                              )
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
        
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT MsgAttribsEO.NAME,\n" + 
            "         MsgAttribsEO.DISPLAY_NAME,\n" + 
            "         MsgAttribsEO.DESCRIPTION,\n" + 
            "         NtfAttribsEO.TEXT_VALUE,\n" + 
            "         NtfAttribsEO.NUMBER_VALUE,\n" + 
            "         NtfAttribsEO.DATE_VALUE,\n" + 
            "         MsgAttribsEO.TYPE,\n" + 
            "         MsgAttribsEO.FORMAT,\n" + 
            "         MsgAttribsEO.MESSAGE_TYPE,\n" + 
            "         MsgAttribsEO.MESSAGE_NAME,\n" + 
            "         NtfAttribsEO.NOTIFICATION_ID\n" + 
            "    FROM WF_NOTIFICATION_ATTRIBUTES NtfAttribsEO,\n" + 
            "         WF_MESSAGE_ATTRIBUTES_VL MsgAttribsEO,\n" + 
            "         WF_NOTIFICATIONS NtfEO\n" + 
            "   WHERE     (NtfAttribsEO.NAME = MsgAttribsEO.NAME)\n" + 
            "         AND (    (MsgAttribsEO.MESSAGE_TYPE = NtfEO.MESSAGE_TYPE)\n" + 
            "              AND (MsgAttribsEO.MESSAGE_NAME = NtfEO.MESSAGE_NAME))\n" + 
            "         AND (    NtfEO.NOTIFICATION_ID = ? and NtfEO.recipient_role= upper(mbc_mobile_pkg.get_session_data('USER_NAME') ) \n" + 
            "              AND NtfAttribsEO.NOTIFICATION_ID = NtfEO.NOTIFICATION_ID\n" + 
            "              AND MsgAttribsEO.MESSAGE_NAME = NtfEO.MESSAGE_NAME\n" + 
            "              AND MsgAttribsEO.MESSAGE_TYPE = NtfEO.MESSAGE_TYPE\n" + 
            "              AND MsgAttribsEO.NAME = NtfAttribsEO.NAME\n" + 
            "              AND MsgAttribsEO.SUBTYPE = 'RESPOND'\n" + 
            "              AND MsgAttribsEO.TYPE <> 'FORM'\n" + 
            "              AND MsgAttribsEO.NAME <> 'RESULT')\n" + 
            "ORDER BY MsgAttribsEO.SEQUENCE\n";
            try {

                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getNotificationAttributes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
            statement = connection.prepareStatement(sqlStemt);
              
                statement.setString(1, notificationId);
               
             rs = statement.executeQuery();

               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
               //     response = Response.status(404).entity("Unable to find record").type("text/plain").build();
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }



    @POST
    @Produces("application/json")
    @Path("/getLookUp")
    public Response getLookUp (@Context SecurityContext sc, 
                               @HeaderParam("lookUpType") String lookUpType,
                               @HeaderParam("wsid") String wsid,
                               @HeaderParam("signature") String signature)
    {
        

            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
             
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select lookup_code , meaning  ,  lookup_type\n" + 
            "from WF_LOOKUPS_TL\n" + 
            "where LOOKUP_TYPE = ?\n" + 
            "AND LANGUAGE = 'US'\n";
            try {

                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getLookUp");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
            statement = connection.prepareStatement(sqlStemt);
              
                statement.setString(1, lookUpType);
               
             rs = statement.executeQuery();

               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
               //     response = Response.status(404).entity("Unable to find record").type("text/plain").build();
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }


// delegation 
    @POST
    @Produces("application/json")
    @Path("/getDelegationHistory")
    public Response getDelegationHistory(@Context SecurityContext sc, 
                                         @HeaderParam("wsid") String wsid,
                                         @HeaderParam("signature") String signature
                                      )
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select wfr.rule_id,wfr.role , to_char(wfr.begin_date,'DD-MON-YYYY') begin_date,to_char(wfr.end_date,'DD-MON-YYYY')  end_date  ,wfr.rule_comment, wfr.action_argument,case when sysdate > wfr.end_date then 'Inactive' else 'Active' end status  ,\n" + 
            "        papf.first_name ||' '|| papf.last_name globa_name, 'class'||(MOD(LENGTH(papf.local_name),4)+1) CLASS_NAME , message_type , \n" + 
            "    DECODE (TRIM(GLOBAL_NAME) , 'SYSADMIN' , 'SYS' , SUBSTR (TRIM(REPLACE(GLOBAL_NAME,' ','')) , INSTR(TRIM(REPLACE(GLOBAL_NAME,' ','')), ',' )+1,1))|| UPPER(SUBSTR (TRIM(GLOBAL_NAME),1,1) )INTIALS \n" + 
            ",nvl((SELECT T.DISPLAY_NAME from WF_ITEM_TYPES B, WF_ITEM_TYPES_TL T WHERE B.NAME = T.NAME and T.LANGUAGE = 'US' and b.NAME=wfr.message_type and rownum=1),'All') display_name , to_char(wfr.begin_date,'YYYY-MM-DD') begin_date_p ,  to_char(wfr.end_date,'YYYY-MM-DD') end_date_p \n"+
            "from WF_ROUTING_RULES wfr , per_all_people_f  papf  , fnd_user fnd\n" + 
            "where fnd.EMPLOYEE_ID = PAPF.PERSON_ID\n" + 
            "AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE " +
            "AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'\n" + 
            "and fnd.user_name=wfr.action_argument\n" + 
            "and upper(wfr.role)=upper(mbc_mobile_pkg.get_session_data('USER_NAME') )";
           
       
            try {

                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getDelegationHistory");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement(sqlStemt);
              
               
             rs = statement.executeQuery();

               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
             {
                 s.printStackTrace();
                 response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                 }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
  
    @POST
    @Produces("application/json")
    @Path("/addVacatiobRule")
    public Response addVacatiobRule(
    
        @Context SecurityContext sc  
                                                
                                                
                                                ,@HeaderParam("dateStart") String dateStart
                                                ,@HeaderParam("dateEnd") String dateEnd
                                                ,@HeaderParam("it") String itemType
                                                ,@HeaderParam("newRole") String toUser
                                                ,@HeaderParam("comments") String comments 
                                                ,@HeaderParam("wsid") String wsid
                                                ,@HeaderParam("signature") String signature
                                                     )
  
    {
    if (!sc.isUserInRole("Authenticated"))  
    throw new SecurityException("User is unauthorized.");
    
        if (wsid==null || signature==null )  
        return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
        
        wsid=URLDecoder.decode(wsid);
        signature=URLDecoder.decode(signature);
        
    Response response=null;
    Connection connection = null;
    CallableStatement cs=null;
    String result = null;
    

    String block="DECLARE \n" + 
    "V_CHECK VARCHAR2 (500);\n" + 
    "BEGIN\n" + 
    "V_CHECK :=MBC_MOBILE_PKG.ADD_VACATION_RULE( mbc_mobile_pkg.get_session_data('USER_NAME')  ,TO_DATE (:1 , 'YYYY-MM-DD'),TO_DATE (:2 , 'YYYY-MM-DD'),:3,:4,:5);\n" + 
    ":6 := V_CHECK;\n" + 
    "END;";
    
    try {
        
 if (comments!=null)
     comments=URLDecoder.decode(comments);
        
        connection = Helper.getOracleConnection();
        
        String resultx=Helper.checkSession(connection, wsid, signature , wsid+"addVacatiobRule");
        if (!"OK".equals(resultx))
        throw new SecurityException("Unauthorized Access."); 
        
         cs=  connection.prepareCall(block);
        
        cs.setString(1,dateStart );
        cs.setString(2,dateEnd );
        cs.setString(3,itemType );
        cs.setString(4,toUser );
        cs.setString(5,comments );
    cs.registerOutParameter(6,Types.VARCHAR);


        cs.executeUpdate();
       
    result  =cs.getString(6);
        if (!"success".equalsIgnoreCase(result))
        {
            response = Helper.StringtoReponse(result, "2");
        }
        else
        {
                response = Helper.StringtoReponse("Success" , "1");
            }
        
    
    }
        catch(SecurityException s)
        {
            s.printStackTrace();
            response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            }
    
    catch(Exception e)
    {
        e.printStackTrace();
            response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
        }
    finally {
                try {
                    if ((connection != null) && !(connection.isClosed()))
                        connection.close();
                    if ((cs != null) && !(cs.isClosed()))
                                    cs.close();
                } catch (SQLException sqle) {
                    // TODO: Add catch code
                    sqle.printStackTrace();
                }
            }
    
      return response; 
    }  
    
    @POST
    @Produces("application/json")
    @Path("/updateVacationRule")
    public Response updateVacationRule(
    
        @Context SecurityContext sc  
                                                
                                                ,@HeaderParam("rxd") String ruleId
                                                ,@HeaderParam("dateStart") String dateStart
                                                ,@HeaderParam("dateEnd") String dateEnd
                                                ,@HeaderParam("newRole") String toUser
                                                ,@HeaderParam("comments") String comment
                                                ,@HeaderParam("wsid") String wsid
                                                ,@HeaderParam("signature") String signature
                                                     )
    
    {
    if (!sc.isUserInRole("Authenticated"))  
    throw new SecurityException("User is unauthorized.");
    
        if (wsid==null || signature==null )  
        return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
    
        wsid=URLDecoder.decode(wsid);
        signature=URLDecoder.decode(signature);
        
    Response response=null;
    Connection connection = null;
    CallableStatement cs=null;
    String result = null;
    
       
       
        
    String block=" DECLARE \n" + 
    "V_CHECK VARCHAR2(500);\n" + 
    "BEGIN\n" + 
    "V_CHECK :=MBC_MOBILE_PKG.UPDATE_VACATION_RULE (:1,TO_DATE (:2 , 'YYYY-MM-DD'),TO_DATE (:3 ,'YYYY-MM-DD'),:4,:5); \n" + 
    ":6 := V_CHECK; \n" + 
    "END; ";
    
    try {
        
    if (comment!=null)
     comment=URLDecoder.decode(comment);
        
        connection = Helper.getOracleConnection();
        
        String resultx=Helper.checkSession(connection, wsid, signature , wsid+"updateVacationRule");
        if (!"OK".equals(resultx))
        throw new SecurityException("Unauthorized Access."); 
        
         cs=  connection.prepareCall(block);
        cs.setString(1,ruleId );
        cs.setString(2,dateStart );
        cs.setString(3,dateEnd );
        cs.setString(4,toUser );
        cs.setString(5,comment );
    cs.registerOutParameter(6,Types.VARCHAR);


        cs.executeUpdate();
       
    result  =cs.getString(6);
        if (!"success".equalsIgnoreCase(result))
        {
            response = Helper.StringtoReponse(result, "2");
        }
        else
        {
                response = Helper.StringtoReponse("Success" , "1");
            }
        
    
    }
        catch(SecurityException s)
        {
            s.printStackTrace();
            response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            }
    catch(Exception e)
    {
        e.printStackTrace();
            response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
        }
    finally {
                try {
                    if ((connection != null) && !(connection.isClosed()))
                        connection.close();
                    if ((cs != null) && !(cs.isClosed()))
                                    cs.close();
                } catch (SQLException sqle) {
                    // TODO: Add catch code
                    sqle.printStackTrace();
                }
            }
    
      return response; 
    }  
    
    
    @POST
    @Produces("application/json")
    @Path("/deleteVacationRule")
    public Response deleteVacationRule(
    
        @Context SecurityContext sc  
                                                
                                                ,@HeaderParam("rxd") String ruleId
                                                ,@HeaderParam("wsid") String wsid
                                                ,@HeaderParam("signature") String signature
                                                
                                                     )
    
    {
    if (!sc.isUserInRole("Authenticated"))  
    throw new SecurityException("User is unauthorized.");
        if (wsid==null || signature==null )  
        return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
    
        wsid=URLDecoder.decode(wsid);
        signature=URLDecoder.decode(signature);
        
    Response response=null;
    Connection connection = null;
    CallableStatement cs=null;
    String result = null;
    
       

    String block="DECLARE \n" + 
    "V_CHECK VARCHAR2 (500);\n" + 
    "BEGIN\n" + 
    "V_CHECK :=MBC_MOBILE_PKG.DELETE_VACATION_RULE(:1);\n" + 
    ":2 := V_CHECK; \n" + 
    "END;";
    
    try {
        

        
        connection = Helper.getOracleConnection();
        
        String resultx=Helper.checkSession(connection, wsid, signature , wsid+"deleteVacationRule");
        if (!"OK".equals(resultx))
        throw new SecurityException("Unauthorized Access."); 
        
         cs=  connection.prepareCall(block);
        cs.setString(1,ruleId );

    cs.registerOutParameter(2,Types.VARCHAR);


        cs.executeUpdate();
       
    result  =cs.getString(2);
        if (!"success".equalsIgnoreCase(result))
        {
            response = Helper.StringtoReponse(result, "2");
        }
        else
        {
                response = Helper.StringtoReponse("Success" , "1");
            }
        
    
    }
    catch(Exception e)
    {
        e.printStackTrace();
            response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
        }
    finally {
                try {
                    if ((connection != null) && !(connection.isClosed()))
                        connection.close();
                    if ((cs != null) && !(cs.isClosed()))
                                    cs.close();
                } catch (SQLException sqle) {
                    // TODO: Add catch code
                    sqle.printStackTrace();
                }
            }
    
      return response; 
    }  
    
    
    @POST
    @Produces("application/json")
    @Path("/getDelegationTypes")
    public Response getDelegationTypes(@Context SecurityContext sc, 
                                       @HeaderParam("wsid") String wsid,
                                       @HeaderParam("signature") String signature
                                       )
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT  'ALL'  AS DISPLAY_NAME, '*' AS NAME\n" + 
            "  FROM DUAL\n" + 
            "UNION\n" + 
            "SELECT DISTINCT ItemTypesEO.DISPLAY_NAME, ItemTypesEO.NAME\n" + 
            "  FROM WF_ITEM_TYPES_VL ItemTypesEO, WF_NOTIFICATIONS NotificationsEO\n" + 
            "WHERE     ItemTypesEO.NAME = NotificationsEO.MESSAGE_TYPE\n" + 
            "       AND NotificationsEO.RECIPIENT_ROLE IN (SELECT UserRolesEO.ROLE_NAME\n" + 
            "                                                FROM WF_USER_ROLES UserRolesEO\n" + 
            "                                               WHERE     UserRolesEO.USER_NAME = upper(mbc_mobile_pkg.get_session_data('USER_NAME') ) \n" + 
            "--                                                     AND UserRolesEO.USER_ORIG_SYSTEM = :3\n" + 
            "--                                                     AND UserRolesEO.USER_ORIG_SYSTEM_ID = :4\n" + 
            "                                                     )\n" + 
            "UNION\n" + 
            "SELECT DISTINCT ItemTypesEO.DISPLAY_NAME, ItemTypesEO.NAME\n" + 
            "  FROM WF_ITEM_TYPES_VL ItemTypesEO\n" + 
            "WHERE ItemTypesEO.NAME IN (SELECT lookup_code\n" + 
            "                              FROM fnd_lookups\n" + 
            "                             WHERE     lookup_type = 'WF_RR_ITEM_TYPES'\n" + 
            "                                   AND enabled_flag = 'Y'\n" + 
            "                                   AND TRUNC (\n" + 
            "                                          NVL (start_date_active, SYSDATE),\n" + 
            "                                          'dd') <= TRUNC (SYSDATE, 'dd')\n" + 
            "                                   AND TRUNC (NVL (end_date_active, SYSDATE),\n" + 
            "                                              'dd') >= TRUNC (SYSDATE, 'dd'))\n" + 
            "ORDER BY DISPLAY_NAME";
           
       
            try {

                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getDelegationTypes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
            statement = connection.prepareStatement(sqlStemt);
             

               
             rs = statement.executeQuery();

               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
}

package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.model.am.SchedulerAMImpl;
import com.mbc.oracle.scheduler.model.vo.AsAdminRoleVoImpl;
import com.mbc.oracle.scheduler.model.vo.AsAssignmentEmpsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsAssignmentsVORowImpl;

import com.mbc.oracle.scheduler.model.vo.AsEmail_configurationvoImpl;
import com.mbc.oracle.scheduler.model.vo.AsSendEmailSMSVoImpl;
import com.mbc.oracle.scheduler.view.util.ADFUtils;

import com.mbc.oracle.scheduler.view.util.DateConversion;
import com.mbc.oracle.scheduler.view.util.JSFUtils;

import com.mbc.oracle.scheduler.view.util.SchedulerUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.text.SimpleDateFormat;


import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.event.LaunchPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.server.ViewObjectImpl;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Date;
import oracle.adf.model.BindingContext;
import oracle.binding.BindingContainer;
import oracle.adf.model.AttributeBinding;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adfinternal.view.faces.model.binding.FacesCtrlLOVBinding;

import oracle.jbo.Key;
import oracle.jbo.RowSetIterator;

public class AssignmentsBean {
    private RichPopup assignPop;

    public AssignmentsBean() {
    }

    public void onClicCurrentAssignBtn(ActionEvent actionEvent) {
        // Add event code here...
        Row currRow = ADFUtils.findIterator("AsAssignmentEmpsVOIterator").getCurrentRow();
        Number personId = (Number) currRow.getAttribute("PersonId");
        Date empStartDate = (Date) currRow.getAttribute("EmpStartDate");
        Date empEndDate = (Date) currRow.getAttribute("EmpEndDate");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");


        ViewObjectImpl empSchedulesCalVO =
            (ViewObjectImpl) ADFUtils.findIterator("EmpSchedulesCalVOIterator").getViewObject();
        empSchedulesCalVO.setNamedWhereClauseParam("PersonIdPar", personId);
        empSchedulesCalVO.setWhereClause( " SCHEDULAR_START_DATE between to_date('" +
                                         sdf.format(empStartDate.dateValue()) + "','dd-mm-rrrr') and to_date('" +
                                         sdf.format(empEndDate.dateValue()) + "','dd-mm-rrrr')");
        empSchedulesCalVO.executeQuery();
        empSchedulesCalVO.setWhereClause(null);

        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getAssignPop().show(hints);
    }

    public void setAssignPop(RichPopup assignPop) {
        this.assignPop = assignPop;
    }

    public RichPopup getAssignPop() {
        return assignPop;
    }

    public void onClickApplyAssignmentsBtn(ActionEvent actionEvent) {
        // Add event code here...
        ViewObject empSchedulesCalVO = ADFUtils.findIterator("EmpSchedulesCalVOIterator").getViewObject();
        ViewObject asSchedulerTransHdrsVO = ADFUtils.findIterator("AsSchedulerTransHdrsVOIterator").getViewObject();

        Row asAssignmentsRow = ADFUtils.findIterator("AsAssignmentsVOIterator").getCurrentRow();
        Date assignmentStartDate = (Date) asAssignmentsRow.getAttribute("AssignmentStartDate");
        Date assignmentEndDate = (Date) asAssignmentsRow.getAttribute("AssignmentEndDate");
        for (Row row : empSchedulesCalVO.getAllRowsInRange()) {
            String transType = (String) row.getAttribute("TransType");
            if (transType != null && !"".equals(transType)) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Row asSchedulerTransHdrsRow = asSchedulerTransHdrsVO.createRow();
                asSchedulerTransHdrsRow.setAttribute("SchedulerStaffId", row.getAttribute("SchedulerStaffId"));
                asSchedulerTransHdrsRow.setAttribute("RequesterId",
                                                     (Number) JSFUtils.resolveExpression("#{sessionScope.userHelper.personId}"));
                asSchedulerTransHdrsRow.setAttribute("TransType", transType);
                asSchedulerTransHdrsRow.setAttribute("ChangeReason", "ASSIGNMENT");
                asSchedulerTransHdrsRow.setAttribute("EffectiveStartDate", row.getAttribute("SchedularStartDate"));
                asSchedulerTransHdrsRow.setAttribute("EffectiveEndDate", row.getAttribute("SchedularEndDate"));
                String notes =
                    ADFUtils.getBoundAttributeValue("AssignmentName") + " from " +
                    sdf.format(DateConversion.getUtilDateFromJboDate(assignmentStartDate)) + " to " +
                    sdf.format(DateConversion.getUtilDateFromJboDate(assignmentEndDate));
                asSchedulerTransHdrsRow.setAttribute("Notes", notes);
                if ("REPLACE".equals(transType)) {
                    asSchedulerTransHdrsRow.setAttribute("SuggestedPersonId", row.getAttribute("SuggestedPersonId"));
                } else if ("CHANGE".equals(transType)) {
                    asSchedulerTransHdrsRow.setAttribute("StartTimeHours", row.getAttribute("ChangeStartTimeHours"));
                    asSchedulerTransHdrsRow.setAttribute("StartTimeMinutes",
                                                         row.getAttribute("ChangeStartTimeMinutes"));
                    asSchedulerTransHdrsRow.setAttribute("EndTimeHours", row.getAttribute("ChangeEndTimeHours"));
                    asSchedulerTransHdrsRow.setAttribute("EndTimeMinutes", row.getAttribute("ChangeEndTimeMinutes"));
                } else if ("CANCEL".equals(transType)) {

                }
                asSchedulerTransHdrsVO.insertRow(asSchedulerTransHdrsRow);
                ADFUtils.findOperation("Commit").execute();
                Number schedulerTransHdrId = (Number) asSchedulerTransHdrsRow.getAttribute("SchedulerTransHdrId");
                OperationBinding oper = ADFUtils.findOperation("submitSchdlrTrans");
                oper.getParamsMap().put("schedulerTransHdrId", schedulerTransHdrId);
                oper.execute();
            }
        }
        SchedulerUtil.showPageMessage("Approval", "Changes have been submitted for approval.",
                                      FacesMessage.SEVERITY_INFO);

        this.getAssignPop().hide();
    }

    public void onClickCancelPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        this.getAssignPop().hide();
    }

    public void onChangeTransTypeChoiceList(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        UIComponent comp = valueChangeEvent.getComponent();
        comp.processUpdates(FacesContext.getCurrentInstance());
        FacesContext.getCurrentInstance().renderResponse();

        String transType = (String) ADFUtils.getBoundAttributeValue("TransType");
        /*if ("CHANGE".equals(transType)) {
            ADFUtils.setBoundAttributeValue("ChangeStartTimeHours", ADFUtils.getBoundAttributeValue("StartTimeHours"));
            ADFUtils.setBoundAttributeValue("ChangeStartTimeMinutes",
                                            ADFUtils.getBoundAttributeValue("StartTimeMinutes"));
            ADFUtils.setBoundAttributeValue("ChangeEndTimeHours", ADFUtils.getBoundAttributeValue("EndTimeHours"));
            ADFUtils.setBoundAttributeValue("ChangeEndTimeMinutes", ADFUtils.getBoundAttributeValue("EndTimeMinutes"));
        }*/
    }
    // Added By Heba 24-7-2017//
    //for filterization of list of value of replaced by in assignment 
//    public  void onLovLaunch(LaunchPopupEvent launchPopupEvent) {
//     
//     BindingContext bctx = BindingContext.getCurrent();
//     BindingContainer bindings = bctx.getCurrentBindingsEntry();
//     EmpSchedulesCalVOImpl vo=  (EmpSchedulesCalVOImpl)ADFUtils.findIterator("EmpSchedulesCalVOIterator").getViewObject();
//     EmpSchedulesCalVORowImpl r=(EmpSchedulesCalVORowImpl)vo.getCurrentRow();
//     Date StartDate=   r.getSchedularStartDate();
//     SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//     Row currwk = ADFUtils.findIterator("AsAssignmentEmpsVOIterator").getCurrentRow();
//     String workgroupid=currwk.getAttribute("Workgroupid").toString();  
//      FacesCtrlLOVBinding lov = (FacesCtrlLOVBinding)bindings.get("SuggestedEmployeeName");
//      lov.getListIterBinding().getViewObject().setWhereClause(null);
//     lov.getListIterBinding().getViewObject().setWhereClauseParams(null);
//        String whereclause="";
//        String workgroupclause="";
//        String schedulerclause="";
//        String assignment_condition="";
//      if(Integer.parseInt(workgroupid)!=0)
//      {
//              workgroupclause="WORK_GROUP_ID=".concat(workgroupid);
//              schedulerclause="and PERSON_ID not in (select person_id from as_scheduler_staff where SCHEDULAR_START_DATE=to_date('".concat(sdf.format(StartDate.dateValue())).concat("','dd-MM-yyyy'))");
//              assignment_condition= "and person_id not in (select person_id from AS_ASSIGNMENT_EMPS where to_date('".concat(sdf.format(StartDate.dateValue())).concat("','dd-mm-yyyy') between emp_start_date and emp_end_date)");
//              whereclause=workgroupclause+schedulerclause+ assignment_condition ;
//
//          }
//      
//      else 
//      {
//              schedulerclause="PERSON_ID not in (select person_id from as_scheduler_staff where SCHEDULAR_START_DATE=to_date('".concat(sdf.format(StartDate.dateValue())).concat("','dd-MM-yyyy'))");
//              assignment_condition= "and person_id not in (select person_id from AS_ASSIGNMENT_EMPS where to_date('".concat(sdf.format(StartDate.dateValue())).concat("','dd-mm-yyyy') between emp_start_date and emp_end_date)");
//              whereclause=schedulerclause+ assignment_condition ;
//          }
//
//    lov.getListIterBinding().getViewObject().setWhereClause(whereclause);
//         //.setNamedWhereClauseParam("pss_date", sdf.format(new java.util.Date()));
//  
//             
//    }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }


    // added 26-August-2018//
        
            public void deleteAssignmentEmployee_Dialogue_Listener(DialogEvent dialogEvent) {
            // Add event code here...
            
            if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok)
            {
                    deleteEmpAssignment_action();

                }
   
            
        }
    public void onClickDeleteAssignmentpBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number assignmentId = (Number) actionEvent.getComponent().getAttributes().get("AssignmentId");
        DCIteratorBinding iter = ADFUtils.findIterator("AsAssignmentsVOIterator");
        Key key = new Key(new Object[] { assignmentId });
       // JSFUtils.addFacesErrorMessage(assignmentId.toString());
        iter.setCurrentRowWithKey(key.toStringFormat(true));
        
        AsAssignmentsVORowImpl currRow =
            (AsAssignmentsVORowImpl) iter.getCurrentRow();
        
        int wrkgrpLinesCount = currRow.getAsAssignmentEmpsVO().getAllRowsInRange().length;
        if (wrkgrpLinesCount > 0) {
            JSFUtils.addFacesErrorMessage("Please delete Employees(s) inside Assignment first.");
        } else {
            
            deleteAssignment_action();
//            ADFUtils.findOperation("DeleteWgHdr").execute();
//            ADFUtils.findOperation("Commit").execute();
            JSFUtils.addFacesInformationMessage("Assignment has been deleted successfully.");

        }
    }  
            
            
    public String deleteEmpAssignment_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("DeleteAssignmentEmp");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        commit_action();
        return null;
    }
    public String deleteAssignment_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("DeleteAssignment");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        commit_action();
        return null;
    }
    public String commit_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }
    
    //--------------- Send Sms /Email //
    public SchedulerAMImpl getApplicationModule()
    {
        
            FacesContext fctx = FacesContext.getCurrentInstance(); 
            BindingContext bindingContext = BindingContext.getCurrent();
            DCDataControl dc = bindingContext.findDataControl("SchedulerAMDataControl");
            SchedulerAMImpl am = (SchedulerAMImpl)dc.getDataProvider();
          return am;
        }
    public String sendemployeeSms() {
        // Add event code here...
        // First Commit Changes//
         commit_action();
     SchedulerAMImpl am =  this.getApplicationModule();
     // GET CURRENT ASSIGNMENT EMP ROW
     DCIteratorBinding iter = ADFUtils.findIterator("AsAssignmentEmpsVOIterator");
        AsAssignmentEmpsVORowImpl currRow = (AsAssignmentEmpsVORowImpl) iter.getCurrentRow();
           
        // get required parameters to send email 
        String mobilenum=currRow.getPhoneNumber();
        StringBuilder msg=new StringBuilder();
        msg.append("You have been Assigned to Assignment ");
        msg.append((String) ADFUtils.getBoundAttributeValue("AssignmentName"));
        msg.append(" From ") .append(currRow.getEmpStartDate()).append(" To ").append(currRow.getEmpEndDate());
        String smsmessage =msg.toString();
        StringBuilder adminmsg=new StringBuilder();
        adminmsg.append(currRow.getEmployeeName());
        adminmsg.append(" has been Assigned to Assignment ");
        adminmsg.append((String) ADFUtils.getBoundAttributeValue("AssignmentName"));
        adminmsg.append(" From ") .append(currRow.getEmpStartDate()).append(" To ").append(currRow.getEmpEndDate());
         
       String adminsmsmessage =adminmsg.toString();
       Number personid=currRow.getPersonId();
        // first send to assigned Person 
           String rmsg= sendSMSAction(mobilenum,smsmessage);
           insertsmsstatus(personid,mobilenum,rmsg) ;  
        /// second send to admins 
                       
       AsAdminRoleVoImpl vo =  am.getAsAdminRoleVo1();
        RowSetIterator rsIterator = vo.createRowSetIterator(null);  
        rsIterator.reset();  
        while (rsIterator.hasNext()) {  
          Row row = rsIterator.next();  
            if(row.getAttribute("PhoneNumber") !=null)
            {
          rmsg= sendSMSAction(row.getAttribute("PhoneNumber").toString(),adminsmsmessage);
          insertsmsstatus((Number)row.getAttribute("PersonId"),row.getAttribute("PhoneNumber").toString(),rmsg) ; 
            }
        }  
        rsIterator.closeRowSetIterator();  
        // end send to admins
        // third unroled assigned employee to receive email//
           AsEmail_configurationvoImpl convo=  am.getAsEmail_configurationvo1();
                RowSetIterator crsIterator = convo.createRowSetIterator(null);  
                crsIterator.reset();  
                while (crsIterator.hasNext())
                {  
                  Row crow = crsIterator.next();  
                    rmsg= sendSMSAction(crow.getAttribute("ConfigPhone").toString(),adminsmsmessage);
                    insertsmsstatus((Number)crow.getAttribute("PersonId"),crow.getAttribute("ConfigPhone").toString(),rmsg) ; 
                }
                crsIterator.closeRowSetIterator(); 

       
       FacesMessage Message = new FacesMessage("SMS sent Successfully,Please Check SMS-Email monitoring for more details .");   
       Message.setSeverity(FacesMessage.SEVERITY_INFO);   
       FacesContext fc = FacesContext.getCurrentInstance();   
       fc.addMessage(null, Message);
        
        
        
        
        return null;
    }
    public String sendSMSAction(String mobilenum,String messagetext) {
       
             String mobile = mobilenum;
             String senderid = "MEN";
             String message = messagetext;

             URLConnection myURLConnection = null;
             URL myURL = null;
             BufferedReader reader = null;
          String encoded_message = URLEncoder.encode(message);
             //Send SMS API
             String mainUrl = getSmsUrl();
                 //"https://www.smsglobal.com/http-api.php?action=sendsms&user=bk6honho&password=samJfPVC&";
             //Prepare parameter string
             StringBuilder sbPostData = new StringBuilder(mainUrl);
             sbPostData.append("from=" + senderid);
             sbPostData.append("&to=" + mobile);
             sbPostData.append("&text=" + encoded_message);
              mainUrl = sbPostData.toString();
             System.out.println("URL to Send SMS-" + mainUrl);
             String messagestatus="";
             try {
                 //prepare connection
                 myURL = new URL(mainUrl);
                 myURLConnection = myURL.openConnection();
                 myURLConnection.connect();
                 reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
                 //reading response
                 String response;
                 while ((response = reader.readLine()) != null) {
                     //print response
                     messagestatus=response;
                     System.out.println(response);
                 }
                 System.out.println(response);
                 //finally close connection
                 reader.close();


             } catch (IOException e) {
                 e.printStackTrace();
             }
             
             
             

         
             
             return messagestatus ;
       
     }
    public  void insertsmsstatus(Number vpersonid,String vphone,String vstatusmessage)
    {
            OperationBinding oper = ADFUtils.findOperation("insertSmsMessageStatus");
            oper.getParamsMap().put("personid", vpersonid);
            oper.getParamsMap().put("phone", vphone);
            oper.getParamsMap().put("statusmessage", vstatusmessage);
            oper.execute();
            String retValue = (String) oper.getResult();
        
        
        
        }
    public String getSmsUrl()
    {
            OperationBinding oper = ADFUtils.findOperation("getSmsUrl");
          
            oper.execute();
            String retValue = (String) oper.getResult();
            return retValue;
        
        }
    
    
    public  void insertemail(String vemail,String vemployeename,String vmessage)
    {
            OperationBinding oper = ADFUtils.findOperation("sendemail");
            oper.getParamsMap().put("toemail", vemail);
            oper.getParamsMap().put("employeename", vemployeename);
            oper.getParamsMap().put("emailmessage", vmessage);
            oper.execute();
            String retValue = (String) oper.getResult();
        
        
        
        }


 
    public String sendemail() {
        // Add event code here...

                 commit_action();
     SchedulerAMImpl am =  this.getApplicationModule();
     // GET CURRENT ASSIGNMENT EMP ROW
       DCIteratorBinding iter = ADFUtils.findIterator("AsAssignmentEmpsVOIterator");
        AsAssignmentEmpsVORowImpl currRow = (AsAssignmentEmpsVORowImpl) iter.getCurrentRow();
           
        // get required parameters to send email 
        String mobilenum=currRow.getPhoneNumber();
        StringBuilder msg=new StringBuilder();
        msg.append("You have been Assigned to Assignment ");
        msg.append((String) ADFUtils.getBoundAttributeValue("AssignmentName"));
        msg.append(" From ") .append(currRow.getEmpStartDate()).append(" To ").append(currRow.getEmpEndDate());
        String emailmessage =msg.toString();
        StringBuilder adminmsg=new StringBuilder();
        adminmsg.append(currRow.getEmployeeName());
        adminmsg.append(" has been Assigned to Assignment ");
        adminmsg.append((String) ADFUtils.getBoundAttributeValue("AssignmentName"));
        adminmsg.append(" From ") .append(currRow.getEmpStartDate()).append(" To ").append(currRow.getEmpEndDate());
         
        // first send to assigned Person 
        insertemail(currRow.getEmailAddress(),currRow.getEmployeeName(),emailmessage);
    // Second Send to Admins -Editors-Org Admins
    AsAdminRoleVoImpl vo =  am.getAsAdminRoleVo1();
     RowSetIterator rsIterator = vo.createRowSetIterator(null);  
     rsIterator.reset();  
     while (rsIterator.hasNext())
     {  
       Row row = rsIterator.next();  
         insertemail(row.getAttribute("EmailAddress").toString(),row.getAttribute("EmployeeName").toString(),adminmsg.toString());
  }  
     rsIterator.closeRowSetIterator();  
     // end send to admins
// third unroled assigned employee to receive email//
   AsEmail_configurationvoImpl convo=  am.getAsEmail_configurationvo1();
        RowSetIterator crsIterator = convo.createRowSetIterator(null);  
        crsIterator.reset();  
        while (crsIterator.hasNext())
        {  
          Row crow = crsIterator.next();  
            insertemail(crow.getAttribute("ConfigEmail").toString(),crow.getAttribute("ConfigValue").toString(),adminmsg.toString());
        }
        crsIterator.closeRowSetIterator(); 
 
       FacesMessage Message = new FacesMessage("Email sent Successfully,Please Check SMS-Email monitoring for more details .");   
       Message.setSeverity(FacesMessage.SEVERITY_INFO);   
       FacesContext fc = FacesContext.getCurrentInstance();   
       fc.addMessage(null, Message);
        
        
        return null;
    }
    
    
    
    
    
}

package com.mbc.oracle.scheduler.view.util;

import java.util.Calendar;

public class SchedulerDates {
    public SchedulerDates() {
        super();
    }
    public  java.util.Date addDays(java.util.Date date, int days)
      {
          Calendar cal = Calendar.getInstance();
          cal.setTime(date);
          cal.add(Calendar.DATE, days); //minus number would decrement the days
          return cal.getTime();
      }
}

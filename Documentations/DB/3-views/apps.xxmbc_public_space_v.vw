

/* Formatted on 10/Dec/2018 2:43:15 PM (QP5 v5.215.12089.38647) */
CREATE OR REPLACE FORCE VIEW APPS.XXMBC_PUBLIC_SPACE_V
(
   SCHEDULER_STAFF_ID,
   SHIFT_ID,
   WORK_GROUP_ID,
   FK_ORGANIZATION_ID,
   SCHEDULER_ID,
   PERSON_ID,
   FK_REPLACED_BY_ID,
   SCHEDULAR_START_DATE,
   SCHEDULAR_END_DATE,
   SHIFT,
   BULLETINS,
   SCHEDULERNOTES
)
AS
   SELECT scheduler_staff_id,
          AsSchedulerStaffEO.shift_id,
          AsSchedulerStaffEO.WORK_GROUP_ID,
          schdlr.fk_organization_id,
          AsSchedulerStaffEO.SCHEDULER_ID,
          AsSchedulerStaffEO.person_id,
          AsSchedulerStaffEO.fk_replaced_by_id,
          AsSchedulerStaffEO.SCHEDULAR_START_DATE,
          AsSchedulerStaffEO.SCHEDULAR_END_DATE,
          --   papf.full_name, REPLACEMENT.FULL_NAME ReplacedBy,
          --  papf.person_id,REPLACEMENT.person_id,
          ASS.SHIFT_NAME shift,
          as_schdlr_pkg.SCHEDULER_STAFF_BULLETIN (scheduler_staff_id)
             bulletins,
          DECODE (NVL (AsSchedulerStaffEO.NOTES, 'N'),
                  'N', '',
                  AsSchedulerStaffEO.NOTES)
             Schedulernotes
     FROM AS_SCHEDULER_STAFF AsSchedulerStaffEO,
          --   PER_ALL_PEOPLE_F PAPF,
          --    PER_ALL_PEOPLE_F REPLACEMENT,
          AS_SCHEDULERs schdlr,
          AS_SHIFTS ASS,
          AS_WORK_GROUPS_HDR AWGH
    --   as_user_roles us,
    --    as_user_roles us_replacement

    --    AsSchedulerStaffEO.PERSON_ID = PAPF.PERSON_ID(+)
    --     AND AsSchedulerStaffEO.PERSON_ID = us.PERSON_Id(+)
    --   AND AsSchedulerStaffEO.FK_REPLACED_BY_ID = REPLACEMENT.PERSON_ID(+)

    WHERE     schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
          --          AND AsSchedulerStaffEO.FK_REPLACED_BY_ID =
          --                 us_replacement.PERSON_Id(+)
          --          AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
          --                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1)
          --          AND SYSDATE BETWEEN NVL (REPLACEMENT.EFFECTIVE_START_DATE,
          --                                   SYSDATE - 1)
          --                          AND NVL (REPLACEMENT.EFFECTIVE_END_DATE,
          --                                   SYSDATE + 1)
          AND AsSchedulerStaffEO.SCHEDULER_ID = schdlr.SCHEDULER_ID
          AND AsSchedulerStaffEO.SHIFT_ID = ASS.SHIFT_ID(+)
          AND AsSchedulerStaffEO.WORK_GROUP_ID = AWGH.WORK_GROUP_ID(+);

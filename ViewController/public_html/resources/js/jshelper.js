

var globalFieldCopy = null;
var globalLastVisitedField = null;
function nocontextmenu(event) {
event.cancel();
} 


function openPopup(popupId,popupId2,popupId3){
  return function(evt){
    evt.cancel();    
   txtField = evt.getSource();
     globalLastVisitedField = txtField;
    var clientId = txtField.getClientId();

   // add menu 
    var popup = AdfPage.PAGE.findComponentByAbsoluteId(popupId);
   
     var hints = {align:"end_before", alignId:clientId}; 
 
    popup.show(hints);  
 

  }
}

function pasteFromMenu(serverListenerId, serverListenerOwnerComponentId){
  return function(evt){
   globalFieldCopy = globalLastVisitedField;
  
    if(globalFieldCopy == null){
      //no value copied
      alert("No value copied. Please copy value first");
      evt.cancel();
    }
    else{
      //handle paste
   
      copyValueToSelectedRows(serverListenerId, serverListenerOwnerComponentId);
   
      evt.cancel();
    }
  }
}

function copyValueToSelectedRows(serverListenerId, serverListenerOwnerComponentId, mtype) {   
    
    var txtField = globalFieldCopy; 
      
   var column = txtField.getProperty('colname'); 
    
  var rowKeyIndx   = txtField.getProperty('rwKeyIndx');
   
   //var submittedValue = txtField.getSubmittedValue();
   
    var serverListenerHolder = AdfPage.PAGE.findComponentByAbsoluteId(serverListenerOwnerComponentId);
        AdfCustomEvent.queue(
              serverListenerHolder,
              serverListenerId,
             {column:column, rowKeyIndex:rowKeyIndx},
              true);
           
             globalFieldCopy = null; 
}




package com.mbc.oracle.scheduler.model.vo;

import com.mbc.oracle.scheduler.model.eo.AsShiftsEOImpl;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Mar 08 10:21:58 GST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AsShiftsVORowImpl extends ViewRowImpl {


    public static final int ENTITY_ASSHIFTSEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ShiftId,
        ShiftName,
        EnabledFlag,
        StartTimeHours,
        StartTimeMinutes,
        Remarks,
        CreationDate,
        CreatedBy,
        LastUpdateDate,
        LastUpdatedBy,
        LastUpdateLogin,
        Channel,
        EndTimeHours,
        EndTimeMinutes,
        ShiftType,
        ShiftDisplayName,
        ImageLookupCode,
        Sequence,
        ImageName,
        Status,
        ImagePath,
        FullnameShift,
        EmployeesCount,
        AsShiftWorkGroupsVO,
        ChannelsVOAccessor,
        ShiftTypeVOAccessor,
        AsShiftCodevo1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int SHIFTID = AttributesEnum.ShiftId.index();
    public static final int SHIFTNAME = AttributesEnum.ShiftName.index();
    public static final int ENABLEDFLAG = AttributesEnum.EnabledFlag.index();
    public static final int STARTTIMEHOURS = AttributesEnum.StartTimeHours.index();
    public static final int STARTTIMEMINUTES = AttributesEnum.StartTimeMinutes.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATELOGIN = AttributesEnum.LastUpdateLogin.index();
    public static final int CHANNEL = AttributesEnum.Channel.index();
    public static final int ENDTIMEHOURS = AttributesEnum.EndTimeHours.index();
    public static final int ENDTIMEMINUTES = AttributesEnum.EndTimeMinutes.index();
    public static final int SHIFTTYPE = AttributesEnum.ShiftType.index();
    public static final int SHIFTDISPLAYNAME = AttributesEnum.ShiftDisplayName.index();
    public static final int IMAGELOOKUPCODE = AttributesEnum.ImageLookupCode.index();
    public static final int SEQUENCE = AttributesEnum.Sequence.index();
    public static final int IMAGENAME = AttributesEnum.ImageName.index();
    public static final int STATUS = AttributesEnum.Status.index();
    public static final int IMAGEPATH = AttributesEnum.ImagePath.index();
    public static final int FULLNAMESHIFT = AttributesEnum.FullnameShift.index();
    public static final int EMPLOYEESCOUNT = AttributesEnum.EmployeesCount.index();
    public static final int ASSHIFTWORKGROUPSVO = AttributesEnum.AsShiftWorkGroupsVO.index();
    public static final int CHANNELSVOACCESSOR = AttributesEnum.ChannelsVOAccessor.index();
    public static final int SHIFTTYPEVOACCESSOR = AttributesEnum.ShiftTypeVOAccessor.index();
    public static final int ASSHIFTCODEVO1 = AttributesEnum.AsShiftCodevo1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AsShiftsVORowImpl() {
    }

    /**
     * Gets AsShiftsEO entity object.
     * @return the AsShiftsEO
     */
    public AsShiftsEOImpl getAsShiftsEO() {
        return (AsShiftsEOImpl) getEntity(ENTITY_ASSHIFTSEO);
    }

    /**
     * Gets the attribute value for SHIFT_ID using the alias name ShiftId.
     * @return the SHIFT_ID
     */
    public Number getShiftId() {
        return (Number) getAttributeInternal(SHIFTID);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIFT_ID using the alias name ShiftId.
     * @param value value to set the SHIFT_ID
     */
    public void setShiftId(Number value) {
        setAttributeInternal(SHIFTID, value);
    }

    /**
     * Gets the attribute value for SHIFT_NAME using the alias name ShiftName.
     * @return the SHIFT_NAME
     */
    public String getShiftName() {
        return (String) getAttributeInternal(SHIFTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIFT_NAME using the alias name ShiftName.
     * @param value value to set the SHIFT_NAME
     */
    public void setShiftName(String value) {
        setAttributeInternal(SHIFTNAME, value);
    }

    /**
     * Gets the attribute value for ENABLED_FLAG using the alias name EnabledFlag.
     * @return the ENABLED_FLAG
     */
    public String getEnabledFlag() {
        return (String) getAttributeInternal(ENABLEDFLAG);
    }

    /**
     * Sets <code>value</code> as attribute value for ENABLED_FLAG using the alias name EnabledFlag.
     * @param value value to set the ENABLED_FLAG
     */
    public void setEnabledFlag(String value) {
        setAttributeInternal(ENABLEDFLAG, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EmployeesCount.
     * @return the EmployeesCount
     */
    public Number getEmployeesCount() {
        return (Number) getAttributeInternal(EMPLOYEESCOUNT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EmployeesCount.
     * @param value value to set the  EmployeesCount
     */
    public void setEmployeesCount(Number value) {
        setAttributeInternal(EMPLOYEESCOUNT, value);
    }

    /**
     * Gets the attribute value for SEQUENCE using the alias name Sequence.
     * @return the SEQUENCE
     */
    public Number getSequence() {
        return (Number) getAttributeInternal(SEQUENCE);
    }

    /**
     * Sets <code>value</code> as attribute value for SEQUENCE using the alias name Sequence.
     * @param value value to set the SEQUENCE
     */
    public void setSequence(Number value) {
        setAttributeInternal(SEQUENCE, value);
    }

    /**
     * Gets the attribute value for STATUS using the alias name Status.
     * @return the STATUS
     */
    public String getStatus() {
        return (String) getAttributeInternal(STATUS);
    }

    /**
     * Sets <code>value</code> as attribute value for STATUS using the alias name Status.
     * @param value value to set the STATUS
     */
    public void setStatus(String value) {
        setAttributeInternal(STATUS, value);
    }

    /**
     * Gets the attribute value for FULLNAME_SHIFT using the alias name FullnameShift.
     * @return the FULLNAME_SHIFT
     */
    public String getFullnameShift() {
        return (String) getAttributeInternal(FULLNAMESHIFT);
    }

    /**
     * Sets <code>value</code> as attribute value for FULLNAME_SHIFT using the alias name FullnameShift.
     * @param value value to set the FULLNAME_SHIFT
     */
    public void setFullnameShift(String value) {
        setAttributeInternal(FULLNAMESHIFT, value);
    }

    /**
     * Gets the attribute value for SHIFT_DISPLAY_NAME using the alias name ShiftDisplayName.
     * @return the SHIFT_DISPLAY_NAME
     */
    public String getShiftDisplayName() {
        return (String) getAttributeInternal(SHIFTDISPLAYNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIFT_DISPLAY_NAME using the alias name ShiftDisplayName.
     * @param value value to set the SHIFT_DISPLAY_NAME
     */
    public void setShiftDisplayName(String value) {
        setAttributeInternal(SHIFTDISPLAYNAME, value);
    }

    /**
     * Gets the attribute value for START_TIME_HOURS using the alias name StartTimeHours.
     * @return the START_TIME_HOURS
     */
    public Number getStartTimeHours() {
        return (Number) getAttributeInternal(STARTTIMEHOURS);
    }

    /**
     * Sets <code>value</code> as attribute value for START_TIME_HOURS using the alias name StartTimeHours.
     * @param value value to set the START_TIME_HOURS
     */
    public void setStartTimeHours(Number value) {
        setAttributeInternal(STARTTIMEHOURS, value);
    }

    /**
     * Gets the attribute value for START_TIME_MINUTES using the alias name StartTimeMinutes.
     * @return the START_TIME_MINUTES
     */
    public Number getStartTimeMinutes() {
        return (Number) getAttributeInternal(STARTTIMEMINUTES);
    }

    /**
     * Sets <code>value</code> as attribute value for START_TIME_MINUTES using the alias name StartTimeMinutes.
     * @param value value to set the START_TIME_MINUTES
     */
    public void setStartTimeMinutes(Number value) {
        setAttributeInternal(STARTTIMEMINUTES, value);
    }

    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Date getCreationDate() {
        return (Date) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public Number getCreatedBy() {
        return (Number) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(Number value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Date getLastUpdateDate() {
        return (Date) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public Number getLastUpdatedBy() {
        return (Number) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(Number value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_LOGIN using the alias name LastUpdateLogin.
     * @return the LAST_UPDATE_LOGIN
     */
    public Number getLastUpdateLogin() {
        return (Number) getAttributeInternal(LASTUPDATELOGIN);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_LOGIN using the alias name LastUpdateLogin.
     * @param value value to set the LAST_UPDATE_LOGIN
     */
    public void setLastUpdateLogin(Number value) {
        setAttributeInternal(LASTUPDATELOGIN, value);
    }

    /**
     * Gets the attribute value for CHANNEL using the alias name Channel.
     * @return the CHANNEL
     */
    public String getChannel() {
        return (String) getAttributeInternal(CHANNEL);
    }

    /**
     * Sets <code>value</code> as attribute value for CHANNEL using the alias name Channel.
     * @param value value to set the CHANNEL
     */
    public void setChannel(String value) {
        setAttributeInternal(CHANNEL, value);
    }

    /**
     * Gets the attribute value for END_TIME_HOURS using the alias name EndTimeHours.
     * @return the END_TIME_HOURS
     */
    public Number getEndTimeHours() {
        return (Number) getAttributeInternal(ENDTIMEHOURS);
    }

    /**
     * Sets <code>value</code> as attribute value for END_TIME_HOURS using the alias name EndTimeHours.
     * @param value value to set the END_TIME_HOURS
     */
    public void setEndTimeHours(Number value) {
        setAttributeInternal(ENDTIMEHOURS, value);
    }

    /**
     * Gets the attribute value for END_TIME_MINUTES using the alias name EndTimeMinutes.
     * @return the END_TIME_MINUTES
     */
    public Number getEndTimeMinutes() {
        return (Number) getAttributeInternal(ENDTIMEMINUTES);
    }

    /**
     * Sets <code>value</code> as attribute value for END_TIME_MINUTES using the alias name EndTimeMinutes.
     * @param value value to set the END_TIME_MINUTES
     */
    public void setEndTimeMinutes(Number value) {
        setAttributeInternal(ENDTIMEMINUTES, value);
    }

    /**
     * Gets the attribute value for SHIFT_TYPE using the alias name ShiftType.
     * @return the SHIFT_TYPE
     */
    public String getShiftType() {
        return (String) getAttributeInternal(SHIFTTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIFT_TYPE using the alias name ShiftType.
     * @param value value to set the SHIFT_TYPE
     */
    public void setShiftType(String value) {
        setAttributeInternal(SHIFTTYPE, value);
    }


    /**
     * Gets the attribute value for IMAGE_LOOKUP_CODE using the alias name ImageLookupCode.
     * @return the IMAGE_LOOKUP_CODE
     */
    public String getImageLookupCode() {
        return (String) getAttributeInternal(IMAGELOOKUPCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for IMAGE_LOOKUP_CODE using the alias name ImageLookupCode.
     * @param value value to set the IMAGE_LOOKUP_CODE
     */
    public void setImageLookupCode(String value) {
        setAttributeInternal(IMAGELOOKUPCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ImageName.
     * @return the ImageName
     */
    public String getImageName() {
        return (String) getAttributeInternal(IMAGENAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ImageName.
     * @param value value to set the  ImageName
     */
    public void setImageName(String value) {
        setAttributeInternal(IMAGENAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ImagePath.
     * @return the ImagePath
     */
    public String getImagePath() {
        return (String) getAttributeInternal(IMAGEPATH);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ImagePath.
     * @param value value to set the  ImagePath
     */
    public void setImagePath(String value) {
        setAttributeInternal(IMAGEPATH, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link AsShiftWorkGroupsVO.
     */
    public RowIterator getAsShiftWorkGroupsVO() {
        return (RowIterator) getAttributeInternal(ASSHIFTWORKGROUPSVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ChannelsVOAccessor.
     */
    public RowSet getChannelsVOAccessor() {
        return (RowSet) getAttributeInternal(CHANNELSVOACCESSOR);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ShiftTypeVOAccessor.
     */
    public RowSet getShiftTypeVOAccessor() {
        return (RowSet) getAttributeInternal(SHIFTTYPEVOACCESSOR);
    }

    /**
     * Gets the view accessor <code>RowSet</code> AsShiftCodevo1.
     */
    public RowSet getAsShiftCodevo1() {
        return (RowSet) getAttributeInternal(ASSHIFTCODEVO1);
    }

}


package ws;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;


@Path("sshr/leavesHistory")
public class LeavesWS {
    public LeavesWS() {
        super();
    }


    
    @POST
    @Produces("application/json")
    @Path("/getApprovedLeaves")
    public Response getApprovedLeaves(@Context SecurityContext sc 
                                      , @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature)
    {
            
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
           
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            try {
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getApprovedLeaves");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                
                Helper.logActivity(connection, "Open Leave History", "Approved Leaves");
            statement = connection.prepareStatement("select  a.start_date,a.end_date, a.leave_desc  leave_desc ,\n" + 
            "            a.absence_category,a.Return_Date,a.Comments, a.Compassionate_Reason,a.Level_Of_Sickness,a.absence_no_days,a.Half_Day,a.replaced_person_name \n" + 
            " from mob_sshr_leave_history a" +
                " where  a.person_id=mbc_mobile_pkg.get_session_data('PERSON_ID') and business_group=mbc_mobile_pkg.get_session_data('BUSINESS_GROUP_ID') ");
                
             rs = statement.executeQuery();
          
               
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                 
                  //  response = Response.status(404).entity("Unable to find record").type("text/plain").build();
                }
               
            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                e.printStackTrace();
                
                
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }



    
    @POST
    @Produces("application/json")
    @Path("/getApprovedLeavesCount")
    public Response getApprovedLeavesCount (@Context SecurityContext sc
                                            ,@HeaderParam("wsid") String wsid,@HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
            
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;

            ResultSet rs=null;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature,wsid+"getApprovedLeavesCount");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement("select  count(*) approvedCount   from mob_sshr_leave_history a "+
                       " where  a.person_id=mbc_mobile_pkg.get_session_data('PERSON_ID')  and business_group=mbc_mobile_pkg.get_session_data('BUSINESS_GROUP_ID') ");
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                         connection.close();
                           if ((statement != null) && !(statement.isClosed()))
                                     statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                        rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }



    
    @POST
    @Produces("application/json")
    @Path("/getPendingLeaves")
    public Response getPendingLeaves(@Context SecurityContext sc
                                     , @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature)
    {
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
        
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getPendingLeaves");
                 if (!"OK".equals(result))
                 throw new SecurityException("Unauthorized Access.");
                
                
            statement = connection.prepareStatement("select  a.start_date,a.end_date,a.leave_desc,\n" + 
            "            a.absence_category,a.Return_Date,a.Comments, a.Compassionate_Reason,a.Level_Of_Sickness,a.absence_no_days,a.Half_Day,a.replaced_person_name , ITEM_KEY, ITEM_TYPE \n" + 
            " from mob_sshr_pending_leave_history a" +
                " where  a.person_id= mbc_mobile_pkg.get_session_data('PERSON_ID')  ");
                
              
             rs = statement.executeQuery();
          
              
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
             {
                 s.printStackTrace();
                 response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                 }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          


                            if ((connection != null) && !(connection.isClosed()))
                                      connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                  statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                     rs.close();
                                                     
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    

    
    @POST
    @Produces("application/json")
    @Path("/getPendingLeavesCount")
    public Response getPendingLeavesCount(@Context SecurityContext sc
                                    , @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature
                                          )
    {
        
            if (!sc.isUserInRole("Authenticated"))  
                throw new SecurityException("User is unauthorized.");
           
            if (wsid==null || signature==null )  
             throw new SecurityException("Unauthorized Access.");

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getPendingLeavesCount");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement("select  count(*) pending_count \n" + 
            " from mob_sshr_pending_leave_history a" +
                " where  a.person_id= mbc_mobile_pkg.get_session_data('PERSON_ID')  ");
                
              
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
             {
                 s.printStackTrace();
                 response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                 }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          


                                if ((connection != null) && !(connection.isClosed()))
                                          connection.close();
                                if ((statement != null) && !(statement.isClosed()))
                                      statement.close();
                                if ((rs != null) && !(rs.isClosed()))
                                         rs.close();
                                                                                 
                                
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
     
    

    @POST
    @Produces("application/json")
    @Path("/getToilBalance")
    
    public Response getToilBalance(@Context SecurityContext sc 
                                  , @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature )
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
             throw new SecurityException("Unauthorized Access.");
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStmnt="SELECT nvl(sum(decode(paa.absence_attendance_type_id,65,paa.ABSENCE_DAYS*-1,paa.ABSENCE_DAYS)),0) Available_no,\n" + 
            " 'Available ('|| nvl(sum(decode(paa.absence_attendance_type_id,65,paa.ABSENCE_DAYS*-1,paa.ABSENCE_DAYS)),0)||')' Available_txt,\n" + 
            "       nvl(sum(decode(paa.absence_attendance_type_id,65,paa.ABSENCE_DAYS,0)),0) Taken_no,\n" + 
            "         'Approved ('||nvl(sum(decode(paa.absence_attendance_type_id,65,paa.ABSENCE_DAYS,0)),0)||')' Taken_txt         \n" + 
            "FROM per_people_f papf\n" + 
            ",per_assignments_f paaf\n" + 
            ",hr_organization_units haou\n" + 
            ",per_absence_attendances paa\n" + 
            ",per_absence_attendance_types paat\n" + 
            ",pay_people_groups ppg\n" + 
            ",fnd_user fu\n" + 
            ",fnd_user fus\n" + 
            "WHERE\n" + 
            "papf.person_id = paaf.person_id\n" + 
            "and papf.person_id =mbc_mobile_pkg.get_session_data('PERSON_ID')  \n" + 
            "AND paaf.organization_id = haou.organization_id\n" + 
            "AND papf.business_group_id = FND_PROFILE.VALUE('PER_BUSINESS_GROUP_ID')\n" + 
            "AND trunc(SYSDATE) BETWEEN papf.effective_start_date AND papf.effective_end_date\n" + 
            "AND trunc(SYSDATE) BETWEEN paaf.effective_start_date AND paaf.effective_end_date\n" + 
            "and paaf.people_group_id = ppg.people_group_id \n" + 
            "AND paaf.primary_flag = 'Y'\n" + 
            "and papf.current_employee_flag is not null\n" + 
            "and papf.person_Id = paa.person_id\n" + 
            "and paa.absence_attendance_type_id = paat.absence_attendance_type_id\n" + 
            "and papf.person_id = fu.employee_id(+)\n" + 
            "and paaf.supervisor_id = fus.employee_id(+)\n" + 
            "and paat.absence_attendance_type_id in (65,66) -- ('Extra Time Worked', 'Time Off in Lieu', 'Time Off In Lieu' )\n" + 
            "\n";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getToilBalance");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement(sqlStmnt);
                
              
             rs = statement.executeQuery();
          
               
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
               
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          

                            if ((connection != null) && !(connection.isClosed()))
                                      connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                  statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                     rs.close();
                                                                             
     
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    @POST
    @Produces("application/json")
    @Path("/getAnnualLeavesBalance")
    public Response getAnnualLeavesBalance(@Context SecurityContext sc 
                                , @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
                    throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStm="select sum(nvl(credit,0))+ sum(nvl(debit,0))  available_no   , 'Available ('||(sum(nvl(credit,0))+ sum(nvl(debit,0)))||')' available_txt ,\n" + 
            "abs(sum(nvl(debit,0))) taken_no ,   'Approved ('||abs(sum(nvl(debit,0)))||')'  taken_txt from\n" + 
            "(\n" + 
            "SELECT DISTINCT  \n" + 
            "       e.person_id,  \n" + 
            "       e.employee_number,  \n" + 
            "       e.full_name,  \n" + 
            "       d.supervisor_id,  \n" + 
            "       d.assignment_id,  \n" + 
            "       c.business_group_id,  \n" + 
            "       a.attribute1 || '  ' || a.attribute2 comments ,  \n" + 
            "       a.effective_start_date START_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN e.effective_end_date IS NULL  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          WHEN TO_CHAR (e.effective_end_date, 'RRRR') = '4712'  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          ELSE  \n" + 
            "             e.effective_end_date  \n" + 
            "       END  \n" + 
            "          END_DATE --,round(DECODE(sign(to_number(f.screen_entry_value)), 1,to_number(f.screen_entry_value)),2) credit  \n" + 
            " --,round(DECODE(sign(to_number(f.screen_entry_value)),-1,to_number(f.screen_entry_value)),2) Debit  \n" + 
            "       ,  \n" + 
            "       CASE  \n" + 
            "          WHEN element_name = 'Leave Adjustment'  \n" + 
            "          THEN  \n" + 
            "             ROUND (TO_NUMBER (f.screen_entry_value), 2)  \n" + 
            "          ELSE  \n" + 
            "             0  \n" + 
            "       END  \n" + 
            "          credit,  \n" + 
            "       CASE  \n" + 
            "          WHEN element_name = 'Leave Encashment'  \n" + 
            "          THEN  \n" + 
            "             ROUND (TO_NUMBER (f.screen_entry_value), 2) * -1  \n" + 
            "          ELSE  \n" + 
            "             0  \n" + 
            "       END  \n" + 
            "          Debit,  \n" + 
            "       d.payroll_id,  \n" + 
            "       pap.accrual_plan_id,  \n" + 
            "       e.effective_end_date last_working_date  \n" + 
            "  FROM pay_element_entries_f a,  \n" + 
            "       pay_element_links_f b,  \n" + 
            "       pay_element_types_f c,  \n" + 
            "       per_all_assignments_f d,  \n" + 
            "       per_people_f e,  \n" + 
            "       pay_element_entry_values_f f,  \n" + 
            "       pay_input_values_f i,  \n" + 
            "       per_periods_of_service j,  \n" + 
            "       pay_accrual_plans pap  \n" + 
            " WHERE     a.element_link_id = b.element_link_Id  \n" + 
            "       AND b.element_type_id = c.element_type_id  \n" + 
            "       AND a.assignment_id = d.assignment_Id  \n" + 
            "       AND d.person_Id = e.person_id  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN d.effective_start_date  \n" + 
            "                               AND d.effective_end_date  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN e.effective_start_date  \n" + 
            "                               AND e.effective_end_date  \n" + 
            "       AND a.element_entry_id = f.element_entry_id  \n" + 
            "       AND f.input_value_Id = i.input_value_Id  \n" + 
            "       AND e.person_id = j.person_id  \n" + 
            "       AND element_name IN ('Leave Adjustment', 'Leave Encashment')  \n" + 
            "       AND i.name IN ('Days', 'Days Encashed')  \n" + 
            "       AND TO_CHAR (a.creation_date, 'YYYY') = TO_CHAR (SYSDATE, 'YYYY')  \n" + 
            "       AND (a.EFFECTIVE_START_DATE BETWEEN f.effective_start_date  \n" + 
            "                                       AND f.effective_end_date)  \n" + 
            "       AND current_employee_flag = 'Y'  \n" + 
            "       AND e.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
            "       AND d.business_group_id = pap.business_group_id  \n" + 
            "       AND f.element_entry_id IN  \n" + 
            "              (SELECT f.element_entry_id  \n" + 
            "                 FROM pay_element_entries_f a,  \n" + 
            "                      pay_element_links_f b,  \n" + 
            "                      pay_element_types_f c,  \n" + 
            "                      per_all_assignments_f d,  \n" + 
            "                      per_people_f e,  \n" + 
            "                      pay_element_entry_values_f f,  \n" + 
            "                      pay_input_values_f i,  \n" + 
            "                      pay_people_groups h,  \n" + 
            "                      per_periods_of_service j,  \n" + 
            "                      pay_accrual_plans pap  \n" + 
            "                WHERE     a.element_link_id = b.element_link_Id  \n" + 
            "                      AND b.element_type_id = c.element_type_id  \n" + 
            "                      AND a.assignment_id = d.assignment_Id  \n" + 
            "                      AND d.person_Id = e.person_id  \n" + 
            "                      AND TRUNC (SYSDATE) BETWEEN d.effective_start_date  \n" + 
            "                                              AND d.effective_end_date  \n" + 
            "                      AND TRUNC (SYSDATE) BETWEEN e.effective_start_date  \n" + 
            "                                              AND e.effective_end_date  \n" + 
            "                      AND a.element_entry_id = f.element_entry_id  \n" + 
            "                      AND f.input_value_Id = i.input_value_Id  \n" + 
            "                      AND d.people_group_id = h.people_group_Id  \n" + 
            "                      AND e.person_id = j.person_id  \n" + 
            "                      AND element_name IN  \n" + 
            "                             ('Leave Adjustment', 'Leave Encashment')  \n" + 
            "                      AND i.name = 'Effective Date'  \n" + 
            "                      AND SUBSTR (f.screen_entry_value, 1, 4) =  \n" + 
            "                             TO_CHAR (SYSDATE, 'YYYY')  \n" + 
            "                      --and to_char(a.creation_date,'YYYY') <> to_char(sysdate,'YYYY')  \n" + 
            "                      AND (a.EFFECTIVE_START_DATE BETWEEN f.effective_start_date  \n" + 
            "                                                      AND f.effective_end_date)  \n" + 
            "                      AND current_employee_flag = 'Y'  \n" + 
            "                      AND d.business_group_id = pap.business_group_id)  \n" + 
            "--Begin for Saudi  \n" + 
            "UNION ALL  \n" + 
            "SELECT DISTINCT  \n" + 
            "       e.person_id,  \n" + 
            "       e.employee_number,  \n" + 
            "       e.full_name,  \n" + 
            "       d.supervisor_id,  \n" + 
            "       d.assignment_id,  \n" + 
            "       c.business_group_id,  \n" + 
            "       element_name comments,  \n" + 
            "       a.effective_start_date START_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN e.effective_end_date IS NULL  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          WHEN TO_CHAR (e.effective_end_date, 'RRRR') = '4712'  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          ELSE  \n" + 
            "             e.effective_end_date  \n" + 
            "       END  \n" + 
            "          END_DATE --,round(DECODE(sign(to_number(f.screen_entry_value)), 1,to_number(f.screen_entry_value)),2) credit  \n" + 
            " --,round(DECODE(sign(to_number(f.screen_entry_value)),-1,to_number(f.screen_entry_value)),2) Debit  \n" + 
            "       ,  \n" + 
            "       CASE  \n" + 
            "          WHEN element_name = 'Annual Accrual Plan Carried Over'  \n" + 
            "               AND SIGN (ROUND (TO_NUMBER (f.screen_entry_value), 2)) = 1  \n" + 
            "          THEN  \n" + 
            "             ROUND (TO_NUMBER (f.screen_entry_value), 2)  \n" + 
            "          ELSE  \n" + 
            "             0  \n" + 
            "       END  \n" + 
            "          credit,  \n" + 
            "       CASE  \n" + 
            "          WHEN element_name = 'Annual Accrual Plan Carried Over'  \n" + 
            "               AND SIGN (ROUND (TO_NUMBER (f.screen_entry_value), 2)) = -1  \n" + 
            "          THEN  \n" + 
            "             ROUND (TO_NUMBER (f.screen_entry_value), 2)  \n" + 
            "          ELSE  \n" + 
            "             0  \n" + 
            "       END  \n" + 
            "          Debit,  \n" + 
            "       d.payroll_id,  \n" + 
            "       pap.accrual_plan_id,  \n" + 
            "       e.effective_end_date last_working_date  \n" + 
            "  FROM pay_element_entries_f a,  \n" + 
            "       pay_element_links_f b,  \n" + 
            "       pay_element_types_f c,  \n" + 
            "       per_all_assignments_f d,  \n" + 
            "       per_people_f e,  \n" + 
            "       pay_element_entry_values_f f,  \n" + 
            "       pay_input_values_f i,  \n" + 
            "       per_periods_of_service j,  \n" + 
            "       pay_accrual_plans pap  \n" + 
            " WHERE     a.element_link_id = b.element_link_Id  \n" + 
            "       AND b.element_type_id = c.element_type_id  \n" + 
            "       AND a.assignment_id = d.assignment_Id  \n" + 
            "       AND d.person_Id = e.person_id  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN d.effective_start_date  \n" + 
            "                               AND d.effective_end_date  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN e.effective_start_date  \n" + 
            "                               AND e.effective_end_date  \n" + 
            "       AND a.element_entry_id = f.element_entry_id  \n" + 
            "       AND f.input_value_Id = i.input_value_Id  \n" + 
            "       AND e.person_id = j.person_id  \n" + 
            "       AND element_name = 'Annual Accrual Plan Carried Over'  \n" + 
            "       AND i.name = 'Plan Days'  \n" + 
            "       --and to_char(a.creation_date,'YYYY') = to_char(sysdate,'YYYY')  \n" + 
            "       AND TO_CHAR (a.EFFECTIVE_START_DATE, 'YYYY') =  \n" + 
            "              TO_CHAR (SYSDATE, 'YYYY')  \n" + 
            "       AND (a.EFFECTIVE_START_DATE BETWEEN f.effective_start_date  \n" + 
            "                                       AND f.effective_end_date)  \n" + 
            "       AND current_employee_flag = 'Y'  \n" + 
            "       AND e.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
            "       AND d.business_group_id = pap.business_group_id  \n" + 
            "--End for Saudi  \n" + 
            "UNION ALL  \n" + 
            "SELECT DISTINCT  \n" + 
            "       papf.person_id,  \n" + 
            "       papf.employee_number,  \n" + 
            "       papf.full_name,  \n" + 
            "       paaf.supervisor_id,  \n" + 
            "       paaf.assignment_id,  \n" + 
            "       papf.business_group_id,  \n" + 
            "       TO_CHAR (SYSDATE, 'YYYY') || ' Entitlement' Description,  \n" + 
            "       CASE  \n" + 
            "          WHEN papf.effective_start_date > TRUNC (SYSDATE, 'RRRR')  \n" + 
            "          THEN  \n" + 
            "             papf.effective_start_date  \n" + 
            "          ELSE  \n" + 
            "             TRUNC (SYSDATE, 'RRRR')  \n" + 
            "       END  \n" + 
            "          START_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN papf.effective_end_date IS NULL  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          WHEN TO_CHAR (papf.effective_end_date, 'RRRR') = '4712'  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          ELSE  \n" + 
            "             papf.effective_end_date  \n" + 
            "       END  \n" + 
            "          END_DATE,  \n" + 
            "       ROUND (xxhr_mbc_reports_pkg.get_leave_accrual (  \n" + 
            "                 paaf.assignment_id,  \n" + 
            "                 LEAST (  \n" + 
            "                    papf.effective_end_date,  \n" + 
            "                    TO_CHAR (ADD_MONTHS (TRUNC (SYSDATE, 'YEAR'), 12) - 1,  \n" + 
            "                             'DD-MON-YYYY')),  \n" + 
            "                 pap.accrual_plan_id,  \n" + 
            "                 papf.business_group_id,  \n" + 
            "                 paaf.payroll_id),  \n" + 
            "              2)  \n" + 
            "          Credit,  \n" + 
            "       NULL debit,  \n" + 
            "       paaf.payroll_id,  \n" + 
            "       pap.accrual_plan_id,  \n" + 
            "       papf.effective_end_date last_working_date  \n" + 
            "  FROM per_people_f papf, per_assignments_f paaf, pay_accrual_plans pap  \n" + 
            " WHERE papf.person_id = paaf.person_id  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN papf.effective_start_date  \n" + 
            "                               AND papf.effective_end_date  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN paaf.effective_start_date  \n" + 
            "                               AND paaf.effective_end_date  \n" + 
            "       AND paaf.primary_flag = 'Y'  \n" + 
            "       AND papf.current_employee_flag IS NOT NULL  \n" + 
            "       AND papf.business_group_id = pap.business_group_Id  \n" + 
            "       AND current_employee_flag = 'Y'  \n" + 
            "       AND papf.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
            "UNION ALL  \n" + 
            "SELECT DISTINCT  \n" + 
            "       papf.person_id,  \n" + 
            "       papf.employee_number,  \n" + 
            "       papf.full_name,  \n" + 
            "       paaf.supervisor_id,  \n" + 
            "       paaf.assignment_id,  \n" + 
            "       papf.business_group_id,  \n" + 
            "       'Annual Leave' Description,  \n" + 
            "       paa.date_start START_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN paa.date_end IS NULL  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          WHEN TO_CHAR (paa.date_end, 'RRRR') = '4712'  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          ELSE  \n" + 
            "             paa.date_end  \n" + 
            "       END  \n" + 
            "          END_DATE,  \n" + 
            "       0 Credit,  \n" + 
            "       -ROUND (paa.ABSENCE_DAYS, 1) debit,  \n" + 
            "       paaf.payroll_id,  \n" + 
            "       pap.accrual_plan_id,  \n" + 
            "       papf.effective_end_date last_working_date  \n" + 
            "  FROM per_people_f papf,  \n" + 
            "       per_assignments_f paaf,  \n" + 
            "       pay_accrual_plans pap,  \n" + 
            "       per_absence_attendances paa,  \n" + 
            "       per_absence_attendance_types paat  \n" + 
            " WHERE papf.person_id = paaf.person_id AND paaf.person_id = PAA.PERSON_ID  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN papf.effective_start_date  \n" + 
            "                               AND papf.effective_end_date  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN paaf.effective_start_date  \n" + 
            "                               AND paaf.effective_end_date  \n" + 
            "       AND paaf.primary_flag = 'Y'  \n" + 
            "       AND papf.current_employee_flag IS NOT NULL  \n" + 
            "       AND papf.business_group_id = pap.business_group_Id  \n" + 
            "       AND paa.absence_attendance_type_id = paat.absence_attendance_type_id  \n" + 
            "       AND paat.name = 'Annual Leave'  \n" + 
            "       AND (paa.date_start BETWEEN TO_DATE (TRUNC (SYSDATE, 'YYYY'))  \n" + 
            "                               AND TO_DATE (  \n" + 
            "                                      ADD_MONTHS (TRUNC (SYSDATE, 'YYYY'),  \n" + 
            "                                                  12)))  \n" + 
            "       AND current_employee_flag = 'Y'  \n" + 
            "       AND papf.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
            "UNION ALL  \n" + 
            "SELECT DISTINCT  \n" + 
            "       papf.person_id,  \n" + 
            "       papf.employee_number,  \n" + 
            "       papf.full_name,  \n" + 
            "       paaf.supervisor_id,  \n" + 
            "       paaf.assignment_id,  \n" + 
            "       papf.business_group_id,  \n" + 
            "       'Emergency Leave' Description,  \n" + 
            "       paa.date_start START_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN paa.date_end IS NULL  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          WHEN TO_CHAR (paa.date_end, 'RRRR') = '4712'  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          ELSE  \n" + 
            "             paa.date_end  \n" + 
            "       END  \n" + 
            "          END_DATE,  \n" + 
            "       0 Credit,  \n" + 
            "       -ROUND (paa.ABSENCE_DAYS, 1) debit,  \n" + 
            "       paaf.payroll_id,  \n" + 
            "       pap.accrual_plan_id,  \n" + 
            "       papf.effective_end_date last_working_date  \n" + 
            "  FROM per_people_f papf,  \n" + 
            "       per_assignments_f paaf,  \n" + 
            "       pay_accrual_plans pap,  \n" + 
            "       per_absence_attendances paa,  \n" + 
            "       per_absence_attendance_types paat  \n" + 
            " WHERE     papf.person_id = paaf.person_id  \n" + 
            "       AND paaf.person_id = PAA.PERSON_ID  \n" + 
            "       AND papf.business_group_id = 169  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN papf.effective_start_date  \n" + 
            "                               AND papf.effective_end_date  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN paaf.effective_start_date  \n" + 
            "                               AND paaf.effective_end_date  \n" + 
            "       AND paaf.primary_flag = 'Y'  \n" + 
            "       AND papf.current_employee_flag IS NOT NULL  \n" + 
            "       AND papf.business_group_id = pap.business_group_Id  \n" + 
            "       AND paa.absence_attendance_type_id = paat.absence_attendance_type_id  \n" + 
            "       AND paat.name = 'Emergency Leave'  \n" + 
            "       AND (paa.date_start BETWEEN TO_DATE (TRUNC (SYSDATE, 'YYYY'))  \n" + 
            "                               AND TO_DATE (  \n" + 
            "                                      ADD_MONTHS (TRUNC (SYSDATE, 'YYYY'),  \n" + 
            "                                                  12)))  \n" + 
            "       AND current_employee_flag = 'Y'  \n" + 
            "       AND papf.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
            "/* Osama E. A. Wanis - 8-JAN-2017  \n" + 
            "The following added as part of the carry over process ---> so that the \"Annual Leave Accrual Carried Over'\" is included/shown in the report  \n" + 
            "*/  \n" + 
            "UNION ALL  \n" + 
            "SELECT DISTINCT  \n" + 
            "       e.person_id,  \n" + 
            "       e.employee_number,  \n" + 
            "       e.full_name,  \n" + 
            "       d.supervisor_id,  \n" + 
            "       d.assignment_id,  \n" + 
            "       c.business_group_id,  \n" + 
            "          'Carried Over from previous year('  \n" + 
            "       || TO_CHAR (TO_NUMBER (TO_CHAR (SYSDATE, 'YYYY')) - 1)  \n" + 
            "       || ')'  \n" + 
            "          Comments,  \n" + 
            "       a.effective_start_date START_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN e.effective_end_date IS NULL  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          WHEN TO_CHAR (e.effective_end_date, 'RRRR') = '4712'  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          ELSE  \n" + 
            "             e.effective_end_date  \n" + 
            "       END  \n" + 
            "          END_DATE,  \n" + 
            "       /* commented by Osama E. Adly (5-JUNE-2017) : so that carry over displayed in credit NOT debit  \n" + 
            "       CASE  \n" + 
            "          WHEN element_name = 'Annual Leave Accrual Carried Over'  \n" + 
            "          THEN  \n" + 
            "             ROUND (TO_NUMBER (f.screen_entry_value), 2)  \n" + 
            "          ELSE  \n" + 
            "             NULL  \n" + 
            "       END */  \n" + 
            "  \n" + 
            "       /* commented by Osama E. Adly (5-JUNE-2017) : so that carry over displayed in credit NOT debit  \n" + 
            "       CASE  \n" + 
            "           WHEN element_name = 'Annual Leave Accrual Carried Over'  \n" + 
            "                --AND SIGN (ROUND (TO_NUMBER (f.screen_entry_value), 2)) = 1  \n" + 
            "           THEN  \n" + 
            "              ROUND (TO_NUMBER (f.screen_entry_value), 2)  \n" + 
            "           ELSE  \n" + 
            "              NULL  \n" + 
            "        END */  \n" + 
            "       ROUND (TO_NUMBER (f.screen_entry_value), 2) credit,  \n" + 
            "       /* commented by Osama E. Adly (5-JUNE-2017) : so that carry over displayed in credit NOT debit  \n" + 
            "       CASE  \n" + 
            "           WHEN element_name = 'Annual Leave Accrual Carried Over'  \n" + 
            "                AND SIGN (ROUND (TO_NUMBER (f.screen_entry_value), 2)) = -1  \n" + 
            "           THEN  \n" + 
            "              ROUND (TO_NUMBER (f.screen_entry_value), 2)  \n" + 
            "           ELSE  \n" + 
            "              NULL  \n" + 
            "        END    */  \n" + 
            "       0 Debit,  \n" + 
            "       d.payroll_id,  \n" + 
            "       pap.accrual_plan_id,  \n" + 
            "       e.effective_end_date last_working_date  \n" + 
            "  FROM pay_element_entries_f a,  \n" + 
            "       pay_element_links_f b,  \n" + 
            "       pay_element_types_f c,  \n" + 
            "       per_all_assignments_f d,  \n" + 
            "       per_people_f e,  \n" + 
            "       pay_element_entry_values_f f,  \n" + 
            "       pay_input_values_f i,  \n" + 
            "       per_periods_of_service j,  \n" + 
            "       pay_accrual_plans pap  \n" + 
            " WHERE     a.element_link_id = b.element_link_Id  \n" + 
            "       AND b.element_type_id = c.element_type_id  \n" + 
            "       AND a.assignment_id = d.assignment_Id  \n" + 
            "       AND d.person_Id = e.person_id  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN d.effective_start_date  \n" + 
            "                               AND d.effective_end_date  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN e.effective_start_date  \n" + 
            "                               AND e.effective_end_date  \n" + 
            "       AND a.element_entry_id = f.element_entry_id  \n" + 
            "       AND f.input_value_Id = i.input_value_Id  \n" + 
            "       AND e.person_id = j.person_id  \n" + 
            "       AND element_name = 'Annual Leave Accrual Carried Over'  \n" + 
            "       AND i.name = 'Plan Days'  \n" + 
            "       AND TO_CHAR (a.EFFECTIVE_START_DATE, 'DD-MON-YYYY') =  \n" + 
            "              '01-JAN-' || TO_CHAR (SYSDATE, 'YYYY')  \n" + 
            "       AND (a.EFFECTIVE_START_DATE BETWEEN f.effective_start_date  \n" + 
            "                                       AND f.effective_end_date)  \n" + 
            "       AND current_employee_flag = 'Y'  \n" + 
            "       AND d.business_group_id = pap.business_group_id  \n" + 
            "       AND e.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
            "UNION ALL  \n" + 
            "SELECT DISTINCT  \n" + 
            "       e.person_id,  \n" + 
            "       e.employee_number,  \n" + 
            "       e.full_name,  \n" + 
            "       d.supervisor_id,  \n" + 
            "       d.assignment_id,  \n" + 
            "       c.business_group_id,  \n" + 
            "       element_name || ' ' || a.attribute1 || '  ' || a.attribute2 comments,  \n" + 
            "       a.effective_start_date START_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN e.effective_end_date IS NULL  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          WHEN TO_CHAR (e.effective_end_date, 'RRRR') = '4712'  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          ELSE  \n" + 
            "             e.effective_end_date  \n" + 
            "       END  \n" + 
            "          END_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN element_name = 'Public Holiday Reimbursement'  \n" + 
            "          THEN  \n" + 
            "             ROUND (TO_NUMBER (f.screen_entry_value), 2)  \n" + 
            "          ELSE  \n" + 
            "             0  \n" + 
            "       END  \n" + 
            "          credit,  \n" + 
            "       0 Debit,  \n" + 
            "       d.payroll_id,  \n" + 
            "       pap.accrual_plan_id,  \n" + 
            "       e.effective_end_date last_working_date  \n" + 
            "  FROM pay_element_entries_f a,  \n" + 
            "       pay_element_links_f b,  \n" + 
            "       pay_element_types_f c,  \n" + 
            "       per_all_assignments_f d,  \n" + 
            "       per_people_f e,  \n" + 
            "       pay_element_entry_values_f f,  \n" + 
            "       pay_input_values_f i,  \n" + 
            "       per_periods_of_service j,  \n" + 
            "       pay_accrual_plans pap  \n" + 
            " WHERE     a.element_link_id = b.element_link_Id  \n" + 
            "       AND b.element_type_id = c.element_type_id  \n" + 
            "       AND a.assignment_id = d.assignment_Id  \n" + 
            "       AND d.person_Id = e.person_id  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN d.effective_start_date  \n" + 
            "                               AND d.effective_end_date  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN e.effective_start_date  \n" + 
            "                               AND e.effective_end_date  \n" + 
            "       AND a.element_entry_id = f.element_entry_id  \n" + 
            "       AND f.input_value_Id = i.input_value_Id  \n" + 
            "       AND e.person_id = j.person_id  \n" + 
            "       AND element_name IN ('Public Holiday Reimbursement')  \n" + 
            "       AND i.name IN ('Days')  \n" + 
            "       AND TO_CHAR (a.creation_date, 'YYYY') = TO_CHAR (SYSDATE, 'YYYY')  \n" + 
            "       AND (a.EFFECTIVE_START_DATE BETWEEN f.effective_start_date  \n" + 
            "                                       AND f.effective_end_date)  \n" + 
            "       AND current_employee_flag = 'Y'  \n" + 
            "       AND e.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
            "       AND d.business_group_id = pap.business_group_id  \n" + 
            "       AND f.element_entry_id IN  \n" + 
            "              (SELECT f.element_entry_id  \n" + 
            "                 FROM pay_element_entries_f a,  \n" + 
            "                      pay_element_links_f b,  \n" + 
            "                      pay_element_types_f c,  \n" + 
            "                      per_all_assignments_f d,  \n" + 
            "                      per_people_f e,  \n" + 
            "                      pay_element_entry_values_f f,  \n" + 
            "                      pay_input_values_f i,  \n" + 
            "                      per_periods_of_service j,  \n" + 
            "                      pay_accrual_plans pap  \n" + 
            "                WHERE     a.element_link_id = b.element_link_Id  \n" + 
            "                      AND b.element_type_id = c.element_type_id  \n" + 
            "                      AND a.assignment_id = d.assignment_Id  \n" + 
            "                      AND d.person_Id = e.person_id  \n" + 
            "                      AND TRUNC (SYSDATE) BETWEEN d.effective_start_date  \n" + 
            "                                              AND d.effective_end_date  \n" + 
            "                      AND TRUNC (SYSDATE) BETWEEN e.effective_start_date  \n" + 
            "                                              AND e.effective_end_date  \n" + 
            "                      AND a.element_entry_id = f.element_entry_id  \n" + 
            "                      AND f.input_value_Id = i.input_value_Id  \n" + 
            "                      AND e.person_id = j.person_id  \n" + 
            "                      AND element_name = 'Public Holiday Reimbursement'  \n" + 
            "                      AND i.name = 'Effective Date'  \n" + 
            "                      AND SUBSTR (f.screen_entry_value, 1, 4) =  \n" + 
            "                             TO_CHAR (SYSDATE, 'YYYY')  \n" + 
            "                      --and to_char(a.creation_date,'YYYY') <> to_char(sysdate,'YYYY')  \n" + 
            "                      AND (a.EFFECTIVE_START_DATE BETWEEN f.effective_start_date  \n" + 
            "                                                      AND f.effective_end_date)  \n" + 
            "                      AND current_employee_flag = 'Y'  \n" + 
            "                      AND d.business_group_id = pap.business_group_id)  \n" + 
            "UNION ALL  \n" + 
            "SELECT DISTINCT  \n" + 
            "       e.person_id,  \n" + 
            "       e.employee_number,  \n" + 
            "       e.full_name,  \n" + 
            "       d.supervisor_id,  \n" + 
            "       d.assignment_id,  \n" + 
            "       c.business_group_id,  \n" + 
            "       element_name || ' ' || a.attribute1 || '  ' || a.attribute2 comments,  \n" + 
            "       a.effective_start_date START_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN e.effective_end_date IS NULL  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          WHEN TO_CHAR (e.effective_end_date, 'RRRR') = '4712'  \n" + 
            "          THEN  \n" + 
            "             (ADD_MONTHS (TRUNC (SYSDATE, 'RRRR'), 12) - 1)  \n" + 
            "          ELSE  \n" + 
            "             e.effective_end_date  \n" + 
            "       END  \n" + 
            "          END_DATE,  \n" + 
            "       CASE  \n" + 
            "          WHEN element_name = 'Rota Annual Leave Reimbursement'  \n" + 
            "          THEN  \n" + 
            "             ROUND (TO_NUMBER (f.screen_entry_value), 2)  \n" + 
            "          ELSE  \n" + 
            "             0  \n" + 
            "       END  \n" + 
            "          credit,  \n" + 
            "       0 Debit,  \n" + 
            "       d.payroll_id,  \n" + 
            "       pap.accrual_plan_id,  \n" + 
            "       e.effective_end_date last_working_date  \n" + 
            "  FROM pay_element_entries_f a,  \n" + 
            "       pay_element_links_f b,  \n" + 
            "       pay_element_types_f c,  \n" + 
            "       per_all_assignments_f d,  \n" + 
            "       per_people_f e,  \n" + 
            "       pay_element_entry_values_f f,  \n" + 
            "       pay_input_values_f i,  \n" + 
            "       per_periods_of_service j,  \n" + 
            "       pay_accrual_plans pap  \n" + 
            " WHERE     a.element_link_id = b.element_link_Id  \n" + 
            "       AND b.element_type_id = c.element_type_id  \n" + 
            "       AND a.assignment_id = d.assignment_Id  \n" + 
            "       AND d.person_Id = e.person_id  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN d.effective_start_date  \n" + 
            "                               AND d.effective_end_date  \n" + 
            "       AND TRUNC (SYSDATE) BETWEEN e.effective_start_date  \n" + 
            "                               AND e.effective_end_date  \n" + 
            "       AND a.element_entry_id = f.element_entry_id  \n" + 
            "       AND f.input_value_Id = i.input_value_Id  \n" + 
            "       AND e.person_id = j.person_id  \n" + 
            "       AND element_name IN ('Rota Annual Leave Reimbursement')  \n" + 
            "       AND i.name IN ('Days')  \n" + 
            "       AND TO_CHAR (a.creation_date, 'YYYY') = TO_CHAR (SYSDATE, 'YYYY')  \n" + 
            "       AND (a.EFFECTIVE_START_DATE BETWEEN f.effective_start_date  \n" + 
            "                                       AND f.effective_end_date)  \n" + 
            "       AND current_employee_flag = 'Y'  \n" + 
            "       AND e.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
            "       AND d.business_group_id = pap.business_group_id  \n" + 
            "       AND f.element_entry_id IN  \n" + 
            "              (SELECT f.element_entry_id  \n" + 
            "                 FROM pay_element_entries_f a,  \n" + 
            "                      pay_element_links_f b,  \n" + 
            "                      pay_element_types_f c,  \n" + 
            "                      per_all_assignments_f d,  \n" + 
            "                      per_people_f e,  \n" + 
            "                      pay_element_entry_values_f f,  \n" + 
            "                      pay_input_values_f i,  \n" + 
            "                      per_periods_of_service j,  \n" + 
            "                      pay_accrual_plans pap  \n" + 
            "                WHERE     a.element_link_id = b.element_link_Id  \n" + 
            "                      AND b.element_type_id = c.element_type_id  \n" + 
            "                      AND a.assignment_id = d.assignment_Id  \n" + 
            "                      AND d.person_Id = e.person_id  \n" + 
            "                      AND TRUNC (SYSDATE) BETWEEN d.effective_start_date  \n" + 
            "                                              AND d.effective_end_date  \n" + 
            "                      AND TRUNC (SYSDATE) BETWEEN e.effective_start_date  \n" + 
            "                                              AND e.effective_end_date  \n" + 
            "                      AND a.element_entry_id = f.element_entry_id  \n" + 
            "                      AND f.input_value_Id = i.input_value_Id  \n" + 
            "                      AND e.person_id = j.person_id  \n" + 
            "                      AND element_name = 'Rota Annual Leave Reimbursement'  \n" + 
            "                      AND i.name = 'Effective Date'  \n" + 
            "                      AND SUBSTR (f.screen_entry_value, 1, 4) =  \n" + 
            "                             TO_CHAR (SYSDATE, 'YYYY')  \n" + 
            "                      --and to_char(a.creation_date,'YYYY') <> to_char(sysdate,'YYYY')  \n" + 
            "                      AND (a.EFFECTIVE_START_DATE BETWEEN f.effective_start_date  \n" + 
            "                                                      AND f.effective_end_date)  \n" + 
            "                      AND current_employee_flag = 'Y'  \n" + 
            "                      AND d.business_group_id = pap.business_group_id)                      \n" + 
            "                )\n";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getAnnualLeavesBalance");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "Open Leave Balance", "Annual Leaves");
            statement = connection.prepareStatement(sqlStm);
                
              
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          

                            if ((connection != null) && !(connection.isClosed()))
                                      connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                  statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                     rs.close();
                                                                     
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    
    
    @POST
    @Produces("application/json")
    @Path("/getTOILExpirePost")
    public Response getTOILExpirePost(@Context SecurityContext sc ,  @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs =null;
            try {
            connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getTOILExpirePost");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "Open Toil Expire ", "Toil Expire");
                
            statement = connection.prepareStatement("SELECT PAPF.EMPLOYEE_NUMBER, PAPF.GLOBAL_NAME, PAPF.FULL_NAME, PAAF.SUPERVISOR_ID,\n" + 
            "SUM(CASE WHEN UPPER(PAAT.NAME) = 'TIME OFF IN LIEU' THEN PAA.ABSENCE_DAYS * -1 ELSE PAA.ABSENCE_DAYS END ) ||' Day(s)' NO_OF_DAYS,\n" + 
            "DECODE (TRIM(PAPF.GLOBAL_NAME) , 'SYSADMIN' , 'SYS' , (SUBSTR (TRIM(REPLACE(PAPF.GLOBAL_NAME,' ','')) , INSTR(TRIM(REPLACE(PAPF.GLOBAL_NAME,' ','')), ',' )+1,1))|| UPPER(SUBSTR (TRIM(PAPF.GLOBAL_NAME),1,1) ))INTIALS ,\n" + 
            "'class'||(MOD(LENGTH(TRIM(PAPF.GLOBAL_NAME)),4)+1) CLASS_NAME \n"+
            "FROM PER_PEOPLE_F PAPF\n" + 
            ",PER_ASSIGNMENTS_F PAAF\n" + 
            ",PER_ABSENCE_ATTENDANCES PAA\n" + 
            ",PER_ABSENCE_ATTENDANCE_TYPES PAAT\n" + 
            "WHERE 1=1\n" + 
            "AND PAPF.PERSON_ID = PAAF.PERSON_ID\n" + 
            "AND TRUNC(SYSDATE) BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE\n" + 
            "AND TRUNC(SYSDATE) BETWEEN PAAF.EFFECTIVE_START_DATE AND PAAF.EFFECTIVE_END_DATE\n" + 
            "AND PAAF.PRIMARY_FLAG = 'Y'\n" + 
            "AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'\n" + 
            "AND PAPF.PERSON_ID = PAA.PERSON_ID\n" + 
            "AND PAA.ABSENCE_ATTENDANCE_TYPE_ID = PAAT.ABSENCE_ATTENDANCE_TYPE_ID\n" + 
            "AND UPPER(PAAT.NAME) IN ('EXTRA TIME WORKED', 'TIME OFF IN LIEU')\n" + 
            "AND CASE WHEN UPPER(PAAT.NAME) = 'TIME OFF IN LIEU' THEN LAST_DAY(SYSDATE) ELSE ADD_MONTHS(PAA.DATE_END,3) END <= LAST_DAY(SYSDATE) \n" + 
            "AND PAPF.BUSINESS_GROUP_ID=mbc_mobile_pkg.get_session_data('BUSINESS_GROUP_ID')\n" + 
            "AND (PAAF.SUPERVISOR_ID = mbc_mobile_pkg.get_session_data('PERSON_ID')  OR PAPF.PERSON_ID = mbc_mobile_pkg.get_session_data('PERSON_ID') )  \n" + 
            "GROUP BY PAPF.EMPLOYEE_NUMBER, PAPF.GLOBAL_NAME, PAPF.FULL_NAME, PAAF.SUPERVISOR_ID\n" + 
            "HAVING SUM(CASE WHEN UPPER(PAAT.NAME) = 'TIME OFF IN LIEU' THEN PAA.ABSENCE_DAYS * -1 ELSE PAA.ABSENCE_DAYS END) > 0\n");
               
             rs = statement.executeQuery();
          
              
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                e.printStackTrace();
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
}

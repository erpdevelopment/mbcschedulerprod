import  {NavController, NavParams } from 'ionic-angular';
import { AppModule } from '../../../app/app.module';
import { Navbar } from 'ionic-angular';
import { ViewChild, Component } from '@angular/core';
import {SchedulerProvider} from '../../../providers/scheduler/scheduler'
import { LoadingController } from 'ionic-angular';
import {AlertController } from 'ionic-angular';
import { PublicSpacePage } from  '../../Scheduler/publicSpace/publicspace'
/**
 * Generated class for the OrgsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-orgs',
  templateUrl: 'orgs.html',
})
export class OrgsPage {

 
  MyOrgList;
  MyOrgListAll;
  fromR=0;
  v_step=12;
  toR=this.v_step;

  @ViewChild('pageTopx') pageTopx;
  

 AArole :string ;
  constructor(public navCtrl: NavController, public navParams: NavParams,public SchedulerData:SchedulerProvider,
    public loadingController: LoadingController ,  public AlertController :AlertController) {
  
 


    
    

  }

  ionViewDidLoad() {
   // this.setBackButtonAction();
   this.getMyOrgList();
    console.log('ionViewDidLoad OrganizationPage');
  
  }

  getMyOrgList()
  {
    
    
   

    try{

      this.SchedulerData.getMyOrgList()
     // this.payrollData.getPayslipList(this.businessGroup,this.personId)
      
      .then(data => {
        
      this.MyOrgListAll= data;
      if (this.MyOrgListAll)
      {
      this.MyOrgList=this.MyOrgListAll.slice(this.fromR,this.toR);
      console.log("DataLoaded");
    }
      });
      console.log("Test After Loading");
     
    }
    catch (error)
    {
    
      
      AppModule.showMessage(this.AlertController,error);
    }
  }



  async doRefresh(refresher) {
    this.toR=this.v_step;
    await this.getMyOrgList();
    refresher.complete();

    }



    doInfinite(infiniteScroll) {


      // this.fromR=this.fromR+this.v_step;
       this.toR=this.toR+this.v_step;
   
   
       try{
   
         
           
         if(this.MyOrgListAll)
         this.MyOrgList=(this.MyOrgListAll.slice(0 ,this.toR));
         
         infiniteScroll.complete();
     }
       catch (err)
       {
         
       }
       console.log(' infiniteScroll has ended');
     }


     async orgTapped(event, item)
     {
 
      if (item==null)
      return;
 
       
 
    
 
     
      try
      {  
    
        this.AArole
        console.log (item.organization_id)
        this.AArole=item.role_name ;
        console.log(this.AArole);
       if( this.AArole.includes('EMP') )
       { AppModule.sAAmyspace='Y';
       console.log('showworkspace');
      }
        if(this.AArole=='EMP')
        {  AppModule.sAAmyteam='Y';
        console.log('showwmyteams');
      }
        if(this.AArole !='EMP')
       { AppModule.sAAworkgroups='Y';
      console.log('showworkgroups');
      }
      console.log('workgroup'+AppModule.sAAworkgroups);
      console.log('myteam'+AppModule.sAAmyteam);
      console.log('myspace'+AppModule.sAAmyspace);
       AppModule.sAAOrganizationId=item.organization_id;
       this.navCtrl.push(PublicSpacePage);
         
  
      }
      catch (err)
      {
        //alert ('dddd');
        AppModule.showMessage(this.AlertController,"error");
      }
    
     
   
     }
     
   
     
   


}

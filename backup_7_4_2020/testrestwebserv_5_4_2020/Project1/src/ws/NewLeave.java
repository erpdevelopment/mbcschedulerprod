package ws;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Encoded;
import java.sql.Types;
import javax.ws.rs.HeaderParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import javax.ws.rs.core.SecurityContext;

import oracle.jdbc.OracleDriver;


    @Path("sshr/newLeave")
public class NewLeave {
    public NewLeave() {
        super();
    }

    @POST
    @Produces("application/json")
    @Path("/getLeavesTypes")
    public Response getLeavesTypes(@Context SecurityContext sc,
                                   @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
               throw new SecurityException("User is unauthorized.");
            if (wsid==null || signature==null )  
             throw new SecurityException("Unauthorized Access.");
        
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT ABSENCE_ATTENDANCE_TYPE_ID , Name LeaveName \n" + 
            "    FROM (SELECT paat.ABSENCE_ATTENDANCE_TYPE_ID,\n" + 
            "                 paat.BUSINESS_GROUP_ID,\n" + 
            "                 paat.INPUT_VALUE_ID,\n" + 
            "                 paat.DATE_EFFECTIVE,\n" + 
            "                 paattl.NAME,\n" + 
            "                 paat.ABSENCE_CATEGORY,\n" + 
            "                 paat.DATE_END,\n" + 
            "                 paat.HOURS_OR_DAYS,\n" + 
            "                 paat.INCREASING_OR_DECREASING_FLAG,\n" + 
            "                 paat.REQUEST_ID,\n" + 
            "                 paat.PROGRAM_APPLICATION_ID,\n" + 
            "                 paat.PROGRAM_ID,\n" + 
            "                 paat.PROGRAM_UPDATE_DATE,\n" + 
            "                 paat.ATTRIBUTE_CATEGORY,\n" + 
            "                 paat.ATTRIBUTE1,\n" + 
            "                 paat.ATTRIBUTE2,\n" + 
            "                 paat.ATTRIBUTE3,\n" + 
            "                 paat.ATTRIBUTE4,\n" + 
            "                 paat.ATTRIBUTE5,\n" + 
            "                 paat.ATTRIBUTE6,\n" + 
            "                 paat.ATTRIBUTE7,\n" + 
            "                 paat.ATTRIBUTE8,\n" + 
            "                 paat.ATTRIBUTE9,\n" + 
            "                 paat.ATTRIBUTE10,\n" + 
            "                 paat.ATTRIBUTE11,\n" + 
            "                 paat.ATTRIBUTE12,\n" + 
            "                 paat.ATTRIBUTE13,\n" + 
            "                 paat.ATTRIBUTE14,\n" + 
            "                 paat.ATTRIBUTE15,\n" + 
            "                 paat.ATTRIBUTE16,\n" + 
            "                 paat.ATTRIBUTE17,\n" + 
            "                 paat.ATTRIBUTE18,\n" + 
            "                 paat.ATTRIBUTE19,\n" + 
            "                 paat.ATTRIBUTE20,\n" + 
            "                 paat.LAST_UPDATE_DATE,\n" + 
            "                 paat.LAST_UPDATED_BY,\n" + 
            "                 paat.LAST_UPDATE_LOGIN,\n" + 
            "                 paat.CREATED_BY,\n" + 
            "                 paat.CREATION_DATE,\n" + 
            "                 paat.COMMENTS \n" + 
            "                 \n" + 
            "            FROM per_absence_attendance_types paat,\n" + 
            "                 per_abs_attendance_types_tl paattl\n" + 
            "           WHERE     paat.BUSINESS_GROUP_ID =  mbc_mobile_pkg.get_session_data('BUSINESS_GROUP_ID') \n" + 
            "           \n" + 
            "                 AND (paat.date_end IS NULL OR paat.date_end > SYSDATE)\n" + 
            "                 AND (paat.date_effective < SYSDATE)\n" + 
            "                 AND paat.absence_attendance_type_id =\n" + 
            "                        paattl.absence_attendance_type_id\n" + 
            "                 AND paattl.language = USERENV ('LANG')\n" + 
            "                 AND (   (    hr_api.return_legislation_code (\n" + 
            "                                 paat.business_group_id) = 'GB'\n" + 
            "                          AND paat.absence_category NOT IN ('M',\n" + 
            "                                                            'GB_PAT_ADO',\n" + 
            "                                                            'GB_PAT_BIRTH',\n" + 
            "                                                            'GB_ADO',\n" + 
            "                                                            'GB_ADDL_PAT_ADOPT',\n" + 
            "                                                            'GB_ADDL_PAT_BIRTH',\n" + 
            "                                                            'GB_SPL_BIRTH',\n" + 
            "                                                            'GB_SPL_ADOPT'))\n" + 
            "                      OR (    hr_api.return_legislation_code (\n" + 
            "                                 paat.business_group_id) <> 'GB'\n" + 
            "                          AND paat.absence_category NOT IN ('GB_PAT_ADO',\n" + 
            "                                                            'GB_PAT_BIRTH',\n" + 
            "                                                            'GB_ADO',\n" + 
            "                                                            'GB_ADDL_PAT_ADOPT',\n" + 
            "                                                            'GB_ADDL_PAT_BIRTH',\n" + 
            "                                                            'GB_SPL_BIRTH',\n" + 
            "                                                            'GB_SPL_ADOPT'))))\n" + 
            "         QRSLT\n" + 
            "   WHERE (ABSENCE_ATTENDANCE_TYPE_ID NOT IN (select regexp_substr(HR_ABSENCE_RESTRICTED.ABSENCES_RESTRICTED(mbc_mobile_pkg.get_session_data('PERSON_ID') ,null),'[^,]+', 1, level) from dual\n" + 
            "  connect by regexp_substr(HR_ABSENCE_RESTRICTED.ABSENCES_RESTRICTED(mbc_mobile_pkg.get_session_data('PERSON_ID') ,null), '[^,]+', 1, level) is not null))\n" + 
            "ORDER BY Name\n";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getLeavesTypes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection,  "Open New Leave", " Leave Types");
            statement = connection.prepareStatement(sqlStemt);
                
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          

                            if ((connection != null) && !(connection.isClosed()))
                                      connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                  statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                     rs.close();
                                                                             
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }


    @POST
    @Produces("application/json")
    @Path("/getLOSTypes")
    public Response getLOSTypes(@Context SecurityContext sc,  @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select meaning LOS \n" + 
            "from fnd_lookup_values\n" + 
            "where 1=1\n" + 
            "and lookup_type = 'LEVEL_OF_SKINESS'\n" + 
            "and language = 'US'\n";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getLOSTypes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement(sqlStemt);
                //statement.setString(1, depId);  
               
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          

                            if ((connection != null) && !(connection.isClosed()))
                                      connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                  statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                     rs.close();
                                                                             
                                                     
                          
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }


    /**
     * @return
     */
    @POST
    @Produces("application/json")
    @Path("/getCLRTypes")
    public Response getCLRTypes(@Context SecurityContext sc , @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
                      throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
              
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select meaning CLR \n" + 
            "from fnd_lookup_values\n" + 
            "where 1=1\n" + 
            "and lookup_type = 'COMPASSIONATE_LEAVE_REASON'\n" + 
            "and language = 'US'\n";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getCLRTypes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement(sqlStemt);
                //statement.setString(1, depId);  
             
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          

                            if ((connection != null) && !(connection.isClosed()))
                                      connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                  statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                     rs.close();
                                                                             
                                             
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    @POST
    @Produces("application/json")
    @Path("/applyNewLeave")
    public Response applyNewLeave(@Context SecurityContext sc ,
                                  @HeaderParam("dateStart") String dateStart, @HeaderParam("dateEnd") String dateEnd,
                                  @HeaderParam("absAttx") String absenceAttendanceTypeId,
                                  @HeaderParam("abst") String absenceTypeName,
                                  @HeaderParam("status") String status,
                                  @HeaderParam("atx") String attachId,
                                  @HeaderParam("attribute1") String attribute1,
                                  @HeaderParam("attribute2") String attribute2,
                                  @HeaderParam("attribute3") String attribute3,
                                  @HeaderParam("attribute4") String attribute4,
                                  @HeaderParam("attribute5") String attribute5,
                                  @HeaderParam("attribute6") String attribute6,
                                  @HeaderParam("attribute7") String attribute7,
                                  @HeaderParam("attribute8") String attribute8,
                                  @HeaderParam("rp") String replacedPerson,
                                  @HeaderParam("wsid") String wsid, @HeaderParam("signature") String signature)
    {
           
          
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Response response=null;
            Connection connection = null;
            CallableStatement cs=null;
         
            String result=null;
            String transactionId=null;
            String itemKey=null;
        String block="DECLARE\n" + 
        "   RETURN_VALUE VARCHAR2(32000);\n" + 
        "   V_TRANSACTION_ID                  NUMBER;\n" + 
        "   V_TRANSACTION_STEP_ID             NUMBER;\n" + 
        "   V_ITEM_KEY                        NUMBER;\n" + 
        "   V_ABSENCE_ATTENDANCE_ID           NUMBER;\n" +
        "   V_DURATION                        VARCHAR2(50);\n" + 
        "    v_photo blob;\n" + 
        "BEGIN\n" + 
        "APPS.MBC_MOBILE_PKG.INITIALIZE_LEAVE(V_TRANSACTION_ID,V_TRANSACTION_STEP_ID , V_ITEM_KEY ,V_ABSENCE_ATTENDANCE_ID )       ; \n" + 
        "  RETURN_VALUE := APPS.MBC_MOBILE_PKG.NEW_LEAVE_REQUEST(\n" + 
        "    P_USER_ID=> mbc_mobile_pkg.get_session_data('USER_ID')  ,\n" + 
        "    P_ASSIGNMENT_ID=> mbc_mobile_pkg.get_session_data('ASSIGNMENT_ID')  ,\n" + 
        "    P_BUSINESS_GROUP_ID=> mbc_mobile_pkg.get_session_data('BUSINESS_GROUP_ID') ,\n" + 
        "    P_DATE_START=>  TO_DATE (? , 'YYYY-MM-DD'),\n" + 
        "    P_DATE_END=>    TO_DATE (? , 'YYYY-MM-DD'),\n" + 
        "    P_ABSENCE_ATTENDANCE_TYPE_ID=> ? ,\n" + 
        "    P_TRANSACTION_ID=>V_TRANSACTION_ID,\n" + 
        "    P_TRANSACTION_STEP_ID=>V_TRANSACTION_STEP_ID,\n" + 
        "    P_ITEM_KEY=>V_ITEM_KEY,\n" + 
        "    P_ABSENCE_ATTENDANCE_ID=>V_ABSENCE_ATTENDANCE_ID,\n" + 
        "    P_PERSON_ID=> mbc_mobile_pkg.get_session_data('PERSON_ID') , \n" + 
       // "    P_REPLACEMENT_PERSON_ID=>NULL,\n" + 
        "    P_ABSENCE_TYPE_NAME=>?,\n" + 
        "    P_STATUS => ?,\n" + 
        "    P_ATTACH_ID => ?,\n" +
        "    P_ATTRIBUTE1=> ? ,\n" + 
        "    P_ATTRIBUTE2=> ? ,\n" + 
        "    P_ATTRIBUTE3=> ?,\n" + 
        "    P_ABS_ATTRIBUTE1=> ?,\n" + 
        "    P_ATTRIBUTE5=> ?,\n" + 
        "    P_ATTRIBUTE6=>?,\n" + 
        "    P_ATTRIBUTE7=>?,\n" + 
        "    P_ATTRIBUTE8=> ? ,\n" + 
        "    P_DURATION=> V_DURATION ,\n" + 
        "    P_REPLACEMENT_PERSON_ID=> ? );\n" + 
   //     "    commit;\n" + 
      "    ?:=RETURN_VALUE ;   \n" + 
      "    ?:=V_TRANSACTION_ID ;   \n" + 
      "    ?:=V_ITEM_KEY ;   \n" + 
      "    ?:=V_DURATION ;   \n" + 
        "    \n" + 
        "END;";
            try {
                connection = Helper.getOracleConnection();
                String resultx=Helper.checkSession(connection, wsid, signature , wsid+"applyNewLeave");
                if (!"OK".equals(resultx))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "apply new Leave", "Status is "+status);
                
//                if( status.equalsIgnoreCase("W") && attachId !=null )
//                {
//                    addAttachImage(connection, personId, attachId, fileData);
//                }
//               
                
                if (attribute2!=null)
                    attribute2= URLDecoder.decode(attribute2);
        
                
                    
            
                 cs=  connection.prepareCall(block);
      
                
                cs.setString(1,dateStart );
                cs.setString(2,dateEnd );
                cs.setString(3,absenceAttendanceTypeId );
                cs.setString(4,absenceTypeName );
                cs.setString(5,status );
                cs.setString(6,attachId );
                cs.setString(7,attribute1);
                cs.setString(8,attribute2 );
                cs.setString(9,attribute3 );
                cs.setString(10,attribute4 );
                cs.setString(11,attribute5 );
                cs.setString(12,attribute6 );
                cs.setString(13,attribute7 );
                cs.setString(14,attribute8 );
                cs.setString(15,replacedPerson );
                
                cs.registerOutParameter(16, Types.VARCHAR);
                cs.registerOutParameter(17, Types.VARCHAR);
                cs.registerOutParameter(18, Types.VARCHAR);
                cs.registerOutParameter(19, Types.VARCHAR);

                cs.executeUpdate();
               
               result=cs.getString(16);
           
           System.out.println("result is "+result);
              
              if (!"success".equalsIgnoreCase(result))
              {
                 if (result!=null) 
                  response = Helper.StringtoReponse(result.replace("\n", "").replace("\r", ""));
                 else
                      response = Helper.StringtoReponse("Error:101, Please contact system administrator");
                  
                  }
                else
              {
                  
                     
                  if( status.equalsIgnoreCase("Y") )
                  {
                      connection.commit();
                      
                      response = Helper.StringtoReponse(result,"3");
                      
                  }
                  else
                  {
                          transactionId=cs.getString(17);
                          response= getApproverList(connection,transactionId , cs.getString(19));
                          
                          connection.rollback();
                          itemKey=cs.getString(18);
                       cancelTransaction(connection, transactionId, itemKey);
                      
                      }
                  
               
                  
                  }

                
            
            }
            catch(SecurityException s)
             {
                 s.printStackTrace();
                 response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                 }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                            cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
            
              return response; 
       
        
        }
    

    
    public Response getApproverList(   Connection connection, String transactionId ,String duration)
    {
        
        
            //Connection connection=null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select line_no,job_title,display_name,intials,imge from  table (MBC_MOBILE_PKG.GET_LEAVE_APPROVER_LIST(?))";
            try {
         //   connection = getConnection();
             //  connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, transactionId);  
               
               
             rs = statement.executeQuery();
       
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,duration);
                    
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
               
                map.writeValue(outPut, m);
                
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   // response = Response.status(404).entity("Unable to find record").type("text/plain").build();
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((statement != null) && !(statement.isClosed()))
                               // connection.close();
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                                                   rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    public Response getNoApproverList(   Connection connection )
    {
        
        
            //Connection connection=null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select  null line_no, null user_name, null user_id, null person_id, 'No Approvers' job_title, null display_name, null intials, null imge from dual";
            try {
         //   connection = getConnection();
             //  connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
                
               
               
             rs = statement.executeQuery();
       
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"");
                    
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
               
                map.writeValue(outPut, m);
                
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                }
            finally {
                        try {
                          
                            if ((statement != null) && !(statement.isClosed()))
                              
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                                                   rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    



    public void cancelTransaction( Connection connection ,String transactionId , String itemKey )
    {
        
        
            //Connection connection=null;
            CallableStatement cs=null;
           
            String sqlStemt="BEGIN\n" + 
            "  apps.mbc_mobile_pkg.cancel_request(\n" + 
            "    p_item_type=>'HRSSA',\n" + 
            "    p_item_key=> ?,\n" + 
            "    p_process_int_name=>'MBC_LEAVE_REQUEST',\n" + 
            "    p_transaction_id=> ?\n" + 
            "    );\n" + 
            "END;";
            try {
         //   connection = getConnection();
              //  connection = Helper.getOracleConnection();
              cs=  connection.prepareCall(sqlStemt);
          
                cs.setString(1, itemKey);  
                cs.setString(2, transactionId);  
                cs.executeUpdate();;
            }
          catch (Exception e)
                {
                e.printStackTrace();
                }
            finally {
                try {
                  
                    if ((cs != null) && !(cs.isClosed()))
                     //   connection.close();
                                  cs.close();
                } catch (SQLException sqle) {
                    // TODO: Add catch code
                    sqle.printStackTrace();
                }
            }
        }
    
//    public static void main (String[] args) {
//        
//        applyNewLeave(
//        "23451", // USER
//        "40941", // ASSIGMENT
//        "81",   // GROUP IS 
//        "22-10-2021", // START
//        "22-10-2021", // END 
//        "61",  // LEAVE TYPE
//        "53435",  // PERSON
//        "webservice call ",  // TYPE NAME 
//        "Y",  // STATUS 
//        "",
//        "TEST INTERFACE ",
//        "",
//        "",
//        "",
//        "",
//        "",
//        ""
//     
//            
//            
//            
//            
//            );
//    }

    
    @POST
    @Produces("application/json")
    @Path("/addAttach")
    public Response addAttach(@Context SecurityContext sc , 
                              @HeaderParam("atx") String attachId,
                              @HeaderParam("fd") String fileData
                              , @HeaderParam("wsid") String wsid,
                              @HeaderParam("signature") String signature)
    {
            
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            throw new SecurityException("Unauthorized Access.");
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Response response=null;
                        Connection connection = null;
                        CallableStatement cs=null;
                        String result;
            String block="begin ?:=MBC_MOBILE_PKG.upload_image( mbc_mobile_pkg.get_session_data('PERSON_ID') ,?,?); end;";
            try {
                            connection = Helper.getOracleConnection();
                String resultx=Helper.checkSession(connection, wsid, signature , wsid+"addAttach");
                if (!"OK".equals(resultx))
                throw new SecurityException("Unauthorized Access."); 
                             cs=  connection.prepareCall(block);
                if (fileData!=null)
                fileData=fileData.replaceAll(" ", "+");
                          
                
                cs.setString(3,attachId );
                cs.setString(4,fileData );
            cs.registerOutParameter(1, Types.VARCHAR);
                
            cs.executeUpdate();
                          
                          result=cs.getString(1);
              //  System.out.println("data is  is "+fileData);
                      System.out.println("result is "+result);
            response = Helper.StringtoReponse(result);
            }
                  
            catch(SecurityException s)
             {
                 s.printStackTrace();
                 response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                 }   
                        catch(Exception e)
                        {
                            e.printStackTrace();
                                response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                            }
                        finally {
                                    try {
                                        if ((connection != null) && !(connection.isClosed()))
                                            connection.close();
                                        if ((cs != null) && !(cs.isClosed()))
                                                        cs.close();
                                    } catch (SQLException sqle) {
                                        // TODO: Add catch code
                                        sqle.printStackTrace();
                                    }
                                }
            
                          return response; 
        
        }
    
    
    

    
    
    public static Connection getConnection() throws SQLException {
        String username = "apps";
        String password = "apps";
        String thinConn = "jdbc:oracle:thin:@10.10.131.25:1528:ERPUPG2";
        DriverManager.registerDriver(new OracleDriver());
        Connection conn = DriverManager.getConnection(thinConn, username, password);
        conn.setAutoCommit(false);
        return conn;
    }

}

CREATE OR REPLACE PACKAGE APPS.AS_SCHDLR_PKG
IS
   PROCEDURE REPEAT_PATTERN (IN_SCHEDULER_STAFF_ID       NUMBER,
                             OUT_RESULT              OUT VARCHAR2);
--                             PROCEDURE REPEATRANGE_PATTERN (IN_SCHEDULER_STAFF_ID       NUMBER,IN_SCHEDULAR_DATE date,
--                             OUT_RESULT              OUT VARCHAR2);
  PROCEDURE REPEAT_PATTERN_RANGE (IN_SCHEDULER_STAFF_ID       NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date
                            , OUT_RESULT              OUT VARCHAR2);

  PROCEDURE REPEAT_PATTERNNEWSROOM (IN_SCHEDULER_STAFF_ID       NUMBER,IN_SCHEDULAR_DATE date, IN_FK_SHIFT_ID Number ,IN_FK_SHIFT_ATTRIBUTE_ID Number
                             , OUT_RESULT              OUT VARCHAR2);
 PROCEDURE REPEATRANGE_PATTERNNEWSROOM (IN_SCHEDULER_STAFF_ID       NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date, IN_FK_SHIFT_ID Number ,IN_FK_SHIFT_ATTRIBUTE_ID Number
                         ,IN_PERSON_ID NUMBER    , OUT_RESULT OUT VARCHAR2);
  PROCEDURE REPEAT_PATTERNPRESENTER (IN_SCHEDULER_STAFF_ID       NUMBER,IN_SCHEDULAR_DATE date,IN_FK_SECTION_ID Number
                             , OUT_RESULT              OUT VARCHAR2);
    PROCEDURE REPEATRANGE_PATTERNPRESENTER (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date,IN_FK_SECTION_ID Number
                             , OUT_RESULT              OUT VARCHAR2);
   PROCEDURE DELETE_ALL_SCHDLR (IN_SCHEDULER_STAFF_ID NUMBER);
    PROCEDURE DELETE_SCHDLR_RANGE (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date);
    PROCEDURE DELETE_ALL_SCHDLR_NEWSROOM (IN_SCHEDULER_STAFF_ID NUMBER);
     PROCEDURE DELETE_ALL_SCHDLR_NROM_RANGE (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date,IN_PERSON_ID NUMBER);

    PROCEDURE DELETE_ALL_SCHDLR_PRESENTER (IN_SCHEDULER_STAFF_ID NUMBER);
    PROCEDURE DELETE_RANGE_SCHDLR_PRSNTR (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date);

 PROCEDURE DELETE_SCHDLR (IN_SCHEDULER_STAFF_ID NUMBER,VTYPE VARCHAR2,IN_PERSON_ID NUMBER);
   FUNCTION GET_WRKGRP_ANNUAL_LEAVE_USED (IN_WORK_GROUP_ID NUMBER)
      RETURN NUMBER;

   FUNCTION GET_SCHDLR_COUNT_ON_DATE (IN_SCHEDULER_ID            NUMBER,
                                      IN_PERSON_ID               NUMBER,
                                      IN_SCHEDULAR_START_DATE    DATE)
      RETURN NUMBER;
       FUNCTION SCHDLRNEWSROOM_COUNT_ON_DATE (IN_SCHEDULER_ID            NUMBER,INSHIFTATTRIBUTE_ID Number,
                                      IN_PERSON_ID               NUMBER,
                                      IN_SCHEDULAR_START_DATE    DATE)RETURN NUMBER;

    FUNCTION SCHDLRPRESENTER_COUNT_ON_DATE (IN_SCHEDULER_ID            NUMBER,INSECTION_ID Number,
                                      IN_PERSON_ID               NUMBER,
                                      IN_SCHEDULAR_START_DATE    DATE)RETURN NUMBER;

   PROCEDURE SCHDLR_INTERSECT_ASSIGNMENT (
      IN_SCHEDULER_ID               NUMBER,
      IN_PERSON_ID                  NUMBER,
      IN_SCHEDULAR_START_DATE       DATE,
      OUT_IS_EXIST              OUT VARCHAR2,
      OUT_EXIST_MESSAGE         OUT VARCHAR2);

   PROCEDURE SCHDLR_INTERSECT_LEAVE (IN_SCHEDULER_ID               NUMBER,
                                     IN_PERSON_ID                  NUMBER,
                                     IN_SCHEDULAR_START_DATE       DATE,
                                     OUT_IS_EXIST              OUT VARCHAR2,
                                     OUT_EXIST_MESSAGE         OUT VARCHAR2);

   PROCEDURE SUBMIT_SCHDLR_TRANS (IN_SCHEDULER_TRANS_HDR_ID    NUMBER,
                                  IN_PERSON_ID                 NUMBER);

   PROCEDURE APPLY_SCHDLR_TRANS (IN_SCHEDULER_TRANS_HDR_ID        NUMBER,
                                 OUT_GEN_SCHEDULER_STAFF_ID   OUT NUMBER,
                                 OUT_STATUS                   OUT VARCHAR2);

   PROCEDURE SUBMIT_SCHEDULER (IN_SCHEDULER_ID       NUMBER,
                               IN_PERSON_ID          NUMBER,
                               OUT_STATUS        OUT VARCHAR2);


   PROCEDURE PUSH_SCHEDULER_CALENDAR (IN_SCHEDULER_ID       NUMBER,
                                      OUT_STATUS        OUT VARCHAR2);
                                        PROCEDURE PUSH_OUTLOOK_CALENDAR (IN_SCHEDULER_ID NUMBER   , IN_SCHEDULER_STAFF_ID       NUMBER,
                                      OUT_STATUS        OUT VARCHAR2);

   PROCEDURE GET_WORKLIST_COUNT (IN_PERSONID        NUMBER,
                                 OUT_APPROVED   OUT NUMBER,
                                 OUT_REJECTED   OUT NUMBER,
                                 OUT_WAITING    OUT NUMBER);

   PROCEDURE WORKLIST_ACTION (IN_WORKLIST_ID    NUMBER,
                              IN_ACTION         VARCHAR2,
                              IN_NOTES          VARCHAR2);

   PROCEDURE VALIDATE_SCHEDULER (IN_SCHEDULER_ID           NUMBER,
                                 OUT_VALIDATE_REPORT   OUT CLOB);

   FUNCTION CHECK_ASSIGNMENT_OVERLAPPING (IN_ASSIGNMENT_EMP_ID    NUMBER,
                                          IN_PERSON_ID            NUMBER,
                                          IN_START_DATE           DATE,
                                          IN_END_DATE             DATE)
      RETURN VARCHAR2;

   PROCEDURE DELETE_AS_SCHEDULER (IN_SCHEDULER_ID NUMBER);
    --Added by Heba 11-july for tas integartion --
   function GetDateDifference(pstartdate date,penddate date)return number;
   function VALIDATE_TAS_RECORD(pperson_id Number,p_employeecode varchar2,pshiftcode varchar2,pshiftdate date,pshiftstart number,pshiftend number)return varchar2;
 PROCEDURE INSERT_TAS_RECORD(pperson_id Number,p_employeecode varchar2,pshiftcode varchar2,pshiftdate date,pshiftstart number,pshiftend number,pstatus varchar2 ,poff_flag varchar2);
  function GET_ASSIGNMENT_DURATION(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2;
    function GET_LEAVE_DURATION(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2;
     function GET_LEAVE_DURATION_NW(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2;
      function GET_LEAVE_STATUS(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2;
        function EXIST_IN_LEAVE(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2 ;
  function getOnSchedul_Leave_employee(pschedulerid Number) return Number;
            function GET_LEAVE_STATUS2(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2;
  function getValidationDetails(pschedulerid Number ,pdate Date) return varchar2;
  function getFirstdayofweekschedule(pdate Date) return date ;
    function GET_LEAVE_STATUS_COLOR(pperson_id number ,pstartdate Date ,pendddate Date,pschdulerstaffid number) return varchar2;
    function GET_LEAVE_STATUS_COLOR_NW(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2;
        function GET_FIRSTLEAVE_STATUS_COLOR_NA(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2;
  procedure Insertlookup;
      function CHECK_PATTERN_ON_OFF( PPattern_id Number ,  pdate Date )return varchar2;
      procedure WORKGROUP_VALIDATION ;
        PROCEDURE DELETE_SCHDLR_STAFF_FROM_TEMP(IN_SCHEDULER_ID NUMBER, SCHEDULER_STAFF_DATE Date );
       FUNCTION SCHEDULER_STAFF_BULLETIN(IN_SCHEDULER_STAFF_ID NUMBER) return varchar2;
       PROCEDURE LOAD_TEMPLATE_BULLETINS (IN_SCHEDULER_ID NUMBER,IN_SCHEDULER_TITLE varchar2,IN_TEMPLATE_ID NUMBER ,IN_PERSON_ID NUMBER,IN_COLUMN_DATE DATE);
   PROCEDURE INSERT_BULLETINS (IN_TEMPLATE_EMP_ID NUMBER,IN_SCHEDULER_STAFF_ID NUMBER);

          FUNCTION GETPUBLICSPACEREPLACEMENT(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2)RETURN VARCHAR2 ;
 FUNCTION PUBLICSPACEREPLACEMENT(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID NUMBER,P_ORG_ID NUMBER)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACEREPLACEMENT1(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACEBULLETIN(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID NUMBER,P_ORG_ID NUMBER)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACEBULLETIN1(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACESHIFT(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID NUMBER,P_ORG_ID NUMBER)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACESHIFT1(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACENOTES(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID NUMBER,P_ORG_ID NUMBER)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACENOTES1(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACEASSIGNMENTS(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACELEAVES(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2)RETURN VARCHAR2 ;
FUNCTION PUBLICSPACEREPLACEMENTDATA(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,P_ORG_ID NUMBER,PWORKGROUP_ID NUMBER)RETURN VARCHAR2 ;
PROCEDURE CREATE_SCHEDULER_SHIFT(IN_SCHEDULER_ID NUMBER ,FK_CATAGEORY_ID NUMBER   , OUT_RESULT              OUT VARCHAR2);
FUNCTION GETPENDING_LEAVE_DURATION(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2;
  FUNCTION Checkday_pending_leave(pperson_id number ,pstartdate Date ) return Number;
      FUNCTION Checkday_leave(pperson_id number ,pstartdate Date ) return Number;
      FUNCTION check_leave_pending_validation(pperson_id number,preplace_id number  ,pstartdate Date ) return varchar2;
              FUNCTION check_leave_pending_Repeat(pperson_id number ,pstartdate Date,penddate Date ) return varchar2;
              FUNCTION CHECK_UNIQUE_WORK_CATEGORY (PCATEGORY_NAME VARCHAR2,P_ORG_ID NUMBER)return VARCHAR2;
            PROCEDURE SHOWHIDE_SCHEDULER(VSTATUS varchar2);
                  
        PROCEDURE COMMIT_WORK_GROUP_LINE(P_WG_ID NUMBER, OUT_RESULT   OUT VARCHAR2);
    FUNCTION GET_INTIAL_NAME (P_PERSON_ID NUMBER)RETURN VARCHAR2;
       FUNCTION GET_PERSON_TYPE (P_PERSON_ID NUMBER) RETURN VARCHAR2 ;   
        
        
         FUNCTION GET_USERID(P_FIRST_NAME     VARCHAR2,
                       P_LAST_NAME      VARCHAR2) RETURN VARCHAR2;
           FUNCTION GET_EMP_PWD (P_FIRST_NAME          VARCHAR2,
                          P_LAST_NAME           VARCHAR2,
                          P_EMPLOYEE_NUMBER     VARCHAR2) RETURN VARCHAR2; 
                          PROCEDURE ADD_USER(P_PERSON_ID NUMBER ,P_FK_ORG_ID NUMBER ,OUT_RESULT  OUT VARCHAR2 ) ;
                        FUNCTION CREATE_USER(P_PERSON_ID NUMBER   )  RETURN NUMBER   ;            
                      FUNCTION ASSIGN_RESPONSABILITY(P_USER_ID NUMBER,P_START_DATE DATE )return varchar2;
                  
END AS_SCHDLR_PKG;
/
package com.mbc.oracle.scheduler.model.vo;

import oracle.jbo.domain.Number;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Mar 07 14:37:00 GST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AsOnEmployeeVORowImpl extends ViewRowImpl {

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        PersonId,
        WorkGroupName,
        Tasktype,
        Taskname,
        Employee,
        Taskdate,
        Replacedby,
        Replacing,
        Leaves,
        Assignment,
        AssgColor,
        LvColor,
        Xxcolor,
        Color,
        Notes,
        StartTimeHours;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int PERSONID = AttributesEnum.PersonId.index();
    public static final int WORKGROUPNAME = AttributesEnum.WorkGroupName.index();
    public static final int TASKTYPE = AttributesEnum.Tasktype.index();
    public static final int TASKNAME = AttributesEnum.Taskname.index();
    public static final int EMPLOYEE = AttributesEnum.Employee.index();
    public static final int TASKDATE = AttributesEnum.Taskdate.index();
    public static final int REPLACEDBY = AttributesEnum.Replacedby.index();
    public static final int REPLACING = AttributesEnum.Replacing.index();
    public static final int LEAVES = AttributesEnum.Leaves.index();
    public static final int ASSIGNMENT = AttributesEnum.Assignment.index();
    public static final int ASSGCOLOR = AttributesEnum.AssgColor.index();
    public static final int LVCOLOR = AttributesEnum.LvColor.index();
    public static final int XXCOLOR = AttributesEnum.Xxcolor.index();
    public static final int COLOR = AttributesEnum.Color.index();
    public static final int NOTES = AttributesEnum.Notes.index();
    public static final int STARTTIMEHOURS = AttributesEnum.StartTimeHours.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AsOnEmployeeVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute PersonId.
     * @return the PersonId
     */
    public Number getPersonId() {
        return (Number) getAttributeInternal(PERSONID);
    }


    /**
     * Gets the attribute value for the calculated attribute WorkGroupName.
     * @return the WorkGroupName
     */
    public String getWorkGroupName() {
        return (String) getAttributeInternal(WORKGROUPNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute Tasktype.
     * @return the Tasktype
     */
    public String getTasktype() {
        return (String) getAttributeInternal(TASKTYPE);
    }

    /**
     * Gets the attribute value for the calculated attribute Taskname.
     * @return the Taskname
     */
    public String getTaskname() {
        return (String) getAttributeInternal(TASKNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute Employee.
     * @return the Employee
     */
    public String getEmployee() {
        return (String) getAttributeInternal(EMPLOYEE);
    }

    /**
     * Gets the attribute value for the calculated attribute Taskdate.
     * @return the Taskdate
     */
    public String getTaskdate() {
        return (String) getAttributeInternal(TASKDATE);
    }

    /**
     * Gets the attribute value for the calculated attribute Replacedby.
     * @return the Replacedby
     */
    public String getReplacedby() {
        return (String) getAttributeInternal(REPLACEDBY);
    }

    /**
     * Gets the attribute value for the calculated attribute Replacing.
     * @return the Replacing
     */
    public String getReplacing() {
        return (String) getAttributeInternal(REPLACING);
    }

    /**
     * Gets the attribute value for the calculated attribute Leaves.
     * @return the Leaves
     */
    public String getLeaves() {
        return (String) getAttributeInternal(LEAVES);
    }

    /**
     * Gets the attribute value for the calculated attribute Assignment.
     * @return the Assignment
     */
    public String getAssignment() {
        return (String) getAttributeInternal(ASSIGNMENT);
    }

    /**
     * Gets the attribute value for the calculated attribute AssgColor.
     * @return the AssgColor
     */
    public String getAssgColor() {
        return (String) getAttributeInternal(ASSGCOLOR);
    }

    /**
     * Gets the attribute value for the calculated attribute LvColor.
     * @return the LvColor
     */
    public String getLvColor() {
        return (String) getAttributeInternal(LVCOLOR);
    }

    /**
     * Gets the attribute value for the calculated attribute Xxcolor.
     * @return the Xxcolor
     */
    public String getXxcolor() {
        return (String) getAttributeInternal(XXCOLOR);
    }

    /**
     * Gets the attribute value for the calculated attribute Color.
     * @return the Color
     */
    public String getColor() {
        return (String) getAttributeInternal(COLOR);
    }

    /**
     * Gets the attribute value for the calculated attribute Notes.
     * @return the Notes
     */
    public String getNotes() {
        return (String) getAttributeInternal(NOTES);
    }

    /**
     * Gets the attribute value for the calculated attribute StartTimeHours.
     * @return the StartTimeHours
     */
    public Number getStartTimeHours() {
        return (Number) getAttributeInternal(STARTTIMEHOURS);
    }


}


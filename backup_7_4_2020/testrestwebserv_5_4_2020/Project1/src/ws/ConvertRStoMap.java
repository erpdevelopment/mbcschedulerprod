package ws;



import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ConvertRStoMap {
    public ConvertRStoMap() {
        super();
    }

    protected static List<Map<String, Object>> getEntitiesFromResultSet(ResultSet resultSet) throws SQLException {
        ArrayList<Map<String, Object>> entities = new ArrayList<Map<String, Object>> ();
     //   while (resultSet.next()) {
            System.out.println("multiple");
        //    entities.add(getEntityFromResultSet(resultSet));
      //  }
      getEntityFromResultSet(resultSet,entities);
            
        System.out.println("en"+entities.size());
        return entities;
    }

    protected static List<Map<String, Object>> getEntitiesFromResultSet(ResultSet resultSet ,String type) throws SQLException {
        ArrayList<Map<String, Object>> entities = new ArrayList<Map<String, Object>> ();
     //   while (resultSet.next()) {
            System.out.println("multiple");
        //    entities.add(getEntityFromResultSet(resultSet));
      //  }
      getEntityFromResultSet(resultSet,entities,type);
            
        System.out.println("en"+entities.size());
        return entities;
    }
    
    protected static List<Map<String, Object>> getEntitiesFromResultSet(ResultSet resultSet ,int type) throws SQLException {
        ArrayList<Map<String, Object>> entities = new ArrayList<Map<String, Object>> ();
     //   while (resultSet.next()) {
            System.out.println("multiple");
        //    entities.add(getEntityFromResultSet(resultSet));
      //  }
      getEntityFromResultSet(resultSet,entities,type);
            
        System.out.println("en"+entities.size());
        return entities;
    }
    protected static Map<String, Object> getEntityFromResultSet(ResultSet resultSet) throws SQLException {
   int c=0;
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        Map<String, Object> resultsMap = new HashMap<String, Object>();
        while (resultSet.next()) {
         //   System.out.println("conut ="+c);
            c++;
            System.out.println("single");
            resultsMap.put("message", "1");
           
            for (int i = 1; i <= columnCount; ++i) {
                String columnName = metaData.getColumnName(i).toLowerCase();
                Object object = resultSet.getObject(i);
                System.out.println("cols i s "+columnName +" data is " + resultSet.getObject(i));
                resultsMap.put(columnName, object);
            }
        }
       System.out.println("su "+ resultsMap.size());
        return resultsMap;
    }
    
    protected static  ArrayList<Map<String, Object>> getEntityFromResultSet(ResultSet resultSet , ArrayList<Map<String, Object>> entities ) throws SQLException {
        
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
       
        while (resultSet.next()) {
         //   System.out.println("conut ="+c);
         Map<String, Object> resultsMap = new HashMap<String, Object>();
            System.out.println("single");
            resultsMap.put("message", "1");
            for (int i = 1; i <= columnCount; ++i) {
                String columnName = metaData.getColumnName(i).toLowerCase();
                Object object = resultSet.getObject(i);
            
             //   System.out.println("cols i s "+columnName +" data is " + resultSet.getObject(i));
                resultsMap.put(columnName, object);
            }
            
            entities.add(resultsMap);
        }
        return entities;
    }
    
    
    protected static  ArrayList<Map<String, Object>> getEntityFromResultSet(ResultSet resultSet , ArrayList<Map<String, Object>> entities   , String type) throws SQLException {
        
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
       
        while (resultSet.next()) {
         //   System.out.println("conut ="+c);
         Map<String, Object> resultsMap = new HashMap<String, Object>();
            System.out.println("single");
            resultsMap.put("message", "1");
            resultsMap.put("duration", type+"");
            for (int i = 1; i <= columnCount; ++i) {
                String columnName = metaData.getColumnName(i).toLowerCase();
                Object object=null;
                      if (metaData.getColumnTypeName(i).equalsIgnoreCase("CLOB"))
                       {
                         object=resultSet.getString(i);
                           }
                       else
                       {
                        object= resultSet.getObject(i);
                       }
            
             //   System.out.println("cols i s "+columnName +" data is " + resultSet.getObject(i));
                resultsMap.put(columnName, object);
            }
            entities.add(resultsMap);
        }
        return entities;
    }
    
    
    protected static  ArrayList<Map<String, Object>> getEntityFromResultSet(ResultSet resultSet , ArrayList<Map<String, Object>> entities   , int type) throws SQLException {
        
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
       
        while (resultSet.next()) {
         //   System.out.println("conut ="+c);
         Map<String, Object> resultsMap = new HashMap<String, Object>();
            System.out.println("single");
          //  resultsMap.put("message", "1");
            //resultsMap.put("duration", type+"");
            for (int i = 1; i <= columnCount; ++i) {
                String columnName = metaData.getColumnName(i);
                Object object=null;
                      if (metaData.getColumnTypeName(i).equalsIgnoreCase("CLOB"))
                       {
                         object=resultSet.getString(i);
                           }
                       else
                       {
                        object= resultSet.getObject(i);
                       }
            
             //   System.out.println("cols i s "+columnName +" data is " + resultSet.getObject(i));
                resultsMap.put(columnName, object);
            }
            
            entities.add(resultsMap);
        }
        Map<String, Object> resultsMap2 = new HashMap<String, Object>();
     //resultsMap2.put("Task", "Hours per Day");
        //entities.add(resultsMap2);
        return entities;
    }
    
    protected static List<Map<String, Object>> getEntitiesFromResultSetNestedJsonArray(ResultSet resultSet) throws SQLException {
        ArrayList<Map<String, Object>> entities = new ArrayList<Map<String, Object>> ();
     //   while (resultSet.next()) {
            System.out.println("multiple");
        //    entities.add(getEntityFromResultSet(resultSet));
      //  }
      getEntityFromResultSetSubjson(resultSet,entities);
            
        System.out.println("en"+entities.size());
        return entities;
    }
    
    protected static  ArrayList<Map<String, Object>> getEntityFromResultSetSubjson(ResultSet resultSet , ArrayList<Map<String, Object>> entities ) throws SQLException {
        
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
       
        while (resultSet.next()) {
         //   System.out.println("conut ="+c);
         Map<String, Object> resultsMap = new HashMap<String, Object>();
            System.out.println("single");
            resultsMap.put("message", "1");
            for (int i = 1; i <= columnCount; ++i) {
                String columnName = metaData.getColumnName(i).toLowerCase();
              
            
                if(columnName.equalsIgnoreCase("base_actions") || columnName.equalsIgnoreCase("more_actions")  )
                {
                    ArrayList<Map<String, Object>> netstedJsonArray = new ArrayList<Map<String, Object>> ();
                  
                    
                    String buttons = (String)resultSet.getObject(i);
                    if(buttons!=null)
                    {
                    String [] ButtonsArray=  buttons.split(";");  
                     
                     for (int button=0 ; button<ButtonsArray.length ;button++ )
                     {
                            Map<String, Object> resultsMap2 = new HashMap<String, Object>();
                             String [] buttonPrporties    =ButtonsArray[button].split(",");
                             for(int property=0;property<buttonPrporties.length;property++ )
                             {
                                 String propertyName=(buttonPrporties[property].split(":"))[0].toLowerCase().trim();
                                 String propertyValue=(buttonPrporties[property].split(":"))[1].trim();
                                 
                                 resultsMap2.put(propertyName, propertyValue);
                                 
                             }
                         netstedJsonArray.add(resultsMap2); 
                     
                     }
                     
                        resultsMap.put(columnName, netstedJsonArray);
                   
                    } else { // button is null
                           resultsMap.put(columnName, null);   }
                    
                    
                }
              else   // normal coloumn 
                {
                    
                        Object object=null;
                              if (metaData.getColumnTypeName(i).equalsIgnoreCase("CLOB"))
                               {
                                 object=resultSet.getString(i);
                                   }
                               else
                               {
                                object= resultSet.getObject(i);
                               }
                        resultsMap.put(columnName, object);
                    }
             
             
               
            }
         
            entities.add(resultsMap);
        }
        return entities;
    }
    
    
}

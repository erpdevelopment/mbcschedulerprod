import  {NavController, NavParams } from 'ionic-angular';

import { StartPage } from '../../shared-pages/start/start';

import { AppModule } from '../../../app/app.module';
import { Navbar } from 'ionic-angular';
import { ViewChild, Component } from '@angular/core';
import{MySpacePage}from '../../Scheduler/myspace/myspace'
import {MyTeamPage} from '../../Scheduler/myteam/myteam';
import {WorkgroupsPage} from '../../Scheduler/workgroups/workgroups';

/**
 * Generated class for the PublicSpacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: 'page-publicspace',
  templateUrl: 'publicspace.html',
})
export class PublicSpacePage {
AAmyspace :string ;
AAmyteam :string;
AAworkgroups :string ;

  @ViewChild(Navbar) Navbar: Navbar;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // this.userId            =this.navParams.get('userId');  
    // this.personId          =this.navParams.get('personId'); 
    // this.businessGroup     =this.navParams.get('businessGroup'); 
    // this.assignmentId      =this.navParams.get('assignmentId'); 
    this.AAmyspace=AppModule.sAAmyspace
    //AppModule.getProperty('myspace');
    this.AAmyteam=AppModule.sAAmyteam;
    //.getProperty('myteam');
    this.AAworkgroups=AppModule.sAAworkgroups;
    //.getProperty('workgroups');
  
  }

  ionViewDidLoad() {
    this.setBackButtonAction();
    console.log('ionViewDidLoad PublicSpacePage');
  }
  setBackButtonAction(){
    this.Navbar.backButtonClick = () => {
     
      this.navCtrl.push(StartPage);
   

    
     }
    }
    goMySpacePage()
    {
this.navCtrl.push(MySpacePage);
    }
    goMyTeamPage()
    {
this.navCtrl.push(MyTeamPage);
    }
    goWKage()
    {
this.navCtrl.push(WorkgroupsPage);
    }
  
}

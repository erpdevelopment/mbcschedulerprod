var whrChartData = [{value: 0, valueColor: "#609480"}, {value: 0, valueColor: "#F8DD46"}, {value: 0, valueColor: "#DEAF43"}, {value: 0, valueColor: "#DE4A43"}];
var whrChartCategories = ['>95%', '90%-95%', '85%-90%', '<85%'];
whrChartData[0].value += 150;
whrChartData[1].value += 320;
whrChartData[2].value += 450;
whrChartData[3].value += 180;

function empWhrChartCreate() {
            $("#_absence_employee_whr_bar").kendoChart({
                theme: 'material',
                legend: {
                    visible: false
                },
                chartArea: {
                    background: ""
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [{
                    name: "#Employees",
                    colorField: "valueColor",
                    data: whrChartData 
                }],
                valueAxis: {
                    visible:true,
min:0,
                    line: {
                        visible: false
                    },
                    majorGridLines: {
                        visible: false
                    },
                    axisCrossingValue: 0
                },
                categoryAxis: {
                    categories: whrChartCategories,
                    labels: {
                        font: "12px Calibri,Arial"
                    },
                    line: {
                        visible: false
                    },
                    majorGridLines: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}%",
                    template: "#= series.name #: #= value #"
                }
            });
        }
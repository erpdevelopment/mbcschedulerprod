package com.mbc.oracle.scheduler.view.servlet;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.sql.Blob;

import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.ResultSet;

import java.sql.SQLException;

import javax.naming.Context;

import javax.naming.InitialContext;

import javax.servlet.*;
import javax.servlet.http.*;

import javax.sql.DataSource;

public class EmpImgServlet extends HttpServlet {
    //private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private static final String CONTENT_TYPE = "image/gif; charset=utf-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String personId = "";
        try {
            personId = request.getParameter("personId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println("EmpImgServlet ::: personId=" + personId);
        response.setContentType(CONTENT_TYPE);

        OutputStream os = response.getOutputStream();
        Connection conn = null;
        try {
            
            conn = this.getDataSourceConnection("jdbc/MbcSchedulerDS");
                //("jdbc/MbcSchedulerUPG");
                //
                //("jdbc/MbcSchedulerUPG");
                //;
            PreparedStatement statement =
                conn.prepareStatement("SELECT IMAGE FROM PER_IMAGES WHERE TABLE_NAME='PER_PEOPLE_F' AND PARENT_ID = ?");
            statement.setString(1, personId);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {

                Blob blob = rs.getBlob("IMAGE");
                BufferedInputStream in = new BufferedInputStream(blob.getBinaryStream());
                int b;
                byte[] buffer = new byte[10240];
                while ((b = in.read(buffer, 0, 10240)) != -1) {
                    os.write(buffer, 0, b);
                }
                os.close();
            } else {

                ServletContext servletCtx = request.getServletContext();
                InputStream is = servletCtx.getResourceAsStream("/resources/images/avatar.jpg");
                BufferedInputStream in = new BufferedInputStream(is);
                int b;
                byte[] buffer = new byte[10240];
                while ((b = in.read(buffer, 0, 10240)) != -1) {
                    os.write(buffer, 0, b);
                }
                os.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
        }
    }

    private Connection getDataSourceConnection(String dataSourceName) throws Exception {
        Context ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup(dataSourceName);
        return ds.getConnection();
    }
}

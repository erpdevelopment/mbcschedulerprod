package ws;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

@Path("thanks/card")
public class ThanksCard {
    public ThanksCard() {
        super();
    }


    @POST
    @Produces("application/json")
    @Path("/getCards")
    public Response getCards(@Context SecurityContext sc, @HeaderParam("cardType") String cardType,
                                       @HeaderParam("personId") String personId
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select papf.person_id , papf.employee_number ,  papf.first_name ||' '|| papf.last_name  full_name , \n" + 
            "(select mbc_mobile_pkg.BASE64ENCODE( image ) from  per_images where parent_id= papf.person_id) img \n" + 
            ", 'class'||(MOD(LENGTH(TRIM(GLOBAL_NAME)),4)+1) CLASS_NAME , (select meaning  from fnd_lookup_values  where LOOKUP_TYPE = 'MBC_THANK_YOU_SUBJECTS' AND LANGUAGE = 'US' and lookup_code=mtuc.subject_id) subject\n" + 
            " ,DECODE (TRIM(GLOBAL_NAME) , 'SYSADMIN' , 'SYS' , SUBSTR (TRIM(REPLACE(GLOBAL_NAME,' ','')) , INSTR(TRIM(REPLACE(GLOBAL_NAME,' ','')), ',' )+1,1))|| UPPER(SUBSTR (TRIM(GLOBAL_NAME),1,1) )INTIALS" +
            ",mtuc.comments ,TO_CHAR (mtuc.CREATION_DATE , 'DD-MON-RRRR') card_date , MBC_MOBILE_PKG.CKECK_GIVEN_THANKS_CARD (:1) adding_state \n" + 
            "from  per_all_people_f papf , MBC_THANK_YOU_CARDS mtuc\n" + 
            "where papf.person_id= decode ( upper(:2) , 'GIVEN' , mtuc.to_person_id , 'RECEIVED' , mtuc.from_person_id , null)\n" + 
            "and sysdate between papf.effective_start_date and papf.effective_end_date\n" + 
            "and papf.current_employee_flag='Y'\n" + 
            "and to_char (mtuc.creation_date,'rrrr')  = to_char (sysdate,'rrrr')\n" + 
            "and decode ( upper(:3) , 'GIVEN' , mtuc.from_person_id , 'RECEIVED' , mtuc.to_person_id , null)=:4 order by mtuc.card_id desc  ";

            
        
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);

                statement.setString(1, personId);
                statement.setString(2, cardType);
                statement.setString(3, cardType);
                statement.setString(4, personId);
            
            
                
                
             
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    @POST
    @Produces("application/json")
    @Path("/getLeaderBoard")
    public Response getLeaderBoard(@Context SecurityContext sc )
    {
        
            
                if (!sc.isUserInRole("Authenticated"))
                throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt0="select papf.person_id , papf.employee_number ,  papf.first_name ||' '|| papf.last_name  full_name , (select mbc_mobile_pkg.BASE64ENCODE(  image ) from  per_images where parent_id= papf.person_id) img \n" + 
            ",count(mtuc.card_id) thanks_count , 'class'||(MOD(LENGTH(TRIM(GLOBAL_NAME)),4)+1) CLASS_NAME\n" + 
            " ,DECODE (TRIM(GLOBAL_NAME) , 'SYSADMIN' , 'SYS' , SUBSTR (TRIM(REPLACE(GLOBAL_NAME,' ','')) , INSTR(TRIM(REPLACE(GLOBAL_NAME,' ','')), ',' )+1,1))|| UPPER(SUBSTR (TRIM(GLOBAL_NAME),1,1) ) INTIALS\n" + 
            "from  per_all_people_f papf , MBC_THANK_YOU_CARDS mtuc\n" + 
            "where papf.person_id=mtuc.to_person_id\n" + 
            "and sysdate between papf.effective_start_date and papf.effective_end_date\n" + 
            "and papf.current_employee_flag='Y'\n" + 
            "and to_char (mtuc.creation_date,'rrrr')  = to_char (sysdate,'rrrr') \n" + 
            "group by papf.person_id , papf.employee_number , papf.first_name ,papf.last_name , GLOBAL_NAME\n" + 
            "order by 5 DESC";

            String sqlStemt="select * from \n" + 
            "(\n" + 
            "select papf.person_id , papf.employee_number ,  papf.first_name ||' '|| papf.last_name  full_name , (select mbc_mobile_pkg.BASE64ENCODE(  image ) from  per_images where parent_id= papf.person_id) img \n" + 
            ", thanks_count , 'class'||(MOD(LENGTH(TRIM(GLOBAL_NAME)),4)+1) CLASS_NAME\n" + 
            " ,DECODE (TRIM(GLOBAL_NAME) , 'SYSADMIN' , 'SYS' , SUBSTR (TRIM(REPLACE(GLOBAL_NAME,' ','')) , INSTR(TRIM(REPLACE(GLOBAL_NAME,' ','')), ',' )+1,1))|| UPPER(SUBSTR (TRIM(GLOBAL_NAME),1,1) ) INTIALS\n" + 
            " , count (papf.person_id ) over (partition by null order by thanks_count desc  ,  papf.first_name  , papf.person_id rows between unbounded PRECEDING and  current row  ) row_seq\n" + 
            "from  per_all_people_f papf , (select count (card_id) thanks_count, to_person_id from MBC_THANK_YOU_CARDS where  to_char (creation_date,'rrrr')  = to_char (sysdate,'rrrr') group by to_person_id  )  mtuc\n" + 
            "where papf.person_id=mtuc.to_person_id\n" + 
            "and sysdate between papf.effective_start_date and papf.effective_end_date\n" + 
            "and papf.current_employee_flag='Y'\n" + 
            ")\n" + 
            "where row_seq between 0 and 50";
        
            try {
      
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
    
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    @POST
    @Produces("application/json")
    @Path("/getSubjectTypes")
    public Response getSubjectTypes(@Context SecurityContext sc )
    {
        
            
                if (!sc.isUserInRole("Authenticated"))
                throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select meaning , lookup_code  \n" + 
            "from fnd_lookup_values  \n" + 
            "where LOOKUP_TYPE = 'MBC_THANK_YOU_SUBJECTS' \n" + 
            "AND LANGUAGE = 'US'";

            
        
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);

       
            
                
                
             
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    @POST
    @Produces("application/json")
    @Path("/checkAdd")
    public Response getCards(@Context SecurityContext sc, 
                                       @HeaderParam("personId") String personId
                                      
    
                                       )
    {
        
            


            
        
    if (!sc.isUserInRole("Authenticated"))  
    throw new SecurityException("User is unauthorized.");
    Response response=null;
    Connection connection = null;
    CallableStatement cs=null;
    String result = null;
    

    

    String block="DECLARE \n" + 
    "V_CHECK VARCHAR2 (500);\n" + 
    "BEGIN\n" + 
    "V_CHECK :=MBC_MOBILE_PKG.CKECK_GIVEN_THANKS_CARD(:1);\n" + 
    ":2 := V_CHECK;\n" + 
    "END;";
   
    try {
        

        
        connection = Helper.getOracleConnection();
         cs=  connection.prepareCall(block);
        cs.setString(1,personId );
   cs.registerOutParameter(2,Types.VARCHAR);

        
 

        cs.executeUpdate();
       
  result  =cs.getString(2);
        if (!"success".equalsIgnoreCase(result))
        {
            response = Helper.StringtoReponse(result, "2");
        }
        else
        {
                response = Helper.StringtoReponse("Success" , "1");
            }
        
    
    }
    catch(Exception e)
    {
        e.printStackTrace();
            response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
        }
    finally {
                try {
                    if ((connection != null) && !(connection.isClosed()))
                        connection.close();
                    if ((cs != null) && !(cs.isClosed()))
                                    cs.close();
                } catch (SQLException sqle) {
                    // TODO: Add catch code
                    sqle.printStackTrace();
                }
            }
    
      return response; 
    }  
    
    
    @POST
    @Produces("application/json")
    @Path("newCard")
    public Response getCards(@Context SecurityContext sc, @HeaderParam("fromPersonId") String fromPersonId,
                                       @HeaderParam("toPersonId") String toPersonId,
                                       @HeaderParam("subjectId") String subjectId,
                                       @HeaderParam("comments") String comments,
                                        @HeaderParam("userId") String userId
    
                                       )
    {
        
            


            
        
    if (!sc.isUserInRole("Authenticated"))  
    throw new SecurityException("User is unauthorized.");
    Response response=null;
    Connection connection = null;
    CallableStatement cs=null;
    

    

    String block="DECLARE\n" + 
    "BEGIN\n" + 
    "  apps.mbc_mobile_pkg.insert_card(\n" + 
    "    p_from_person_id=> :1,\n" + 
    "    p_to_person_id=> :2 ,\n" + 
    "    p_subject_id=> :3 ,\n" + 
    "    p_comments=> :4,\n" + 
    "    p_user_id=> :5 );\n" + 
    "    commit;\n" + 
    "END; ";
    
    try {
        

        
        connection = Helper.getOracleConnection();

        
        if (comments!=null)
            comments= URLDecoder.decode(comments);
    
        
            
    
         cs=  connection.prepareCall(block);
     //   System.out.println("val is "+p_ATTRIBUTE2);
    //     System.out.println("sql is \n"+block);
        cs.setString(1,fromPersonId );
        cs.setString(2,toPersonId );
        cs.setString(3,subjectId );
        cs.setString(4,comments );
        cs.setString(5,userId );

        
    

        cs.executeUpdate();
       

        
    response = Helper.StringtoReponse("Success" , "1");
    }
    catch(Exception e)
    {
        e.printStackTrace();
            response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
        }
    finally {
                try {
                    if ((connection != null) && !(connection.isClosed()))
                        connection.close();
                    if ((cs != null) && !(cs.isClosed()))
                                    cs.close();
                } catch (SQLException sqle) {
                    // TODO: Add catch code
                    sqle.printStackTrace();
                }
            }
    
      return response; 
    }  
    
    @POST
    @Produces("application/json")
    @Path("/getRemaingCards")
    public Response getRemaingCards(@Context SecurityContext sc,
                                       @HeaderParam("personId") String personId
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select  MBC_MOBILE_PKG.GET_MAX_THANKS_CONST()- COUNT(*) ||' Cards Left' remaing_card_text\n" + 
            "from  MBC_THANK_YOU_CARDS mtuc\n" + 
            "where mtuc.from_person_id=? \n" + 
            "and to_char (mtuc.creation_date,'rrrr')  = to_char (sysdate,'rrrr')";

            
        
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);

                statement.setString(1, personId);

            
            
                
                
             
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
   // //@GET
    @Produces("application/json")
    @Path("/getImage")
    public Response getImage(@Context SecurityContext sc , @QueryParam ("personId") String personId)
    {
        
//            if (!sc.isUserInRole("Authenticated"))  
//            throw new SecurityException("User is unauthorized.");
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select mbc_mobile_pkg.BASE64ENCODE( image ) image from  per_images\n" + 
            " where parent_id= :1";
           
       
            try {

                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
              
                statement.setString(1,personId );
               
             rs = statement.executeQuery();

               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
}

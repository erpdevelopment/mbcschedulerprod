

CREATE OR REPLACE PACKAGE BODY APPS.AS_SCDLR_WF_PKG
AS
 PROCEDURE XXSCDLR_RUN_PATTERN_PROCESS(ORG_ID IN  NUMBER ,FROM_ROLE VARCHAR2)
 is
 v_itemtype varchar2(1000);
v_itemkey varchar2(1000);
v_process varchar2(1000);
v_org_admin varchar2(1000);
v_erp_admin_role varchar2(1000);
  lv_body VARCHAR2(32767);
begin
v_itemtype:='MBCSCDLR';
v_itemkey:='PATTERN'||PATTERN_WORKFLOW_SEQ.nextval;

v_process:='PATTERNPROCESS';
  -- Create Process
  wf_engine.createprocess (v_itemtype, v_itemkey, v_process);
  --- Set Attributes 
--set owner
     wf_engine.setitemowner (itemtype      => v_itemtype,
                              itemkey       => v_itemkey,
                              owner         => 'SYSADMIN'
                             );
                             -- set from role for Notification 1 (Notification send from Scheduler Admin to Org Admin)
       wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#FROM_ROLE',
                                   avalue        => FROM_ROLE);
                                   
                                   --- set Performer for notification1
                                   
                                   SELECT USER_NAME     into v_org_admin
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=ORG_ID    
                       and role_name='OGADMIN'
                       and rownum=1 
                       order by user_name desc ;
                       
             
       wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'PTN_MANAGER',
                                   avalue        => v_org_admin);

                                    wf_engine.setitemattrnumber (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#ORG_ID',
                                   avalue        => ORG_ID);
                                   ----- Update un Approved Patterns to Pending Approval Status 
                                              update as_pattern_hdrs 
                                        set status='Pending Approval'
                                        where fk_organization_id=ORG_ID
                                        and status!='Approved';
              
  ------ set Html Body ----
  
 WF_ENGINE.SetItemAttrText (ITEMTYPE =>v_itemtype,ITEMKEY => v_itemkey,ANAME  => 'PATTERN_BODY_DETAILS', AVALUE =>'PLSQLCLOB:AS_SCDLR_WF_PKG. XX_CREATE_HTML_PATTERN/'
                                                                                                                                                                                                                                     ||ORG_ID );
                                                                                                                   


   wf_engine.startprocess (v_itemtype, v_itemkey);
     COMMIT;
end ;

                                    PROCEDURE SET_ATTR_PATTERN_ERP_NOTIF (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   )
   
   
   IS 
    v_org_id Number;
   v_org_admin varchar2(1000);
   l_role varchar2(2000);
   l_role_display_name varchar2(2000);
   cursor erpadmins (vorg_id Number)
   is
     SELECT USER_NAME   
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=vorg_id    
                       and role_name='SYSADMIN'
                     --  and rownum=1 
                       order by user_name desc ;
   BEGIN 
   
         v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID'
                                        );
   
               SELECT USER_NAME     into v_org_admin
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=v_org_id    
                       and role_name='OGADMIN'
                       and rownum=1 
                       order by user_name desc ;
         wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => '#FROM_ROLE',
                                   avalue        => v_org_admin);
                                   
                                   ---create Adhoc to set perofrmer of erp admin notification 
                                   l_role_display_name:='MBC Scheduler Pattern  ERP Admin Approval';
                                   l_role:='ERPADMIN'||itemkey;
                                     apps.wf_directory.createadhocrole 
                                     
          (role_name   => l_role,
           role_display_name       => l_role_display_name, 
           notification_preference => 'MAILHTM2' );
           
           for i in erpadmins(v_org_id)
           loop
          wf_directory.AddUsersToAdHocRole(   role_name=>  l_role,
role_users  => i.USER_NAME
); 
           
           end loop;
      wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => 'PATTERNERPADMIN',
                                   avalue        => l_role);
                                   
                                   
                                     RESULT := 'COMPLETE:';     
   
   END ;
  PROCEDURE APRROVE_PATTERN_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
                                  IS
                                    l_result               VARCHAR2 (3000);
                                  v_org_id Number;
                                  BEGIN
                                        l_result :=
                 wf_notification.getattrtext (wf_engine.context_nid, 'RESULT');
                          v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID'
                                        );
                                       IF l_result = 'APPROVED'
      THEN

                                        --- update pattern  status  to Approved
                                        update as_pattern_hdrs 
                                        set status='Approved'
                                        where fk_organization_id=v_org_id;
                                        
                                         RESULTOUT:= 'APPROVED';
                                        END IF;
                               IF l_result = 'REJECTED'
                                   
                                   THEN
                                       update as_pattern_hdrs 
                                        set status='Rejected'
                                        where fk_organization_id=v_org_id and status!='Approved';
                                    RESULTOUT := 'REJECTED';
                                   
                                   END IF ;
                                   
                                  
                                  END;

PROCEDURE APRROVE_MANAGER_PATTERN_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
                                  IS
                                    l_result               VARCHAR2 (3000);
                                  v_org_id Number;
                                  BEGIN
                                        l_result :=
                 wf_notification.getattrtext (wf_engine.context_nid, 'RESULT');
                          v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID'
                                        );
                                       IF l_result = 'APPROVED'
      THEN

                        
                                        
                                         RESULTOUT:= 'APPROVED';
                                        END IF;
                               IF l_result = 'REJECTED'
                                   
                                   THEN
                                       update as_pattern_hdrs 
                                        set status='Rejected'
                                        where fk_organization_id=v_org_id and status!='Approved';
                                    RESULTOUT := 'REJECTED';
                                   
                                   END IF ;
                                   
                                  
                                  END;


 
     PROCEDURE XX_CREATE_HTML_PATTERN
(
org_id     IN              VARCHAR2,
DISPLAY_TYPE    IN              VARCHAR2,
DOCUMENT        IN OUT NOCOPY   VARCHAR2,
document_type   IN OUT NOCOPY   VARCHAR2
)
IS
lv_Body             VARCHAR2 (32767);
V_ITEMKEY          VARCHAR2(100);
v_test varchar2(100);
cursor p_patterns is 
select * from as_pattern_hdrs
where fk_organization_id=org_id and status='Pending Approval';
cursor p_pattern_lines(v_header_id Number) is
select * from  as_pattern_lines 
where pattern_id=v_header_id  order by order_seq ;


BEGIN
lv_Body:='';
lv_Body :=lv_body||lv_body||'<table border="1px"   style="margin-left:150px;   text-align:left; width:50%;  border-collapse: collapse;   font-family:Arial; font-size:larger; "  align="left"  >';
for i in p_patterns
loop
lv_Body :=lv_body||'<tr><td style= padding-left:100px;"width:100%;text-align:left;color:Black">'||i.pattern_name||'</td></tr>';
lv_Body :=lv_body||'<tr><td style="width:100%;text-align:center;color:Black; font-size:12px;"><table border="1px"   style="   text-align:center; width:100%;  border-collapse: collapse;   font-family:Arial; font-size:larger; "  align="left"  >'||'<tr><th style="width:30%; background-color:#353d4e; color:white">Order Sequence</th>'
||' <th style="width:35%;background-color:#353d4e; color:white; font-size:12px;">on Off Flag</th>'
||'<th style="width:35%;background-color:#353d4e; color:white; font-size:12px;">No Of Days</th></tr>';

for ii in p_pattern_lines(i.pattern_id)
loop
lv_Body :=lv_body||'<tr><td style="width:35%;text-align:center;color:Black">'||ii.order_seq||'</td>';
lv_Body :=lv_body||'<td style="width:35%;text-align:center;color:Black">'||ii.on_off_flag||'</td>';
lv_Body :=lv_body||'<td style="width:35%;text-align:center;color:Black">'||ii.no_of_days||'</td></tr>';
end loop;
lv_Body :=lv_body||'</table>' ;
lv_Body :=lv_body||'</td></tr>';

end loop;

lv_Body :=lv_body||'</table>' ;


 



document :=lv_Body;
document_type := 'text/html';

EXCEPTION
WHEN OTHERS
THEN
document := '<H4>Error " || SQLERRM || "</H4>';

END;

 PROCEDURE XXSCDLR_RUN_SHIFT_PROCESS(ORG_ID IN  NUMBER ,FROM_ROLE VARCHAR2)
 is
 v_itemtype varchar2(1000);
v_itemkey varchar2(1000);
v_process varchar2(1000);
v_org_admin varchar2(1000);
v_erp_admin_role varchar2(1000);
  lv_body VARCHAR2(32767);
begin
v_itemtype:='MBCSCDLR';
v_itemkey:='SHIFT'||SHIFT_WORKFLOW_SEQ.nextval;

v_process:='SHIFTPROCESS';
  -- Create Process
  wf_engine.createprocess (v_itemtype, v_itemkey, v_process);
  --- Set Attributes 
--set owner
     wf_engine.setitemowner (itemtype      => v_itemtype,
                              itemkey       => v_itemkey,
                              owner         => 'SYSADMIN'
                             );
                             -- set from role for Notification 1 (Notification send from Scheduler Admin to Org Admin)
       wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#FROM_ROLE',
                                   avalue        => FROM_ROLE);
                                   
                                   --- set Performer for notification1
                                   
                                   SELECT USER_NAME     into v_org_admin
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=ORG_ID    
                       and role_name='OGADMIN'
                       and rownum=1 
                       order by user_name desc ;
                       
             
       wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'SHIFT_MANAGER',
                                   avalue        => v_org_admin);

                                    wf_engine.setitemattrnumber (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#ORG_ID_SHIFT',
                                   avalue        => ORG_ID);
                                   ----- Update un Approved Patterns to Pending Approval Status 
                                              update as_shifts
                                        set status='Pending Approval'
                                        where fk_organization_id=ORG_ID
                                        and status!='Approved';
              
  ------ set Html Body ----
  
 WF_ENGINE.SetItemAttrText (ITEMTYPE =>v_itemtype,ITEMKEY => v_itemkey,ANAME  => 'SHIFT_BODY_DETAILS', AVALUE =>'PLSQLCLOB:AS_SCDLR_WF_PKG. XX_CREATE_HTML_SHIFT/'
                                                                                                                                                                                                                                     ||ORG_ID );
                                                                                                                   


   wf_engine.startprocess (v_itemtype, v_itemkey);
     COMMIT;
end ;

                                    PROCEDURE SET_ATTR_SHIFT_ERP_NOTIF (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   )
   
   
   IS 
    v_org_id Number;
   v_org_admin varchar2(1000);
   l_role varchar2(2000);
   l_role_display_name varchar2(2000);
   cursor erpadmins (vorg_id Number)
   is
     SELECT USER_NAME   
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=vorg_id    
                       and role_name='SYSADMIN'
                     --  and rownum=1 
                       order by user_name desc ;
   BEGIN 
   
         v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID_SHIFT'
                                        );
   
               SELECT USER_NAME     into v_org_admin
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=v_org_id    
                       and role_name='OGADMIN'
                       and rownum=1 
                       order by user_name desc ;
         wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => '#FROM_ROLE',
                                   avalue        => v_org_admin);
                                   
                                   ---create Adhoc to set perofrmer of erp admin notification 
                                   l_role_display_name:='MBC Scheduler Shift  ERP Admin Approval';
                                   l_role:='SHIFTERPADMIN'||itemkey;
                                     apps.wf_directory.createadhocrole 
          (role_name   => l_role,
           role_display_name       => l_role_display_name, 
           notification_preference => 'MAILHTM2' );
           
           for i in erpadmins(v_org_id)
           loop
          wf_directory.AddUsersToAdHocRole(   role_name=>  l_role,
role_users  => i.USER_NAME
); 
           
           end loop;
      wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => 'SHIFTERPADMIN',
                                   avalue        => l_role);
                                   
                                   
                                     RESULT := 'COMPLETE:';     
   
   END ;
    PROCEDURE APRROVE_MANAGER_SHIFT_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
                                  IS
                                    l_result               VARCHAR2 (3000);
                                  v_org_id Number;
                                  BEGIN
                                        l_result :=
                 wf_notification.getattrtext (wf_engine.context_nid, 'RESULT');
                      v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID_SHIFT'
                                        );
                                       IF l_result = 'APPROVED'
      THEN
    
                                         RESULTOUT:= 'APPROVED';
                                        END IF;
                                   IF l_result = 'REJECTED'
                                   
                                   THEN
                                   update as_shifts 
                                        set status='Rejected'
                                        where fk_organization_id=v_org_id and status !='Approved';
                                    RESULTOUT := 'REJECTED';
                                   
                                   END IF ;
                                   
                                  
                                  END;



  PROCEDURE APRROVE_SHIFT_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
                                  IS
                                    l_result               VARCHAR2 (3000);
                                  v_org_id Number;
                                  BEGIN
                                        l_result :=
                 wf_notification.getattrtext (wf_engine.context_nid, 'RESULT');
                         v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID_SHIFT'
                                        );
                                       IF l_result = 'APPROVED'
      THEN
 
                                        --- update pattern  status  to Approved
                                        update as_shifts 
                                        set status='Approved'
                                        where fk_organization_id=v_org_id;
                                        
                                         RESULTOUT:= 'APPROVED';
                                        END IF;
                                   IF l_result = 'REJECTED'
                                   
                                   THEN
                                   update as_shifts 
                                        set status='Rejected'
                                        where fk_organization_id=v_org_id and status !='Approved';
                                    RESULTOUT := 'REJECTED';
                                   
                                   END IF ;
                                   
                                  
                                  END;



 
     PROCEDURE XX_CREATE_HTML_SHIFT
(
org_id     IN              VARCHAR2,
DISPLAY_TYPE    IN              VARCHAR2,
DOCUMENT        IN OUT NOCOPY   VARCHAR2,
document_type   IN OUT NOCOPY   VARCHAR2
)
IS
lv_Body             VARCHAR2 (32767);
V_ITEMKEY          VARCHAR2(100);

cursor p_shifts is 
select shift_id,shift_name,shift_display_name from as_shifts
where fk_organization_id=org_id and status='Pending Approval';
cursor p_shifts_workgoups(v_header_id Number) is
select  wh.work_group_name ,ws.EMPLOYEES_COUNT_MAX,ws.EMPLOYEES_COUNT
 from  AS_SHIFT_WORK_GROUPS ws ,AS_WORK_GROUPS_HDR wh
where shift_id=v_header_id   and ws.work_group_id=wh.work_group_id ;


BEGIN
lv_Body:='';
lv_Body :=lv_body||lv_body||'<table border="1px"   style="margin-left:150px;   text-align:left; width:50%;  border-collapse: collapse;   font-family:Arial; font-size:larger; "  align="left"  >';
for i in p_shifts
loop
lv_Body :=lv_body||'<tr><td style= padding-left:100px;"width:100%;text-align:left;color:Black">'||i.shift_display_name||'-'||i.shift_name||'</td></tr>';
lv_Body :=lv_body||'<tr><td style="width:100%;text-align:center;color:Black; font-size:12px;"><table border="1px"   style="   text-align:center; width:100%;  border-collapse: collapse;   font-family:Arial; font-size:larger; "  align="left"  >'||'<tr><th style="width:30%; background-color:#353d4e; color:white">Order Sequence</th>'
||' <th style="width:35%;background-color:#353d4e; color:white; font-size:12px;">on Off Flag</th>'
||'<th style="width:35%;background-color:#353d4e; color:white; font-size:12px;">No Of Days</th></tr>';

for ii in  p_shifts_workgoups(i.shift_id)
loop
lv_Body :=lv_body||'<tr><td style="width:35%;text-align:center;color:Black">'||ii.work_group_name||'</td>';
lv_Body :=lv_body||'<td style="width:35%;text-align:center;color:Black">'||ii.EMPLOYEES_COUNT||'</td>';
lv_Body :=lv_body||'<td style="width:35%;text-align:center;color:Black">'||ii.EMPLOYEES_COUNT_MAX||'</td></tr>';
end loop;
lv_Body :=lv_body||'</table>' ;
lv_Body :=lv_body||'</td></tr>';

end loop;

lv_Body :=lv_body||'</table>' ;


 



document :=lv_Body;
document_type := 'text/html';

EXCEPTION
WHEN OTHERS
THEN
document := '<H4>Error " || SQLERRM || "</H4>';

END;


 PROCEDURE XXSCDLR_RUN_BULLETIN_PROCESS(ORG_ID IN  NUMBER ,FROM_ROLE VARCHAR2)
 is
 v_itemtype varchar2(1000);
v_itemkey varchar2(1000);
v_process varchar2(1000);
v_org_admin varchar2(1000);
v_erp_admin_role varchar2(1000);
  lv_body VARCHAR2(32767);
begin
v_itemtype:='MBCSCDLR';
v_itemkey:='BULLETIN'||BULLETIN_WORKFLOW_SEQ.nextval;

v_process:='BULLETINPROCESS';
  -- Create Process
  wf_engine.createprocess (v_itemtype, v_itemkey, v_process);
  --- Set Attributes 
--set owner
     wf_engine.setitemowner (itemtype      => v_itemtype,
                              itemkey       => v_itemkey,
                              owner         => 'SYSADMIN'
                             );
                             -- set from role for Notification 1 (Notification send from Scheduler Admin to Org Admin)
       wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#FROM_ROLE',
                                   avalue        => FROM_ROLE);
                                   
                                   --- set Performer for notification1
                                   
                                   SELECT USER_NAME     into v_org_admin
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=ORG_ID    
                       and role_name='OGADMIN'
                       and rownum=1 
                       order by user_name desc ;
                       
             
       wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'BULLETIN_MANAGER',
                                   avalue        => v_org_admin);

                                    wf_engine.setitemattrnumber (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#ORG_ID_BULLETIN',
                                   avalue        => ORG_ID);
                                   ----- Update un Approved Patterns to Pending Approval Status 
                                              update as_bulletins
                                        set status='Pending Approval'
                                        where fk_organization_id=ORG_ID
                                        and status!='Approved';
              
  ------ set Html Body ----
  
 WF_ENGINE.SetItemAttrText (ITEMTYPE =>v_itemtype,ITEMKEY => v_itemkey,ANAME  => 'BULLETIN_BODY_DETAILS', AVALUE =>'PLSQLCLOB:AS_SCDLR_WF_PKG. XX_CREATE_HTML_BULLETIN/'
                                                                                                                                                                                                                                     ||ORG_ID );
                                                                                                                   


   wf_engine.startprocess (v_itemtype, v_itemkey);
     COMMIT;
end ;

                                    PROCEDURE SET_ATTR_BULLETIN_ERP_NOTIF (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   )
   
   
   IS 
    v_org_id Number;
   v_org_admin varchar2(1000);
   l_role varchar2(2000);
   l_role_display_name varchar2(2000);
   cursor erpadmins (vorg_id Number)
   is
     SELECT USER_NAME   
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=vorg_id    
                       and role_name='SYSADMIN'
                     --  and rownum=1 
                       order by user_name desc ;
   BEGIN 
   
         v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID_BULLETIN'
                                        );
   
               SELECT USER_NAME     into v_org_admin
  FROM AS_USER_ROLES AsUserRolesEO, PER_ALL_PEOPLE_F PAPF    ,FND_USER FU        
 WHERE AsUserRolesEO.PERSON_ID = PAPF.PERSON_ID            
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE         
                       and FU.EMPLOYEE_ID=PAPF.PERSON_ID
                       and fk_organization_id=v_org_id    
                       and role_name='OGADMIN'
                       and rownum=1 
                       order by user_name desc ;
         wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => '#FROM_ROLE',
                                   avalue        => v_org_admin);
                                   
                                   ---create Adhoc to set perofrmer of erp admin notification 
                                   l_role_display_name:='MBC Scheduler Bulletin  ERP Admin Approval';
                                   l_role:='BULLETINERPADMIN'||itemkey;
                                     apps.wf_directory.createadhocrole 
          (role_name   => l_role,
           role_display_name       => l_role_display_name, 
           notification_preference => 'MAILHTM2' );
           
           for i in erpadmins(v_org_id)
           loop
          wf_directory.AddUsersToAdHocRole(   role_name=>  l_role,
role_users  => i.USER_NAME
); 
           
           end loop;
      wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => 'BULLETINERPADMIN',
                                   avalue        => l_role);
                                   
                                   
                                     RESULT := 'COMPLETE:';     
   
   END ;
    PROCEDURE  APRROVE_MANAGER_BLTIN_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
                                  IS
                                    l_result               VARCHAR2 (3000);
                                  v_org_id Number;
                                  BEGIN
                                        l_result :=
                 wf_notification.getattrtext (wf_engine.context_nid, 'RESULT');
                      v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID_BULLETIN'
                                        );
                                       IF l_result = 'APPROVED'
      THEN
    
                                         RESULTOUT:= 'APPROVED';
                                        END IF;
                                   IF l_result = 'REJECTED'
                                   
                                   THEN
                                   update as_bulletins 
                                        set status='Rejected'
                                        where fk_organization_id=v_org_id and status !='Approved';
                                    RESULTOUT := 'REJECTED';
                                   
                                   END IF ;
                                   
                                  
                                  END;



  PROCEDURE APRROVE_BULLETIN_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
                                  IS
                                    l_result               VARCHAR2 (3000);
                                  v_org_id Number;
                                  BEGIN
                                        l_result :=
                 wf_notification.getattrtext (wf_engine.context_nid, 'RESULT');
                         v_org_id :=
            wf_engine.getitemattrnumber (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => '#ORG_ID_BULLETIN'
                                        );
                                       IF l_result = 'APPROVED'
      THEN
 
                                        --- update pattern  status  to Approved
                                        update as_bulletins 
                                        set status='Approved'
                                        where fk_organization_id=v_org_id;
                                        
                                         RESULTOUT:= 'APPROVED';
                                        END IF;
                                   IF l_result = 'REJECTED'
                                   
                                   THEN
                                   update as_bulletins
                                        set status='Rejected'
                                        where fk_organization_id=v_org_id and status !='Approved';
                                    RESULTOUT := 'REJECTED';
                                   
                                   END IF ;
                                   
                                  
                                  END;



 
     PROCEDURE XX_CREATE_HTML_BULLETIN
(
org_id     IN              VARCHAR2,
DISPLAY_TYPE    IN              VARCHAR2,
DOCUMENT        IN OUT NOCOPY   VARCHAR2,
document_type   IN OUT NOCOPY   VARCHAR2
)
IS
lv_Body             VARCHAR2 (32767);
V_ITEMKEY          VARCHAR2(100);

cursor p_bulletins is 
select bulletin_name ,effective_start_date,effective_end_date , to_char(onair_start_time ,'HH24')||':'|| to_char(onair_start_time ,'MI') start_time, to_char(onair_end_time ,'HH24')||':'|| to_char(onair_end_time ,'MI') End_time  from as_bulletins
where fk_organization_id=org_id and status='Pending Approval';



BEGIN
lv_Body:='';
lv_Body :=lv_body||lv_body||'<table border="1px"   style="margin-left:150px;   text-align:left; width:50%;  border-collapse: collapse;   font-family:Arial; font-size:larger; "  align="left"  >';
lv_Body :=lv_body||'<tr><th style="width:30%; background-color:#353d4e; color:white">Bulletin Name</th>'
||' <th style="width:20%;background-color:#353d4e; color:white; font-size:12px;"> Effective Start Date</th>'
||'<th style="width:20%;background-color:#353d4e; color:white; font-size:12px;">Effective End Date</th><th style="width:15%;background-color:#353d4e; color:white; font-size:12px;">On air Start Time</th><th style="width:15%;background-color:#353d4e; color:white; font-size:12px;">On air End time</th></tr>';

for i in p_bulletins
loop

lv_Body :=lv_body||'<tr><td style="width:30%;text-align:center;color:Black">'||i.Bulletin_name||'</td>';
lv_Body :=lv_body||'<td style="width:20%;text-align:center;color:Black">'||i.Effective_start_date||'</td>';
lv_Body :=lv_body||'<td style="width:20%;text-align:center;color:Black">'||i.Effective_End_date||'</td>';
lv_Body :=lv_body||'<td style="width:15%;text-align:center;color:Black">'||i.start_time ||'</td>';
lv_Body :=lv_body||'<td style="width:15%;text-align:center;color:Black">'||i.end_time ||'</td></tr>';
--lv_Body :=lv_body||'</table>' ;
--lv_Body :=lv_body||'</td></tr>';

end loop;

lv_Body :=lv_body||'</table>' ;


 



document :=lv_Body;
document_type := 'text/html';

EXCEPTION
WHEN OTHERS
THEN
document := '<H4>Error " || SQLERRM || "</H4>';

END;


 END ;
/

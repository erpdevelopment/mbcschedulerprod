function loadPictoChartData() {
    var today = new Date();
    var yyyy = today.getFullYear();
    var mm = today.getMonth() + 1;//January is 0!
    if(mm<10){
        mm='0'+mm;
    }
    
    var currPersonId = document.getElementById('CurrPersonId');
    console.log('test = '+currPersonId.value);
    var url = "http://10.10.131.26:7003/schedulerRest/rest/AASchedules1.0/HomePictoChartVO?limit=100&finder=RowFinder;PersonIdPar="
                +currPersonId.value+",MonthDatePar=01-" + mm + "-" + yyyy;
    var monthDays = [];
    var jsonItems;

    $.ajaxSetup( {
        async : false
    });

    $.getJSON(url, function (jsonResult) {
        var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });

        $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            monthDays.push(jsonItemAttrs.DayStatus);
        });

    });

    $.ajaxSetup( {
        async : true
    });
    return monthDays;
}

requirejs.config( {
    // Path mappings for the logical module names
    waitSeconds : 20, 
    //baseUrl: 'resources/oraclejet/js',
    paths :  {
        'knockout' : 'libs/knockout/knockout-3.4.0', 'jquery' : 'libs/jquery/jquery-3.1.0.min', 'jqueryui-amd' : 'libs/jquery/jqueryui-amd-1.12.0.min', 'ojs' : 'libs/oj/v2.3.0/min', 'ojL10n' : 'libs/oj/v2.3.0/ojL10n', 'ojtranslations' : 'libs/oj/v2.3.0/resources', 'text' : 'libs/require/text', 'promise' : 'libs/es6-promise/es6-promise.min', 'hammerjs' : 'libs/hammer/hammer-2.0.8.min', 'signals' : 'libs/js-signals/signals.min', 'ojdnd' : 'libs/dnd-polyfill/dnd-polyfill-1.0.0.min', 'css' : 'libs/require-css/css.min', 'customElements' : 'libs/webcomponents/CustomElements.min', 'proj4' : 'libs/proj4js/dist/proj4'
    },
    // Shim configurations for modules that do not expose AMD
    shim :  {
        'jquery' :  {
            exports : ['jQuery', '$']
        },
        'maps' :  {
            deps : ['jquery', 'i18n'], exports : ['MVMapView']
        }
    },
    // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
    // resources with a custom translation file.
    // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
    // a path that is relative to the location of this main.js file.
    config :  {
        ojL10n :  {
            merge :  {
                //'ojtranslations/nls/ojtranslations': 'resources/nls/menu'
            }
        }
    }
    //,waitSeconds: 15
});

$(document).ready(function () {
    require(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojpictochart', 'ojs/ojlegend'], function (oj, ko, $) {

        function PictoChartModel() {
            var colorHandler = new oj.ColorAttributeGroupHandler();
            var legendNames = ["On", "Off"];
            var legendItems = [];
            var colors = ["#a2bf39", "#fad55c"];

            legendItems.push( {
                text : legendNames[0], color : "#" + colors[0]
            });
            legendItems.push( {
                text : legendNames[1], color : "#" + colors[1]
            });

            var getPictoItems = function () {
                var pictoItems = [];
                var values = loadPictoChartData();
                for (var i = 0;i < values.length;i++) {
                    var val = values[i];
                    var legendIndex = legendNames.indexOf(val);
                    pictoItems.push( {
                        name : "Day " + (i + 1) + " is " + val, color : colors[legendIndex]
                    });
                }
                return pictoItems;
            }

            this.currMonthItems = ko.observableArray(getPictoItems());
            this.legendSections = ko.observableArray([{items : legendItems}]);

            this.tooltipFunction = function (dataContext) {
                return dataContext.name;
            }
        }

        var pictoChartModel = new PictoChartModel();
        ko.applyBindings(pictoChartModel, document.getElementById('homeCal-container'));

    });
});
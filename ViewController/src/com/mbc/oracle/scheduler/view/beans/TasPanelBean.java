package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;
public class TasPanelBean {
    private RichPopup tasPop;

    public TasPanelBean() {
    }

    public void setTasPop(RichPopup tasPop) {
        this.tasPop = tasPop;
    }

    public RichPopup getTasPop() {
        return tasPop;
    }

    public void onClickEmployeeImageLink(ActionEvent actionEvent) {
        // Add event code here...
        UIComponent comp = actionEvent.getComponent();
        Number personId = (Number) comp.getAttributes().get("PersonId");
        System.out.println("personId="+personId);
        ViewObject asWorkGroupSchdlrTasDataVO =
            ADFUtils.findIterator("AsWorkGroupSchdlrTasDataVOIterator").getViewObject();
        asWorkGroupSchdlrTasDataVO.setWhereClause("PERSON_ID="+personId + " and TRXDATE = TO_DATE ('01-01-2017', 'dd-mm-rrrr')");
        asWorkGroupSchdlrTasDataVO.executeQuery();
        
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getTasPop().show(hints);
    }
}

DROP PACKAGE BODY APPS.AS_NTF_CONTENT_PKG;

CREATE OR REPLACE PACKAGE BODY APPS.AS_NTF_CONTENT_PKG
IS
   FUNCTION replace_clob (i_src CLOB, i_match VARCHAR2, i_rep VARCHAR2)
      RETURN CLOB
   IS
      MAX_VARCHAR2     NUMBER DEFAULT 15000; -- depending on your character set, 32767 may not be your actual number of available characters

      l_ret_clob       CLOB;                  -- temporary CLOB to be returned
      l_next_idx       NUMBER; -- used to traverse the source CLOB, captures the next instance of the match string
      l_search_begin   NUMBER DEFAULT 1; -- used to traverse the source CLOB, indicates the starting point of the next search
      l_parse_begin    NUMBER DEFAULT 1; -- used to copy date from the source CLOB, indicates the beginning of the copy window
      l_parse_end      NUMBER; -- used to copy date from the source CLOB, indicates the beginning of the end window
   BEGIN
      -- Create a temporary CLOB to be returned
      DBMS_LOB.CREATETEMPORARY (l_ret_clob, TRUE);

      LOOP
         l_next_idx :=
            DBMS_LOB.INSTR (i_src,
                            i_match,
                            l_search_begin,
                            1);

         -- Exit once we do not find another occurance of the match string
         EXIT WHEN l_next_idx = 0;

         -- Special handling if we find a match at the first character of the source string
         IF l_next_idx = 1
         THEN
            l_parse_begin := LENGTH (i_match) + 1;

            IF LENGTH (i_rep) > 0
            THEN
               DBMS_LOB.APPEND (l_ret_clob, i_rep);
            END IF;
         ELSE
            -- Copy source data up to the match
            l_parse_end := l_next_idx - 1;
            DBMS_LOB.COPY (l_ret_clob,
                           i_src,
                           (l_parse_end - l_parse_begin + 1),
                           DBMS_LOB.GETLENGTH (l_ret_clob) + 1,
                           l_parse_begin);

            -- Replace the match string
            IF LENGTH (i_rep) > 0
            THEN
               DBMS_LOB.APPEND (l_ret_clob, i_rep);
            END IF;

            -- Move the beginning of the next parse window past our match string
            l_parse_begin := l_parse_end + LENGTH (i_match) + 1;
         END IF;

         -- Move the beginning of the search window past our match string
         l_search_begin := l_next_idx + LENGTH (i_match);
      END LOOP;


      -- Special handling for the trailing final characters of the source CLOB
      IF DBMS_LOB.GETLENGTH (i_src) > (l_parse_end + LENGTH (i_match))
      THEN
         l_parse_begin := l_parse_end + LENGTH (i_match) + 1;
         l_parse_end := DBMS_LOB.GETLENGTH (i_src);
         DBMS_LOB.COPY (l_ret_clob,
                        i_src,
                        (l_parse_end - l_parse_begin + 1),
                        DBMS_LOB.GETLENGTH (l_ret_clob) + 1,
                        l_parse_begin);
      END IF;

      RETURN l_ret_clob;
   END replace_clob;

   FUNCTION BUILD_CAL_EVENT (
      P_SUMMARY           IN VARCHAR2,
      P_ORGANIZER_NAME    IN VARCHAR2 DEFAULT 'AA Schedules',
      P_ORGANIZER_EMAIL   IN VARCHAR2,
      P_START_DATE        IN DATE,
      P_END_DATE          IN DATE)
      RETURN VARCHAR2
   AS
      L_RETVAL   VARCHAR2 (32767);
      L_LF       CHAR (1) := CHR (10);
   BEGIN
      L_RETVAL :=
            'BEGIN:VCALENDAR'
         || L_LF
         || 'PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN'
         || L_LF
         || 'VERSION:2.0'
         || L_LF
         || 'METHOD:REQUEST'
         || L_LF
         || 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE'
         || L_LF
         --|| 'CALSCALE:GREGORIAN'|| L_LF
         || 'BEGIN:VTIMEZONE'
         || L_LF
         || 'TZID:Arabian Standard Time'
         || L_LF
         || 'BEGIN:STANDARD'
         || L_LF
         || 'DTSTART:16010101T000000'
         || L_LF
         || 'TZOFFSETFROM:+0400'
         || L_LF
         || 'TZOFFSETTO:+0400'
         || L_LF
         || 'END:STANDARD'
         || L_LF
         || 'END:VTIMEZONE'
         || L_LF
         || 'BEGIN:VEVENT'
         || L_LF
         || 'ATTENDEE;CN="'
         || P_ORGANIZER_EMAIL
         || '";RSVP=TRUE:mailto:'
         || P_ORGANIZER_EMAIL
         --|| 'ATTENDEE;CN="Mahmoud Elsayed";RSVP=TRUE:mailto:JASMINA.MUSIC@MBC.NET'
         || L_LF
         || 'CLASS:PUBLIC'
         || L_LF
         || 'CREATED:'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (SYSDATE, 'HH24MISS')
         || 'Z'
         || L_LF
         || 'DESCRIPTION:'
         || P_SUMMARY
         || ' \n\n'
         || L_LF
         --|| 'DTEND;TZID="Arabian Standard Time":20170410T153000'
         || 'DTEND;TZID=Arabian Standard Time:'
         || TO_CHAR (P_END_DATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (P_END_DATE, 'HH24MISS')
         || L_LF
         --|| 'DTSTAMP:20170410T062149Z'
         || 'DTSTAMP:'
         || TO_CHAR (P_START_DATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (P_START_DATE, 'HH24MISS')
         || 'Z'
         || L_LF
         --|| 'DTSTART;TZID="Arabian Standard Time":20170410T150000'
         || 'DTSTART;TZID=Arabian Standard Time:'
         || TO_CHAR (P_START_DATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (P_START_DATE, 'HH24MISS')
         || L_LF
         || 'LAST-MODIFIED:'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || 'T'
         || TO_CHAR (SYSDATE, 'HH24MISS')
         || 'Z'
         || L_LF
         || 'LOCATION:MBC Building'
         || L_LF
         || 'ORGANIZER;CN="Rami Abu Diab":mailto:rami.abudiab@mbc.net'
         || L_LF
         || 'PRIORITY:5'
         || L_LF
         || 'SEQUENCE:0'
         || L_LF
         || 'SUMMARY;LANGUAGE=en-us:'
         || P_SUMMARY
         || L_LF
         || 'TRANSP:OPAQUE'
         || L_LF
         || 'UID:'
         || RAWTOHEX (SYS_GUID ())
         || L_LF
         || 'X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE'
         || L_LF
         || 'X-MICROSOFT-CDO-IMPORTANCE:1'
         || L_LF
         || 'X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY'
         || L_LF
         || 'X-MICROSOFT-DISALLOW-COUNTER:FALSE'
         || L_LF
         || 'X-MS-OLK-CONFTYPE:0'
         || L_LF
         || 'BEGIN:VALARM'
         || L_LF
         || 'TRIGGER:-PT15M'
         || L_LF
         || 'REPEAT:1'
         || L_LF
         || 'ACTION:DISPLAY'
         || L_LF
         || 'DESCRIPTION:Reminder'
         || L_LF
         || 'END:VALARM'
         || L_LF
         || 'END:VEVENT'
         || L_LF
         || 'END:VCALENDAR';


      RETURN L_RETVAL;
   END BUILD_CAL_EVENT;

   PROCEDURE GET_SCHDLR_NTF (IN_SCHEDULER_ID          NUMBER,
                             IN_NOTIFICATION_ID       NUMBER,
                             IN_ACCESS_KEY            VARCHAR2,
                             OUT_NTF_SUBJECT      OUT VARCHAR2,
                             OUT_NTF_BODY         OUT CLOB)
   IS
      l_application_link        VARCHAR2 (1024);
      l_TEMPLATE_CONTENT        AS_NTF_TEMPLATES.TEMPLATE_CONTENT%TYPE;
      L_NXT_ACTION_HISTORY_ID   NUMBER;
      l_approver_name           VARCHAR2 (512 BYTE);
      l_shifts                  VARCHAR2 (32000);
      l_workgroups              VARCHAR2 (32000);

      CURSOR scheduler_cur
      IS
         SELECT SCHEDULER_ID,
                SCHEDULER_TITLE,
                START_DATE,
                END_DATE,
                VERSION_NUMBER,
                SCHEDULAR_STATUS,
                PUBLISH_FLAG,
                CHANNEL
           FROM as_schedulers
          WHERE SCHEDULER_ID = IN_SCHEDULER_ID;
   BEGIN
      FOR scheduler_rec IN scheduler_cur
      LOOP
         SELECT MIN (SCHEDULER_ACTION_HISTORY_ID)
           INTO L_NXT_ACTION_HISTORY_ID
           FROM AS_SCHEDULER_ACTION_HISTORY
          WHERE     TRANS_ID = scheduler_rec.SCHEDULER_ID
                AND TRANS_TYPE = 'SCHDLR'
                AND ACTION_TYPE_TAKEN IS NULL;

         SELECT papf.full_name
           INTO l_approver_name
           FROM AS_SCHEDULER_ACTION_HISTORY hst, per_all_people_f papf
          WHERE hst.PERSON_ID = papf.PERSON_ID
                AND SYSDATE BETWEEN papf.effective_start_date
                                AND papf.effective_end_date
                AND SCHEDULER_ACTION_HISTORY_ID = L_NXT_ACTION_HISTORY_ID;

         l_application_link :=
               'http://10.10.131.26:7003/scheduler/faces/notifications?ntfid='
            || IN_NOTIFICATION_ID
            || '&accesskey='
            || IN_ACCESS_KEY;


         OUT_NTF_SUBJECT :=
               'AA Schedules : Schedules '
            || scheduler_rec.SCHEDULER_TITLE
            || ' requires your approval.';

         SELECT TEMPLATE_CONTENT
           INTO l_TEMPLATE_CONTENT
           FROM AS_NTF_TEMPLATES
          WHERE TEMPLATE_KEY = 'SCHEDULER_APPROVER';

         l_TEMPLATE_CONTENT :=
            replace_Clob (l_TEMPLATE_CONTENT, '{1}', l_approver_name);

         l_TEMPLATE_CONTENT :=
            replace_Clob (l_TEMPLATE_CONTENT,
                          '{2}',
                          scheduler_rec.SCHEDULER_TITLE);

         l_TEMPLATE_CONTENT :=
            replace_Clob (l_TEMPLATE_CONTENT, '{3}', scheduler_rec.CHANNEL);



         l_TEMPLATE_CONTENT :=
            replace_Clob (l_TEMPLATE_CONTENT,
                          '{4}',
                          TO_CHAR (scheduler_rec.START_DATE, 'dd-MON-rrrr'));

         l_TEMPLATE_CONTENT :=
            replace_Clob (l_TEMPLATE_CONTENT,
                          '{5}',
                          TO_CHAR (scheduler_rec.END_DATE, 'dd-MON-rrrr'));

         FOR shft_rec
            IN (SELECT DISTINCT shft.shift_name
                  FROM AS_SCHEDULER_SHIFTS ass, as_shifts shft
                 WHERE ass.shift_id = shft.shift_id
                       AND ass.SCHEDULER_ID = in_SCHEDULER_ID)
         LOOP
            l_shifts := l_shifts || ',' || shft_rec.shift_name;
         END LOOP;

         l_TEMPLATE_CONTENT :=
            replace_Clob (l_TEMPLATE_CONTENT, '{6}', l_shifts);

         FOR shft_rec
            IN (SELECT DISTINCT wrkgrp.WORK_GROUP_NAME
                  FROM AS_SCHEDULER_SHIFTS ass, AS_WORK_GROUPS_HDR wrkgrp
                 WHERE ass.work_group_id = wrkgrp.work_group_id
                       AND ass.SCHEDULER_ID = in_SCHEDULER_ID)
         LOOP
            l_workgroups := l_workgroups || ',' || shft_rec.WORK_GROUP_NAME;
         END LOOP;

         l_TEMPLATE_CONTENT :=
            replace_Clob (l_TEMPLATE_CONTENT, '{7}', l_workgroups);

         l_TEMPLATE_CONTENT :=
            replace_Clob (l_TEMPLATE_CONTENT, '{8}', l_application_link);
      END LOOP;

      OUT_NTF_BODY := l_TEMPLATE_CONTENT;
   END GET_SCHDLR_NTF;

   PROCEDURE GET_SCHDLR_CAL (IN_SCHEDULER_ID             NUMBER,
                             in_SCHEDULER_STAFF_ID       NUMBER,
                             OUT_CAL_SUBJECT         OUT VARCHAR2,
                             OUT_CAL_CONTENT         OUT CLOB)
   IS
      l_event_data   VARCHAR2 (32000);
      l_start_date   DATE;
      l_end_date     DATE;
      L_LF           CHAR (1) := CHR (10);

      CURSOR STAFF_SCHDLR_CUR
      IS
         SELECT ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID,
                ASSCHEDULERSTAFFEO.SCHEDULER_ID,
                ASSCHEDULERSTAFFEO.PERSON_ID,
                ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE,
                ASSCHEDULERSTAFFEO.SCHEDULAR_END_DATE,
                ASSCHEDULERSTAFFEO.START_TIME_HOURS,
                ASSCHEDULERSTAFFEO.START_TIME_MINUTES,
                ASSCHEDULERSTAFFEO.END_TIME_HOURS,
                ASSCHEDULERSTAFFEO.END_TIME_MINUTES,
                ASSCHEDULERSTAFFEO.SHIFT_ID,
                ASSCHEDULERSTAFFEO.WORK_GROUP_ID,
                ASSCHEDULERSTAFFEO.RECURRING_FLAG,
                ASSCHEDULERSTAFFEO.BULLETIN_FLAG,
                ASSCHEDULERSTAFFEO.PARENT_SCHEDULER_STAFF_ID,
                PAPF.EMPLOYEE_NUMBER,
                PAPF.FULL_NAME EMPLOYEE_NAME,
                PAPF.EMAIL_ADDRESS,
                ASS.SHIFT_NAME,
                AWGH.WORK_GROUP_NAME,
                SCHDLR.SCHEDULER_TITLE
           FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,
                PER_ALL_PEOPLE_F PAPF,
                AS_SCHEDULERS SCHDLR,
                AS_SHIFTS ASS,
                AS_WORK_GROUPS_HDR AWGH
          WHERE ASSCHEDULERSTAFFEO.PERSON_ID = PAPF.PERSON_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = SCHDLR.SCHEDULER_ID
                AND ASSCHEDULERSTAFFEO.SHIFT_ID = ASS.SHIFT_ID(+)
                AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID = AWGH.WORK_GROUP_ID(+)
                AND ASSCHEDULERSTAFFEO.PARENT_SCHEDULER_STAFF_ID IS NULL
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = IN_SCHEDULER_ID
                AND ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID =
                       in_SCHEDULER_STAFF_ID;          --1442;-- IN_PERSON_ID;
   BEGIN
      FOR STAFF_SCHDLR_REC IN STAFF_SCHDLR_CUR
      LOOP
         OUT_CAL_SUBJECT :=
               STAFF_SCHDLR_REC.EMPLOYEE_NAME
            || ' in schedules '
            || STAFF_SCHDLR_REC.SCHEDULER_TITLE
            || ' ,Date '
            || TO_CHAR (STAFF_SCHDLR_REC.SCHEDULAR_START_DATE,
                        'DAY dd-mm-rrrr');

         l_start_date :=
            TO_DATE (
               TO_CHAR (STAFF_SCHDLR_REC.SCHEDULAR_START_DATE, 'dd-mm-rrrr')
               || LPAD (STAFF_SCHDLR_REC.START_TIME_HOURS, 2, '0')
               || ':'
               || LPAD (STAFF_SCHDLR_REC.START_TIME_MINUTES, 2, '0'),
               'dd-mm-rrrr hh24:mi');


         l_end_date :=
            TO_DATE (
                  TO_CHAR (STAFF_SCHDLR_REC.SCHEDULAR_END_DATE, 'dd-mm-rrrr')
               || LPAD (STAFF_SCHDLR_REC.END_TIME_HOURS, 2, '0')
               || ':'
               || LPAD (STAFF_SCHDLR_REC.END_TIME_MINUTES, 2, '0'),
               'dd-mm-rrrr hh24:mi');


         l_event_data :=
            BUILD_CAL_EVENT (P_SUMMARY           => OUT_CAL_SUBJECT,
                             P_ORGANIZER_NAME    => 'AA Schedules',
                             P_ORGANIZER_EMAIL   => 'JASMINA.MUSIC@MBC.NET',
                             P_START_DATE        => l_start_date,
                             P_END_DATE          => l_end_date);

         OUT_CAL_CONTENT :=
               'Content-class: urn:content-classes:calendarmessage'
            || L_LF
            || 'MIME-Version: 1.0'
            || L_LF
            || 'Content-Type: multipart/alternative;'
            || L_LF
            || ' boundary="----_=_NextPart"'
            || L_LF
            || 'Subject: '
            || OUT_CAL_SUBJECT
            || L_LF
            || 'Date: '
            || TO_CHAR (SYSDATE, 'DAY, DD-MON-RR HH24:MI')
            || L_LF
            || 'From:'
            || 'rami.abudiab@mbc.net'                           --P_FROM_EMAIL
            || L_LF
            || 'To: '
            || 'JASMINA.MUSIC@MBC.NET'                    --P_RECEIPTS_EMAIL
            || L_LF
            || '------_=_NextPart'
            || L_LF
            || 'Content-Type: text/html;'
            || L_LF
            || ' charset="windows-1256"'
            --|| l_lf|| 'Content-Transfer-Encoding: quoted-printable'
            || L_LF
            || L_LF
            || OUT_CAL_SUBJECT               -- Content of calendar P_NTF_BODY
            || L_LF
            || L_LF
            || '------_=_NextPart'
            || L_LF
            || 'Content-class: urn:content-classes:calendarmessage'
            || L_LF
            || 'Content-Type: text/calendar;'
            || L_LF
            || '  method=REQUEST;'
            || L_LF
            || '  name="meeting.ics"'
            --|| l_lf|| 'Content-Transfer-Encoding: 8bit'
            || L_LF
            || L_LF
            || l_event_data
            || L_LF
            || L_LF
            || '------_=_NextPart--';
      END LOOP;
   END GET_SCHDLR_CAL;

   PROCEDURE GET_TRANS_NTF (IN_SCHEDULER_TRANS_HDR_ID       NUMBER,
                            IN_NOTIFICATION_ID              NUMBER,
                            IN_ACCESS_KEY                   VARCHAR2,
                            OUT_NTF_SUBJECT             OUT VARCHAR2,
                            OUT_NTF_BODY                OUT CLOB)
   IS
      l_application_link        VARCHAR2 (1024);
      l_TEMPLATE_CONTENT        AS_NTF_TEMPLATES.TEMPLATE_CONTENT%TYPE;
      L_NXT_ACTION_HISTORY_ID   NUMBER;
      l_approver_name           VARCHAR2 (512 BYTE);

      CURSOR trans_cur
      IS
         SELECT AsSchedulerTransHdrsEO.SCHEDULER_TRANS_HDR_ID,
                AsSchedulerTransHdrsEO.TRANS_TYPE,
                AsSchedulerTransHdrsEO.NOTES,
                AsSchedulerTransHdrsEO.CHANGE_REASON,
                rsn_lkp.MEANING CHANGE_REASON_name,
                AsSchedulerTransHdrsEO.START_TIME_HOURS,
                AsSchedulerTransHdrsEO.START_TIME_MINUTES,
                AsSchedulerTransHdrsEO.END_TIME_HOURS,
                AsSchedulerTransHdrsEO.END_TIME_MINUTES,
                AsSchedulerTransHdrsEO.EFFECTIVE_START_DATE,
                AsSchedulerTransHdrsEO.EFFECTIVE_END_DATE,
                ass.START_TIME_HOURS orig_START_TIME_HOURS,
                ass.START_TIME_MINUTES orig_START_TIME_MINUTES,
                ass.END_TIME_HOURS orig_END_TIME_HOURS,
                ass.END_TIME_MINUTES orig_END_TIME_MINUTES,
                PAPF_REQ.full_name req_employee_name,
                ass.PERSON_ID trans_person_id,
                PAPF_TRANSEMP.full_name trans_employee_name,
                AsSchedulerTransHdrsEO.SUGGESTED_PERSON_ID,
                PAPF_SUGG.full_name sugg_employee_name,
                shft.shift_name,
                wrkgrp.WORK_GROUP_NAME
           FROM AS_SCHEDULER_TRANS_HDRS AsSchedulerTransHdrsEO,
                AS_SCHEDULER_STAFF ass,
                PER_ALL_PEOPLE_F PAPF_REQ,
                PER_ALL_PEOPLE_F PAPF_TRANSEMP,
                PER_ALL_PEOPLE_F PAPF_SUGG,
                AS_SHIFTS shft,
                AS_WORK_GROUPS_HDR wrkgrp,
                FND_LOOKUP_VALUES_VL rsn_lkp
          WHERE AsSchedulerTransHdrsEO.SCHEDULER_STAFF_ID =
                   ass.SCHEDULER_STAFF_ID
                AND AsSchedulerTransHdrsEO.REQUESTER_ID =
                       PAPF_REQ.PERSON_ID(+)
                AND SYSDATE BETWEEN PAPF_REQ.EFFECTIVE_START_DATE(+)
                                AND PAPF_REQ.EFFECTIVE_END_DATE(+)
                AND ass.PERSON_ID = PAPF_TRANSEMP.PERSON_ID
                AND SYSDATE BETWEEN PAPF_TRANSEMP.EFFECTIVE_START_DATE
                                AND PAPF_TRANSEMP.EFFECTIVE_END_DATE
                AND AsSchedulerTransHdrsEO.SUGGESTED_PERSON_ID =
                       PAPF_SUGG.PERSON_ID(+)
                AND SYSDATE BETWEEN PAPF_SUGG.EFFECTIVE_START_DATE(+)
                                AND PAPF_SUGG.EFFECTIVE_END_DATE(+)
                AND ass.shift_id = shft.shift_id(+)
                AND ass.WORK_GROUP_ID = wrkgrp.WORK_GROUP_ID(+)
                AND AsSchedulerTransHdrsEO.CHANGE_REASON =
                       rsn_lkp.LOOKUP_CODE(+)
                AND rsn_lkp.LOOKUP_TYPE(+) = 'XXMBC_AA_SCHDLR_TRANS_REASONS'
                AND AsSchedulerTransHdrsEO.SCHEDULER_TRANS_HDR_ID =
                       IN_SCHEDULER_TRANS_HDR_ID;
   BEGIN
      FOR trans_rec IN trans_cur
      LOOP
         SELECT MIN (SCHEDULER_ACTION_HISTORY_ID)
           INTO L_NXT_ACTION_HISTORY_ID
           FROM AS_SCHEDULER_ACTION_HISTORY
          WHERE     TRANS_ID = trans_rec.SCHEDULER_TRANS_HDR_ID
                AND TRANS_TYPE = 'TRANS'
                AND ACTION_TYPE_TAKEN IS NULL;

         SELECT papf.full_name
           INTO l_approver_name
           FROM AS_SCHEDULER_ACTION_HISTORY hst, per_all_people_f papf
          WHERE hst.PERSON_ID = papf.PERSON_ID
                AND SYSDATE BETWEEN papf.effective_start_date
                                AND papf.effective_end_date
                AND SCHEDULER_ACTION_HISTORY_ID = L_NXT_ACTION_HISTORY_ID;

         l_application_link :=
               'http://10.10.131.26:7003/scheduler/faces/notifications?ntfid='
            || IN_NOTIFICATION_ID
            || '&accesskey='
            || IN_ACCESS_KEY;

         IF trans_rec.TRANS_TYPE = 'REPLACE'
         THEN
            OUT_NTF_SUBJECT :=
                  'AA Schedules : Replace attendance  of  '
               || trans_rec.trans_employee_name
               || ' requires your approval.';

            SELECT TEMPLATE_CONTENT
              INTO l_TEMPLATE_CONTENT
              FROM AS_NTF_TEMPLATES
             WHERE TEMPLATE_KEY = 'REPLACE_APPROVER';

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{1}', l_approver_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{2}',
                             trans_rec.trans_employee_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{3}',
                             trans_rec.CHANGE_REASON_name);



            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{4}',
                             trans_rec.trans_person_id);

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{5}',
                  TO_CHAR (trans_rec.EFFECTIVE_START_DATE, 'dd-MON-rrrr'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{6}',
                  TO_CHAR (trans_rec.EFFECTIVE_END_DATE, 'dd-MON-rrrr'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{7}',
                     LPAD (trans_rec.orig_START_TIME_HOURS, 2, '0')
                  || ':'
                  || LPAD (trans_rec.orig_START_TIME_MINUTES, 2, '0'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{8}',
                     LPAD (trans_rec.orig_END_TIME_HOURS, 2, '0')
                  || ':'
                  || LPAD (trans_rec.orig_END_TIME_MINUTES, 2, '0'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{9}', trans_rec.shift_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{10}',
                             trans_rec.WORK_GROUP_NAME);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{11}',
                             trans_rec.sugg_employee_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{12}',
                             trans_rec.suggested_person_id);


            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{13}', trans_rec.notes);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{14}', l_application_link);
         ELSIF trans_rec.TRANS_TYPE = 'CANCEL'
         THEN
            OUT_NTF_SUBJECT :=
                  'AA Schedules : Cancel attendance  of  '
               || trans_rec.trans_employee_name
               || ' requires your approval.';

            SELECT TEMPLATE_CONTENT
              INTO l_TEMPLATE_CONTENT
              FROM AS_NTF_TEMPLATES
             WHERE TEMPLATE_KEY = 'CANCEL_APPROVER';

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{1}', l_approver_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{2}',
                             trans_rec.trans_employee_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{3}',
                             trans_rec.trans_person_id);

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{4}',
                  TO_CHAR (trans_rec.EFFECTIVE_START_DATE, 'dd-MON-rrrr'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{5}',
                  TO_CHAR (trans_rec.EFFECTIVE_END_DATE, 'dd-MON-rrrr'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{6}',
                     LPAD (trans_rec.orig_START_TIME_HOURS, 2, '0')
                  || ':'
                  || LPAD (trans_rec.orig_START_TIME_MINUTES, 2, '0'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{7}',
                     LPAD (trans_rec.orig_END_TIME_HOURS, 2, '0')
                  || ':'
                  || LPAD (trans_rec.orig_END_TIME_MINUTES, 2, '0'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{8}', trans_rec.shift_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{9}',
                             trans_rec.WORK_GROUP_NAME);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{10}', trans_rec.notes);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{11}', l_application_link);
         ELSIF trans_rec.TRANS_TYPE = 'CHANGE'
         THEN
            OUT_NTF_SUBJECT :=
                  'AA Schedules : Change attendance  of  '
               || trans_rec.trans_employee_name
               || ' requires your approval.';



            SELECT TEMPLATE_CONTENT
              INTO l_TEMPLATE_CONTENT
              FROM AS_NTF_TEMPLATES
             WHERE TEMPLATE_KEY = 'CHANGE_APPROVER';

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{1}', l_approver_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{2}',
                             trans_rec.trans_employee_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{3}',
                             trans_rec.trans_person_id);

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{4}',
                  TO_CHAR (trans_rec.EFFECTIVE_START_DATE, 'dd-MON-rrrr'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{5}',
                  TO_CHAR (trans_rec.EFFECTIVE_END_DATE, 'dd-MON-rrrr'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{6}',
                     LPAD (trans_rec.orig_START_TIME_HOURS, 2, '0')
                  || ':'
                  || LPAD (trans_rec.orig_START_TIME_MINUTES, 2, '0'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{7}',
                     LPAD (trans_rec.orig_END_TIME_HOURS, 2, '0')
                  || ':'
                  || LPAD (trans_rec.orig_END_TIME_MINUTES, 2, '0'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{8}', trans_rec.shift_name);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT,
                             '{9}',
                             trans_rec.WORK_GROUP_NAME);



            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{10}',
                  TO_CHAR (trans_rec.EFFECTIVE_START_DATE, 'dd-MON-rrrr'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{11}',
                  TO_CHAR (trans_rec.EFFECTIVE_END_DATE, 'dd-MON-rrrr'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{12}',
                     LPAD (trans_rec.START_TIME_HOURS, 2, '0')
                  || ':'
                  || LPAD (trans_rec.START_TIME_MINUTES, 2, '0'));

            l_TEMPLATE_CONTENT :=
               replace_Clob (
                  l_TEMPLATE_CONTENT,
                  '{13}',
                     LPAD (trans_rec.END_TIME_HOURS, 2, '0')
                  || ':'
                  || LPAD (trans_rec.END_TIME_MINUTES, 2, '0'));



            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{14}', trans_rec.notes);

            l_TEMPLATE_CONTENT :=
               replace_Clob (l_TEMPLATE_CONTENT, '{15}', l_application_link);
         END IF;

         OUT_NTF_BODY := l_TEMPLATE_CONTENT;
      END LOOP;
   END GET_TRANS_NTF;
END AS_NTF_CONTENT_PKG;
/

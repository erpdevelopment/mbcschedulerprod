--------------------------------------------------------
--  DDL for Table AS_SCHEDULER_ACTION_HISTORY
--------------------------------------------------------

  CREATE TABLE "APPS"."AS_SCHEDULER_ACTION_HISTORY" ("SCHEDULER_ACTION_HISTORY_ID" NUMBER, "TRANS_ID" NUMBER, "ORDER_SEQ" NUMBER, "PERSON_ID" NUMBER, "ACTION_TYPE" VARCHAR2(32), "NOTES" VARCHAR2(1024), "CREATED_BY" NUMBER, "LAST_UPDATE_DATE" DATE, "LAST_UPDATED_BY" NUMBER, "LAST_UPDATE_LOGIN" NUMBER, "ACTION_TYPE_TAKEN" VARCHAR2(32), "TRANS_TYPE" VARCHAR2(32))

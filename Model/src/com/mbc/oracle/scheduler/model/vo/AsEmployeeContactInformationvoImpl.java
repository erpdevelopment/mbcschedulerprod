package com.mbc.oracle.scheduler.model.vo;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Sun Jun 23 15:45:35 GST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AsEmployeeContactInformationvoImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public AsEmployeeContactInformationvoImpl() {
    }

    /**
     * Returns the bind variable value for person_id.
     * @return bind variable value for person_id
     */
    public String getperson_id() {
        return (String) getNamedWhereClauseParam("person_id");
    }

    /**
     * Sets <code>value</code> for bind variable person_id.
     * @param value value to bind as person_id
     */
    public void setperson_id(String value) {
        setNamedWhereClauseParam("person_id", value);
    }

    /**
     * Returns the bind variable value for replacement_id.
     * @return bind variable value for replacement_id
     */
    public String getreplacement_id() {
        return (String) getNamedWhereClauseParam("replacement_id");
    }

    /**
     * Sets <code>value</code> for bind variable replacement_id.
     * @param value value to bind as replacement_id
     */
    public void setreplacement_id(String value) {
        setNamedWhereClauseParam("replacement_id", value);
    }
}


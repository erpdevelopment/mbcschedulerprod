package com.mbc.oracle.scheduler.model.eo;

import oracle.jbo.AttributeList;
import oracle.jbo.Key;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.SequenceImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jul 10 15:20:08 GST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AsSchedulerStaffHistoryEOImpl extends EntityImpl {
    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        super.doDML(operation, e);
    }

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        SchedulerStaffId,
        SchedulerId,
        PersonId,
        SchedularStartDate,
        SchedularEndDate,
        StartTimeHours,
        StartTimeMinutes,
        EndTimeHours,
        EndTimeMinutes,
        CreationDate,
        CreatedBy,
        LastUpdateDate,
        LastUpdatedBy,
        LastUpdateLogin,
        RecurringFlag,
        BulletinFlag,
        ParentSchedulerStaffId,
        ShiftId,
        WorkGroupId,
        Notes,
        FkSchdlrShiftAttributeId,
        FkSchedulerSectionId,
        ReplaceType,
        FkReplacedById,
        EmailNotification,
        SmsNotifiction,
        FkWkReplacedId,
        SourceDate,
        SourceShift,
        SourceShiftAttribute,
        HistoryId,
        SourceShiftId,
        SourceShiftAttributeId,
        ActionType;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int SCHEDULERSTAFFID = AttributesEnum.SchedulerStaffId.index();
    public static final int SCHEDULERID = AttributesEnum.SchedulerId.index();
    public static final int PERSONID = AttributesEnum.PersonId.index();
    public static final int SCHEDULARSTARTDATE = AttributesEnum.SchedularStartDate.index();
    public static final int SCHEDULARENDDATE = AttributesEnum.SchedularEndDate.index();
    public static final int STARTTIMEHOURS = AttributesEnum.StartTimeHours.index();
    public static final int STARTTIMEMINUTES = AttributesEnum.StartTimeMinutes.index();
    public static final int ENDTIMEHOURS = AttributesEnum.EndTimeHours.index();
    public static final int ENDTIMEMINUTES = AttributesEnum.EndTimeMinutes.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATELOGIN = AttributesEnum.LastUpdateLogin.index();
    public static final int RECURRINGFLAG = AttributesEnum.RecurringFlag.index();
    public static final int BULLETINFLAG = AttributesEnum.BulletinFlag.index();
    public static final int PARENTSCHEDULERSTAFFID = AttributesEnum.ParentSchedulerStaffId.index();
    public static final int SHIFTID = AttributesEnum.ShiftId.index();
    public static final int WORKGROUPID = AttributesEnum.WorkGroupId.index();
    public static final int NOTES = AttributesEnum.Notes.index();
    public static final int FKSCHDLRSHIFTATTRIBUTEID = AttributesEnum.FkSchdlrShiftAttributeId.index();
    public static final int FKSCHEDULERSECTIONID = AttributesEnum.FkSchedulerSectionId.index();
    public static final int REPLACETYPE = AttributesEnum.ReplaceType.index();
    public static final int FKREPLACEDBYID = AttributesEnum.FkReplacedById.index();
    public static final int EMAILNOTIFICATION = AttributesEnum.EmailNotification.index();
    public static final int SMSNOTIFICTION = AttributesEnum.SmsNotifiction.index();
    public static final int FKWKREPLACEDID = AttributesEnum.FkWkReplacedId.index();
    public static final int SOURCEDATE = AttributesEnum.SourceDate.index();
    public static final int SOURCESHIFT = AttributesEnum.SourceShift.index();
    public static final int SOURCESHIFTATTRIBUTE = AttributesEnum.SourceShiftAttribute.index();
    public static final int HISTORYID = AttributesEnum.HistoryId.index();
    public static final int SOURCESHIFTID = AttributesEnum.SourceShiftId.index();
    public static final int SOURCESHIFTATTRIBUTEID = AttributesEnum.SourceShiftAttributeId.index();
    public static final int ACTIONTYPE = AttributesEnum.ActionType.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AsSchedulerStaffHistoryEOImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("com.mbc.oracle.scheduler.model.eo.AsSchedulerStaffHistoryEO");
    }


    /**
     * Gets the attribute value for SchedulerStaffId, using the alias name SchedulerStaffId.
     * @return the value of SchedulerStaffId
     */
    public Number getSchedulerStaffId() {
        return (Number) getAttributeInternal(SCHEDULERSTAFFID);
    }

    /**
     * Sets <code>value</code> as the attribute value for SchedulerStaffId.
     * @param value value to set the SchedulerStaffId
     */
    public void setSchedulerStaffId(Number value) {
        setAttributeInternal(SCHEDULERSTAFFID, value);
    }

    /**
     * Gets the attribute value for SchedulerId, using the alias name SchedulerId.
     * @return the value of SchedulerId
     */
    public Number getSchedulerId() {
        return (Number) getAttributeInternal(SCHEDULERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for SchedulerId.
     * @param value value to set the SchedulerId
     */
    public void setSchedulerId(Number value) {
        setAttributeInternal(SCHEDULERID, value);
    }

    /**
     * Gets the attribute value for PersonId, using the alias name PersonId.
     * @return the value of PersonId
     */
    public Number getPersonId() {
        return (Number) getAttributeInternal(PERSONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for PersonId.
     * @param value value to set the PersonId
     */
    public void setPersonId(Number value) {
        setAttributeInternal(PERSONID, value);
    }

    /**
     * Gets the attribute value for SchedularStartDate, using the alias name SchedularStartDate.
     * @return the value of SchedularStartDate
     */
    public Date getSchedularStartDate() {
        return (Date) getAttributeInternal(SCHEDULARSTARTDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for SchedularStartDate.
     * @param value value to set the SchedularStartDate
     */
    public void setSchedularStartDate(Date value) {
        setAttributeInternal(SCHEDULARSTARTDATE, value);
    }

    /**
     * Gets the attribute value for SchedularEndDate, using the alias name SchedularEndDate.
     * @return the value of SchedularEndDate
     */
    public Date getSchedularEndDate() {
        return (Date) getAttributeInternal(SCHEDULARENDDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for SchedularEndDate.
     * @param value value to set the SchedularEndDate
     */
    public void setSchedularEndDate(Date value) {
        setAttributeInternal(SCHEDULARENDDATE, value);
    }

    /**
     * Gets the attribute value for StartTimeHours, using the alias name StartTimeHours.
     * @return the value of StartTimeHours
     */
    public Number getStartTimeHours() {
        return (Number) getAttributeInternal(STARTTIMEHOURS);
    }

    /**
     * Sets <code>value</code> as the attribute value for StartTimeHours.
     * @param value value to set the StartTimeHours
     */
    public void setStartTimeHours(Number value) {
        setAttributeInternal(STARTTIMEHOURS, value);
    }

    /**
     * Gets the attribute value for StartTimeMinutes, using the alias name StartTimeMinutes.
     * @return the value of StartTimeMinutes
     */
    public Number getStartTimeMinutes() {
        return (Number) getAttributeInternal(STARTTIMEMINUTES);
    }

    /**
     * Sets <code>value</code> as the attribute value for StartTimeMinutes.
     * @param value value to set the StartTimeMinutes
     */
    public void setStartTimeMinutes(Number value) {
        setAttributeInternal(STARTTIMEMINUTES, value);
    }

    /**
     * Gets the attribute value for EndTimeHours, using the alias name EndTimeHours.
     * @return the value of EndTimeHours
     */
    public Number getEndTimeHours() {
        return (Number) getAttributeInternal(ENDTIMEHOURS);
    }

    /**
     * Sets <code>value</code> as the attribute value for EndTimeHours.
     * @param value value to set the EndTimeHours
     */
    public void setEndTimeHours(Number value) {
        setAttributeInternal(ENDTIMEHOURS, value);
    }

    /**
     * Gets the attribute value for EndTimeMinutes, using the alias name EndTimeMinutes.
     * @return the value of EndTimeMinutes
     */
    public Number getEndTimeMinutes() {
        return (Number) getAttributeInternal(ENDTIMEMINUTES);
    }

    /**
     * Sets <code>value</code> as the attribute value for EndTimeMinutes.
     * @param value value to set the EndTimeMinutes
     */
    public void setEndTimeMinutes(Number value) {
        setAttributeInternal(ENDTIMEMINUTES, value);
    }

    /**
     * Gets the attribute value for CreationDate, using the alias name CreationDate.
     * @return the value of CreationDate
     */
    public Date getCreationDate() {
        return (Date) getAttributeInternal(CREATIONDATE);
    }


    /**
     * Gets the attribute value for CreatedBy, using the alias name CreatedBy.
     * @return the value of CreatedBy
     */
    public Number getCreatedBy() {
        return (Number) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as the attribute value for CreatedBy.
     * @param value value to set the CreatedBy
     */
    public void setCreatedBy(Number value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for LastUpdateDate, using the alias name LastUpdateDate.
     * @return the value of LastUpdateDate
     */
    public Date getLastUpdateDate() {
        return (Date) getAttributeInternal(LASTUPDATEDATE);
    }


    /**
     * Gets the attribute value for LastUpdatedBy, using the alias name LastUpdatedBy.
     * @return the value of LastUpdatedBy
     */
    public Number getLastUpdatedBy() {
        return (Number) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as the attribute value for LastUpdatedBy.
     * @param value value to set the LastUpdatedBy
     */
    public void setLastUpdatedBy(Number value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LastUpdateLogin, using the alias name LastUpdateLogin.
     * @return the value of LastUpdateLogin
     */
    public Number getLastUpdateLogin() {
        return (Number) getAttributeInternal(LASTUPDATELOGIN);
    }

    /**
     * Sets <code>value</code> as the attribute value for LastUpdateLogin.
     * @param value value to set the LastUpdateLogin
     */
    public void setLastUpdateLogin(Number value) {
        setAttributeInternal(LASTUPDATELOGIN, value);
    }

    /**
     * Gets the attribute value for RecurringFlag, using the alias name RecurringFlag.
     * @return the value of RecurringFlag
     */
    public String getRecurringFlag() {
        return (String) getAttributeInternal(RECURRINGFLAG);
    }

    /**
     * Sets <code>value</code> as the attribute value for RecurringFlag.
     * @param value value to set the RecurringFlag
     */
    public void setRecurringFlag(String value) {
        setAttributeInternal(RECURRINGFLAG, value);
    }

    /**
     * Gets the attribute value for BulletinFlag, using the alias name BulletinFlag.
     * @return the value of BulletinFlag
     */
    public String getBulletinFlag() {
        return (String) getAttributeInternal(BULLETINFLAG);
    }

    /**
     * Sets <code>value</code> as the attribute value for BulletinFlag.
     * @param value value to set the BulletinFlag
     */
    public void setBulletinFlag(String value) {
        setAttributeInternal(BULLETINFLAG, value);
    }

    /**
     * Gets the attribute value for ParentSchedulerStaffId, using the alias name ParentSchedulerStaffId.
     * @return the value of ParentSchedulerStaffId
     */
    public Number getParentSchedulerStaffId() {
        return (Number) getAttributeInternal(PARENTSCHEDULERSTAFFID);
    }

    /**
     * Sets <code>value</code> as the attribute value for ParentSchedulerStaffId.
     * @param value value to set the ParentSchedulerStaffId
     */
    public void setParentSchedulerStaffId(Number value) {
        setAttributeInternal(PARENTSCHEDULERSTAFFID, value);
    }

    /**
     * Gets the attribute value for ShiftId, using the alias name ShiftId.
     * @return the value of ShiftId
     */
    public Number getShiftId() {
        return (Number) getAttributeInternal(SHIFTID);
    }

    /**
     * Sets <code>value</code> as the attribute value for ShiftId.
     * @param value value to set the ShiftId
     */
    public void setShiftId(Number value) {
        setAttributeInternal(SHIFTID, value);
    }

    /**
     * Gets the attribute value for WorkGroupId, using the alias name WorkGroupId.
     * @return the value of WorkGroupId
     */
    public Number getWorkGroupId() {
        return (Number) getAttributeInternal(WORKGROUPID);
    }

    /**
     * Sets <code>value</code> as the attribute value for WorkGroupId.
     * @param value value to set the WorkGroupId
     */
    public void setWorkGroupId(Number value) {
        setAttributeInternal(WORKGROUPID, value);
    }

    /**
     * Gets the attribute value for Notes, using the alias name Notes.
     * @return the value of Notes
     */
    public String getNotes() {
        return (String) getAttributeInternal(NOTES);
    }

    /**
     * Sets <code>value</code> as the attribute value for Notes.
     * @param value value to set the Notes
     */
    public void setNotes(String value) {
        setAttributeInternal(NOTES, value);
    }

    /**
     * Gets the attribute value for FkSchdlrShiftAttributeId, using the alias name FkSchdlrShiftAttributeId.
     * @return the value of FkSchdlrShiftAttributeId
     */
    public Number getFkSchdlrShiftAttributeId() {
        return (Number) getAttributeInternal(FKSCHDLRSHIFTATTRIBUTEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FkSchdlrShiftAttributeId.
     * @param value value to set the FkSchdlrShiftAttributeId
     */
    public void setFkSchdlrShiftAttributeId(Number value) {
        setAttributeInternal(FKSCHDLRSHIFTATTRIBUTEID, value);
    }

    /**
     * Gets the attribute value for FkSchedulerSectionId, using the alias name FkSchedulerSectionId.
     * @return the value of FkSchedulerSectionId
     */
    public Number getFkSchedulerSectionId() {
        return (Number) getAttributeInternal(FKSCHEDULERSECTIONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FkSchedulerSectionId.
     * @param value value to set the FkSchedulerSectionId
     */
    public void setFkSchedulerSectionId(Number value) {
        setAttributeInternal(FKSCHEDULERSECTIONID, value);
    }

    /**
     * Gets the attribute value for ReplaceType, using the alias name ReplaceType.
     * @return the value of ReplaceType
     */
    public String getReplaceType() {
        return (String) getAttributeInternal(REPLACETYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for ReplaceType.
     * @param value value to set the ReplaceType
     */
    public void setReplaceType(String value) {
        setAttributeInternal(REPLACETYPE, value);
    }

    /**
     * Gets the attribute value for FkReplacedById, using the alias name FkReplacedById.
     * @return the value of FkReplacedById
     */
    public Number getFkReplacedById() {
        return (Number) getAttributeInternal(FKREPLACEDBYID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FkReplacedById.
     * @param value value to set the FkReplacedById
     */
    public void setFkReplacedById(Number value) {
        setAttributeInternal(FKREPLACEDBYID, value);
    }

    /**
     * Gets the attribute value for EmailNotification, using the alias name EmailNotification.
     * @return the value of EmailNotification
     */
    public String getEmailNotification() {
        return (String) getAttributeInternal(EMAILNOTIFICATION);
    }

    /**
     * Sets <code>value</code> as the attribute value for EmailNotification.
     * @param value value to set the EmailNotification
     */
    public void setEmailNotification(String value) {
        setAttributeInternal(EMAILNOTIFICATION, value);
    }

    /**
     * Gets the attribute value for SmsNotifiction, using the alias name SmsNotifiction.
     * @return the value of SmsNotifiction
     */
    public String getSmsNotifiction() {
        return (String) getAttributeInternal(SMSNOTIFICTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for SmsNotifiction.
     * @param value value to set the SmsNotifiction
     */
    public void setSmsNotifiction(String value) {
        setAttributeInternal(SMSNOTIFICTION, value);
    }

    /**
     * Gets the attribute value for FkWkReplacedId, using the alias name FkWkReplacedId.
     * @return the value of FkWkReplacedId
     */
    public Number getFkWkReplacedId() {
        return (Number) getAttributeInternal(FKWKREPLACEDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FkWkReplacedId.
     * @param value value to set the FkWkReplacedId
     */
    public void setFkWkReplacedId(Number value) {
        setAttributeInternal(FKWKREPLACEDID, value);
    }

    /**
     * Gets the attribute value for SourceDate, using the alias name SourceDate.
     * @return the value of SourceDate
     */
    public Date getSourceDate() {
        return (Date) getAttributeInternal(SOURCEDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for SourceDate.
     * @param value value to set the SourceDate
     */
    public void setSourceDate(Date value) {
        setAttributeInternal(SOURCEDATE, value);
    }

    /**
     * Gets the attribute value for SourceShift, using the alias name SourceShift.
     * @return the value of SourceShift
     */
    public String getSourceShift() {
        return (String) getAttributeInternal(SOURCESHIFT);
    }

    /**
     * Sets <code>value</code> as the attribute value for SourceShift.
     * @param value value to set the SourceShift
     */
    public void setSourceShift(String value) {
        setAttributeInternal(SOURCESHIFT, value);
    }

    /**
     * Gets the attribute value for SourceShiftAttribute, using the alias name SourceShiftAttribute.
     * @return the value of SourceShiftAttribute
     */
    public String getSourceShiftAttribute() {
        return (String) getAttributeInternal(SOURCESHIFTATTRIBUTE);
    }

    /**
     * Sets <code>value</code> as the attribute value for SourceShiftAttribute.
     * @param value value to set the SourceShiftAttribute
     */
    public void setSourceShiftAttribute(String value) {
        setAttributeInternal(SOURCESHIFTATTRIBUTE, value);
    }

    /**
     * Gets the attribute value for HistoryId, using the alias name HistoryId.
     * @return the value of HistoryId
     */
    public Number getHistoryId() {
        return (Number) getAttributeInternal(HISTORYID);
    }

    /**
     * Sets <code>value</code> as the attribute value for HistoryId.
     * @param value value to set the HistoryId
     */
    public void setHistoryId(Number value) {
        setAttributeInternal(HISTORYID, value);
    }


    /**
     * Gets the attribute value for SourceShiftId, using the alias name SourceShiftId.
     * @return the value of SourceShiftId
     */
    public Number getSourceShiftId() {
        return (Number) getAttributeInternal(SOURCESHIFTID);
    }

    /**
     * Sets <code>value</code> as the attribute value for SourceShiftId.
     * @param value value to set the SourceShiftId
     */
    public void setSourceShiftId(Number value) {
        setAttributeInternal(SOURCESHIFTID, value);
    }

    /**
     * Gets the attribute value for SourceShiftAttributeId, using the alias name SourceShiftAttributeId.
     * @return the value of SourceShiftAttributeId
     */
    public Number getSourceShiftAttributeId() {
        return (Number) getAttributeInternal(SOURCESHIFTATTRIBUTEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for SourceShiftAttributeId.
     * @param value value to set the SourceShiftAttributeId
     */
    public void setSourceShiftAttributeId(Number value) {
        setAttributeInternal(SOURCESHIFTATTRIBUTEID, value);
    }

    /**
     * Gets the attribute value for ActionType, using the alias name ActionType.
     * @return the value of ActionType
     */
    public String getActionType() {
        return (String) getAttributeInternal(ACTIONTYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for ActionType.
     * @param value value to set the ActionType
     */
    public void setActionType(String value) {
        setAttributeInternal(ACTIONTYPE, value);
    }


    /**
     * @param historyId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(Number historyId) {
        return new Key(new Object[] { historyId });
    }

    /**
     * Add attribute defaulting logic in this method.
     * @param attributeList list of attribute names/values to initialize the row
     */
    protected void create(AttributeList attributeList) {
        super.create(attributeList);
        SequenceImpl seq=new SequenceImpl("scheduler_staff_history_seq", this.getDBTransaction());
        this.setHistoryId(seq.getSequenceNumber());
    }

    /**
     * Add entity remove logic in this method.
     */
    public void remove() {
        super.remove();
    }
}




CREATE OR REPLACE PACKAGE APPS.AS_SCDLR_WF_PKG
AS
 PROCEDURE XXSCDLR_RUN_PATTERN_PROCESS(ORG_ID IN  NUMBER ,FROM_ROLE VARCHAR2);
 PROCEDURE APRROVE_PATTERN_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2);
                                   PROCEDURE APRROVE_MANAGER_PATTERN_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2);
                                    PROCEDURE SET_ATTR_PATTERN_ERP_NOTIF (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   );
     PROCEDURE XX_CREATE_HTML_PATTERN
(
org_id     IN              VARCHAR2,
DISPLAY_TYPE    IN              VARCHAR2,
DOCUMENT        IN OUT NOCOPY   VARCHAR2,
document_type   IN OUT NOCOPY   VARCHAR2
);
  

 PROCEDURE XXSCDLR_RUN_SHIFT_PROCESS(ORG_ID IN  NUMBER ,FROM_ROLE VARCHAR2);
 PROCEDURE APRROVE_SHIFT_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2);
                                            PROCEDURE APRROVE_MANAGER_SHIFT_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2);
                                    PROCEDURE SET_ATTR_SHIFT_ERP_NOTIF (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   );
     PROCEDURE XX_CREATE_HTML_SHIFT
(
org_id     IN              VARCHAR2,
DISPLAY_TYPE    IN              VARCHAR2,
DOCUMENT        IN OUT NOCOPY   VARCHAR2,
document_type   IN OUT NOCOPY   VARCHAR2
); 





 PROCEDURE XXSCDLR_RUN_BULLETIN_PROCESS(ORG_ID IN  NUMBER ,FROM_ROLE VARCHAR2);
 PROCEDURE APRROVE_BULLETIN_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2);
                                            PROCEDURE APRROVE_MANAGER_BLTIN_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2);
                                    PROCEDURE SET_ATTR_BULLETIN_ERP_NOTIF (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   );
     PROCEDURE XX_CREATE_HTML_BULLETIN
(
org_id     IN              VARCHAR2,
DISPLAY_TYPE    IN              VARCHAR2,
DOCUMENT        IN OUT NOCOPY   VARCHAR2,
document_type   IN OUT NOCOPY   VARCHAR2
); 

 
 
 END ;
/

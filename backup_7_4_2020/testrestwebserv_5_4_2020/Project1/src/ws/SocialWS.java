package ws;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

@Path("social")
public class SocialWS {
    public SocialWS() {
        super();
    }

    @POST
    @Produces("application/json")
    @Path("/insertHappiness")
    public Response insertHappiness(@Context SecurityContext sc, 
                                    @HeaderParam("fcx") String faceId,@HeaderParam("wsid") String wsid,
                                    @HeaderParam("signature") String signature,
                                    @HeaderParam("comments") String comments,
                                    @HeaderParam("apx") String applicationId
                                          
                                            )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            CallableStatement cs=null;
            Response response = null;
            String sqlStemt="begin " +
            "APPS.MBC_MOBILE_PKG.INSERT_HAPPINESS ( ?,  mbc_mobile_pkg.get_session_data('PERSON_ID') , ?, ?,  mbc_mobile_pkg.get_session_data('USER_ID')   );" +
            "end;";
            try {
                if (comments !=null)
                    comments=URLDecoder.decode(comments);
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"insertHappiness");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            cs = connection.prepareCall(sqlStemt);
               
                cs.setString(1, applicationId); 
               
                cs.setString(2, faceId);  
                cs.setString(3, comments);  
                
           
                cs.executeUpdate();
        
      
                response = Helper.StringtoReponse("ok","1");

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
             response = Helper.StringtoReponse( e.getMessage().replace("\n", "").replace("\r", ""),"0");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                          cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    @POST
    @Produces("application/json")
    @Path("/insertCard")
    public Response insertCard(@Context SecurityContext sc, @HeaderParam("fromPersonId") String fromPersonId,
                               @HeaderParam("toPersonId") String toPersonId,
                               @HeaderParam("subjectId") String subjectId,
                               @HeaderParam("comments") String comments,
                               @HeaderParam("userId") String userId
                                          
                                            )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            Connection connection = null;
            CallableStatement cs=null;
            Response response = null;
            String sqlStemt="begin " +
            "APPS.MBC_MOBILE_PKG.INSERT_CARD ( ?, ?, ?,?, ? );" +
            "end;";
            try {
                if (comments !=null)
                    comments=URLDecoder.decode(comments);
                connection = Helper.getOracleConnection();
            cs = connection.prepareCall(sqlStemt);
               
             
                cs.setString(1, fromPersonId);  
                cs.setString(2, toPersonId);  
                cs.setString(3, subjectId);  
                cs.setString(4, comments);  
                cs.setString(5, userId);  
           
                cs.executeUpdate();
        
      
                response = Helper.StringtoReponse("ok","1");

            }
            catch(Exception e)
            {
                e.printStackTrace();
             response = Helper.StringtoReponse( e.getMessage().replace("\n", "").replace("\r", ""),"0");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                          cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    @GET
    @Produces("application/json")
    @Path("/getMyData")
    public Response getMyData()
                                          
                                           
    {
           
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT 1 Id, 138 Numofresponse, 'Over Expectations' Feedback, 's1' Selector\n" + 
            "  FROM DUAL\n" + 
            "UNION ALL\n" + 
            "SELECT 2 Id, 35 Numofresponse, 'Under Expectation' Feedback, 's2' Selector\n" + 
            "  FROM DUAL\n" + 
            "UNION ALL\n" + 
            "SELECT 3 Id, 131 Numofresponse, 'Meet  Expectations' Feedback, 's3' Selector\n" + 
            "  FROM DUAl";
            try {
                connection = Helper.getOracleConnection();
                
            statement = connection.prepareStatement(sqlStemt);
               
               
               
             rs = statement.executeQuery();
            
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,1);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
            
   
            
        }
    
}

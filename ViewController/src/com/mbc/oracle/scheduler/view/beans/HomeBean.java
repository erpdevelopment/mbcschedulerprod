package com.mbc.oracle.scheduler.view.beans;

import java.util.Date;

import oracle.adf.view.faces.bi.component.timeline.UITimeline;

public class HomeBean {
    private static final long day = (long) (1000 * 60 * 60 * 24);
    public HomeBean() {
    }

    public Date getTimelineStart() {
        return new Date(new Date().getTime() - (14 * day));
    }

    public Date getTimelineEnd() {
        return new Date(getTimelineStart().getTime() + (90 * day));
    }
}

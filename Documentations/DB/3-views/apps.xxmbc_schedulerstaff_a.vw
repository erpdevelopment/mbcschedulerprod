
/* Formatted on 10/Dec/2018 2:43:29 PM (QP5 v5.215.12089.38647) */
CREATE OR REPLACE FORCE VIEW APPS.XXMBC_SCHEDULERSTAFF_A
(
   SHIFT_ATTRIBUTE_ID,
   SCHEDULER_STAFF_ID,
   SCHEDULER_ID,
   FK_SCHDLR_SHIFT_ATTRIBUTE_ID,
   SCHEDULAR_START_DATE,
   FULL_NAME,
   PERSON_ID,
   EMPSHIFT,
   NOTES_BULLETIN
)
AS
   SELECT SHIFT_ATTRIBUTE_ID,
          scheduler_staff_id,
          AsSchedulerStaffEO.SCHEDULER_ID,
          FK_SCHDLR_SHIFT_ATTRIBUTE_ID,
          SCHEDULAR_START_DATE,
          papf.full_name,
          papf.person_id,
             DECODE (NVL (us.INTIAL_NAME, 'NNN'),
                     'NNN', NVL (papf.full_name, 'XXX'),
                     NVL (us.INTIAL_NAME, 'XXX'))
          || DECODE (NVL (us_replacement.INTIAL_NAME, 'N'),
                     'N', ' - ' || REPLACEMENT.full_name,
                     ' - ' || us_replacement.INTIAL_NAME)
             empshift,
             as_schdlr_pkg.SCHEDULER_STAFF_BULLETIN (scheduler_staff_id)
          || DECODE (NVL (NOTES, 'N'), 'N', '', '/' || NOTES)
             notes_bulletin
     FROM AS_SCHEDULER_STAFF AsSchedulerStaffEO,
          AS_SHIFT_ATTRIBUTES AsShiftAttributesEO,
          PER_ALL_PEOPLE_F PAPF,
          PER_ALL_PEOPLE_F REPLACEMENT,
          AS_SCHEDULERs schdlr,
          AS_SHIFTS ASS,
          AS_WORK_GROUPS_HDR AWGH,
          as_user_roles us,
          as_user_roles us_replacement
    WHERE     AsSchedulerStaffEO.PERSON_ID = PAPF.PERSON_ID(+)
          AND AsSchedulerStaffEO.PERSON_ID = us.PERSON_Id(+)
          AND AsSchedulerStaffEO.FK_REPLACED_BY_ID = REPLACEMENT.PERSON_ID(+)
          AND AsSchedulerStaffEO.FK_REPLACED_BY_ID =
                 us_replacement.PERSON_Id(+)
          AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1)
          AND SYSDATE BETWEEN NVL (REPLACEMENT.EFFECTIVE_START_DATE,
                                   SYSDATE - 1)
                          AND NVL (REPLACEMENT.EFFECTIVE_END_DATE,
                                   SYSDATE + 1)
          AND AsSchedulerStaffEO.SCHEDULER_ID = schdlr.SCHEDULER_ID
          AND AsSchedulerStaffEO.SHIFT_ID = ASS.SHIFT_ID(+)
          AND AsSchedulerStaffEO.WORK_GROUP_ID = AWGH.WORK_GROUP_ID(+)
          AND AsSchedulerStaffEO.SCHEDULER_ID =
                 AsShiftAttributesEO.FKSCHEDULER_ID
          AND AsSchedulerStaffEO.FK_SCHDLR_SHIFT_ATTRIBUTE_ID =
                 AsShiftAttributesEO.SHIFT_ATTRIBUTE_ID
-- AND  AsSchedulerStaffEO.SCHEDULER_ID=13

--select * from

--          select * from  as_scheduler_staff st,as_shift_attributes sb
--
--        where   st.FK_SCHDLR_SHIFT_ATTRIBUTE_ID =   sb.SHIFT_ATTRIBUTE_ID
--        and FKSCHEDULER_ID=13
--            select * from as_schedulers
--            where scheduler_id=13
;

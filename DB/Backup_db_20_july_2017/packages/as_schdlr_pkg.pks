DROP PACKAGE APPS.AS_SCHDLR_PKG;

CREATE OR REPLACE PACKAGE APPS.AS_SCHDLR_PKG
IS
   PROCEDURE REPEAT_PATTERN (IN_SCHEDULER_STAFF_ID       NUMBER,
                             OUT_RESULT              OUT VARCHAR2);

   PROCEDURE DELETE_ALL_SCHDLR (IN_SCHEDULER_STAFF_ID NUMBER);

   FUNCTION GET_WRKGRP_ANNUAL_LEAVE_USED (IN_WORK_GROUP_ID NUMBER)
      RETURN NUMBER;

   FUNCTION GET_SCHDLR_COUNT_ON_DATE (IN_SCHEDULER_ID            NUMBER,
                                      IN_PERSON_ID               NUMBER,
                                      IN_SCHEDULAR_START_DATE    DATE)
      RETURN NUMBER;

   PROCEDURE SCHDLR_INTERSECT_ASSIGNMENT (
      IN_SCHEDULER_ID               NUMBER,
      IN_PERSON_ID                  NUMBER,
      IN_SCHEDULAR_START_DATE       DATE,
      OUT_IS_EXIST              OUT VARCHAR2,
      OUT_EXIST_MESSAGE         OUT VARCHAR2);

   PROCEDURE SCHDLR_INTERSECT_LEAVE (IN_SCHEDULER_ID               NUMBER,
                                     IN_PERSON_ID                  NUMBER,
                                     IN_SCHEDULAR_START_DATE       DATE,
                                     OUT_IS_EXIST              OUT VARCHAR2,
                                     OUT_EXIST_MESSAGE         OUT VARCHAR2);

   PROCEDURE SUBMIT_SCHDLR_TRANS (IN_SCHEDULER_TRANS_HDR_ID    NUMBER,
                                  IN_PERSON_ID                 NUMBER);

   PROCEDURE APPLY_SCHDLR_TRANS (IN_SCHEDULER_TRANS_HDR_ID        NUMBER,
                                 OUT_GEN_SCHEDULER_STAFF_ID   OUT NUMBER,
                                 OUT_STATUS                   OUT VARCHAR2);

   PROCEDURE SUBMIT_SCHEDULER (IN_SCHEDULER_ID       NUMBER,
                               IN_PERSON_ID          NUMBER,
                               OUT_STATUS        OUT VARCHAR2);


   PROCEDURE PUSH_SCHEDULER_CALENDAR (IN_SCHEDULER_ID       NUMBER,
                                      OUT_STATUS        OUT VARCHAR2);

   PROCEDURE GET_WORKLIST_COUNT (IN_PERSONID        NUMBER,
                                 OUT_APPROVED   OUT NUMBER,
                                 OUT_REJECTED   OUT NUMBER,
                                 OUT_WAITING    OUT NUMBER);

   PROCEDURE WORKLIST_ACTION (IN_WORKLIST_ID    NUMBER,
                              IN_ACTION         VARCHAR2,
                              IN_NOTES          VARCHAR2);

   PROCEDURE VALIDATE_SCHEDULER (IN_SCHEDULER_ID           NUMBER,
                                 OUT_VALIDATE_REPORT   OUT CLOB);

   FUNCTION CHECK_ASSIGNMENT_OVERLAPPING (IN_ASSIGNMENT_EMP_ID    NUMBER,
                                          IN_PERSON_ID            NUMBER,
                                          IN_START_DATE           DATE,
                                          IN_END_DATE             DATE)
      RETURN VARCHAR2;

   PROCEDURE DELETE_AS_SCHEDULER (IN_SCHEDULER_ID NUMBER);
    --Added by Heba 11-july for tas integartion --
   function GetDateDifference(pstartdate date,penddate date)return number;
   function VALIDATE_TAS_RECORD(pperson_id Number,p_employeecode varchar2,pshiftcode varchar2,pshiftdate date,pshiftstart number,pshiftend number)return varchar2;
 PROCEDURE INSERT_TAS_RECORD(pperson_id Number,p_employeecode varchar2,pshiftcode varchar2,pshiftdate date,pshiftstart number,pshiftend number,pstatus varchar2 ,poff_flag varchar2);

END AS_SCHDLR_PKG;
/

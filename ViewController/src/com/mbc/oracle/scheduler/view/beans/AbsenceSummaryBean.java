package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;
import com.mbc.oracle.scheduler.view.util.DateConversion;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;

import java.util.HashMap;

import java.util.Iterator;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.faces.bi.component.gantt.TaskbarFormat;
import oracle.adf.view.faces.bi.component.gantt.UIGantt;
import oracle.adf.view.faces.bi.component.gantt.UISchedulingGantt;

import oracle.jbo.ViewObject;
import oracle.jbo.domain.Date;

public class AbsenceSummaryBean {
    private Date srchAbsenceStartDate;
    private Date srchAbsenceEndDate;
    private UISchedulingGantt absenceGanttChart;
    private HashMap selectedAbsenceTypesId;
    private HashMap selectedWorkGroupId;

    public AbsenceSummaryBean() {
    }


    public void onChangeWorkGrpChkbx(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        Object workGroupId = valueChangeEvent.getComponent().getAttributes().get("WorkGroupId");
        Boolean newValue = (Boolean) valueChangeEvent.getNewValue();
        if (newValue) {
            this.getSelectedWorkGroupId().put(workGroupId, "Y");
        } else {
            this.getSelectedWorkGroupId().remove(workGroupId);
        }
    }

    public void setSrchAbsenceStartDate(Date srchAbsenceStartDate) {
        this.srchAbsenceStartDate = srchAbsenceStartDate;
    }

    public Date getSrchAbsenceStartDate() {
        if (srchAbsenceStartDate == null) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {
                java.util.Date sdfDate = sdf.parse("01-01-" + year);
                srchAbsenceStartDate = new Date(new java.sql.Date(sdfDate.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return srchAbsenceStartDate;
    }

    public void setSrchAbsenceEndDate(Date srchAbsenceEndDate) {
        this.srchAbsenceEndDate = srchAbsenceEndDate;
    }

    public Date getSrchAbsenceEndDate() {
        if (srchAbsenceEndDate == null) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {
                java.util.Date sdfDate = sdf.parse("31-12-" + year);
                srchAbsenceEndDate = new Date(new java.sql.Date(sdfDate.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return srchAbsenceEndDate;
    }

    public java.util.Date getSrchAbsenceStartDateUtil() {
        return DateConversion.getUtilDateFromJboDate(this.getSrchAbsenceStartDate());
    }

    public java.util.Date getSrchAbsenceEndDateUtil() {
        return DateConversion.getUtilDateFromJboDate(this.getSrchAbsenceEndDate());
    }

    public void onClickSearchAbsenceBtn(ActionEvent actionEvent) {
        // Add event code here...
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        ViewObject absenceSummaryVO = ADFUtils.findIterator("AbsenceSummaryVOIterator").getViewObject();
        String whereClause =
            "start_date >= to_date('" +
            sdf.format(DateConversion.getUtilDateFromJboDate(this.getSrchAbsenceStartDate())) +
            "' ,'dd-mm-rrrr') and end_date <= to_date('" +
            sdf.format(DateConversion.getUtilDateFromJboDate(this.getSrchAbsenceEndDate())) + "','dd-mm-rrrr')";
        /*String concatenatedAbsenceTypes = this.getConcatenatedKey(this.getSelectedAbsenceTypesId());
        if (!"".equals(concatenatedAbsenceTypes)) {
            whereClause = whereClause + " and absence_attendance_type_id in(" + concatenatedAbsenceTypes + ") ";
        }*/
        absenceSummaryVO.setWhereClause(whereClause);

        ViewObject allWorkGroupEmpsVO = ADFUtils.findIterator("AllWorkGroupEmpsVOIterator").getViewObject();
        String concatenatedWorkGroups = this.getConcatenatedKey(this.getSelectedWorkGroupId());
        if (!"".equals(concatenatedWorkGroups)) {
            String whereClauseWrkEmp =
                " person_id in (select person_id from AS_WORK_GROUPS_LINES awgl where awgl.WORK_GROUP_ID in (" +
                concatenatedWorkGroups + "))";
            allWorkGroupEmpsVO.setWhereClause(whereClauseWrkEmp);
        } else {
            allWorkGroupEmpsVO.setWhereClause(null);
        }
        allWorkGroupEmpsVO.executeQuery();
        absenceSummaryVO.executeQuery();
    }

    public void setAbsenceGanttChart(UISchedulingGantt absenceGanttChart) {
        this.absenceGanttChart = absenceGanttChart;
        this.addFormatsToGantt(absenceGanttChart);
    }

    public String initAbsenceSummary() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date startDatePar = null;
        try {
            java.util.Date sdfDate = sdf.parse("01-01-2016");
            startDatePar = new Date(new java.sql.Date(sdfDate.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDatePar = new Date(Date.getCurrentDate());

        ViewObject absenceTypeStatVO = ADFUtils.findIterator("AbsenceTypeStatVOIterator").getViewObject();
        absenceTypeStatVO.setNamedWhereClauseParam("StartDatePar", startDatePar);
        absenceTypeStatVO.setNamedWhereClauseParam("EndDatePar", endDatePar);
        absenceTypeStatVO.executeQuery();

        ViewObject absenceWorkGroupsVO = ADFUtils.findIterator("AbsenceWorkGroupsVOIterator").getViewObject();
        absenceWorkGroupsVO.setNamedWhereClauseParam("StartDatePar", startDatePar);
        absenceWorkGroupsVO.setNamedWhereClauseParam("EndDatePar", endDatePar);
        absenceWorkGroupsVO.executeQuery();

        ADFUtils.findIterator("AllWorkGroupEmpsVOIterator").getViewObject().executeEmptyRowSet();
        this.setSelectedAbsenceTypesId(new HashMap());
        this.setSelectedWorkGroupId(new HashMap());
        return "initAbsenceSummary";
    }

    public UISchedulingGantt getAbsenceGanttChart() {
        return absenceGanttChart;
    }

    public void setSelectedAbsenceTypesId(HashMap selectedAbsenceTypesId) {
        this.selectedAbsenceTypesId = selectedAbsenceTypesId;
    }

    public HashMap getSelectedAbsenceTypesId() {
        return selectedAbsenceTypesId;
    }

    public void setSelectedWorkGroupId(HashMap selectedWorkGroupId) {
        this.selectedWorkGroupId = selectedWorkGroupId;
    }

    public HashMap getSelectedWorkGroupId() {
        return selectedWorkGroupId;
    }

    public void addFormatsToGantt(UIGantt theGantt) {
        TaskbarFormat greenFormat =
            new TaskbarFormat("greenFormat", "#BBBBBB", "/resources/images/greenGradient.png", "#DDDDDD", 23);
        greenFormat.setFillColor("green");
        greenFormat.setBorderColor("green");
        greenFormat.setLabelColor("maroon");
        greenFormat.setLabel("greenFormat");
        greenFormat.setHeight(15);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Annual Leave", greenFormat);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Training", greenFormat);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Hajj Leave", greenFormat);
        //theGantt.getTaskbarFormatManager().registerTaskbarFormat("Compassionate Leave", greenFormat);
        //theGantt.getTaskbarFormatManager().registerTaskbarFormat("Compassionate Leave", greenFormat);

        TaskbarFormat redFormat = new TaskbarFormat("redFormat", "#BBBBBB", "redGradient.png", "#DDDDDD", 23);
        redFormat.setFillColor("red");
        redFormat.setBorderColor("red");
        redFormat.setLabelColor("maroon");
        redFormat.setLabel("redFormat");
        redFormat.setHeight(15);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Business Trip", redFormat);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Time Off in Lieu", redFormat);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Extra Time Worked", redFormat);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Sick Leave", redFormat);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Last Year Booking", redFormat);
        theGantt.getTaskbarFormatManager().registerTaskbarFormat("Unpaid Leave", redFormat);
    }


    public void onChangeAbsenceTypesChkbx(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        Object absenceAttendanceTypeId = valueChangeEvent.getComponent().getAttributes().get("AbsenceAttendanceTypeId");
        Boolean newValue = (Boolean) valueChangeEvent.getNewValue();
        if (newValue) {
            this.getSelectedAbsenceTypesId().put(absenceAttendanceTypeId, "Y");
        } else {
            this.getSelectedAbsenceTypesId().remove(absenceAttendanceTypeId);
        }
    }

    private String getConcatenatedKey(Map map) {
        String retValue = "";
        Iterator iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry pair = (Map.Entry) iter.next();
            retValue = retValue + pair.getKey() + ",";
        }
        if (retValue != null && retValue.length() > 0) {
            retValue = retValue.substring(0, retValue.length() - 1);
        }
        return retValue;
    }
    
    public String[] getAbsenceGanttTooltipKeyLabels() {

        return new String[] { "Employee", "Type", "Start Date", "End Date", "Days" };

    }
}

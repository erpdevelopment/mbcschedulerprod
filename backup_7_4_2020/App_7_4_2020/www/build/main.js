webpackJsonp([0],{

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaveHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__leave_history_leave_history__ = __webpack_require__(527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__leave_balance_leave_balance__ = __webpack_require__(537);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__new_leave_new_leave__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_pages_start_start__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__toil_expire_toil_expire__ = __webpack_require__(539);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the LeaveHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LeaveHomePage = (function () {
    function LeaveHomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LeaveHomePage.prototype.ionViewDidLoad = function () {
        this.setBackButtonAction();
        console.log('ionViewDidLoad LeaveHomePage');
    };
    LeaveHomePage.prototype.goPageH = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__leave_history_leave_history__["a" /* LeaveHistoryPage */], { userId: this.userId, personId: this.personId, businessGroup: this.businessGroup, assignmentId: this.assignmentId });
    };
    LeaveHomePage.prototype.goPageN = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__new_leave_new_leave__["a" /* NewLeavePage */], { userId: this.userId, personId: this.personId, businessGroup: this.businessGroup, assignmentId: this.assignmentId });
    };
    LeaveHomePage.prototype.goPageB = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__leave_balance_leave_balance__["a" /* LeaveBalancePage */], { userId: this.userId, personId: this.personId, businessGroup: this.businessGroup, assignmentId: this.assignmentId });
    };
    LeaveHomePage.prototype.backPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__shared_pages_start_start__["a" /* StartPage */], { userId: this.userId, personId: this.personId, businessGroup: this.businessGroup, assignmentId: this.assignmentId });
    };
    LeaveHomePage.prototype.setBackButtonAction = function () {
        var _this = this;
        this.Navbar.backButtonClick = function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__shared_pages_start_start__["a" /* StartPage */]);
        };
    };
    LeaveHomePage.prototype.goPageTOIL = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__toil_expire_toil_expire__["a" /* ToilExpirePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */])
    ], LeaveHomePage.prototype, "Navbar", void 0);
    LeaveHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave-home',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\leave-home\leave-home.html"*/'<!--\n\n  Generated template for the LeaveHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n   \n\n  <ion-navbar hideBackButton=\'false\' >\n\n    \n\n    <ion-title>Leaves</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding class="ion-content">\n\n\n\n<!-- \n\n  <div class="mnb"  id="ApprovalCenter">\n\n    <button ion-button  color="dark"   class="ico-fa-btn1" (click)="goPageH()">\n\n      <fa-icon name="calendar-check-o"  class="aw-ico1"></fa-icon>\n\n        \n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt" >Leave     History</h6>\n\n  </div>\n\n  <div class="mnb"  id="Profile">\n\n      <button ion-button   color="dark"   class="ico-fa-btn1" (click)="goPageN()">\n\n          <fa-icon name="address-card"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">New Leave  </h6>\n\n  </div> -->\n\n  \n\n  <div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goPageN()">\n\n        <fa-icon name="calendar-plus-o"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >New</h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n  </div>\n\n\n\n\n\n  <div class="mnb"   >\n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goPageB()">\n\n          <fa-icon name="pie-chart"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Balance </h6>\n\n      <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n  </div>\n\n\n\n  <div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goPageH()">\n\n        <fa-icon name="list-ul"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >History  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n</div>\n\n\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"  class="ico-fa-btn1 " (click)="goPageTOIL()">\n\n        <fa-icon name="hourglass-end"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >TOIL </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >Expiry</h6>\n\n</div>\n\n\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\leave-home\leave-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], LeaveHomePage);
    return LeaveHomePage;
}());

//# sourceMappingURL=leave-home.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThanksCardProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the ThanksCardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ThanksCardProvider = (function () {
    function ThanksCardProvider(http, loadingController, AlertController) {
        this.http = http;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        console.log('Hello ThanksCardProvider Provider');
    }
    ThanksCardProvider.prototype.getCards = function (cardType) {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/thanks/card/getCards';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'cardType': '' + cardType + '',
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'resourceName': 'thanks/card/getCards'
        });
        return new Promise(function (resolve) {
            _this.http.post(null, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    ThanksCardProvider.prototype.getLeaderBoard = function () {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/thanks/card/getLeaderBoard';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'resourceName': 'thanks/card/getLeaderBoard'
        });
        return new Promise(function (resolve) {
            _this.http.post(null, null, { headers: methodHeaders }).timeout(30000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                console.log(err);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    ThanksCardProvider.prototype.getSubjectTypes = function () {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/thanks/card/getSubjectTypes';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'resourceName': 'thanks/card/getSubjectTypes'
        });
        return new Promise(function (resolve) {
            _this.http.post(null, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    ThanksCardProvider.prototype.sendCard = function (toPersonId, subjectId, comments) {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/thanks/card/newCard';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'toPersonId': toPersonId + '',
            'subjectId': subjectId + '',
            'comments': encodeURI(comments + ' '),
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'resourceName': 'thanks/card/newCard'
        });
        return new Promise(function (resolve) {
            _this.http.post(null, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    ThanksCardProvider.prototype.checkAddCard = function (personId) {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/thanks/card/checkAdd';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'personId': personId + '',
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'resourceName': 'thanks/card/checkAdd'
        });
        return new Promise(function (resolve) {
            _this.http.post(null, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    ThanksCardProvider.prototype.getRemaingCards = function () {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/thanks/card/getRemaingCards';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'resourceName': 'thanks/card/getRemaingCards'
        });
        return new Promise(function (resolve) {
            _this.http.post(null, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    ThanksCardProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */]])
    ], ThanksCardProvider);
    return ThanksCardProvider;
}());

//# sourceMappingURL=thanks-card.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PayrollProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the PayrollProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var PayrollProvider = (function () {
    function PayrollProvider(http, httpClient, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
        this.wsURLx = 'http://10.10.131.34:7004/mbcWebservice/resources';
        console.log('Hello PayrollProvider Provider');
    }
    PayrollProvider.prototype.getBankInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'payroll/info/getBankInfo';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    PayrollProvider.prototype.getPayslipList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'payroll/info/getPayslipList';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'bu': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('bu'),
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    PayrollProvider.prototype.getPayrollEarnings = function (actionContextId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'payroll/info/getPayrollEarnings';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + actionContextId)];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'bu': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('bu'),
                            'acx': '' + actionContextId
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    PayrollProvider.prototype.getPayrollDeductions = function (actionContextId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'payroll/info/getPayrollDeductions';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + actionContextId)];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'bu': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('bu'),
                            'acx': '' + actionContextId,
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    PayrollProvider.prototype.getsalaryHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'payroll/info/getSalaryHistory';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    PayrollProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */]])
    ], PayrollProvider);
    return PayrollProvider;
}());

//# sourceMappingURL=payroll.js.map

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__main_main__ = __webpack_require__(526);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_log_in_service_log_in_service__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_fingerprint_aio__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(586);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_transfer__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_path__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_aes_256__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};

















//import { resolve } from 'path';
var LoginPage = (function () {
    function LoginPage(navCtrl, logProvider, toastCtrl, fingerPrint, platform, storage, statusBar, loadingController, camera, transfer, file, filePath, actionSheetCtrl, alertCtrl, aes256) {
        this.navCtrl = navCtrl;
        this.logProvider = logProvider;
        this.toastCtrl = toastCtrl;
        this.fingerPrint = fingerPrint;
        this.platform = platform;
        this.storage = storage;
        this.statusBar = statusBar;
        this.loadingController = loadingController;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.aes256 = aes256;
        this.showFingerButton = false;
        this.showFaceButton = false;
        this.bioType = 'finger';
        this.loginresult = 0;
        this.appUpdate = __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].appUpdate;
        this.statusBar.overlaysWebView(true);
        if (this.platform.is('ios')) {
            //this.statusBar.hide();
            this.statusBar.overlaysWebView(false);
            __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].platform = 2;
        }
        // this.statusBar.backgroundColorByHexString("#00a2e8");
        this.fop = {
            clientId: 'Fingerprint-Demo',
            clientSecret: 'password',
            disableBackup: true,
            localizedFallbackTitle: 'Use Pin',
            localizedReason: 'Smart Touch'
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // consider putting loading 
        setTimeout(function () {
            _this.intilaiazelogin();
        }, 3000);
    };
    LoginPage.prototype.intilaiazelogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var userfinger, FPA, ref1, ref2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getValue('bx')];
                    case 1:
                        userfinger = _a.sent();
                        if (!userfinger) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.checkFingerPrintAvailabliity()];
                    case 2:
                        FPA = _a.sent();
                        if (!(FPA == 1)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.getValue('ref1')];
                    case 3:
                        ref1 = _a.sent();
                        return [4 /*yield*/, this.getValue('ref2')];
                    case 4:
                        ref2 = _a.sent();
                        if (ref1) {
                            this.showfin(ref1, ref2);
                            if (this.bioType == 'face')
                                this.showFaceButton = true;
                            else
                                this.showFingerButton = true;
                        }
                        else {
                        }
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.mm = function (s, e) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.logProvider.xx(s, e)
                            .then(function (data) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                this.mmx = data;
                                return [2 /*return*/, data];
                            });
                        }); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var error, loader_1;
            return __generator(this, function (_a) {
                error = 0;
                if (this.user == null || this.user.length == 0) {
                    this.showErrorUser();
                    error = 1;
                }
                if (this.userPass == null || this.userPass.length == 0) {
                    this.showErrorPass();
                    error = 1;
                }
                if (error == 1)
                    return [2 /*return*/];
                //
                //AppModule.authentication='Basic '+ AppModule.tobase64(this.user+':'+this.userPass);
                try {
                    loader_1 = __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
                    setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                        var result, FPA;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].stopLoader(loader_1);
                                    return [4 /*yield*/, this.getAuthentication(this.user, this.userPass)];
                                case 1:
                                    result = _a.sent();
                                    console.log('result is :' + this.loginresult);
                                    if (!(this.loginresult == 0)) return [3 /*break*/, 2];
                                    if (this.loginMessage && this.loginMessage.toUpperCase().indexOf('UPDATE') > 1) {
                                        // update 
                                        __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].showUpdateMessage(this.alertCtrl, this.loginMessage);
                                    }
                                    else {
                                        // other messages 
                                        __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].showMessage(this.alertCtrl, this.loginMessage);
                                    }
                                    return [3 /*break*/, 4];
                                case 2: return [4 /*yield*/, this.checkFingerPrintAvailabliity()];
                                case 3:
                                    FPA = _a.sent();
                                    if (FPA == 1) {
                                        if (this.platform.is('ios')) {
                                            this.showconfirmfinger();
                                        }
                                        else
                                            this.showConfirm();
                                    }
                                    else {
                                        this.navigat();
                                    }
                                    _a.label = 4;
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); }, 3000);
                }
                catch (error) {
                    __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].showMessage(this.alertCtrl, 'Error:' + error);
                }
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.navigat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__main_main__["a" /* MainPage */]);
    };
    LoginPage.prototype.getAuthentication = function (user, pa) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.logProvider.getUserAuthentication(user, pa)
                                .then(function (data) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    this.isok = data;
                                    if (this.isok[0].message == '0') {
                                        // console.log('wrong');
                                        this.loginresult = 0;
                                        this.loginMessage = this.isok[0].value;
                                    }
                                    else {
                                        //  this.setValue(user, pa);
                                        this.loginresult = 1;
                                    }
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.getAouth2 = function (user, pa) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.logProvider.getAouth(user, pa)
                                .then(function (data) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    this.authResult = data;
                                    //alert (this.authResult.access_token)
                                    __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].authentication = 'Bearer ' + this.authResult.access_token;
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.loginAio = function (token, signature) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.logProvider.loginAio(token, signature)
                                .then(function (data) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    this.isok = data;
                                    if (this.isok[0].message == '0') {
                                        this.loginresult = 0;
                                        this.loginMessage = this.isok[0].value;
                                    }
                                    else {
                                        this.loginresult = 1;
                                    }
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.showConfirm = function () {
        var _this = this;
        var message = 'Do you want to use your Touch ID to login ?';
        if (this.bioType == 'face')
            message = 'Do you want to use your Face ID to login ?';
        var confirm = this.alertCtrl.create({
            title: 'Login',
            message: message,
            buttons: [
                {
                    text: 'No',
                    handler: function () {
                        _this.navigat();
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.showconfirmfinger();
                    }
                }
            ]
        });
        confirm.present();
    };
    LoginPage.prototype.showfin = function (token, signature) {
        return __awaiter(this, void 0, void 0, function () {
            var av, res, result, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 6, , 7]);
                        return [4 /*yield*/, this.platform.ready()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.fingerPrint.isAvailable()];
                    case 2:
                        av = _a.sent();
                        if (!(av == 'OK' || av == 'finger' || av == 'face')) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.fingerPrint.show(this.fop)];
                    case 3:
                        res = _a.sent();
                        return [4 /*yield*/, this.loginAio(token, signature)];
                    case 4:
                        result = _a.sent();
                        if (this.loginresult == 0) {
                            if (this.loginMessage && this.loginMessage.toUpperCase().indexOf('UPDATE') > 1) {
                                // update 
                                __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].showUpdateMessage(this.alertCtrl, this.loginMessage);
                            }
                            else {
                                // other messages 
                                __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].showMessage(this.alertCtrl, this.loginMessage);
                            }
                        }
                        else {
                            this.navigat();
                        }
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        error_4 = _a.sent();
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.registerAio = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.logProvider.registerAio(this.userPass)
                                .then(function (data) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    this.AioResult = data;
                                    if (this.AioResult[0].message == '1') {
                                        this.storage.set('ref1', this.AioResult[0].ref1);
                                        this.storage.set('ref2', this.AioResult[0].ref2);
                                        this.storage.set('bx', '0xxxx0t');
                                    }
                                    else {
                                        alert(this.AioResult[0].value);
                                        __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].showMessage(this.alertCtrl, 'Failed to register Biometric');
                                    }
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_5 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.showconfirmfinger = function () {
        return __awaiter(this, void 0, void 0, function () {
            var av, res, token, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 7, , 8]);
                        return [4 /*yield*/, this.platform.ready()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.fingerPrint.isAvailable()];
                    case 2:
                        av = _a.sent();
                        if (!(av == 'OK' || av == 'finger' || av == 'face')) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.fingerPrint.show(this.fop)];
                    case 3:
                        res = _a.sent();
                        return [4 /*yield*/, this.registerAio()];
                    case 4:
                        token = _a.sent();
                        this.navigat();
                        return [3 /*break*/, 6];
                    case 5:
                        this.navigat();
                        _a.label = 6;
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        error_6 = _a.sent();
                        this.navigat();
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.checkFingerPrintAvailabliity = function () {
        return __awaiter(this, void 0, void 0, function () {
            var av, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.platform.ready()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.fingerPrint.isAvailable()];
                    case 2:
                        av = _a.sent();
                        if (av == 'OK' || av == 'finger' || av == 'face') {
                            if (av == 'face')
                                this.bioType = 'face';
                            return [2 /*return*/, 1];
                        }
                        else
                            return [2 /*return*/, 0];
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        return [2 /*return*/, 0];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.getValue = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        _this.storage.get(name).then(function (val) {
                            resolve(val);
                        });
                    })];
            });
        });
    };
    LoginPage.prototype.showErrorUser = function () {
        var el;
        el = document.getElementById("userId");
        __WEBPACK_IMPORTED_MODULE_7_jquery__(el).addClass('error-color');
        // x.style.display="none";
        // $(x).notify("You should enter a userName",  { className:"error",  position:"top center",autoHideDelay: 2500,showDuration: 200 });
    };
    LoginPage.prototype.showErrorPass = function () {
        var el;
        el = document.getElementById("passId");
        __WEBPACK_IMPORTED_MODULE_7_jquery__(el).addClass('error-color');
    };
    LoginPage.prototype.fingerPrinAction = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ref1, ref2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getValue('ref1')];
                    case 1:
                        ref1 = _a.sent();
                        return [4 /*yield*/, this.getValue('ref2')];
                    case 2:
                        ref2 = _a.sent();
                        if (ref1) {
                            this.showfin(ref1, ref2);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\login\login.html"*/'<ion-content padding class="page-container">\n\n  <ion-avatar item-start>\n\n\n\n  </ion-avatar>\n\n\n\n  <ion-list-header class="header1 ">\n\n\n\n  </ion-list-header>\n\n\n\n  <ion-list class="list1">\n\n \n\n    <ion-item class="user-class" Id="userId">\n\n      <ion-icon item-start name="ios-person-outline" class="icon1"></ion-icon>\n\n      <ion-input type="text" placeholder="Username" [(ngModel)]="user">\n\n\n\n      </ion-input>\n\n    </ion-item>\n\n    <ion-item class="pass-class" Id="passId">\n\n      <ion-icon item-start name="ios-unlock-outline" class="icon1"></ion-icon>\n\n      <ion-input type="password" placeholder="Password" [(ngModel)]="userPass">\n\n\n\n      </ion-input>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n  <button ion-button block color="dark" (click)="login()" class="btn1">Login</button>\n\n \n\n  \n\n  <div class=\'finger-div\'  *ngIf="showFingerButton">\n\n  <button ion-button icon-only outline  color=\'light\' class=\'finger-btn\' (click)="fingerPrinAction()" >\n\n    <ion-icon name="ios-finger-print" class=\'finger-icon\' icon></ion-icon>\n\n  </button>\n\n</div>\n\n\n\n<div class=\'finger-div\' *ngIf="showFaceButton">\n\n  <button ion-button icon-only outline  color=\'light\' class=\'finger-btn\' (click)="fingerPrinAction()" >\n\n    <!-- <ion-icon name="qr-scanner" class=\'finger-icon\' icon></ion-icon> -->\n\n    <ion-img src=\'assets/imgs/face-id.png\' class="img3" *ngIf="showFaceButton"> </ion-img>\n\n  </button>\n\n</div>\n\n  <div class="logos">\n\n    <div class="img1-div">\n\n    <ion-img src=\'assets/imgs/mbc-logo.png\' class="img1"> </ion-img>\n\n  </div>\n\n  <div class="img2-div">\n\n    <ion-img src=\'assets/imgs/aa_logos.png\' class="img2"> </ion-img>\n\n  </div>\n\n  </div>\n\n \n\n  <div class="app-verson">\n\n    {{appUpdate}}\n\n    </div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_log_in_service_log_in_service__["a" /* LogInServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_fingerprint_aio__["a" /* FingerprintAIO */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_transfer__["a" /* Transfer */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_13__ionic_native_aes_256__["a" /* AES256 */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewLeavePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__new_leave_submit_new_leave_submit__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_wheel_selector__ = __webpack_require__(530);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_pages_employees_employees__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








// for upload










/**
 * Generated class for the NewLeavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewLeavePage = (function () {
    function NewLeavePage(navCtrl, navParams, toastCtrl, leavesProvider, loadingController, AlertController, modalCtrl, 
        // for upload
        camera, transfer, file, filePath, actionSheetCtrl, platform, selector) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.leavesProvider = leavesProvider;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.modalCtrl = modalCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
        this.selector = selector;
        this.halfDay = false;
        ///
        this.dummyJson = {
            days: [
                // {description: 'Mon'},
                // {description: 'Tue'},
                // {description: 'Wed'},
                // {description: 'Thu'},
                { description: 'Fri' }
            ],
            people: [
                { description: 'Mike' },
                { description: 'Max' },
                { description: 'Adam' },
                { description: 'Brandy' },
                { description: 'Ben' }
            ]
        };
        // for ulpoad
        this.lastImage = null;
        this.attchedImages = [];
        // this.attchedImages.push({image:'assets/imgs/aa_logos.png'});
        // this.attchedImages.push({image:'assets/imgs/mbc-logo.png'});
        try {
            this.leavesProvider.getLeaveTypes()
                .then(function (data) {
                _this.LeavesTypes = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_10__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, error.message);
        }
        // get LOS 
        try {
            this.leavesProvider.getLOStypes()
                .then(function (data) {
                _this.lOStypes = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_10__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, error.message);
        }
        // get CLR 
        try {
            this.leavesProvider.getCLRtypes()
                .then(function (data) {
                _this.CLRtypes = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_10__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, error.message);
        }
    }
    NewLeavePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewLeavePage');
    };
    NewLeavePage.prototype.showToast = function (position, txt) {
        var x = this.loadingController.create({
            spinner: 'hide',
            content: " ",
        });
        x.present();
        var toast = this.toastCtrl.create({
            message: txt,
            position: position,
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.onDidDismiss(function () { return x.dismiss(); });
        toast.present(toast);
    };
    NewLeavePage.prototype.showErrorMessage = function (elementId, message) {
        //	document.getElementById("d").style.display="none";
        var x;
        x = document.getElementById(elementId);
        __WEBPACK_IMPORTED_MODULE_4_jquery__(x).addClass('error-color');
        // x.style.display="none";
        // $(x).notify(message,  { className:"error",  position:"bottom center",autoHideDelay: 2500,showDuration: 200 });
    };
    // for upload 
    NewLeavePage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Attachment Source',
            buttons: [
                {
                    text: 'Load from Mobile',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    NewLeavePage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 20,
            targetWidth: 600,
            targetHeight: 600,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    NewLeavePage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    NewLeavePage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.attchedImages.push({ image: newFileName });
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    NewLeavePage.prototype.showImages = function () {
        try {
            var i = 0;
            for (i = 0; i < this.attchedImages.length; i++) {
            }
        }
        catch (err) {
            __WEBPACK_IMPORTED_MODULE_10__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
        }
    };
    NewLeavePage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    NewLeavePage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            if (this.platform.is('android'))
                return cordova.file.dataDirectory + img;
            else
                return Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* normalizeURL */])(cordova.file.dataDirectory + img);
        }
    };
    NewLeavePage.prototype.uploadImage = function () {
        // Destination URL
        var url = "http://10.10.131.34:7004/mbcWebservice/resources/sshr/newLeave/addAttach";
        // File for Upload
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        // const fileTransfer: TransferObject = this.transfer.create();
        // this.loading = this.loadingCtrl.create({
        //   content: 'Uploading...',
        // });
        // this.loading.present();
        // Use the FileTransfer to upload the image
        //fileTransfer.upload(targetPath, url, options).then(data => {
        // this.loading.dismissAll()
        //this.presentToast('Image succesful uploaded.');
        //}, err => {
        //this.loading.dismissAll()
        //this.presentToast('Error while uploading file.');
        //});
        var xx;
        this.convertToBase64(targetPath, 'image/png').then(function (data) {
            xx = data.toString().substring(22);
            // this.postimage(,attch,xx);
        });
    };
    NewLeavePage.prototype.convertToBase64 = function (url, outputFormat) {
        return new Promise(function (resolve, reject) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS'), ctx = canvas.getContext('2d'), dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                canvas = null;
                resolve(dataURL);
            };
            img.src = url;
        });
    };
    ///
    NewLeavePage.prototype.postimage = function (attachId, img) {
        try {
            this.leavesProvider.addAttach(attachId, img)
                .then(function (data) {
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_10__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, error.message);
        }
    };
    NewLeavePage.prototype.RemoveAttach = function () {
        this.lastImage = null;
    };
    //
    NewLeavePage.prototype.openPicker = function () {
        var _this = this;
        this.selector.show({
            title: 'Select Your Contact',
            items: [
                this.dummyJson.days,
                this.dummyJson.people
            ],
            positiveButtonText: 'Choose',
            negativeButtonText: 'Nah',
            defaultItems: [
                { index: 0, value: this.dummyJson.days[4].description },
                { index: 1, value: this.dummyJson.people[1].description }
            ]
        }).then(function (result) {
            var msg = "Selected " + result[0].description + " with " + result[1].description;
            var toast = _this.toastCtrl.create({
                message: msg,
                duration: 4000
            });
            toast.present();
        }, function (err) { return console.log('Error: ', err); });
    };
    NewLeavePage.prototype.openPicker2 = function () {
        this.dummyJson.days.pop();
        //for (i=0;i<=)
        // var u=this.LeavesTypes[0].leavename;
        //this.dummyJson.days.push({description:[this.LeavesTypes].[leavename]})
        var i = 0;
        for (i = 0; i < this.LeavesTypes.length; i++) {
            this.dummyJson.days.push({ description: this.LeavesTypes[i].leavename });
        }
        // i=0;
        for (i = 0; i < this.dummyJson.days.length; i++) {
            console.log(this.dummyJson.days[i].description);
        }
        this.selector.show({
            title: 'Select Your Contact',
            items: [
                this.dummyJson.days,
            ],
            positiveButtonText: 'Choose',
            negativeButtonText: 'no',
        }).then(function (result) {
            var msg = "Selected " + result[0].description + " ";
        }, function (err) { return console.log('Error: ', err); });
    };
    NewLeavePage.prototype.previewleave = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var hd, los, clr, absenceAttendanceTypeId, error, attachId, imgClob, currentDate, days, months, years, hrs, ms, ss, cimagePath_1, _loop_1, i, p;
            return __generator(this, function (_a) {
                hd = 'No';
                error = 0;
                if (!this.leaveType) {
                    this.showErrorMessage('leaveTypeId', 'you should enter Leave Type');
                    error = 1;
                }
                if (!this.dateFrom) {
                    this.showErrorMessage('dateFrom', 'you should enter Date From');
                    error = 1;
                }
                if (!this.dateTo) {
                    this.showErrorMessage('dateTo', 'you should enter Date To');
                    error = 1;
                }
                if (error == 1)
                    return [2 /*return*/];
                absenceAttendanceTypeId = this.leaveType.absence_attendance_type_id;
                if (this.halfDay)
                    hd = 'Yes';
                if (this.lOStype)
                    los = this.lOStype.los;
                if (this.CLRtype)
                    clr = this.CLRtype.clr;
                try {
                    // upload if exists 
                    if (this.attchedImages.length >= 1) {
                        currentDate = new Date();
                        days = currentDate.getDate();
                        months = currentDate.getMonth() + 1;
                        years = currentDate.getFullYear();
                        hrs = currentDate.getHours();
                        ms = currentDate.getMinutes();
                        ss = currentDate.getSeconds();
                        attachId = '' + years + months + days + hrs + ms + ss;
                        _loop_1 = function (i, p) {
                            p.then(function (_) { return new Promise(function (resolve) {
                                cimagePath_1 = _this.pathForImage(_this.attchedImages[i].image);
                                // cimagePath=this.attchedImages[i].image;
                                _this.convertToBase64(cimagePath_1, 'image/png')
                                    .then(function (d) {
                                    var imgClob = d.toString().substring(22);
                                    _this.postimage(attachId, imgClob);
                                    console.log(d);
                                    console.log('IX' + i);
                                })
                                    .then(function () {
                                    resolve();
                                    if (i == _this.attchedImages.length - 1) {
                                        // apply 
                                        _this.leavesProvider.newLeaveRequest(_this.dateFrom, _this.dateTo, absenceAttendanceTypeId, _this.leaveType.leavename, 'W', _this.returnDate, _this.comments, '', hd, '', '', los, clr, null, null, _this.selectedreplacedPesonId)
                                            .then(function (data) {
                                            console.log(data);
                                            if (data[0].message == '2') {
                                                __WEBPACK_IMPORTED_MODULE_10__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                                                return;
                                            }
                                            else {
                                                _this.approverList = data;
                                                _this.currentleave = [];
                                                _this.currentleave.push({ dateStart: _this.dateFrom,
                                                    dateEnd: _this.dateTo, absenceAttendanceTypeId: absenceAttendanceTypeId, absenceTypeName: _this.leaveType.leavename, status: 'Y', attribute1: _this.returnDate,
                                                    attribute2: _this.comments, attribute3: '', attribute4: hd, attribute5: '',
                                                    attribute6: '', attribute7: los, attribute8: clr, selectedreplacedPesonId: _this.selectedreplacedPesonId, selectedreplacedPesonName: _this.selectedreplacedPesonName });
                                                //this.navCtrl.push();
                                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__new_leave_submit_new_leave_submit__["a" /* NewLeaveSubmitPage */], {
                                                    approverList: _this.approverList,
                                                    leaveDetails: _this.currentleave[0],
                                                    pAttachId: attachId,
                                                    attchCount: _this.attchedImages.length
                                                });
                                            }
                                        });
                                        // end apply 
                                        console.log('Finshid');
                                    }
                                });
                                console.log('I' + i);
                            }); });
                        };
                        for (i = 0, p = Promise.resolve(); i < this.attchedImages.length; i++) {
                            _loop_1(i, p);
                        }
                        console.log('b');
                    }
                    else {
                        this.leavesProvider.newLeaveRequest(this.dateFrom, this.dateTo, absenceAttendanceTypeId, this.leaveType.leavename, 'W', this.returnDate, this.comments, '', hd, '', '', los, clr, attachId, null, this.selectedreplacedPesonId)
                            .then(function (data) {
                            if (data[0].message == '2') {
                                __WEBPACK_IMPORTED_MODULE_10__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                                return;
                            }
                            else {
                                _this.approverList = data;
                                _this.currentleave = [];
                                _this.currentleave.push({ dateStart: _this.dateFrom,
                                    dateEnd: _this.dateTo, absenceAttendanceTypeId: absenceAttendanceTypeId, absenceTypeName: _this.leaveType.leavename, status: 'Y', attribute1: _this.returnDate,
                                    attribute2: _this.comments, attribute3: '', attribute4: hd, attribute5: '',
                                    attribute6: '', attribute7: los, attribute8: clr, selectedreplacedPesonId: _this.selectedreplacedPesonId, selectedreplacedPesonName: _this.selectedreplacedPesonName });
                                //this.navCtrl.push();
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__new_leave_submit_new_leave_submit__["a" /* NewLeaveSubmitPage */], {
                                    approverList: _this.approverList,
                                    leaveDetails: _this.currentleave[0],
                                    pAttachId: attachId,
                                    attchCount: _this.attchedImages.length
                                });
                            }
                        });
                    }
                }
                catch (err) {
                    __WEBPACK_IMPORTED_MODULE_10__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
                }
                return [2 /*return*/];
            });
        });
    };
    NewLeavePage.prototype.getListIndex = function (item) {
        var index = this.attchedImages.indexOf(item);
        if (index > -1) {
            this.attchedImages.splice(index, 1);
        }
    };
    NewLeavePage.prototype.testxx = function () {
        for (var i = 0, p = Promise.resolve(); i < this.attchedImages.length; i++) {
        }
    };
    NewLeavePage.prototype.replacedTapped = function (event) {
        var _this = this;
        var modalAction = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_11__shared_pages_employees_employees__["a" /* EmployeesPage */], { title: 'Replaced By',
            callingType: 1 });
        modalAction.onDidDismiss(function (data) {
            if (data) {
                _this.replacedPerson = data.user;
                _this.selectedreplacedPesonName = _this.replacedPerson.g;
                _this.selectedreplacedPesonId = _this.replacedPerson.d;
            }
        });
        modalAction.present();
    };
    NewLeavePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-new-leave',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\new-leave\new-leave.html"*/'<!--\n\n  Generated template for the NewLeavePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>New Leave </ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <ion-item-divider color="light"> Please provide the following details</ion-item-divider>\n\n    <ion-list>\n\n    <ion-item Id="leaveTypeId" >\n\n        <ion-label>Type <i class="req-symbol">*</i></ion-label>\n\n        <ion-select [(ngModel)]="leaveType" class="select-1" interface="action-sheet" placeholder="Choose Leave" >\n\n          <!-- <ion-option *ngFor="let o of hairColorData"    [value]="o"   >{{o.text}}      </ion-option> -->\n\n               <ion-option *ngFor="let item of LeavesTypes"   [value]="item"> {{item.leavename}}   </ion-option>\n\n         </ion-select>\n\n      </ion-item>\n\n      <ion-item Id="dateFrom" >\n\n          <ion-label>Date From <i class="req-symbol">*</i></ion-label>\n\n          <ion-datetime displayFormat="DDDD, DD-MMM-YYYY"  [(ngModel)]="dateFrom"   placeholder="Date From" ></ion-datetime>\n\n        </ion-item>\n\n        <ion-item Id="dateTo" >\n\n        <ion-label>Date To <i class="req-symbol">*</i> </ion-label>\n\n        <ion-datetime displayFormat="DDDD, DD-MMM-YYYY" [(ngModel)]="dateTo" placeholder="Date To"></ion-datetime>\n\n      </ion-item>\n\n      <ion-item>\n\n          <ion-label>Return Date </ion-label>\n\n          <ion-datetime displayFormat="DDDD, DD-MMM-YYYY" [(ngModel)]="returnDate"></ion-datetime>\n\n        </ion-item>\n\n      <ion-item>\n\n          <ion-label>Half Day</ion-label>\n\n          <ion-toggle [(ngModel)]="halfDay" color="dark"></ion-toggle>\n\n        </ion-item>\n\n        <!-- <ion-item>\n\n            <ion-label>Level of Sickness </ion-label>\n\n            <ion-select [(ngModel)]="lOStype" interface="action-sheet">\n\n                <ion-option *ngFor="let item of lOStypes"   [value]="item"> {{item.los}}   </ion-option>\n\n             </ion-select>\n\n          </ion-item> -->\n\n\n\n          <button ion-item  (click)="replacedTapped($event)" >\n\n              Replaced By\n\n\n\n            <span class="button_item_title1"  >{{selectedreplacedPesonName}}</span>\n\n                \n\n              \n\n                \n\n              </button>\n\n\n\n          <ion-item>\n\n              <ion-label>Compassionate Reason</ion-label>\n\n              <ion-select [(ngModel)]="CLRtype" interface="action-sheet">\n\n                 <ion-option *ngFor="let item of CLRtypes"   [value]="item"> {{item.clr}}   </ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            \n\n              \n\n            <ion-item>\n\n                <ion-label>Comments</ion-label>\n\n                \n\n                </ion-item>\n\n            <ion-item >\n\n                <ion-textarea  [(ngModel)]="comments" class="txt-area" ></ion-textarea>\n\n              </ion-item >\n\n\n\n\n\n              <!-- <button ion-button icon-end block color=\'light\' class=\'attach-btn\' (click)="presentActionSheet()">\n\n                Add Attachement\n\n                <ion-icon name="md-attach"></ion-icon>\n\n              </button> -->\n\n             \n\n<!-- \n\n              <ion-item>\n\n               =<button ion-button color=\'vlight\'  class="x3" icon-only (click)="getListIndex(item)">\n\n                      <ion-icon name="add" color="secondary"></ion-icon>\n\n                    </button>\n\n                    <h6 class="x4">\n\n                      Attachments\n\n                  </h6 >\n\n                  </ion-item> -->\n\n    \n\n\n\n                    <ion-item>\n\n                   \n\n                    <button ion-button color=\'vlight\' class="x4" icon-only (click)="presentActionSheet()">\n\n                        <ion-icon name="md-attach" color="dark"></ion-icon>\n\n                      </button>\n\n                      <P class="label-1">\n\n                          Attachment(s)\n\n                      </P>\n\n                    </ion-item>\n\n               <ion-item ion-item *ngFor="let item of attchedImages"  block >\n\n               \n\n                <ion-avatar item-start>\n\n                <img  src="{{pathForImage(item.image)}}" />\n\n                <!-- <img  src="{{item.image}}" /> -->\n\n            \n\n               </ion-avatar>\n\n           \n\n           \n\n          \n\n           \n\n            <!-- <ion-buttons end> -->\n\n                <button ion-button color=\'vlight\'  class="x3" icon-only (click)="getListIndex(item)">\n\n                  <ion-icon name="trash" color="dark"></ion-icon>\n\n                </button>\n\n                <h6 class="item-note typec2 x1">\n\n                    {{item.image}}\n\n                </h6 >\n\n\n\n            </ion-item>\n\n   \n\n  \n\n              <!-- <img src="{{pathForImage(lastImage)}}" style="width: 100%" [hidden]="lastImage === null"> -->\n\n             <!-- <ion-buttons>\n\n              <button ion-button icon-end block color=\'light\' class=\'attach-btn\' (click)="RemoveAttach()" [hidden]="true">\n\n                Remove Attachement\n\n                <ion-icon name="md-trash"></ion-icon>\n\n                \n\n              </button>\n\n\n\n              </ion-buttons> -->\n\n              \n\n             \n\n      </ion-list>\n\n</ion-content>\n\n<ion-footer>\n\n\n\n    <button ion-button  color="dark" block (click)="previewleave()">Preview</button>\n\n   \n\n</ion-footer>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\new-leave\new-leave.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__["a" /* SshrLeavesProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["a" /* Transfer */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_wheel_selector__["a" /* WheelSelector */]])
    ], NewLeavePage);
    return NewLeavePage;
}());

//# sourceMappingURL=new-leave.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DelegationHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__new_delegation_new_delegation__ = __webpack_require__(534);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_work_list_ntf_work_list_ntf__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__work_list_work_list__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the DelegationHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DelegationHomePage = (function () {
    function DelegationHomePage(navCtrl, navParams, WorkListNtfProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.WorkListNtfProvider = WorkListNtfProvider;
        this.AlertController = AlertController;
        this.isHidden = [];
        this.getDelegationHistory();
    }
    DelegationHomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DelegationHomePage');
        this.setBackButtonAction();
    };
    DelegationHomePage.prototype.newDelagtion = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__new_delegation_new_delegation__["a" /* NewDelegationPage */], {});
    };
    DelegationHomePage.prototype.getDelegationHistory = function () {
        var _this = this;
        try {
            this.WorkListNtfProvider.getDelegationHistory().then(function (data) {
                _this.delegationHistory = data;
            });
        }
        catch (error) {
            //this.showToast('middle','Connection Problem');
            alert(error);
        }
    };
    DelegationHomePage.prototype.setBackButtonAction = function () {
        var _this = this;
        this.Navbar.backButtonClick = function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__work_list_work_list__["a" /* WorkListPage */]);
        };
    };
    DelegationHomePage.prototype.delegatioTapped = function (event, item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__new_delegation_new_delegation__["a" /* NewDelegationPage */], { mode: 'edit',
            itemType: item.display_name,
            dateFrom: item.begin_date_p,
            dateTo: item.end_date_p,
            personDisplayName: item.globa_name,
            personName: item.action_argument,
            comments: item.rule_comment,
            ruleId: item.rule_id
        });
    };
    DelegationHomePage.prototype.deleteRequest = function (item, i) {
        var _this = this;
        try {
            this.WorkListNtfProvider.deleteVacationRule(item.rule_id).then(function (data) {
                if (data[0].message == '2') {
                    __WEBPACK_IMPORTED_MODULE_4__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                else {
                    // this.navCtrl.push(DelegationHomePage);
                    _this.isHidden[i] = true;
                }
            });
        }
        catch (err) {
            __WEBPACK_IMPORTED_MODULE_4__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */])
    ], DelegationHomePage.prototype, "Navbar", void 0);
    DelegationHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-delegation-home',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\delegation-home\delegation-home.html"*/'<!--\n\n  Generated template for the DelegationHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Delegation</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="newDelagtion()">\n\n        <ion-icon name="md-add" color="light"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <ion-list  >\n\n        <ion-item-sliding *ngFor="let item of delegationHistory   let i = index "  #itemx  >\n\n\n\n      \n\n        <button ion-item (click)="delegatioTapped($event, item )" block  [ngClass]="item.status"   [hidden]="isHidden[i]" >\n\n        \n\n\n\n\n\n              <ion-avatar item-start> \n\n                 \n\n                  <div class="{{item.class_name}}"   >  {{item.intials}} </div>\n\n                 </ion-avatar>\n\n        \n\n          <div class="item-note list-item-title1" >\n\n              {{item.globa_name}}\n\n            </div> \n\n            <div class="item-note list-item-sub-title1" >\n\n              <span class="left">\n\n              {{item.begin_date}}\n\n            </span> \n\n            <span class="right">\n\n                {{item.end_date}}\n\n              </span> \n\n              </div>\n\n              <div class="item-note list-item-sub-title1" >\n\n                  {{item.rule_comment}}\n\n                </div>\n\n\n\n               \n\n        </button>\n\n        <ion-item-options side="right">\n\n            <button ion-button color=\'danger\' (click)="deleteRequest(item,i)">\n\n              <ion-icon name="trash"></ion-icon>\n\n              Delete\n\n            </button>\n\n          </ion-item-options>\n\n        </ion-item-sliding>\n\n      </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\delegation-home\delegation-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_work_list_ntf_work_list_ntf__["a" /* WorkListNtfProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], DelegationHomePage);
    return DelegationHomePage;
}());

//# sourceMappingURL=delegation-home.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThankscardsHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__thanks_cards_thanks_cards__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__leader_board_leader_board__ = __webpack_require__(536);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_pages_start_start__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ThankscardsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ThankscardsHomePage = (function () {
    function ThankscardsHomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ThankscardsHomePage.prototype.ionViewDidLoad = function () {
        this.setBackButtonAction();
        console.log('ionViewDidLoad ThankscardsHomePage');
    };
    ThankscardsHomePage.prototype.setBackButtonAction = function () {
        var _this = this;
        this.Navbar.backButtonClick = function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__shared_pages_start_start__["a" /* StartPage */]);
        };
    };
    ThankscardsHomePage.prototype.openGivenCards = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__thanks_cards_thanks_cards__["a" /* ThanksCardsPage */], { title: 'Given Cards', cardType: 'Given' });
    };
    ThankscardsHomePage.prototype.openReceivedCards = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__thanks_cards_thanks_cards__["a" /* ThanksCardsPage */], { title: 'Received Cards', cardType: 'Received' });
    };
    ThankscardsHomePage.prototype.openLeaderBoard = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__leader_board_leader_board__["a" /* LeaderBoardPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */])
    ], ThankscardsHomePage.prototype, "Navbar", void 0);
    ThankscardsHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-thankscards-home',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\thank-u\thankscards-home\thankscards-home.html"*/'<!--\n\n  Generated template for the ThankscardsHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Thank You</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <div class="mnb"   >\n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openLeaderBoard()">\n\n        <fa-icon name="bar-chart"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Leader</h6>\n\n      <h6 ion-text color="dark" class="btn-txt " >Board</h6>\n\n    </div>\n\n  \n\n  \n\n    <div class="mnb"   >\n\n        <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openReceivedCards()">\n\n         \n\n            <fa-icon name="stack-overflow"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n        <h6 ion-text color="dark" class="btn-txt " >Received </h6>\n\n        <h6 ion-text color="dark" class="btn-txt " >Cards</h6>\n\n    </div>\n\n\n\n    <div class="mnb"   >\n\n        <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openGivenCards()">\n\n         \n\n            <fa-icon name="thumbs-up"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n        <h6 ion-text color="dark" class="btn-txt " >Given </h6>\n\n        <h6 ion-text color="dark" class="btn-txt " >Cards</h6>\n\n    </div>\n\n  </ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\thank-u\thankscards-home\thankscards-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ThankscardsHomePage);
    return ThankscardsHomePage;
}());

//# sourceMappingURL=thankscards-home.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThanksCardsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_thanks_card_thanks_card__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_pages_employees_employees__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the ThanksCardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ThanksCardsPage = (function () {
    function ThanksCardsPage(navCtrl, navParams, ThanksCardProvider, AlertController, sanitized, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ThanksCardProvider = ThanksCardProvider;
        this.AlertController = AlertController;
        this.sanitized = sanitized;
        this.modalCtrl = modalCtrl;
        this.showAdd = false;
        this.disableAdd = true;
        this.fromR = 0;
        this.v_step = 10;
        this.toR = this.v_step;
        this.title = this.navParams.get('title');
        this.cardType = this.navParams.get('cardType');
        if (this.cardType == 'Given')
            this.showAdd = true;
        this.getCards(this.cardType);
    }
    ThanksCardsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ThanksCardsPage');
    };
    ThanksCardsPage.prototype.getCards = function (cardtype) {
        var _this = this;
        try {
            this.ThanksCardProvider.getCards(cardtype)
                .then(function (data) {
                _this.cardDataAll = data;
                if (_this.cardDataAll)
                    _this.cardData = _this.cardDataAll.slice(_this.fromR, _this.toR);
                // handel add Button
                if (_this.showAdd) {
                    if (_this.cardData && _this.cardData.length > 0) {
                        if (_this.cardData[0].adding_state && _this.cardData[0].adding_state.toLowerCase() == 'success') {
                            _this.disableAdd = false;
                        }
                    }
                    if (_this.cardData && _this.cardData.length == 0) {
                        _this.disableAdd = false;
                    }
                }
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    ThanksCardsPage.prototype.newCard = function () {
        // try{
        //   this.ThanksCardProvider.checkAddCard( this.personId )
        //   .then(data => {
        //    if(data && data[0].message=='2')   // user couldn't add
        //    {
        //     AppModule.showMessage(this.AlertController,data[0].value)
        //    } 
        //    else  // user could add
        //    {
        //     this.navCtrl.push(NotificationActionsPage , {callingType: 3 ,title: 'Choose Person to Thank'});
        //    }
        //   });
        // }
        // catch (error)
        // {
        //   AppModule.showMessage(this.AlertController,'Error:'+error)
        // }
        //  
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__shared_pages_employees_employees__["a" /* EmployeesPage */], { callingType: 3, title: 'Choose Person to Thank' });
    };
    ThanksCardsPage.prototype.doRefresh = function (refresher) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.toR = this.v_step;
                        return [4 /*yield*/, this.getCards(this.cardType)];
                    case 1:
                        _a.sent();
                        refresher.complete();
                        return [2 /*return*/];
                }
            });
        });
    };
    ThanksCardsPage.prototype.doInfinite = function (infiniteScroll) {
        this.toR = this.toR + this.v_step;
        try {
            if (this.cardDataAll)
                this.cardData = (this.cardDataAll.slice(0, this.toR));
            infiniteScroll.complete();
        }
        catch (err) {
        }
        console.log(' infiniteScroll has ended');
    };
    ThanksCardsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-thanks-cards',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\thank-u\thanks-cards\thanks-cards.html"*/'<!--\n\n  Generated template for the ThanksCardsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{title}}</ion-title>\n\n    <ion-buttons end [hidden]="!showAdd" >\n\n        <button ion-button icon-only (click)="newCard()" [disabled]="disableAdd">\n\n          <ion-icon name="md-add" color="light"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header> \n\n\n\n\n\n<ion-content >\n\n\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content pullingIcon="arrow-dropdown"\n\n        pullingText="Pull to refresh"\n\n        refreshingSpinner="circles"\n\n        refreshingText="Refreshing..."></ion-refresher-content>\n\n      </ion-refresher>\n\n\n\n      <ion-list  >\n\n          <ion-item *ngFor="let item of cardData"  block >\n\n          \n\n       \n\n\n\n                <ion-avatar item-start> \n\n                     <!-- <ion-img [src]="\'data:image/jpg;base64,\'+item.img" [hidden]="item.img==null">\n\n                      </ion-img> -->\n\n                        <img [src]="sanitized.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+item.img)" [hidden]="item.img==null"/>\n\n                    <div class="{{item.class_name}}"  [hidden]="item.img!=null" >  {{item.intials}} </div>\n\n                   </ion-avatar>\n\n          \n\n\n\n                   <h2 class="list_item_title1" >  {{item.full_name}} </h2>\n\n                   <div class="list_item_sub_block">\n\n                   <div  class="list_item_sub_title2" > {{item.subject}} </div> \n\n                    <div class="list_item_sub_title3" > {{item.card_date}}</div>\n\n             </div>\n\n             <p class="list_item_sub_title1" > {{item.comments}} </p>\n\n        \n\n          </ion-item>\n\n        </ion-list>\n\n      \n\n\n\n\n\n        <ion-infinite-scroll (ionInfinite)="doInfinite($event)" > \n\n            <ion-infinite-scroll-content\n\n            loadingSpinner="bubbles"\n\n            loadingText="Loading more data...">\n\n          </ion-infinite-scroll-content> \n\n          </ion-infinite-scroll>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\thank-u\thanks-cards\thanks-cards.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_thanks_card_thanks_card__["a" /* ThanksCardProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], ThanksCardsPage);
    return ThanksCardsPage;
}());

//# sourceMappingURL=thanks-cards.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogInServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_aes_256__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var LogInServiceProvider = (function () {
    function LogInServiceProvider(http, httpClient, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
        this.logWSURL = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/RESTWs/getlogin?userName=';
    }
    LogInServiceProvider.prototype.xx = function (v_start, v_end) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        _this.httpClient.get('https://www.openpowerlifting.org/api/rankings?start=' + v_start + '&end=' + v_end + '&lang=en&units=lbs', {}).timeout(30000).subscribe(function (data) {
                            // console.log(JSON.stringify(data));
                            // console.log('data'); 
                            // console.log(data);  
                            var x = JSON.stringify(data);
                            x = x.replace('{"total_length":240707,"rows":[', '');
                            x = x.replace(']}', '');
                            //  x=x.replace(/],[/g,';')
                            //  x=x.replace([/gi,'');
                            // x=x.replace(']','')
                            //  x=x.replace('}','')
                            //  x=x.replace('{','')
                            //  x=x.replace(']]','')
                            //  x=x.replace(',[',';')
                            // x=x.replace(/]/g,'')
                            resolve(x);
                        }, function (err) {
                        });
                    })];
            });
        });
    };
    LogInServiceProvider.prototype.getUserAuthentication = function (user, userPass) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var k, v, t, signature, ws, authMethodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        k = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getComplex(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].index * 2);
                        v = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getComplex(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].index);
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHashing(userPass, k, v)];
                    case 1:
                        t = _a.sent();
                        signature = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doRev(k) + v + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getComplex(976);
                        ws = 'RESTWs/authenticate';
                        authMethodHeaders = {
                            's': user,
                            'av': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].appVersion,
                            'a': encodeURI(t),
                            'signature': encodeURI(signature),
                            'attribute1': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].appUpdate + ''
                        };
                        //   if (1==2)
                        // {
                        //   alert ('x')
                        //   return new Promise(resolve => {
                        //     this.httpClient.post(" http://127.0.0.1:7101/mbcWebservice1_1/resources/RESTWs/xx", null, { }).timeout(30000).subscribe(data => {
                        //   alert (data[0].value)
                        //     console.log(data)
                        //       //  resolve(data);
                        //       }
                        //       , err => {
                        //         console.log(err)
                        //         alert ('err')
                        //       }
                        //     );
                        //     });
                        // }
                        // else
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, authMethodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].intializeData(data);
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    LogInServiceProvider.prototype.registerAio = function (userPass) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'RESTWs/registerAio';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, userPass)];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    LogInServiceProvider.prototype.loginAio = function (tk, signature) {
        var _this = this;
        var ws = 'RESTWs/loginAio';
        var methodHeaders = {
            'signature': encodeURI(signature + ''),
            'tk': encodeURI(tk + ''),
            'av': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].appVersion,
            'attribute1': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].appUpdate + ''
        };
        return new Promise(function (resolve) {
            __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].intializeData(data);
                resolve(data);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
            });
        });
    };
    LogInServiceProvider.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'RESTWs/logout';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, null, null, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    LogInServiceProvider.prototype.getAouth = function (username, userpass) {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url = 'https://idcs-5fdca24dfdab4263b0f2341aebde4d78.identity.oraclecloud.com/oauth2/v1/token';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic OEY2MUZGRUUzMzAxNDc3N0IyQTg5OEI3QjQxMkQzRTRfQVBQSUQ6MjJmNzI4MDAtZjJkOS00OWYyLWI4OGQtMzBkM2NmNGFkNGYw'
        });
        var body = 'scope=https://8F61FFEE33014777B2A898B7B412D3E4.jcs.ocp.oraclecloud.com:443external&grant_type=password&username=' + username + '&password=' + userpass;
        return new Promise(function (resolve) {
            _this.httpClient.post(url, body, { headers: methodHeaders }).subscribe(function (data) {
                resolve(data);
                //  alert (data.access_token+'')
                console.log(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                console.log(err);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    LogInServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_aes_256__["a" /* AES256 */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */]])
    ], LogInServiceProvider);
    return LogInServiceProvider;
}());

//# sourceMappingURL=log-in-service.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimelineComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return TimelineItemComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return TimelineTimeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TimelineComponent = (function () {
    function TimelineComponent() {
        this.endIcon = "ionic";
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('endIcon'),
        __metadata("design:type", Object)
    ], TimelineComponent.prototype, "endIcon", void 0);
    TimelineComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'timeline',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\components\timeline\timeline.html"*/'<div class="timeline">\n\n  <ng-content></ng-content>\n\n\n\n  <!-- <timeline-item>\n\n    <ion-icon class="" [name]="endIcon"></ion-icon>\n\n  </timeline-item> -->\n\n\n\n</div>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\components\timeline\timeline.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TimelineComponent);
    return TimelineComponent;
}());

var TimelineItemComponent = (function () {
    function TimelineItemComponent() {
    }
    TimelineItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'timeline-item',
            template: '<ng-content></ng-content>'
        }),
        __metadata("design:paramtypes", [])
    ], TimelineItemComponent);
    return TimelineItemComponent;
}());

var TimelineTimeComponent = (function () {
    function TimelineTimeComponent() {
        this.time = {};
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('time'),
        __metadata("design:type", Object)
    ], TimelineTimeComponent.prototype, "time", void 0);
    TimelineTimeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'timeline-time',
            template: '<span>{{time.subtitle}}</span> <span>{{time.title}}</span>'
        }),
        __metadata("design:paramtypes", [])
    ], TimelineTimeComponent);
    return TimelineTimeComponent;
}());

//# sourceMappingURL=timeline.js.map

/***/ }),

/***/ 218:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 218;

/***/ }),

/***/ 262:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 262;

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the ProfileProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ProfileProvider = (function () {
    function ProfileProvider(http, httpClient, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
        console.log('Hello ProfileProvider Provider');
    }
    ProfileProvider.prototype.getPerssonalInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getBasicDetails';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getEmplomentSummary = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getEmploymentSummary';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getEmplomenyeePhones = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getPhones';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getEmplomenyeeAddresses = function (businessGroup) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getAddresses';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'bu': businessGroup
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getEmplomenyeePassportInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getPassportInfo';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getEmployeeVisa = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getVisaInfo';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getDependentVisa = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getDependentVisa';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getDependentTickets = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getDependentTickets';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getEmpoyeeLaborCardInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getLaborCardInfo';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getEmpoyeeRelationsInMBC = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getRelationsInMBC';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider.prototype.getEmpoyeegetNationalIdCard = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'profile/info/getNationalIdCard';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    ProfileProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */]])
    ], ProfileProvider);
    return ProfileProvider;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__leaves_leave_home_leave_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__approval_center_work_list_work_list__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__hr_request_hr_requests_hr_requests__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__payroll_payroll_home_payroll_home__ = __webpack_require__(546);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__profile_profile_home_profile_home__ = __webpack_require__(552);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__thank_u_thankscards_home_thankscards_home__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__dash_board_dash_board_dash_board__ = __webpack_require__(563);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__Scheduler_publicSpace_publicspace__ = __webpack_require__(577);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__Scheduler_orgs_orgs__ = __webpack_require__(582);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










//AA


/**
 * Generated class for the StartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StartPage = (function () {
    function StartPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = ['ApprovalCenter', 'Profile', 'Leaves', 'Payroll', 'HRrequests', 'Expense', 'Dashboard', 'Scheduler', 'thanksCard'];
        this.addURL = 'http://www.mbc.net/en/privacy';
        this.isHidden = [];
        this.isExecutive = __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('isExecutive');
        //Updated by Heba 6-4-2020 //
        this.isAA = __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('AAOrgCount');
        //AppModule.getProperty('AAorganization');
        //added By Heba 1-4-2020//
        this.isRegualrEmp = __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('EmpType');
        // end Added By heba //
    }
    StartPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StartPage');
        console.log(this.isAA);
    };
    StartPage.prototype.goPage = function (pageName) {
        pageName = __WEBPACK_IMPORTED_MODULE_2__leaves_leave_home_leave_home__["a" /* LeaveHomePage */];
        this.navCtrl.push(pageName, {});
    };
    StartPage.prototype.openApprovalCenter = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__approval_center_work_list_work_list__["a" /* WorkListPage */], {});
    };
    StartPage.prototype.openHRrequest = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__hr_request_hr_requests_hr_requests__["a" /* HrRequestsPage */], {});
    };
    StartPage.prototype.arrCompare = function (arr1) {
        // var a1 = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
        // var a2 = ['a', 'b', 'c', 'd', 'z', 'hey', 'there'];
        var p1 = this.items;
        var p2 = arr1;
        var i;
        var x;
        console.log('p2');
        console.log(p2);
        // hide 
        if (p2 != null) {
            var hide = p1.filter(function (item) { return p2.indexOf(item) < 0; });
            console.log('hide');
            console.log(hide);
            for (i = 0; i < hide.length; i++) {
                console.log(hide[i]);
                x = document.getElementById(hide[i]);
                x.style.display = "none";
            }
            for (i = 0; i < arr1.length; i++) {
                console.log(arr1[i]);
                x = document.getElementById(arr1[i]);
                x.style.display = "inline-block";
            }
        }
        else {
            for (i = 0; i < this.items.length; i++) {
                console.log(this.items[i]);
                x = document.getElementById(this.items[i]);
                x.style.display = "inline-block";
            }
        }
    };
    StartPage.prototype.arrCompare2 = function (arr1) {
        // var a1 = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
        // var a2 = ['a', 'b', 'c', 'd', 'z', 'hey', 'there'];
        var p1 = this.items;
        var p2 = arr1;
        var i;
        var x;
        console.log('p2');
        console.log(p2);
        // hide 
        if (p2 != null) {
            var hide = p1.filter(function (item) { return p2.indexOf(item) < 0; });
            for (i = 0; i < hide.length; i++) {
                //x=document.getElementById(hide[i]);
                // x.style.display="none";
                var t = p1.indexOf(hide[i]);
                this.isHidden[t] = true;
            }
            for (i = 0; i < arr1.length; i++) {
                // x=document.getElementById(arr1[i]);
                //x.style.display="inline-block";
                var t = p1.indexOf(arr1[i]);
                this.isHidden[t] = false;
            }
        }
        else {
            for (i = 0; i < this.items.length; i++) {
                console.log(this.items[i]);
                //   x=document.getElementById(this.items[i]);
                //  x.style.display="inline-block";
                this.isHidden[i] = false;
            }
        }
    };
    StartPage.prototype.onInput = function (ev) {
        var filterItems;
        console.log('xx' + this.searchItem);
        var val = ev.target.value;
        console.log('vv ' + val);
        if (val && val.trim() !== '') {
            filterItems = this.items.filter(function (item) {
                return item.toLowerCase().includes(val.toLowerCase());
            });
        }
        this.arrCompare2(filterItems);
        console.log('ff ' + this.items);
        // var x;
        //var y;
        //x=document.getElementById("yyx");
        // if (val=='1')
        // x.style.display="none";
        //  else
        // x.style.display="inline-block";
        // y=document.getElementById("ProfileTxt");
        // y.style.display="none";
    };
    StartPage.prototype.onCancel = function () {
        console.log('yy' + this.searchItem);
        var x;
        var y;
        x = document.getElementById("Profile");
        x.style.display = "inline-block";
        // y=document.getElementById("ProfileTxt");
        // y.style.display="inline-block";
    };
    StartPage.prototype.openPayroll = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__payroll_payroll_home_payroll_home__["a" /* PayrollHomePage */]);
    };
    StartPage.prototype.openThanks = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__thank_u_thankscards_home_thankscards_home__["a" /* ThankscardsHomePage */]);
    };
    StartPage.prototype.openProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__profile_profile_home_profile_home__["a" /* ProfileHomePage */]);
    };
    StartPage.prototype.openDashBoard = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__dash_board_dash_board_dash_board__["a" /* DashBoardPage */]);
    };
    // added by Heba 13-may-2019
    // updated by heba 5-4-2020
    StartPage.prototype.goPublicSpace = function () {
        console.log("OrgNum:" + __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('AAOrgCount'));
        console.log("url:" + __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].wsURL);
        __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].sAAwkId = '0';
        if (__WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('AAOrgCount') != '1') {
            __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].sAAmyspace = 'N';
            __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].sAAmyteam = 'N';
            __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].sAAworkgroups = 'N';
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__Scheduler_orgs_orgs__["a" /* OrgsPage */]);
        }
        else {
            //set show hide of myspace /myteam /workgruops
            __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].sAAmyspace = __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('myspace');
            __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].sAAmyteam = __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('myteam');
            __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].sAAworkgroups = __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('workgroups');
            __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].sAAOrganizationId = __WEBPACK_IMPORTED_MODULE_9__app_app_module__["a" /* AppModule */].getProperty('AAorganization');
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__Scheduler_publicSpace_publicspace__["a" /* PublicSpacePage */]);
        }
    };
    StartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-start',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\start\start.html"*/'<!--\n\n  Generated template for the StartPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header class="mmm">\n\n   \n\n<ion-searchbar  \n\n  [(ngModel)]="searchItem"\n\n  [showCancelButton]="shouldShowCancel"\n\n  animated=true\n\n  placeholder="All Services"\n\n  (ionInput)="onInput($event)"\n\n  (ionCancel)="onCancel($event)">\n\n</ion-searchbar>\n\n\n\n</ion-header>\n\n<ion-content  padding class="ion-content">\n\n \n\n\n\n<div class="mnb"  id="ApprovalCenter" [hidden]="isHidden[0]" *ngIf="isRegualrEmp!=\'UNEMP\'">\n\n  <button ion-button  color="dark"    class="ico-fa-btn1"   (click)="openApprovalCenter()">\n\n    <fa-icon name="calendar-check-o"  class="aw-ico1"></fa-icon>\n\n      \n\n  </button>\n\n  <h6 ion-text color="dark" class="btn-txt" >Approval</h6>\n\n  <h6 ion-text color="dark" class="btn-txt" >Center</h6>\n\n</div>\n\n\n\n \n\n\n\n<div class="mnb"  id="Profile" [hidden]="isHidden[1]" *ngIf="isRegualrEmp!=\'UNEMP\'">\n\n    <button ion-button  color="dark"  class="ico-fa-btn1"  (click)="openProfile()">\n\n        <fa-icon name="address-card"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">Profile  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n    \n\n</div>\n\n\n\n<div class="mnb"   id="Leaves" [hidden]="isHidden[2]" *ngIf="isRegualrEmp!=\'UNEMP\'"> \n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goPage(\'ListPag\')">\n\n        <span class="aw-ico2">\n\n        <fa-icon name="calendar"  class="aw-ico1"></fa-icon>\n\n        </span>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Leaves</h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n \n\n</div>\n\n\n\n\n\n\n\n<div class="mnb"  id="Payroll" [hidden]="isHidden[3]" *ngIf="isRegualrEmp!=\'UNEMP\'">\n\n        <button ion-button color="dark"    class="ico-fa-btn1" (click)="openPayroll()">\n\n            <fa-icon name="money"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n    \n\n        <h6 ion-text color="dark" class="btn-txt" >Payroll  </h6>\n\n        <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n    </div>\n\n<div class="mnb"  id="Letters" [hidden]="isHidden[4]" *ngIf="isRegualrEmp!=\'UNEMP\'">\n\n    <button ion-button  color="dark"   class="ico-fa-btn1"    (click)="openHRrequest()"   >\n\n        <fa-icon name="file-text-o"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt"  >HR    </h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >Requests</h6>\n\n</div>\n\n<div class="mnb"  id="Expense" [hidden]="isHidden[5]" *ngIf="isRegualrEmp!=\'UNEMP\'">\n\n    <button ion-button  color="dark"  disabled="true" class="ico-fa-btn1">\n\n        <fa-icon name="google-wallet"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n   \n\n    <h6 ion-text color="dark" class="btn-txt" >Expense  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n</div> \n\n\n\n<div class="mnb"  id="DashBoard" [hidden]="isHidden[6]" *ngIf="isExecutive==\'Y\'" >\n\n    <button ion-button  color="dark"  class="ico-fa-btn1" (click)="openDashBoard()">\n\n        <fa-icon name="pie-chart"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n   \n\n    <h6 ion-text color="dark" class="btn-txt" >Dashboards  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n</div> \n\n\n\n<div class="mnb"  id="Scheduler" [hidden]="isHidden[7]" *ngIf="isAA!=0">\n\n    <button ion-button  color="dark"  class="ico-fa-btn1"  (click)="goPublicSpace()">\n\n        <fa-icon name="television"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n   \n\n    <h6 ion-text color="dark" class="btn-txt" >Al Arabiya  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >Scheduler </h6>\n\n</div> \n\n \n\n<!-- keep this at last always -->\n\n<div class="mnb"  id="thanksCard" [hidden]="true">\n\n        <button ion-button  color="dark"  class="ico-fa-btn1"  (click)="openThanks()">\n\n            <fa-icon name="handshake-o"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n       \n\n        <h6 ion-text color="dark" class="btn-txt" >Thank </h6>\n\n        <h6 ion-text color="dark" class="btn-txt" >You</h6>\n\n    </div> \n\n   \n\n<!-- <div #frame style="width:100%;height:100%;overflow:scroll !important;-webkit-overflow-scrolling:touch !important">\n\n        <iframe src={{adURLx}}  ></iframe>\n\n      </div> -->\n\n\n\n\n\n\n\n\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\start\start.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]) === "function" && _b || Object])
    ], StartPage);
    return StartPage;
    var _a, _b;
}());

//# sourceMappingURL=start.js.map

/***/ }),

/***/ 526:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__start_start__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__approval_center_work_list_work_list__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__more_more__ = __webpack_require__(583);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_social_social__ = __webpack_require__(584);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MainPage = (function () {
    function MainPage(navCtrl, navParams, modalCtrl, SocialProvider, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.SocialProvider = SocialProvider;
        this.platform = platform;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__start_start__["a" /* StartPage */];
        this.inBoxRoot = __WEBPACK_IMPORTED_MODULE_3__approval_center_work_list_work_list__["a" /* WorkListPage */];
        this.more = __WEBPACK_IMPORTED_MODULE_4__more_more__["a" /* MorePage */];
        this.tab2Root = null;
        this.tab3Root = null;
        this.noOfNotifications = __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].notificationNO;
        this.showHappinees = true;
        this.isUp = false;
    }
    MainPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MainPage');
    };
    MainPage.prototype.GetNoOfNotifications = function () {
        return window.localStorage.getItem('noOfNotifications') ? window.localStorage.getItem('noOfNotifications') : '';
    };
    MainPage.prototype.showHappiness = function () {
        // let modal = this.modalCtrl.create(WorkListPage);
        //modal.present();
        var x;
        x = document.getElementsByClassName("main-happiness-container")[0];
        var y = document.getElementsByClassName("smiles-div")[0];
        if (this.showHappinees) {
            // show
            this.showHappinees = false;
            this.tabClass1 = 'tab-class1';
            //
            // $(x).addClass('leng-elemnt');
            // $(x).removeClass('short-elemnt');
        }
        else {
            if (this.selectedFace == 1 || this.selectedFace == 0) {
                this.sendAction();
                this.clearData();
            }
            else {
                this.clearData();
            }
            // $(x).addClass('short-elemnt');
            // $(x).removeClass('leng-elemnt');
        }
        //this.showHappinees=true;
        //
    };
    MainPage.prototype.happinessSetAction = function (smile) {
        // this.showHappinees=true;
        if (smile == '1') {
            this.smileClass1 = 'smile-fa-btn1 dark-bg btn-rotate';
            this.smileIcon1 = '#fff';
            this.smileClass2 = '';
            this.smileIcon2 = '#353D4e';
            this.smileClass3 = '';
            this.smileIcon3 = '#353D4e';
            this.selectedFace = 1;
            this.buttonTxt = 'Happy >>';
        }
        if (smile == '2') {
            this.smileClass2 = 'smile-fa-btn1 dark-bg btn-rotate';
            this.smileIcon2 = '#fff';
            this.smileClass1 = '';
            this.smileIcon1 = '#353D4e';
            this.smileClass3 = '';
            this.smileIcon3 = '#353D4e';
            this.selectedFace = 0;
            this.buttonTxt = 'Neutral >>';
        }
        if (smile == '3') {
            this.smileClass3 = 'smile-fa-btn1 dark-bg btn-rotate';
            this.smileIcon3 = '#fff';
            this.smileClass1 = '';
            this.smileIcon1 = '#353D4e';
            this.smileClass2 = '';
            this.smileIcon2 = '#353D4e';
            this.selectedFace = -1;
            this.buttonTxt = 'Sad    >>';
        }
        //
    };
    MainPage.prototype.hideContainer = function (event) {
        //  this.showHappinees=true;
        //
        var source = event.target.id;
        var classesName = event.target.className;
        if (source && source == 'main-div') {
            if (this.selectedFace == 1 || this.selectedFace == 0) {
                this.sendAction();
                this.clearData();
            }
            else {
                this.clearData();
            }
        }
        else {
            if (classesName && classesName.indexOf('input') > 1) {
            }
            else
                this.goDown();
        }
    };
    MainPage.prototype.sendAction = function () {
        try {
            this.SocialProvider.sendHappiness(this.selectedFace, this.comments, '1');
            this.clearData();
        }
        catch (err) {
        }
    };
    MainPage.prototype.clearData = function () {
        this.selectedFace = null;
        this.comments = null;
        this.showHappinees = true;
        this.smileClass1 = '';
        this.smileIcon1 = '#353D4e';
        this.smileClass2 = '';
        this.smileIcon2 = '#353D4e';
        this.smileClass3 = '';
        this.smileIcon3 = '#353D4e';
        this.tabClass1 = '';
        this.goDown();
    };
    MainPage.prototype.goUp = function () {
        if (this.platform.is('android')) {
            if (!this.isUp) {
                this.isUp = true;
                this.divClass = 'up-class';
            }
        }
    };
    MainPage.prototype.goDown = function () {
        this.isUp = false;
        this.divClass = '';
    };
    MainPage.prototype.goOut = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myTabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Tabs */])
    ], MainPage.prototype, "tabRef", void 0);
    MainPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-main',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\main\main.html"*/'<!-- <ion-content class="sxxp"> -->\n\n<ion-tabs #myTabs [ngClass]="tabClass1">\n\n       \n\n    <ion-tab [root]="tab1Root" tabTitle="Home" [rootParams]="tab1Params" tabIcon="apps"></ion-tab>\n\n    <ion-tab tabs-only (ionSelect)="showHappiness()"tabTitle="Happiness" tabIcon="happy" ></ion-tab>\n\n    <ion-tab [root]="inBoxRoot" tabTitle="Inbox" [tabBadge]="GetNoOfNotifications()"  [rootParams]="tab1Params" tabIcon="notifications"></ion-tab>\n\n    <ion-tab [root]="tab2Root" tabTitle="Profile" tabIcon="ios-contact"></ion-tab>\n\n\n\n    <ion-tab [root]="more" tabTitle="More" tabIcon="ios-more" ></ion-tab> \n\n\n\n\n\n  <div class="main-happiness-container" id=\'main-div\' (click)="hideContainer($event)"  [@mainvisibilityChanged]="showHappinees"  >\n\n \n\n    <div class="smiles-div"    [ngClass]="divClass" id=\'sub-div\'>\n\n\n\n            <div class="smile-text" >\n\n                How was your experience?\n\n            </div>\n\n          <div class="ico-div ico-div-left"  >\n\n              <button ion-button  color="dark"   icon-only outline  class="smile-fa-btn1"    (click)="happinessSetAction(1)"   [ngClass]="smileClass1" >\n\n                <fa-icon name="smile-o"  class="smile-ico1" [style.color]="smileIcon1"></fa-icon>\n\n                  \n\n              </button>\n\n             \n\n            </div>\n\n            <div class="ico-div"  >\n\n                <button ion-button   color="dark"  icon-only  outline class="smile-fa-btn1"  (click)="happinessSetAction(2)"  [ngClass]="smileClass2">\n\n                    <fa-icon name="meh-o"  class="smile-ico1"  [style.color]="smileIcon2"></fa-icon>\n\n                </button>\n\n               \n\n            </div>\n\n            \n\n            <div class="ico-div"  >\n\n                <button ion-button  color="dark"  icon-only outline   class="smile-fa-btn1"  (click)="happinessSetAction(3)"  [ngClass]="smileClass3">\n\n                    <fa-icon name="frown-o"  class="smile-ico1" [style.color]="smileIcon3" ></fa-icon>\n\n                </button>\n\n               \n\n               \n\n            </div>\n\n    \n\n            <ion-textarea id="txt-c" [(ngModel)]="comments" placeholder="Tell us more" autocomplete="on" autocorrect="on"  class="txt-area-h" (ionBlur)="goOut()" (ionFocus)="goUp()"></ion-textarea>\n\n\n\n            <div class="send-div"  *ngIf="selectedFace!=null">\n\n                <button ion-button  color="dark"  small  class="send-btn1"    (click)="sendAction()"    >\n\n                  {{buttonTxt}}\n\n                    \n\n                </button>\n\n\n\n      </div>\n\n\n\n      </div> \n\n\n\n      </div>\n\n    \n\n\n\n\n\n</ion-tabs>\n\n\n\n  '/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\main\main.html"*/,
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('visibilityChanged', [
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('false', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ opacity: '1' })),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('true', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ opacity: '0' })),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('true => *', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('3s')),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('false => *', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('3s'))
                ]),
                Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('mainvisibilityChanged', [
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('false', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ height: '100%' })),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('true', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ height: '0px' })),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('true => *', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.5s')),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('false => *', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.3s'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_social_social__["a" /* SocialProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */]])
    ], MainPage);
    return MainPage;
}());

//# sourceMappingURL=main.js.map

/***/ }),

/***/ 527:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaveHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__leave_details_leave_details__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_sshr_leaves_sshr_leaves__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__new_leave_new_leave__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__leave_home_leave_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_aes_256__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the LeaveHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

var LeaveHistoryPage = (function () {
    function LeaveHistoryPage(navCtrl, navParams, toastCtrl, leavesHistory, loadingController, aes256) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.leavesHistory = leavesHistory;
        this.loadingController = loadingController;
        this.aes256 = aes256;
        this.appovedLeavesCount = 0;
        this.pendingLeavescount = 0;
        try {
            this.leavesHistory.getPendingLeavesCount()
                .then(function (data) {
                _this.pendingLeavescount = data[0].pending_count;
                if (_this.pendingLeavescount != 0) {
                    try {
                        _this.leavesHistory.getPendingLeaves()
                            .then(function (data) {
                            _this.pendingLeaves = data;
                        });
                    }
                    catch (error) {
                        _this.showToast('middle', 'Connection Problem');
                    }
                }
            });
        }
        catch (error) {
            this.showToast('middle', 'error');
        }
        try {
            this.leavesHistory.getApprovedLeavesCount()
                .then(function (data) {
                _this.appovedLeavesCount = data[0].approvedcount;
                if (_this.appovedLeavesCount != 0) {
                    try {
                        _this.leavesHistory.getApprovedLeaves()
                            .then(function (data) {
                            _this.appovedLeaves = data;
                        });
                    }
                    catch (error) {
                        _this.showToast('middle', 'Connection Problem');
                    }
                }
            });
        }
        catch (error) {
            this.showToast('middle', 'Connection Problem');
        }
        // loader.dismiss();
    }
    LeaveHistoryPage.prototype.showToast = function (position, txt) {
        var x = this.loadingController.create({
            spinner: 'hide',
            content: " ",
        });
        x.present();
        var toast = this.toastCtrl.create({
            message: txt,
            position: position,
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.onDidDismiss(function () { return x.dismiss(); });
        toast.present(toast);
    };
    LeaveHistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LeaveHistoryPage');
    };
    LeaveHistoryPage.prototype.leaveTapped = function (event, item, lType) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__leave_details_leave_details__["a" /* LeaveDetailsPage */], {
            item: item,
            leaveType: lType
        });
    };
    LeaveHistoryPage.prototype.newLeave = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__new_leave_new_leave__["a" /* NewLeavePage */], { userId: this.userId, personId: this.personId, businessGroup: this.businessGroup, assignmentId: this.assignmentId });
    };
    LeaveHistoryPage.prototype.backPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__leave_home_leave_home__["a" /* LeaveHomePage */], { userId: this.userId, personId: this.personId, businessGroup: this.businessGroup, assignmentId: this.assignmentId });
    };
    LeaveHistoryPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        try {
            this.leavesHistory.getPendingLeavesCount()
                .then(function (data) {
                _this.pendingLeavescount = data[0].pending_count;
                if (_this.pendingLeavescount != 0) {
                    try {
                        _this.leavesHistory.getPendingLeaves()
                            .then(function (data) {
                            _this.pendingLeaves = data;
                        });
                    }
                    catch (error) {
                        _this.showToast('middle', 'Connection Problem');
                    }
                }
            });
        }
        catch (error) {
            this.showToast('middle', 'error');
        }
        try {
            this.leavesHistory.getApprovedLeavesCount()
                .then(function (data) {
                _this.appovedLeavesCount = data[0].approvedcount;
                if (_this.appovedLeavesCount != 0) {
                    try {
                        _this.leavesHistory.getApprovedLeaves()
                            .then(function (data) {
                            _this.appovedLeaves = data;
                        });
                    }
                    catch (error) {
                        _this.showToast('middle', 'Connection Problem');
                    }
                }
            });
        }
        catch (error) {
            this.showToast('middle', 'Connection Problem');
        }
        refresher.complete();
    };
    LeaveHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave-history',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\leave-history\leave-history.html"*/'<!--\n\n  Generated template for the LeaveHistoryPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar >\n\n    <!-- <ion-buttons left>\n\n    <button ion-button icon-only (click)="backPage()">\n\n      <ion-icon name="arrow-back" color="light"></ion-icon>\n\n    </button>\n\n    </ion-buttons> -->\n\n    <ion-title >Leave History </ion-title>\n\n   \n\n    <ion-buttons end>\n\n        <button ion-button icon-only (click)="newLeave()">\n\n          <ion-icon name="md-add" color="light"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n</ion-navbar>\n\n  \n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown"\n\n    pullingText="Pull to refresh"\n\n    refreshingSpinner="circles"\n\n    refreshingText="Refreshing..."></ion-refresher-content>\n\n  </ion-refresher>\n\n\n\n  <ion-item-divider color="light"  *ngIf="pendingLeavescount!=0" item-end>Pending Leaves ({{pendingLeavescount}}) </ion-item-divider >\n\n    <ion-list  >\n\n        <button ion-item *ngFor="let item of pendingLeaves" (click)="leaveTapped($event, item , 0)" block >\n\n        \n\n          \n\n          <div class="item-note typec" >\n\n              <ion-icon  name="ios-help-outline" item-start class="ico2"></ion-icon>\n\n              {{item.absence_category}}\n\n            </div>\n\n          <div class="item-note dates" >\n\n            {{item.leave_desc}}\n\n          </div>\n\n          <div class="item-note durationc" >\n\n              {{item.absence_no_days}}\n\n            </div>\n\n            <!-- <div class="item-note remarksc" >\n\n                {{item.start_date}}\n\n              </div> -->\n\n        </button>\n\n      </ion-list>\n\n      <ion-item-divider color="light" *ngIf="appovedLeavesCount!=0" >Approved Leaves ({{appovedLeavesCount}})</ion-item-divider>\n\n    <ion-list  >\n\n        <button ion-item *ngFor="let item of appovedLeaves" (click)="leaveTapped($event, item ,1)" block >\n\n        \n\n          \n\n          <div class="item-note typec" >\n\n              <ion-icon  name="checkmark-circle-outline" item-start class="ico1"></ion-icon>\n\n              {{item.absence_category}} \n\n            </div>\n\n          <div class="item-note dates" >\n\n            {{item.leave_desc}}\n\n           \n\n          </div>\n\n          <div class="item-note durationc" >\n\n              {{item.absence_no_days}}\n\n            </div>\n\n            <!-- <div class="item-note remarksc" >\n\n                {{item.start_date}}\n\n              </div> -->\n\n        </button>\n\n      </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\leave-history\leave-history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_sshr_leaves_sshr_leaves__["a" /* SshrLeavesProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_aes_256__["a" /* AES256 */]])
    ], LeaveHistoryPage);
    return LeaveHistoryPage;
}());

//# sourceMappingURL=leave-history.js.map

/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaveDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the LeaveDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LeaveDetailsPage = (function () {
    function LeaveDetailsPage(navCtrl, navParams, leavesProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.leavesProvider = leavesProvider;
        this.leaveType = 'Pending Leave';
        this.selectedLeave = navParams.get('item');
        // this.leaveType = navParams.get('leaveType');
        if (navParams.get('leaveType') == '1')
            this.leaveType = 'Approved Leave';
        if (this.leaveType == 'Pending Leave') {
            this.getApproversHistory();
        }
    }
    LeaveDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LeaveDetailsPage');
    };
    LeaveDetailsPage.prototype.getApproversHistory = function () {
        var _this = this;
        try {
            this.leavesProvider.getleaveHistory(this.selectedLeave.item_type, this.selectedLeave.item_key)
                .then(function (data) {
                _this.leaveHistory = data;
                if (_this.leaveHistory && _this.leaveHistory.length == 0)
                    _this.leaveHistory = null;
            });
        }
        catch (error) {
        }
    };
    LeaveDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave-details',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\leave-details\leave-details.html"*/'<!--\n\n  Generated template for the LeaveDetailsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Leave Details</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <ion-item-divider color="light"> {{leaveType}}</ion-item-divider>\n\n  <ion-list>\n\n\n\n    <ion-item>\n\n      <div class="item-note typec">\n\n\n\n        Type\n\n      </div>\n\n      <div class="item-note typsesc" item-end>\n\n        {{selectedLeave.absence_category}}\n\n      </div>\n\n\n\n    </ion-item>\n\n    <ion-item>\n\n      <div class="item-note typec">\n\n\n\n        Date From\n\n      </div>\n\n      <div class="item-note typsesc" item-end>\n\n        {{selectedLeave.start_date}}\n\n      </div>\n\n\n\n    </ion-item>\n\n    <ion-item>\n\n      <div class="item-note typec">\n\n\n\n          Date To\n\n      </div>\n\n      <div class="item-note typsesc" item-end>\n\n        {{selectedLeave.end_date}}\n\n      </div>\n\n\n\n    </ion-item>\n\n    <ion-item>\n\n      <div class="item-note typec">\n\n\n\n        Duration\n\n      </div>\n\n      <div class="item-note typsesc" item-end>\n\n        {{selectedLeave.absence_no_days}}\n\n      </div>\n\n\n\n    </ion-item>\n\n    <ion-item>\n\n      <div class="item-note typec">\n\n\n\n        Return Date      \n\n      </div>\n\n      <div class="item-note typsesc" item-end>\n\n        {{selectedLeave.return_date}}\n\n      </div>\n\n\n\n    </ion-item>\n\n   \n\n  <ion-item>\n\n  <div class="item-note typec">\n\n\n\n      Half Day\n\n  </div>\n\n  <div class="item-note typsesc" item-end>\n\n    {{selectedLeave.half_day}}\n\n  </div>\n\n\n\n</ion-item>\n\n<ion-item>\n\n    <div class="item-note typec">\n\n  \n\n        Replaced by \n\n    </div>\n\n    <div class="item-note typsesc" item-end>\n\n      {{selectedLeave.replaced_person_name}}\n\n    </div>\n\n  \n\n  </ion-item>\n\n\n\n<!-- <ion-item>\n\n<div class="item-note typec">\n\n\n\n    Level of Sickness\n\n</div>\n\n<div class="item-note typsesc2" >\n\n  {{selectedLeave.level_of_sickness}}\n\n</div>\n\n\n\n</ion-item> -->\n\n\n\n<ion-item>\n\n<div class="item-note typec">\n\n\n\n    Compassionate Reason\n\n</div>\n\n<div class="item-note typsesc2" >\n\n  {{selectedLeave.compassionate_reason}}\n\n</div>\n\n\n\n</ion-item>\n\n<ion-item>\n\n    <div class="item-note typec">\n\n  \n\n        Comments\n\n    </div>\n\n    <div class="item-note typsesc2" >\n\n      {{selectedLeave.comments}}\n\n    </div>\n\n  \n\n  </ion-item>\n\n\n\n  <ion-item-divider color="light" class="divider-class1" *ngIf="leaveHistory!=null"  item-end> History </ion-item-divider >\n\n   \n\n    <ion-spinner name="circles" class="spiner-1"  *ngIf="leaveHistory==null && leaveType==\'Pending Leave\' " >\n\n      </ion-spinner>\n\n   \n\n  \n\n    <ion-list  >\n\n    \n\n      \n\n\n\n    <ion-item *ngFor="let item of leaveHistory"  >\n\n    \n\n      \n\n      <div class="item-note histroy-name-class" >\n\n          {{item.name}}\n\n         \n\n          <span class="histroy-action-class"   >{{item.action}}</span>\n\n            \n\n        </div>\n\n\n\n        <div class="item-note histroy-note-class" *ngIf="item.note!=null">\n\n         \n\n           \n\n                {{item.note}}\n\n              \n\n          </div>\n\n\n\n\n\n      \n\n    \n\n    </ion-item>\n\n  </ion-list>\n\n\n\n\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\leave-details\leave-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__["a" /* SshrLeavesProvider */]])
    ], LeaveDetailsPage);
    return LeaveDetailsPage;
}());

//# sourceMappingURL=leave-details.js.map

/***/ }),

/***/ 529:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewLeaveSubmitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__leave_home_leave_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the NewLeaveSubmitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewLeaveSubmitPage = (function () {
    function NewLeaveSubmitPage(navCtrl, navParams, leavesProvider, loadingController, _DomSanitizer, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.leavesProvider = leavesProvider;
        this.loadingController = loadingController;
        this._DomSanitizer = _DomSanitizer;
        this.AlertController = AlertController;
        this.approverList = navParams.get('approverList');
        this.selectedLeave = navParams.get('leaveDetails');
        this.imgx = "data:image/jpg;base64," + this.approverList[0].imge;
        this.imgx2 = _DomSanitizer.bypassSecurityTrustUrl(this.imgx);
        this.attachIdx = navParams.get('pAttachId');
        this.attchCount = navParams.get('attchCount');
        if (this.approverList) {
            this.duration = this.approverList[0].duration;
            if (this.duration == '1')
                this.duration = this.duration + ' Day';
            else
                this.duration = this.duration + ' Days';
        }
        else {
            this.duration = '0';
        }
    }
    NewLeaveSubmitPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewLeaveSubmitPage');
    };
    NewLeaveSubmitPage.prototype.submitleave = function () {
        var _this = this;
        try {
            this.leavesProvider.newLeaveRequest(this.selectedLeave.dateStart, this.selectedLeave.dateEnd, this.selectedLeave.absenceAttendanceTypeId, this.selectedLeave.absenceTypeName, 'Y', this.selectedLeave.attribute1, this.selectedLeave.attribute2, '', this.selectedLeave.attribute4, '', '', this.selectedLeave.attribute7, this.selectedLeave.attribute8, this.attachIdx, null, this.selectedLeave.selectedreplacedPesonId)
                .then(function (data) {
                if (data[0].message == '2') {
                    // this.showToast('middle' , data[0].value);
                    __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                //  this.showToast('middle' , 'Submitted');
                _this.navCtrl.push(
                //this.navCtrl.setRoot(
                __WEBPACK_IMPORTED_MODULE_3__leave_home_leave_home__["a" /* LeaveHomePage */], { userId: _this.selectedLeave.userId, personId: _this.selectedLeave.personId, businessGroup: _this.selectedLeave.businessGroup, assignmentId: _this.selectedLeave.assignmentId });
                //return;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, error.message);
            // this.showToast('middle','Connection Problem');
        }
    };
    NewLeaveSubmitPage.prototype.showToast = function (position, txt) {
        var x = this.loadingController.create({
            spinner: 'hide',
            content: " ",
        });
    };
    NewLeaveSubmitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-new-leave-submit',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\new-leave-submit\new-leave-submit.html"*/'<!--\n\n  Generated template for the NewLeaveSubmitPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar >\n\n    <ion-title>Submit Leave </ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <ion-item-divider color="light"> Leave details</ion-item-divider>\n\n    <ion-list>\n\n  \n\n      <ion-item>\n\n        <div class="item-note typec">\n\n  \n\n          Type\n\n        </div>\n\n        <div class="item-note typsesc" item-end>\n\n          {{selectedLeave.absenceTypeName}}\n\n        </div>\n\n  \n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="item-note typec">\n\n  \n\n          Date From\n\n        </div>\n\n        <div class="item-note typsesc" item-end >\n\n          {{selectedLeave.dateStart | date: \'dd-MMM-yyyy\'}}\n\n        </div>\n\n  \n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="item-note typec">\n\n  \n\n            Date To\n\n        </div>\n\n        <div class="item-note typsesc" item-end>\n\n          {{selectedLeave.dateEnd | date: \'dd-MMM-yyyy\'}}\n\n        </div>\n\n  \n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="typec">\n\n    \n\n            Duration\n\n          <span class="list-value">\n\n              {{duration}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="item-note typec">\n\n  \n\n          Return Date      \n\n        </div>\n\n        <div class="item-note typsesc" item-end>\n\n          {{selectedLeave.attribute1 | date: \'dd-MMM-yyyy\'}}\n\n        </div>\n\n  \n\n      </ion-item>\n\n     \n\n    <ion-item>\n\n    <div class="item-note typec">\n\n  \n\n        Half Day\n\n    </div>\n\n    <div class="item-note typsesc" item-end>\n\n      {{selectedLeave.attribute4}}\n\n    </div>\n\n  \n\n  </ion-item>\n\n\n\n\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n    \n\n            Replaced by\n\n          <span class="list-value">\n\n              {{selectedLeave.selectedreplacedPesonName}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n     \n\n\n\n  <!-- <ion-item>\n\n  <div class="item-note typec">\n\n  \n\n      Level of Sickness\n\n  </div>\n\n  <div class="item-note typsesc2" >\n\n    {{selectedLeave.attribute7}}\n\n  </div>\n\n  \n\n  </ion-item> -->\n\n  \n\n  <ion-item>\n\n  <div class="item-note typec">\n\n  \n\n      Compassionate Reason\n\n  </div>\n\n  <div class="item-note typsesc2" >\n\n    {{selectedLeave.attribute8}}\n\n  </div>\n\n  \n\n  </ion-item>\n\n  <ion-item>\n\n      <div class="item-note typec">\n\n    \n\n          Comments\n\n      </div>\n\n      <div class="item-note typsesc2" >\n\n        {{selectedLeave.attribute2}}\n\n      </div>\n\n    \n\n    </ion-item>\n\n\n\n    <ion-item>\n\n                   \n\n        <div class="x4">\n\n            <ion-icon name="md-attach"  class="x4"> </ion-icon>\n\n            <i class="attch-no">{{attchCount}}</i>\n\n            </div>\n\n\n\n            <div class="item-note typec">\n\n    \n\n            \n\n            Attachment(s)\n\n        \n\n        </div>\n\n        </ion-item>\n\n\n\n    </ion-list>\n\n    <ion-item-divider color="light"   item-end>Approvers</ion-item-divider >\n\n      <ion-list  >\n\n          \n\n          <ion-item ion-item *ngFor="let item of approverList"  block >\n\n          \n\n            \n\n            <!-- <div class="item-note typec2" > -->\n\n               \n\n                <ion-avatar item-start>\n\n                   <img [src]="_DomSanitizer.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+item.imge)" [hidden]="item.imge==null" />\n\n                   <div class="divx" [hidden]="item.imge!=null"  >  {{item.intials}} </div>\n\n                  </ion-avatar>\n\n                  <h2 class="item-note typec2">\n\n                {{item.display_name}}\n\n                </h2 >\n\n              <!-- </div> -->\n\n            <p class="item-note dates2" >\n\n              {{item.job_title}}\n\n            </p>\n\n            <!-- <div class="item-note durationc" >\n\n                {{item.absence_no_days}}\n\n              </div> -->\n\n              <!-- <div class="item-note remarksc" >\n\n                  {{item.start_date}}\n\n                </div> -->\n\n          </ion-item>\n\n        </ion-list>\n\n      \n\n</ion-content>\n\n<ion-footer>\n\n  \n\n  <button ion-button block  color="dark" (click)="submitleave()">Submit</button>\n\n\n\n</ion-footer>\n\n\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\new-leave-submit\new-leave-submit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__["a" /* SshrLeavesProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], NewLeaveSubmitPage);
    return NewLeaveSubmitPage;
}());

//# sourceMappingURL=new-leave-submit.js.map

/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NtfactionSubmitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__work_list_work_list__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_work_list_ntf_work_list_ntf__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the NtfactionSubmitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NtfactionSubmitPage = (function () {
    function NtfactionSubmitPage(navCtrl, navParams, WorkListNtf, viewCtrl, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.WorkListNtf = WorkListNtf;
        this.viewCtrl = viewCtrl;
        this.AlertController = AlertController;
        this.comments_class = null;
        this.hr = '';
        this.overtime = '';
        this.expenses = '';
        this.requisitions = '';
        this.po = '';
        this.others = '';
        this.title = this.navParams.get('title');
        this.selectedUser = this.navParams.get('user');
        this.notificationId = this.navParams.get('notificationId');
        this.hr = this.navParams.get('hr');
        this.overtime = this.navParams.get('overtime');
        this.expenses = this.navParams.get('expenses');
        this.requisitions = this.navParams.get('requisitions');
        this.po = this.navParams.get('po');
        this.others = this.navParams.get('others');
    }
    NtfactionSubmitPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NtfactionSubmitPage');
    };
    NtfactionSubmitPage.prototype.takeAcion = function () {
        if (!this.comments) {
            // var el;
            // el=document.getElementsByClassName('comments')[0];
            // $(el).addClass('error-color');
            this.comments_class = 'error-color';
            return;
        }
        else {
            // submit;
            this.takeReassignAction();
        }
    };
    NtfactionSubmitPage.prototype.takeReassignAction = function () {
        var _this = this;
        this.WorkListNtf.takeReassignAction(this.notificationId, this.selectedUser.m, this.comments, this.title.toUpperCase())
            .then(function (data) {
            if (!data && data[0].value != 'Success') {
                __WEBPACK_IMPORTED_MODULE_4__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__work_list_work_list__["a" /* WorkListPage */], { hr: _this.hr,
                    overtime: _this.overtime,
                    expenses: _this.expenses,
                    requisitions: _this.requisitions,
                    po: _this.po,
                    others: _this.others });
            }
        });
    };
    NtfactionSubmitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ntfaction-submit',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\ntfaction-submit\ntfaction-submit.html"*/'<!--\n\n  Generated template for the NtfactionSubmitPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{title}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content >\n\n \n\n<ion-item class="selected-user">\n\n      \n\n     \n\n    <ion-avatar item-start> \n\n    \n\n      <div class="{{selectedUser.class_name}}"   >  {{selectedUser.intials}} </div>\n\n     </ion-avatar>\n\n    \n\n     <h2 class="list_item_title1" >{{selectedUser.g}}</h2>\n\n    \n\n    <div  class="list_item_sub_title2" >{{selectedUser.m}}  </div> \n\n\n\n\n\n</ion-item>\n\n \n\n\n\n    <ion-label class="txt-area-label-class" [ngClass]="comments_class" >Comments<i class="req-symbol">*</i></ion-label>\n\n    \n\n    \n\n<ion-item>\n\n    <ion-textarea  [(ngModel)]="comments" class="txt-area" ></ion-textarea>\n\n  \n\n</ion-item>\n\n\n\n\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n\n\n    <button ion-button  color="dark" block (click)="takeAcion()">Send</button>\n\n   \n\n</ion-footer>\n\n\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\ntfaction-submit\ntfaction-submit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_work_list_ntf_work_list_ntf__["a" /* WorkListNtfProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], NtfactionSubmitPage);
    return NtfactionSubmitPage;
}());

//# sourceMappingURL=ntfaction-submit.js.map

/***/ }),

/***/ 532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_work_list_ntf_work_list_ntf__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__work_list_work_list__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_pages_employees_employees__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_pages_look_up_look_up__ = __webpack_require__(533);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the NotificationDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NotificationDetailsPage = (function () {
    function NotificationDetailsPage(navCtrl, navParams, sanitized, loadingController, workListProvider, platform, actionsheetCtrl, modalCtrl, viewCtrl, AlertController) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sanitized = sanitized;
        this.loadingController = loadingController;
        this.workListProvider = workListProvider;
        this.platform = platform;
        this.actionsheetCtrl = actionsheetCtrl;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.AlertController = AlertController;
        this.hr = '';
        this.overtime = '';
        this.expenses = '';
        this.requisitions = '';
        this.po = '';
        this.others = '';
        this.hr = this.navParams.get('hr');
        this.overtime = this.navParams.get('overtime');
        this.expenses = this.navParams.get('expenses');
        this.requisitions = this.navParams.get('requisitions');
        this.po = this.navParams.get('po');
        this.others = this.navParams.get('others');
        this.ntfAttributesTextValues = [];
        this.ntfAttributesNumberValues = [];
        this.ntfAttributesRolesValues = [];
        this.ntfAttributeslookUpValues = [];
        this.notificationBody = this.navParams.get('notificationBody');
        //this.moreActions = this.navParams.get('moreActions');
        this.baseActions = null;
        this.baseActions2 = this.navParams.get('baseActions');
        this.wf = this.navParams.get('wf');
        //this.baseActions2.reverse();
        var xx = "";
        this.notificationBody = this.sanitized.bypassSecurityTrustHtml(this.notificationBody);
        this.getNotificationAttributes();
        // get approvers history 
        try {
            this.workListProvider.getNotificationHistory(this.wf.notification_id, this.wf.item_type, this.wf.item_key, this.wf.process_activity, this.wf.filter_type)
                .then(function (data) {
                _this.NotificationHistory = data;
                if (_this.NotificationHistory && _this.NotificationHistory.length == 0)
                    _this.NotificationHistory = null;
            });
        }
        catch (error) {
        }
    }
    NotificationDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificationDetailsPage');
    };
    NotificationDetailsPage.prototype.morex = function (bu) {
        var wf = this.wf;
        if (bu.button_name == 'MORE') {
            this.openMenu(wf);
        }
        else if (bu.button_name == 'MORE') {
            null;
        }
        else if (bu.button_name == 'MORE_INFORMATION_REQUEST') {
            this.takeReaasignAction('More Information', wf.notification_id);
        }
        else if (bu.button_name == 'REASSIGN') {
            this.takeReaasignAction('Reassign', wf.notification_id);
        }
        else {
            this.takeNotificationAction(wf.notification_id, wf.item_type, wf.item_key, wf.process_activity, bu.button_name);
        }
    };
    NotificationDetailsPage.prototype.openMenu = function (wf) {
        var _this = this;
        var bu = wf.more_actions;
        this.moreActions = [];
        var _loop_1 = function (i) {
            this_1.moreActions.push({
                text: bu[i].button_label,
                icon: !this_1.platform.is('ios') ? bu[i].button_icon : null,
                handler: function () { _this.actionSheetHandler(bu[i].button_name, wf); }
            });
        };
        var this_1 = this;
        for (var i = 0; i < bu.length; i++) {
            _loop_1(i);
        }
        // adding cancel 
        this.moreActions.push({
            text: 'Cancel',
            role: 'cancel',
            icon: !this.platform.is('ios') ? 'close' : null,
            handler: function () { _this.actionSheetHandler('cancel', wf); }
        });
        var actionSheet = this.actionsheetCtrl.create({
            // title: 'Actions',
            //cssClass: 'action-sheets-basic-page',
            buttons: this.moreActions
        });
        actionSheet.present();
    };
    NotificationDetailsPage.prototype.actionSheetHandler = function (act, wf) {
        if (act == 'cancel')
            null;
        else if (act == 'MORE_INFORMATION_REQUEST') {
            this.takeReaasignAction('More Information', wf.notification_id);
        }
        else if (act == 'REASSIGN') {
            this.takeReaasignAction('Reassign', wf.notification_id);
        }
        else {
            this.takeNotificationAction(wf.notification_id, wf.item_type, wf.item_key, wf.process_activity, act);
            //  item.setElementClass("oo" ,true);
            //  setTimeout(() => {
            //   item.setElementClass("oo1" ,true);
            //  }, 999);
            // item.close();
        }
    };
    NotificationDetailsPage.prototype.validateBeforeAction = function (buttonName) {
        // check request More Info Replay 
        if (buttonName && buttonName == 'SUBMIT') {
            for (var i = 0; i < this.ntfAttributesText.length; i++) {
                if (this.ntfAttributesText[i] && this.ntfAttributesText[i].name == 'WF_NOTE') {
                    if (!this.ntfAttributesTextValues[i]) {
                        __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Please add notes');
                        return 0;
                    }
                }
            }
        }
        // check Forward 
        if (buttonName && buttonName.indexOf('FORWARD') > 1) {
            for (var i = 0; i < this.ntfAttributesRoles.length; i++) {
                if (this.ntfAttributesRoles[i]) {
                    if (!this.ntfAttributesRolesValues[i]) {
                        __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Please choose person before forward');
                        return 0;
                    }
                }
            }
        }
        return 1;
    };
    NotificationDetailsPage.prototype.takeNotificationAction = function (notificationId, itemType, itemKey, activityId, buttonName) {
        var _this = this;
        var validated = this.validateBeforeAction(buttonName);
        if (validated == 0)
            return;
        try {
            var attrs = this.setNotificationAttributes();
            if (!attrs)
                attrs = 'none';
            this.workListProvider.takeAction(notificationId, itemType, itemKey, activityId, buttonName, attrs)
                .then(function (data) {
                if (data[0].message == '0') {
                    __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                else {
                    // update count here 
                    window.localStorage.setItem('noOfNotifications', data[0].message);
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__work_list_work_list__["a" /* WorkListPage */], {
                        hr: _this.hr,
                        overtime: _this.overtime,
                        expenses: _this.expenses,
                        requisitions: _this.requisitions,
                        po: _this.po,
                        others: _this.others
                    });
                    // this.viewCtrl.dismiss();
                }
            });
            // .catch ()
            // {
            //   loader.dismiss();
            // }
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error.message);
        }
    };
    NotificationDetailsPage.prototype.testxx = function () {
    };
    NotificationDetailsPage.prototype.takeReaasignAction = function (title, notificationId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__shared_pages_employees_employees__["a" /* EmployeesPage */], {
            title: title, notificationId: notificationId, hr: this.hr,
            overtime: this.overtime,
            expenses: this.expenses,
            requisitions: this.requisitions,
            po: this.po,
            others: this.others
        });
    };
    NotificationDetailsPage.prototype.getNotificationAttributes = function () {
        var _this = this;
        try {
            this.workListProvider.getNotificationAttributes(this.wf.notification_id)
                .then(function (data) {
                if (data)
                    _this.ntfAttributes = data;
                if (_this.ntfAttributes.length <= 0)
                    _this.ntfAttributes = null;
                else {
                    _this.ntfAttributesNumber = _this.ntfAttributes.filter(function (item) {
                        return (item.type.toLowerCase().indexOf('number') > -1);
                    });
                    _this.ntfAttributesText = _this.ntfAttributes.filter(function (item) {
                        return (item.type.toLowerCase().indexOf('varchar2') > -1);
                    });
                    _this.ntfAttributesRoles = _this.ntfAttributes.filter(function (item) {
                        return (item.type.toLowerCase().indexOf('role') > -1);
                    });
                    _this.ntfAttributeslookUp = _this.ntfAttributes.filter(function (item) {
                        return (item.type.toLowerCase().indexOf('lookup') > -1);
                    });
                }
            });
        }
        catch (error) {
            //this.showToast('middle','Connection Problem');
            __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error.message);
        }
    };
    NotificationDetailsPage.prototype.roleTapped = function (event, item, i) {
        var _this = this;
        var modalAction = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__shared_pages_employees_employees__["a" /* EmployeesPage */], {
            title: item.display_name,
            notificationId: this.wf.notification_id, callingType: 1
        });
        modalAction.onDidDismiss(function (data) {
            if (data) {
                _this.ntfAttributesRolesValues[i] = data.user;
            }
        });
        modalAction.present();
    };
    NotificationDetailsPage.prototype.lookUpTapped = function (event, item, i) {
        var _this = this;
        var modalAction = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__shared_pages_look_up_look_up__["a" /* LookUpPage */], {
            title: item.display_name,
            lookUpType: item.format
        });
        modalAction.onDidDismiss(function (data) {
            if (data) {
                _this.ntfAttributeslookUpValues[i] = data.selectedLookUp;
            }
        });
        modalAction.present();
    };
    NotificationDetailsPage.prototype.setNotificationAttributes = function () {
        var attributesString = ';';
        if (this.ntfAttributes) {
            // text values 
            for (var i = 0; i < this.ntfAttributesText.length; i++) {
                if (this.ntfAttributesTextValues[i]) {
                    attributesString = attributesString + 'type:text,' + 'name:' + this.ntfAttributesText[i].name + ',value:' + this.ntfAttributesTextValues[i] + ';';
                }
            }
            // number values 
            for (var i = 0; i < this.ntfAttributesNumber.length; i++) {
                if (this.ntfAttributesNumberValues[i]) {
                    attributesString = attributesString + 'type:number,' + 'name:' + this.ntfAttributesNumber[i].name + ',value:' + this.ntfAttributesNumberValues[i] + ';';
                }
            }
            // role values 
            for (var i = 0; i < this.ntfAttributesRoles.length; i++) {
                if (this.ntfAttributesRolesValues[i]) {
                    attributesString = attributesString + 'type:role,' + 'name :' + this.ntfAttributesRoles[i].name + ',value:' + this.ntfAttributesRolesValues[i].m + ';';
                }
            }
            // lookUp values 
            for (var i = 0; i < this.ntfAttributeslookUp.length; i++) {
                if (this.ntfAttributeslookUpValues[i]) {
                    attributesString = attributesString + 'type:lookup,' + 'name:' + this.ntfAttributeslookUp[i].name + ',value:' + this.ntfAttributeslookUpValues[i].lookup_code + ';';
                }
            }
            if (attributesString.length > 2)
                attributesString = attributesString.substring(1, attributesString.length - 1);
            else
                attributesString = null; // no value
        }
        else {
            attributesString = null;
        }
        return attributesString;
    };
    NotificationDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notification-details',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\notification-details\notification-details.html"*/'<!--\n\n  Generated template for the NotificationDetailsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Notification Details</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n   \n\n  <div\n\n    [innerHTML]="notificationBody" >\n\n  </div>\n\n\n\n  <ion-item-divider color="light" class="divider-class1" *ngIf="ntfAttributes!=null"  item-end> Response </ion-item-divider >\n\n\n\n  \n\n\n\n    <ion-item  *ngFor="let item of ntfAttributesText;   let i = index">\n\n        <ion-label floating>{{item.display_name}}\n\n        \n\n        </ion-label>\n\n        <ion-input type="text" [(ngModel)]="ntfAttributesTextValues[i]">\n\n        </ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item  *ngFor="let item of ntfAttributesNumber;   let i = index">\n\n          <ion-label floating>{{item.display_name}}\n\n          \n\n          </ion-label>\n\n          <ion-input type="number" [(ngModel)]="ntfAttributesNumberValues[i]">\n\n          </ion-input>\n\n        </ion-item>\n\n\n\n        \n\n        <button ion-item  *ngFor="let item of ntfAttributesRoles;   let i = index" (click)="roleTapped($event, item  , i)" >\n\n          {{item.display_name}}\n\n            \n\n\n\n        \n\n          \n\n         \n\n            \n\n             <span class="button_item_title1"  *ngIf="ntfAttributesRolesValues[i]!=null" >{{ntfAttributesRolesValues[i].g}}</span>\n\n            \n\n          \n\n            \n\n          </button>\n\n          \n\n\n\n          <button ion-item  *ngFor="let item of ntfAttributeslookUp;   let i = index" (click)="lookUpTapped($event, item  , i)" >\n\n              {{item.display_name}}\n\n              \n\n              <span class="button_item_title1"  *ngIf="ntfAttributeslookUpValues[i]!=null" >{{ntfAttributeslookUpValues[i].meaning}}</span>\n\n            </button>\n\n\n\n\n\n  <ion-item-divider color="light" class="divider-class1" *ngIf="NotificationHistory!=null"  item-end> History </ion-item-divider >\n\n   \n\n    <ion-spinner name="circles" class="spiner-1"  *ngIf="NotificationHistory==null" >\n\n      </ion-spinner>\n\n   \n\n  \n\n    <ion-list  >\n\n    \n\n      \n\n\n\n    <ion-item *ngFor="let item of NotificationHistory"  >\n\n    \n\n      \n\n      <div class="item-note histroy-name-class" >\n\n          {{item.name}}\n\n         \n\n          <span class="histroy-action-class"   >{{item.action}}</span>\n\n            \n\n        </div>\n\n\n\n        <div class="item-note histroy-note-class" *ngIf="item.note!=null">\n\n         \n\n           \n\n                {{item.note}}\n\n              \n\n          </div>\n\n\n\n\n\n      \n\n    \n\n    </ion-item>\n\n  </ion-list>\n\n \n\n</ion-content>\n\n<ion-footer class="page_footer">\n\n  \n\n  \n\n<div class="div_footer">\n\n\n\n  <button class="notification-btn1" ion-button icon-right color="{{actButtons.button_color}}" *ngFor="let actButtons of baseActions2.slice().reverse() " (click)="morex(actButtons)" >\n\n    {{actButtons.button_label}}\n\n  \n\n    \n\n  </button>\n\n\n\n  </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\notification-details\notification-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_work_list_ntf_work_list_ntf__["a" /* WorkListNtfProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], NotificationDetailsPage);
    return NotificationDetailsPage;
}());

//# sourceMappingURL=notification-details.js.map

/***/ }),

/***/ 533:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LookUpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_work_list_ntf_work_list_ntf__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the LookUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LookUpPage = (function () {
    function LookUpPage(navCtrl, navParams, WorkListProvider, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.WorkListProvider = WorkListProvider;
        this.viewCtrl = viewCtrl;
        this.title = this.navParams.get('title');
        this.lookUpType = this.navParams.get('lookUpType');
        this.getlookUp();
    }
    LookUpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LookUpPage');
    };
    LookUpPage.prototype.getlookUp = function () {
        var _this = this;
        try {
            this.WorkListProvider.getLookUp(this.lookUpType)
                .then(function (data) {
                _this.lookUpData = data;
            });
        }
        catch (error) {
        }
    };
    LookUpPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    LookUpPage.prototype.LookUpTapped = function (event, item) {
        var data = { selectedLookUp: item };
        this.viewCtrl.dismiss(data);
    };
    LookUpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-look-up',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\look-up\look-up.html"*/'<!--\n\n  Generated template for the LookUpPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n      <ion-buttons start >\n\n          <button ion-button (click)="dismiss()" class="close-btn">Close</button>\n\n        </ion-buttons>\n\n    <ion-title>{{title}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n    <ion-list >\n\n\n\n        <button ion-item *ngFor="let item of lookUpData;   let i = index "  detail-none  (click)="LookUpTapped($event, item )" >\n\n        \n\n       \n\n          \n\n            \n\n             <h2 class="list_item_title1" >{{item.meaning}}</h2>\n\n            \n\n   \n\n  \n\n  \n\n      </button>\n\n  \n\n  \n\n  \n\n      </ion-list>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\look-up\look-up.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_work_list_ntf_work_list_ntf__["a" /* WorkListNtfProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ViewController */]])
    ], LookUpPage);
    return LookUpPage;
}());

//# sourceMappingURL=look-up.js.map

/***/ }),

/***/ 534:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewDelegationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_work_list_ntf_work_list_ntf__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_pages_employees_employees__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__delegation_home_delegation_home__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jquery__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the NewDelegationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewDelegationPage = (function () {
    function NewDelegationPage(navCtrl, navParams, WorkListNtfProvider, modalCtrl, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.WorkListNtfProvider = WorkListNtfProvider;
        this.modalCtrl = modalCtrl;
        this.AlertController = AlertController;
        this.disableUpdate = true;
        this.errorFound = false;
        this.selectedreplacedPesonName = 'Choose Person';
        this.mode = this.navParams.get('mode');
        if (!this.mode) {
            this.mode = 'new';
            this.title = 'New Delegation';
            this.getDelegationTypes();
        }
        else {
            this.title = 'Edit Delegation';
            this.itemType = this.navParams.get('itemType');
            var dateFrom = this.navParams.get('dateFrom');
            this.dateFrom = dateFrom;
            var dateTo = this.navParams.get('dateTo');
            this.dateTo = dateTo;
            this.selectedreplacedPesonName = this.navParams.get('personDisplayName');
            this.selectedPersonArgument = this.navParams.get('personName');
            this.comments = this.navParams.get('comments');
            this.ruleId = this.navParams.get('ruleId');
        }
    }
    NewDelegationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewDelegationPage');
    };
    NewDelegationPage.prototype.getDelegationTypes = function () {
        var _this = this;
        try {
            this.WorkListNtfProvider.getDelegationTypes().then(function (data) {
                _this.delegationTypes = data;
            });
        }
        catch (error) {
            alert(error);
        }
    };
    NewDelegationPage.prototype.personTapped = function () {
        var _this = this;
        var modalAction = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__shared_pages_employees_employees__["a" /* EmployeesPage */], { title: 'Delegate to',
            callingType: 1 });
        modalAction.onDidDismiss(function (data) {
            if (data) {
                _this.delegatePerson = data.user;
                _this.selectedreplacedPesonName = _this.delegatePerson.g;
                _this.selectedPersonArgument = _this.delegatePerson.m;
                _this.disableUpdate = false;
            }
        });
        modalAction.present();
    };
    NewDelegationPage.prototype.saveRquest = function () {
        var _this = this;
        try {
            var error = 0;
            if (!this.selecteditemType) {
                this.showErrorMessage('delegation-type');
                error = 1;
            }
            if (this.selectedreplacedPesonName == 'Choose Person') {
                this.showErrorMessage('selected-person');
                error = 1;
            }
            if (!this.dateFrom) {
                this.showErrorMessage('date-from');
                error = 1;
            }
            if (!this.dateTo) {
                this.showErrorMessage('date-to');
                error = 1;
            }
            if (error == 1)
                return;
            if (this.errorFound)
                return;
            this.WorkListNtfProvider.addVactionRule(this.dateFrom, this.dateTo, this.selecteditemType.name, this.delegatePerson.m, this.comments).then(function (data) {
                // console.log(data);
                if (data[0].message == '2') {
                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__delegation_home_delegation_home__["a" /* DelegationHomePage */]);
                }
            });
        }
        catch (err) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
        }
    };
    NewDelegationPage.prototype.deleteRequest = function () {
        var _this = this;
        var confirm = this.AlertController.create({
            title: null,
            message: 'Do you want to delete this Rule?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'delete',
                    handler: function () {
                        try {
                            _this.WorkListNtfProvider.deleteVacationRule(_this.ruleId).then(function (data) {
                                if (data[0].message == '2') {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                                    return;
                                }
                                else {
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__delegation_home_delegation_home__["a" /* DelegationHomePage */]);
                                }
                            });
                        }
                        catch (err) {
                            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, err.message);
                        }
                    }
                }
            ]
        });
        confirm.present();
    };
    NewDelegationPage.prototype.updateRquest = function () {
        var _this = this;
        try {
            var error = 0;
            if (this.selectedreplacedPesonName == 'Choose Person') {
                this.showErrorMessage('selected-person');
                error = 1;
            }
            if (!this.dateFrom) {
                this.showErrorMessage('date-from');
                error = 1;
            }
            if (!this.dateTo) {
                this.showErrorMessage('date-to');
                error = 1;
            }
            if (error == 1)
                return;
            this.WorkListNtfProvider.updateVactionRule(this.ruleId, this.dateFrom, this.dateTo, this.selectedPersonArgument, this.comments).then(function (data) {
                if (data[0].message == '2') {
                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__delegation_home_delegation_home__["a" /* DelegationHomePage */]);
                }
            });
        }
        catch (err) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
        }
    };
    NewDelegationPage.prototype.showErrorMessage = function (elementId) {
        var x;
        x = document.getElementById(elementId);
        __WEBPACK_IMPORTED_MODULE_6_jquery__(x).addClass('error-color');
    };
    NewDelegationPage.prototype.removeErrorMessage = function (elementId) {
        var x;
        x = document.getElementById(elementId);
        __WEBPACK_IMPORTED_MODULE_6_jquery__(x).removeClass('error-color');
    };
    NewDelegationPage.prototype.onChange = function () {
        this.disableUpdate = false;
    };
    NewDelegationPage.prototype.onckeckDate = function () {
        this.disableUpdate = false;
        if (this.dateFrom && this.dateTo) {
            if (this.dateFrom > this.dateTo) {
                this.showErrorMessage('date-from');
                this.showErrorMessage('date-to');
                this.disableUpdate = true;
                this.errorFound = true;
            }
            else {
                this.errorFound = false;
                this.removeErrorMessage('date-from');
                this.removeErrorMessage('date-to');
            }
        }
    };
    NewDelegationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-new-delegation',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\new-delegation\new-delegation.html"*/'<!--\n\n  Generated template for the NewDelegationPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{title}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item Id="delegation-type"  *ngIf="mode==\'new\'">\n\n            <ion-label>Item Type <i class="req-symbol">*</i></ion-label>\n\n            <ion-select [(ngModel)]="selecteditemType"  interface="action-sheet" placeholder="Choose Type" >\n\n           \n\n                   <ion-option *ngFor="let item of delegationTypes"   [value]="item"> {{item.display_name}}   </ion-option>\n\n             </ion-select>\n\n\n\n          </ion-item>\n\n\n\n          <ion-item Id="item-type-txt"  *ngIf="mode==\'edit\'">\n\n              <div>Item Type \n\n                  <span class="item_txt"  >{{itemType}}</span>\n\n\n\n              </div>\n\n     \n\n            </ion-item>\n\n\n\n          <button ion-item  (click)="personTapped($event)" Id="selected-person">\n\n              Person <i class="req-symbol">*</i>\n\n            <span class="button_item_title1"  >{{selectedreplacedPesonName}}</span>\n\n\n\n              </button> \n\n\n\n          <ion-item Id="date-from">\n\n              <ion-label>Start Date <i class="req-symbol">*</i></ion-label>\n\n              <ion-datetime displayFormat="DDDD, DD-MMM-YYYY"  [(ngModel)]="dateFrom"   placeholder="Date From" (ionChange)="onckeckDate()"></ion-datetime>\n\n            </ion-item>\n\n            <ion-item Id="date-to">\n\n            <ion-label>End Date<i class="req-symbol">*</i> </ion-label>\n\n            <ion-datetime displayFormat="DDDD, DD-MMM-YYYY" [(ngModel)]="dateTo" placeholder="Date To" (ionChange)="onckeckDate()"></ion-datetime>\n\n          </ion-item>\n\n\n\n         \n\n\n\n                 \n\n            <ion-item>\n\n                <ion-label>Message</ion-label>\n\n                \n\n                </ion-item>\n\n            <ion-item >\n\n                <ion-textarea  [(ngModel)]="comments" class="txt-area" (ionChange)="onChange()"></ion-textarea>\n\n              </ion-item >\n\n\n\n\n\n\n\n          </ion-list>\n\n</ion-content>\n\n<ion-footer class=\'fotter-container\'>\n\n   <div  *ngIf=" mode==\'new\' ">\n\n    <button ion-button  color="dark" block (click)="saveRquest()">Save</button>\n\n    </div>\n\n    <div  *ngIf="mode==\'edit\' ">\n\n   \n\n    <button ion-button class=\'fotter-btn\' color="danger"   (click)="deleteRequest()">Delete</button>\n\n    <button ion-button class=\'fotter-btn\' color="blue" [disabled]=\'disableUpdate\' (click)="updateRquest()">Save Changes </button>\n\n    </div>\n\n   \n\n</ion-footer>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\new-delegation\new-delegation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_work_list_ntf_work_list_ntf__["a" /* WorkListNtfProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], NewDelegationPage);
    return NewDelegationPage;
}());

//# sourceMappingURL=new-delegation.js.map

/***/ }),

/***/ 535:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_thanks_card_thanks_card__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__thankscards_home_thankscards_home__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the NewCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewCardPage = (function () {
    function NewCardPage(navCtrl, navParams, ThanksCardProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ThanksCardProvider = ThanksCardProvider;
        this.AlertController = AlertController;
        this.subjectButtons = [];
        this.selectedUser = this.navParams.get('user');
        this.title = 'Card to ' + this.selectedUser.g;
        this.placeHolder = 'Tell ' + this.selectedUser.g + ' more';
        this.getSubjects();
        this.getRemaingCard();
    }
    NewCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewCardPage');
    };
    NewCardPage.prototype.getSubjects = function () {
        var _this = this;
        try {
            this.ThanksCardProvider.getSubjectTypes()
                .then(function (data) {
                _this.subjectData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    NewCardPage.prototype.subjectTapped = function (event, subject, selectedindex) {
        this.selectedSubject = subject.lookup_code;
        for (var i = 0; i < this.subjectButtons.length; i++) {
            this.subjectButtons[i] = '';
        }
        this.subjectButtons[selectedindex] = 'seleted-subject';
    };
    NewCardPage.prototype.onChange = function () {
    };
    NewCardPage.prototype.sendCard = function () {
        var _this = this;
        if (!this.thanksComments) {
            this.commentsError = 'commentsError';
            return;
        }
        this.commentsError = '';
        try {
            this.ThanksCardProvider.sendCard(this.selectedUser.d, this.selectedSubject, this.thanksComments)
                .then(function (data) {
                if (data && data[0].messgae == "2")
                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                else
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__thankscards_home_thankscards_home__["a" /* ThankscardsHomePage */]);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    NewCardPage.prototype.getRemaingCard = function () {
        var _this = this;
        try {
            this.ThanksCardProvider.getRemaingCards()
                .then(function (data) {
                if (data && data[0])
                    _this.remaingCardText = data[0].remaing_card_text;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    NewCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-new-card',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\thank-u\new-card\new-card.html"*/'<!--\n\n  Generated template for the NewCardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{title}}</ion-title>\n\n\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n<div class="icon-div">\n\n  \n\n    <fa-icon name="thumbs-up" color="blue" class="thanks-icon"></fa-icon>\n\n\n\n    </div>\n\n    <ion-list >\n\n\n\n        <button ion-item *ngFor="let item of subjectData; let i = index"  detail-none class="sub-btn" (click)="subjectTapped($event, item ,i)" >\n\n\n\n            \n\n            <div  class="list_item_sub_title2" [ngClass]="subjectButtons[i]" >{{item.meaning}}  </div> \n\n  \n\n  \n\n      </button>\n\n  \n\n  \n\n  \n\n      </ion-list>\n\n\n\n      <h6 class="reaming-class" > {{remaingCardText}} </h6 >\n\n\n\n      <ion-textarea  [(ngModel)]="thanksComments" placeholder={{placeHolder}} \n\n          autocomplete="on" [ngClass]="commentsError" autocorrect="on"  class="txt-area-comm"  maxlength="120"  (ionChange)="onChange()"></ion-textarea>\n\n\n\n</ion-content>\n\n<ion-footer>\n\n\n\n    <button ion-button  color="dark" block (click)="sendCard()" [disabled]="selectedSubject==null">Send Card</button>\n\n   \n\n</ion-footer>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\thank-u\new-card\new-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_thanks_card_thanks_card__["a" /* ThanksCardProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], NewCardPage);
    return NewCardPage;
}());

//# sourceMappingURL=new-card.js.map

/***/ }),

/***/ 536:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaderBoardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_thanks_card_thanks_card__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__thanks_cards_thanks_cards__ = __webpack_require__(194);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the LeaderBoardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LeaderBoardPage = (function () {
    function LeaderBoardPage(navCtrl, navParams, ThanksCardProvider, AlertController, sanitized) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ThanksCardProvider = ThanksCardProvider;
        this.AlertController = AlertController;
        this.sanitized = sanitized;
        this.fromR = 0;
        this.v_step = 10;
        this.toR = this.v_step;
        this.getLeaderBoard();
    }
    LeaderBoardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LeaderBoardPage');
    };
    LeaderBoardPage.prototype.getLeaderBoard = function () {
        var _this = this;
        try {
            this.ThanksCardProvider.getLeaderBoard()
                .then(function (data) {
                _this.boardDataAll = data;
                if (_this.boardDataAll)
                    _this.boardData = (_this.boardDataAll.slice(0, _this.toR));
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    LeaderBoardPage.prototype.cardTapped = function (event, card) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__thanks_cards_thanks_cards__["a" /* ThanksCardsPage */], { cardType: 'Received', title: 'Cards For ' + card.full_name, personId: card.person_id });
    };
    LeaderBoardPage.prototype.doRefresh = function (refresher) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.toR = this.v_step;
                        return [4 /*yield*/, this.getLeaderBoard()];
                    case 1:
                        _a.sent();
                        refresher.complete();
                        return [2 /*return*/];
                }
            });
        });
    };
    LeaderBoardPage.prototype.doInfinite = function (infiniteScroll) {
        this.toR = this.toR + this.v_step;
        try {
            if (this.boardDataAll)
                this.boardData = (this.boardDataAll.slice(0, this.toR));
            infiniteScroll.complete();
        }
        catch (err) {
        }
        console.log(' infiniteScroll has ended');
    };
    LeaderBoardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leader-board',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\thank-u\leader-board\leader-board.html"*/'<!--\n\n  Generated template for the LeaderBoardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Leader Board</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content pullingIcon="arrow-dropdown"\n\n        pullingText="Pull to refresh"\n\n        refreshingSpinner="circles"\n\n        refreshingText="Refreshing..."></ion-refresher-content>\n\n      </ion-refresher>\n\n      \n\n    <ion-list  >\n\n        <button ion-item *ngFor="let item of boardData"  block (click)="cardTapped($event, item )">\n\n        \n\n     \n\n\n\n              <ion-avatar item-start> \n\n\n\n                      <img [src]="sanitized.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+item.img)" [hidden]="item.img==null"/>\n\n                  <div class="{{item.class_name}}"  [hidden]="item.img!=null" >  {{item.intials}} </div>\n\n                 </ion-avatar>\n\n        \n\n\n\n                 <h2 class="list_item_title1" >  {{item.full_name}} </h2>\n\n                 <div class="list_item_sub_block">\n\n                 <div  class="list_item_sub_title5" > {{item.thanks_count}}  \n\n                 <span  class="list_item_sub_title4" > Thank You Cards </span> \n\n\n\n                 </div>\n\n           </div>\n\n          \n\n      \n\n        </button>\n\n      </ion-list>\n\n\n\n      <ion-infinite-scroll (ionInfinite)="doInfinite($event)" > \n\n          <ion-infinite-scroll-content\n\n          loadingSpinner="bubbles"\n\n          loadingText="Loading more data...">\n\n        </ion-infinite-scroll-content> \n\n        </ion-infinite-scroll>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\thank-u\leader-board\leader-board.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_thanks_card_thanks_card__["a" /* ThanksCardProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */]])
    ], LeaderBoardPage);
    return LeaderBoardPage;
}());

//# sourceMappingURL=leader-board.js.map

/***/ }),

/***/ 537:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaveBalancePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_piecelabel_js__ = __webpack_require__(538);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_piecelabel_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_piecelabel_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { Chart } from 'chart.js';
/**
 * Generated class for the LeaveBalancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LeaveBalancePage = (function () {
    function LeaveBalancePage(navCtrl, navParams, leavesHistory) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.leavesHistory = leavesHistory;
        this.seg = "Annual";
        this.chartType = 'pie';
        this.chartLabels = ['January', 'February', 'March'];
        this.chartData = [1, 1, 1];
        this.chartOptions = {
            pieceLabel: {
                render: 'value',
                fontColor: ['white', 'white'],
                fontStyle: 'bold',
                fontSize: '12',
                // arc :true ,
                position: 'border ',
                legend: {
                    onClick: null
                }
                //  render:'lavel'
                //  function (args) {
                //   const label = args.label,
                //         value = args.value;
                //  return 'label' + 'percentage' ;
                // }
            }
        };
        this.doughnutChartColors = [
            {
                // backgroundColor:["#FF7360", "#6FC8CE", "#FAFFF2", "#FFFCC4", "#B9E8E0"] 
                backgroundColor: ["red", "#353D4e"],
                fontColor: ["gray", "#008AE6"]
            }
        ];
        this.doughnutChartType = 'doughnut';
        this.pieChartType = 'pie';
        this.isdisplayed = false;
        this.isdisplayedt = false;
        this.toilTabdisplayed = false;
        if (__WEBPACK_IMPORTED_MODULE_4__app_app_module__["a" /* AppModule */].getProperty('bu') == '81') {
            this.toilTabdisplayed = true;
        }
        // annual
        try {
            this.leavesHistory.getAnnualLeavesBalance()
                .then(function (data) {
                _this.doughnutChartLabelsAnuual = [];
                _this.doughnutChartLabelsAnuual[0] = data[0].taken_txt;
                _this.doughnutChartLabelsAnuual[1] = data[0].available_txt;
                _this.doughnutChartvaluesAnuual = [];
                _this.doughnutChartvaluesAnuual[0] = data[0].taken_no;
                _this.doughnutChartvaluesAnuual[1] = data[0].available_no;
                _this.isdisplayed = true;
            });
        }
        catch (error) {
            alert('Connection Problem');
        }
        // this.doughnutChartLabelsAnuual=[];
        //       this.doughnutChartLabelsAnuual[0]='Taken (12) ';
        //    this.doughnutChartLabelsAnuual[1]='Available (19)';
        //  this.doughnutChartvaluesAnuual=[];
        //  this.doughnutChartvaluesAnuual[0]=12;
        //   this.doughnutChartvaluesAnuual[1]=19;
        // toil
        try {
            this.leavesHistory.getToilBalance()
                .then(function (data) {
                _this.doughnutChartLabelsToil = [];
                _this.doughnutChartLabelsToil[0] = data[0].taken_txt;
                _this.doughnutChartLabelsToil[1] = data[0].available_txt;
                _this.doughnutChartvaluesToil = [];
                _this.doughnutChartvaluesToil[0] = data[0].taken_no;
                _this.doughnutChartvaluesToil[1] = data[0].available_no;
                _this.isdisplayedt = true;
            });
        }
        catch (error) {
            alert('Connection Problem');
        }
        // toil
        this.doughnutChartLabelsToil = [];
        this.doughnutChartLabelsToil[0] = 'Taken (15) ';
        this.doughnutChartLabelsToil[1] = 'Available (17)';
        this.doughnutChartvaluesToil = [];
        this.doughnutChartvaluesToil[0] = 15;
        this.doughnutChartvaluesToil[1] = 17;
    }
    LeaveBalancePage.prototype.ionViewDidLoad = function () {
        // try{
        //     this.leavesHistory.getAnnualLeavesBalance(this.personId)
        //     .then(data => {
        //      this.doughnutChartLabels= [];
        //      this.doughnutChartLabels[0]=data[0].available_txt;
        //      this.doughnutChartLabels[1]=data[0].taken_txt;
        //   this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
        //     type: 'pie',
        //     data: {
        //         labels: ["Taken (12) ", "Available (19)"],
        //         datasets: [{
        //             label: '# of Votes',
        //             data: [12, 19],
        //             backgroundColor: [
        //                 'gray',
        //                 '#008AE6'
        //             ],
        //             hoverBackgroundColor: [
        //                 "#565656",
        //                 "#36A2EB"
        //             ]
        //         }]
        //     },
        //     options: {
        //   }
        // });
        // catch (error)
        // {
        //   alert('Connection Problem');
        // }
        // this.doughnutChartLabels= [];
        // this.doughnutChartLabels[0]='dddd';
        // this.doughnutChartLabels[1]='ggg';
    };
    // events
    LeaveBalancePage.prototype.chartClicked = function (e) {
    };
    LeaveBalancePage.prototype.chartHovered = function (e) {
    };
    LeaveBalancePage.prototype.xx = function () {
        var data = {
            datasets: [{
                    data: [
                        11,
                        16,
                        7,
                        3,
                        14
                    ],
                    backgroundColor: [
                        "#FF6384",
                        "#4BC0C0",
                        "#FFCE56",
                        "#E7E9ED",
                        "#36A2EB"
                    ],
                    label: 'My dataset' // for legend
                }],
            labels: [
                "Red",
                "Green",
                "Yellow",
                "Grey",
                "Blue"
            ]
        };
        /*
        var pieOptions = {
          events: false,
          animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
              var ctx = this.chart.ctx;
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
        
              this.data.datasets.forEach(function (dataset) {
        
                for (var i = 0; i < dataset.data.length; i++) {
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                      start_angle = model.startAngle,
                      end_angle = model.endAngle,
                      mid_angle = start_angle + (end_angle - start_angle)/2;
        
                  var x = mid_radius * Math.cos(mid_angle);
                  var y = mid_radius * Math.sin(mid_angle);
        
                  ctx.fillStyle = '#fff';
                  if (i == 3){ // Darker text color for lighter background
                    ctx.fillStyle = '#444';
                  }
                  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                  ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                  // Display percent in another line, line break doesn't work for fillText
                  ctx.fillText(percent, model.x + x, model.y + y + 15);
                }
              });
            }
          }
        };
        
        var pieChartCanvas = $("#pieChart");
        var pieChart = new Chart(pieChartCanvas, {
          type: 'pie', // or doughnut
          data: data,
          options: pieOptions
        });
        
        }
        
        */
        /*
        
         drawSegmentValues()
        {
            
          
          //var canvas = document.getElementById("canvas");
          var ctx =this.doughnutCanvas.nativeElement;
          var midX = 40;
          var midY = 80;
          var totalValue = 50;
          var radius = this.doughnutCanvas.outerRadius;
          
          for(var i=0; i<this.doughnutCanvas.nativeElement; i++)
            {
               
             // alert (i+'d');
              ctx.fillStyle="white";
                var textSize = 600/15;
                ctx.font= textSize+"px Verdana";
                // Get needed variables
                var value = this.doughnutCanvas.segments[i].value/totalValue*100;
                // if(Math.round(value) !== value)
                // 	value = (this.doughnutCanvas.segments[i].value/totalValue*100).toFixed(1);
                // value = value + '%';
                value=15;
                var startAngle = this.doughnutCanvas.segments[i].startAngle;
                var endAngle = this.doughnutCanvas.segments[i].endAngle;
                var middleAngle = startAngle + ((endAngle - startAngle)/2);
        
                // Compute text location
                var posX = (radius/2) * Math.cos(middleAngle) + midX;
                var posY = (radius/2) * Math.sin(middleAngle) + midY;
        
                // Text offside by middle
                var w_offset = ctx.measureText(value).width/2;
                var h_offset = textSize/4;
        
                ctx.fillText(value, posX - w_offset, posY + h_offset);
            }
        }
        
        */
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LeaveBalancePage.prototype, "doughnutCanvas", void 0);
    LeaveBalancePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave-balance',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\leave-balance\leave-balance.html"*/'<!--\n\n  Generated template for the LeaveBalancePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Leave Balance</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n\n\n  <!-- <div id="chart-container">\n\n    <canvas baseChart \n\n            [chartType]="chartType"\n\n            [data]="chartData"\n\n            [labels]="chartLabels"\n\n            [options]="chartOptions">\n\n    </canvas>\n\n</div>\n\n\n\n    <canvas #doughnutCanvas style="height:60vh; width:80vw"></canvas>\n\n    <canvas id="pieChart" width=200 height=200></canvas> -->\n\n  <ion-segment [(ngModel)]="seg">\n\n    <ion-segment-button value="Annual">\n\n      Annual\n\n    </ion-segment-button>\n\n    <ion-segment-button value="TOIL" *ngIf="toilTabdisplayed">\n\n      TOIL\n\n    </ion-segment-button>\n\n\n\n  </ion-segment>\n\n\n\n\n\n  <div [ngSwitch]="seg">\n\n    <ion-list *ngSwitchCase="\'Annual\'">\n\n   \n\n      <div *ngIf="isdisplayed" style="display: block">\n\n          <canvas style="height:60vh; width:80vw" baseChart\n\n                      [data]="doughnutChartvaluesAnuual"\n\n                      [colors]="doughnutChartColors"\n\n                      [labels]="doughnutChartLabelsAnuual"\n\n                      [chartType]="doughnutChartType"\n\n                      [options]="chartOptions"\n\n                      >\n\n                      (chartHover)="chartHovered($event)"\n\n                      (chartClick)="chartClicked($event)"\n\n                    \n\n                    </canvas>\n\n      \n\n        </div> \n\n\n\n      \n\n\n\n     \n\n    </ion-list>\n\n\n\n    <ion-list *ngSwitchCase="\'TOIL\'">\n\n        \n\n        <div *ngIf="isdisplayedt"  style="display: block">\n\n            <canvas style="height:60vh; width:80vw" baseChart\n\n                        [data]="doughnutChartvaluesToil"\n\n                        [colors]="doughnutChartColors"\n\n                        [labels]="doughnutChartLabelsToil"\n\n                        [chartType]="doughnutChartType"\n\n                        [options]="chartOptions">\n\n                        (chartHover)="chartHovered($event)"\n\n                        (chartClick)="chartClicked($event)"\n\n                      \n\n                      </canvas>\n\n        \n\n          </div>\n\n    </ion-list>\n\n\n\n \n\n  </div>\n\n\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\leave-balance\leave-balance.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__["a" /* SshrLeavesProvider */]])
    ], LeaveBalancePage);
    return LeaveBalancePage;
}());

//# sourceMappingURL=leave-balance.js.map

/***/ }),

/***/ 539:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToilExpirePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ToilExpirePage = (function () {
    function ToilExpirePage(navCtrl, navParams, LeavesProvider, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.LeavesProvider = LeavesProvider;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.getToil();
    }
    ToilExpirePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ToilExpirePage');
    };
    ToilExpirePage.prototype.getToil = function () {
        var _this = this;
        try {
            this.LeavesProvider.getToilExpire()
                .then(function (data) {
                _this.toilData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    ToilExpirePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-toil-expire',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\toil-expire\toil-expire.html"*/'<!--\n\n  Generated template for the ToilExpirePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>TOIL Expiry</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <ion-list >\n\n\n\n        <ion-item *ngFor="let item of toilData"    >\n\n        \n\n       \n\n             <ion-avatar item-start> \n\n            \n\n              <div class="{{item.class_name}}"   >  {{item.intials}} </div>\n\n             </ion-avatar> \n\n            \n\n             <h2 class="list_item_title1" >{{item.global_name}}</h2>\n\n            \n\n            <div  class="list_item_sub_title2" >{{item.no_of_days}}  </div> \n\n  \n\n  \n\n      </ion-item>\n\n  \n\n  \n\n  \n\n      </ion-list>\n\n\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\leaves\toil-expire\toil-expire.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_sshr_leaves_sshr_leaves__["a" /* SshrLeavesProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ToilExpirePage);
    return ToilExpirePage;
}());

//# sourceMappingURL=toil-expire.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkListNtfProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









/*
  Generated class for the WorkListNtfProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var WorkListNtfProvider = (function () {
    function WorkListNtfProvider(http, httpClient, platform, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.platform = platform;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
        console.log('Hello WorkListNtfProvider Provider');
    }
    WorkListNtfProvider.prototype.getWorkListPost = function (fromRecord, toREcord) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getWorkListPost';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.takeAction = function (notificationId, itemType, itemKey, activityId, buttonName, attributes) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders, actionMethodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/takeNotificationAction';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            'Content-Type': 'application/json ; charset=UTF-8',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
                            'Access-Control-Allow-origin': '*',
                            'wsid': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].authentication,
                            'notificationId': notificationId + '',
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        actionMethodHeaders = {
                            'signature': encodeURI(t + ''),
                            'ntf': notificationId + '',
                        };
                        if (itemType) {
                            methodHeaders = methodHeaders.append('it', itemType);
                            actionMethodHeaders['it'] = itemType;
                        }
                        if (itemKey) {
                            methodHeaders = methodHeaders.append('ik', itemKey);
                            actionMethodHeaders['ik'] = itemKey;
                        }
                        if (activityId) {
                            methodHeaders = methodHeaders.append('ai', activityId);
                            actionMethodHeaders['ai'] = activityId;
                        }
                        if (buttonName) {
                            methodHeaders = methodHeaders.append('bn', buttonName);
                            actionMethodHeaders['bn'] = buttonName;
                        }
                        if (attributes) {
                            methodHeaders = methodHeaders.append('attributes', attributes);
                            actionMethodHeaders['attributes'] = attributes;
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, actionMethodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.getNoticationBody = function (notificationId, itemType, itemKey, generatorApi) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders, bodyMethodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getNotoficationBody';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            'Content-Type': 'application/json ; charset=UTF-8',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
                            'Access-Control-Allow-origin': '*',
                            'wsid': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].authentication,
                            'notificationId': notificationId + '',
                            'generatorApi': generatorApi + '',
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        bodyMethodHeaders = {
                            'signature': encodeURI(t + ''),
                            'ntf': notificationId + '',
                            'gn': generatorApi + ''
                        };
                        if (itemType) {
                            methodHeaders = methodHeaders.append('it', itemType);
                            bodyMethodHeaders['it'] = itemType;
                        }
                        if (itemKey) {
                            methodHeaders = methodHeaders.append('ik', itemKey);
                            bodyMethodHeaders['ik'] = itemKey;
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, bodyMethodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.getNotificationHistory = function (notificationId, itemType, itemKey, processActivity, filterType) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders, historyMethodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getNotificationHistory';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            'Content-Type': 'application/json ; charset=UTF-8',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
                            'Access-Control-Allow-origin': '*',
                            'wsid': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].authentication,
                            'notificationId': notificationId + '',
                            'filterType': '' + filterType + '',
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        historyMethodHeaders = {
                            'signature': encodeURI(t + ''),
                            'ntf': notificationId + '',
                            'ft': '' + filterType + '',
                        };
                        if (itemType) {
                            methodHeaders = methodHeaders.append('it', itemType);
                            historyMethodHeaders['it'] = itemType;
                        }
                        if (itemKey) {
                            methodHeaders = methodHeaders.append('ik', itemKey);
                            historyMethodHeaders['ik'] = itemKey;
                        }
                        if (processActivity) {
                            methodHeaders = methodHeaders.append('pa', processActivity);
                            historyMethodHeaders['pa'] = processActivity;
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, historyMethodHeaders, null, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.getWorkListcountPost = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getWorkListCountPost';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.getUsers = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getUsers';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.takeReassignAction = function (notificationId, newRole, comments, buttonName) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/takeReassignAction';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'ntf': notificationId + '',
                            'newRole': '' + newRole + '',
                            'comments': '' + encodeURI(comments) + '',
                            'bn': '' + buttonName + '',
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.getNotificationAttributes = function (notificationId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getNotificationAttributes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'ntf': notificationId + ''
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.getLookUp = function (lookUpType) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getLookUp';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'lookUpType': lookUpType + ''
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.getDelegationHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getDelegationHistory';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.getDelegationTypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getDelegationTypes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.addVactionRule = function (dateStart, dateEnd, itemType, toUser, comments) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/addVacatiobRule';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'dateStart': dateStart.toString(),
                            'dateEnd': dateEnd.toString(),
                            'it': itemType + '',
                            'newRole': toUser + '',
                        };
                        if (comments) {
                            methodHeaders['comments'] = encodeURI(comments);
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.updateVactionRule = function (ruleId, dateStart, dateEnd, toUser, comments) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/updateVacationRule';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'rxd': ruleId + '',
                            'dateStart': dateStart.toString(),
                            'dateEnd': dateEnd.toString(),
                            'newRole': toUser + ''
                        };
                        if (comments) {
                            methodHeaders['comments'] = encodeURI(comments);
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider.prototype.deleteVacationRule = function (ruleId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/deleteVacationRule';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'rxd': ruleId + '',
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, null, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    WorkListNtfProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* App */]])
    ], WorkListNtfProvider);
    return WorkListNtfProvider;
}());

//# sourceMappingURL=work-list-ntf.js.map

/***/ }),

/***/ 540:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LettersPreviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__letters_submit_letters_submit__ = __webpack_require__(541);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the LettersPreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LettersPreviewPage = (function () {
    function LettersPreviewPage(navCtrl, navParams, hrSit, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.hrSit = hrSit;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.UAEsegmnets = false;
        this.KSAsegments = false;
        this.otherSegmnts = false;
        // AppModule.businessGroup=997;
        this.businessGroup = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].businessGroup;
        if (this.businessGroup) {
            if (this.businessGroup == '81') {
                this.UAEsegmnets = true;
            }
            else if (this.businessGroup == '998') {
                this.KSAsegments = true;
            }
            else {
                this.otherSegmnts = true;
            }
        }
        this.getpageLettersTypes();
        if (this.businessGroup && this.businessGroup == '81') {
            this.getTecomSalatUpdateTypes();
        }
    }
    LettersPreviewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LettersPreviewPage');
    };
    LettersPreviewPage.prototype.getpageLettersTypes = function () {
        var _this = this;
        try {
            this.hrSit.getLettersTypes().then(function (data) {
                _this.letterTypes = data;
            });
        }
        catch (error) {
        }
    };
    LettersPreviewPage.prototype.getTecomSalatUpdateTypes = function () {
        var _this = this;
        try {
            this.hrSit.getSalaryUpdateTypes().then(function (data) {
                _this.tecomUpdateSalaryTypes = data;
            });
        }
        catch (error) {
        }
    };
    LettersPreviewPage.prototype.previewLetterRequest = function () {
        var _this = this;
        var error = 0;
        if (!this.selectedLetterType) {
            this.showError('letterTypeId');
            error = 1;
        }
        else {
            this.removeError('letterTypeId');
        }
        // check uae 
        if (this.UAEsegmnets) {
            if (!this.cbt) {
                this.showError('cbt');
                error = 1;
            }
            else {
                this.removeError('cbt');
            }
        }
        // check KSA
        if (this.KSAsegments) {
            if (!this.lt1) {
                this.showError('lt1');
                error = 1;
            }
            else {
                this.removeError('lt1');
            }
        }
        // check others
        if (this.otherSegmnts) {
            if (!this.cb) {
                this.showError('cb');
                error = 1;
            }
            else {
                this.removeError('cb');
            }
        }
        if (error == 1)
            return;
        else {
            var selectedTecomUpdateSalary_1;
            if (this.selectedTecomUpdateSalaryType) {
                selectedTecomUpdateSalary_1 = this.selectedTecomUpdateSalaryType.flex_value;
            }
            try {
                this.hrSit.submitLetter('W', this.selectedLetterType.letter_code, this.cbt, this.TravelDestination, selectedTecomUpdateSalary_1, this.lt1, this.lt2, this.cb).then(function (data) {
                    if (data[0].message == '2') {
                        __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                        return;
                    }
                    else {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__letters_submit_letters_submit__["a" /* LettersSubmitPage */], {
                            RselectedLetterType: _this.selectedLetterType,
                            RselectedTecomUpdateSalaryType: selectedTecomUpdateSalary_1,
                            RTravelDestination: _this.TravelDestination,
                            Rcbt: _this.cbt,
                            Rcb: _this.cb,
                            Rlt1: _this.lt1,
                            Rlt2: _this.lt2,
                            RapproverList: data
                        });
                    }
                });
            }
            catch (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
            }
        }
    };
    LettersPreviewPage.prototype.showError = function (elementId) {
        var el;
        el = document.getElementsByClassName(elementId)[0];
        __WEBPACK_IMPORTED_MODULE_5_jquery__(el).addClass('error-color');
    };
    LettersPreviewPage.prototype.removeError = function (elementId) {
        var el;
        el = document.getElementsByClassName(elementId)[0];
        __WEBPACK_IMPORTED_MODULE_5_jquery__(el).removeClass('error-color');
    };
    LettersPreviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-letters-preview',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\letters-preview\letters-preview.html"*/'<!--\n\n  Generated template for the LettersPreviewPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Letters</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-item-divider color="light"> Please provide the following details</ion-item-divider>\n\n  <ion-list> \n\n    <ion-item Id="letterTypeIdx">\n\n      <ion-label floating class="letterTypeId">Letter Type\n\n        <i class="req-symbol">*</i>\n\n      </ion-label>\n\n      <ion-select [(ngModel)]="selectedLetterType" class="select-1" interface="action-sheet" >\n\n        <ion-option *ngFor="let item of letterTypes" [value]="item"> {{item.letter_type}} </ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item *ngIf="UAEsegmnets" >\n\n      <ion-label class="cbt" floating>Company/Consulate/Bank/TECOM\n\n        <i class="req-symbol">*</i>\n\n      </ion-label>\n\n      <ion-input type="text" [(ngModel)]="cbt">\n\n      </ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item *ngIf="otherSegmnts">\n\n      <ion-label class="cb" floating>Company/Consulate/Bank\n\n        <i class="req-symbol">*</i>\n\n      </ion-label>\n\n      <ion-input type="text" [(ngModel)]="cb">\n\n      </ion-input>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item *ngIf="KSAsegments">\n\n      <ion-label class="lt1" floating>Letter To (Line 1)\n\n        <i class="req-symbol">*</i>\n\n      </ion-label>\n\n      <ion-input type="text" [(ngModel)]="lt1">\n\n      </ion-input>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item *ngIf="KSAsegments">\n\n      <ion-label class="lt2" floating>Letter To (Line 2)\n\n\n\n      </ion-label>\n\n      <ion-input type="text" [(ngModel)]="lt2">\n\n      </ion-input>\n\n    </ion-item>\n\n\n\n\n\n    \n\n    <ion-item *ngIf="UAEsegmnets || otherSegmnts" >\n\n      <ion-label floating >Travel Destination</ion-label>\n\n      <ion-input type="text" [(ngModel)]="TravelDestination">\n\n      </ion-input>\n\n    </ion-item>\n\n\n\n\n\n\n\n    <ion-item *ngIf="UAEsegmnets" >\n\n      <ion-label floating class="TecomSalary">Salary Update in TECOM\n\n       \n\n      </ion-label>\n\n      <ion-select [(ngModel)]="selectedTecomUpdateSalaryType" class="select-1" interface="action-sheet" >\n\n        <ion-option *ngFor="let item of tecomUpdateSalaryTypes" [value]="item"> {{item.flex_value}} </ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n</ion-content>\n\n<ion-footer>\n\n\n\n    <button ion-button  color="dark" block (click)="previewLetterRequest()">Preview</button>\n\n   \n\n</ion-footer>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\letters-preview\letters-preview.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__["a" /* HrSitProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], LettersPreviewPage);
    return LettersPreviewPage;
}());

//# sourceMappingURL=letters-preview.js.map

/***/ }),

/***/ 541:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LettersSubmitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__hr_requests_hr_requests__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the LettersSubmitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LettersSubmitPage = (function () {
    function LettersSubmitPage(navCtrl, navParams, loadingController, hrSit, _DomSanitizer, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingController = loadingController;
        this.hrSit = hrSit;
        this._DomSanitizer = _DomSanitizer;
        this.AlertController = AlertController;
        this.UAEsegmnets = false;
        this.KSAsegments = false;
        this.otherSegmnts = false;
        this.selectedLetterType = this.navParams.get('RselectedLetterType');
        this.selectedTecomUpdateSalaryType = this.navParams.get('RselectedTecomUpdateSalaryType');
        ;
        this.TravelDestination = this.navParams.get('RTravelDestination');
        this.cbt = this.navParams.get('Rcbt');
        this.cb = this.navParams.get('Rcb');
        this.lt1 = this.navParams.get('Rlt1');
        this.lt2 = this.navParams.get('Rlt2');
        this.approverList = this.navParams.get('RapproverList');
        this.businessGroup = __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup;
        if (this.businessGroup) {
            if (this.businessGroup == '81') {
                this.UAEsegmnets = true;
            }
            else if (this.businessGroup == '998') {
                this.KSAsegments = true;
            }
            else {
                this.otherSegmnts = true;
            }
        }
    }
    LettersSubmitPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LettersSubmitPage');
    };
    LettersSubmitPage.prototype.submitRerquest = function () {
        var _this = this;
        try {
            this.hrSit.submitLetter('Y', this.selectedLetterType.letter_code, this.cbt, this.TravelDestination, this.selectedTecomUpdateSalaryType, this.lt1, this.lt2, this.cb).then(function (data) {
                if (data[0].message == '2') {
                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__hr_requests_hr_requests__["a" /* HrRequestsPage */]);
                }
            });
        }
        catch (err) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
        }
    };
    LettersSubmitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-letters-submit',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\letters-submit\letters-submit.html"*/'<!--\n\n  Generated template for the LettersSubmitPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Submit Request</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content >\n\n\n\n    \n\n        <ion-item-divider color="light"> Request details</ion-item-divider>\n\n        <ion-list>\n\n      \n\n          <ion-item>\n\n            <div class="typec">\n\n      \n\n              Letter Type\n\n              <span class=" typsesc" >\n\n                  {{selectedLetterType.letter_type}}\n\n                </span>\n\n            </div>\n\n          \n\n      \n\n          </ion-item>\n\n\n\n          <ion-item *ngIf="UAEsegmnets" >\n\n            <div class=" typec">\n\n      \n\n                Company/Consulate/Bank/TECOM\n\n                <span class="typsesc"  >\n\n                    {{cbt}}\n\n                </span>\n\n            </div>\n\n           \n\n      \n\n          </ion-item>\n\n\n\n          <ion-item *ngIf="otherSegmnts">\n\n            <div class=" typec">\n\n      \n\n                Company/Consulate/Bank\n\n                <span class="typsesc"  >\n\n                    {{cb}}\n\n                </span>\n\n            </div>\n\n\n\n\n\n           \n\n      \n\n          </ion-item>\n\n\n\n          <ion-item *ngIf="UAEsegmnets || otherSegmnts" >\n\n            <div class="item-note typec">\n\n      \n\n                Travel Destination\n\n                <span class="typsesc" >\n\n                    {{TravelDestination}}\n\n                  </span>\n\n            </div>\n\n            \n\n      \n\n          </ion-item>\n\n          \n\n         \n\n        <ion-item *ngIf="UAEsegmnets">\n\n        <div class="item-note typec">\n\n      \n\n            Salary Update in TECOM\n\n        </div>\n\n        <div class="item-note typsesc" item-end>\n\n          \n\n          {{selectedTecomUpdateSalaryType}}\n\n        </div>\n\n      \n\n      </ion-item>\n\n\n\n\n\n      <ion-item *ngIf="KSAsegments">\n\n        <div class="item-note typec">\n\n  \n\n          Letter To (Line 1)\n\n            <span class="typsesc" >\n\n                {{lt1}}\n\n              </span>\n\n        </div>\n\n</ion-item>\n\n\n\n        <ion-item *ngIf="KSAsegments">\n\n          <div class="item-note typec">\n\n    \n\n            Letter To (Line 2)\n\n              <span class="typsesc" >\n\n                  {{lt2}}\n\n                </span>\n\n          </div>\n\n      </ion-item>\n\n     \n\n      </ion-list>\n\n\n\n      <ion-item-divider color="light"   item-end>Approvers</ion-item-divider >\n\n        <ion-list  >\n\n\n\n      <ion-item ion-item *ngFor="let item of approverList"  block >\n\n          \n\n            \n\n      \n\n           \n\n            <ion-avatar item-start>\n\n               <img [src]="_DomSanitizer.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+item.imge)" [hidden]="item.imge==null" />\n\n               <div class="divx" [hidden]="item.imge!=null"  >  {{item.intials}} </div>\n\n              </ion-avatar>\n\n              <h2 class="item-note typec2">\n\n            {{item.display_name}}\n\n            </h2 >\n\n          <!-- </div> -->\n\n        <p class="item-note dates2" >\n\n          {{item.job_title}}\n\n        </p>\n\n\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n</ion-content>\n\n<ion-footer>\n\n\n\n    <button ion-button  color="dark" block (click)="submitRerquest()">Submit</button>\n\n   \n\n</ion-footer>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\letters-submit\letters-submit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__["a" /* HrSitProvider */], __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], LettersSubmitPage);
    return LettersSubmitPage;
}());

//# sourceMappingURL=letters-submit.js.map

/***/ }),

/***/ 542:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HousingPreviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_hr_sit_hr_sit__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__housing_submit_housing_submit__ = __webpack_require__(543);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the HousingPreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HousingPreviewPage = (function () {
    function HousingPreviewPage(navCtrl, navParams, hrSit, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.hrSit = hrSit;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.getpageRequestTypes();
    }
    HousingPreviewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HousingPreviewPage');
    };
    HousingPreviewPage.prototype.getpageRequestTypes = function () {
        var _this = this;
        try {
            this.hrSit.getHousingRequestTypes().then(function (data) {
                _this.requestTypes = data;
            });
        }
        catch (error) {
        }
    };
    HousingPreviewPage.prototype.previewHousingRequest = function () {
        var _this = this;
        if (!this.selectedRequestType) {
            this.showError('RequestTypeId');
            return;
        }
        else {
            /////
            try {
                this.hrSit.submitHousing('W', this.selectedRequestType.meaning).then(function (data) {
                    if (data[0].message == '2') {
                        __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                        return;
                    }
                    else {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__housing_submit_housing_submit__["a" /* HousingSubmitPage */], { RselectedRequestType: _this.selectedRequestType.meaning, RapproverList: data });
                    }
                });
            }
            catch (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
            }
            ////
        }
    };
    HousingPreviewPage.prototype.showError = function (elementId) {
        var el;
        el = document.getElementsByClassName(elementId)[0];
        __WEBPACK_IMPORTED_MODULE_4_jquery__(el).addClass('error-color');
    };
    HousingPreviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-housing-preview',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\housing-preview\housing-preview.html"*/'<!--\n\n  Generated template for the HousingPreviewPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Housing Advance</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    \n\n  <ion-item-divider color="light"> Please provide the following details</ion-item-divider>\n\n        <ion-list> \n\n          <ion-item >\n\n            <ion-label floating class="RequestTypeId"> Request Type\n\n              <i class="req-symbol">*</i>\n\n            </ion-label>\n\n            <ion-select [(ngModel)]="selectedRequestType"  interface="action-sheet" >\n\n              <ion-option *ngFor="let item of requestTypes" [value]="item"> {{item.meaning}} </ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n\n\n          </ion-list>\n\n</ion-content>\n\n<ion-footer>\n\n\n\n    <button ion-button  color="dark" block (click)="previewHousingRequest()">Preview</button>\n\n   \n\n</ion-footer>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\housing-preview\housing-preview.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_hr_sit_hr_sit__["a" /* HrSitProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], HousingPreviewPage);
    return HousingPreviewPage;
}());

//# sourceMappingURL=housing-preview.js.map

/***/ }),

/***/ 543:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HousingSubmitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__hr_requests_hr_requests__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the HousingSubmitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HousingSubmitPage = (function () {
    function HousingSubmitPage(navCtrl, navParams, _DomSanitizer, hrSit, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._DomSanitizer = _DomSanitizer;
        this.hrSit = hrSit;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.selectedRequestType = this.navParams.get('RselectedRequestType');
        this.approverList = this.navParams.get('RapproverList');
    }
    HousingSubmitPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HousingSubmitPage');
    };
    HousingSubmitPage.prototype.submitRerquest = function () {
        ////
        var _this = this;
        try {
            this.hrSit.submitHousing('Y', this.selectedRequestType).then(function (data) {
                if (data[0].message == '2') {
                    __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__hr_requests_hr_requests__["a" /* HrRequestsPage */]);
                }
            });
        }
        catch (err) {
            __WEBPACK_IMPORTED_MODULE_5__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
        }
        ///
    };
    HousingSubmitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-housing-submit',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\housing-submit\housing-submit.html"*/'<!--\n\n  Generated template for the HousingSubmitPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Submit Request</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <ion-item-divider color="light"> Request details</ion-item-divider>\n\n    <ion-list>\n\n  \n\n      <ion-item>\n\n        <div class="typec">\n\n            <span class=" typsesc" >\n\n                {{selectedRequestType}}\n\n              </span>\n\n          Request Type\n\n        </div>\n\n        \n\n  \n\n      </ion-item>\n\n      </ion-list>\n\n\n\n      <ion-item-divider color="light"   item-end>Approvers</ion-item-divider >\n\n        <ion-list  >\n\n\n\n      <ion-item ion-item *ngFor="let item of approverList"  block >\n\n          \n\n            \n\n      \n\n           \n\n            <ion-avatar item-start>\n\n               <img [src]="_DomSanitizer.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+item.imge)" [hidden]="item.imge==null" />\n\n               <div class="divx" [hidden]="item.imge!=null"  >  {{item.intials}} </div>\n\n              </ion-avatar>\n\n              <h2 class="item-note typec2">\n\n            {{item.display_name}}\n\n            </h2 >\n\n          <!-- </div> -->\n\n        <p class="item-note dates2" >\n\n          {{item.job_title}}\n\n        </p>\n\n        <!-- <div class="item-note durationc" >\n\n            {{item.absence_no_days}}\n\n          </div> -->\n\n          <!-- <div class="item-note remarksc" >\n\n              {{item.start_date}}\n\n            </div> -->\n\n      </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n<ion-footer>\n\n\n\n    <button ion-button  color="dark" block (click)="submitRerquest()" >Submit</button>\n\n   \n\n</ion-footer>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\housing-submit\housing-submit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__["a" /* HrSitProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], HousingSubmitPage);
    return HousingSubmitPage;
}());

//# sourceMappingURL=housing-submit.js.map

/***/ }),

/***/ 544:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BusinesscardPreviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__businesscard_submit_businesscard_submit__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**

/**
 * Generated class for the BusinesscardPreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BusinesscardPreviewPage = (function () {
    function BusinesscardPreviewPage(navCtrl, navParams, hrSit, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.hrSit = hrSit;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.getBusinessCardTypes();
        this.getBusinessCardlanguages();
        this.SetDefaultCardData();
    }
    BusinesscardPreviewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BusinesscardPreviewPage');
    };
    BusinesscardPreviewPage.prototype.getBusinessCardTypes = function () {
        var _this = this;
        try {
            this.hrSit.getBusinessCardTypes().then(function (data) {
                _this.CardTypes = data;
            });
        }
        catch (error) {
        }
    };
    BusinesscardPreviewPage.prototype.getBusinessCardlanguages = function () {
        var _this = this;
        try {
            this.hrSit.getBusinessCardLanguages().then(function (data) {
                _this.Languages = data;
            });
        }
        catch (error) {
        }
    };
    BusinesscardPreviewPage.prototype.SetDefaultCardData = function () {
        var _this = this;
        try {
            this.hrSit.getBusinessCardDefaultData().then(function (data) {
                if (data) {
                    if (data && data[0])
                        _this.englishName = data[0].full_name;
                    _this.enJobTitle = data[0].job_title;
                    _this.officePhone = data[0].phone;
                    _this.mobile = data[0].mobile;
                    _this.email = data[0].email_address;
                    _this.officePhone = '043919999';
                }
            });
        }
        catch (error) {
            console.log('error');
        }
    };
    BusinesscardPreviewPage.prototype.previewRequest = function () {
        var _this = this;
        var error = 0;
        if (!this.selectedCardType) {
            this.showError('CardTypeId');
            error = 1;
        }
        else {
            this.removeError('CardTypeId');
        }
        if (!this.selecteLanguage) {
            this.showError('language');
            error = 1;
        }
        else {
            this.removeError('language');
        }
        if (!this.englishName) {
            this.showError('englishName');
            error = 1;
        }
        else {
            this.removeError('englishName');
        }
        if (!this.enJobTitle) {
            this.showError('enJobTitle');
            error = 1;
        }
        else {
            this.removeError('enJobTitle');
        }
        if (!this.officePhone) {
            this.showError('officePhone');
            error = 1;
        }
        else {
            this.removeError('officePhone');
        }
        if (!this.mobile) {
            this.showError('mobile');
            error = 1;
        }
        else {
            this.removeError('mobile');
        }
        if (error == 1)
            return;
        else {
            try {
                this.hrSit.submitBusinessCard('W', this.selectedCardType.lookup_code, this.selecteLanguage.lookup_code, this.englishName, this.arbichName, this.enJobTitle, this.arjobTitle, this.officePhone, this.ext, this.officeFax, this.mobile, this.email).then(function (data) {
                    if (data[0].message == '2') {
                        __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                        return;
                    }
                    else {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__businesscard_submit_businesscard_submit__["a" /* BusinesscardSubmitPage */], {
                            RselectedCardType: _this.selectedCardType,
                            RselecteLanguage: _this.selecteLanguage,
                            RenglishName: _this.englishName,
                            RarbichName: _this.arbichName,
                            RenJobTitle: _this.enJobTitle,
                            RarjobTitle: _this.arjobTitle,
                            RofficePhone: _this.officePhone,
                            Rext: _this.ext,
                            RofficeFax: _this.officeFax,
                            Rmobile: _this.mobile,
                            Remail: _this.email,
                            RapproverList: data
                        });
                    }
                });
            }
            catch (err) {
                __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
            }
        }
    };
    BusinesscardPreviewPage.prototype.showError = function (elementId) {
        var el;
        el = document.getElementsByClassName(elementId)[0];
        __WEBPACK_IMPORTED_MODULE_5_jquery__(el).addClass('error-color');
    };
    BusinesscardPreviewPage.prototype.removeError = function (elementId) {
        var el;
        el = document.getElementsByClassName(elementId)[0];
        __WEBPACK_IMPORTED_MODULE_5_jquery__(el).removeClass('error-color');
    };
    BusinesscardPreviewPage.prototype.onChange = function () {
        var _this = this;
        this.selecteLanguage = null;
        this.filterLanguages = this.Languages.filter(function (item) {
            return (item.tag.toLowerCase().indexOf(_this.selectedCardType.lookup_code.toLowerCase()) > -1);
        });
    };
    BusinesscardPreviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-businesscard-preview',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\businesscard-preview\businesscard-preview.html"*/'<!--\n\n  Generated template for the BusinesscardPreviewPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Business Card</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <ion-item-divider color="light"> Please provide the following details</ion-item-divider>\n\n    <ion-list> \n\n        \n\n      <ion-item >\n\n          <ion-label floating class="CardTypeId" >Card Type\n\n            <i class="req-symbol">*</i>\n\n          </ion-label>\n\n          <ion-select [(ngModel)]="selectedCardType" interface="action-sheet" (ionChange)="onChange()" >\n\n            <ion-option *ngFor="let item of CardTypes" [value]="item"> {{item.meaning}} </ion-option>\n\n          </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item >\n\n            <ion-label floating class="language">Language\n\n              <i class="req-symbol">*</i>\n\n            </ion-label>\n\n            <ion-select [(ngModel)]="selecteLanguage" interface="action-sheet" >\n\n              <ion-option *ngFor="let item of filterLanguages" [value]="item"> {{item.meaning}} </ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n\n\n\n\n          <ion-item  >\n\n              <ion-label class="englishName" floating>English Name\n\n                <i class="req-symbol">*</i>\n\n              </ion-label>\n\n              <ion-input type="text" [(ngModel)]="englishName">\n\n              </ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item  >\n\n                <ion-label  floating>Arabic Name\n\n                  \n\n                </ion-label>\n\n                <ion-input type="text" [(ngModel)]="arbichName">\n\n                </ion-input>\n\n              </ion-item>\n\n\n\n              <ion-item  >\n\n                  <ion-label class="enJobTitle" floating>English Job Title\n\n                    <i class="req-symbol">*</i>\n\n                  </ion-label>\n\n                  <ion-input type="text" [(ngModel)]="enJobTitle">\n\n                  </ion-input>\n\n                </ion-item>\n\n\n\n                <ion-item  >\n\n                    <ion-label  floating>Arabic Job Title\n\n                      \n\n                    </ion-label>\n\n                    <ion-input type="text" [(ngModel)]="arjobTitle">\n\n                    </ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item  >\n\n                      <ion-label class="officePhone" floating>Office Phone\n\n                        <i class="req-symbol">*</i>\n\n                      </ion-label>\n\n                      <ion-input type="number" [(ngModel)]="officePhone">\n\n                      </ion-input>\n\n                    </ion-item>\n\n\n\n                    <ion-item  >\n\n                        <ion-label floating>Ext\n\n                         \n\n                        </ion-label>\n\n                        <ion-input type="number" [(ngModel)]="ext">\n\n                        </ion-input>\n\n                      </ion-item>\n\n\n\n\n\n                      <ion-item  >\n\n                          <ion-label  floating>Office Fax\n\n                            \n\n                          </ion-label>\n\n                          <ion-input type="number" [(ngModel)]="officeFax">\n\n                          </ion-input>\n\n                        </ion-item>\n\n\n\n\n\n                        <ion-item  >\n\n                            <ion-label class="mobile" floating>Mobile\n\n                              <i class="req-symbol">*</i>\n\n                            </ion-label>\n\n                            <ion-input type="number" [(ngModel)]="mobile">\n\n                            </ion-input>\n\n                          </ion-item>\n\n\n\n\n\n                          <ion-item  calss="email-block">\n\n                              <ion-label class="email" floating>Email\n\n                                <i class="req-symbol">*</i>\n\n                              </ion-label>\n\n                               <ion-input readonly=\'true\' type="email" [(ngModel)]="email"> \n\n                              \n\n                              </ion-input>\n\n                            </ion-item>\n\n\n\n\n\n    \n\n        </ion-list>\n\n\n\n</ion-content>\n\n<ion-footer>\n\n \n\n    <button ion-button  color="dark" block (click)="previewRequest()">Preview</button>\n\n   \n\n</ion-footer>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\businesscard-preview\businesscard-preview.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__["a" /* HrSitProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], BusinesscardPreviewPage);
    return BusinesscardPreviewPage;
}());

//# sourceMappingURL=businesscard-preview.js.map

/***/ }),

/***/ 545:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BusinesscardSubmitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__hr_requests_hr_requests__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the BusinesscardSubmitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BusinesscardSubmitPage = (function () {
    function BusinesscardSubmitPage(navCtrl, navParams, hrSit, loadingController, _DomSanitizer, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.hrSit = hrSit;
        this.loadingController = loadingController;
        this._DomSanitizer = _DomSanitizer;
        this.AlertController = AlertController;
        this.selectedCardType = this.navParams.get('RselectedCardType');
        this.selecteLanguage = this.navParams.get('RselecteLanguage');
        this.englishName = this.navParams.get('RenglishName');
        this.arbichName = this.navParams.get('RarbichName');
        this.enJobTitle = this.navParams.get('RenJobTitle');
        this.arjobTitle = this.navParams.get('RarjobTitle');
        this.officePhone = this.navParams.get('RofficePhone');
        this.ext = this.navParams.get('Rext');
        this.officeFax = this.navParams.get('RofficeFax');
        this.mobile = this.navParams.get('Rmobile');
        this.email = this.navParams.get('Remail');
        this.approverList = this.navParams.get('RapproverList');
    }
    BusinesscardSubmitPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BusinesscardSubmitPage');
    };
    BusinesscardSubmitPage.prototype.submitRerquest = function () {
        var _this = this;
        try {
            this.hrSit.submitBusinessCard('Y', this.selectedCardType.lookup_code, this.selecteLanguage.lookup_code, this.englishName, this.arbichName, this.enJobTitle, this.arjobTitle, this.officePhone, this.ext, this.officeFax, this.mobile, this.email).then(function (data) {
                if (data[0].message == '2') {
                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__hr_requests_hr_requests__["a" /* HrRequestsPage */]);
                }
            });
        }
        catch (err) {
            __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, err.message);
        }
    };
    BusinesscardSubmitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-businesscard-submit',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\businesscard-submit\businesscard-submit.html"*/'<!--\n\n  Generated template for the BusinesscardSubmitPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Submit Request</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <ion-item-divider color="light"> Request details</ion-item-divider>\n\n    \n\n\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Card Type\n\n          <span class=" typsesc" >\n\n              {{selectedCardType.meaning}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Language\n\n          <span class=" typsesc" >\n\n              {{selecteLanguage.meaning}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            English Name\n\n          <span class=" typsesc" >\n\n              {{englishName}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Arabic Name\n\n          <span class=" typsesc" >\n\n              {{arbichName}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            English Job Title\n\n          <span class=" typsesc" >\n\n              {{enJobTitle}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Arabic Job Title\n\n          <span class=" typsesc" >\n\n              {{arjobTitle}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Office Phone\n\n          <span class=" typsesc" >\n\n              {{officePhone}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Ext\n\n          <span class=" typsesc" >\n\n              {{ext}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Office Fax\n\n          <span class=" typsesc" >\n\n              {{officeFax}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Mobile\n\n          <span class=" typsesc" >\n\n              {{mobile}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="typec">\n\n  \n\n            Email\n\n          <span class=" typsesc" >\n\n              {{email}}\n\n            </span>\n\n        </div>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item-divider color="light"   item-end>Approvers</ion-item-divider >\n\n      <ion-list  >\n\n    \n\n    <ion-item ion-item *ngFor="let item of approverList"  block >\n\n        \n\n          \n\n    \n\n         \n\n          <ion-avatar item-start>\n\n             <img [src]="_DomSanitizer.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+item.imge)" [hidden]="item.imge==null" />\n\n             <div class="divx" [hidden]="item.imge!=null"  >  {{item.intials}} </div>\n\n            </ion-avatar>\n\n            <h2 class="item-note typec2">\n\n          {{item.display_name}}\n\n          </h2 >\n\n        <!-- </div> -->\n\n      <p class="item-note dates2" >\n\n        {{item.job_title}}\n\n      </p>\n\n    \n\n    </ion-item>\n\n    </ion-list>\n\n    \n\n\n\n</ion-content>\n\n\n\n\n\n\n\n<ion-footer>\n\n\n\n    <button ion-button  color="dark" block (click)="submitRerquest()">Submit</button>\n\n   \n\n</ion-footer>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\businesscard-submit\businesscard-submit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_hr_sit_hr_sit__["a" /* HrSitProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], BusinesscardSubmitPage);
    return BusinesscardSubmitPage;
}());

//# sourceMappingURL=businesscard-submit.js.map

/***/ }),

/***/ 546:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PayrollHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_pages_start_start__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__payslip_payslip__ = __webpack_require__(547);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__bank_info_bank_info__ = __webpack_require__(549);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__salary_history_salary_history__ = __webpack_require__(551);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the PayrollHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PayrollHomePage = (function () {
    function PayrollHomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PayrollHomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PayrollHomePage');
        this.setBackButtonAction();
    };
    PayrollHomePage.prototype.setBackButtonAction = function () {
        var _this = this;
        this.Navbar.backButtonClick = function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__shared_pages_start_start__["a" /* StartPage */]);
        };
    };
    PayrollHomePage.prototype.goPaySlipPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__payslip_payslip__["a" /* PayslipPage */]);
    };
    PayrollHomePage.prototype.goBankInfoPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__bank_info_bank_info__["a" /* BankInfoPage */]);
    };
    PayrollHomePage.prototype.goSalaryHistory = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__salary_history_salary_history__["a" /* SalaryHistoryPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */])
    ], PayrollHomePage.prototype, "Navbar", void 0);
    PayrollHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payroll-home',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\payroll-home\payroll-home.html"*/'<!--\n\n  Generated template for the PayrollHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n<ion-header>\n\n\n\n  <ion-navbar hideBackButton=\'flase\'>\n\n\n\n    <ion-title>Payroll</ion-title>\n\n  </ion-navbar>\n\n  \n\n  \n\n \n\n  \n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goPaySlipPage()">\n\n      <fa-icon name="file-powerpoint-o"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Payslip</h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n  </div>\n\n\n\n\n\n  <div class="mnb"   >\n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goBankInfoPage()">\n\n       \n\n          <fa-icon name="university"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Bank </h6>\n\n      <h6 ion-text color="dark" class="btn-txt " >Info</h6>\n\n  </div>\n\n\n\n  <div class="mnb"   >\n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goSalaryHistory()">\n\n       \n\n          <fa-icon name="line-chart"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Salary </h6>\n\n      <h6 ion-text color="dark" class="btn-txt " >History</h6>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\payroll-home\payroll-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], PayrollHomePage);
    return PayrollHomePage;
}());

//# sourceMappingURL=payroll-home.js.map

/***/ }),

/***/ 547:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PayslipPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_payroll_payroll__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__payslip_details_payslip_details__ = __webpack_require__(548);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the PayslipPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PayslipPage = (function () {
    function PayslipPage(navCtrl, navParams, payrollData, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.payrollData = payrollData;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.fromR = 0;
        this.v_step = 12;
        this.toR = this.v_step;
        this.businessGroup = __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup;
        this.getPayslipList();
    }
    PayslipPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PayslipPage');
    };
    PayslipPage.prototype.getPayslipList = function () {
        var _this = this;
        try {
            this.payrollData.getPayslipList()
                .then(function (data) {
                _this.payrollListAll = data;
                if (_this.payrollListAll) {
                    _this.payrollList = _this.payrollListAll.slice(_this.fromR, _this.toR);
                }
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, "error");
        }
    };
    PayslipPage.prototype.getSelectedEarnings = function (actionContextid) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        _this.payrollData.getPayrollEarnings(actionContextid)
                            .then(function (data) {
                            resolve(data);
                        }, function (err) {
                            resolve();
                        });
                    })];
            });
        });
    };
    PayslipPage.prototype.getSelectedDeduction = function (actionContextid) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        _this.payrollData.getPayrollDeductions(actionContextid)
                            .then(function (data) {
                            resolve(data);
                        }, function (err) {
                            resolve();
                        });
                    })];
            });
        });
    };
    PayslipPage.prototype.doRefresh = function (refresher) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.toR = this.v_step;
                        return [4 /*yield*/, this.getPayslipList()];
                    case 1:
                        _a.sent();
                        refresher.complete();
                        return [2 /*return*/];
                }
            });
        });
    };
    PayslipPage.prototype.payslipTapped = function (event, item) {
        return __awaiter(this, void 0, void 0, function () {
            var earning, deduction, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (item == null)
                            return [2 /*return*/];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, this.getSelectedEarnings(item.action_context_id)];
                    case 2:
                        earning = _a.sent();
                        return [4 /*yield*/, this.getSelectedDeduction(item.action_context_id)];
                    case 3:
                        // console.log (earning)
                        deduction = _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        err_1 = _a.sent();
                        //alert ('dddd');
                        __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, "error");
                        return [3 /*break*/, 5];
                    case 5:
                        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__payslip_details_payslip_details__["a" /* PayslipDetailsPage */], {
                            item: item,
                            earning: earning,
                            deduction: deduction
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PayslipPage.prototype.doInfinite = function (infiniteScroll) {
        // this.fromR=this.fromR+this.v_step;
        this.toR = this.toR + this.v_step;
        try {
            if (this.payrollListAll)
                this.payrollList = (this.payrollListAll.slice(0, this.toR));
            infiniteScroll.complete();
        }
        catch (err) {
        }
        console.log(' infiniteScroll has ended');
    };
    PayslipPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payslip',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\payslip\payslip.html"*/'<!--\n\n  Generated template for the PayslipPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Payslip</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content pullingIcon="arrow-dropdown"\n\n        pullingText="Pull to refresh"\n\n        refreshingSpinner="circles"\n\n        refreshingText="Refreshing..."></ion-refresher-content>\n\n      </ion-refresher>\n\n      <!-- <ion-item-divider color="light"  item-end> Payslips</ion-item-divider > -->\n\n      <ion-list  >\n\n          <button ion-item *ngFor="let item of payrollList" (click)="payslipTapped($event, item )" block >\n\n          \n\n            \n\n              <div class="item-note typec" >\n\n                  <!-- <ion-icon  name="checkmark-circle-outline" item-start class="ico1"></ion-icon> -->\n\n                  {{item.payslip_txt}}\n\n                </div>\n\n\n\n                <!-- <ion-avatar item-start> \n\n                   \n\n                    <div class="divx"   >  {{item.intials}} </div>\n\n                   </ion-avatar> -->\n\n          \n\n            <!-- <div class="item-note durationc" >\n\n                {{item.effective_date}}\n\n              </div> -->\n\n          </button>\n\n        </ion-list>\n\n        <ion-infinite-scroll (ionInfinite)="doInfinite($event)" > \n\n            <ion-infinite-scroll-content\n\n            loadingSpinner="bubbles"\n\n            loadingText="Loading more data...">\n\n          </ion-infinite-scroll-content> \n\n          </ion-infinite-scroll>\n\n\n\n          \n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\payslip\payslip.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_payroll_payroll__["a" /* PayrollProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], PayslipPage);
    return PayslipPage;
}());

//# sourceMappingURL=payslip.js.map

/***/ }),

/***/ 548:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PayslipDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PayslipDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PayslipDetailsPage = (function () {
    function PayslipDetailsPage(navCtrl, navParams, decimalPipe) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.decimalPipe = decimalPipe;
        this.payslip = "Summary";
        this.totalEarning = 0;
        this.totalDeduction = 0;
        this.totalEarning_txt = '0';
        this.totalDeduction_txt = '0';
        this.netPay = 0;
        try {
            this.selectedPaySlip = this.navParams.get('item');
            this.title = this.selectedPaySlip.payslip_txt;
            this.earning = this.navParams.get('earning');
            this.deduction = this.navParams.get('deduction');
            if (this.earning && this.earning[0] && this.earning[0].total_value) {
                this.totalEarning = this.earning[0].total_value;
                this.currency = this.earning[0].currency;
                //this.totalEarning_txt=this.earning[0].total_value_txt;
            }
            if (this.deduction && this.deduction[0] && this.deduction[0].total_value) {
                this.totalDeduction = this.deduction[0].total_value;
                //this.totalDeduction_txt=this.earning[0].total_value_txt;
            }
            this.netPay = this.totalEarning - this.totalDeduction;
        }
        catch (err) {
        }
    }
    ;
    PayslipDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PayslipDetailsPage');
    };
    PayslipDetailsPage.prototype.twoDecimals = function (number) {
        return this.decimalPipe.transform(number, '1.2-2');
    };
    PayslipDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payslip-details',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\payslip-details\payslip-details.html"*/'<!--\n\n  Generated template for the PayslipDetailsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{title}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding="true">\n\n\n\n  <ion-segment [(ngModel)]="payslip">\n\n    <ion-segment-button value="Summary">\n\n      Summary\n\n    </ion-segment-button>\n\n    <ion-segment-button value="Earning">\n\n      Earnings\n\n    </ion-segment-button>\n\n    <ion-segment-button value="Deductions">\n\n      Deductions\n\n    </ion-segment-button>\n\n\n\n  </ion-segment>\n\n\n\n\n\n  <div [ngSwitch]="payslip">\n\n\n\n    <ion-list *ngSwitchCase="\'Summary\'">\n\n        <ion-list class="list1">\n\n        <ion-item>\n\n          <div class="list-name">\n\n           Total Earnings\n\n            <span class="list-value">\n\n              {{totalEarning | number:\'1.2-2\' }}{{currency}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <div class="list-name">\n\n             Total Deductions\n\n              <span class="list-value">\n\n                {{totalDeduction | number:\'1.2-2\' }}{{currency}}\n\n              </span>\n\n            </div>\n\n          </ion-item>\n\n\n\n\n\n          <ion-item>\n\n              <div class="list-name bold-txt">\n\n               Net Pay\n\n                <span class="list-value bold-txt">\n\n                  {{netPay| number:\'1.2-2\' }}{{currency}}\n\n                </span>\n\n              </div>\n\n            </ion-item>\n\n\n\n        \n\n      </ion-list>\n\n\n\n\n\n\n\n\n\n\n\n\n\n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'Earning\'">\n\n\n\n\n\n      <ion-list class="list1">\n\n        <ion-item *ngFor="let item of earning">\n\n\n\n\n\n          <div class="list-name">\n\n\n\n            {{item.element_name}}\n\n            <span class="list-value">\n\n              {{item.pay_value}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n      </ion-list>\n\n\n\n\n\n\n\n\n\n\n\n\n\n    </ion-list>\n\n\n\n\n\n\n\n\n\n    <ion-list *ngSwitchCase="\'Deductions\'">\n\n      <ion-list class="list1">\n\n        <ion-item *ngFor="let item of deduction">\n\n\n\n\n\n          <div class="list-name">\n\n\n\n            {{item.element_name}}\n\n            <span class="list-value">\n\n              {{item.pay_value}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n      </ion-list>\n\n\n\n    </ion-list>\n\n\n\n\n\n  </div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\payslip-details\payslip-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["d" /* DecimalPipe */]])
    ], PayslipDetailsPage);
    return PayslipDetailsPage;
}());

//# sourceMappingURL=payslip-details.js.map

/***/ }),

/***/ 549:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BankInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_payroll_payroll__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__bank_details_bank_details__ = __webpack_require__(550);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the BankInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BankInfoPage = (function () {
    function BankInfoPage(navCtrl, navParams, payrollData) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.payrollData = payrollData;
        this.businessGroup = __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup;
        this.getBanksInfo();
    }
    BankInfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BankInfoPage');
    };
    BankInfoPage.prototype.getBanksInfo = function () {
        var _this = this;
        try {
            this.payrollData.getBankInfo()
                .then(function (data) {
                _this.banks = data;
            });
        }
        catch (error) {
            //this.showToast('middle','Connection Problem');
            alert(error);
        }
    };
    BankInfoPage.prototype.doRefresh = function (refresher) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getBanksInfo()];
                    case 1:
                        _a.sent();
                        refresher.complete();
                        return [2 /*return*/];
                }
            });
        });
    };
    BankInfoPage.prototype.bankTapped = function (event, item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__bank_details_bank_details__["a" /* BankDetailsPage */], {
            item: item
        });
    };
    BankInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-bank-info',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\bank-info\bank-info.html"*/'<!--\n\n  Generated template for the BankInfoPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Bank Info</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content pullingIcon="arrow-dropdown"\n\n        pullingText="Pull to refresh"\n\n        refreshingSpinner="circles"\n\n        refreshingText="Refreshing..."></ion-refresher-content>\n\n      </ion-refresher>\n\n\n\n      <ion-list  >\n\n          <button ion-item *ngFor="let item of banks" (click)="bankTapped($event, item )" block >\n\n          \n\n       \n\n\n\n                <ion-avatar item-start> \n\n                   \n\n                    <div class="divx"   >  {{item.priority}} </div>\n\n                   </ion-avatar>\n\n          \n\n            <div class="item-note list-item-title1" >\n\n                {{item.bank_name}}\n\n              </div> \n\n              <div class="item-note list-item-sub-title1" >\n\n                  {{item.bank_branch}}\n\n                </div>\n\n                <div class="item-note list-item-sub-title1" >\n\n                    {{item.iban}}\n\n                  </div>\n\n          </button>\n\n        </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\bank-info\bank-info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_payroll_payroll__["a" /* PayrollProvider */]])
    ], BankInfoPage);
    return BankInfoPage;
}());

//# sourceMappingURL=bank-info.js.map

/***/ }),

/***/ 550:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BankDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BankDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BankDetailsPage = (function () {
    function BankDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.selectedBank = this.navParams.get('item');
        this.title = this.selectedBank.bank_name;
    }
    BankDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BankDetailsPage');
    };
    BankDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-bank-details',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\bank-details\bank-details.html"*/'<!--\n\n  Generated template for the BankDetailsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{title}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <!-- <ion-item>\n\n    <div class="list-name">\n\n\n\n        Priority\n\n      <span class="list-value">\n\n        {{selectedBank.priority}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n      Percentage\n\n      <span class="list-value">\n\n        {{selectedBank.percentage}}%\n\n      </span>\n\n    </div>\n\n  </ion-item> -->\n\n\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Payment Method\n\n      <span class="list-value">\n\n        {{selectedBank.org_payment_method_name}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Currency\n\n      <span class="list-value">\n\n        {{selectedBank.currency_code}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Bank\n\n      <span class="list-value">\n\n        {{selectedBank.bank_name}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Branch\n\n      <span class="list-value">\n\n        {{selectedBank.bank_branch}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Beneficiary\n\n      <span class="list-value">\n\n        {{selectedBank.beneficiary_name}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Account Number\n\n      <span class="list-value">\n\n        {{selectedBank.account_number}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Swift Code\n\n      <span class="list-value">\n\n        {{selectedBank.swift_code}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n      IBAN\n\n      <span class="list-value">\n\n        {{selectedBank.iban}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n      City\n\n      <span class="list-value">\n\n        {{selectedBank.city}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n      Country\n\n      <span class="list-value">\n\n        {{selectedBank.country}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n      <div class="list-name">\n\n  \n\n          BIC Code INT Bank\n\n        <span class="list-value">\n\n          {{selectedBank.bic_code_int_bank}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\bank-details\bank-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], BankDetailsPage);
    return BankDetailsPage;
}());

//# sourceMappingURL=bank-details.js.map

/***/ }),

/***/ 551:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SalaryHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_payroll_payroll__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SalaryHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SalaryHistoryPage = (function () {
    function SalaryHistoryPage(navCtrl, navParams, payrollData) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.payrollData = payrollData;
        this.items = [
            {
                title: 'Courgette daikon',
                content: 'Parsley amaranth tigernut silver beet maize fennel spinach. Ricebean black-eyed pea maize scallion green bean spinach cabbage jícama bell pepper carrot onion corn plantain garbanzo. Sierra leone bologi komatsuna celery peanut swiss chard silver beet squash dandelion maize chicory burdock tatsoi dulse radish wakame beetroot.',
                icon: '',
                time: { subtitle: '4/16/2013', title: '21:30' }
            }
            // },
            // {
            //   title: 'Courgette daikon',
            //   content: 'Parsley amaranth tigernut silver beet maize fennel spinach. Ricebean black-eyed pea maize scallion green bean spinach cabbage jícama bell pepper carrot onion corn plantain garbanzo. Sierra leone bologi komatsuna celery peanut swiss chard silver beet squash dandelion maize chicory burdock tatsoi dulse radish wakame beetroot.',
            //   icon: '',
            //   time: {subtitle: 'January', title: '29'}
            // },
            // {
            //   title: 'Courgette daikon',
            //   content: 'Parsley amaranth tigernut silver beet maize fennel spinach. Ricebean black-eyed pea maize scallion green bean spinach cabbage jícama bell pepper carrot onion corn plantain garbanzo. Sierra leone bologi komatsuna celery peanut swiss chard silver beet squash dandelion maize chicory burdock tatsoi dulse radish wakame beetroot.',
            //   icon: '',
            //   time: {title: 'Salary'}
            // }
        ];
        this.getsalaryHistory();
    }
    SalaryHistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SalaryHistoryPage');
    };
    SalaryHistoryPage.prototype.getsalaryHistory = function () {
        var _this = this;
        try {
            this.payrollData.getsalaryHistory()
                .then(function (data) {
                _this.historyList = data;
            });
        }
        catch (error) {
            //this.showToast('middle','Connection Problem');
            alert(error);
        }
    };
    SalaryHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-salary-history',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\salary-history\salary-history.html"*/'<!--\n\n  Generated template for the SalaryHistoryPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Salary History</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <timeline endIcon="call">\n\n    <timeline-item *ngFor="let item of historyList">\n\n      <!-- <timeline-time [time]="item.time"></timeline-time> -->\n\n      <ion-icon [name]="item.icon"></ion-icon>\n\n      <ion-card>\n\n        <!-- <ion-card-header>\n\n          \n\n        </ion-card-header> -->\n\n        <ion-card-content>\n\n            <div class=\'header1\'>{{item.change_date}} </div>\n\n            <div class=\'header2\'> {{item.current_value}}</div> \n\n         <div class=\'card-content1\' *ngIf="item.change_value!=null" >\n\n           {{ item.change_percentage}} ({{item.change_value}})\n\n           \n\n         </div>\n\n         <div class=\'card-content2\' >\n\n          {{item.reason}}\n\n          </div>\n\n        </ion-card-content>\n\n      </ion-card>\n\n    </timeline-item>\n\n  </timeline>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\payroll\salary-history\salary-history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_payroll_payroll__["a" /* PayrollProvider */]])
    ], SalaryHistoryPage);
    return SalaryHistoryPage;
}());

//# sourceMappingURL=salary-history.js.map

/***/ }),

/***/ 552:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basic_data_basic_data__ = __webpack_require__(553);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__employment_summary_employment_summary__ = __webpack_require__(554);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__employee_phone_employee_phone__ = __webpack_require__(555);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__employee_adressess_employee_adressess__ = __webpack_require__(556);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__employee_passport_employee_passport__ = __webpack_require__(557);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__employee_visa_employee_visa__ = __webpack_require__(558);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__dependent_tickets_dependent_tickets__ = __webpack_require__(559);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__labor_card_labor_card__ = __webpack_require__(560);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__relationin_mbc_relationin_mbc__ = __webpack_require__(561);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__national_card_national_card__ = __webpack_require__(562);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













/**
 * Generated class for the ProfileHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfileHomePage = (function () {
    function ProfileHomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.gulfIcons = false;
        if (__WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].businessGroup == '81' || __WEBPACK_IMPORTED_MODULE_12__app_app_module__["a" /* AppModule */].businessGroup == '998')
            this.gulfIcons = true;
    }
    ProfileHomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfileHomePage');
    };
    ProfileHomePage.prototype.openBasicData = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__basic_data_basic_data__["a" /* BasicDataPage */]);
    };
    ProfileHomePage.prototype.openEmploymentSummary = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__employment_summary_employment_summary__["a" /* EmploymentSummaryPage */]);
    };
    ProfileHomePage.prototype.openEmploymentSPhones = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__employee_phone_employee_phone__["a" /* EmployeePhonePage */]);
    };
    ProfileHomePage.prototype.openEmploymentSAddress = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__employee_adressess_employee_adressess__["a" /* EmployeeAdressessPage */]);
    };
    ProfileHomePage.prototype.openEmployeePassport = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__employee_passport_employee_passport__["a" /* EmployeePassportPage */]);
    };
    ProfileHomePage.prototype.openEmployeeVisa = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__employee_visa_employee_visa__["a" /* EmployeeVisaPage */]);
    };
    ProfileHomePage.prototype.openDependentTicket = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__dependent_tickets_dependent_tickets__["a" /* DependentTicketsPage */]);
    };
    ProfileHomePage.prototype.openLaborCard = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__labor_card_labor_card__["a" /* LaborCardPage */]);
    };
    ProfileHomePage.prototype.openRelationinMbc = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__relationin_mbc_relationin_mbc__["a" /* RelationinMbcPage */]);
    };
    ProfileHomePage.prototype.openNationalCard = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__national_card_national_card__["a" /* NationalCardPage */]);
    };
    ProfileHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile-home',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\profile-home\profile-home.html"*/'<!--\n\n  Generated template for the ProfileHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Profile</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openBasicData()">\n\n        <fa-icon name="user"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Personal</h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >Information</h6>\n\n  </div>\n\n\n\n\n\n  <div class="mnb"   >\n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openEmploymentSummary()">\n\n          <fa-icon name="history"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Employee</h6>\n\n      <h6 ion-text color="dark" class="btn-txt " >History</h6>\n\n  </div>\n\n\n\n  <!-- <div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openEmploymentSummary()">\n\n        <fa-icon name="certificate"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Education  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >Qualification</h6>\n\n</div> -->\n\n\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openEmploymentSPhones()">\n\n        <fa-icon name="phone"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Phones  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n</div>\n\n\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openEmploymentSAddress()">\n\n        <fa-icon name="map-marker"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Address  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n</div>\n\n\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openEmployeePassport()">\n\n        <fa-icon name="id-card"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Passport  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n</div>\n\n\n\n<!-- <div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openEmploymentSummary()">\n\n        <fa-icon name="address-card"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Dependent  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >Visa</h6>\n\n</div> -->\n\n\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openRelationinMbc()">\n\n        <fa-icon name="street-view"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Relations  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >In MBC</h6>\n\n</div>\n\n\n\n\n\n<div *ngIf="gulfIcons">\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openEmployeeVisa()">\n\n        <fa-icon name="globe"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Employee  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >Visa</h6>\n\n</div>\n\n\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openLaborCard()">\n\n        <fa-icon name="vcard"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Labor  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >Card</h6>\n\n</div>\n\n\n\n\n\n\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openNationalCard()">\n\n        <fa-icon name="id-badge"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >National  </h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >ID</h6>\n\n</div>\n\n\n\n<div class="mnb"   >\n\n    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openDependentTicket()">\n\n        <fa-icon name="ticket"  class="aw-ico1"></fa-icon>\n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt " >Dependant</h6>\n\n    <h6 ion-text color="dark" class="btn-txt " >Tickets</h6>\n\n</div>\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\profile-home\profile-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ProfileHomePage);
    return ProfileHomePage;
}());

//# sourceMappingURL=profile-home.js.map

/***/ }),

/***/ 553:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasicDataPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the BasicDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BasicDataPage = (function () {
    function BasicDataPage(navCtrl, navParams, ProfileProvider, AlertController, sanitized) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.sanitized = sanitized;
        this.getPerssonalInfo();
    }
    BasicDataPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BasicDataPage');
    };
    BasicDataPage.prototype.getPerssonalInfo = function () {
        var _this = this;
        try {
            this.ProfileProvider.getPerssonalInfo()
                .then(function (data) {
                // if (data)
                _this.personInfo = data;
                // this.xx=this.personInfo.date_of_birth;
                //console.log(this.personInfo)
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    BasicDataPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-basic-data',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\basic-data\basic-data.html"*/'<!--\n\n  Generated template for the BasicDataPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Personal Information</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <div *ngFor="let item of personInfo"  >\n\n        <ion-avatar item-start>   \n\n        <img [src]="sanitized.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+item.picture)" [hidden]="item.picture==null" class="img-p1"/>\n\n        </ion-avatar>\n\n        <div class="name-class">\n\n            {{item.full_name}}\n\n          </div>\n\n          <ion-item>\n\n              <div class="list-name">\n\n          \n\n                  Employee Number\n\n                <span class="list-value">\n\n                  {{item.employee_number}}\n\n                </span>\n\n              </div>\n\n            </ion-item>\n\n            <ion-item>\n\n                <div class="list-name">\n\n            \n\n                    Nationality\n\n                  <span class="list-value">\n\n                    {{item.nationality}}\n\n                  </span>\n\n                </div>\n\n              </ion-item>\n\n            <ion-item>\n\n                <div class="list-name">\n\n            \n\n                    National ID\n\n                  <span class="list-value">\n\n                    {{item.national_identifier}}\n\n                  </span>\n\n                </div>\n\n              </ion-item>\n\n\n\n\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Birthdate\n\n      <span class="list-value">\n\n        {{item.date_of_birth}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n\n\n\n\n\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Age\n\n      <span class="list-value">\n\n        {{item.age}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n      <div class="list-name">\n\n  \n\n          Hire Date\n\n        <span class="list-value">\n\n          {{item.hire_date}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n\n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Gender\n\n      <span class="list-value">\n\n        {{item.gender}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  \n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Religion\n\n      <span class="list-value">\n\n        {{item.religion}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n  \n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n        Marital Status\n\n      <span class="list-value">\n\n        {{item.marital_status}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n \n\n  <ion-item>\n\n    <div class="list-name">\n\n\n\n      Email\n\n      <span class="list-value">\n\n        {{item.email_address}}\n\n      </span>\n\n    </div>\n\n  </ion-item>\n\n\n\n\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\basic-data\basic-data.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */]])
    ], BasicDataPage);
    return BasicDataPage;
}());

//# sourceMappingURL=basic-data.js.map

/***/ }),

/***/ 554:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmploymentSummaryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the EmploymentSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
//declare var bot;
//declare var bot2;
var EmploymentSummaryPage = (function () {
    function EmploymentSummaryPage(navCtrl, navParams, ProfileProvider, AlertController) {
        // bot.mm();
        // alert(bot);
        //bot.tt();
        // let xx=bot();
        //bot();
        // bot2();
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.getEmplomentSummary();
    }
    EmploymentSummaryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EmploymentSummaryPage');
    };
    EmploymentSummaryPage.prototype.getEmplomentSummary = function () {
        var _this = this;
        try {
            this.ProfileProvider.getEmplomentSummary()
                .then(function (data) {
                // if (data)
                _this.empInfo = data;
                // this.xx=this.personInfo.date_of_birth;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    EmploymentSummaryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-employment-summary',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employment-summary\employment-summary.html"*/'<!--\n\n  Generated template for the EmploymentSummaryPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Employment History</ion-title>\n\n  </ion-navbar>\n\n\n\n  \n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n    <timeline endIcon="call">\n\n        <timeline-item *ngFor="let item of empInfo">\n\n          <!-- <timeline-time [time]="item.time"></timeline-time> -->\n\n          <ion-icon [name]="item.icon"></ion-icon>\n\n          <ion-card>\n\n            <!-- <ion-card-header>\n\n              \n\n            </ion-card-header> -->\n\n            <ion-card-content>\n\n\n\n                <div class=\'header1\'>{{item.assignment_start_date}} </div> \n\n              \n\n\n\n                <div class="list-name">\n\n          \n\n                    Job Title\n\n                  <span class="list-value">\n\n                    {{item.job_name}}\n\n                  </span>\n\n                </div>\n\n              \n\n                <div class="list-name">\n\n          \n\n                    Grade\n\n                  <span class="list-value">\n\n                    {{item.grade}}\n\n                  </span>\n\n                </div>\n\n\n\n\n\n\n\n                \n\n                <div class="list-name">\n\n          \n\n                    Department\n\n                  <span class="list-value">\n\n                    {{item.department}}\n\n                  </span>\n\n                </div>\n\n\n\n                <div class="list-name">\n\n          \n\n                    Manager\n\n                  <span class="list-value">\n\n                    {{item.manager}}\n\n                  </span>\n\n                </div>\n\n\n\n\n\n            </ion-card-content>\n\n          </ion-card>\n\n        </timeline-item>\n\n        </timeline>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employment-summary\employment-summary.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], EmploymentSummaryPage);
    return EmploymentSummaryPage;
}());

//# sourceMappingURL=employment-summary.js.map

/***/ }),

/***/ 555:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeePhonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the EmployeePhonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EmployeePhonePage = (function () {
    function EmployeePhonePage(navCtrl, navParams, ProfileProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.getPhonesData();
    }
    EmployeePhonePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EmployeePhonePage');
    };
    EmployeePhonePage.prototype.getPhonesData = function () {
        var _this = this;
        try {
            this.ProfileProvider.getEmplomenyeePhones()
                .then(function (data) {
                _this.phonesData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    EmployeePhonePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-employee-phone',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employee-phone\employee-phone.html"*/'<!--\n\n  Generated template for the EmployeePhonePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Employee Phone</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <div *ngFor="let item of phonesData" >\n\n\n\n        <ion-item>\n\n            <div class="list-name">\n\n        \n\n                {{item.phone_type}}\n\n              <span class="list-value">\n\n                {{item.phone_number}}\n\n              </span>\n\n            </div>\n\n          </ion-item>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employee-phone\employee-phone.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], EmployeePhonePage);
    return EmployeePhonePage;
}());

//# sourceMappingURL=employee-phone.js.map

/***/ }),

/***/ 556:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeeAdressessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the EmployeeAdressessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EmployeeAdressessPage = (function () {
    function EmployeeAdressessPage(navCtrl, navParams, ProfileProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.UAE = false;
        this.KSA = false;
        this.JORDON = false;
        this.EGYPT_Lebanon = false;
        if (__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup == '81')
            this.UAE = true;
        if (__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup == '998')
            this.KSA = true;
        if (__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup == '638')
            this.JORDON = true;
        if (__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup == '169' || __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup == '205')
            this.EGYPT_Lebanon = true;
        this.filterAddess();
    }
    EmployeeAdressessPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EmployeeAdressessPage');
    };
    EmployeeAdressessPage.prototype.filterAddess = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAdressData()];
                    case 1:
                        _a.sent();
                        if (this.adressData) {
                            //this.primaryAdrress=this.adressData;
                            this.primaryAdrress = this.adressData.filter(function (item) {
                                return (item.primary_address == 'Yes');
                            });
                            this.nonPrimaryAdrress = this.adressData.filter(function (item) {
                                return (item.primary_address == 'No');
                            });
                            console.log(this.nonPrimaryAdrress);
                            if (this.primaryAdrress.length == 0)
                                this.primaryAdrress = null;
                            if (this.nonPrimaryAdrress.length == 0)
                                this.nonPrimaryAdrress = null;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EmployeeAdressessPage.prototype.getAdressData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.ProfileProvider.getEmplomenyeeAddresses(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].businessGroup)
                                .then(function (data) {
                                _this.adressData = data;
                                // resolve(data)
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EmployeeAdressessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-employee-adressess',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employee-adressess\employee-adressess.html"*/'<!--\n\n  Generated template for the EmployeeAdressessPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Employee Address</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-item-divider color="light" *ngIf="primaryAdrress!=null"> Primary Address</ion-item-divider>\n\n  <div *ngFor="let item of primaryAdrress">\n\n    <!-- UAE  -->\n\n    <div *ngIf="UAE">\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Country\n\n          <span class="list-value">\n\n            {{item.country}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Emirate\n\n          <span class="list-value">\n\n            {{item.emirate}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Address Line1\n\n          <span class="list-value">\n\n            {{item.address_line1}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Address Line2\n\n          <span class="list-value">\n\n            {{item.address_line2}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Street\n\n          <span class="list-value">\n\n            {{item.street}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n     \n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          PO Box\n\n          <span class="list-value">\n\n            {{item.po_box}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Building\n\n          <span class="list-value">\n\n            {{item.building}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n    </div>\n\n     <!-- End UAE  -->\n\n <!-- KSA  -->\n\n <div *ngIf="KSA">\n\n    <ion-item>\n\n      <div class="list-name">\n\n\n\n        Country\n\n        <span class="list-value">\n\n          {{item.country}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item>\n\n      <div class="list-name">\n\n\n\n          Area\n\n        <span class="list-value">\n\n          {{item.area}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item>\n\n      <div class="list-name">\n\n\n\n        Address Line1\n\n        <span class="list-value">\n\n          {{item.address_line1}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item>\n\n      <div class="list-name">\n\n\n\n        Address Line2\n\n        <span class="list-value">\n\n          {{item.address_line2}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <div class="list-name">\n\n  \n\n          City\n\n          <span class="list-value">\n\n            {{item.city}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n    <ion-item>\n\n      <div class="list-name">\n\n\n\n        Street\n\n        <span class="list-value">\n\n          {{item.street}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n\n\n\n\n\n\n    <ion-item>\n\n      <div class="list-name">\n\n\n\n        PO Box\n\n        <span class="list-value">\n\n          {{item.po_box}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <div class="list-name">\n\n\n\n          Postal Code\n\n        <span class="list-value">\n\n          {{item.postal_code}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n  </div>\n\n\n\n   <!-- Egypt & Lebabon  -->\n\n   <div *ngIf="EGYPT_Lebanon">\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Country\n\n          <span class="list-value">\n\n            {{item.country}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            City/Town\n\n          <span class="list-value">\n\n            {{item.town_or_city}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Address Line1\n\n          <span class="list-value">\n\n            {{item.address_line1}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          Address Line2\n\n          <span class="list-value">\n\n            {{item.address_line2}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Address Line3\n\n            <span class="list-value">\n\n              {{item.address_line3}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            Region 1\n\n          <span class="list-value">\n\n            {{item.region_1}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n          <div class="list-name">\n\n  \n\n              Region 2\n\n            <span class="list-value">\n\n              {{item.region_2}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <div class="list-name">\n\n    \n\n                Region 3\n\n              <span class="list-value">\n\n                {{item.region_3}}\n\n              </span>\n\n            </div>\n\n          </ion-item>\n\n\n\n     \n\n\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            \n\n          Postal Code\n\n          <span class="list-value">\n\n            {{item.postal_code}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            Telephone Number 1\n\n          <span class="list-value">\n\n            {{item.telephone_number_1}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n          <div class="list-name">\n\n  \n\n              Telephone Number 2\n\n            <span class="list-value">\n\n              {{item.telephone_number_2}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n\n\n    </div>\n\n   <!-- END Egypt & Lebabon  -->\n\n\n\n   <!-- Jordan -->\n\n   <div *ngIf="JORDON">\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            Governorate\n\n          <span class="list-value">\n\n            {{item.Governorate}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            City English\n\n          <span class="list-value">\n\n            {{item.City_English}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            City Arabic\n\n          <span class="list-value">\n\n            {{item.City_arabic}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            Street English\n\n          <span class="list-value">\n\n            {{item.Street_English}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            Street Arabic\n\n          <span class="list-value">\n\n            {{item.Street_Arabic}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n     \n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n          PO Box\n\n          <span class="list-value">\n\n            {{item.po_box}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n\n\n            phone\n\n          <span class="list-value">\n\n            {{item.phone}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n    </div>\n\n     <!-- End Jordan  -->\n\n\n\n\n\n  </div>\n\n  \n\n  <div *ngFor="let item of nonPrimaryAdrress;  let i = index">\n\n      <ion-item-divider color="light" > Other Address ({{i+1}})</ion-item-divider>\n\n          <!-- UAE  -->\n\n    <div *ngIf="UAE">\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Country\n\n            <span class="list-value">\n\n              {{item.country}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Emirate\n\n            <span class="list-value">\n\n              {{item.emirate}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Address Line1\n\n            <span class="list-value">\n\n              {{item.address_line1}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Address Line2\n\n            <span class="list-value">\n\n              {{item.address_line2}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n  \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Street\n\n            <span class="list-value">\n\n              {{item.street}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n       \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            PO Box\n\n            <span class="list-value">\n\n              {{item.po_box}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Building\n\n            <span class="list-value">\n\n              {{item.building}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n      </div>\n\n       <!-- End UAE  -->\n\n   <!-- KSA  -->\n\n   <div *ngIf="KSA">\n\n      <ion-item>\n\n        <div class="list-name">\n\n  \n\n          Country\n\n          <span class="list-value">\n\n            {{item.country}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="list-name">\n\n  \n\n            Area\n\n          <span class="list-value">\n\n            {{item.area}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n      <ion-item>\n\n        <div class="list-name">\n\n  \n\n          Address Line1\n\n          <span class="list-value">\n\n            {{item.address_line1}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n  \n\n  \n\n      <ion-item>\n\n        <div class="list-name">\n\n  \n\n          Address Line2\n\n          <span class="list-value">\n\n            {{item.address_line2}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n          <div class="list-name">\n\n    \n\n            City\n\n            <span class="list-value">\n\n              {{item.city}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n      <ion-item>\n\n        <div class="list-name">\n\n  \n\n          Street\n\n          <span class="list-value">\n\n            {{item.street}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n  \n\n  \n\n  \n\n  \n\n      <ion-item>\n\n        <div class="list-name">\n\n  \n\n          PO Box\n\n          <span class="list-value">\n\n            {{item.po_box}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <div class="list-name">\n\n  \n\n            Postal Code\n\n          <span class="list-value">\n\n            {{item.postal_code}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n  \n\n    </div>\n\n  \n\n     <!-- Egypt & Lebabon  -->\n\n     <div *ngIf="EGYPT_Lebanon">\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Country\n\n            <span class="list-value">\n\n              {{item.country}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              City/Town\n\n            <span class="list-value">\n\n              {{item.town_or_city}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Address Line1\n\n            <span class="list-value">\n\n              {{item.address_line1}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            Address Line2\n\n            <span class="list-value">\n\n              {{item.address_line2}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n        <ion-item>\n\n            <div class="list-name">\n\n    \n\n              Address Line3\n\n              <span class="list-value">\n\n                {{item.address_line3}}\n\n              </span>\n\n            </div>\n\n          </ion-item>\n\n  \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              Region 1\n\n            <span class="list-value">\n\n              {{item.region_1}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n        <ion-item>\n\n            <div class="list-name">\n\n    \n\n                Region 2\n\n              <span class="list-value">\n\n                {{item.region_2}}\n\n              </span>\n\n            </div>\n\n          </ion-item>\n\n  \n\n          <ion-item>\n\n              <div class="list-name">\n\n      \n\n                  Region 3\n\n                <span class="list-value">\n\n                  {{item.region_3}}\n\n                </span>\n\n              </div>\n\n            </ion-item>\n\n  \n\n       \n\n  \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              \n\n            Postal Code\n\n            <span class="list-value">\n\n              {{item.postal_code}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              Telephone Number 1\n\n            <span class="list-value">\n\n              {{item.telephone_number_1}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n        <ion-item>\n\n            <div class="list-name">\n\n    \n\n                Telephone Number 2\n\n              <span class="list-value">\n\n                {{item.telephone_number_2}}\n\n              </span>\n\n            </div>\n\n          </ion-item>\n\n  \n\n      </div>\n\n     <!-- END Egypt & Lebabon  -->\n\n  \n\n     <!-- Jordan -->\n\n     <div *ngIf="JORDON">\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              Governorate\n\n            <span class="list-value">\n\n              {{item.Governorate}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              City English\n\n            <span class="list-value">\n\n              {{item.City_English}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              City Arabic\n\n            <span class="list-value">\n\n              {{item.City_arabic}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              Street English\n\n            <span class="list-value">\n\n              {{item.Street_English}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n  \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              Street Arabic\n\n            <span class="list-value">\n\n              {{item.Street_Arabic}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n       \n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n            PO Box\n\n            <span class="list-value">\n\n              {{item.po_box}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n        <ion-item>\n\n          <div class="list-name">\n\n  \n\n              phone\n\n            <span class="list-value">\n\n              {{item.phone}}\n\n            </span>\n\n          </div>\n\n        </ion-item>\n\n  \n\n      </div>\n\n       <!-- End Jordan  -->\n\n  \n\n    \n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employee-adressess\employee-adressess.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], EmployeeAdressessPage);
    return EmployeeAdressessPage;
}());

//# sourceMappingURL=employee-adressess.js.map

/***/ }),

/***/ 557:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeePassportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the EmployeePassportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EmployeePassportPage = (function () {
    function EmployeePassportPage(navCtrl, navParams, ProfileProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.getPassportData();
    }
    EmployeePassportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EmployeePassportPage');
    };
    EmployeePassportPage.prototype.getPassportData = function () {
        var _this = this;
        try {
            this.ProfileProvider.getEmplomenyeePassportInfo()
                .then(function (data) {
                _this.passportData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    EmployeePassportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-employee-passport',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employee-passport\employee-passport.html"*/'<!--\n\n  Generated template for the EmployeePassportPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Employee Passport</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <timeline endIcon="call">\n\n    <timeline-item *ngFor="let item of passportData">\n\n\n\n      <ion-icon [name]="item.icon"></ion-icon>\n\n      <ion-card>\n\n\n\n        <ion-card-content>\n\n\n\n           \n\n          \n\n\n\n           \n\n          \n\n            <div class="list-name">\n\n      \n\n               Number\n\n              <span class="list-value">\n\n                {{item.passport_number}}\n\n              </span>\n\n            </div>\n\n\n\n            <div class="list-name">\n\n      \n\n              Name\n\n              <span class="list-value">\n\n                {{item.name_as_passport}}\n\n              </span>\n\n            </div>\n\n\n\n            <div class="list-name">\n\n      \n\n              Passport Type\n\n              <span class="list-value">\n\n                {{item.passport_type}}\n\n              </span>\n\n            </div>\n\n            \n\n\n\n            <div class="list-name">\n\n      \n\n              Country\n\n              <span class="list-value">\n\n                {{item.passport_country}}\n\n              </span>\n\n            </div>\n\n\n\n           \n\n            <div class="list-name">\n\n      \n\n              Issue Date\n\n              <span class="list-value">\n\n                {{item.issue_date}}\n\n              </span>\n\n            </div>\n\n\n\n            <div class="list-name">\n\n      \n\n              Expiry Date\n\n              <span class="list-value">\n\n                {{item.expiry_date}}\n\n              </span>\n\n            </div>\n\n\n\n\n\n        </ion-card-content>\n\n      </ion-card>\n\n    </timeline-item>\n\n    </timeline>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employee-passport\employee-passport.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], EmployeePassportPage);
    return EmployeePassportPage;
}());

//# sourceMappingURL=employee-passport.js.map

/***/ }),

/***/ 558:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeeVisaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the EmployeeVisaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EmployeeVisaPage = (function () {
    function EmployeeVisaPage(navCtrl, navParams, ProfileProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.getVisaData();
    }
    EmployeeVisaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EmployeeVisaPage');
    };
    EmployeeVisaPage.prototype.getVisaData = function () {
        var _this = this;
        try {
            this.ProfileProvider.getEmployeeVisa()
                .then(function (data) {
                _this.visaData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    EmployeeVisaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-employee-visa',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employee-visa\employee-visa.html"*/'<!--\n\n  Generated template for the EmployeeVisaPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Employee Visa</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <timeline endIcon="call">\n\n    <timeline-item *ngFor="let item of visaData">\n\n    \n\n      <ion-icon [name]="item.icon"></ion-icon>\n\n      <ion-card>\n\n\n\n        <ion-card-content>\n\n\n\n            <div class=\'header1\'>{{item.visa_number}} </div> \n\n    \n\n\n\n            <div class="list-name">\n\n      \n\n                 Issue Place\n\n              <span class="list-value">\n\n                {{item.visa_issue_place}}\n\n              </span>\n\n            </div>\n\n            <div class="list-name">\n\n      \n\n               Issue Date\n\n              <span class="list-value">\n\n                {{item.visa_issue_date}}\n\n              </span>\n\n            </div>\n\n\n\n            <div class="list-name">\n\n      \n\n               Expiry Date\n\n              <span class="list-value">\n\n                {{item.visa_expiry_date}}\n\n              </span>\n\n            </div>\n\n\n\n            \n\n            <div class="list-name">\n\n      \n\n               Type\n\n              <span class="list-value">\n\n                {{item.visa_type}}\n\n              </span>\n\n            </div>\n\n\n\n            <div class="list-name">\n\n      \n\n              Name\n\n              <span class="list-value">\n\n                {{item.full_name}}\n\n              </span>\n\n            </div>\n\n\n\n           \n\n\n\n        </ion-card-content>\n\n      </ion-card>\n\n    </timeline-item>\n\n    </timeline>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\employee-visa\employee-visa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], EmployeeVisaPage);
    return EmployeeVisaPage;
}());

//# sourceMappingURL=employee-visa.js.map

/***/ }),

/***/ 559:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DependentTicketsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DependentTicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DependentTicketsPage = (function () {
    function DependentTicketsPage(navCtrl, navParams, ProfileProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.getDependentTickets();
    }
    DependentTicketsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DependentTicketsPage');
    };
    DependentTicketsPage.prototype.getDependentTickets = function () {
        var _this = this;
        try {
            this.ProfileProvider.getDependentTickets()
                .then(function (data) {
                _this.TicketData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    DependentTicketsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dependent-tickets',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\dependent-tickets\dependent-tickets.html"*/'<!--\n\n  Generated template for the DependentTicketsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Dependent Tickets</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <div *ngFor="let item of TicketData" >\n\n\n\n      <ion-item-divider color="light" >   {{item.name}}</ion-item-divider>\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n    \n\n          Relationship\n\n          <span class="list-value">\n\n            {{item.relationship}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n    \n\n           Birthdate\n\n          <span class="list-value">\n\n            {{item.date_of_birth}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n\n\n\n\n      <ion-item>\n\n        <div class="list-name">\n\n    \n\n         Payment Start Date\n\n          <span class="list-value">\n\n            {{item.effective_payment_date}}\n\n          </span>\n\n        </div>\n\n      </ion-item>\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\dependent-tickets\dependent-tickets.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], DependentTicketsPage);
    return DependentTicketsPage;
}());

//# sourceMappingURL=dependent-tickets.js.map

/***/ }),

/***/ 560:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LaborCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LaborCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LaborCardPage = (function () {
    function LaborCardPage(navCtrl, navParams, ProfileProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.getEmpoyeeLaborCardInfo();
    }
    LaborCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LaborCardPage');
    };
    LaborCardPage.prototype.getEmpoyeeLaborCardInfo = function () {
        var _this = this;
        try {
            this.ProfileProvider.getEmpoyeeLaborCardInfo()
                .then(function (data) {
                _this.cardData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    LaborCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-labor-card',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\labor-card\labor-card.html"*/'<!--\n\n  Generated template for the LaborCardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Labor Card</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <timeline endIcon="call">\n\n    <timeline-item *ngFor="let item of cardData">\n\n    \n\n      <ion-icon [name]="item.icon"></ion-icon>\n\n      <ion-card>\n\n\n\n        <ion-card-content>\n\n\n\n          \n\n    \n\n\n\n            <div class="list-name">\n\n      \n\n                Card Number\n\n              <span class="list-value">\n\n                {{item.labor_card_number}}\n\n              </span>\n\n            </div>\n\n          \n\n            <div class="list-name">\n\n      \n\n              Issue Date\n\n              <span class="list-value">\n\n                {{item.labor_card_issue_date}}\n\n              </span>\n\n            </div>\n\n\n\n            \n\n            <div class="list-name">\n\n      \n\n              Expiry Date\n\n              <span class="list-value">\n\n                {{item.labor_card_expiry_date}}\n\n              </span>\n\n            </div>\n\n\n\n\n\n        </ion-card-content>\n\n      </ion-card>\n\n    </timeline-item>\n\n    </timeline>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\labor-card\labor-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], LaborCardPage);
    return LaborCardPage;
}());

//# sourceMappingURL=labor-card.js.map

/***/ }),

/***/ 561:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelationinMbcPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RelationinMbcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RelationinMbcPage = (function () {
    function RelationinMbcPage(navCtrl, navParams, ProfileProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.getEmpoyeeRelationsInMBC();
    }
    RelationinMbcPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RelationinMbcPage');
    };
    RelationinMbcPage.prototype.getEmpoyeeRelationsInMBC = function () {
        var _this = this;
        try {
            this.ProfileProvider.getEmpoyeeRelationsInMBC()
                .then(function (data) {
                _this.relationData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    RelationinMbcPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-relationin-mbc',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\relationin-mbc\relationin-mbc.html"*/'<!--\n\n  Generated template for the RelationinMbcPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Relations in MBC</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <div *ngFor="let item of relationData" >\n\n\n\n    <ion-item-divider color="light" >   {{item.name}}</ion-item-divider>\n\n\n\n    <ion-item>\n\n      <div class="list-name">\n\n  \n\n        Relationship\n\n        <span class="list-value">\n\n          {{item.relationship}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <div class="list-name">\n\n  \n\n        Directorate\n\n        <span class="list-value">\n\n          {{item.department_directorate}}\n\n        </span>\n\n      </div>\n\n    </ion-item>\n\n\n\n\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\relationin-mbc\relationin-mbc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], RelationinMbcPage);
    return RelationinMbcPage;
}());

//# sourceMappingURL=relationin-mbc.js.map

/***/ }),

/***/ 562:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NationalCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the NationalCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NationalCardPage = (function () {
    function NationalCardPage(navCtrl, navParams, ProfileProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProfileProvider = ProfileProvider;
        this.AlertController = AlertController;
        this.getEmpoyeegetNationalIdCard();
    }
    NationalCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NationalCardPage');
    };
    NationalCardPage.prototype.getEmpoyeegetNationalIdCard = function () {
        var _this = this;
        try {
            this.ProfileProvider.getEmpoyeegetNationalIdCard()
                .then(function (data) {
                _this.cardData = data;
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    NationalCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-national-card',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\national-card\national-card.html"*/'<!--\n\n  Generated template for the NationalCardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>National ID</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <timeline endIcon="call">\n\n    <timeline-item *ngFor="let item of cardData">\n\n    \n\n      <ion-icon [name]="item.icon"></ion-icon>\n\n      <ion-card>\n\n\n\n        <ion-card-content>\n\n\n\n          \n\n    \n\n\n\n            <div class="list-name">\n\n      \n\n                ID\n\n              <span class="list-value">\n\n                {{item.id_card_number}}\n\n              </span>\n\n            </div>\n\n          \n\n            <div class="list-name">\n\n      \n\n              Issue Date\n\n              <span class="list-value">\n\n                {{item.issue_date}}\n\n              </span>\n\n            </div>\n\n\n\n            \n\n            <div class="list-name">\n\n      \n\n              Expiry Date\n\n              <span class="list-value">\n\n                {{item.expiry_date}}\n\n              </span>\n\n            </div>\n\n\n\n\n\n        </ion-card-content>\n\n      </ion-card>\n\n    </timeline-item>\n\n    </timeline>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\profile\national-card\national-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_profile_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], NationalCardPage);
    return NationalCardPage;
}());

//# sourceMappingURL=national-card.js.map

/***/ }),

/***/ 563:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashBoardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dash_board_hr_hr_dashboard_hr_dashboard__ = __webpack_require__(564);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dash_board_scm_scm_dashboard_scm_dashboard__ = __webpack_require__(573);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DashBoardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashBoardPage = (function () {
    function DashBoardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DashBoardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashBoardPage');
    };
    DashBoardPage.prototype.openHrDashboard = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__dash_board_hr_hr_dashboard_hr_dashboard__["a" /* HrDashboardPage */]);
    };
    DashBoardPage.prototype.openSCMdashboard = function () {
        // this.navCtrl.push(ApprovedPoDbPage);
        //this.navCtrl.push(MyRequisitionDbPage);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__dash_board_scm_scm_dashboard_scm_dashboard__["a" /* ScmDashboardPage */]);
    };
    DashBoardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dash-board',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\dash-board\dash-board.html"*/'<!--\n\n  Generated template for the DashBoardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Dashboard</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <div class="mnb"  id="HRDashboard" >\n\n    <button ion-button  color="dark"    class="ico-fa-btn1"   (click)="openHrDashboard()">\n\n      <fa-icon name="calendar-check-o"  class="aw-ico1"></fa-icon>\n\n        \n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt" >HR</h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n  </div>\n\n  \n\n   \n\n<!--   \n\n  <div class="mnb"  id="Procurment" >\n\n      <button ion-button  color="dark"  class="ico-fa-btn1"  (click)="openSCMdashboard()">\n\n          <fa-icon name="address-card"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">Procurment</h6>\n\n      <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n      \n\n  </div>\n\n  \n\n  <div class="mnb"   id="Finicials" > \n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " >\n\n          <span class="aw-ico2">\n\n          <fa-icon name="calendar"  class="aw-ico1"></fa-icon>\n\n          </span>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Finicials</h6>\n\n      <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n   \n\n  </div> -->\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\dash-board\dash-board.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], DashBoardPage);
    return DashBoardPage;
}());

//# sourceMappingURL=dash-board.js.map

/***/ }),

/***/ 564:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HrDashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__leave_balance_db_leave_balance_db__ = __webpack_require__(565);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__bu_headcount_db_bu_headcount_db__ = __webpack_require__(566);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__absenc_calendar_db_absenc_calendar_db__ = __webpack_require__(567);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__leave_utilzation_db_leave_utilzation_db__ = __webpack_require__(568);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__over_time_db_over_time_db__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__recruitment_db_recruitment_db__ = __webpack_require__(570);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__over_time_db_executive_over_time_db_executive__ = __webpack_require__(571);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__extra_time_db_extra_time_db__ = __webpack_require__(572);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the HrDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HrDashboardPage = (function () {
    function HrDashboardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HrDashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HrDashboardPage');
    };
    HrDashboardPage.prototype.openleaveBalanceDB = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__leave_balance_db_leave_balance_db__["a" /* LeaveBalanceDbPage */]);
    };
    HrDashboardPage.prototype.openHeadCountDB = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__bu_headcount_db_bu_headcount_db__["a" /* BuHeadcountDbPage */]);
    };
    HrDashboardPage.prototype.openabsenceCalendar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__absenc_calendar_db_absenc_calendar_db__["a" /* AbsencCalendarDbPage */]);
    };
    HrDashboardPage.prototype.openLeaveUtilization = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__leave_utilzation_db_leave_utilzation_db__["a" /* LeaveUtilzationDbPage */]);
    };
    HrDashboardPage.prototype.openOverTime = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__over_time_db_over_time_db__["a" /* OverTimeDbPage */]);
    };
    HrDashboardPage.prototype.openRecruitment = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__recruitment_db_recruitment_db__["a" /* RecruitmentDbPage */]);
    };
    HrDashboardPage.prototype.openOverTimeExecutive = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__over_time_db_executive_over_time_db_executive__["a" /* OverTimeDbExecutivePage */]);
    };
    HrDashboardPage.prototype.openextraTimeWorked = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__extra_time_db_extra_time_db__["a" /* ExtraTimeDbPage */]);
    };
    HrDashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-hr-dashboard',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\hr-dashboard\hr-dashboard.html"*/'<!--\n\n  Generated template for the HrDashboardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>HR Dashboard</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n      \n\n  <div class="mnb"   id="HeadCount" > \n\n        <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openHeadCountDB()">\n\n            <span class="aw-ico2">\n\n            <fa-icon name="building"  class="aw-ico1"></fa-icon>\n\n           \n\n            </span>\n\n        </button>\n\n        <h6 ion-text color="dark" class="btn-txt " >Head</h6>\n\n        <h6 ion-text color="dark" class="btn-txt" >Count</h6>\n\n     \n\n    </div>\n\n\n\n        <div class="mnb"  id="OverTime-e" >\n\n                <button ion-button  color="dark"  class="ico-fa-btn1" (click)="openOverTimeExecutive()">\n\n                    <fa-icon name="address-card"  class="aw-ico1"></fa-icon>\n\n                </button>\n\n                <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">Over</h6>\n\n                <h6 ion-text color="dark" class="btn-txt" >Time</h6>\n\n                \n\n            </div>\n\n\n\n            <div class="mnb"   id="extraTimeWorked" > \n\n                    <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openextraTimeWorked()">\n\n                        <span class="aw-ico2">\n\n                        <fa-icon name="pie-chart"  class="aw-ico1"></fa-icon>\n\n                       \n\n                        </span>\n\n                    </button>\n\n                    <h6 ion-text color="dark" class="btn-txt " >ExtraTime</h6>\n\n                    <h6 ion-text color="dark" class="btn-txt" >Worked</h6>\n\n                 \n\n                </div>\n\n               \n\n\n\n\n\n\n\n<!-- \n\n  <div class="mnb"  id="Balance" >\n\n    <button ion-button  color="dark"    class="ico-fa-btn1"   (click)="openleaveBalanceDB()">\n\n      <fa-icon name="calendar-check-o"  class="aw-ico1"></fa-icon>\n\n        \n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt" >Leave</h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >Balance</h6>\n\n  </div>\n\n  \n\n   \n\n  \n\n  <div class="mnb"  id="OverTime" >\n\n      <button ion-button  color="dark"  class="ico-fa-btn1" (click)="openOverTime()">\n\n          <fa-icon name="address-card"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">OverTime</h6>\n\n      <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n      \n\n  </div>\n\n\n\n\n\n\n\n\n\n \n\n  <div class="mnb"   id="Calendar" > \n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openabsenceCalendar()">\n\n          <span class="aw-ico2">\n\n          <fa-icon name="calendar"  class="aw-ico1"></fa-icon>\n\n          </span>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Absence</h6>\n\n      <h6 ion-text color="dark" class="btn-txt" >Calendar</h6>\n\n   \n\n  </div>\n\n\n\n\n\n  <div class="mnb"   id="Utilization" > \n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openLeaveUtilization()">\n\n          <span class="aw-ico2">\n\n          <fa-icon name="pie-chart"  class="aw-ico1"></fa-icon>\n\n         \n\n          </span>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Leave</h6>\n\n      <h6 ion-text color="dark" class="btn-txt" >Utilization</h6>\n\n   \n\n  </div>\n\n\n\n\n\n\n\n  <div class="mnb"   id="Recruitment" > \n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="openRecruitment()">\n\n          <span class="aw-ico2">\n\n          <fa-icon name="file-text-o"  class="aw-ico1"></fa-icon>\n\n         \n\n          </span>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Recruitment</h6>\n\n      <h6 ion-text color="dark" class="btn-txt" >&nbsp;</h6>\n\n   \n\n  </div> -->\n\n\n\n  \n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\hr-dashboard\hr-dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], HrDashboardPage);
    return HrDashboardPage;
}());

//# sourceMappingURL=hr-dashboard.js.map

/***/ }),

/***/ 565:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaveBalanceDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_piecelabel_js__ = __webpack_require__(538);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_piecelabel_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_piecelabel_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the LeaveBalanceDbPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LeaveBalanceDbPage = (function () {
    function LeaveBalanceDbPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.seg = "Team";
        this.doughnutChartColors = [
            {
                backgroundColor: ["red", "#353D4e"],
                fontColor: ["gray", "#008AE6"]
            }
        ];
        this.doughnutChartType = 'horizontalBar'; //'doughnut';
        this.chartOptions = {
            scales: {
                xAxes: [{
                        stacked: true
                    }],
                yAxes: [{
                        stacked: true
                    }]
            }
        };
        this.doughnutChartOptions = {
            pieceLabel: {
                render: 'value',
                fontColor: ['white', 'white'],
                fontStyle: 'bold',
                fontSize: '12',
                // arc :true ,
                position: 'border ',
                legend: {
                    onClick: null
                }
            }
        };
        /*
        this.datax={
          labels: ['Standing costs', 'Running costs'], // responsible for how many bars are gonna show on the chart
        
          datasets: [{
             label: 'Washing and cleaning',
             data: [0, 8],
             backgroundColor: '#22aa99'
          }, {
             label: 'Traffic tickets',
             data: [0, 2],
             backgroundColor: '#994499'
          }, {
             label: 'Tolls',
             data: [0, 1],
             backgroundColor: '#316395'
          }, {
             label: 'Parking',
             data: [5, 2],
             backgroundColor: '#b82e2e'
          }, {
             label: 'Car tax',
             data: [0, 1],
             backgroundColor: '#66aa00'
          }, {
             label: 'Repairs and improvements',
             data: [0, 2],
             backgroundColor: '#dd4477'
          }, {
             label: 'Maintenance',
             data: [6, 1],
             backgroundColor: '#0099c6'
          }, {
             label: 'Inspection',
             data: [0, 2],
             backgroundColor: '#990099'
          }, {
             label: 'Loan interest',
             data: [0, 3],
             backgroundColor: '#109618'
          }, {
             label: 'Depreciation of the vehicle',
             data: [0, 2],
             backgroundColor: '#109618'
          }, {
             label: 'Fuel',
             data: [0, 1],
             backgroundColor: '#dc3912'
          }, {
             label: 'Insurance and Breakdown cover',
             data: [4, 0],
             backgroundColor: '#3366cc'
          }]
        };
        */
        this.doughnutChartvaluesAnuual = [];
        this.doughnutChartvaluesAnuual[0] = 12;
        this.doughnutChartvaluesAnuual[1] = 19;
        this.doughnutChartvaluesAnuual[2] = 15;
        this.doughnutChartLabelsAnuual = [];
        this.doughnutChartLabelsAnuual[0] = 'Emad';
        this.doughnutChartLabelsAnuual[1] = 'Rafik';
        this.doughnutChartLabelsAnuual[2] = 'Mahmoud';
        this.doughnutChartLabelsAnuual[3] = 'Jasmina';
        this.doughnutChartLabelsAnuual[4] = 'Rami';
        this.doughnutChartLabelsAnuual[5] = 'Heba';
        this.doughnutChartLabelsAnuual[6] = 'Amr';
        this.doughnutChartLabels = [];
        this.doughnutChartLabels[0] = 'Annual';
        this.doughnutChartLabels[1] = 'Sick';
        this.doughnutChartLabels[2] = 'Toil';
        this.datasets = [];
        // this.datasets[0] =xx; 
        this.datasets = [
            {
                label: 'Annual Leave',
                data: [17, 12, 21, 14, 5, 17, 18],
                backgroundColor: '#FF0000'
            },
            {
                label: 'Sick Leave',
                data: [5, 12, 1, 14, 15, 7, 10],
                backgroundColor: '#FAEBCC'
            },
            {
                label: 'Toil',
                data: [3, 19, 4, 14, 25, 17, 18],
                backgroundColor: '#FF0000'
            }
        ];
    }
    LeaveBalanceDbPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LeaveBalanceDbPage');
        // google.charts.setOnLoadCallback(this.drawChart2());
        // setTimeout(() => {
        //   google.charts.setOnLoadCallback(this.drawChart());
        // }, 3000);
        this.segmentChanged(null);
    };
    LeaveBalanceDbPage.prototype.segmentChanged = function (event) {
        var _this = this;
        if (event && event._value == 'Me') {
            console.log(event._value);
            setTimeout(function () {
                google.charts.setOnLoadCallback(_this.drawChart());
            }, 10);
        }
        else 
        //if (event._value=='Team')
        {
            setTimeout(function () {
                google.charts.setOnLoadCallback(_this.drawChart2());
            }, 10);
        }
        //google.charts.setOnLoadCallback(this.drawChart());
        //   setTimeout(() => {
        //     google.charts.setOnLoadCallback(this.drawChart());
        //   }, 1);
    };
    LeaveBalanceDbPage.prototype.drawChart = function () {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
            ['Mushrooms', 3],
            ['Onions', 1],
            ['Olives', 1],
            ['Zucchini', 1],
            ['Pepperoni', 5]
        ]);
        // Set chart options
        var options = { 'title': 'How Much Pizza I Ate Last Night',
            // 'width':400,
            // 'height':300,
            animation: {
                duration: 700,
                startup: true
            }
        };
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('bar_div'));
        //  var chart = new google.visualization.PieChart(this.doughnutCanvas.nativeElement);
        chart.draw(data, options);
    };
    LeaveBalanceDbPage.prototype.drawChart2 = function () {
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
            ['Genre', 'Annual', 'Toil', 'Sick', { role: 'annotation' }],
            ['2020', 10, 24, 20, 'Emad'],
            ['3030', 16, 22, 23, 'rafik'],
            ['4040', 28, 19, 29, 'azmi']
        ]);
        var options = {
            // width: '100%',
            // height: 400,
            animation: {
                duration: 1000,
                startup: true
            },
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '85%' },
            isStacked: true
        };
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('bar_div2'));
        //  var chart = new google.visualization.PieChart(this.doughnutCanvas.nativeElement);
        chart.draw(data, options);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LeaveBalanceDbPage.prototype, "doughnutCanvas", void 0);
    LeaveBalanceDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave-balance-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\leave-balance-db\leave-balance-db.html"*/'<!--\n\n  Generated template for the LeaveBalanceDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Leave Balance</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    \n\n    <ion-segment [(ngModel)]="seg" (ionChange)="segmentChanged($event)">\n\n        <ion-segment-button value="Me">\n\n          Me\n\n        </ion-segment-button>\n\n        <ion-segment-button value="Team" >\n\n          Team\n\n        </ion-segment-button>\n\n    \n\n      </ion-segment>\n\n      <div [ngSwitch]="seg">\n\n          <ion-list *ngSwitchCase="\'Me\'">\n\n              <div id="bar_div"></div>\n\n  <div  style="display: block">\n\n      <canvas style="height:60vh; width:80vw" baseChart\n\n                \n\n                  \n\n     \n\n                  [colors]="doughnutChartColors"\n\n                  [data]="doughnutChartvaluesAnuual"\n\n                  [labels]="doughnutChartLabels"\n\n                  [chartType]="\'doughnut\'"\n\n                  [options]="doughnutChartOptions"\n\n                  >\n\n                  (chartHover)="chartHovered($event)"\n\n                  (chartClick)="chartClicked($event)"\n\n                \n\n                </canvas>\n\n  \n\n    </div>   \n\n</ion-list>\n\n\n\n \n\n    <ion-list *ngSwitchCase="\'Team\'">\n\n\n\n        <!-- <div #doughnutCanvas></div> -->\n\n        <div id="bar_div2"></div>\n\n  <div  style="display: block">\n\n      <canvas style="height:60vh; width:80vw" baseChart\n\n                \n\n                  \n\n      [datasets]="datasets"\n\n      [labels]="doughnutChartLabelsAnuual"\n\n                  [chartType]="doughnutChartType"\n\n                  [options]="chartOptions"\n\n                \n\n                  >\n\n                 \n\n                \n\n                </canvas>\n\n  \n\n    </div> \n\n  \n\n  </ion-list>\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\leave-balance-db\leave-balance-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], LeaveBalanceDbPage);
    return LeaveBalanceDbPage;
}());

//# sourceMappingURL=leave-balance-db.js.map

/***/ }),

/***/ 566:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuHeadcountDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var BuHeadcountDbPage = (function () {
    function BuHeadcountDbPage(navCtrl, navParams, DashbaordProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DashbaordProvider = DashbaordProvider;
        this.AlertController = AlertController;
        this.colors = ['rgb(51, 102, 204)', 'rgb(220, 57, 18)', 'rgb(255, 153, 0)', 'rgb(16, 150, 24)', 'rgb(153, 0, 153)', 'rgb(0, 153, 198)'];
        this.seg = 'P';
        this.getEmployeeCountBUDirectorate();
        //this.drawhHadcountGender();
        //this.getEmployeeHeadcountGender();
        this.allDictorateData = null;
    }
    BuHeadcountDbPage.prototype.ionViewDidLoad = function () {
        //google.charts.setOnLoadCallback(this.drawChart());
        //google.charts.setOnLoadCallback(this.drawBasic());
        //this.drawhHadcountGender();
    };
    BuHeadcountDbPage.prototype.getEmployeeHeadcountGender = function () {
        var _this = this;
        try {
            this.DashbaordProvider.getEmployeeHeadcountGender()
                .then(function (data) {
                _this.BuGenderHeadCount = data;
                _this.drawhHadcountGender(data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    BuHeadcountDbPage.prototype.getEmployeeCountBUDirectorate = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.DashbaordProvider.getEmployeeCountBUDirectorate()
                        .then(function (data) {
                        _this.BuHeadCount = data;
                        _this.drawChart(data);
                    });
                }
                catch (error) {
                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
                }
                return [2 /*return*/];
            });
        });
    };
    BuHeadcountDbPage.prototype.getEmployeeCountPersonTypeDirectorate = function (val, label, businessGroup) {
        var _this = this;
        try {
            this.DashbaordProvider.getEmployeeCountPersonTypeDirectorate(businessGroup)
                .then(function (data) {
                //this.allDictorateData.
                _this.drawBasic(val, label, data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    BuHeadcountDbPage.prototype.drawBGpersonType = function (val, label, businessGroup) {
        this.getEmployeeCountPersonTypeDirectorate(val, label, businessGroup);
    };
    BuHeadcountDbPage.prototype.drawChart = function (buData) {
        if (!buData)
            return;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'BU');
        data.addColumn('number', 'headCount');
        data.addColumn('number', 'id');
        data.addColumn('number', 'BuId');
        /*data.addRows([
          ['DXB', 1264 ,1],
          ['EG', 401 ,2],
          ['KSA', 600,3],
          ['Jor', 324,4],
          ['LEB',136,5]
        ]);*/
        for (var i = 0; i < buData.length; i++) {
            data.addRows([[buData[i].business_groups, buData[i].employee_count, i, buData[i].business_group_id]]);
        }
        var options = {
            //'title':'Head Count',
            pieHole: 0.4,
            // is3D:true,
            legend: {
                position: 'top',
                maxLines: 2
            },
            colors: this.colors,
            // pieSliceText: 'value-and-percentage',
            slices: { 1: { offset: 0 },
                2: { offset: 0.02 },
                3: { offset: 0.01 },
                4: { offset: 0.02 },
            },
        };
        var t = this;
        var chart = new google.visualization.PieChart(document.getElementById('pie_div_hc'));
        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                //var value = data.getValue(selectedItem.row, selectedItem.column);
                var value = data.getValue(selectedItem.row, 2);
                var label = data.getValue(selectedItem.row, 0);
                var buId = data.getValue(selectedItem.row, 3);
                // alert('The user selected ' + value);
                // t.drawBasic(value , label );
                t.drawBGpersonType(value, label, buId + '');
            }
        }
        google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data, options);
    };
    BuHeadcountDbPage.prototype.drawBasic = function (val, label, BUpersonTypeData) {
        // var data = google.visualization.arrayToDataTable([
        //   ['Diectorate', 'Head count'],
        //   ['Arabya', 300],
        //   ['HR', 240],
        //   ['Production', 270],
        //   ['Operations', 201]
        // ]);
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'person');
        data.addColumn('number', 'headCount');
        // data.addRows([
        //   ['Arabya', 300 ],
        //   ['HR', 240 ],
        //   ['Production', 270],
        //   ['Operations', 201]
        // ]);
        var barHeight = 0;
        for (var i = 0; i < BUpersonTypeData.length; i++) {
            data.addRows([[BUpersonTypeData[i].person_type, BUpersonTypeData[i].employee_count]]);
            barHeight = barHeight + 50;
        }
        var color = [];
        try {
            color[0] = this.colors[val];
        }
        catch (err) {
            color[0] = 'red';
        }
        var options = {
            title: label + ' Person Type',
            height: 250,
            legend: 'none',
            chartArea: { width: '65%' },
            bar: { groupWidth: "55%" },
            colors: color,
            hAxis: {
                //  title: 'Total Population',
                minValue: 0
            },
            vAxis: {
                // title: 'City'
                textStyle: { fontSize: 10 }
            },
            animation: {
                duration: 500,
                startup: true
            }
        };
        var chart = new google.visualization.BarChart(document.getElementById('bar_div_hc'));
        chart.draw(data, options);
    };
    BuHeadcountDbPage.prototype.drawhHadcountGender = function (genderData) {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'BG');
        data.addColumn('number', 'Male');
        data.addColumn('number', 'Female');
        for (var i = 0; i < genderData.length; i++)
            data.addRows([[genderData[i].business_groups, genderData[i].male, genderData[i].female]]);
        var options = {
            //  'width':400,
            'height': 300,
            colors: ['#6633CC', '#DD4477'],
            animation: {
                duration: 1000,
                startup: true
            },
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '85%' },
            isStacked: true
        };
        var chart = new google.visualization.BarChart(document.getElementById('bar_head_count_gender'));
        chart.draw(data, options);
    };
    BuHeadcountDbPage.prototype.segmentChanged = function () {
        if (this.seg == 'P') {
            this.getEmployeeCountBUDirectorate();
        }
        else {
            this.getEmployeeHeadcountGender();
        }
    };
    BuHeadcountDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-bu-headcount-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\bu-headcount-db\bu-headcount-db.html"*/'<!--\n\n  Generated template for the BuHeadcountDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Head Count</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n\n\n    <!-- <div id="pie_div_hc" class="chart1"></div>\n\n\n\n    <div id="bar_div_hc" class="chart2" ></div> -->\n\n    \n\n      \n\n \n\n        <ion-segment [(ngModel)]="seg" (ionChange)="segmentChanged($event)">\n\n            <ion-segment-button value="P">\n\n              Person Type\n\n            </ion-segment-button>\n\n            <ion-segment-button value="G" >\n\n                Gender\n\n            </ion-segment-button>\n\n        \n\n          </ion-segment>\n\n\n\n       \n\n\n\n          <div [ngSwitch]="seg">\n\n              <ion-list *ngSwitchCase="\'P\'">\n\n                  <div id="pie_div_hc" class="chart1"></div>\n\n                  <div id="bar_div_hc"  class="chart"> </div>\n\n    </ion-list> \n\n    \n\n     \n\n        <ion-list *ngSwitchCase="\'G\'">\n\n    \n\n        \n\n            <div id="bar_head_count_gender"  ></div>\n\n     \n\n      \n\n      </ion-list>\n\n      </div>\n\n      \n\n      <div id="series_chart_div"  class="chart"> </div>\n\n    <div  style="display: block">\n\n        <canvas style="height:60vh; width:80vw;" baseChart\n\n        [data]="doughnutChartvaluesAnuual"\n\n        [labels]="doughnutChartLabels"\n\n      \n\n                    [chartType]="\'line\'"\n\n                    [colors]="doughnutChartColors"\n\n                    [options]="doughnutChartOptions"\n\n                     \n\n        >\n\n                    (chartHover)="chartHovered($event)"\n\n                    (chartClick)="chartClicked($event)"\n\n                  \n\n                  </canvas>\n\n    \n\n      </div> \n\n      \n\n      <!-- [colors]="doughnutChartColors"\n\n         [datasets]="datasets"\n\n      [data]="doughnutChartvaluesAnuual"\n\n       [labels]="doughnutChartLabels"\n\n         [datasets]="datasets"   \n\n     -->\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\bu-headcount-db\bu-headcount-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__["a" /* DashbaordProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], BuHeadcountDbPage);
    return BuHeadcountDbPage;
}());

//# sourceMappingURL=bu-headcount-db.js.map

/***/ }),

/***/ 567:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AbsencCalendarDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AbsencCalendarDbPage = (function () {
    function AbsencCalendarDbPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AbsencCalendarDbPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AbsencCalendarDbPage');
        google.charts.setOnLoadCallback(this.drawChart());
    };
    AbsencCalendarDbPage.prototype.drawChart = function () {
        var container = document.getElementById('timeline');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();
        var options = {
            width: 1000,
            //'height':600,
            legend: { position: 'none' },
            animation: {
                duration: 1500,
                startup: true
            },
            //title: 'MBC HeadCount',
            hAxis: { title: 'Head Count' },
            vAxis: { title: 'Ranking' },
            bubble: { textStyle: { fontSize: 11 } }
        };
        dataTable.addColumn({ type: 'string', id: 'President' });
        dataTable.addColumn({ type: 'string', id: 'name' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addRows([
            ['Emad', 'Emad', new Date(2019, 0, 1), new Date(2019, 0, 4)],
            ['Emad', 'Emad', new Date(2019, 2, 21), new Date(2019, 2, 23)],
            ['Rami', 'Rami', new Date(2019, 1, 5), new Date(2019, 1, 7)],
            ['Rafik', 'Rafik', new Date(2019, 1, 7), new Date(2019, 1, 10)],
            ['Azmi', 'Azmi', new Date(2019, 0, 31), new Date(2019, 1, 5)],
            ['Amr', 'Amr', new Date(2019, 2, 10), new Date(2019, 2, 15)],
            ['Heba', 'Heba', new Date(2019, 1, 1), new Date(2019, 2, 20)],
            ['Jasmina', 'Jasmina', new Date(2019, 2, 15), new Date(2019, 2, 30)],
            ['Mahmoud', 'Mahmoud', new Date(2019, 0, 1), new Date(2019, 1, 4)]
        ]);
        chart.draw(dataTable, options);
    };
    AbsencCalendarDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-absenc-calendar-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\absenc-calendar-db\absenc-calendar-db.html"*/'<!--\n\n  Generated template for the AbsencCalendarDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n   \n\n  <ion-navbar>\n\n    <ion-title>Absence Calendar</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n \n\n<ion-content>\n\n<div id="timeline"  class="chart"> </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\absenc-calendar-db\absenc-calendar-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], AbsencCalendarDbPage);
    return AbsencCalendarDbPage;
}());

//# sourceMappingURL=absenc-calendar-db.js.map

/***/ }),

/***/ 568:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaveUtilzationDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LeaveUtilzationDbPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LeaveUtilzationDbPage = (function () {
    function LeaveUtilzationDbPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.allClass = 'marker-selected';
        this.DXBclass = '';
    }
    LeaveUtilzationDbPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LeaveUtilzationDbPage');
    };
    LeaveUtilzationDbPage.prototype.onAllClicked = function () {
        this.allClass = 'marker-selected';
        this.DXBclass = '';
    };
    LeaveUtilzationDbPage.prototype.onHrClicked = function () {
        this.allClass = '';
        this.DXBclass = 'marker-selected';
    };
    LeaveUtilzationDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave-utilzation-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\leave-utilzation-db\leave-utilzation-db.html"*/'<!--\n\n  Generated template for the LeaveUtilzationDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <!-- <ion-title>Leave Utilzation</ion-title> -->\n\n\n\n    <ion-title>Payment</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <div id="timeline-wrap">\n\n        <div id="timeline"></div>\n\n        \n\n        <!-- This is the individual marker-->\n\n        <div class="marker mfirst timeline-icon one" [ngClass]="allClass">\n\n            <!-- <i class="fa fa-pencil"></i> -->\n\n            <button ion-button icon-only outline  color=\'light\' class=\'tm-btn\'  (click)="onAllClicked()">\n\n            <i   class="time-line-in-txt"> All</i>\n\n            </button>\n\n        </div>\n\n        <!-- / marker -->\n\n      \n\n        <!-- This is the individual marker-->\n\n        <div class="marker m2 timeline-icon two" [ngClass]="DXBclass">\n\n            <button ion-button icon-only outline  color=\'light\' class=\'tm-btn\' (click)="onHrClicked()">\n\n            <i   class="time-line-in-txt"> DXB</i>\n\n            </button>\n\n        </div>\n\n        <!-- / marker -->\n\n      \n\n          <!-- This is the individual panel-->\n\n        <!-- <div class="timeline-panel">\n\n          <p>text</p>\n\n        </div> -->\n\n        <!-- / panel -->\n\n        \n\n        <!-- This is the individual marker-->\n\n        <div class="marker m3 timeline-icon three">\n\n            <i   class="time-line-in-txt"> KSA</i>\n\n        </div>\n\n        <!-- / marker -->\n\n      \n\n        \n\n        <!-- This is the individual marker-->\n\n        <div class="marker m4 timeline-icon four">\n\n            <i   class="time-line-in-txt"> LEB</i>\n\n        </div>\n\n        <!-- / marker -->\n\n\n\n        <!-- This is the individual marker-->\n\n        <div class="marker m5 timeline-icon four">\n\n            <i   class="time-line-in-txt"> EG</i>\n\n          </div>\n\n          <!-- / marker -->\n\n\n\n          <!-- This is the individual marker-->\n\n        <div class="marker mlast timeline-icon four">\n\n            <i   class="time-line-in-txt"> Jo</i>\n\n          </div>\n\n          <!-- / marker -->\n\n      \n\n      \n\n       \n\n      </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\leave-utilzation-db\leave-utilzation-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], LeaveUtilzationDbPage);
    return LeaveUtilzationDbPage;
}());

//# sourceMappingURL=leave-utilzation-db.js.map

/***/ }),

/***/ 569:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverTimeDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OverTimeDbPage = (function () {
    function OverTimeDbPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.seg = "Team";
        this.isvisted = false;
    }
    OverTimeDbPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OverTimeDbPage');
        this.segmentChanged(null);
    };
    OverTimeDbPage.prototype.segmentChanged = function (event) {
        //if (!this.isvisted)
        //{
        var _this = this;
        if (event && event._value == 'Me') {
            console.log(event._value);
            this.isvisted = true;
            setTimeout(function () {
                google.charts.setOnLoadCallback(_this.drawChart());
            }, 100);
        }
        else {
            setTimeout(function () {
                google.charts.setOnLoadCallback(_this.drawChart2());
            }, 10);
        }
        //}
    };
    OverTimeDbPage.prototype.drawChart = function () {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
            ['Mushrooms', 3],
            ['Onions', 1],
            ['Olives', 1],
            ['Zucchini', 1],
            ['Pepperoni', 5]
        ]);
        var options = { 'title': 'How Much Pizza I Ate Last Night',
            //   'width':400,
            // 'height':300,
            //    animation: {
            //     duration: 700,
            //     startup: true
            // }
            legend: 'none',
            pieSliceText: 'label',
            slices: { 1: { offset: 0 },
                2: { offset: 0.02 },
                3: { offset: 0.01 },
                4: { offset: 0.02 },
            },
        };
        var t = this;
        var chart = new google.visualization.PieChart(document.getElementById('pie_div_o'));
        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                console.log(selectedItem);
                //var value = data.getValue(selectedItem.row, selectedItem.column);
                var value = data.getValue(selectedItem.row, 0);
                alert('The user selected ' + value);
                t.testxx();
            }
        }
        google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data, options);
    };
    OverTimeDbPage.prototype.drawChart2 = function () {
        var data = google.visualization.arrayToDataTable([
            ['Genre', 'Annual', 'Toil', 'Sick', { role: 'annotation' }],
            ['2020', 10, 24, 20, 'Emad'],
            ['3030', 16, 22, 23, 'rafik'],
            ['4040', 28, 19, 29, 'azmi']
        ]);
        var options = {
            //  'width':400,
            // 'height':300,
            animation: {
                duration: 1000,
                startup: true
            },
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '85%' },
            isStacked: true
        };
        var chart = new google.visualization.BarChart(document.getElementById('bar_div_o'));
        chart.draw(data, options);
    };
    OverTimeDbPage.prototype.testxx = function () {
        // this.navCtrl.push(HrDashboardPage);
        alert('x');
    };
    OverTimeDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-over-time-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\over-time-db\over-time-db.html"*/'<!--\n\n  Generated template for the OverTimeDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Overtime</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-segment [(ngModel)]="seg" (ionChange)="segmentChanged($event)">\n\n        <ion-segment-button value="Me">\n\n          Me\n\n        </ion-segment-button>\n\n        <ion-segment-button value="Team" >\n\n          Team\n\n        </ion-segment-button>\n\n    \n\n      </ion-segment>\n\n      <div [ngSwitch]="seg">\n\n          <ion-list *ngSwitchCase="\'Me\'">\n\n              <div id="pie_div_o"   class="ttx1"></div>\n\n \n\n</ion-list> \n\n\n\n \n\n    <ion-list *ngSwitchCase="\'Team\'">\n\n\n\n    \n\n        <div id="bar_div_o" class="ttx"></div>\n\n \n\n  \n\n  </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\over-time-db\over-time-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], OverTimeDbPage);
    return OverTimeDbPage;
}());

//# sourceMappingURL=over-time-db.js.map

/***/ }),

/***/ 570:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecruitmentDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RecruitmentDbPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RecruitmentDbPage = (function () {
    function RecruitmentDbPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RecruitmentDbPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RecruitmentDbPage');
    };
    RecruitmentDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-recruitment-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\recruitment-db\recruitment-db.html"*/'<!--\n\n  Generated template for the RecruitmentDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Recruitment</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\recruitment-db\recruitment-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], RecruitmentDbPage);
    return RecruitmentDbPage;
}());

//# sourceMappingURL=recruitment-db.js.map

/***/ }),

/***/ 571:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverTimeDbExecutivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the OverTimeDbExecutivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OverTimeDbExecutivePage = (function () {
    //colors=['rgb(51, 102, 204)' , 'rgb(220, 57, 18)','rgb(255, 153, 0)','rgb(16, 150, 24)' ,'rgb(153, 0, 153)','rgb(0, 153, 198)'];
    function OverTimeDbExecutivePage(navCtrl, navParams, DashbaordProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DashbaordProvider = DashbaordProvider;
        this.AlertController = AlertController;
        this.colors = ['#990099', '#3366CC', '#DC3912', '#FF9900', '#109618', '#3B3EAC', '#0099C6', '#DD4477', '#66AA00', '#B82E2E', '#316395', '#994499', '#22AA99', '#AAAA11', '#6633CC', '#E67300', '#8B0707', '#329262', '#5574A6', '#3B3EAC'];
        this.getOverTimeMonthsDirectorateType();
    }
    OverTimeDbExecutivePage.prototype.ionViewDidLoad = function () {
    };
    OverTimeDbExecutivePage.prototype.getOverTimeMonthsDirectorateType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.DashbaordProvider.getOverTimeMonthsDirectorateType()
                        .then(function (data) {
                        _this.drawChart(data);
                    });
                }
                catch (error) {
                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
                }
                return [2 /*return*/];
            });
        });
    };
    OverTimeDbExecutivePage.prototype.drawChart = function (buData) {
        if (!buData)
            return;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'month');
        data.addColumn('number', 'amount');
        data.addColumn('number', 'id');
        data.addColumn('string', 'MonthNo');
        data.addColumn('string', 'year');
        /*data.addRows([
          ['DXB', 1264 ,1],
          ['EG', 401 ,2],
          ['KSA', 600,3],
          ['Jor', 324,4],
          ['LEB',136,5]
        ]);*/
        for (var i = 0; i < buData.length; i++) {
            data.addRows([[buData[i].month, buData[i].amount, i, buData[i].month_no + '', buData[i].year + '']]);
        }
        var options = {
            //'title':'Head Count',
            //   pieHole: 0.6,
            // is3D:true,
            legend: {
                position: 'top',
                maxLines: 3
            },
            colors: this.colors,
        };
        var t = this;
        var chart = new google.visualization.PieChart(document.getElementById('pie_div_overtime'));
        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                //var value = data.getValue(selectedItem.row, selectedItem.column);
                var value = data.getValue(selectedItem.row, 2);
                var label = data.getValue(selectedItem.row, 0);
                var monthNo = data.getValue(selectedItem.row, 3);
                var year = data.getValue(selectedItem.row, 4);
                // alert('The user selected ' + value);
                // t.drawBasic(value , label );
                t.drawOverTimeDepartments(value, label, year + '', monthNo + '');
            }
        }
        google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data, options);
    };
    OverTimeDbExecutivePage.prototype.drawOverTimeDepartments = function (val, label, year, monthNo) {
        this.getOverTimeMonthDepartments(val, label, year, monthNo);
    };
    OverTimeDbExecutivePage.prototype.getOverTimeMonthDepartments = function (val, label, year, monthNo) {
        var _this = this;
        try {
            this.DashbaordProvider.getOverTimeMonthDepartments(year, monthNo)
                .then(function (data) {
                //this.allDictorateData.
                _this.drawBasic(val, label, data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    OverTimeDbExecutivePage.prototype.drawBasic = function (val, label, overTimeDepartmentData) {
        // var data = google.visualization.arrayToDataTable([
        //   ['Diectorate', 'Head count'],
        //   ['Arabya', 300],
        //   ['HR', 240],
        //   ['Production', 270],
        //   ['Operations', 201]
        // ]);
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Departmnt');
        data.addColumn('number', 'amount');
        // data.addRows([
        //   ['Arabya', 300 ],
        //   ['HR', 240 ],
        //   ['Production', 270],
        //   ['Operations', 201]
        // ]);
        var barHeight = 0;
        for (var i = 0; i < overTimeDepartmentData.length; i++) {
            data.addRows([[overTimeDepartmentData[i].dept_name, overTimeDepartmentData[i].amount]]);
            barHeight = barHeight + 30;
        }
        var color = [];
        try {
            color[0] = this.colors[val];
            if (!color[0]) {
                color[0] = 'red';
            }
        }
        catch (err) {
            alert(err);
            color[0] = 'red';
        }
        var options = {
            //title: label+' Directories Head Count'
            height: 250,
            legend: 'none',
            chartArea: { width: '65%' },
            bar: { groupWidth: "55%" },
            colors: color,
            hAxis: {
                //  title: 'Total Population',
                minValue: 0
            },
            vAxis: {
                // title: 'City'
                textStyle: { fontSize: 10 }
            },
            animation: {
                duration: 700,
                startup: true
            }
        };
        var chart = new google.visualization.BarChart(document.getElementById('bar_div_overtime'));
        chart.draw(data, options);
    };
    OverTimeDbExecutivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-over-time-db-executive',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\over-time-db-executive\over-time-db-executive.html"*/'<!--\n\n  Generated template for the OverTimeDbExecutivePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Overtime</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <div id="pie_div_overtime" class="chart1"></div>\n\n\n\n  <div id="bar_div_overtime" class="chart2" ></div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\over-time-db-executive\over-time-db-executive.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__["a" /* DashbaordProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], OverTimeDbExecutivePage);
    return OverTimeDbExecutivePage;
}());

//# sourceMappingURL=over-time-db-executive.js.map

/***/ }),

/***/ 572:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExtraTimeDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ExtraTimeDbPage = (function () {
    function ExtraTimeDbPage(navCtrl, navParams, DashbaordProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DashbaordProvider = DashbaordProvider;
        this.AlertController = AlertController;
        this.colors = ['#990099', '#3366CC', '#DC3912', '#FF9900', '#109618', '#3B3EAC', '#0099C6', '#DD4477', '#66AA00', '#B82E2E', '#316395', '#994499', '#22AA99', '#AAAA11', '#6633CC', '#E67300', '#8B0707', '#329262', '#5574A6', '#3B3EAC'];
        this.getExtraTimeWorkedBGDierctorateType();
        //this.getDirectorateHeadCount('81');
        this.allDictorateData = null;
    }
    ExtraTimeDbPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ExtraTimeDbPage');
        //google.charts.setOnLoadCallback(this.drawChart());
        //google.charts.setOnLoadCallback(this.drawBasic());
    };
    ExtraTimeDbPage.prototype.getExtraTimeWorkedBGDierctorateType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.DashbaordProvider.getExtraTimeWorkedBGDierctorateType()
                        .then(function (data) {
                        _this.BuHeadCount = data;
                        _this.drawChart(data);
                    });
                }
                catch (error) {
                    __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
                }
                return [2 /*return*/];
            });
        });
    };
    ExtraTimeDbPage.prototype.getExtraTimeWorkedDepartmentDierctorateType = function (val, label, businessGroup) {
        var _this = this;
        try {
            this.DashbaordProvider.getExtraTimeWorkedDepartmentDierctorateType(businessGroup)
                .then(function (data) {
                //this.allDictorateData.
                _this.drawBasic(val, label, data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    ExtraTimeDbPage.prototype.drawExtraTimeWorkedDepartmentDierctorateTyp = function (val, label, businessGroup) {
        this.getExtraTimeWorkedDepartmentDierctorateType(val, label, businessGroup);
    };
    ExtraTimeDbPage.prototype.drawChart = function (buData) {
        if (!buData)
            return;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'BU');
        data.addColumn('number', 'extratime');
        data.addColumn('number', 'id');
        data.addColumn('number', 'BuId');
        /*data.addRows([
          ['DXB', 1264 ,1],
          ['EG', 401 ,2],
          ['KSA', 600,3],
          ['Jor', 324,4],
          ['LEB',136,5]
        ]);*/
        for (var i = 0; i < buData.length; i++) {
            data.addRows([[buData[i].business_groups, buData[i].extra_time_worked, i, buData[i].business_group_id]]);
        }
        var options = {
            //'title':'Head Count',
            pieHole: 0.6,
            is3D: true,
            legend: {
                position: 'top',
                maxLines: 2
            },
            colors: this.colors,
            // pieSliceText: 'value-and-percentage',
            slices: { 1: { offset: 0 },
                2: { offset: 0.02 },
                3: { offset: 0.01 },
                4: { offset: 0.02 },
            },
        };
        var t = this;
        var chart = new google.visualization.PieChart(document.getElementById('pie_div_hc'));
        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                //var value = data.getValue(selectedItem.row, selectedItem.column);
                var value = data.getValue(selectedItem.row, 2);
                var label = data.getValue(selectedItem.row, 0);
                var buId = data.getValue(selectedItem.row, 3);
                // alert('The user selected ' + value);
                // t.drawBasic(value , label );
                t.drawExtraTimeWorkedDepartmentDierctorateTyp(value, label, buId + '');
            }
        }
        google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data, options);
    };
    ExtraTimeDbPage.prototype.drawBasic = function (val, label, BUpersonTypeData) {
        // var data = google.visualization.arrayToDataTable([
        //   ['Diectorate', 'Head count'],
        //   ['Arabya', 300],
        //   ['HR', 240],
        //   ['Production', 270],
        //   ['Operations', 201]
        // ]);
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Department');
        data.addColumn('number', 'Extra time worked');
        // data.addRows([
        //   ['Arabya', 300 ],
        //   ['HR', 240 ],
        //   ['Production', 270],
        //   ['Operations', 201]
        // ]);
        var barHeight = 0;
        for (var i = 0; i < BUpersonTypeData.length; i++) {
            data.addRows([[BUpersonTypeData[i].dept_name, BUpersonTypeData[i].extra_time_worked]]);
            barHeight = barHeight + 10;
        }
        var color = [];
        try {
            color[0] = this.colors[val];
        }
        catch (err) {
            color[0] = 'red';
        }
        var options = {
            title: label + ' Extra time worked',
            height: 350,
            legend: 'none'
            // ,chartArea: {width: '65%'}
            // ,bar: {groupWidth: "55%"}
            ,
            colors: color,
            hAxis: {
                //  title: 'Total Population',
                minValue: 0
            },
            vAxis: {
                // title: 'City'
                textStyle: { fontSize: 10 }
            },
            animation: {
                duration: 700,
                startup: true
            }
        };
        var chart = new google.visualization.BarChart(document.getElementById('bar_div_hc'));
        chart.draw(data, options);
    };
    ExtraTimeDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-extra-time-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\extra-time-db\extra-time-db.html"*/'<!--\n\n  Generated template for the ExtraTimeDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Extra Time Worked</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <div id="pie_div_hc" class="chart1"></div>\n\n\n\n  <div id="bar_div_hc" class="chart2" ></div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\hr\extra-time-db\extra-time-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__["a" /* DashbaordProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ExtraTimeDbPage);
    return ExtraTimeDbPage;
}());

//# sourceMappingURL=extra-time-db.js.map

/***/ }),

/***/ 573:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScmDashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_requisition_db_my_requisition_db__ = __webpack_require__(574);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__approved_po_db_approved_po_db__ = __webpack_require__(575);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__budget_account_budget_account__ = __webpack_require__(576);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ScmDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScmDashboardPage = (function () {
    function ScmDashboardPage(navCtrl, navParams) {
        //this.searchDate=this.searchDate+3;
        //this.searchDate = new Date();
        // this.searchDate.setDate( this.searchDate.getDate() + 3 );
        //this.searchDate=this.searchDate.toISOString();
        //alert (this.searchDate);
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        //myDate: String = new Date().toISOString();
        this.myDate = new Date().toString();
        //////////////////
        var datex = new Date();
        //datex.setDate( datex.getDate() + 3 );
        this.searchDate = datex.toISOString();
        //alert (datex.toString());
    }
    ScmDashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScmDashboardPage');
    };
    ScmDashboardPage.prototype.openApprovedPOG = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__approved_po_db_approved_po_db__["a" /* ApprovedPoDbPage */]);
    };
    ScmDashboardPage.prototype.openApprovedPOL = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__my_requisition_db_my_requisition_db__["a" /* MyRequisitionDbPage */]);
    };
    ScmDashboardPage.prototype.openApprovedAcc = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__budget_account_budget_account__["a" /* BudgetAccountPage */]);
    };
    ScmDashboardPage.prototype.onchangeDate = function () {
        //alert (this.searchDate.toString());
        // this.searchDate=this.searchDate+3;
    };
    ScmDashboardPage.prototype.goNextDay = function () {
        var datex = new Date(this.searchDate);
        datex.setDate(datex.getDate() + 1);
        this.searchDate = datex.toISOString();
        var formattedDate = datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();
        //alert (formattedDate);
    };
    ScmDashboardPage.prototype.goPreviousDay = function () {
        var datex = new Date(this.searchDate);
        datex.setDate(datex.getDate() - 1);
        this.searchDate = datex.toISOString();
        var formattedDate = datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();
    };
    ScmDashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-scm-dashboard',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\scm\scm-dashboard\scm-dashboard.html"*/'<!--\n\n  Generated template for the ScmDashboardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>SCM Dashboard</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n  <!-- <div class="mnb"  id="ApprovedPOG" >\n\n    <button ion-button  color="dark"    class="ico-fa-btn1"   (click)="openApprovedPOG()">\n\n      <fa-icon name="calendar-check-o"  class="aw-ico1"></fa-icon>\n\n        \n\n    </button>\n\n    <h6 ion-text color="dark" class="btn-txt" >Approved</h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >PO</h6>\n\n  </div>\n\n  \n\n   \n\n  \n\n  <div class="mnb"  id="ApprovedPOL" >\n\n      <button ion-button  color="dark"  class="ico-fa-btn1"  (click)="openApprovedPOL()">\n\n          <fa-icon name="address-card"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">Approved</h6>\n\n      <h6 ion-text color="dark" class="btn-txt" >PO(OU)</h6>\n\n      \n\n  </div>\n\n\n\n  <div class="mnb"  id="ApprovedPOLacc" >\n\n    <button ion-button  color="dark"  class="ico-fa-btn1"  (click)="openApprovedAcc()">\n\n        <fa-icon name="address-card"  class="aw-ico1"></fa-icon>\n\n    </button> \n\n    <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">Approved</h6>\n\n    <h6 ion-text color="dark" class="btn-txt" >PO(Account)</h6>\n\n    \n\n</div> -->\n\n\n\n<!-- <ion-item>\n\n  <ion-label>Return Date </ion-label>\n\n  <ion-datetime displayFormat="DD-MMM-YYYY" [(ngModel)]="returnDate" (ionChange)="onchangeDate()"></ion-datetime>\n\n</ion-item>\n\n\n\n<button ion-button icon-only outline  color=\'light\' class=\'finger-btn\' (click)="addOneDay()" >\n\n  <ion-icon name="fastforward" class=\'finger-icon\' icon></ion-icon>\n\n</button>\n\n \n\n<button ion-button icon-only outline  color=\'light\' class=\'finger-btn\' (click)="addOneDay()" >\n\n  <ion-icon name="rewind" class=\'finger-icon\' icon></ion-icon>\n\n</button> -->\n\n<div class="containerx">\n\n<div class="containerz"> \n\n\n\n<div class="backz">\n\n    <button ion-button icon-only outline  color=\'light\' class=\'act-btn\' (click)="goPreviousDay()" >\n\n        <ion-icon name="rewind" class=\'act-icon\' icon></ion-icon>\n\n      </button>\n\n  </div>\n\n\n\n  <div class="datez">\n\n     \n\n      <ion-datetime displayFormat="DD-MMM-YYYY"  class="datezx"  [(ngModel)]="searchDate" (ionChange)="onchangeDate()"></ion-datetime>\n\n       \n\n    </div>\n\n \n\n\n\n    <div class="nextz">\n\n        <button ion-button icon-only outline  color=\'light\' class=\'act-btn\' (click)="goNextDay()" >\n\n            <ion-icon name="fastforward" class=\'act-icon\' icon></ion-icon>\n\n          </button>\n\n      </div>\n\n\n\n\n\n\n\n  </div>\n\n\n\n  <ion-searchbar  \n\n  [(ngModel)]="searchItem"\n\n  >\n\n  </ion-searchbar>\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\scm\scm-dashboard\scm-dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ScmDashboardPage);
    return ScmDashboardPage;
}());

//# sourceMappingURL=scm-dashboard.js.map

/***/ }),

/***/ 574:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyRequisitionDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyRequisitionDbPage = (function () {
    function MyRequisitionDbPage(navCtrl, navParams, DashbaordProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DashbaordProvider = DashbaordProvider;
        this.AlertController = AlertController;
        this.getApprovedPo();
    }
    MyRequisitionDbPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MyRequisitionDbPage');
        //google.charts.setOnLoadCallback(this.drawChart());
        //google.charts.setOnLoadCallback(this.drawChart2());
        // setTimeout(() => {
        //   this.drawChart();
        // }, 1000);
    };
    MyRequisitionDbPage.prototype.getApprovedPo = function () {
        var _this = this;
        try {
            this.DashbaordProvider.getApprovedPo()
                .then(function (data) {
                _this.poData = data;
                console.log(data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    MyRequisitionDbPage.prototype.getOperatinUnitBudgetPerAccount = function (organizationId, index) {
        var _this = this;
        try {
            this.DashbaordProvider.getOperatinUnitBudgetPerAccount(organizationId)
                .then(function (data) {
                console.log('dataxx');
                console.log(data);
                _this.drawBudget('chart-div' + index, data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    MyRequisitionDbPage.prototype.drawBudget = function (PdivId, budgetData) {
        if (!budgetData)
            return;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Acount');
        data.addColumn('number', 'Amount');
        data.addColumn('number', 'BilledAmount');
        data.addColumn('number', 'BudgetAmount');
        var height = 100;
        for (var i = 0; i < budgetData.length; i++) {
            data.addRows([[budgetData[i].account, budgetData[i].amount, budgetData[i].amount_billed, budgetData[i].budget_amount]]);
            height = height + 20;
        }
        // var data = google.visualization.arrayToDataTable([
        //   ['City', '2010 Population', '2000 Population'],
        //   ['New York City, NY', 8175000, 8008000],
        //   ['Los Angeles, CA', 3792000, 3694000],
        //   ['Chicago, IL', 2695000, 2896000],
        //   ['Houston, TX', 2099000, 1953000],
        //   ['Philadelphia, PA', 1526000, 1517000]
        // ]);
        var options = {
            title: 'Approved PO vs Billed Amount 2018',
            //height:height,
            chartArea: { width: '60%' },
            // hAxis: {
            //   title: 'Total Population',
            //   minValue: 0
            // },
            // vAxis: {
            //   title: 'City'
            // } , 
            legend: 'none'
        };
        var chart = new google.visualization.BarChart(document.getElementById(PdivId));
        chart.draw(data, options);
    };
    MyRequisitionDbPage.prototype.onPoCkicked = function (index, poData) {
        console.log(poData);
        if (poData && poData.operatin_unit_id)
            this.getOperatinUnitBudgetPerAccount(poData.operatin_unit_id, index);
    };
    MyRequisitionDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-my-requisition-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\scm\my-requisition-db\my-requisition-db.html"*/'<!--\n\n  Generated template for the MyRequisitionDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Approved PO</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<!--\n\n<ion-content > \n\n \n\n    <div id="chart_div"></div>\n\n    <div id="piechart_3d"></div>      \n\n  \n\n        <div class="container1">\n\n                      \n\n       \n\n                      <div  class="pa1"> \n\n                                <div class="cha1 cha">\n\n                                  <div class="sub-div sub-div1 sub-div-l">\n\n                                      <span class="txt-req1"> \n\n                                    5\n\n                                    </span>\n\n                                    </div>\n\n\n\n                                    <div class="sub-div sub-div2 sub-div-l">\n\n                                        <span class="txt-req3"> \n\n                                      Pending\n\n                                      </span>\n\n                                      </div>\n\n\n\n                                </div>\n\n                                <div  class="cha2 cha">\n\n                                    <div class="sub-div sub-div1 sub-div-r">\n\n                                        <span class="txt-req2"> \n\n                                      7\n\n                                      </span>\n\n                                        </div>\n\n\n\n                                        <div class="sub-div sub-div2 sub-div-r">\n\n                                            <span class="txt-req3"> \n\n                                          In Process\n\n                                          </span>\n\n                                            </div>\n\n                                  </div>\n\n                    \n\n\n\n                                </div> \n\n                             \n\n                  \n\n\n\n                              <div class="mid1">\n\n\n\n                                \n\n                                \n\n\n\n\n\n\n\n\n\n                                \n\n                              </div>\n\n                 <div  class="pa1"> \n\n                                <div class="cha1 cha">\n\n                                  <div class="sub-div sub-div1 sub-div-l">\n\n                                      <span class="txt-req1"> \n\n                                    12\n\n                                    </span>\n\n                                    </div>\n\n\n\n                                    <div class="sub-div sub-div2 sub-div-l">\n\n                                        <span class="txt-req3"> \n\n                                            Pending Receipt\n\n                                      </span>\n\n                                      </div>\n\n\n\n                                </div>\n\n                                <div  class="cha2 cha">\n\n                                    <div class="sub-div sub-div1 sub-div-r">\n\n                                        <span class="txt-req2"> \n\n                                      123\n\n                                      </span>\n\n                                        </div>\n\n\n\n                                        <div class="sub-div sub-div2 sub-div-r">\n\n                                            <span class="txt-req3"> \n\n                                          Closed PR\n\n                                          </span>\n\n                                            </div>\n\n                                  </div>\n\n                      \n\n                    </div> \n\n\n\n                    <div class="mid1">\n\n\n\n                                \n\n                                \n\n\n\n\n\n\n\n\n\n                                \n\n                      </div>\n\n\n\n\n\n                    <div  class="pa1"> \n\n                        <div class="cha1 cha">\n\n                          <div class="sub-div sub-div1 sub-div-l">\n\n                              <span class="txt-req1"> \n\n                            12\n\n                            </span>\n\n                            </div>\n\n\n\n                            <div class="sub-div sub-div2 sub-div-l">\n\n                                <span class="txt-req3"> \n\n                                    Pending Receipt\n\n                              </span>\n\n                              </div>\n\n\n\n                        </div>\n\n                        <div  class="cha2 cha">\n\n                            <div class="sub-div sub-div1 sub-div-r">\n\n                                <span class="txt-req2"> \n\n                              123\n\n                              </span>\n\n                                </div>\n\n\n\n                                <div class="sub-div sub-div2 sub-div-r">\n\n                                    <span class="txt-req3"> \n\n                                  Closed PR\n\n                                  </span>\n\n                                    </div>\n\n                          </div>\n\n              \n\n            </div> \n\n                  </div>\n\n \n\n</ion-content>\n\n-->\n\n<ion-content>\n\n\n\n\n\n<div class="container">\n\n    <div class="row">\n\n        <div class="col-md-12">\n\n            <div class="main-timeline">\n\n           \n\n\n\n                <div class="main-timelinex" *ngFor="let po of poData;   let i = index ">\n\n                        <div class="timeline" >\n\n                    <div class="timeline-content">\n\n                        <span class="timeline-year">\n\n                                <button ion-button icon-only outline  color=\'light\' class=\'tm-btn\'  (click)="onPoCkicked(i,po)"> {{po.bu_short_name}}</button> \n\n                           </span>\n\n                        <div class="content">\n\n                            <h3 class="title">{{po.operatin_unit}}</h3>\n\n                            <p class="description">\n\n                               {{po.amt}}$\n\n                            </p>\n\n                        </div>\n\n                      \n\n                      </div>\n\n\n\n                      \n\n                </div>\n\n                <div [id]="\'chart-div\'+i" class=\'grap-div\'></div>\n\n                </div>\n\n                        \n\n                <!-- <div *ngFor="let po of poData;   let i = index ">    \n\n            </div> -->\n\n\n\n                <!-- <div class="timeline">\n\n                    <div class="timeline-content">\n\n                        <span class="timeline-year">2017</span>\n\n                        <div class="content">\n\n                            <h3 class="title">Remaining</h3>\n\n                            <p class="description">\n\n                                1,23,456$\n\n                            </p>\n\n                        </div>\n\n                      </div>\n\n                </div>\n\n\n\n                <div class="timeline">\n\n                    <div class="timeline-content">\n\n                        <span class="timeline-year">2016</span>\n\n                        <div class="content">\n\n                            <h3 class="title">Commited</h3>\n\n                            <p class="description">\n\n                                1,23,456$\n\n                            </p>\n\n                        </div>\n\n                      </div>\n\n                </div>\n\n\n\n\n\n                <div class="timeline">\n\n                    <div class="timeline-content">\n\n                        <span class="timeline-year">2015</span>\n\n                        <div class="content">\n\n                            <h3 class="title">Purchasing</h3>\n\n                            <p class="description">\n\n                                1,23,456$\n\n                            </p>\n\n                        </div>\n\n                      </div>\n\n                </div> -->\n\n            </div>\n\n        </div>\n\n    </div>\n\n</div>\n\n  </ion-content>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\scm\my-requisition-db\my-requisition-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__["a" /* DashbaordProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], MyRequisitionDbPage);
    return MyRequisitionDbPage;
}());

//# sourceMappingURL=my-requisition-db.js.map

/***/ }),

/***/ 575:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApprovedPoDbPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApprovedPoDbPage = (function () {
    function ApprovedPoDbPage(navCtrl, navParams, DashbaordProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DashbaordProvider = DashbaordProvider;
        this.AlertController = AlertController;
        this.getApprovedPo();
    }
    ApprovedPoDbPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ApprovedPoDbPage');
    };
    ApprovedPoDbPage.prototype.getApprovedPo = function () {
        var _this = this;
        try {
            this.DashbaordProvider.getApprovedPo()
                .then(function (data) {
                _this.drawAppovedPo(data);
                console.log(data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    ApprovedPoDbPage.prototype.drawAppovedPo = function (Podata) {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'operating Unit');
        data.addColumn('number', 'Amount');
        data.addColumn('number', 'BilledAmount');
        data.addColumn('number', 'Budget');
        for (var i = 0; i < Podata.length; i++)
            data.addRows([[Podata[i].operatin_unit, Podata[i].amount, Podata[i].amount_billed, /*Podata[i].budget_amount*/ null]]);
        var options = {
            legend: 'none',
            height: 550,
            title: 'Approved PO vs Billed Amount 2018',
            //  colors:['red'],
            vAxis: {
                gridlines: {
                    color: 'transparent'
                }
            }
        };
        var chart = new google.visualization.LineChart(document.getElementById('chart_div_po'));
        // var chart = new google.visualization.PieChart(document.getElementById('chart_div_po'));
        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            console.log(chart.getSelection());
            console.log('selectedItem');
            console.log(chart);
            if (selectedItem) {
                console.log(selectedItem);
                //var value = data.getValue(selectedItem.row, selectedItem.column);
                // alert (selectedItem);
                var value = data.getValue(selectedItem.row, 1);
                var label = data.getValue(selectedItem.row, 0);
                var buId = data.getValue(selectedItem.row, 1);
                // alert('The user selected ' + value);
                // t.drawBasic(value , label );
                // t.drawBGpersonType(value , label ,buId+'');
            }
        }
        google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data, options);
    };
    ApprovedPoDbPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-approved-po-db',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\scm\approved-po-db\approved-po-db.html"*/'<!--\n\n  Generated template for the ApprovedPoDbPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Approved PO</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n<div id=\'chart_div_po\' class=\'line-chart1\'></div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\scm\approved-po-db\approved-po-db.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__["a" /* DashbaordProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ApprovedPoDbPage);
    return ApprovedPoDbPage;
}());

//# sourceMappingURL=approved-po-db.js.map

/***/ }),

/***/ 576:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BudgetAccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BudgetAccountPage = (function () {
    function BudgetAccountPage(navCtrl, navParams, DashbaordProvider, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DashbaordProvider = DashbaordProvider;
        this.AlertController = AlertController;
        this.getApprovedPo();
    }
    BudgetAccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BudgetAccountPage');
    };
    BudgetAccountPage.prototype.getApprovedPo = function () {
        var _this = this;
        try {
            this.DashbaordProvider.getApprovedPo()
                .then(function (data) {
                _this.drawhHadcountGender(data);
                console.log(data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    BudgetAccountPage.prototype.drawhHadcountGender = function (Podata) {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'operating Unit');
        data.addColumn('number', 'Amount');
        data.addColumn('number', 'BilledAmount');
        data.addColumn('number', 'orgid');
        for (var i = 0; i < Podata.length; i++)
            data.addRows([[Podata[i].operatin_unit, Podata[i].amount, Podata[i].amount_billed, Podata[i].operatin_unit_id]]);
        /*Podata[i].budget_amount*/
        var options = {
            //  'width':400,
            height: 500,
            //colors:['#6633CC','#DD4477'],
            animation: {
                duration: 1000,
                startup: true
            },
            //legend: { position: 'top', maxLines: 3 },
            legend: 'none',
            bar: { groupWidth: '85%' },
            isStacked: true
        };
        var t = this;
        var chart = new google.visualization.BarChart(document.getElementById('bar_budgt'));
        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            console.log(chart.getSelection());
            console.log('selectedItem');
            console.log(chart);
            if (selectedItem) {
                console.log(selectedItem);
                //var value = data.getValue(selectedItem.row, selectedItem.column);
                //  var value = data.getValue(selectedItem.row, 2);
                //var label = data.getValue(selectedItem.row, 0);
                var org = data.getValue(selectedItem.row, 3);
                // alert('The user selected ' + value);
                // t.drawBasic(value , label );
                //alert (org)
                t.getOperatinUnitBudgetPerAccount(org);
            }
        }
        google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data, options);
    };
    BudgetAccountPage.prototype.getOperatinUnitBudgetPerAccount = function (organizationId) {
        var _this = this;
        try {
            this.DashbaordProvider.getOperatinUnitBudgetPerAccount(organizationId)
                .then(function (data) {
                console.log('dataxx');
                console.log(data);
                _this.drawBudget('chart-div', data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error);
        }
    };
    BudgetAccountPage.prototype.drawBudget = function (PdivId, budgetData) {
        if (!budgetData)
            return;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Acount');
        data.addColumn('number', 'Amount');
        data.addColumn('number', 'BilledAmount');
        data.addColumn('number', 'BudgetAmount');
        var height = 100;
        for (var i = 0; i < budgetData.length; i++) {
            data.addRows([[budgetData[i].account, budgetData[i].amount, budgetData[i].amount_billed, budgetData[i].budget_amount]]);
            height = height + 20;
        }
        // var data = google.visualization.arrayToDataTable([
        //   ['City', '2010 Population', '2000 Population'],
        //   ['New York City, NY', 8175000, 8008000],
        //   ['Los Angeles, CA', 3792000, 3694000],
        //   ['Chicago, IL', 2695000, 2896000],
        //   ['Houston, TX', 2099000, 1953000],
        //   ['Philadelphia, PA', 1526000, 1517000]
        // ]);
        var options = {
            title: 'Approved PO vs Billed Amount 2018',
            //height:height,
            chartArea: { width: '60%' },
            // hAxis: {
            //   title: 'Total Population',
            //   minValue: 0
            // },
            // vAxis: {
            //   title: 'City'
            // } , 
            legend: 'none'
        };
        var chart = new google.visualization.BarChart(document.getElementById(PdivId));
        chart.draw(data, options);
    };
    BudgetAccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-budget-account',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\scm\budget-account\budget-account.html"*/'<!--\n\n  Generated template for the BudgetAccountPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Commitment vs Actual </ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <div id="bar_budgt" class="g1" > </div>\n\n  <div id="chart-div" class=\'grap-div\'></div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\dash-board\scm\budget-account\budget-account.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_dashbaord_dashbaord__["a" /* DashbaordProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], BudgetAccountPage);
    return BudgetAccountPage;
}());

//# sourceMappingURL=budget-account.js.map

/***/ }),

/***/ 577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicSpacePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_pages_start_start__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Scheduler_myspace_myspace__ = __webpack_require__(578);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Scheduler_myteam_myteam__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Scheduler_workgroups_workgroups__ = __webpack_require__(581);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the PublicSpacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PublicSpacePage = (function () {
    function PublicSpacePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // this.userId            =this.navParams.get('userId');  
        // this.personId          =this.navParams.get('personId'); 
        // this.businessGroup     =this.navParams.get('businessGroup'); 
        // this.assignmentId      =this.navParams.get('assignmentId'); 
        this.AAmyspace = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].sAAmyspace;
        //AppModule.getProperty('myspace');
        this.AAmyteam = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].sAAmyteam;
        //.getProperty('myteam');
        this.AAworkgroups = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].sAAworkgroups;
        //.getProperty('workgroups');
    }
    PublicSpacePage.prototype.ionViewDidLoad = function () {
        this.setBackButtonAction();
        console.log('ionViewDidLoad PublicSpacePage');
    };
    PublicSpacePage.prototype.setBackButtonAction = function () {
        var _this = this;
        this.Navbar.backButtonClick = function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__shared_pages_start_start__["a" /* StartPage */]);
        };
    };
    PublicSpacePage.prototype.goMySpacePage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__Scheduler_myspace_myspace__["a" /* MySpacePage */]);
    };
    PublicSpacePage.prototype.goMyTeamPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__Scheduler_myteam_myteam__["a" /* MyTeamPage */]);
    };
    PublicSpacePage.prototype.goWKage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__Scheduler_workgroups_workgroups__["a" /* WorkgroupsPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* Navbar */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* Navbar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* Navbar */]) === "function" && _a || Object)
    ], PublicSpacePage.prototype, "Navbar", void 0);
    PublicSpacePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
            selector: 'page-publicspace',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\publicSpace\publicspace.html"*/'<!--\n\n  Generated template for the LeaveHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n   \n\n    <ion-navbar hideBackButton=\'false\' >\n\n      \n\n      <ion-title>Al Arabiya Scheduler</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding class="ion-content">\n\n  \n\n\n\n    <div class="mnb" *ngIf="AAmyspace!=\'N\'"  >\n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goMySpacePage()">\n\n          <fa-icon name="calendar-o"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      \n\n      <h6 ion-text color="dark" class="btn-txt " >My Space</h6>\n\n      <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n    </div>\n\n  \n\n  \n\n    <div class="mnb"  *ngIf="AAmyteam!=\'N\'" >\n\n        <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goMyTeamPage()" >\n\n            <fa-icon name="list"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n        <h6 ion-text color="dark" class="btn-txt " >My Team</h6>\n\n        <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n    </div>\n\n    <div class="mnb"  *ngIf="AAworkgroups!=\'N\'" >\n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goWKage()" >\n\n          <fa-icon name="list"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >WorkGroups</h6>\n\n      <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n  </div>\n\n\n\n    <div class="mnb"   >\n\n      <button ion-button  color="dark"   class="ico-fa-btn1 " [disabled]="true"  >\n\n          <fa-icon name="calendar-plus-o"  class="aw-ico1"></fa-icon>\n\n      </button>\n\n      <h6 ion-text color="dark" class="btn-txt " >Admin</h6>\n\n      <h6 ion-text color="dark" class="btn-txt " >&nbsp;</h6>\n\n  </div>\n\n   \n\n  \n\n  \n\n  \n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\publicSpace\publicspace.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */]) === "function" && _c || Object])
    ], PublicSpacePage);
    return PublicSpacePage;
    var _a, _b, _c;
}());

//# sourceMappingURL=publicspace.js.map

/***/ }),

/***/ 578:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MySpacePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_scheduler_scheduler__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the PublicSpacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MySpacePage = (function () {
    function MySpacePage(navCtrl, navParams, SchedulerData, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.SchedulerData = SchedulerData;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.fromR = 0;
        this.v_step = 12;
        this.toR = this.v_step;
    }
    MySpacePage.prototype.ionViewDidLoad = function () {
        // this.setBackButtonAction();
        this.ShowToday();
        this.getMySpaceList();
        console.log('ionViewDidLoad MySpacePage');
    };
    MySpacePage.prototype.getMySpaceList = function () {
        var _this = this;
        var datex = new Date(this.searchDate);
        // this.searchDate=datex.toISOString();
        var formattedDate = datex.getFullYear() + "-" + (datex.getMonth() + 1) + "-" + datex.getDate();
        //datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();
        try {
            this.SchedulerData.getMySpaceList(formattedDate)
                .then(function (data) {
                _this.MySpaceListAll = data;
                if (_this.MySpaceListAll) {
                    _this.MySpaceList = _this.MySpaceListAll.slice(_this.fromR, _this.toR);
                }
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, "error");
        }
    };
    MySpacePage.prototype.doRefresh = function (refresher) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.toR = this.v_step;
                        return [4 /*yield*/, this.getMySpaceList()];
                    case 1:
                        _a.sent();
                        refresher.complete();
                        return [2 /*return*/];
                }
            });
        });
    };
    MySpacePage.prototype.doInfinite = function (infiniteScroll) {
        // this.fromR=this.fromR+this.v_step;
        this.toR = this.toR + this.v_step;
        try {
            if (this.MySpaceListAll)
                this.MySpaceList = (this.MySpaceListAll.slice(0, this.toR));
            infiniteScroll.complete();
        }
        catch (err) {
        }
        console.log(' infiniteScroll has ended');
    };
    MySpacePage.prototype.onchangeDate = function () {
        //alert (this.searchDate.toString());
        this.getMySpaceList();
        // this.searchDate=this.searchDate+3;
    };
    MySpacePage.prototype.goNextDay = function () {
        var datex = new Date(this.searchDate);
        datex.setDate(datex.getDate() + 1);
        this.searchDate = datex.toISOString();
        var formattedDate = datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();
        //alert (formattedDate);
    };
    MySpacePage.prototype.goPreviousDay = function () {
        var datex = new Date(this.searchDate);
        datex.setDate(datex.getDate() - 1);
        this.searchDate = datex.toISOString();
        var formattedDate = datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();
    };
    MySpacePage.prototype.ShowToday = function () {
        var datex = new Date();
        this.searchDate = datex.toISOString();
        this.pageTopx.scrollToTop();
        // alert (this.pageTopx)
    };
    MySpacePage.prototype.onSwipe = function (e) {
        //alert (e.direction)
        var t = e.direction;
        if (t == 2)
            this.goNextDay();
        else
            this.goPreviousDay();
        // 2 next day
        // 4 previous day
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('pageTopx'),
        __metadata("design:type", Object)
    ], MySpacePage.prototype, "pageTopx", void 0);
    MySpacePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-myspace',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\myspace\myspace.html"*/'<!--\n\n  Generated template for the LeaveHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n   \n\n    <ion-navbar hideBackButton=\'false\' >\n\n      \n\n      <ion-title>My Space</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content #pageTopx>  \n\n      <div (swipe)="onSwipe($event)" style="min-height:100%;"  >\n\n      <ion-refresher (ionRefresh)="doRefresh($event)">\n\n          <ion-refresher-content pullingIcon="arrow-dropdown"\n\n          pullingText="Pull to refresh"\n\n          refreshingSpinner="circles"\n\n          refreshingText="Refreshing..."></ion-refresher-content>\n\n        </ion-refresher>\n\n \n\n\n\n        <div class="containerz"> \n\n\n\n          <div class="backz">\n\n              <button ion-button icon-only outline  color=\'light\' class=\'act-btn\' (click)="goPreviousDay()" >\n\n                  <ion-icon name="ios-arrow-back" class=\'act-icon\' icon></ion-icon>\n\n                </button>\n\n            </div>\n\n          \n\n            <div class="datez">\n\n              <ion-item class="in-date">\n\n              \n\n   \n\n                <ion-datetime displayFormat="DDDD, DD-MMM-YYYY"  class="in-date2"  [(ngModel)]="searchDate" (ionChange)="onchangeDate()"></ion-datetime>\n\n               \n\n              </ion-item>\n\n              </div>\n\n          \n\n          \n\n              <div class="nextz">\n\n                  <button ion-button icon-only outline  color=\'light\' class=\'act-btn\' (click)="goNextDay()" >\n\n                      <ion-icon name="ios-arrow-forward" class=\'act-icon\' icon></ion-icon>\n\n                    </button>\n\n                </div>\n\n          \n\n            </div>\n\n          \n\n     <ion-list >\n\n         <ion-item  *ngFor="let item of MySpaceList" >\n\n           \n\n             <ion-avatar item-start> \n\n                  <div class="{{item.cssclass}}"   >  {{item.calendar_avatar}} </div>\n\n                </ion-avatar>\n\n          \n\n                <h2 class="list_item_title1" >  {{item.calendar_actvity_title}} </h2>\n\n                <div class="list_item_sub_block">\n\n                   <div  >  \n\n                <span  class="list_item_sub_title5" >  {{item.schdr_cal_time}}</span> \n\n                 </div>\n\n                <div   >  \n\n                <span  class="list_item_sub_title4" >  {{item.employee_name}} </span> \n\n                </div>\n\n                <div  [hidden]="item.cssclass!=\'sh\'" >  \n\n                  <span  class="list_item_sub_title6" >  {{item.note}} </span> \n\n                  </div>\n\n          </div>\n\n             </ion-item> \n\n      </ion-list> \n\n  \n\n     \n\n \n\n   \n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)" > \n\n      <ion-infinite-scroll-content\n\n      loadingSpinner="bubbles"\n\n      loadingText="Loading more data...">\n\n    </ion-infinite-scroll-content> \n\n    </ion-infinite-scroll>\n\n  \n\n   \n\n  </div>\n\n  \n\n  </ion-content>\n\n  <ion-footer class="page_footer">\n\n  \n\n  \n\n      <div class="div_footer">\n\n          <button ion-button block item-end (click)="ShowToday()">Today</button>\n\n        </div>\n\n      </ion-footer>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\myspace\myspace.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_scheduler_scheduler__["a" /* SchedulerProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */]])
    ], MySpacePage);
    return MySpacePage;
}());

//# sourceMappingURL=myspace.js.map

/***/ }),

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyTeamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__myteam_teamdetails__ = __webpack_require__(580);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_scheduler_scheduler__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_aes_256__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










/**
 * Generated class for the PublicSpacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyTeamPage = (function () {
    function MyTeamPage(navCtrl, navParams, modalCtrl, SchedulerData, loadingController, AlertController, sanitized, aes256) {
        //this.userId            =AppModule.userId; 
        //this.personId          =AppModule.personId; 
        //this.businessGroup     =AppModule.businessGroup; 
        //this.assignmentId      =AppModule.assignmentId; 
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.SchedulerData = SchedulerData;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.sanitized = sanitized;
        this.aes256 = aes256;
        this.MyTeamList = [];
        this.MyTeamListFixed = [];
        this.fromR = 0;
        this.v_step = 12;
        this.toR = this.v_step;
        //public newSubcategories: any[] = [];
        this.items = [];
        //     let datex :Date=new Date();
        // this.searchDate=datex.toISOString();
    }
    MyTeamPage.prototype.ionViewDidLoad = function () {
        // 
        this.ShowToday();
        this.getMyTeamList();
        console.log('ionViewDidLoad MyTeamPage');
    };
    MyTeamPage.prototype.getMyTeamList = function () {
        var _this = this;
        var datex = new Date(this.searchDate);
        // this.searchDate=datex.toISOString();
        var formattedDate = datex.getFullYear() + "-" + (datex.getMonth() + 1) + "-" + datex.getDate();
        try {
            this.SchedulerData.getMyTeamList(formattedDate)
                .then(function (data) {
                _this.MyTeamListAll = data;
                // this.MyTeamList=this.MyTeamListAll;
                if (_this.MyTeamListAll) {
                    _this.MyTeamList = _this.MyTeamListAll.slice(_this.fromR, _this.toR);
                    _this.MyTeamListFixed = _this.MyTeamListAll;
                    //
                }
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, "error");
        }
    };
    MyTeamPage.prototype.doRefresh = function (refresher) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.toR = this.v_step;
                        return [4 /*yield*/, this.getMyTeamList()];
                    case 1:
                        _a.sent();
                        refresher.complete();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyTeamPage.prototype.doInfinite = function (infiniteScroll) {
        // this.fromR=this.fromR+this.v_step;
        this.toR = this.toR + this.v_step;
        try {
            if (this.MyTeamListAll)
                this.MyTeamList = (this.MyTeamListAll.slice(0, this.toR));
            infiniteScroll.complete();
        }
        catch (err) {
        }
        console.log(' infiniteScroll has ended');
    };
    MyTeamPage.prototype.openModal = function (detail) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__myteam_teamdetails__["a" /* TeamDetailsPage */], detail);
        modal.present();
    };
    MyTeamPage.prototype.onTabEmployee = function (detail) {
        //let modal = this.modalCtrl.create(TeamDetailsPage, detail);
        //modal.present();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__myteam_teamdetails__["a" /* TeamDetailsPage */], { info: detail });
    };
    MyTeamPage.prototype.reloadlist = function () {
        this.MyTeamList = this.MyTeamListFixed.slice(this.fromR, this.toR);
        ;
    };
    MyTeamPage.prototype.getItems = function (ev) {
        this.reloadlist();
        console.log("MyTeam1:" + this.MyTeamList.length);
        var val = ev.target.value;
        console.log('vv ' + val);
        if (val && val.trim() !== '') {
            this.MyTeamList = this.MyTeamListFixed.filter(function (item) {
                // return (item.full_name.toLowerCase().includes(val.toLowerCase()));
                // to be able to search with shift in webservice set nvl(shift(---))
                return (item.full_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
                //||item.shift.trim().toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            console.log("MyTeam2:" + this.MyTeamList.length);
            //        for(let i in this.MyTeamList)
            //    {
            //  console.log("employeename:"+this.MyTeamList[i].full_name);     
            //    }
            console.log("WholeTeam:" + this.MyTeamListFixed.length);
        }
    };
    MyTeamPage.prototype.onchangeDate = function () {
        this.searchemp = "";
        this.getMyTeamList();
        //alert (this.searchDate.toString());
        // this.searchDate=this.searchDate+3;
    };
    MyTeamPage.prototype.goNextDay = function () {
        var datex = new Date(this.searchDate);
        datex.setDate(datex.getDate() + 1);
        this.searchDate = datex.toISOString();
        var formattedDate = datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();
        //alert (formattedDate);
    };
    MyTeamPage.prototype.goPreviousDay = function () {
        var datex = new Date(this.searchDate);
        datex.setDate(datex.getDate() - 1);
        this.searchDate = datex.toISOString();
        var formattedDate = datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();
    };
    MyTeamPage.prototype.onSwipe = function (e) {
        //alert (e.direction)
        var t = e.direction;
        if (t == 2)
            this.goNextDay();
        else
            this.goPreviousDay();
        // 2 next day
        // 4 previous day
    };
    MyTeamPage.prototype.geturl = function (personId) {
        return __awaiter(this, void 0, void 0, function () {
            var token, url, t, x;
            return __generator(this, function (_a) {
                token = __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].getProperty('wsid');
                token = token.replace(/#/g, '@@@@');
                token = encodeURI(token);
                url = __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].wsURL.substr(0, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].wsURL.lastIndexOf("/"));
                t = __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].getProperty('wsid') + personId);
                x = encodeURI(t + '');
                x = x + '';
                return [2 /*return*/, url + '/imageservlet?px=' + personId + '&token=' + token + '&signature=' + x];
            });
        });
    };
    MyTeamPage.prototype.ShowToday = function () {
        var datex = new Date();
        this.searchDate = datex.toISOString();
        this.pageTop.scrollToTop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* Navbar */])
    ], MyTeamPage.prototype, "Navbar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('pageTop'),
        __metadata("design:type", Object)
    ], MyTeamPage.prototype, "pageTop", void 0);
    MyTeamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-myteam',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\myteam\myteam.html"*/'<!--\n\n  Generated template for the LeaveHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar hideBackButton=\'false\' >\n\n        <ion-searchbar [(ngModel)]="searchemp" class="teams-search"\n\n        placeholder=" Search Team"\n\n      (ionInput)="getItems($event)"\n\n      \n\n     >\n\n    </ion-searchbar>\n\n     <!-- <ion-title> My Team</ion-title> -->\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n\n\n  <ion-content  #pageTop> \n\n\n\n      <div (swipe)="onSwipe($event)" style="min-height:100%;">\n\n\n\n\n\n\n\n  <div class="containerz"> \n\n\n\n    <div class="backz">\n\n        <button ion-button icon-only outline  color=\'light\' class=\'act-btn\' (click)="goPreviousDay()" >\n\n            <ion-icon name="ios-arrow-back" class=\'act-icon\' icon></ion-icon>\n\n          </button>\n\n      </div>\n\n\n\n\n\n      <div class="datez">\n\n      <ion-item class="in-date">\n\n          <ion-datetime displayFormat="DDDD, DD-MMM-YYYY"  class="in-date2"  [(ngModel)]="searchDate" (ionChange)="onchangeDate()"></ion-datetime>\n\n        </ion-item>\n\n        </div>\n\n    \n\n    \n\n        <div class="nextz">\n\n            <button ion-button icon-only outline  color=\'light\' class=\'act-btn\' (click)="goNextDay()" >\n\n                <ion-icon name="ios-arrow-forward" class=\'act-icon\' icon></ion-icon>\n\n              </button>\n\n          </div>\n\n    \n\n      </div>\n\n\n\n\n\n \n\n    \n\n      <ion-refresher (ionRefresh)="doRefresh($event)">\n\n          <ion-refresher-content pullingIcon="arrow-dropdown"\n\n          pullingText="Pull to refresh"\n\n          refreshingSpinner="circles"\n\n          refreshingText="Refreshing..."></ion-refresher-content>\n\n        </ion-refresher>\n\n     \n\n\n\n\n\n                  <ion-list >\n\n                \n\n                \n\n                    <button ion-item   *ngFor="let item of MyTeamList"  (click)="onTabEmployee(item)">\n\n                        <!-- <button ion-item  detail-none [hidden]="isHidden[i]" (click)="noticationTapped($event, wf )" > -->\n\n                      <ion-avatar item-start>\n\n                          <!-- <img [src]="sanitized.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+character.img)" /> -->\n\n\n\n                          \n\n                        <img  [hidden]="item.img==null"[src]="sanitized.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+item.img)">\n\n                        <img  [hidden]="item.img!=null" src="assets/imgs/avatar.png">\n\n\n\n                        <!-- <img [hidden]="item.img==null" [src]="sanitized.bypassSecurityTrustUrl(geturl(item.person_id))" class="image-css" /> \n\n                        <div class="class4"   >  AA</div>\n\n                        <img  [hidden]="item.img!=null" src="assets/imgs/avatar.png">\n\n                        -->\n\n                        <!-- <img src="assets/imgs/mbclogo.png" > -->\n\n\n\n\n\n                      </ion-avatar>\n\n                     \n\n                       <!-- <h2 [class.leaves]="item.leaves!=null" [class.noleaves]="item.leaves==null" >  -->\n\n                         <h2 [ngClass]="item.leaves!=null ? \'leaves\' : \'noleaves\'">\n\n                        {{item.full_name}}</h2>\n\n                      <p style="font-size:10px;" >{{item.shift}}</p>\n\n                      <!-- <button ion-button clear item-end (click)="openModal({info: item})">more</button> -->\n\n                   </button>\n\n                    <!-- </ion-item> -->\n\n                \n\n                                   </ion-list>\n\n                \n\n                  <ion-infinite-scroll (ionInfinite)="doInfinite($event)" > \n\n                    <ion-infinite-scroll-content\n\n                    loadingSpinner="bubbles"\n\n                    loadingText="Loading more data...">\n\n                  </ion-infinite-scroll-content> \n\n                  </ion-infinite-scroll>\n\n  </div>\n\n              \n\n\n\n  </ion-content>\n\n  <ion-footer class="page_footer">\n\n  \n\n  \n\n    <div class="div_footer">\n\n        <button ion-button block item-end (click)="ShowToday()">Today</button>\n\n      </div>\n\n    </ion-footer>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\myteam\myteam.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_4__providers_scheduler_scheduler__["a" /* SchedulerProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_aes_256__["a" /* AES256 */]])
    ], MyTeamPage);
    return MyTeamPage;
}());

//# sourceMappingURL=myteam.js.map

/***/ }),

/***/ 580:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeamDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the PublicSpacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TeamDetailsPage = (function () {
    function TeamDetailsPage(platform, params, viewCtrl, sanitized, aes256) {
        this.platform = platform;
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.sanitized = sanitized;
        this.aes256 = aes256;
        this.character = this.params.get('info');
    }
    TeamDetailsPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    TeamDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MyTeamPage');
    };
    TeamDetailsPage.prototype.geturl = function (personId) {
        var token = __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].getProperty('wsid');
        token = token.replace(/#/g, '@@@@');
        token = encodeURI(token);
        var url = __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].wsURL.substr(0, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].wsURL.lastIndexOf("/"));
        var t = __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].getProperty('wsid') + personId);
        var x = encodeURI(t + '');
        x = x + '';
        return url + '/imageservlet?px=' + personId + '&token=' + token + '&signature=' + x;
    };
    TeamDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-teamdetails',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\myteam\teamdetails.html"*/'<!--\n\n  Generated template for the LeaveHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n      <ion-header>\n\n \n\n        <!-- <ion-toolbar>\n\n          <ion-title>\n\n             Description\n\n      </ion-title>\n\n                <ion-buttons start >\n\n                    <button ion-button (click)="dismiss()" class="close-btn">Close</button>\n\n                  </ion-buttons>\n\n            \n\n                </ion-toolbar> -->\n\n                <ion-navbar hideBackButton=\'false\' >\n\n      \n\n                    <ion-title>My Team</ion-title>\n\n                  </ion-navbar>\n\n          </ion-header>\n\n      <ion-content>\n\n        \n\n      \n\n           \n\n              <ion-avatar item-start>\n\n                <img [hidden]="character.img==null" [src]="sanitized.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+character.img)" class="img-p1"/>\n\n                <!-- <img [hidden]="character.img==null" [src]="geturl(character.person_id)" class="img-p1"/> -->\n\n                <img  [hidden]="character.img!=null" src="assets/imgs/avatar.png"class="img-p1">\n\n              </ion-avatar>\n\n              <div class="name-class">\n\n                  {{character.full_name}}\n\n                </div>\n\n              <!-- <h2>{{character.work_group_name}}</h2> -->\n\n             \n\n              <!-- <h2>{{character.full_name}}</h2> -->\n\n           \n\n           \n\n            <ion-item  [hidden]="character.shift==null" text-wrap>\n\n               Shift\n\n                \n\n              <ion-note item-end>\n\n             {{character.shift}}\n\n                        </ion-note>\n\n            </ion-item>\n\n            <ion-item  [hidden]="character.notes==null" text-wrap>\n\n              Notes\n\n               \n\n             <ion-note item-end>\n\n            {{character.notes}}\n\n                       </ion-note>\n\n           </ion-item>\n\n            <ion-item  [hidden]="character.bulletins==null" text-wrap>\n\n                                                                              \n\n                    Bulletins\n\n                 \n\n                    \n\n                          <ion-note item-end>\n\n                            {{character.bulletins}}\n\n                          </ion-note>\n\n                        </ion-item>\n\n                        <ion-item  [hidden]="character.assginments==null" text-wrap>\n\n                          Assignment\n\n                                      <ion-note item-end>\n\n                                        {{character.assginments}}\n\n                                      </ion-note>\n\n                                    </ion-item>\n\n                                    <ion-item  [hidden]="character.leaves==null" text-wrap>\n\n                                                                                                     \n\n                                         Leaves\n\n                                         \n\n                                        \n\n                                                  <ion-note item-end>\n\n                                                    {{character.leaves}}\n\n                                                  </ion-note>\n\n                                                </ion-item>\n\n                                    <ion-item   [hidden]="character.replacedby==null" text-wrap>\n\n                                                                           \n\n                                     \n\n                                                                \n\n                                            Replaced By\n\n                                         \n\n                                         \n\n                              \n\n                                 \n\n                                                  <ion-note item-end>\n\n                                                \n\n                                                    {{character.replacedby}}\n\n                                                 \n\n                                                  </ion-note>\n\n                                               \n\n                                                </ion-item>\n\n                                                <ion-item  [hidden]="character.relacing==null" text-wrap>\n\n                                                    <ion-note item-start [class]="txt"  >\n\n                                                                \n\n                                                        Replacing\n\n                                                     \n\n                                                      </ion-note>                               \n\n                                           \n\n                                                              \n\n                                                              <ion-note item-end   >\n\n                                                                 \n\n                                                                {{character.relacing}}\n\n                                                             \n\n                                                              </ion-note>\n\n                                                        \n\n                                                            </ion-item>\n\n\n\n       \n\n      </ion-content>\n\n      '/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\myteam\teamdetails.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["s" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */]])
    ], TeamDetailsPage);
    return TeamDetailsPage;
}());

//# sourceMappingURL=teamdetails.js.map

/***/ }),

/***/ 581:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkgroupsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_scheduler_scheduler__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Scheduler_myteam_myteam__ = __webpack_require__(579);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the WorkgroupsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WorkgroupsPage = (function () {
    function WorkgroupsPage(navCtrl, navParams, SchedulerData, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.SchedulerData = SchedulerData;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.fromR = 0;
        this.v_step = 12;
        this.toR = this.v_step;
    }
    WorkgroupsPage.prototype.ionViewDidLoad = function () {
        // this.setBackButtonAction();
        this.getMywkList();
        console.log('ionViewDidLoad MyWorkGroupPage');
    };
    WorkgroupsPage.prototype.getMywkList = function () {
        var _this = this;
        try {
            this.SchedulerData.getworkgroupsList()
                .then(function (data) {
                _this.MywkListAll = data;
                if (_this.MywkListAll) {
                    _this.MywkList = _this.MywkListAll.slice(_this.fromR, _this.toR);
                    console.log("DataLoaded");
                }
            });
            console.log("Test After Loading");
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, error);
        }
    };
    WorkgroupsPage.prototype.doRefresh = function (refresher) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.toR = this.v_step;
                        return [4 /*yield*/, this.getMywkList()];
                    case 1:
                        _a.sent();
                        refresher.complete();
                        return [2 /*return*/];
                }
            });
        });
    };
    WorkgroupsPage.prototype.doInfinite = function (infiniteScroll) {
        // this.fromR=this.fromR+this.v_step;
        this.toR = this.toR + this.v_step;
        try {
            if (this.MywkListAll)
                this.MywkList = (this.MywkListAll.slice(0, this.toR));
            infiniteScroll.complete();
        }
        catch (err) {
        }
        console.log(' infiniteScroll has ended');
    };
    WorkgroupsPage.prototype.wkTapped = function (event, item) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (item == null)
                    return [2 /*return*/];
                try {
                    __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAwkId = item.work_group_id;
                    console.log('workgroupid' + __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAwkId);
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__Scheduler_myteam_myteam__["a" /* MyTeamPage */]);
                }
                catch (err) {
                    __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, "error");
                }
                return [2 /*return*/];
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('pageTopx'),
        __metadata("design:type", Object)
    ], WorkgroupsPage.prototype, "pageTopx", void 0);
    WorkgroupsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-workgroups',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\workgroups\workgroups.html"*/'<!--\n  Generated template for the WorkgroupsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Work Groups</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content #pageTopx>  \n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n      <ion-refresher-content pullingIcon="arrow-dropdown"\n      pullingText="Pull to refresh"\n      refreshingSpinner="circles"\n      refreshingText="Refreshing..."></ion-refresher-content>\n    </ion-refresher>\n\n\n\n      \n  \n   <ion-list  >\n    <button ion-item *ngFor="let item of MywkList" (click)="wkTapped($event, item )" block >\n    \n      \n        <div class="item-note typec" >\n           \n           {{item.work_group_name}}\n          </div>\n\n        \n    </button>\n  </ion-list>\n\n \n\n\n<ion-infinite-scroll (ionInfinite)="doInfinite($event)" > \n  <ion-infinite-scroll-content\n  loadingSpinner="bubbles"\n  loadingText="Loading more data...">\n</ion-infinite-scroll-content> \n</ion-infinite-scroll>\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\workgroups\workgroups.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__providers_scheduler_scheduler__["a" /* SchedulerProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_scheduler_scheduler__["a" /* SchedulerProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */]) === "function" && _e || Object])
    ], WorkgroupsPage);
    return WorkgroupsPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=workgroups.js.map

/***/ }),

/***/ 582:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrgsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_scheduler_scheduler__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Scheduler_publicSpace_publicspace__ = __webpack_require__(577);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the OrgsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OrgsPage = (function () {
    function OrgsPage(navCtrl, navParams, SchedulerData, loadingController, AlertController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.SchedulerData = SchedulerData;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.fromR = 0;
        this.v_step = 12;
        this.toR = this.v_step;
    }
    OrgsPage.prototype.ionViewDidLoad = function () {
        // this.setBackButtonAction();
        this.getMyOrgList();
        console.log('ionViewDidLoad OrganizationPage');
    };
    OrgsPage.prototype.getMyOrgList = function () {
        var _this = this;
        try {
            this.SchedulerData.getMyOrgList()
                .then(function (data) {
                _this.MyOrgListAll = data;
                if (_this.MyOrgListAll) {
                    _this.MyOrgList = _this.MyOrgListAll.slice(_this.fromR, _this.toR);
                    console.log("DataLoaded");
                }
            });
            console.log("Test After Loading");
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, error);
        }
    };
    OrgsPage.prototype.doRefresh = function (refresher) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.toR = this.v_step;
                        return [4 /*yield*/, this.getMyOrgList()];
                    case 1:
                        _a.sent();
                        refresher.complete();
                        return [2 /*return*/];
                }
            });
        });
    };
    OrgsPage.prototype.doInfinite = function (infiniteScroll) {
        // this.fromR=this.fromR+this.v_step;
        this.toR = this.toR + this.v_step;
        try {
            if (this.MyOrgListAll)
                this.MyOrgList = (this.MyOrgListAll.slice(0, this.toR));
            infiniteScroll.complete();
        }
        catch (err) {
        }
        console.log(' infiniteScroll has ended');
    };
    OrgsPage.prototype.orgTapped = function (event, item) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (item == null)
                    return [2 /*return*/];
                try {
                    this.AArole;
                    console.log(item.organization_id);
                    this.AArole = item.role_name;
                    console.log(this.AArole);
                    if (this.AArole.includes('EMP')) {
                        __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAmyspace = 'Y';
                        console.log('showworkspace');
                    }
                    if (this.AArole == 'EMP') {
                        __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAmyteam = 'Y';
                        console.log('showwmyteams');
                    }
                    if (this.AArole != 'EMP') {
                        __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAworkgroups = 'Y';
                        console.log('showworkgroups');
                    }
                    console.log('workgroup' + __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAworkgroups);
                    console.log('myteam' + __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAmyteam);
                    console.log('myspace' + __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAmyspace);
                    __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].sAAOrganizationId = item.organization_id;
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__Scheduler_publicSpace_publicspace__["a" /* PublicSpacePage */]);
                }
                catch (err) {
                    //alert ('dddd');
                    __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, "error");
                }
                return [2 /*return*/];
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('pageTopx'),
        __metadata("design:type", Object)
    ], OrgsPage.prototype, "pageTopx", void 0);
    OrgsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-orgs',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\orgs\orgs.html"*/'<!--\n  Generated template for the OrgsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n   \n  <ion-navbar hideBackButton=\'false\' >\n    \n    <ion-title>Organizations</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n      <ion-refresher-content pullingIcon="arrow-dropdown"\n      pullingText="Pull to refresh"\n      refreshingSpinner="circles"\n      refreshingText="Refreshing..."></ion-refresher-content>\n    </ion-refresher>\n\n    <ion-list  >\n        <button ion-item *ngFor="let item of MyOrgList" (click)="orgTapped($event, item )" block >\n        \n          \n            <div class="item-note typec" >\n                <!-- <ion-icon  name="checkmark-circle-outline" item-start class="ico1"></ion-icon> \n                 {{item.role_name}} - {{item.organization_name}}-->\n                 {{item.orgrole}}\n              </div>\n\n            \n        </button>\n      </ion-list>\n      <ion-infinite-scroll (ionInfinite)="doInfinite($event)" > \n          <ion-infinite-scroll-content\n          loadingSpinner="bubbles"\n          loadingText="Loading more data...">\n        </ion-infinite-scroll-content> \n        </ion-infinite-scroll>\n\n        \n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\Scheduler\orgs\orgs.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__providers_scheduler_scheduler__["a" /* SchedulerProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_scheduler_scheduler__["a" /* SchedulerProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */]) === "function" && _e || Object])
    ], OrgsPage);
    return OrgsPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=orgs.js.map

/***/ }),

/***/ 583:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MorePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_log_in_service_log_in_service__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the MorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MorePage = (function () {
    function MorePage(navCtrl, navParams, appCtrl, platform, statusBar, logProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appCtrl = appCtrl;
        this.platform = platform;
        this.statusBar = statusBar;
        this.logProvider = logProvider;
    }
    MorePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MorePage');
    };
    MorePage.prototype.logOut = function () {
        //  this.navCtrl.push(HomePage).then(() => {
        var _this = this;
        //   this.navCtrl.remove(0,this.navCtrl.length()-1);
        // });
        //alert ('x');
        //this.navCtrl.push(HomePage);
        //this.navCtrl.remove(0,this.navCtrl.length());
        // this.navCtrl.setRoot(HomePage);
        //this.appCtrl.getRootNav().setRoot(HomePage);
        try {
            this.logProvider.logout()
                .then(function (data) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/];
                });
            }); });
        }
        catch (err) {
        }
        this.appCtrl.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
        // if (this.platform.is('ios'))  
        //     {
        //     this.statusBar.overlaysWebView(false);
        //     this.statusBar.show();
        //     }
    };
    MorePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-more',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\more\more.html"*/'<!--\n\n  Generated template for the MorePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>More</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n\n\n    <ion-list  >\n\n\n\n        <button ion-item block disabled=\'true\'>\n\n            <ion-avatar item-start> \n\n              <ion-icon  name="information" item-start class="ico1 ico-color-purple"></ion-icon>\n\n              </ion-avatar>\n\n        <!-- <span class="more-list-text">\n\n              About Easy Access\n\n        </span> -->\n\n        <h2   class="icon_txt" > About APP</h2>\n\n        </button>\n\n\n\n        <button ion-item block class="item-note typec" disabled=\'true\'>\n\n            <ion-avatar item-start> \n\n                  <ion-icon  name="call" item-start class="ico1 ico-color-blue1 "></ion-icon>\n\n                  </ion-avatar>\n\n              \n\n               <h2  > Contact Us</h2>\n\n                \n\n            </button>\n\n\n\n            <button ion-item block class="item-note typec" disabled=\'true\'>\n\n                <ion-avatar item-start> \n\n                      <ion-icon  name="lock" item-start class="ico1 ico-color-green "></ion-icon>\n\n                  </ion-avatar>\n\n                   <h2  >  Privacy Policy</h2>\n\n                    \n\n                </button>\n\n\n\n\n\n            <ion-item block class="item-note typec">\n\n                <ion-avatar item-start> \n\n                      <ion-icon  name="notifications" item-start class="ico1 ico-color-blue2 "></ion-icon>\n\n                     </ion-avatar>\n\n                      \n\n                      <ion-label >Notifications</ion-label>\n\n                      <ion-toggle disabled=\'true\' color="dark"></ion-toggle>\n\n                </ion-item>\n\n\n\n                <ion-item  >\n\n                    <ion-avatar item-start> \n\n                          <ion-icon  name="finger-print" item-start class="ico1 ico-color-orange"></ion-icon>\n\n                     </ion-avatar>\n\n                      \n\n                       <ion-label >Touch ID</ion-label>\n\n                       <ion-toggle  disabled=\'true\' color="dark" item-end></ion-toggle>\n\n                    </ion-item>\n\n\n\n\n\n                    <button ion-item block detail-none disabled=\'true\'>\n\n                        <ion-avatar item-start> \n\n                              <ion-icon  name="md-share" item-start class="ico1 ico-color-blue3 "></ion-icon>\n\n                           </ion-avatar>\n\n                           <h2  > Share App</h2>\n\n                          \n\n                        </button>\n\n\n\n                        <button ion-item block detail-none class="item-note typec" (click)="logOut()">\n\n                            <ion-avatar item-start> \n\n                                  <ion-icon  name="power" item-start class="ico1 ico-color-red "></ion-icon>\n\n                             </ion-avatar>\n\n                              <h2  >  Logout</h2>\n\n                             \n\n                            </button>\n\n\n\n\n\n      </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\more\more.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_4__providers_log_in_service_log_in_service__["a" /* LogInServiceProvider */]])
    ], MorePage);
    return MorePage;
}());

//# sourceMappingURL=more.js.map

/***/ }),

/***/ 584:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocialProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the SocialProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SocialProvider = (function () {
    function SocialProvider(http, httpClient, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
        console.log('Hello SocialProvider Provider');
    }
    SocialProvider.prototype.sendHappiness = function (faceId, comments, applicationId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'social/insertHappiness';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'fcx': '' + faceId + '',
                            'apx': applicationId + ''
                        };
                        if (comments) {
                            methodHeaders['comments'] = encodeURI(comments);
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, null, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SocialProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */]])
    ], SocialProvider);
    return SocialProvider;
}());

//# sourceMappingURL=social.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SshrLeavesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the SshrLeavesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SshrLeavesProvider = (function () {
    function SshrLeavesProvider(http, httpClient, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
    }
    SshrLeavesProvider.prototype.getApprovedLeavesCount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/leavesHistory/getApprovedLeavesCount';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getApprovedLeaves = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/leavesHistory/getApprovedLeaves';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getPendingLeavesCount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/leavesHistory/getPendingLeavesCount';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getPendingLeaves = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/leavesHistory/getPendingLeaves';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getLeaveTypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/newLeave/getLeavesTypes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getLOStypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/newLeave/getLOSTypes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getCLRtypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/newLeave/getCLRTypes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.newLeaveRequest = function (dateStart, dateEnd, absenceAttendanceTypeId, absenceTypeName, status, attribute1, attribute2, attribute3, attribute4, attribute5, attribute6, attribute7, attribute8, attachId, img, replacedPerson) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders, leaveMethodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/newLeave/applyNewLeave';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            'Content-Type': 'application/json ; charset=UTF-8',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
                            'Access-Control-Allow-origin': '*',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
                            'wsid': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'dateStart': dateStart.toString(),
                            'dateEnd': dateEnd.toString(),
                            'absenceAttendanceTypeId': absenceAttendanceTypeId + '',
                            'absenceTypeName': absenceTypeName + '',
                            'status': status,
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        leaveMethodHeaders = {
                            'signature': encodeURI(t + ''),
                            'dateStart': dateStart.toString(),
                            'dateEnd': dateEnd.toString(),
                            'absAttx': absenceAttendanceTypeId + '',
                            'abst': absenceTypeName + '',
                            'status': status
                        };
                        if (attribute1) {
                            methodHeaders = methodHeaders.append('attribute1', attribute1);
                            leaveMethodHeaders['attribute1'] = attribute1;
                        }
                        if (attribute2) {
                            methodHeaders = methodHeaders.append('attribute2', encodeURI(attribute2));
                            leaveMethodHeaders['attribute2'] = encodeURI(attribute2);
                        }
                        if (attribute3) {
                            methodHeaders = methodHeaders.append('attribute3', attribute3);
                            leaveMethodHeaders['attribute3'] = attribute3;
                        }
                        if (attribute4) {
                            methodHeaders = methodHeaders.append('attribute4', attribute4);
                            leaveMethodHeaders['attribute4'] = attribute4;
                        }
                        if (attribute5) {
                            methodHeaders = methodHeaders.append('attribute5', attribute5);
                            leaveMethodHeaders['attribute5'] = attribute5;
                        }
                        if (attribute6) {
                            methodHeaders = methodHeaders.append('attribute6', attribute6);
                            leaveMethodHeaders['attribute6'] = attribute6;
                        }
                        if (attribute7) {
                            methodHeaders = methodHeaders.append('attribute7', attribute7);
                            leaveMethodHeaders['attribute7'] = attribute7;
                        }
                        if (attribute8) {
                            methodHeaders = methodHeaders.append('attribute8', attribute8);
                            leaveMethodHeaders['attribute8'] = attribute8;
                        }
                        if (attachId) {
                            methodHeaders = methodHeaders.append('atx', attachId);
                            leaveMethodHeaders['atx'] = attachId;
                        }
                        if (replacedPerson) {
                            methodHeaders = methodHeaders.append('rp', replacedPerson + '');
                            leaveMethodHeaders['rp'] = replacedPerson + '';
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, leaveMethodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.addAttach = function (attachId, img) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/newLeave/addAttach';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'atx': attachId + '',
                            'fd': img,
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getAnnualLeavesBalance = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/leavesHistory/getAnnualLeavesBalance';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getToilBalance = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/leavesHistory/getToilBalance';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getToilExpire = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/leavesHistory/getTOILExpirePost';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider.prototype.getleaveHistory = function (itemType, itemKey) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'wf/notications/getNotificationHistory';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'filterType': 'HR',
                        };
                        if (itemType) {
                            methodHeaders['itemType'] = itemType;
                        }
                        if (itemKey) {
                            methodHeaders['itemKey'] = itemKey;
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SshrLeavesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */]])
    ], SshrLeavesProvider);
    return SshrLeavesProvider;
}());

//# sourceMappingURL=sshr-leaves.js.map

/***/ }),

/***/ 598:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(599);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_web_animations_js_web_animations_min__ = __webpack_require__(1014);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_web_animations_js_web_animations_min___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_web_animations_js_web_animations_min__);



Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 6:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__ = __webpack_require__(603);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_charts__ = __webpack_require__(641);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(691);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_shared_pages_login_login__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_shared_pages_list_list__ = __webpack_require__(981);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_shared_pages_start_start__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_shared_pages_main_main__ = __webpack_require__(526);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_leaves_leave_history_leave_history__ = __webpack_require__(527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_leaves_leave_details_leave_details__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_leaves_new_leave_new_leave__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_leaves_new_leave_submit_new_leave_submit__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_leaves_leave_balance_leave_balance__ = __webpack_require__(537);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_leaves_leave_home_leave_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_approval_center_work_list_work_list__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_approval_center_notification_details_notification_details__ = __webpack_require__(532);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_hr_request_hr_requests_hr_requests__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_hr_request_letters_preview_letters_preview__ = __webpack_require__(540);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_hr_request_letters_submit_letters_submit__ = __webpack_require__(541);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_hr_request_housing_preview_housing_preview__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_hr_request_housing_submit_housing_submit__ = __webpack_require__(543);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_hr_request_businesscard_preview_businesscard_preview__ = __webpack_require__(544);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_hr_request_businesscard_submit_businesscard_submit__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_payroll_payroll_home_payroll_home__ = __webpack_require__(546);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_payroll_bank_info_bank_info__ = __webpack_require__(549);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_payroll_bank_details_bank_details__ = __webpack_require__(550);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_payroll_payslip_payslip__ = __webpack_require__(547);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_payroll_payslip_details_payslip_details__ = __webpack_require__(548);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_shared_pages_more_more__ = __webpack_require__(583);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_shared_pages_employees_employees__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_approval_center_ntfaction_submit_ntfaction_submit__ = __webpack_require__(531);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_leaves_toil_expire_toil_expire__ = __webpack_require__(539);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_shared_pages_look_up_look_up__ = __webpack_require__(533);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_thank_u_thankscards_home_thankscards_home__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_thank_u_thanks_cards_thanks_cards__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_thank_u_leader_board_leader_board__ = __webpack_require__(536);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_thank_u_new_card_new_card__ = __webpack_require__(535);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_approval_center_delegation_home_delegation_home__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_approval_center_new_delegation_new_delegation__ = __webpack_require__(534);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_payroll_salary_history_salary_history__ = __webpack_require__(551);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile_home_profile_home__ = __webpack_require__(552);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_profile_basic_data_basic_data__ = __webpack_require__(553);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_profile_employment_summary_employment_summary__ = __webpack_require__(554);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_profile_employee_phone_employee_phone__ = __webpack_require__(555);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_profile_employee_adressess_employee_adressess__ = __webpack_require__(556);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_profile_employee_passport_employee_passport__ = __webpack_require__(557);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_profile_employee_visa_employee_visa__ = __webpack_require__(558);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_profile_dependent_tickets_dependent_tickets__ = __webpack_require__(559);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_profile_labor_card_labor_card__ = __webpack_require__(560);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_profile_relationin_mbc_relationin_mbc__ = __webpack_require__(561);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_profile_national_card_national_card__ = __webpack_require__(562);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_dash_board_dash_board_dash_board__ = __webpack_require__(563);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_dash_board_hr_hr_dashboard_hr_dashboard__ = __webpack_require__(564);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_dash_board_scm_scm_dashboard_scm_dashboard__ = __webpack_require__(573);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_dash_board_scm_budget_account_budget_account__ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_dash_board_hr_leave_balance_db_leave_balance_db__ = __webpack_require__(565);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_dash_board_hr_bu_headcount_db_bu_headcount_db__ = __webpack_require__(566);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_dash_board_scm_my_requisition_db_my_requisition_db__ = __webpack_require__(574);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__pages_dash_board_hr_absenc_calendar_db_absenc_calendar_db__ = __webpack_require__(567);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_dash_board_hr_leave_utilzation_db_leave_utilzation_db__ = __webpack_require__(568);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__pages_dash_board_hr_over_time_db_over_time_db__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_dash_board_hr_recruitment_db_recruitment_db__ = __webpack_require__(570);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__pages_dash_board_hr_over_time_db_executive_over_time_db_executive__ = __webpack_require__(571);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__pages_dash_board_hr_extra_time_db_extra_time_db__ = __webpack_require__(572);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__pages_dash_board_scm_approved_po_db_approved_po_db__ = __webpack_require__(575);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__ionic_native_status_bar__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__ionic_native_splash_screen__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__providers_log_in_service_log_in_service__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__ionic_native_fingerprint_aio__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__ionic_storage__ = __webpack_require__(586);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__providers_sshr_leaves_sshr_leaves__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__ionic_native_file__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75__ionic_native_transfer__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__ionic_native_file_path__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_77__ionic_native_camera__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_78__ionic_native_wheel_selector__ = __webpack_require__(530);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_79__ionic_native_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_80__components_fa_icon_fa_icon_component__ = __webpack_require__(982);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_81__providers_work_list_ntf_work_list_ntf__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_82__providers_hr_sit_hr_sit__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_83_ionic_native_http_connection_backend__ = __webpack_require__(983);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_84__providers_payroll_payroll__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_85__angular_common__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_86__providers_social_social__ = __webpack_require__(584);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_87__providers_thanks_card_thanks_card__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_88__components_timeline_timeline__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_89__providers_profile_profile__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_90__providers_dashbaord_dashbaord__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_91__ng_idle_keepalive__ = __webpack_require__(988);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_92__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_93_crypto_js__ = __webpack_require__(990);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_93_crypto_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_93_crypto_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_94__pages_Scheduler_publicSpace_publicspace__ = __webpack_require__(577);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_95__pages_Scheduler_myspace_myspace__ = __webpack_require__(578);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_96__pages_Scheduler_myteam_myteam__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_97__pages_Scheduler_orgs_orgs__ = __webpack_require__(582);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_98__pages_Scheduler_workgroups_workgroups__ = __webpack_require__(581);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_99__pages_Scheduler_myteam_teamdetails__ = __webpack_require__(580);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_100__providers_scheduler_scheduler__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





























































































 // this includes the core NgIdleModule but includes keepalive providers for easy wireup



// AA







var AppModule = (function () {
    function AppModule() {
    }
    AppModule_1 = AppModule;
    AppModule.showLoader = function (loadingController) {
        if (!loadingController)
            return null;
        var loader = loadingController.create({
            spinner: 'hide',
            content: "\n  \n    <div class=\"la-ball-scale-ripple-multiple la-dark la-2x\">\n    <div></div>\n    <div></div>\n    <div></div>\n</div>\n       \n        \n      </div>",
        });
        loader.present();
        return loader;
    };
    AppModule.stopLoader = function (loader) {
        if (loader)
            loader.dismiss();
    };
    AppModule.showMessage = function (alertCtrl, message) {
        if (!alertCtrl)
            return;
        var confirm = alertCtrl.create({
            title: null,
            message: message,
            buttons: [
                {
                    text: 'OK',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    AppModule.showUpdateMessage = function (alertCtrl, message) {
        var confirm = alertCtrl.create({
            title: null,
            message: message,
            buttons: [
                {
                    text: 'Update',
                    handler: function () {
                        window.open("http://10.10.131.26:7003/mobile/index.html", '_system', 'location=yes');
                    }
                }
            ]
        });
        confirm.present();
    };
    AppModule.doRev = function (str) {
        if (str === "")
            return "";
        else
            return this.doRev(str.substr(1)) + str.charAt(0);
    };
    AppModule.getComplex = function (length) {
        var str = "";
        for (; str.length < length; str += Math.random().toString(36).substr(2))
            ;
        return str.substr(0, length);
    };
    AppModule.getProperty = function (propertyName) {
        if (!this.attr || this.attr.length == 0 || !propertyName)
            return;
        var propertyValue = this.attr.find(function (item) { return item.propertyName == propertyName; }).propertyValue;
        return propertyValue;
    };
    AppModule.intializeData = function (data) {
        if (!data || data.length == 0)
            return;
        this.attr = [];
        console.log(data);
        for (var i = 0; i < data.length; i++) {
            this.attr.push({ seq: data[i].sequence_no, propertyName: data[i].property_name, propertyValue: data[i].property_value });
            if (data[i].property_name == 'NTF_NO')
                window.localStorage.setItem('noOfNotifications', data[i].property_value);
            if (data[i].property_name == 'bu')
                this.businessGroup = data[i].property_value;
            if (data[i].property_name == 'ref1') {
                var t = this.doRev(data[i].property_value.substr(0, 32));
                this.attr.push({ seq: data[i].sequence_no, propertyName: 'reference', propertyValue: t });
                this.attr.push({ seq: data[i].sequence_no, propertyName: 'ref', propertyValue: data[i].property_value.substr(32, 48) });
            }
        }
    };
    AppModule.doHash = function (aes256, data) {
        var _this = this;
        if (this.platform == 2) {
            return new Promise(function (resolve) {
                aes256.encrypt(_this.getProperty('reference'), _this.getProperty('ref'), data)
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (error) { return resolve('error'); });
            });
        }
        else {
            var k = __WEBPACK_IMPORTED_MODULE_93_crypto_js__["enc"].Utf8.parse(this.getProperty('reference'));
            var v = __WEBPACK_IMPORTED_MODULE_93_crypto_js__["enc"].Utf8.parse(this.getProperty('ref'));
            var message = data;
            var encrypted = __WEBPACK_IMPORTED_MODULE_93_crypto_js__["AES"].encrypt(message, k, {
                iv: v
            });
            return encrypted;
        }
    };
    AppModule.doHashing = function (data, k, v) {
        return __awaiter(this, void 0, void 0, function () {
            var ks, vs, message, encrypted;
            return __generator(this, function (_a) {
                ks = __WEBPACK_IMPORTED_MODULE_93_crypto_js__["enc"].Utf8.parse(k);
                vs = __WEBPACK_IMPORTED_MODULE_93_crypto_js__["enc"].Utf8.parse(v);
                message = data;
                encrypted = __WEBPACK_IMPORTED_MODULE_93_crypto_js__["AES"].encrypt(message, ks, {
                    iv: vs
                });
                return [2 /*return*/, encrypted];
            });
        });
    };
    /*
    static async getValue(aes256)
    {
    
      let t=  await this.encrypty(aes256 );
      return t;
    }
    */
    AppModule.getAuxiliaryValue = function (data) {
        if (!data)
            return '';
        return data.substr(data.lastIndexOf("/") + 1);
    };
    AppModule.logOut = function (appCtrl) {
        appCtrl.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_shared_pages_login_login__["a" /* LoginPage */]);
    };
    AppModule.handleError = function (err, loader, AlertController, appCtrl) {
        var message = '';
        if (this.mode == 0)
            message = err.message;
        else
            message = err.error;
        console.log(err);
        // alert (message);
        if (loader)
            this.stopLoader(loader);
        if (err.status == '401') {
            AppModule_1.showMessage(AlertController, 'Session Expired');
            this.logOut(appCtrl);
        }
        else if (err.status == '400') {
            AppModule_1.showMessage(AlertController, message);
            this.logOut(appCtrl);
        }
        else
            AppModule_1.showMessage(AlertController, 'No Connection');
    };
    AppModule.tobase64 = function (p) {
        // var b = new Buffer(p);
        // var s = b.toString('base64');
        // return s;
        return null;
    };
    AppModule.getData = function (httpClient, http, ws, methodHeaders, loadingController, AlertController, appCtrl) {
        var _this = this;
        var token = null;
        try {
            token = this.getProperty('wsid');
            if (token)
                token = encodeURI(token);
            else
                token = null;
        }
        catch (error) {
        }
        methodHeaders['Content-Type'] = 'application/json ; charset=UTF-8';
        methodHeaders['Accept'] = 'application/json';
        methodHeaders['Authorization'] = this.authentication + '';
        methodHeaders['oracle-mobile-backend-id'] = this.backendId;
        methodHeaders['resourceName'] = ws;
        //
        //methodHeaders['Access-Control-Allow-Origin']='*';
        //methodHeaders['Origin']='http://localhost:8100';
        //methodHeaders['Access-Control-Allow-Origin']='http://localhost';
        //methodHeaders['Access-Control-Allow-Credentials']='true';
        //methodHeaders['Access-Control-Allow-Methods']='GET, PUT, POST, DELETE, OPTIONS';
        if (token)
            methodHeaders['wsid'] = token;
        var loader = this.showLoader(loadingController);
        var url;
        if (this.isCloud) {
            url = this.wsURL;
        }
        else {
            url = this.wsURL + '/' + ws;
        }
        if (this.mode == 0) {
            var r = void 0;
            return new Promise(function (resolve) {
                httpClient.post(url, null, { headers: new __WEBPACK_IMPORTED_MODULE_70__angular_common_http__["e" /* HttpHeaders */](methodHeaders) }).timeout(30000).subscribe(function (data) {
                    resolve(data);
                    _this.stopLoader(loader);
                }, function (err) {
                    _this.handleError(err, loader, AlertController, appCtrl);
                });
            });
        }
        else {
            http.setSSLCertMode('pinned');
            http.clearCookies();
            return new Promise(function (resolve) {
                http.post(url, {}, methodHeaders).then(function (data) {
                    resolve(JSON.parse(data.data));
                    _this.stopLoader(loader);
                }, function (err) {
                    AppModule_1.handleError(err, loader, AlertController, appCtrl);
                });
            });
        }
    };
    // static wsURL : string ='https://FAEE5CBB8B4248F4BDB7C0B0043D61D9.mobile.ocp.oraclecloud.com:443/mobile/custom/prodapi';
    // static authentication : string ='Basic RkFFRTVDQkI4QjQyNDhGNEJEQjdDMEIwMDQzRDYxRDlfTW9iaWxlQW5vbnltb3VzX0FQUElEOjJiYjcxZDg4LTZiZGItNDZiZi04YzM1LTI2OGFlYzgwNTUyMQ==';
    // static isCloud :boolean=true;
    // static backendId:string ='05fb4316-2c8d-46dc-aafd-3c0ff61a7b61';
    AppModule.wsURL = 'http://10.10.131.34:7003/mbcMobileAAWs/resources';
    AppModule.authentication = 'Basic QUFNb2JVc2VyOkhFMzEzMjAyMA==';
    AppModule.isCloud = false;
    AppModule.backendId = '';
    // End AA Parameters //
    // static wsURL='https://FAEE5CBB8B4248F4BDB7C0B0043D61D9.mobile.ocp.oraclecloud.com:443/mobile/custom/MBCWSAPI';
    // static authentication  ='Basic RkFFRTVDQkI4QjQyNDhGNEJEQjdDMEIwMDQzRDYxRDlfTW9iaWxlQW5vbnltb3VzX0FQUElEOjJiYjcxZDg4LTZiZGItNDZiZi04YzM1LTI2OGFlYzgwNTUyMQ==';
    // static isCloud=true;
    // static backendId='05fb4316-2c8d-46dc-aafd-3c0ff61a7b61';
    // static wsURL='http://10.162.2.4:9073/mbcWebservice1_1/resources';
    // static authentication  ='Basic d3NBY2Nlc3MxOTE4OmZ1a0x1Oj4qa1gtND4zMztnZEd7e3oiNUshMjV1QShFSFFfPlJSTmclZ2c/Yndcenk0LFloVSJiPFtKNyY3OXU3NCROKE5gcThuQ0ozTHN3c3k0RWEuOEUiN3coa3MhI0U1USN2VEUlWDpWUjhuM3JgXmRHLiJnVDVDYU5NfiVM';
    // static isCloud=false;
    // static backendId='';
    // static wsURL='http://10.162.2.4:9073/mbcWebservicePG/resources';
    // static authentication  ='Basic d3NBY2Nlc3MxOTE4OmZ1a0x1Oj4qa1gtND4zMztnZEd7e3oiNUshMjV1QShFSFFfPlJSTmclZ2c/Yndcenk0LFloVSJiPFtKNyY3OXU3NCROKE5gcThuQ0ozTHN3c3k0RWEuOEUiN3coa3MhI0U1USN2VEUlWDpWUjhuM3JgXmRHLiJnVDVDYU5NfiVM';
    // static isCloud=false;
    // static backendId='';
    AppModule.appVersion = '1.0.0';
    AppModule.notificationNO = 0;
    AppModule.mode = 0;
    AppModule.platform = 1;
    AppModule.index = 16;
    AppModule.appUpdate = 1.01;
    AppModule.directorateType = 'NotArabiya';
    AppModule = AppModule_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_shared_pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_shared_pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_shared_pages_start_start__["a" /* StartPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_shared_pages_main_main__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_leaves_leave_history_leave_history__["a" /* LeaveHistoryPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_leaves_leave_details_leave_details__["a" /* LeaveDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_leaves_new_leave_new_leave__["a" /* NewLeavePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_leaves_new_leave_submit_new_leave_submit__["a" /* NewLeaveSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_leaves_leave_balance_leave_balance__["a" /* LeaveBalancePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_leaves_leave_home_leave_home__["a" /* LeaveHomePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_approval_center_work_list_work_list__["a" /* WorkListPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_approval_center_notification_details_notification_details__["a" /* NotificationDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_hr_request_hr_requests_hr_requests__["a" /* HrRequestsPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_hr_request_letters_preview_letters_preview__["a" /* LettersPreviewPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_hr_request_letters_submit_letters_submit__["a" /* LettersSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_hr_request_housing_preview_housing_preview__["a" /* HousingPreviewPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_hr_request_housing_submit_housing_submit__["a" /* HousingSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_hr_request_businesscard_preview_businesscard_preview__["a" /* BusinesscardPreviewPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_hr_request_businesscard_submit_businesscard_submit__["a" /* BusinesscardSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_payroll_payroll_home_payroll_home__["a" /* PayrollHomePage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_payroll_bank_info_bank_info__["a" /* BankInfoPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_payroll_bank_details_bank_details__["a" /* BankDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_payroll_payslip_payslip__["a" /* PayslipPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_payroll_payslip_details_payslip_details__["a" /* PayslipDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_shared_pages_more_more__["a" /* MorePage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_shared_pages_employees_employees__["a" /* EmployeesPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_approval_center_ntfaction_submit_ntfaction_submit__["a" /* NtfactionSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_leaves_toil_expire_toil_expire__["a" /* ToilExpirePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_shared_pages_look_up_look_up__["a" /* LookUpPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_thank_u_thankscards_home_thankscards_home__["a" /* ThankscardsHomePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_thank_u_thanks_cards_thanks_cards__["a" /* ThanksCardsPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_thank_u_leader_board_leader_board__["a" /* LeaderBoardPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_thank_u_new_card_new_card__["a" /* NewCardPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_approval_center_delegation_home_delegation_home__["a" /* DelegationHomePage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_approval_center_new_delegation_new_delegation__["a" /* NewDelegationPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_payroll_salary_history_salary_history__["a" /* SalaryHistoryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile_home_profile_home__["a" /* ProfileHomePage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_profile_basic_data_basic_data__["a" /* BasicDataPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_profile_employment_summary_employment_summary__["a" /* EmploymentSummaryPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_profile_employee_phone_employee_phone__["a" /* EmployeePhonePage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_profile_employee_adressess_employee_adressess__["a" /* EmployeeAdressessPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_profile_employee_passport_employee_passport__["a" /* EmployeePassportPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_profile_employee_visa_employee_visa__["a" /* EmployeeVisaPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_profile_dependent_tickets_dependent_tickets__["a" /* DependentTicketsPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_profile_labor_card_labor_card__["a" /* LaborCardPage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_profile_relationin_mbc_relationin_mbc__["a" /* RelationinMbcPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_profile_national_card_national_card__["a" /* NationalCardPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_dash_board_dash_board_dash_board__["a" /* DashBoardPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_dash_board_hr_hr_dashboard_hr_dashboard__["a" /* HrDashboardPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_dash_board_scm_scm_dashboard_scm_dashboard__["a" /* ScmDashboardPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_dash_board_scm_budget_account_budget_account__["a" /* BudgetAccountPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_dash_board_hr_leave_balance_db_leave_balance_db__["a" /* LeaveBalanceDbPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_dash_board_hr_bu_headcount_db_bu_headcount_db__["a" /* BuHeadcountDbPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_dash_board_scm_my_requisition_db_my_requisition_db__["a" /* MyRequisitionDbPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_dash_board_hr_absenc_calendar_db_absenc_calendar_db__["a" /* AbsencCalendarDbPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_dash_board_hr_leave_utilzation_db_leave_utilzation_db__["a" /* LeaveUtilzationDbPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_dash_board_hr_over_time_db_over_time_db__["a" /* OverTimeDbPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_dash_board_hr_recruitment_db_recruitment_db__["a" /* RecruitmentDbPage */],
                __WEBPACK_IMPORTED_MODULE_64__pages_dash_board_hr_over_time_db_executive_over_time_db_executive__["a" /* OverTimeDbExecutivePage */],
                __WEBPACK_IMPORTED_MODULE_65__pages_dash_board_hr_extra_time_db_extra_time_db__["a" /* ExtraTimeDbPage */],
                __WEBPACK_IMPORTED_MODULE_66__pages_dash_board_scm_approved_po_db_approved_po_db__["a" /* ApprovedPoDbPage */],
                //aa
                __WEBPACK_IMPORTED_MODULE_94__pages_Scheduler_publicSpace_publicspace__["a" /* PublicSpacePage */],
                __WEBPACK_IMPORTED_MODULE_95__pages_Scheduler_myspace_myspace__["a" /* MySpacePage */],
                __WEBPACK_IMPORTED_MODULE_96__pages_Scheduler_myteam_myteam__["a" /* MyTeamPage */],
                __WEBPACK_IMPORTED_MODULE_97__pages_Scheduler_orgs_orgs__["a" /* OrgsPage */],
                __WEBPACK_IMPORTED_MODULE_98__pages_Scheduler_workgroups_workgroups__["a" /* WorkgroupsPage */],
                __WEBPACK_IMPORTED_MODULE_99__pages_Scheduler_myteam_teamdetails__["a" /* TeamDetailsPage */],
                //
                __WEBPACK_IMPORTED_MODULE_88__components_timeline_timeline__["a" /* TimelineComponent */],
                __WEBPACK_IMPORTED_MODULE_88__components_timeline_timeline__["b" /* TimelineItemComponent */],
                __WEBPACK_IMPORTED_MODULE_88__components_timeline_timeline__["c" /* TimelineTimeComponent */],
                __WEBPACK_IMPORTED_MODULE_80__components_fa_icon_fa_icon_component__["a" /* FaIconComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_70__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_72__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4_ng2_charts__["ChartsModule"],
                //FormsModule,
                //HttpModule,
                //MomentModule,
                //NgIdleKeepaliveModule.forRoot()
                __WEBPACK_IMPORTED_MODULE_91__ng_idle_keepalive__["a" /* NgIdleKeepaliveModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_shared_pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_shared_pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_shared_pages_start_start__["a" /* StartPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_shared_pages_main_main__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_leaves_leave_history_leave_history__["a" /* LeaveHistoryPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_leaves_leave_details_leave_details__["a" /* LeaveDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_leaves_new_leave_new_leave__["a" /* NewLeavePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_leaves_new_leave_submit_new_leave_submit__["a" /* NewLeaveSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_leaves_leave_balance_leave_balance__["a" /* LeaveBalancePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_approval_center_work_list_work_list__["a" /* WorkListPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_leaves_leave_home_leave_home__["a" /* LeaveHomePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_approval_center_notification_details_notification_details__["a" /* NotificationDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_hr_request_hr_requests_hr_requests__["a" /* HrRequestsPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_hr_request_letters_preview_letters_preview__["a" /* LettersPreviewPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_hr_request_letters_submit_letters_submit__["a" /* LettersSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_hr_request_housing_preview_housing_preview__["a" /* HousingPreviewPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_hr_request_housing_submit_housing_submit__["a" /* HousingSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_hr_request_businesscard_preview_businesscard_preview__["a" /* BusinesscardPreviewPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_hr_request_businesscard_submit_businesscard_submit__["a" /* BusinesscardSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_payroll_payroll_home_payroll_home__["a" /* PayrollHomePage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_payroll_bank_info_bank_info__["a" /* BankInfoPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_payroll_bank_details_bank_details__["a" /* BankDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_payroll_payslip_payslip__["a" /* PayslipPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_payroll_payslip_details_payslip_details__["a" /* PayslipDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_shared_pages_more_more__["a" /* MorePage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_shared_pages_employees_employees__["a" /* EmployeesPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_approval_center_ntfaction_submit_ntfaction_submit__["a" /* NtfactionSubmitPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_leaves_toil_expire_toil_expire__["a" /* ToilExpirePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_shared_pages_look_up_look_up__["a" /* LookUpPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_thank_u_thankscards_home_thankscards_home__["a" /* ThankscardsHomePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_thank_u_thanks_cards_thanks_cards__["a" /* ThanksCardsPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_thank_u_leader_board_leader_board__["a" /* LeaderBoardPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_thank_u_new_card_new_card__["a" /* NewCardPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_approval_center_delegation_home_delegation_home__["a" /* DelegationHomePage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_approval_center_new_delegation_new_delegation__["a" /* NewDelegationPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_payroll_salary_history_salary_history__["a" /* SalaryHistoryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile_home_profile_home__["a" /* ProfileHomePage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_profile_basic_data_basic_data__["a" /* BasicDataPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_profile_employment_summary_employment_summary__["a" /* EmploymentSummaryPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_profile_employee_phone_employee_phone__["a" /* EmployeePhonePage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_profile_employee_adressess_employee_adressess__["a" /* EmployeeAdressessPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_profile_employee_passport_employee_passport__["a" /* EmployeePassportPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_profile_employee_visa_employee_visa__["a" /* EmployeeVisaPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_profile_dependent_tickets_dependent_tickets__["a" /* DependentTicketsPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_profile_labor_card_labor_card__["a" /* LaborCardPage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_profile_relationin_mbc_relationin_mbc__["a" /* RelationinMbcPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_profile_national_card_national_card__["a" /* NationalCardPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_dash_board_dash_board_dash_board__["a" /* DashBoardPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_dash_board_hr_hr_dashboard_hr_dashboard__["a" /* HrDashboardPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_dash_board_scm_scm_dashboard_scm_dashboard__["a" /* ScmDashboardPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_dash_board_scm_budget_account_budget_account__["a" /* BudgetAccountPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_dash_board_hr_leave_balance_db_leave_balance_db__["a" /* LeaveBalanceDbPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_dash_board_hr_bu_headcount_db_bu_headcount_db__["a" /* BuHeadcountDbPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_dash_board_scm_my_requisition_db_my_requisition_db__["a" /* MyRequisitionDbPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_dash_board_hr_absenc_calendar_db_absenc_calendar_db__["a" /* AbsencCalendarDbPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_dash_board_hr_leave_utilzation_db_leave_utilzation_db__["a" /* LeaveUtilzationDbPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_dash_board_hr_over_time_db_over_time_db__["a" /* OverTimeDbPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_dash_board_hr_recruitment_db_recruitment_db__["a" /* RecruitmentDbPage */],
                __WEBPACK_IMPORTED_MODULE_64__pages_dash_board_hr_over_time_db_executive_over_time_db_executive__["a" /* OverTimeDbExecutivePage */],
                __WEBPACK_IMPORTED_MODULE_65__pages_dash_board_hr_extra_time_db_extra_time_db__["a" /* ExtraTimeDbPage */],
                __WEBPACK_IMPORTED_MODULE_66__pages_dash_board_scm_approved_po_db_approved_po_db__["a" /* ApprovedPoDbPage */],
                //
                __WEBPACK_IMPORTED_MODULE_94__pages_Scheduler_publicSpace_publicspace__["a" /* PublicSpacePage */],
                __WEBPACK_IMPORTED_MODULE_95__pages_Scheduler_myspace_myspace__["a" /* MySpacePage */],
                __WEBPACK_IMPORTED_MODULE_96__pages_Scheduler_myteam_myteam__["a" /* MyTeamPage */],
                __WEBPACK_IMPORTED_MODULE_97__pages_Scheduler_orgs_orgs__["a" /* OrgsPage */],
                __WEBPACK_IMPORTED_MODULE_98__pages_Scheduler_workgroups_workgroups__["a" /* WorkgroupsPage */],
                __WEBPACK_IMPORTED_MODULE_99__pages_Scheduler_myteam_teamdetails__["a" /* TeamDetailsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_67__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_68__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_74__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_75__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_77__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_76__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_78__ionic_native_wheel_selector__["a" /* WheelSelector */],
                __WEBPACK_IMPORTED_MODULE_69__providers_log_in_service_log_in_service__["a" /* LogInServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_71__ionic_native_fingerprint_aio__["a" /* FingerprintAIO */],
                __WEBPACK_IMPORTED_MODULE_85__angular_common__["d" /* DecimalPipe */],
                __WEBPACK_IMPORTED_MODULE_73__providers_sshr_leaves_sshr_leaves__["a" /* SshrLeavesProvider */],
                __WEBPACK_IMPORTED_MODULE_81__providers_work_list_ntf_work_list_ntf__["a" /* WorkListNtfProvider */],
                __WEBPACK_IMPORTED_MODULE_82__providers_hr_sit_hr_sit__["a" /* HrSitProvider */],
                __WEBPACK_IMPORTED_MODULE_79__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_83_ionic_native_http_connection_backend__["c" /* NativeHttpModule */], __WEBPACK_IMPORTED_MODULE_83_ionic_native_http_connection_backend__["a" /* NativeHttpBackend */], __WEBPACK_IMPORTED_MODULE_83_ionic_native_http_connection_backend__["b" /* NativeHttpFallback */],
                __WEBPACK_IMPORTED_MODULE_84__providers_payroll_payroll__["a" /* PayrollProvider */],
                __WEBPACK_IMPORTED_MODULE_86__providers_social_social__["a" /* SocialProvider */],
                __WEBPACK_IMPORTED_MODULE_87__providers_thanks_card_thanks_card__["a" /* ThanksCardProvider */],
                __WEBPACK_IMPORTED_MODULE_89__providers_profile_profile__["a" /* ProfileProvider */],
                __WEBPACK_IMPORTED_MODULE_90__providers_dashbaord_dashbaord__["a" /* DashbaordProvider */],
                __WEBPACK_IMPORTED_MODULE_92__ionic_native_aes_256__["a" /* AES256 */],
                //AA
                __WEBPACK_IMPORTED_MODULE_100__providers_scheduler_scheduler__["a" /* SchedulerProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
    var AppModule_1;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HrSitProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the HrSitProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var HrSitProvider = (function () {
    function HrSitProvider(http, httpClient, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
    }
    HrSitProvider.prototype.getLettersTypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/getLettersTypes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'bu': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('bu') + ''
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.getSalaryUpdateTypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/getTecomSalatUpdateTypes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.getSalaryUpdateTypesxx = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var loader, url, ws, t, methodHeaders, xx, methodHeadersxx;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
                        ws = 'sshr/sit/getTecomSalatUpdateTypes';
                        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
                            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
                        }
                        else
                            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/' + ws;
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            'Content-Type': 'application/json ; charset=UTF-8',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
                            'Access-Control-Allow-origin': '*',
                            'wsid': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        xx = {
                            'Content-Type': 'application/json ; charset=UTF-8',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
                            'Access-Control-Allow-origin': '*',
                            'wsid': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
                        };
                        xx['resourceName'] = ws;
                        methodHeadersxx = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */](xx);
                        return [2 /*return*/, new Promise(function (resolve) {
                                _this.httpClient.post(url, null, { headers: methodHeadersxx }).timeout(5000).subscribe(function (data) {
                                    resolve(data);
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].handleError(err, loader, _this.AlertController, _this.appCtrl);
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.getHousingRequestTypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/getHousingRequestypes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.getBusinessCardTypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/getCardTypes';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.getBusinessCardLanguages = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/getCardLanguages';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.getBusinessCardDefaultData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/getCardTDefualtData';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.submitLetter = function (status, letterType, cbt, travleDestination, salaryTecome, letterLine1, letterLine2, cb) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, UAEmethodHeaders, UAEletterMethodHeaders, KSAmethodHeaders, KSAletterMethodHeaders, OthersMethodHeaders, OtherletterMethodHeaders, methodHeaders, lettermethodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/applySITRequestPost';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        UAEmethodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Content-Type',
                            'wsid': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
                            'sitTitle': '' + 'Request Letters from HR',
                            'status': '' + status,
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        UAEletterMethodHeaders = {
                            'signature': encodeURI(t + ''),
                            'sitTitle': '' + 'Request Letters from HR',
                            'status': '' + status
                        };
                        if (letterType) {
                            UAEmethodHeaders = UAEmethodHeaders.append('segment1', letterType);
                            UAEletterMethodHeaders['segment1'] = letterType;
                        }
                        if (cbt) {
                            UAEmethodHeaders = UAEmethodHeaders.append('segment12', encodeURI(cbt));
                            UAEletterMethodHeaders['segment12'] = encodeURI(cbt);
                        }
                        if (travleDestination) {
                            UAEmethodHeaders = UAEmethodHeaders.append('segment11', encodeURI(travleDestination));
                            UAEletterMethodHeaders['segment11'] = encodeURI(travleDestination);
                        }
                        if (salaryTecome) {
                            UAEmethodHeaders = UAEmethodHeaders.append('segment13', salaryTecome);
                            UAEletterMethodHeaders['segment13'] = salaryTecome;
                        }
                        KSAmethodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Content-Type',
                            'wsid': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
                            'sitTitle': '' + 'Request Letters from Saudi HR',
                            'status': '' + status,
                            'segment1': '' + letterType,
                            'segment2': '' + encodeURI(letterLine1),
                            'segment3': '' + encodeURI(letterLine2),
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        KSAletterMethodHeaders = {
                            'signature': encodeURI(t + ''),
                            'sitTitle': '' + 'Request Letters from Saudi HR',
                            'status': '' + status,
                        };
                        if (letterType) {
                            KSAmethodHeaders = KSAmethodHeaders.append('segment1', letterType);
                            KSAletterMethodHeaders['segment1'] = letterType;
                        }
                        if (letterLine1) {
                            KSAmethodHeaders = KSAmethodHeaders.append('segment2', encodeURI(letterLine1));
                            KSAletterMethodHeaders['segment2'] = encodeURI(letterLine1);
                        }
                        if (letterLine2) {
                            KSAmethodHeaders = KSAmethodHeaders.append('segment3', encodeURI(letterLine2));
                            KSAletterMethodHeaders['segment3'] = encodeURI(letterLine2);
                        }
                        OthersMethodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                            'Access-Control-Allow-Headers': 'Content-Type',
                            'wsid': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
                            'sitTitle': '' + 'Egypt Letter Request',
                            'status': '' + status,
                            'segment1': '' + letterType,
                            'segment2': '' + encodeURI(cb),
                            'segment3': '' + encodeURI(travleDestination),
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        OtherletterMethodHeaders = {
                            'signature': encodeURI(t + ''),
                            'sitTitle': '' + 'Egypt Letter Request',
                            'status': '' + status,
                        };
                        if (letterType) {
                            OthersMethodHeaders = OthersMethodHeaders.append('segment1', letterType);
                            OtherletterMethodHeaders['segment1'] = letterType;
                        }
                        if (cb) {
                            OthersMethodHeaders = OthersMethodHeaders.append('segment2', encodeURI(cb));
                            OtherletterMethodHeaders['segment2'] = encodeURI(cb);
                        }
                        if (travleDestination) {
                            OthersMethodHeaders = OthersMethodHeaders.append('segment3', encodeURI(travleDestination));
                            OtherletterMethodHeaders['segment3'] = encodeURI(travleDestination);
                        }
                        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('bu') == '81') {
                            methodHeaders = UAEmethodHeaders;
                            lettermethodHeaders = UAEletterMethodHeaders;
                        }
                        else if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('bu') == '998') {
                            methodHeaders = KSAmethodHeaders;
                            lettermethodHeaders = KSAletterMethodHeaders;
                        }
                        else {
                            methodHeaders = OthersMethodHeaders;
                            lettermethodHeaders = OtherletterMethodHeaders;
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, lettermethodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.submitHousing = function (status, RequestType) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/applySITRequestPost';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'sitTitle': '' + 'Housing Advance Request',
                            'status': '' + status,
                            'segment1': '' + RequestType
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider.prototype.submitBusinessCard = function (status, CardType, Languages, englishName, arbicName, enJobTitle, arjobTitle, officePhone, ext, officeFax, mobile, email) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders, cardmethodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'sshr/sit/applySITRequestPost';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
                            // 'Content-Type':'application/json; charset=utf-8',
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                            'Accept': 'application/json; charset=utf-8',
                            'Access-Control-Allow-Headers': 'Content-Type',
                            'wsid': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid'),
                            'signature': t + '',
                            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
                            'sitTitle': '' + 'Business Card',
                            'status': '' + status,
                            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
                            'resourceName': ws
                        });
                        cardmethodHeaders = {
                            'signature': encodeURI(t + ''),
                            'sitTitle': '' + 'Business Card',
                            'status': '' + status,
                        };
                        if (CardType) {
                            methodHeaders = methodHeaders.append('segment12', CardType);
                            cardmethodHeaders['segment12'] = CardType;
                        }
                        if (Languages) {
                            methodHeaders = methodHeaders.append('segment13', Languages);
                            cardmethodHeaders['segment13'] = Languages;
                        }
                        if (englishName) {
                            methodHeaders = methodHeaders.append('segment1', encodeURI(englishName));
                            cardmethodHeaders['segment1'] = encodeURI(englishName);
                        }
                        if (arbicName) {
                            methodHeaders = methodHeaders.append('segment2', encodeURI(arbicName));
                            cardmethodHeaders['segment2'] = encodeURI(arbicName);
                        }
                        if (enJobTitle) {
                            methodHeaders = methodHeaders.append('segment3', encodeURI(enJobTitle));
                            cardmethodHeaders['segment3'] = encodeURI(enJobTitle);
                        }
                        if (arjobTitle) {
                            methodHeaders = methodHeaders.append('segment4', encodeURI(arjobTitle));
                            cardmethodHeaders['segment4'] = encodeURI(arjobTitle);
                        }
                        if (officePhone) {
                            methodHeaders = methodHeaders.append('segment7', officePhone);
                            cardmethodHeaders['segment7'] = officePhone;
                        }
                        if (ext) {
                            methodHeaders = methodHeaders.append('segment6', ext);
                            cardmethodHeaders['segment6'] = ext;
                        }
                        if (officeFax) {
                            methodHeaders = methodHeaders.append('segment8', officeFax);
                            cardmethodHeaders['segment8'] = officeFax;
                        }
                        if (mobile) {
                            methodHeaders = methodHeaders.append('segment9', mobile);
                            cardmethodHeaders['segment9'] = mobile;
                        }
                        if (email) {
                            methodHeaders = methodHeaders.append('segment10', email);
                            cardmethodHeaders['segment10'] = email;
                        }
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, cardmethodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    HrSitProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */]])
    ], HrSitProvider);
    return HrSitProvider;
}());

//# sourceMappingURL=hr-sit.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashbaordProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the DashbaordProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DashbaordProvider = (function () {
    function DashbaordProvider(http, httpClient, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
        console.log('Hello DashbaordProvider Provider');
    }
    // HR
    DashbaordProvider.prototype.getEmployeeCountBUDirectorate = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'dash/board/getEmployeeCountBUDirectorate';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'directorateType': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('isArabyia')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    DashbaordProvider.prototype.getEmployeeHeadcountGender = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'dash/board/getEmployeeHeadcountGender';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'directorateType': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('isArabyia')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    DashbaordProvider.prototype.getEmployeeCountPersonTypeDirectorate = function (businessGroup) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'dash/board/getEmployeeCountPersonTypeDirectorate';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'directorateType': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('isArabyia'),
                            'businessGroup': '' + businessGroup + ''
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    DashbaordProvider.prototype.getOverTimeMonthsDirectorateType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'dash/board/getOverTimeMonthsDirectorateType';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'directorateType': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('isArabyia')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    DashbaordProvider.prototype.getOverTimeMonthDepartments = function (year, month) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'dash/board/getOverTimeMonthDepartments';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'directorateType': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('isArabyia'),
                            'year': '' + year + '',
                            'monthNo': month + '',
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    DashbaordProvider.prototype.getExtraTimeWorkedBGDierctorateType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'dash/board/getExtraTimeWorkedBGDierctorateType';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'directorateType': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('isArabyia')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    DashbaordProvider.prototype.getExtraTimeWorkedDepartmentDierctorateType = function (businessGroup) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'dash/board/getExtraTimeWorkedDepartmentDierctorateType';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'directorateType': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('isArabyia'),
                            'businessGroup': '' + businessGroup + '',
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    /////////////////////////
    /*
      getBuHeadCount ()
      {
        let loader=AppModule.showLoader(this.loadingController );
    
        let url;
        if (AppModule.isCloud)
        {
          url=AppModule.wsURL;
        }
        else url =AppModule.wsURL+'/dash/board/getBUheadCount';
       
        let methodHeaders = new HttpHeaders({
           'Content-Type':'application/json ; charset=UTF-8' ,
         'Accept':'application/json',
         'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
         'Access-Control-Allow-origin':'*',
         'Authorization': AppModule.authentication,
         'oracle-mobile-backend-id':AppModule.backendId,
          'resourceName' :'dash/board/getBUheadCount'
       
       });
       
       return new Promise(resolve => {
         this.http.post(url, null, { headers: methodHeaders }).timeout(15000).subscribe(data => {
            resolve(data);
            AppModule.stopLoader(loader);
          }
          , err => {
         AppModule.stopLoader(loader);
           AppModule.showMessage(this.AlertController,'No Connection')
       
          }
        );
        });
    
      }
    
      getDirectorateHeadCount (businessGroup: string)
      {
        let loader=AppModule.showLoader(this.loadingController );
    
        let url;
        if (AppModule.isCloud)
        {
          url=AppModule.wsURL;
        }
        else url =AppModule.wsURL+'/dash/board/getDiectorateHeadCount';
       
        let methodHeaders = new HttpHeaders({
           'Content-Type':'application/json ; charset=UTF-8' ,
         'Accept':'application/json',
         'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
         'Access-Control-Allow-origin':'*',
         'businessGroup':''+businessGroup+'',
         'Authorization': AppModule.authentication,
         'oracle-mobile-backend-id':AppModule.backendId,
          'resourceName' :'dash/board/getDiectorateHeadCount'
       
       });
       
       return new Promise(resolve => {
         this.http.post(url, null, { headers: methodHeaders }).timeout(15000).subscribe(data => {
            resolve(data);
            AppModule.stopLoader(loader);
          }
          , err => {
         AppModule.stopLoader(loader);
           AppModule.showMessage(this.AlertController,'No Connection')
       
          }
        );
        });
    
      }
    
    
    
      getOverTimeMonths ()
      {
        let loader=AppModule.showLoader(this.loadingController );
    
        let url;
        if (AppModule.isCloud)
        {
          url=AppModule.wsURL;
        }
        else url =AppModule.wsURL+'/dash/board/getOverTimeMonths';
       
        let methodHeaders = new HttpHeaders({
           'Content-Type':'application/json ; charset=UTF-8' ,
         'Accept':'application/json',
         'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
         'Access-Control-Allow-origin':'*',
         'Authorization': AppModule.authentication,
         'oracle-mobile-backend-id':AppModule.backendId,
          'resourceName' :'dash/board/getOverTimeMonths'
       
       });
       
       return new Promise(resolve => {
         this.http.post(url, null, { headers: methodHeaders }).timeout(150000).subscribe(data => {
            resolve(data);
            AppModule.stopLoader(loader);
          }
          , err => {
         AppModule.stopLoader(loader);
           AppModule.showMessage(this.AlertController,'No Connection')
       
          }
        );
        });
    
      }
    
    
      getOverTimeMonthDirctorates (month_no: string)
      {
        let loader=AppModule.showLoader(this.loadingController );
    
        let url;
        if (AppModule.isCloud)
        {
          url=AppModule.wsURL;
        }
        else url =AppModule.wsURL+'/dash/board/getOverTimeMonthDirctorates';
       
        let methodHeaders = new HttpHeaders({
           'Content-Type':'application/json ; charset=UTF-8' ,
         'Accept':'application/json',
         'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
         'Access-Control-Allow-origin':'*',
         'attribute1':''+month_no+'',
         'Authorization': AppModule.authentication,
         'oracle-mobile-backend-id':AppModule.backendId,
          'resourceName' :'dash/board/getOverTimeMonthDirctorates'
       
       });
       
       return new Promise(resolve => {
         this.http.post(url, null, { headers: methodHeaders }).timeout(150000).subscribe(data => {
            resolve(data);
            AppModule.stopLoader(loader);
          }
          , err => {
         AppModule.stopLoader(loader);
           AppModule.showMessage(this.AlertController,'No Connection')
       
          }
        );
        });
    
      }
    
    */
    /////scm
    DashbaordProvider.prototype.getApprovedPo = function () {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/dash/board/getApprovedPo';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'directorateType': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].directorateType,
            'resourceName': 'dash/board/getApprovedPo'
        });
        return new Promise(function (resolve) {
            _this.httpClient.post(url, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    DashbaordProvider.prototype.getOperatinUnitBudgetPerAccount = function (organizationId) {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/dash/board/getOperatinUnitBudgetPerAccount';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'organizationId': organizationId + '',
            'resourceName': 'dash/board/getOperatinUnitBudgetPerAccount'
        });
        return new Promise(function (resolve) {
            _this.httpClient.post(url, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                console.log('data88');
                console.log(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    DashbaordProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */]])
    ], DashbaordProvider);
    return DashbaordProvider;
}());

//# sourceMappingURL=dashbaord.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_work_list_ntf_work_list_ntf__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__approval_center_ntfaction_submit_ntfaction_submit__ = __webpack_require__(531);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__thank_u_new_card_new_card__ = __webpack_require__(535);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the NotificationActionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EmployeesPage = (function () {
    function EmployeesPage(navCtrl, navParams, ntfData, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ntfData = ntfData;
        this.viewCtrl = viewCtrl;
        this.fromR = 0;
        this.v_step = 10;
        this.toR = this.v_step;
        this.hr = '';
        this.overtime = '';
        this.expenses = '';
        this.requisitions = '';
        this.po = '';
        this.others = '';
        this.title = this.navParams.get('title');
        this.callingType = this.navParams.get('callingType');
        this.notificationId = this.navParams.get('notificationId');
        this.hr = this.navParams.get('hr');
        this.overtime = this.navParams.get('overtime');
        this.expenses = this.navParams.get('expenses');
        this.requisitions = this.navParams.get('requisitions');
        this.po = this.navParams.get('po');
        this.others = this.navParams.get('others');
        this.getUsres();
    }
    EmployeesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificationActionsPage');
    };
    EmployeesPage.prototype.getUsres = function () {
        var _this = this;
        try {
            this.ntfData.getUsers()
                .then(function (data) {
                _this.allUsers = data;
                _this.users = _this.allUsers.slice(_this.fromR, _this.toR);
                //this.users.concat(this.allUsers.slice(this.fromR,this.toR));//= this.allUsers.slice(this.fromR,this.toR);
            });
        }
        catch (error) {
            //this.showToast('middle','Connection Problem');
            alert(error);
        }
    };
    EmployeesPage.prototype.onInput = function (ev) {
        if (!this.users) {
            return;
        }
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.filteredUsers = this.allUsers.filter(function (item) {
                return ((item.g + item.j + item.m).toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            if (this.filteredUsers) {
                this.users = this.filteredUsers.slice(0, this.v_step);
            }
        }
        else {
            this.fromR = 0;
            this.toR = this.v_step;
            this.users = this.allUsers.slice(0, this.v_step);
        }
    };
    EmployeesPage.prototype.doInfinite = function (infiniteScroll) {
        // this.fromR=this.fromR+this.v_step;
        this.toR = this.toR + this.v_step;
        try {
            if (this.filteredUsers) {
                this.users = (this.filteredUsers.slice(0, this.toR));
            }
            else {
                this.users = (this.allUsers.slice(0, this.toR));
            }
            infiniteScroll.complete();
        }
        catch (err) {
        }
    };
    EmployeesPage.prototype.userTapped = function (event, user) {
        if (this.callingType == 1) {
            var data = { user: user };
            this.viewCtrl.dismiss(data);
        }
        else if (this.callingType == 3) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__thank_u_new_card_new_card__["a" /* NewCardPage */], { user: user });
        }
        else
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__approval_center_ntfaction_submit_ntfaction_submit__["a" /* NtfactionSubmitPage */], { title: this.title, user: user, notificationId: this.notificationId, hr: this.hr,
                overtime: this.overtime,
                expenses: this.expenses,
                requisitions: this.requisitions,
                po: this.po,
                others: this.others });
    };
    EmployeesPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    EmployeesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-employees',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\employees\employees.html"*/'<!--\n\n  Generated template for the NotificationActionsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n \n\n  <ion-navbar>\n\n      <ion-buttons start *ngIf="callingType==1">\n\n          <button ion-button (click)="dismiss()" class="close-btn">Close</button>\n\n        </ion-buttons>\n\n    <ion-title>{{title}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  \n\n              \n\n                <ion-searchbar  \n\n                [(ngModel)]="searchItem"\n\n                [showCancelButton]="shouldShowCancel"\n\n                animated="true"\n\n                cancelButtonText="Cancel"\n\n                showCancelButton="true"\n\n                placeholder="Search"\n\n                (ionInput)="onInput($event)"\n\n                (ionCancel)="onCancel($event)">\n\n                </ion-searchbar>\n\n        \n\n         \n\n\n\n  <ion-list >\n\n\n\n      <button ion-item *ngFor="let user of users;   let i = index "  detail-none  (click)="userTapped($event, user )" >\n\n      \n\n     \n\n          <ion-avatar item-start> \n\n          \n\n            <div class="{{user.class_name}}"   >  {{user.intials}} </div>\n\n           </ion-avatar>\n\n          \n\n           <h2 class="list_item_title1" >{{user.g}}</h2>\n\n          \n\n          <div  class="list_item_sub_title2" >{{user.j}}  </div> \n\n\n\n\n\n    </button>\n\n\n\n\n\n\n\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)" > \n\n        <ion-infinite-scroll-content\n\n        loadingSpinner="bubbles"\n\n        loadingText="Loading more data...">\n\n      </ion-infinite-scroll-content> \n\n      </ion-infinite-scroll>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\employees\employees.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_work_list_ntf_work_list_ntf__["a" /* WorkListNtfProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ViewController */]])
    ], EmployeesPage);
    return EmployeesPage;
}());

//# sourceMappingURL=employees.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_work_list_ntf_work_list_ntf__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__notification_details_notification_details__ = __webpack_require__(532);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_pages_employees_employees__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__delegation_home_delegation_home__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_pages_start_start__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














/**
 * Generated class for the WorkListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WorkListPage = (function () {
    function WorkListPage(navCtrl, navParams, sanitized, platform, actionsheetCtrl, workListProvider, loadingController, popoverCtrl, AlertController) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sanitized = sanitized;
        this.platform = platform;
        this.actionsheetCtrl = actionsheetCtrl;
        this.workListProvider = workListProvider;
        this.loadingController = loadingController;
        this.popoverCtrl = popoverCtrl;
        this.AlertController = AlertController;
        this.mm = "  background-color: red;";
        this.uu = "";
        this.v_step = 200;
        this.fromR = 1;
        this.toR = this.v_step;
        // filters
        this.showSearch = true;
        this.othersClass = 'ico-fa-btn1';
        this.POClass = 'ico-fa-btn1 ';
        this.requisitionsClass = 'ico-fa-btn1 ';
        this.expensesClass = 'ico-fa-btn1 ';
        this.overtimeClass = 'ico-fa-btn1 ';
        this.HRClass = 'ico-fa-btn1 ';
        this.filterIcon = 'vlight';
        this.hr = '';
        this.overtime = '';
        this.expenses = '';
        this.requisitions = '';
        this.po = '';
        this.others = '';
        this.showSearch = true;
        this.hr = this.navParams.get('hr');
        this.overtime = this.navParams.get('overtime');
        this.expenses = this.navParams.get('expenses');
        this.requisitions = this.navParams.get('requisitions');
        this.po = this.navParams.get('po');
        this.others = this.navParams.get('others');
        this.pp = this.sanitized.bypassSecurityTrustHtml(this.uu);
        this.isHidden = [];
        try {
            this.workListProvider.getWorkListPost(this.fromR + '', this.toR + '')
                .then(function (data) {
                _this.test1 = data;
                _this.test2 = data;
                _this.test3 = data;
                // try{
                _this.intializeFilter();
                _this.applyfilter();
                // }
                //catch(err)
                //{
                //}
            });
        }
        catch (error) {
            console.log('errorx1');
            console.log(error);
            __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error.message);
        }
        try {
            this.workListProvider.getWorkListcountPost()
                .then(function (data) {
                window.localStorage.setItem('noOfNotifications', data[0].ntf);
                //  console.log(data);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error.message);
        }
        this.chats = [
            {
                img: './assets/avatar-cher.png',
                name: 'Cher',
                message: 'Ugh. As if.',
                time: '9:38 pm'
            }, {
                img: './assets/avatar-dionne.png',
                name: 'Dionne',
                message: 'Mr. Hall was way harsh.',
                time: '8:59 pm'
            }, {
                img: './assets/avatar-murray.png',
                name: 'Murray',
                message: 'Excuse me, "Ms. Dione."',
                time: 'Wed'
            }
        ];
        this.logins = [
            {
                icon: 'logo-twitter',
                name: 'Twitter',
                username: 'admin',
                a: 0,
                actions: [{ x: 'trash', y: 1, z: 'secondary', a: 0 }, { x: 'menu', y: '2', z: 'danger', a: 0 }, { x: 'trash', y: 3, z: 'dark', a: 0 }]
            }, {
                icon: 'logo-github',
                name: 'GitHub',
                username: 'admin37',
                a: 0,
                actions: [{ x: 'trash', y: 4, z: 'dark', a: 0 }, { x: 'menu', y: '5', z: 'danger', a: 0 }, { x: 'volume-off', y: 6, z: 'dark', a: 0 }]
            }, {
                icon: 'logo-instagram',
                name: 'Instagram',
                username: 'imanadmin',
                a: 0,
                actions: [{ x: 'volume-off', y: 2, z: 'danger', a: 0 }, { x: 'trash', y: 'bb', z: 'dark', a: 0 }]
            }, {
                icon: 'logo-codepen',
                name: 'Codepen',
                a: 1,
                username: 'administrator',
                actions: [{ x: 'menu', y: 7, z: 'danger', a: 0 }, { x: 'trash', y: '8', z: 'secondary', a: 0 }, { x: 'trash', y: 9, z: 'dark', a: 0 }]
            }
        ];
        // action sheet 
        this.moreActions = [
            {
                text: 'Delete',
                role: 'destructive',
                icon: !this.platform.is('ios') ? 'trash' : null,
                handler: function () {
                    console.log('Delete clicked');
                }
            },
            {
                text: 'Share',
                icon: !this.platform.is('ios') ? 'share' : null,
                handler: function () {
                    console.log('Share clicked');
                }
            },
            {
                text: 'Play',
                icon: !this.platform.is('ios') ? 'arrow-dropright-circle' : null,
                handler: function () {
                    console.log('Play clicked');
                }
            },
            {
                text: 'Favorite',
                icon: !this.platform.is('ios') ? 'heart-outline' : null,
                handler: function () {
                    console.log('Favorite clicked');
                }
            },
            {
                text: 'Cancel',
                role: 'cancel',
                icon: !this.platform.is('ios') ? 'close' : null,
                handler: function () {
                    console.log('Cancel clicked');
                }
            }
        ];
    }
    WorkListPage.prototype.ionViewDidLoad = function () {
        //let m;
        // m=document.getElementById("ix");
        this.setBackButtonAction();
        //   m.innerHtml=this.uu;
        console.log('ionViewDidLoad WorkListPage');
    };
    WorkListPage.prototype.more = function (item) {
        console.log('More');
        console.log(item);
        item.close();
    };
    WorkListPage.prototype.morex = function (item, wf, bu, i) {
        console.log('Morex');
        console.log(wf.subject);
        console.log(bu.button_name);
        if (bu.button_name == 'MORE') {
            this.openMenu(item, wf, i);
        }
        else if (bu.button_name == 'MORE_INFORMATION_REQUEST') {
            this.takeReaasignAction('More Information', wf.notification_id);
        }
        else if (bu.button_name == 'REASSIGN') {
            this.takeReaasignAction('Reassign', wf.notification_id);
        }
        else {
            //change
            //this.isHidden[i]=true;
            // item.setElementClass("oo" ,true);
            //   setTimeout(() => {
            //   // item.setElementClass("oo1" ,true);
            //  // this.isHidden[i]=true;  // sidar idea
            //   }, 250);
            //  item.close();
            this.takeNotificationAction(item, wf.notification_id, wf.item_type, wf.item_key, wf.process_activity, bu.button_name, i);
        }
    };
    WorkListPage.prototype.openMenu = function (item, wf, index) {
        var _this = this;
        var bu = wf.more_actions;
        this.moreActions = [];
        var _loop_1 = function (i) {
            this_1.moreActions.push({
                text: bu[i].button_label,
                icon: !this_1.platform.is('ios') ? bu[i].button_icon : null,
                handler: function () { _this.actionSheetHandler(bu[i].button_name, item, wf, index); }
            });
        };
        var this_1 = this;
        for (var i = 0; i < bu.length; i++) {
            _loop_1(i);
        }
        // adding cancel 
        this.moreActions.push({
            text: 'Cancel',
            role: 'cancel',
            icon: !this.platform.is('ios') ? 'close' : null,
            handler: function () { _this.actionSheetHandler('cancel', item, wf, index); }
        });
        var actionSheet = this.actionsheetCtrl.create({
            // title: 'Actions',
            //cssClass: 'action-sheets-basic-page',
            buttons: this.moreActions
        });
        actionSheet.present();
    };
    WorkListPage.prototype.actionSheetHandler = function (act, item, wf, index) {
        console.log(act);
        if (act == 'cancel')
            null;
        else if (act == 'MORE_INFORMATION_REQUEST') {
            this.takeReaasignAction('More Information', wf.notification_id);
        }
        else if (act == 'REASSIGN') {
            this.takeReaasignAction('Reassign', wf.notification_id);
        }
        else {
            this.takeNotificationAction(item, wf.notification_id, wf.item_type, wf.item_key, wf.process_activity, act, index);
            //  item.setElementClass("oo" ,true);
            //  setTimeout(() => {
            //   item.setElementClass("oo1" ,true);
            //  }, 999);
            // item.close();
        }
    };
    WorkListPage.prototype.validateBeforeAction = function (buttonName) {
        // check request More Info Replay 
        if (buttonName && buttonName == 'SUBMIT') {
            __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Please open the notification and take this action inside');
            return 0;
        }
        // check Forward 
        if (buttonName && buttonName.indexOf('FORWARD') > 1) {
            __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Please open the notification and take this action inside');
            return 0;
        }
        return 1;
    };
    WorkListPage.prototype.takeNotificationAction = function (item, notificationId, itemType, itemKey, activityId, buttonName, i) {
        var _this = this;
        var validated = this.validateBeforeAction(buttonName);
        if (validated == 0)
            return;
        try {
            this.workListProvider.takeAction(notificationId, itemType, itemKey, activityId, buttonName, 'none')
                .then(function (data) {
                if (data[0].message == '0') {
                    __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, data[0].value);
                    return;
                }
                else {
                    // update count here 
                    window.localStorage.setItem('noOfNotifications', data[0].message);
                    _this.isHidden[i] = true;
                }
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error.message);
        }
    };
    WorkListPage.prototype.noticationTapped = function (event, wf) {
        var _this = this;
        if (!wf.generator_api) {
            __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Notification deatils are not available ');
            return;
        }
        //window.localStorage.setItem('noOfNotifications' ,'10');
        try {
            this.workListProvider.getNoticationBody(wf.notification_id, wf.item_type, wf.item_key, wf.generator_api)
                .then(function (data) {
                // console.log('datax :' + data[0].html_data  );
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__notification_details_notification_details__["a" /* NotificationDetailsPage */], {
                    notificationBody: data[0].html_data,
                    moreActions: wf.more_actions,
                    baseActions: wf.base_actions,
                    wf: wf,
                    hr: _this.hr,
                    overtime: _this.overtime,
                    expenses: _this.expenses,
                    requisitions: _this.requisitions,
                    po: _this.po,
                    others: _this.others
                });
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Error:' + error.message);
        }
    };
    WorkListPage.prototype.onInput = function (ev) {
        if (!this.test1) {
            return;
        }
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.test1 = this.test1.filter(function (item) {
                var itemData = '-';
                ;
                if (item.from_user)
                    itemData = itemData + item.from_user.toLowerCase();
                if (item.subject)
                    itemData = itemData + item.subject.toLowerCase();
                return ((itemData).indexOf(val.toLowerCase()) > -1);
            });
        }
        else
            this.test1 = this.test3;
    };
    WorkListPage.prototype.onCancel = function (ev) {
        __WEBPACK_IMPORTED_MODULE_8__app_app_module__["a" /* AppModule */].showMessage(this.AlertController, 'Cancel');
    };
    WorkListPage.prototype.doInfinite = function (infiniteScroll) {
        console.log('Begin infiniteScroll operation');
        this.fromR = this.fromR + this.v_step;
        this.toR = this.toR + this.v_step;
        try {
            this.workListProvider.getWorkListPost(this.fromR + '', this.toR + '').then(function (data) {
                if (!data) {
                    //  infiniteScroll.enable(false);
                    infiniteScroll.complete();
                    return;
                }
                console.log('data is ' + data);
                //this.test1= this.test1.concat(data);
                //this.test2=this.test2.concat(data);;
                console.log(data);
                infiniteScroll.complete();
            });
        }
        catch (err) {
        }
        console.log(' infiniteScroll has ended');
    };
    WorkListPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        this.searchItem = '';
        this.fromR = 1;
        this.toR = this.v_step;
        try {
            this.workListProvider.getWorkListcountPost()
                .then(function (data) {
                window.localStorage.setItem('noOfNotifications', data[0].ntf);
                console.log(data);
            });
        }
        catch (error) {
        }
        this.workListProvider.getWorkListPost(this.fromR + '', this.toR + '')
            .then(function (data) {
            _this.test1 = data;
            _this.test2 = data;
            _this.test3 = data;
            _this.applyfilter();
            refresher.complete();
        });
    };
    WorkListPage.prototype.backPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__shared_pages_start_start__["a" /* StartPage */], {});
    };
    WorkListPage.prototype.setBackButtonAction = function () {
        var _this = this;
        this.Navbar.backButtonClick = function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__shared_pages_start_start__["a" /* StartPage */]);
        };
    };
    WorkListPage.prototype.CustomtfFilter = function () {
        this.showSearch = false;
        //document.getElementById("search-div").style.display="block";
        //this.addclass('search-div');
        // let x =document.getElementsByClassName("tt");
        // x[0].style.height="100%";
    };
    WorkListPage.prototype.hideFilter = function () {
        //this.removeClass('search-div');
        //document.getElementById("search-div").style.display="none";
        this.showSearch = true;
    };
    WorkListPage.prototype.addclass = function (elementId) {
        var el;
        el = document.getElementById(elementId);
        __WEBPACK_IMPORTED_MODULE_9_jquery__(el).addClass('block-display');
    };
    WorkListPage.prototype.removeClass = function (elementId) {
        var el;
        el = document.getElementById(elementId);
        __WEBPACK_IMPORTED_MODULE_9_jquery__(el).removeClass('block-display');
    };
    WorkListPage.prototype.intializeFilter = function () {
        if (this.others) {
            this.othersClass = 'ico-fa-btn1 dark-bg gg';
            this.otherIcon = '#fff';
        }
        if (this.po) {
            this.POClass = 'ico-fa-btn1 dark-bg gg';
            this.POIcon = '#fff';
        }
        if (this.requisitions) {
            this.requisitionsClass = 'ico-fa-btn1 dark-bg gg';
            this.requisitionsIcon = '#fff';
        }
        if (this.expenses) {
            this.expensesClass = 'ico-fa-btn1 dark-bg gg';
            this.expensesIcon = '#fff';
        }
        if (this.overtime) {
            this.overtimeClass = 'ico-fa-btn1 dark-bg gg';
            this.overtimeIcon = '#fff';
        }
        if (this.hr) {
            this.HRClass = 'ico-fa-btn1 dark-bg gg ';
            this.HRIcon = '#fff';
        }
    };
    WorkListPage.prototype.pressFilterButton = function (filterName, elementId) {
        this.searchItem = '';
        if (filterName == 'Others') {
            if (!this.others) {
                this.others = 'Other';
                this.othersClass = 'ico-fa-btn1 dark-bg gg';
                this.otherIcon = '#fff';
            }
            else {
                this.others = '';
                this.othersClass = 'ico-fa-btn1';
                this.otherIcon = '#353D4e';
            }
        }
        if (filterName == 'PO') {
            if (!this.po) {
                this.po = 'PO';
                this.POClass = 'ico-fa-btn1 dark-bg gg';
                this.POIcon = '#fff';
            }
            else {
                this.po = '';
                this.POClass = 'ico-fa-btn1 ';
                this.POIcon = '#353D4e';
            }
        }
        if (filterName == 'Requisitions') {
            if (!this.requisitions) {
                this.requisitions = 'Requisition';
                this.requisitionsClass = 'ico-fa-btn1 dark-bg gg';
                this.requisitionsIcon = '#fff';
            }
            else {
                this.requisitions = '';
                this.requisitionsClass = 'ico-fa-btn1';
                this.requisitionsIcon = '#353D4e';
            }
        }
        if (filterName == 'Expenses') {
            if (!this.expenses) {
                this.expenses = 'Expenses';
                this.expensesClass = 'ico-fa-btn1 dark-bg gg';
                this.expensesIcon = '#fff';
            }
            else {
                this.expenses = '';
                this.expensesClass = 'ico-fa-btn1';
                this.expensesIcon = '#353D4e';
            }
        }
        if (filterName == 'Overtime') {
            if (!this.overtime) {
                this.overtime = 'Overtime';
                this.overtimeClass = 'ico-fa-btn1 dark-bg gg';
                this.overtimeIcon = '#fff';
            }
            else {
                this.overtime = '';
                this.overtimeClass = 'ico-fa-btn1';
                this.overtimeIcon = '#353D4e';
            }
        }
        if (filterName == 'HR') {
            if (!this.hr) {
                this.hr = 'HR';
                this.HRClass = 'ico-fa-btn1 dark-bg gg ';
                this.HRIcon = '#fff';
            }
            else {
                this.hr = '';
                this.HRClass = 'ico-fa-btn1';
                this.HRIcon = '#353D4e';
            }
        }
        //filter_type
        this.applyfilter();
    };
    WorkListPage.prototype.applyfilter = function () {
        var filterValue = '-';
        if (this.others)
            filterValue = filterValue + this.others + '-';
        if (this.po)
            filterValue = filterValue + this.po + '-';
        if (this.requisitions)
            filterValue = filterValue + this.requisitions + '-';
        if (this.expenses)
            filterValue = filterValue + this.expenses + '-';
        if (this.overtime)
            filterValue = filterValue + this.overtime + '-';
        if (this.hr)
            filterValue = filterValue + this.hr + '-';
        //filterValue=this.others+this.po+this.requisitions+this.expenses+this.overtime+this.hr;
        if (filterValue && filterValue.length > 1) {
            this.filterIcon = 'dark';
            this.test1 = this.test2.filter(function (item) {
                return (filterValue.toLowerCase().indexOf(item.filter_type.toLowerCase()) > -1);
            });
        }
        else {
            this.filterIcon = 'vlight';
            this.test1 = this.test2;
        }
        this.test3 = this.test1;
    };
    // presentPopover() {
    //   let popover = this.popoverCtrl.create(PopoverPage);
    //   let ev = {
    //     target : {
    //       getBoundingClientRect : () => {
    //         return {
    //           top: '100%'
    //         };
    //       }
    //     }
    //   };
    //   popover.present({ev});
    // }
    WorkListPage.prototype.takeReaasignAction = function (title, notificationId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__shared_pages_employees_employees__["a" /* EmployeesPage */], {
            title: title,
            notificationId: notificationId,
            hr: this.hr,
            overtime: this.overtime,
            expenses: this.expenses,
            requisitions: this.requisitions,
            po: this.po,
            others: this.others
        });
    };
    WorkListPage.prototype.openDelagtion = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__delegation_home_delegation_home__["a" /* DelegationHomePage */], {});
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */])
    ], WorkListPage.prototype, "Navbar", void 0);
    WorkListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-work-list',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\work-list\work-list.html"*/'<!--\n\n  Generated template for the WorkListPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar hideBackButton=\'false\'>\n\n  \n\n    <ion-title>Inbox</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="openDelagtion()">\n\n        <ion-icon name="swap" color="light"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n     \n\n<ion-content  >\n\n \n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content pullingIcon="arrow-dropdown"\n\n        pullingText="Pull to refresh"\n\n        refreshingSpinner="circles"\n\n        refreshingText="Refreshing..."></ion-refresher-content>\n\n      </ion-refresher>\n\n\n\n      \n\n\n\n  <ion-grid class="xx" >\n\n      <ion-row justify-content-start align-items-center>\n\n        <ion-col col-10 >\n\n          <div >\n\n            \n\n              <ion-searchbar  \n\n              [(ngModel)]="searchItem"\n\n              [showCancelButton]="shouldShowCancel"\n\n              animated="true"\n\n              cancelButtonText="Cancel"\n\n              showCancelButton="true"\n\n              placeholder="Search"\n\n              (ionInput)="onInput($event)"\n\n              (ionCancel)="onCancel($event)">\n\n              </ion-searchbar>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-2 > \n\n          <div class=\'mm\'> \n\n              <ion-buttons right>\n\n            <button ion-button icon-only color="darkgray" outline  [disabled]="test1==null" class="filter-btn" (click)="CustomtfFilter()">\n\n              <ion-icon name="ios-options" color="{{filterIcon}}" ></ion-icon>\n\n            </button>\n\n            </ion-buttons>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n</ion-grid>\n\n\n\n\n\n<div></div>\n\n\n\n  <ion-list class="list1">\n\n\n\n    <ion-item-sliding *ngFor="let wf of test1;   let i = index "  #itemx  >\n\n      <button ion-item  detail-none [hidden]="isHidden[i]" (click)="noticationTapped($event, wf )" >\n\n   \n\n        <ion-avatar item-start> \n\n          <!-- <img [src]="sanitized.bypassSecurityTrustUrl(+\'data:image/jpg;base64,\'+wf.img)" [hidden]="wf.img==null"/> -->\n\n          <img src=\'assets/imgs/avatar.png\' [hidden]="wf.from_user!=null" class="avater-img"/>\n\n          <div class="{{wf.class_name}}" [hidden]="wf.from_user==null"  >  {{wf.intials}} </div>\n\n         </ion-avatar>\n\n        \n\n         <h2 class="list_item_title1" >{{wf.from_user}}</h2>\n\n        <div class="list_item_sub_block">\n\n        <div  class="list_item_sub_title2" >{{wf.type}}  </div> \n\n         <div class="list_item_sub_title3" >{{wf.begin_date}} </div>\n\n  </div>\n\n\n\n\n\n        <p class="list_item_sub_title1" >{{wf.subject}}</p>\n\n       \n\n   \n\n       \n\n  </button>\n\n\n\n\n\n       <ion-item-options class="notification-pannel">\n\n        <button class="notification-btn1" ion-button color="{{actButtons.button_color}}" *ngFor="let actButtons of wf.base_actions" (click)="morex(itemx,wf,actButtons ,i)" >\n\n          <ion-icon name="{{actButtons.button_icon}}" class="notification-ico"></ion-icon>\n\n          {{actButtons.button_label}}\n\n        </button>\n\n       </ion-item-options> \n\n\n\n      \n\n    </ion-item-sliding> \n\n\n\n  </ion-list>\n\n\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)" threshold="100px"> \n\n      <ion-infinite-scroll-content\n\n      loadingSpinner="bubbles"\n\n      loadingText="Loading more data...">\n\n    </ion-infinite-scroll-content> \n\n    </ion-infinite-scroll>\n\n\n\n\n\n <!-- <div [innerHTML]="pp" ></div> -->\n\n<!--<div Id="ix"> </div>\n\n<button ion-button block (click)="openMenu()">\n\n  Show Action Sheet\n\n</button> -->\n\n<!-- <canvas #doughnutCanvas ></canvas> -->\n\n</ion-content>\n\n<div class="tt" id="search-div" [hidden]=\'showSearch\'>\n\n  <div class="buttons-div">\n\n    \n\n      <div class="mnb2 mnb-left"  >\n\n          <button ion-button  color="dark"    class="ico-fa-btn1"    (click)="pressFilterButton(\'HR\',\'hr\')" [ngClass]="HRClass" >\n\n            <fa-icon name="users"  class="aw-ico1" [style.color]="HRIcon"></fa-icon>\n\n              \n\n          </button>\n\n          <h6 ion-text color="dark" class="btn-txt" >HR</h6>\n\n        </div>\n\n        <div class="mnb2"  >\n\n            <button ion-button   color="dark"  class="ico-fa-btn1" (click)="pressFilterButton(\'Overtime\',\'overtime\')" [ngClass]="overtimeClass">\n\n                <fa-icon name="calendar"  class="aw-ico1" [style.color]="overtimeIcon" ></fa-icon>\n\n            </button>\n\n            <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">Overtime</h6>\n\n        </div>\n\n        \n\n        <div class="mnb2"  >\n\n            <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="pressFilterButton(\'Expenses\',\'expenses\')"  [ngClass]="expensesClass">\n\n                <fa-icon name="credit-card"  class="aw-ico1"  [style.color]="expensesIcon"></fa-icon>\n\n            </button>\n\n            <h6 ion-text color="dark" class="btn-txt" >Expense </h6>\n\n        </div>\n\n\n\n\n\n        <div class="mnb2 mnb-left"  >\n\n            <button ion-button  color="dark"    class="ico-fa-btn1"  (click)="pressFilterButton(\'Requisitions\',\'requisitions\')" [ngClass]="requisitionsClass">\n\n              <fa-icon name="shopping-cart"  class="aw-ico1" [style.color]="requisitionsIcon"></fa-icon>\n\n                \n\n            </button>\n\n            <h6 ion-text color="dark" class="btn-txt" >Requisition</h6>\n\n          </div>\n\n\n\n          <div class="mnb2"  >\n\n              <button ion-button   color="dark"  id="po" (click)="pressFilterButton(\'PO\',\'po\')"  [ngClass]="POClass" >\n\n                  <fa-icon name="truck"  id="po-ico" class="aw-ico1" [style.color]="POIcon"></fa-icon>\n\n              </button>\n\n              <h6 ion-text color="dark" class="btn-txt" id="ProfileTxt">PO</h6>\n\n          </div>\n\n          \n\n          <div class="mnb2"  >\n\n              <button ion-button  color="dark"   (click)="pressFilterButton(\'Others\',\'others\')" id="others"  [ngClass]="othersClass">\n\n                  <fa-icon name="envelope"  class="aw-ico1" id="others-ico" [style.color]="otherIcon"></fa-icon>\n\n              </button>\n\n              <h6 ion-text color="dark" class="btn-txt " >Others </h6>\n\n          </div>\n\n          <div class="mnb-close"  >\n\n              <button ion-button  color="dark"   class="ico-fa-btn2 " (click)="hideFilter()">\n\n                  <fa-icon name="times"  class="aw-ico1-close"></fa-icon>\n\n              </button>\n\n            \n\n          </div>\n\n  </div>\n\n  </div> \n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\approval-center\work-list\work-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_3__providers_work_list_ntf_work_list_ntf__["a" /* WorkListNtfProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], WorkListPage);
    return WorkListPage;
}());

//# sourceMappingURL=work-list.js.map

/***/ }),

/***/ 672:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 306,
	"./af.js": 306,
	"./ar": 307,
	"./ar-dz": 308,
	"./ar-dz.js": 308,
	"./ar-kw": 309,
	"./ar-kw.js": 309,
	"./ar-ly": 310,
	"./ar-ly.js": 310,
	"./ar-ma": 311,
	"./ar-ma.js": 311,
	"./ar-sa": 312,
	"./ar-sa.js": 312,
	"./ar-tn": 313,
	"./ar-tn.js": 313,
	"./ar.js": 307,
	"./az": 314,
	"./az.js": 314,
	"./be": 315,
	"./be.js": 315,
	"./bg": 316,
	"./bg.js": 316,
	"./bm": 317,
	"./bm.js": 317,
	"./bn": 318,
	"./bn.js": 318,
	"./bo": 319,
	"./bo.js": 319,
	"./br": 320,
	"./br.js": 320,
	"./bs": 321,
	"./bs.js": 321,
	"./ca": 322,
	"./ca.js": 322,
	"./cs": 323,
	"./cs.js": 323,
	"./cv": 324,
	"./cv.js": 324,
	"./cy": 325,
	"./cy.js": 325,
	"./da": 326,
	"./da.js": 326,
	"./de": 327,
	"./de-at": 328,
	"./de-at.js": 328,
	"./de-ch": 329,
	"./de-ch.js": 329,
	"./de.js": 327,
	"./dv": 330,
	"./dv.js": 330,
	"./el": 331,
	"./el.js": 331,
	"./en-au": 332,
	"./en-au.js": 332,
	"./en-ca": 333,
	"./en-ca.js": 333,
	"./en-gb": 334,
	"./en-gb.js": 334,
	"./en-ie": 335,
	"./en-ie.js": 335,
	"./en-il": 336,
	"./en-il.js": 336,
	"./en-nz": 337,
	"./en-nz.js": 337,
	"./eo": 338,
	"./eo.js": 338,
	"./es": 339,
	"./es-do": 340,
	"./es-do.js": 340,
	"./es-us": 341,
	"./es-us.js": 341,
	"./es.js": 339,
	"./et": 342,
	"./et.js": 342,
	"./eu": 343,
	"./eu.js": 343,
	"./fa": 344,
	"./fa.js": 344,
	"./fi": 345,
	"./fi.js": 345,
	"./fo": 346,
	"./fo.js": 346,
	"./fr": 347,
	"./fr-ca": 348,
	"./fr-ca.js": 348,
	"./fr-ch": 349,
	"./fr-ch.js": 349,
	"./fr.js": 347,
	"./fy": 350,
	"./fy.js": 350,
	"./gd": 351,
	"./gd.js": 351,
	"./gl": 352,
	"./gl.js": 352,
	"./gom-latn": 353,
	"./gom-latn.js": 353,
	"./gu": 354,
	"./gu.js": 354,
	"./he": 355,
	"./he.js": 355,
	"./hi": 356,
	"./hi.js": 356,
	"./hr": 357,
	"./hr.js": 357,
	"./hu": 358,
	"./hu.js": 358,
	"./hy-am": 359,
	"./hy-am.js": 359,
	"./id": 360,
	"./id.js": 360,
	"./is": 361,
	"./is.js": 361,
	"./it": 362,
	"./it.js": 362,
	"./ja": 363,
	"./ja.js": 363,
	"./jv": 364,
	"./jv.js": 364,
	"./ka": 365,
	"./ka.js": 365,
	"./kk": 366,
	"./kk.js": 366,
	"./km": 367,
	"./km.js": 367,
	"./kn": 368,
	"./kn.js": 368,
	"./ko": 369,
	"./ko.js": 369,
	"./ky": 370,
	"./ky.js": 370,
	"./lb": 371,
	"./lb.js": 371,
	"./lo": 372,
	"./lo.js": 372,
	"./lt": 373,
	"./lt.js": 373,
	"./lv": 374,
	"./lv.js": 374,
	"./me": 375,
	"./me.js": 375,
	"./mi": 376,
	"./mi.js": 376,
	"./mk": 377,
	"./mk.js": 377,
	"./ml": 378,
	"./ml.js": 378,
	"./mn": 379,
	"./mn.js": 379,
	"./mr": 380,
	"./mr.js": 380,
	"./ms": 381,
	"./ms-my": 382,
	"./ms-my.js": 382,
	"./ms.js": 381,
	"./mt": 383,
	"./mt.js": 383,
	"./my": 384,
	"./my.js": 384,
	"./nb": 385,
	"./nb.js": 385,
	"./ne": 386,
	"./ne.js": 386,
	"./nl": 387,
	"./nl-be": 388,
	"./nl-be.js": 388,
	"./nl.js": 387,
	"./nn": 389,
	"./nn.js": 389,
	"./pa-in": 390,
	"./pa-in.js": 390,
	"./pl": 391,
	"./pl.js": 391,
	"./pt": 392,
	"./pt-br": 393,
	"./pt-br.js": 393,
	"./pt.js": 392,
	"./ro": 394,
	"./ro.js": 394,
	"./ru": 395,
	"./ru.js": 395,
	"./sd": 396,
	"./sd.js": 396,
	"./se": 397,
	"./se.js": 397,
	"./si": 398,
	"./si.js": 398,
	"./sk": 399,
	"./sk.js": 399,
	"./sl": 400,
	"./sl.js": 400,
	"./sq": 401,
	"./sq.js": 401,
	"./sr": 402,
	"./sr-cyrl": 403,
	"./sr-cyrl.js": 403,
	"./sr.js": 402,
	"./ss": 404,
	"./ss.js": 404,
	"./sv": 405,
	"./sv.js": 405,
	"./sw": 406,
	"./sw.js": 406,
	"./ta": 407,
	"./ta.js": 407,
	"./te": 408,
	"./te.js": 408,
	"./tet": 409,
	"./tet.js": 409,
	"./tg": 410,
	"./tg.js": 410,
	"./th": 411,
	"./th.js": 411,
	"./tl-ph": 412,
	"./tl-ph.js": 412,
	"./tlh": 413,
	"./tlh.js": 413,
	"./tr": 414,
	"./tr.js": 414,
	"./tzl": 415,
	"./tzl.js": 415,
	"./tzm": 416,
	"./tzm-latn": 417,
	"./tzm-latn.js": 417,
	"./tzm.js": 416,
	"./ug-cn": 418,
	"./ug-cn.js": 418,
	"./uk": 419,
	"./uk.js": 419,
	"./ur": 420,
	"./ur.js": 420,
	"./uz": 421,
	"./uz-latn": 422,
	"./uz-latn.js": 422,
	"./uz.js": 421,
	"./vi": 423,
	"./vi.js": 423,
	"./x-pseudo": 424,
	"./x-pseudo.js": 424,
	"./yo": 425,
	"./yo.js": 425,
	"./zh-cn": 426,
	"./zh-cn.js": 426,
	"./zh-hk": 427,
	"./zh-hk.js": 427,
	"./zh-tw": 428,
	"./zh-tw.js": 428
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 672;

/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(698);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_shared_pages_login_login__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_shared_pages_start_start__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ng_idle_core__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, idle, appCtrl) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.idle = idle;
        this.appCtrl = appCtrl;
        this.mySplashScreen = true;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_shared_pages_login_login__["a" /* LoginPage */]; //ScmDashboardPage;//LeaveUtilzationDbPage;//MyRequisitionDbPage;//LeaveBalanceDbPage;//LoginPage;  
        // idleState = 'Not started.';
        // timedOut = false;
        // lastPing?: Date = null;
        this.idleState = 'Not started.';
        this.timedOut = false;
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_5__pages_shared_pages_login_login__["a" /* LoginPage */] },
            { title: 'start', component: __WEBPACK_IMPORTED_MODULE_6__pages_shared_pages_start_start__["a" /* StartPage */] }
        ];
        // this.idle.setIdle(5);  //after 5 sec idle
        // this.idle.setTimeout(30*60);  //5min countdown
        // this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
        // this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
        // this.idle.onTimeout.subscribe(() => {
        //   this.idleState = 'Timed out!';
        //   this.timedOut = true;
        //  // alert ('idle')
        //  // this.navCtrl.pop();  //go to logout page after 5 min idle.
        //  //this.appCtrl.getRootNav().setRoot(HomePage);
        //  this.nav.setRoot(HomePage);
        //  //this.initializeApp();
        //  //this.navCtrl.setRoot(HomePage);
        //  //this.navCtrl.popToRoot();
        //  this.reload();
        // });
        // this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
        // this.idle.onTimeoutWarning.subscribe((countdown) => {
        //     var data=countdown/60;
        //     this.min=data.toString().split('.')[0];
        //     this.sec=     parseFloat(0+'.'+data.toString().split('.')[1])*60;
        //     this.sec=  (Math.round(this.sec * 100) / 100);
        //     console.log(countdown)
        //   this.idleState = 'You ll logout in ' + this.min+' min ' +this.sec+'  seconds!';
        // });
        //    this.reload();
        //    platform.ready().then(() => {
        //     if (platform.is('cordova')){
        //       //Subscribe on pause
        //       this.platform.pause.subscribe(() => {
        //       console.log('pause')
        //       });
        //       //Subscribe on resume
        //       this.platform.resume.subscribe(() => {
        //         console.log('resume')
        //         window['paused'] = 0;
        //       });
        //      }
        //   });
    }
    MyApp.prototype.reload = function () {
        this.idle.watch();
        this.idleState = 'Started';
        this.timedOut = false;
    };
    MyApp.prototype.reset = function () {
        this.idle.watch();
        this.idleState = 'Started.';
        this.timedOut = false;
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            //  this.mySplashScreen=false;
            if (_this.platform.is('ios')) {
                _this.statusBar.overlaysWebView(false);
            }
            if (!_this.platform.is('ios')) {
                _this.statusBar.hide();
            }
            _this.statusBar.backgroundColorByHexString("#353D4e");
            _this.splashScreen.hide();
            __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__["Observable"].interval(1).subscribe(function () { return _this.mySplashScreen = false; });
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.logout = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\app\app.html"*/'<div *ngIf="mySplashScreen" class="mysplsh">\n\n  <div class="sk-cube-grid">\n\n      <div class="sk-cube sk-cube1"></div>\n\n      <div class="sk-cube sk-cube2"></div>\n\n      <div class="sk-cube sk-cube3"></div>\n\n      <div class="sk-cube sk-cube4"></div>\n\n      <div class="sk-cube sk-cube5"></div>\n\n      <div class="sk-cube sk-cube6"></div>\n\n      <div class="sk-cube sk-cube7"></div>\n\n      <div class="sk-cube sk-cube8"></div>\n\n      <div class="sk-cube sk-cube9"></div>\n\n      \n\n          \n\n        \n\n    </div>\n\n   \n\n    \n\n    \n\n</div>\n\n\n\n<!-- <ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n        {{p.title}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu> -->\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_7__ng_idle_core__["a" /* Idle */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HrRequestsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__letters_preview_letters_preview__ = __webpack_require__(540);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__housing_preview_housing_preview__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__businesscard_preview_businesscard_preview__ = __webpack_require__(544);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_pages_start_start__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_module__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the HrRequestsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HrRequestsPage = (function () {
    function HrRequestsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.businessGroup = __WEBPACK_IMPORTED_MODULE_6__app_app_module__["a" /* AppModule */].businessGroup;
    }
    HrRequestsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HrRequestsPage');
        this.setBackButtonAction();
    };
    HrRequestsPage.prototype.goPageH = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__housing_preview_housing_preview__["a" /* HousingPreviewPage */]);
    };
    HrRequestsPage.prototype.goPageL = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__letters_preview_letters_preview__["a" /* LettersPreviewPage */]);
    };
    HrRequestsPage.prototype.backPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__shared_pages_start_start__["a" /* StartPage */]);
    };
    HrRequestsPage.prototype.goPageBC = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__businesscard_preview_businesscard_preview__["a" /* BusinesscardPreviewPage */]);
    };
    HrRequestsPage.prototype.setBackButtonAction = function () {
        var _this = this;
        this.Navbar.backButtonClick = function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__shared_pages_start_start__["a" /* StartPage */]);
        };
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Navbar */])
    ], HrRequestsPage.prototype, "Navbar", void 0);
    HrRequestsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-hr-requests',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\hr-requests\hr-requests.html"*/'<!--\n\n  Generated template for the HrRequestsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar hideBackButton=\'false\'>\n\n\n\n              \n\n    <ion-title>HR Requests</ion-title>\n\n  </ion-navbar>\n\n  \n\n  \n\n \n\n  \n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding class="ion-content">\n\n\n\n\n\n    <div class="mnb"  >\n\n        <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goPageL()">\n\n            <fa-icon name="file-text-o"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n        <h6 ion-text  color="dark" class="btn-txt " >Letters</h6>\n\n        <h6 ion-text  color="dark" class="btn-txt " >&nbsp;</h6>\n\n        \n\n    </div>\n\n    <div class="mnb" *ngIf="businessGroup==81" >\n\n        <button ion-button  color="dark"   class="ico-fa-btn1 " (click)="goPageH()">\n\n            <fa-icon name="home"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n        <h6  ion-text  color="dark" class="btn-txt " >Housing</h6>\n\n        <h6 ion-text  color="dark" class="btn-txt " >Advance</h6>\n\n    </div>\n\n    <div class="mnb" *ngIf="businessGroup==81" >\n\n        <button ion-button  color="dark"     class="ico-fa-btn1 " (click)="goPageBC()">\n\n            <fa-icon name="id-card"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n        <h6 ion-text  color="dark" class="btn-txt " >Business</h6>\n\n        <h6 ion-text  color="dark" class="btn-txt " >Card</h6>\n\n    </div>\n\n      \n\n    \n\n\n\n\n\n    <div class="mnb"   [hidden]=\'true\'>\n\n        <button ion-button  color="dark"   disabled="true"  class="ico-fa-btn1 " (click)="goPageH()">\n\n            <fa-icon name="user-times"  class="aw-ico1"></fa-icon>\n\n        </button>\n\n        <h6 ion-text color="dark" class="btn-txt " >Submit</h6>\n\n        <h6 ion-text  color="dark" class="btn-txt " >Resignation</h6>\n\n    </div>\n\n\n\n  \n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\hr-request\hr-requests\hr-requests.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], HrRequestsPage);
    return HrRequestsPage;
}());

//# sourceMappingURL=hr-requests.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SchedulerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the SchedulerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SchedulerProvider = (function () {
    function SchedulerProvider(http, httpClient, loadingController, AlertController, aes256, appCtrl) {
        this.http = http;
        this.httpClient = httpClient;
        this.loadingController = loadingController;
        this.AlertController = AlertController;
        this.aes256 = aes256;
        this.appCtrl = appCtrl;
        console.log('Hello SchedulerProvider Provider');
    }
    //   getMySpaceList( personId: string ) {
    //     let loader=AppModule.showLoader(this.loadingController );
    //    // AppModule.wsURL=
    //    // 'http://10.10.131.34:7003/mbcWebserviceAA/resources';
    //     let url;
    //     if (AppModule.isCloud)
    //     {
    //       url=AppModule.wsURL;
    //     }
    //     else url =AppModule.wsURL+'/aa/scheduler/getMyspaceCurrent';
    //     url =AppModule.wsURL+'/aa/scheduler/getMyspaceCurrent';
    //  let methodHeaders = new HttpHeaders({
    //     'Content-Type':'application/json ; charset=UTF-8' ,
    //   'Accept':'application/json',
    //   'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
    //   'Access-Control-Allow-origin':'*',
    //   'Authorization': AppModule.authentication,
    //   'prId': personId+'' , 
    //  // 'businessGroup':''+businessGroup+'',
    //   'oracle-mobile-backend-id':AppModule.backendId,
    // 	'resourceName' :'aa/scheduler/getMyspaceCurrent'
    // });
    SchedulerProvider.prototype.getMySpaceList = function (dateStart) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'aa/scheduler/getMyspace';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            // 'prId': '69146'+'' , 
                            'dateStart': '' + dateStart + '',
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SchedulerProvider.prototype.getMySpaceListx = function (personId, dateStart) {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        // AppModule.wsURL=
        // 'http://10.10.131.34:7003/mbcWebserviceAA/resources';
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/aa/scheduler/getMyspace';
        url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/aa/scheduler/getMyspace';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            //'prId': personId+'' , 
            'dateStart': '' + dateStart + '',
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'resourceName': 'aa/scheduler/getMyspace'
        });
        return new Promise(function (resolve) {
            _this.httpClient.post(url, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                console.log(err);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    SchedulerProvider.prototype.getMyTeamList = function (dateStart) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].sAAwkId == '0')) return [3 /*break*/, 2];
                        ws = 'aa/scheduler/getMyteam';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'orgId': '' + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].sAAOrganizationId,
                            'dateStart': '' + dateStart + '',
                        };
                        console.log('MyTeam');
                        return [3 /*break*/, 4];
                    case 2:
                        ws = 'aa/scheduler/getMyteamWk';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 3:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            'orgId': '' + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].sAAOrganizationId,
                            'wkId': '' + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].sAAwkId,
                            'dateStart': '' + dateStart + '',
                        };
                        console.log('MyTeamWorkgroup');
                        _a.label = 4;
                    case 4: return [2 /*return*/, new Promise(function (resolve) {
                            __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                resolve(data);
                            }, function (err) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                            });
                        })];
                }
            });
        });
    };
    SchedulerProvider.prototype.getworkgroupsList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'aa/scheduler/getMyWk';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + ''),
                            // 'prId': '69146'+'' , 
                            'orgId': '' + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].sAAOrganizationId
                            //AppModule.getProperty('AAorganization')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SchedulerProvider.prototype.getMyOrgList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ws, t, methodHeaders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ws = 'aa/scheduler/getOrg';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].doHash(this.aes256, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getProperty('wsid') + __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getAuxiliaryValue(ws))];
                    case 1:
                        t = _a.sent();
                        methodHeaders = {
                            'signature': encodeURI(t + '')
                        };
                        return [2 /*return*/, new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].getData(_this.httpClient, _this.http, ws, methodHeaders, _this.loadingController, _this.AlertController, _this.appCtrl).then(function (data) {
                                    resolve(data);
                                }, function (err) {
                                    __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'Error');
                                });
                            })];
                }
            });
        });
    };
    SchedulerProvider.prototype.getMyTeamListx = function (personId, orgId, dateStart) {
        var _this = this;
        var loader = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showLoader(this.loadingController);
        // AppModule.wsURL=
        // 'http://10.10.131.34:7003/mbcWebserviceAA/resources';
        var url;
        if (__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].isCloud) {
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL;
        }
        else
            url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/aa/scheduler/getMyteam';
        url = __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].wsURL + '/aa/scheduler/getMyteam';
        var methodHeaders = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpHeaders */]({
            'Content-Type': 'application/json ; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
            'Access-Control-Allow-origin': '*',
            'Authorization': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].authentication,
            'prId': personId + '',
            'orgId': '' + orgId + '',
            'dateStart': '' + dateStart + '',
            'oracle-mobile-backend-id': __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].backendId,
            'resourceName': 'aa/scheduler/getMyteam'
        });
        return new Promise(function (resolve) {
            _this.httpClient.post(url, null, { headers: methodHeaders }).timeout(15000).subscribe(function (data) {
                resolve(data);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
            }, function (err) {
                console.log(err);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].stopLoader(loader);
                __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].showMessage(_this.AlertController, 'No Connection');
            });
        });
    };
    SchedulerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ionic_native_aes_256__["a" /* AES256 */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* App */]) === "function" && _f || Object])
    ], SchedulerProvider);
    return SchedulerProvider;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=scheduler.js.map

/***/ }),

/***/ 981:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list',template:/*ion-inline-start:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\list\list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>List</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n\n      {{item.title}}\n\n      <div class="item-note" item-end>\n\n        {{item.note}}\n\n      </div>\n\n    </button>\n\n  </ion-list>\n\n  <div *ngIf="selectedItem" padding>\n\n    You navigated here from <b>{{selectedItem.title}}</b>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\MBC2020\AA\Mobile\ionicApp\App_6_4_2020\src\pages\shared-pages\list\list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 982:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaIconComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FaIconComponent = (function (_super) {
    __extends(FaIconComponent, _super);
    function FaIconComponent(config, elementRef, renderer) {
        return _super.call(this, config, elementRef, renderer, "fa") || this;
    }
    Object.defineProperty(FaIconComponent.prototype, "fixedWidth", {
        set: function (fixedWidth) {
            this.setElementClass("fa-fw", this.isTrueProperty(fixedWidth));
        },
        enumerable: true,
        configurable: true
    });
    FaIconComponent.prototype.ngOnChanges = function (changes) {
        if (changes.name) {
            this.unsetPrevAndSetCurrentClass(changes.name);
        }
        if (changes.size) {
            this.unsetPrevAndSetCurrentClass(changes.size);
        }
    };
    FaIconComponent.prototype.isTrueProperty = function (val) {
        if (typeof val === "string") {
            val = val.toLowerCase().trim();
            return (val === "true" || val === "on" || val === "");
        }
        return !!val;
    };
    ;
    FaIconComponent.prototype.unsetPrevAndSetCurrentClass = function (change) {
        this.setElementClass("fa-" + change.previousValue, false);
        this.setElementClass("fa-" + change.currentValue, true);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], FaIconComponent.prototype, "name", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], FaIconComponent.prototype, "size", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])("fixed-width"),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], FaIconComponent.prototype, "fixedWidth", null);
    FaIconComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "fa-icon",
            template: "",
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Config */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]])
    ], FaIconComponent);
    return FaIconComponent;
}(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Ion */]));

//# sourceMappingURL=fa-icon.component.js.map

/***/ })

},[598]);
//# sourceMappingURL=main.js.map
ALTER TABLE APPS.AS_WORK_GROUPS_HDR
 DROP PRIMARY KEY CASCADE;

DROP TABLE APPS.AS_WORK_GROUPS_HDR CASCADE CONSTRAINTS;

CREATE TABLE APPS.AS_WORK_GROUPS_HDR
(
  WORK_GROUP_ID      NUMBER,
  WORK_GROUP_NAME    VARCHAR2(256 BYTE)         NOT NULL,
  ENABLED_FLAG       VARCHAR2(1 BYTE)           DEFAULT 'Y',
  CREATION_DATE      DATE,
  CREATED_BY         NUMBER,
  LAST_UPDATE_DATE   DATE,
  LAST_UPDATED_BY    NUMBER,
  LAST_UPDATE_LOGIN  NUMBER,
  IMAGE_LOOKUP_CODE  VARCHAR2(30 BYTE)
)
TABLESPACE APPS_TS_TX_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX APPS.AS_WORK_GROUPS_HDR_PK ON APPS.AS_WORK_GROUPS_HDR
(WORK_GROUP_ID)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


ALTER TABLE APPS.AS_WORK_GROUPS_HDR ADD (
  CONSTRAINT AS_WORK_GROUPS_HDR_PK
  PRIMARY KEY
  (WORK_GROUP_ID)
  USING INDEX APPS.AS_WORK_GROUPS_HDR_PK
  ENABLE VALIDATE);

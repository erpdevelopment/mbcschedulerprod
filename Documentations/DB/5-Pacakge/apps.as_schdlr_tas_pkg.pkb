

CREATE OR REPLACE PACKAGE BODY APPS.as_schdlr_tas_pkg
is


procedure PushData 
is   
ERRORCODE      VARCHAR2 (4000);
COUNTEXIST   NUMBER;
cursor EMP is 
--- Employee attend No Replacement/Absent/ OffByManager

select   scheduler_staff_id, st.person_id,st.shift_id,'N'Is_Replacement, '01' Companycode ,employee_number EmployeeCode ,to_date(to_char(schedular_start_date),'dd-mm-yy') ShiftDate   ,nvl(shift.shift_name ,'S0900E1830') ShiftCode ,
  decode(replace_type,'OffByManager','Off','On') WeeklyOff, to_date(to_char(sysdate),'dd-mm-yy') LastWriteDateTime


 from as_scheduler_staff st  ,per_people_f PAPF ,as_shifts shift
where SCHEDULAR_START_DATE=to_date(sysdate-7 ,'dd/mon/yy')
 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1)
                          and papf.person_id=st.person_id
                          and st.shift_id=shift.shift_id(+) 
                         -- and st.person_id=38564
                          
                      ----- Replacement -----    
                     union      
  
    select scheduler_staff_id,st.fk_replaced_by_id,st.shift_id,'Y' Is_Replacement, '01' Companycode ,employee_number EmployeeCode ,to_date(to_char(schedular_start_date),'dd-mm-yy')  ShiftDate   ,nvl(shift.shift_name ,'S0900E1830') ShiftCode ,
  'Off' WeeklyOff, to_date(to_char(sysdate),'dd-mm-yy') LastWriteDateTime 
        
    from as_scheduler_staff st,per_people_f PAPF ,as_shifts shift
where SCHEDULAR_START_DATE=to_date(sysdate-7 ,'dd/mon/yy')
and replace_type!='No Replacement' and fk_replaced_by_id 
not in (select person_id from as_scheduler_staff 
where SCHEDULAR_START_DATE=to_date(sysdate-7 ,'dd/mon/yy')

)

 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1)
                          and papf.person_id=st.fk_replaced_by_id
                          and st.shift_id=shift.shift_id(+) 
-----------------Off Employee
union

  select  0 scheduler_staff_id,offemployees.person_id,0 shift_id,'Off' Is_Replacement, '01' Companycode ,employee_number EmployeeCode ,to_date(to_char(sysdate-7),'dd-mm-yy')  ShiftDate   ,'' ShiftCode ,
  'Off' WeeklyOff, to_date(to_char(sysdate),'dd-mm-yy') LastWriteDateTime 


from 
(select person_id  from 
as_work_groups_lines
where person_id is not null
minus
(
select person_id from as_scheduler_staff
where SCHEDULAR_START_DATE=to_date(sysdate-7 ,'dd/mon/yy')
union
select fk_replaced_by_id from as_scheduler_staff
where SCHEDULAR_START_DATE=to_date(sysdate-7 ,'dd/mon/yy')
and replace_type!='No Replacement' and fk_replaced_by_id not in (select person_id from as_scheduler_staff
where SCHEDULAR_START_DATE=to_date(sysdate-7 ,'dd/mon/yy'))

)) offemployees ,per_people_f papf
where offemployees.person_id=papf.person_id
 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1) ;

begin 
 for i in EMP
loop
select count("EmployeeCode") into COUNTEXIST from DLI_EmpRoster@sql_link
where "EmployeeCode"=i.employeeCode and "ShiftDate"=i.ShiftDate;
if COUNTEXIST=0
THEN 


INSERT INTO DLI_EmpRoster@sql_link ("CompanyCode", "EmployeeCode","ShiftDate","ShiftCode","WeeklyOff","SyncStatus","LastWriteDateTime")
     VALUES (i.CompanyCode, i.EmployeeCode,i.ShiftDate,i.ShiftCode,i.WeeklyOff,0,i.LastWriteDateTime) ;
     commit;
    insert into AS_TAS_INTEGRATION values(i.scheduler_staff_id,i.shift_id,i.person_id,i.Is_Replacement,i.CompanyCode, i.EmployeeCode,i.ShiftDate,i.ShiftCode,i.WeeklyOff,i.LastWriteDateTime);
commit;

 END IF ;

end loop ;


EXCEPTION
            WHEN OTHERS
            THEN
               ERRORCODE :=
                   '-Error-'
                  || SQLERRM;
               DBMS_OUTPUT.PUT_LINE ('errorcode = ' || ERRORCODE);
            --   insert into AS_TAS_ERRORS values (ERRORCODE,sysdate);
--DBMS_OUTPUT.PUT_LINE ('errorcode =');
--commit;
end ;

procedure PushData_testing 
is   
ERRORCODE      VARCHAR2 (4000);
COUNTEXIST   NUMBER;
cursor EMP is 
--- Employee attend No Replacement/Absent/ OffByManager

select   scheduler_staff_id, st.person_id,st.shift_id,'N'Is_Replacement, '01' Companycode ,employee_number EmployeeCode ,to_date(to_char(schedular_start_date),'dd-mm-yy') ShiftDate   ,nvl(shift.shift_name ,'S0900E1830') ShiftCode ,
  decode(replace_type,'OffByManager','Off','On') WeeklyOff, to_date(to_char(sysdate),'dd-mm-yy') LastWriteDateTime


 from as_scheduler_staff st  ,per_people_f PAPF ,as_shifts shift
where employee_number in('7019','2850','0119','2831')
and ( SCHEDULAR_START_DATE=to_date('01-Jan-2011','dd/mon/yy')
or  SCHEDULAR_START_DATE=to_date('02-Jan-2011','dd/mon/yy'))
--(sysdate-7 ,'dd/mon/yy')
 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1)
                          and papf.person_id=st.person_id
                          and st.shift_id=shift.shift_id(+) 
                          
                        union  
                          
                          
                          
                           select scheduler_staff_id,st.fk_replaced_by_id,st.shift_id,'Y' Is_Replacement, '01' Companycode ,employee_number EmployeeCode ,to_date(to_char(schedular_start_date),'dd-mm-yy')  ShiftDate   ,nvl(shift.shift_name ,'S0900E1830') ShiftCode ,
  'Off' WeeklyOff, to_date(to_char(sysdate),'dd-mm-yy') LastWriteDateTime 
        
    from as_scheduler_staff st,per_people_f PAPF ,as_shifts shift
where    ( SCHEDULAR_START_DATE=to_date('01-Jan-2011','dd/mon/yy')
or  SCHEDULAR_START_DATE=to_date('02-Jan-2011','dd/mon/yy'))

and replace_type!='No Replacement' and fk_replaced_by_id 
not in (select person_id from as_scheduler_staff 
where   ( SCHEDULAR_START_DATE=to_date('01-Jan-2011','dd/mon/yy')
or  SCHEDULAR_START_DATE=to_date('02-Jan-2011','dd/mon/yy'))

)

 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1)
                          and papf.person_id=st.fk_replaced_by_id
                          and st.shift_id=shift.shift_id(+)  ;
                          
                          
                          
                          

begin 
 for i in EMP
loop
select count("EmployeeCode") into COUNTEXIST from DLI_EmpRoster@sql_link
where "EmployeeCode"=i.employeeCode and "ShiftDate"=i.ShiftDate;
if COUNTEXIST=0
THEN 


INSERT INTO DLI_EmpRoster@sql_link ("CompanyCode", "EmployeeCode","ShiftDate","ShiftCode","WeeklyOff","SyncStatus","LastWriteDateTime")
     VALUES (i.CompanyCode, i.EmployeeCode,i.ShiftDate,i.ShiftCode,i.WeeklyOff,0,i.LastWriteDateTime) ;
     commit;
    insert into AS_TAS_INTEGRATION values(i.scheduler_staff_id,i.shift_id,i.person_id,i.Is_Replacement,i.CompanyCode, i.EmployeeCode,i.ShiftDate,i.ShiftCode,i.WeeklyOff,i.LastWriteDateTime);
commit;

 END IF ;

end loop ;


EXCEPTION
            WHEN OTHERS
            THEN
               ERRORCODE :=
                   '-Error-'
                  || SQLERRM;
               DBMS_OUTPUT.PUT_LINE ('errorcode = ' || ERRORCODE);
            --   insert into AS_TAS_ERRORS values (ERRORCODE,sysdate);
--DBMS_OUTPUT.PUT_LINE ('errorcode =');
--commit;
end ;
end;
/

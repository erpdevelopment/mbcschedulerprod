package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import com.mbc.oracle.scheduler.view.util.JSFUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.time.LocalDate;

import java.time.Period;

import java.time.ZoneId;

import java.util.Calendar;
import java.util.Date;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItems;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichGridCell;
import oracle.adf.view.rich.component.rich.layout.RichGridRow;
import oracle.adf.view.rich.component.rich.layout.RichPanelBox;
import oracle.adf.view.rich.component.rich.layout.RichPanelGridLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichToolbar;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Number;
import oracle.binding.BindingContainer;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.model.BindingContext;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

public class Integratetas {
    private RichPanelBox pb1;
    private RichPanelGridLayout pgl1;
    private RichGridRow gr1;
    private RichGridCell gc1;
    private RichGridCell gc2;
    private RichGridRow gr2;
    private RichGridCell gc3;
    private RichGridCell gc4;
    private RichGridRow gr3;
    private RichGridCell gc5;
    private RichGridCell gc6;
    private RichInputDate id1;
    private RichSelectOneChoice soc1;
    private UISelectItems si1;
    private RichInputDate id2;
    private RichInputText it1;
    private RichButton b1;
    private RichPanelBox pb2;
    private RichTable t1;
    public Date StartDate ;
    public Date EndDate ;
    public String Day1;
    public String Day2;
    public String Day3;
    public String Day4;
    public String Day5;
    public String Day6;
    public String Day7;
    public String Day8;
    public String Day9;
    public String Day10;
    public String Day11;
    public String Day12;
    public String Day13;
    public String Day14;
    public String Day15;
    public String Day16;
    public String Day17;
    public String Day18;
    public String Day19;
    public String Day20;
    public String Day21;
    public String Day22;
    public String Day23;
    public String Day24;
    public String Day25;
    public String Day26;
    public String Day27;
    public String Day28;
    public String Day29;
    public String Day30;
    public String Day31;
    private RichPanelGroupLayout pgl2;
    private RichLink l1;
    private RichPanelGroupLayout pgl3;
    private RichPanelCollection pc1;
    private RichToolbar t2;
    private RichButton b2;
    private RichButton b3;
    private RichPanelBox pb3;
    private RichPopup p32;
    private RichDialog d1;


    public void setEmpFullName(String EmpFullName) {
        this.EmpFullName = EmpFullName;
    }

    public String getEmpFullName() {
        return EmpFullName;
    }
    private RichSelectBooleanCheckbox sbc1;
    public String EmpFullName;


  
    public void setStartDate(Date StartDate) {
        this.StartDate = StartDate;
    }

    public Date getStartDate() {
       // return StartDate;
       if(StartDate!= null && (!StartDate.equals("")))
        return StartDate;
           
        else 
       {

        
          return      new Date();
           }
    }

    public void setEndDate(Date EndDate) {
        this.EndDate = EndDate;
    }

    public Date getEndDate() {
        //return EndDate;
        if(EndDate!= null && (!EndDate.equals("")))
         return EndDate;
            
         else 
        {
              
               
                  return   this.addDays(this.getStartDate(),30);
            }
    }

    public void setDay1(String Day1) {
        this.Day1 = Day1;
    }

    public String getDay1() {
        return   this.DayOfWeek(this.getStartDate());
        //return Day1;
    }

    public void setDay2(String Day2) {
        this.Day2 = Day2;
    }

    public String getDay2() {
        if(Integer.valueOf(this.GetStartEndRange())>=2)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 1));
        else
        return"N";
    }

    public void setDay3(String Day3) {
        this.Day3 = Day3;
    }

    public String getDay3() {
        //return Day3;
        if(Integer.valueOf(this.GetStartEndRange())>=3)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 2));
        else
        return"N";
    }

    public void setDay4(String Day4) {
        this.Day4 = Day4;
    }

    public String getDay4() {
        if(Integer.valueOf(this.GetStartEndRange())>=4)
            return   this.DayOfWeek(this.addDays(this.getStartDate(),3));
        else
        return"N";
       // return Day4;
    }

    public void setDay5(String Day5) {
        this.Day5 = Day5;
    }

    public String getDay5() {
        if(Integer.valueOf(this.GetStartEndRange())>=5)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 4));
        else
        return"N";
    
    }

    public void setDay6(String Day6) {
        this.Day6 = Day6;
    }

    public String getDay6() {
        if(Integer.valueOf(this.GetStartEndRange())>=6)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 5));
        else
        return"N";
        //return Day6;
    }

    public void setDay7(String Day7) {
        this.Day7 = Day7;
    }

    public String getDay7() {
        if(Integer.valueOf(this.GetStartEndRange())>=7)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 6));
        else
        return"N";
       // return Day7;
    }

    public void setDay8(String Day8) {
        this.Day8 = Day8;
    }

    public String getDay8() {
        if(Integer.valueOf(this.GetStartEndRange())>=8)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 7));
        else
        return"N";
       // return Day8;
    }

    public void setDay9(String Day9) {
        this.Day9 = Day9;
    }

    public String getDay9() {
        if(Integer.valueOf(this.GetStartEndRange())>=9)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 8));
        else
        return"N";
        //return Day9;
    }

    public void setDay10(String Day10) {
        this.Day10 = Day10;
    }

    public String getDay10() {
        if(Integer.valueOf(this.GetStartEndRange())>=10)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 9));
        else
        return"N";
        //return Day10;
    }

    public void setDay11(String Day11) {
        this.Day11 = Day11;
    }

    public String getDay11() {
        if(Integer.valueOf(this.GetStartEndRange())>=11)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 10));
        else
        return"N";
      //  return Day11;
    }

    public void setDay12(String Day12) {
        this.Day12 = Day12;
    }

    public String getDay12() {
        if(Integer.valueOf(this.GetStartEndRange())>=12)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 11));
        else
        return"N";
      //  return Day12;
    }

    public void setDay13(String Day13) {
        this.Day13 = Day13;
    }

    public String getDay13() {
        if(Integer.valueOf(this.GetStartEndRange())>=13)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 12));
        else
        return"N";
      //  return Day13;
    }

    public void setDay14(String Day14) {
        this.Day14 = Day14;
    }

    public String getDay14() {
        if(Integer.valueOf(this.GetStartEndRange())>=14)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 13));
        else
        return"N";
       // return Day14;
    }

    public void setDay15(String Day15) {
        this.Day15 = Day15;
    }

    public String getDay15() {
        if(Integer.valueOf(this.GetStartEndRange())>=15)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 14));
        else
        return"N";
      //  return Day15;
    }

    public void setDay16(String Day16) {
        this.Day16 = Day16;
    }

    public String getDay16() {
        if(Integer.valueOf(this.GetStartEndRange())>=16)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 15));
        else
        return"N";
      //  return Day16;
    }

    public void setDay17(String Day17) {
        this.Day17 = Day17;
    }

    public String getDay17() {
        if(Integer.valueOf(this.GetStartEndRange())>=17)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 16));
        else
        return"N";
       // return Day17;
    }

    public void setDay18(String Day18) {
        this.Day18 = Day18;
    }

    public String getDay18() {
        if(Integer.valueOf(this.GetStartEndRange())>=18)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 17));
        else
        return"N";
      //  return Day18;
    }

    public void setDay19(String Day19) {
        this.Day19 = Day19;
    }

    public String getDay19() {
        if(Integer.valueOf(this.GetStartEndRange())>=19)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 18));
        else
        return"N";
       // return Day19;
    }

    public void setDay20(String Day20) {
        this.Day20 = Day20;
    }

    public String getDay20() {
        if(Integer.valueOf(this.GetStartEndRange())>=20)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 19));
        else
        return"N";
       // return Day20;
    }

    public void setDay21(String Day21) {
        this.Day21 = Day21;
    }

    public String getDay21() {
        if(Integer.valueOf(this.GetStartEndRange())>=21)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 20));
        else
        return"N";
      //  return Day21;
    }

    public void setDay22(String Day22) {
        this.Day22 = Day22;
    }

    public String getDay22() {
        if(Integer.valueOf(this.GetStartEndRange())>=22)
            return   this.DayOfWeek(this.addDays(this.getStartDate(),21));
        else
        return"N";
       // return Day22;
    }

    public void setDay23(String Day23) {
        this.Day23 = Day23;
    }

    public String getDay23() {
        if(Integer.valueOf(this.GetStartEndRange())>=23)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 22));
        else
        return"N";
      //  return Day23;
    }

    public void setDay24(String Day24) {
        this.Day24 = Day24;
    }

    public String getDay24() {
        if(Integer.valueOf(this.GetStartEndRange())>=24)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 23));
        else
        return"N";
       // return Day24;
    }

    public void setDay25(String Day25) {
        this.Day25 = Day25;
    }

    public String getDay25() {
        if(Integer.valueOf(this.GetStartEndRange())>=25)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 24));
        else
        return"N";
      //  return Day25;
    }

    public void setDay26(String Day26) {
        this.Day26 = Day26;
    }

    public String getDay26() {
        if(Integer.valueOf(this.GetStartEndRange())>=26)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 25));
        else
        return"N";
      //  return Day26;
    }

    public void setDay27(String Day27) {
        this.Day27 = Day27;
    }

    public String getDay27() {
        if(Integer.valueOf(this.GetStartEndRange())>=27)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 26));
        else
        return"N";
       // return Day27;
    }

    public void setDay28(String Day28) {
        this.Day28 = Day28;
    }

    public String getDay28() {
        if(Integer.valueOf(this.GetStartEndRange())>=28)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 27));
        else
        return"N";
     //   return Day28;
    }

    public void setDay29(String Day29) {
        this.Day29 = Day29;
    }

    public String getDay29() {
        if(Integer.valueOf(this.GetStartEndRange())>=29)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 28));
        else
        return"N";
      //  return Day29;
    }

    public void setDay30(String Day30) {
        this.Day30 = Day30;
    }

    public String getDay30() {
        if(Integer.valueOf(this.GetStartEndRange())>=30)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 29));
        else
        return"N";
      //  return Day30;
    }

    public void setDay31(String Day31) {
        this.Day31 = Day31;
    }

    public String getDay31() {
        if(Integer.valueOf(this.GetStartEndRange())>=31)
            return   this.DayOfWeek(this.addDays(this.getStartDate(), 30));
        else
        return"N";
       // return Day31;
    }
   
    public void setPb1(RichPanelBox pb1) {
        this.pb1 = pb1;
    }

    public RichPanelBox getPb1() {
        return pb1;
    }

    public void setPgl1(RichPanelGridLayout pgl1) {
        this.pgl1 = pgl1;
    }

    public RichPanelGridLayout getPgl1() {
        return pgl1;
    }

    public void setGr1(RichGridRow gr1) {
        this.gr1 = gr1;
    }

    public RichGridRow getGr1() {
        return gr1;
    }

    public void setGc1(RichGridCell gc1) {
        this.gc1 = gc1;
    }

    public RichGridCell getGc1() {
        return gc1;
    }

    public void setGc2(RichGridCell gc2) {
        this.gc2 = gc2;
    }

    public RichGridCell getGc2() {
        return gc2;
    }

    public void setGr2(RichGridRow gr2) {
        this.gr2 = gr2;
    }

    public RichGridRow getGr2() {
        return gr2;
    }

    public void setGc3(RichGridCell gc3) {
        this.gc3 = gc3;
    }

    public RichGridCell getGc3() {
        return gc3;
    }

    public void setGc4(RichGridCell gc4) {
        this.gc4 = gc4;
    }

    public RichGridCell getGc4() {
        return gc4;
    }

    public void setGr3(RichGridRow gr3) {
        this.gr3 = gr3;
    }

    public RichGridRow getGr3() {
        return gr3;
    }

    public void setGc5(RichGridCell gc5) {
        this.gc5 = gc5;
    }

    public RichGridCell getGc5() {
        return gc5;
    }

    public void setGc6(RichGridCell gc6) {
        this.gc6 = gc6;
    }

    public RichGridCell getGc6() {
        return gc6;
    }

    public void setId1(RichInputDate id1) {
        this.id1 = id1;
    }

    public RichInputDate getId1() {
        return id1;
    }

    public void setSoc1(RichSelectOneChoice soc1) {
        this.soc1 = soc1;
    }

    public RichSelectOneChoice getSoc1() {
        return soc1;
    }

    public void setSi1(UISelectItems si1) {
        this.si1 = si1;
    }

    public UISelectItems getSi1() {
        return si1;
    }

    public void setId2(RichInputDate id2) {
        this.id2 = id2;
    }

    public RichInputDate getId2() {
        return id2;
    }

    public void setIt1(RichInputText it1) {
        this.it1 = it1;
    }

    public RichInputText getIt1() {
        return it1;
    }

    public void setB1(RichButton b1) {
        this.b1 = b1;
    }

    public RichButton getB1() {
        return b1;
    }
    
    // --------------------Custom Method -------------------------//
    public java.util.Date GetStartDateFromSession()
    {
        
            Map sessionscope=ADFContext.getCurrent().getSessionScope();
         java.util.Date st=(java.util.Date)sessionscope.get("TasStartDate");
         
            return st;
        
        }
    public java.util.Date GetEndDateFromSession()
    {
        
            Map sessionscope=ADFContext.getCurrent().getSessionScope();
                   java.util.Date et=(java.util.Date)sessionscope.get("TasEndDate");
                     return et;
        
        }
    public void SetStarEndDateSessionValues()
    {
            JSFUtils.storeOnSession("TasStartDate", this.getStartDate());
            JSFUtils.storeOnSession("TasEndDate", this.getEndDate());
        
        
        }
    public void  Search_Button_Action(ActionEvent actionEvent) 
   // public String Search_Button_Action()
   {
        // Add event code here...
        SetStarEndDateSessionValues();
        int dater= this.GetStartEndRange();
          //  Integer.valueOf(this.GetStartEndRange());

        
        if(dater<0)
        {
            
                FacesMessage fm=new FacesMessage("Start Date Can't be More than End Date"); 
                fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fcx=FacesContext.getCurrentInstance();
                fcx.addMessage(null, fm);
            }
        if(dater>31)
        {
            FacesMessage fm=new FacesMessage("Please Select Maximum one Month The Date range Selected is "+String.valueOf(dater) +" Days which is more than one month"); 
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
            FacesContext fcx=FacesContext.getCurrentInstance();
            fcx.addMessage(null, fm);
       
        }
        else
        {
                FacesMessage fm=new FacesMessage(String.valueOf(dater)); 
                fm.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fcx=FacesContext.getCurrentInstance();
                fcx.addMessage(null, fm);
               
       OperationBinding oper = ADFUtils.findOperation("TasSearch");
        Number cWorkgroupId = (Number) ADFUtils.getBoundAttributeValue("WorkGroupId");
       
        oper.getParamsMap().put("WorkGroupId",cWorkgroupId);
        oper.getParamsMap().put("pfullname",this.getEmpFullName());
       oper.execute();
            
            }
      
      
       // AdfFacesContext.getCurrentInstance().addPartialTarget(t1);
       // return"";
    }


   public void SelectAllEmp(ActionEvent actionEvent) {

   
//   OperationBinding oper2 = ADFUtils.findOperation("TasSearch");
//   Number cWorkgroupId = (Number) ADFUtils.getBoundAttributeValue("WorkGroupId");
//   
//   oper2.getParamsMap().put("WorkGroupId",cWorkgroupId);
//   oper2.getParamsMap().put("pfullname",this.getEmpFullName());
//   oper2.execute();
   
   
        OperationBinding oper = ADFUtils.findOperation("SelectAllChecked");
                oper.execute();
       // AdfFacesContext.getCurrentInstance().addPartialTarget(t1);
               // return "";
        
    }
    public String UnSelectAllEmp()
    {
        
//        OperationBinding oper2 = ADFUtils.findOperation("TasSearch");
//        Number cWorkgroupId = (Number) ADFUtils.getBoundAttributeValue("WorkGroupId");
//        
//        oper2.getParamsMap().put("WorkGroupId",cWorkgroupId);
//        oper2.getParamsMap().put("pfullname",this.getEmpFullName());
//        oper2.execute();
        OperationBinding oper = ADFUtils.findOperation("SelectAllUnChecked");
                oper.execute();
                return "";
        
    }
    
    
    public  String SetInitalDateLoad()
    {
        
            // public String SelectAllEmp()
            OperationBinding oper = ADFUtils.findOperation("GetStartEndDateFromSession");
                    oper.execute();
               FacesMessage fm=new FacesMessage(String.valueOf(oper.getResult()));
               fm.setSeverity(FacesMessage.SEVERITY_INFO);
               FacesContext fcx=FacesContext.getCurrentInstance();
               fcx.addMessage(null, fm); 
             return "";
        }
//    public DCBindingContainer getDCBindingsContainer() {
//           DCBindingContainer bindingsContainer =
//               (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
//           
//           return bindingsContainer;
//       }
    public void process(ActionEvent actionEvent) {
//          DCBindingContainer bindings = this.getDCBindingsContainer();
//          int countselected=0;
//          DCIteratorBinding itorBinding =
//              bindings.findIteratorBinding("AsEmployee_TasVO1Iterator");
//          ViewObject vo = itorBinding.getViewObject();
//          Row[]  selectedRolesRows =vo.getFilteredRows("SelectRow", true);
//          
//          for(Row row : selectedRolesRows){
//              countselected++;
//             // System.out.print("EmployeeNumber : "+row.getAttribute("EmployeeNumber"));
//             // System.out.println("Department Name : "+row.getAttribute("DepartmentName"));
//          }
//          FacesMessage fm=new FacesMessage(String.valueOf(countselected));
//          fm.setSeverity(FacesMessage.SEVERITY_INFO);
//          FacesContext fcx=FacesContext.getCurrentInstance();
//          fcx.addMessage(null, fm); 
                    OperationBinding oper = ADFUtils.findOperation("pushDataTas");
                    oper.execute();
               FacesMessage fm=new FacesMessage(String.valueOf(oper.getResult()));
               fm.setSeverity(FacesMessage.SEVERITY_INFO);
               FacesContext fcx=FacesContext.getCurrentInstance();
               fcx.addMessage(null, fm);
      }
    
    
    public void setPb2(RichPanelBox pb2) {
        this.pb2 = pb2;
    }

    public RichPanelBox getPb2() {
        return pb2;
    }

    //Added By Heba //
    //Date:11-july-2017//
    public  String DayOfWeek(Date dt)
    {
               Calendar cal = Calendar.getInstance();
               cal.setTime(dt);
              // int year = cal.get(Calendar.YEAR);
               int month = cal.get(Calendar.MONTH);
               int day = cal.get(Calendar.DAY_OF_WEEK);
               int dayofmonth=cal.get(Calendar.DAY_OF_MONTH);
               String DayofWeek="";
               String MonthofYear="";
               if(day==1)
                   DayofWeek="Sun";
             else  if(day==2)
                   DayofWeek= "Mon";
          else  if(day==3)
                DayofWeek= "Tue";
          else  if(day==4)
                DayofWeek= "Wed";
         else   if(day==5)
                DayofWeek= "Thurs";
         else   if(day==6)
                DayofWeek="Fri";
       else     if(day==7)
                DayofWeek= "Sat";
            //------------Month-------
            if(month==0)
                MonthofYear="Jan";
          else  if(month==1)
                MonthofYear="Feb";
            else  if(month==2)
                  MonthofYear="March";
            else  if(month==3)
                  MonthofYear="Apr";
            else  if(month==4)
                  MonthofYear="May";
            else  if(month==5)
                  MonthofYear="Jun";
            else  if(month==6)
                  MonthofYear="Jul";
              else  if(month==7)
                    MonthofYear="Aug";
              else  if(month==8)
                    MonthofYear="Sept";
              else  if(month==9)
                    MonthofYear="Oct";
              else  if(month==10)
                    MonthofYear="Nov";
            else  if(month==11)
                  MonthofYear="Dec";
            return String.valueOf(dayofmonth)+"-"+String.valueOf(month+1);
               // DayofWeek+" "+MonthofYear+" "+String.valueOf(dayofmonth);
          
        }
    // Method to add Days  for the the date inserted in choose week start day to get the other six days
    public  Date addDays(Date date, int days)
      {
          Calendar cal = Calendar.getInstance();
          cal.setTime(date);
          cal.add(Calendar.DATE, days); //minus number would decrement the days
         // cal.
          return cal.getTime();
      }

    public int GetStartEndRange()
    {
            String datediff ="";
            int daterange=0;
           
            OperationBinding oper = ADFUtils.findOperation("GetDateRange");
            oper.getParamsMap().put("StartDate", this.GetStartDateFromSession());
            oper.getParamsMap().put("EndDate", this.GetEndDateFromSession());
               // oper.getParamsMap().put("StartDate",this.getStartDate());
               // oper.getParamsMap().put("EndDate",this.getEndDate());
                            oper.execute();
                            if (!oper.getErrors().isEmpty()) {
                                
                                   }
                            else 
                            {
                                daterange=Integer.valueOf(oper.getResult().toString());
                            // datediff=;
                              //  dater=Integer.valueOf(datediff);
                            }
                            return daterange;
                            
        }
    //---------------End Custom Method //
    public void setT1(RichTable t1) {
        this.t1 = t1;
    }

    public RichTable getT1() {
        return t1;
    }

    public void setPgl2(RichPanelGroupLayout pgl2) {
        this.pgl2 = pgl2;
    }

    public RichPanelGroupLayout getPgl2() {
        return pgl2;
    }

    public void setSbc1(RichSelectBooleanCheckbox sbc1) {
        this.sbc1 = sbc1;
    }

    public RichSelectBooleanCheckbox getSbc1() {
        return sbc1;
    }

    public void setL1(RichLink l1) {
        this.l1 = l1;
    }

    public RichLink getL1() {
        return l1;
    }

    public void setPgl3(RichPanelGroupLayout pgl3) {
        this.pgl3 = pgl3;
    }

    public RichPanelGroupLayout getPgl3() {
        return pgl3;
    }

    public void setPc1(RichPanelCollection pc1) {
        this.pc1 = pc1;
    }

    public RichPanelCollection getPc1() {
        return pc1;
    }

    public void setT2(RichToolbar t2) {
        this.t2 = t2;
    }

    public RichToolbar getT2() {
        return t2;
    }

    public void setB2(RichButton b2) {
        this.b2 = b2;
    }

    public RichButton getB2() {
        return b2;
    }

    public void setB3(RichButton b3) {
        this.b3 = b3;
    }

    public RichButton getB3() {
        return b3;
    }


    public void setPb3(RichPanelBox pb3) {
        this.pb3 = pb3;
    }

    public RichPanelBox getPb3() {
        return pb3;
    }


    public void setP32(RichPopup p32) {
        this.p32 = p32;
    }

    public RichPopup getP32() {
        return p32;
    }

    public void setD1(RichDialog d1) {
        this.d1 = d1;
    }

    public RichDialog getD1() {
        return d1;
    }
}

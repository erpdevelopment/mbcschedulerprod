var CurrentActualData = [];
var YAGOActualData = [];
var TargetData = [];
var QuarterCategories = [];

var ageDistDataSource = new kendo.data.DataSource( {
    transport :  {
        read :  {
            // the remote service url
            url : "http://10.10.131.26:7003/schedulerRest/rest/AASchedules1.0/DashboardAnnualLeavePtrnVO", 
            // the data type of the returned result
            dataType : "json", type : "GET"
        }
    },
    // describe the result format
    schema :  {
        // the data, which the data source will be bound to is in the "list" field of the response
        data : "items", model :  {
            fields :  {
                PatternName :  {
                    type : "string"
                },
                TargetData :  {
                    type : "number"
                },
                AbsenceDaysYago :  {
                    type : "number"
                },
                AbsenceDaysCurr :  {
                    type : "number"
                },
                links :  {
                    rel :  {
                        type : "string"
                    },
                    href :  {
                        type : "string"
                    },
                    name :  {
                        type : "string"
                    },
                    kind :  {
                        type : "string"
                    }
                }
            }
        }
    },
    change : function () {
        var data = this.data();// or this.view();
        for (var i = 0;i < data.length;i++) {
            CurrentActualData.push(data[i].AbsenceDaysCurr);
            YAGOActualData.push(data[i].AbsenceDaysYago);
            TargetData.push(data[i].TargetData);
            QuarterCategories.push(data[i].PatternName);
        }
        AnnualLeaveQtrlyChart();
    }
});

ageDistDataSource.read();

function AnnualLeaveQtrlyChart() {
    $("#_leave_qtrlyHours_bar").kendoChart( {
        theme : 'material', title :  {
            text : ""
        },
        legend :  {
            visible : true, position : "top"
        },
        seriesDefaults :  {
            type : "column"
        },
        series : [{name : "Actual", color : "#30A3D9", spacing : 0, data : CurrentActualData},{name : "YoY", color : "#acdaef", data : YAGOActualData},{name : "Target", type : "line", color : 'rgba(0,0,0,0)', markers :  {background : '#787878', border :  {color : '#787878'}},data : TargetData}], valueAxis :  {
            labels :  {
                format : "{0}%"
            },
            line :  {
                visible : false
            },
            max : 100, min : 0, majorUnit : 25, minorGridLines :  {
                visible : false
            },
            majorGridLines :  {
                visible : false
            },
            axisCrossingValue : 0
        },
        categoryAxis :  {
            categories : QuarterCategories, line :  {
                visible : false
            }
        },
        tooltip :  {
            visible : true, format : "{0}%", template : "#= series.name #: #= value#%"
        }
    });
}
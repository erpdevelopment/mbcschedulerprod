package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Number;

public class Dbmonitoring {
    public Dbmonitoring() {
        super();
    }

    public String btn_delete_action() {
        // Add event code here...
        Number schedulerStaffId= (Number) ADFUtils.getBoundAttributeValue("SchedulerStaffId");
        FacesMessage Message = new FacesMessage(schedulerStaffId.toString());   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        OperationBinding oper = ADFUtils.findOperation("deleteSchdlr");
        oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
        oper.execute();
        OperationBinding oper2 = ADFUtils.findOperation("ExecuteLayoutACounter");
        oper2.execute();
        OperationBinding oper3 = ADFUtils.findOperation("ExecuteMonitorLayoutA");
        oper3.execute();
        return null;
    }
    
    
    public String btn_deleteLayoutC_action() {
        // Add event code here...
        Number schedulerStaffId= (Number) ADFUtils.getBoundAttributeValue("SchedulerCStaffId");
        FacesMessage Message = new FacesMessage(schedulerStaffId.toString());   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        OperationBinding oper = ADFUtils.findOperation("deleteSchdlr");
        oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
        oper.execute();
        OperationBinding oper2 = ADFUtils.findOperation("ExecuteLayoutCCounter");
        oper2.execute();
        OperationBinding oper3 = ADFUtils.findOperation("ExecuteMonitorLayoutC");
        oper3.execute();
        return null;
    }
}

DROP PACKAGE BODY APPS.AS_SCHDLR_PKG;

CREATE OR REPLACE PACKAGE BODY APPS.AS_SCHDLR_PKG
IS
   PROCEDURE GET_NEXT_APPROVER (IN_WORKLIST_ID          IN     NUMBER,
                                OUT_IS_FINAL_APPROVER      OUT VARCHAR2,
                                OUT_NEXT_APPROVER_ID       OUT VARCHAR2)
   IS
      L_WORKLIST_CATEGORY       AS_WORKLIST.WORKLIST_CATEGORY%TYPE;
      L_WORKLIST_TRANS_ID       AS_WORKLIST.WORKLIST_TRANS_ID%TYPE;
      L_WORKLIST_PERSON_ID      AS_WORKLIST.WORKLIST_PERSON_ID%TYPE;
      L_NXT_ACTION_HISTORY_ID   NUMBER;
   BEGIN
      SELECT WORKLIST_CATEGORY, WORKLIST_TRANS_ID, WORKLIST_PERSON_ID
        INTO L_WORKLIST_CATEGORY, L_WORKLIST_TRANS_ID, L_WORKLIST_PERSON_ID
        FROM AS_WORKLIST
       WHERE WORKLIST_ID = IN_WORKLIST_ID;

      IF L_WORKLIST_CATEGORY = 'SCHDLR'
      THEN
         SELECT MIN (SCHEDULER_ACTION_HISTORY_ID)
           INTO L_NXT_ACTION_HISTORY_ID
           FROM AS_SCHEDULER_ACTION_HISTORY
          WHERE     TRANS_ID = L_WORKLIST_TRANS_ID
                AND TRANS_TYPE = L_WORKLIST_CATEGORY
                AND ACTION_TYPE_TAKEN IS NULL;

         IF L_NXT_ACTION_HISTORY_ID IS NULL
         THEN
            OUT_IS_FINAL_APPROVER := 'Y';
         ELSE
            OUT_IS_FINAL_APPROVER := 'N';

            SELECT PERSON_ID
              INTO OUT_NEXT_APPROVER_ID
              FROM AS_SCHEDULER_ACTION_HISTORY
             WHERE SCHEDULER_ACTION_HISTORY_ID = L_NXT_ACTION_HISTORY_ID;
         END IF;
      ELSIF L_WORKLIST_CATEGORY = 'TRANS'
      THEN
         SELECT MIN (SCHEDULER_ACTION_HISTORY_ID)
           INTO L_NXT_ACTION_HISTORY_ID
           FROM AS_SCHEDULER_ACTION_HISTORY
          WHERE     TRANS_ID = L_WORKLIST_TRANS_ID
                AND TRANS_TYPE = L_WORKLIST_CATEGORY
                AND ACTION_TYPE_TAKEN IS NULL;

         IF L_NXT_ACTION_HISTORY_ID IS NULL
         THEN
            OUT_IS_FINAL_APPROVER := 'Y';
         ELSE
            OUT_IS_FINAL_APPROVER := 'N';

            SELECT PERSON_ID
              INTO OUT_NEXT_APPROVER_ID
              FROM AS_SCHEDULER_ACTION_HISTORY
             WHERE SCHEDULER_ACTION_HISTORY_ID = L_NXT_ACTION_HISTORY_ID;
         END IF;
      END IF;
   END;

   PROCEDURE REPEAT_PATTERN (IN_SCHEDULER_STAFF_ID       NUMBER,
                             OUT_RESULT              OUT VARCHAR2)
   IS
      L_START_TIME_HOURS       AS_SCHEDULER_STAFF.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES     AS_SCHEDULER_STAFF.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS         AS_SCHEDULER_STAFF.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES       AS_SCHEDULER_STAFF.END_TIME_MINUTES%TYPE;
      L_SHIFT_ID               AS_SCHEDULER_STAFF.SHIFT_ID%TYPE;
      L_WORK_GROUP_ID          AS_SCHEDULER_STAFF.WORK_GROUP_ID%TYPE;
      L_SCHEDULAR_START_DATE   AS_SCHEDULER_STAFF.SCHEDULAR_START_DATE%TYPE;
      L_SCHEDULER_ID           AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID              AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_BULLETIN_FLAG          AS_SCHEDULER_STAFF.BULLETIN_FLAG%TYPE;
      L_PATTERN_ID             NUMBER;
      L_SCHDLR_END_DATE        DATE;
      L_SKIP_FIRST             BOOLEAN := FALSE;
      L_SCHEDULER_STAFF_ID     NUMBER;
      L_RECORD_EXISTS          NUMBER;
   BEGIN
      SELECT ASS.START_TIME_HOURS,
             ASS.START_TIME_MINUTES,
             ASS.END_TIME_HOURS,
             ASS.END_TIME_MINUTES,
             ASS.SHIFT_ID,
             ASS.WORK_GROUP_ID,
             ASS.SCHEDULAR_START_DATE,
             ASS.SCHEDULER_ID,
             ASS.PERSON_ID,
             ASS.BULLETIN_FLAG,
             ASCH.END_DATE
        INTO L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_SHIFT_ID,
             L_WORK_GROUP_ID,
             L_SCHEDULAR_START_DATE,
             L_SCHEDULER_ID,
             L_PERSON_ID,
             L_BULLETIN_FLAG,
             L_SCHDLR_END_DATE
        FROM AS_SCHEDULER_STAFF ASS, AS_SCHEDULERS ASCH
       WHERE ASS.SCHEDULER_ID = ASCH.SCHEDULER_ID
             AND ASS.SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      SELECT PATTERN_ID
        INTO L_PATTERN_ID
        FROM AS_SHIFTS
       WHERE SHIFT_ID = L_SHIFT_ID;

      WHILE TRUNC (L_SCHEDULAR_START_DATE) <= TRUNC (L_SCHDLR_END_DATE)
      LOOP
         FOR PTRN_REC IN (  SELECT ON_OFF_FLAG, NO_OF_DAYS
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = L_PATTERN_ID
                          ORDER BY ORDER_SEQ ASC)
         LOOP
            FOR DAYS_REC IN (    SELECT LEVEL
                                   FROM DUAL
                             CONNECT BY LEVEL <= PTRN_REC.NO_OF_DAYS)
            LOOP
               IF L_SKIP_FIRST
               THEN
                  L_SCHEDULAR_START_DATE := L_SCHEDULAR_START_DATE + 1;

                  IF TRUNC (L_SCHEDULAR_START_DATE) <=
                        TRUNC (L_SCHDLR_END_DATE)
                  THEN
                     IF PTRN_REC.ON_OFF_FLAG = 'ON'
                     THEN
                        L_RECORD_EXISTS :=
                           GET_SCHDLR_COUNT_ON_DATE (L_SCHEDULER_ID,
                                                     L_PERSON_ID,
                                                     L_SCHEDULAR_START_DATE);


                        IF L_RECORD_EXISTS = 0
                        THEN
                           L_SCHEDULER_STAFF_ID :=
                              AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

                           INSERT
                             INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                                      SCHEDULER_ID,
                                                      PERSON_ID,
                                                      SCHEDULAR_START_DATE,
                                                      SCHEDULAR_END_DATE,
                                                      START_TIME_HOURS,
                                                      START_TIME_MINUTES,
                                                      END_TIME_HOURS,
                                                      END_TIME_MINUTES,
                                                      SHIFT_ID,
                                                      WORK_GROUP_ID,
                                                      BULLETIN_FLAG)
                           VALUES (L_SCHEDULER_STAFF_ID,
                                   L_SCHEDULER_ID,
                                   L_PERSON_ID,
                                   L_SCHEDULAR_START_DATE,
                                   L_SCHEDULAR_START_DATE,
                                   L_START_TIME_HOURS,
                                   L_START_TIME_MINUTES,
                                   L_END_TIME_HOURS,
                                   L_END_TIME_MINUTES,
                                   L_SHIFT_ID,
                                   L_WORK_GROUP_ID,
                                   L_BULLETIN_FLAG);

                           INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                                          SCHEDULER_STAFF_BULLETIN_ID,
                                          SCHEDULER_STAFF_ID,
                                          BULLETIN_ID)
                              SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                                     L_SCHEDULER_STAFF_ID,
                                     BULLETIN_ID
                                FROM AS_SCHEDULER_STAFF_BULLETINS
                               WHERE SCHEDULER_STAFF_ID =
                                        IN_SCHEDULER_STAFF_ID;
                        END IF;
                     --
                     END IF;
                  END IF;
               ELSE
                  L_SKIP_FIRST := TRUE;
               END IF;
            END LOOP;
         END LOOP;
      END LOOP;

      COMMIT;
      OUT_RESULT := 'SUCCESS';
   EXCEPTION
      WHEN OTHERS
      THEN
         OUT_RESULT := 'FAIL';
   END REPEAT_PATTERN;

   PROCEDURE DELETE_ALL_SCHDLR (IN_SCHEDULER_STAFF_ID NUMBER)
   IS
      L_SCHEDULER_ID   AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID      AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
   BEGIN
      SELECT SCHEDULER_ID, PERSON_ID
        INTO L_SCHEDULER_ID, L_PERSON_ID
        FROM AS_SCHEDULER_STAFF
       WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = L_SCHEDULER_ID AND PERSON_ID = L_PERSON_ID;

      COMMIT;
   END DELETE_ALL_SCHDLR;

   FUNCTION GET_WRKGRP_ANNUAL_LEAVE_USED (IN_WORK_GROUP_ID NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      RETURN 0;
   END GET_WRKGRP_ANNUAL_LEAVE_USED;

   FUNCTION GET_SCHDLR_COUNT_ON_DATE (IN_SCHEDULER_ID            NUMBER,
                                      IN_PERSON_ID               NUMBER,
                                      IN_SCHEDULAR_START_DATE    DATE)
      RETURN NUMBER
   IS
      L_RECORD_EXISTS   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO L_RECORD_EXISTS
        FROM AS_SCHEDULER_STAFF
       WHERE PERSON_ID = IN_PERSON_ID AND SCHEDULER_ID = IN_SCHEDULER_ID
             AND TRUNC (SCHEDULAR_START_DATE) =
                    TRUNC (IN_SCHEDULAR_START_DATE);

      RETURN L_RECORD_EXISTS;
   END GET_SCHDLR_COUNT_ON_DATE;

   PROCEDURE SCHDLR_INTERSECT_ASSIGNMENT (
      IN_SCHEDULER_ID               NUMBER,
      IN_PERSON_ID                  NUMBER,
      IN_SCHEDULAR_START_DATE       DATE,
      OUT_IS_EXIST              OUT VARCHAR2,
      OUT_EXIST_MESSAGE         OUT VARCHAR2)
   IS
      L_COUNT   NUMBER;
   BEGIN
      OUT_IS_EXIST := 'N';
      OUT_EXIST_MESSAGE := NULL;

      SELECT COUNT (1)
        INTO L_COUNT
        FROM AS_ASSIGNMENT_EMPS ASE
       WHERE PERSON_ID = IN_PERSON_ID
             AND TRUNC (IN_SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                            EMP_START_DATE)
                                                     AND TRUNC (EMP_END_DATE);

      IF L_COUNT > 0
      THEN
         OUT_IS_EXIST := 'Y';

         SELECT    'Employee is entered on assignment '
                || ASSIGN.ASSIGNMENT_NAME
                || ' from '
                || TO_CHAR (ASE.EMP_START_DATE, 'dd-MON-rrrr')
                || ' to '
                || TO_CHAR (ASE.EMP_END_DATE, 'dd-MON-rrrr')
           INTO OUT_EXIST_MESSAGE
           FROM AS_ASSIGNMENT_EMPS ASE, AS_ASSIGNMENTS ASSIGN
          WHERE ASE.ASSIGNMENT_ID = ASSIGN.ASSIGNMENT_ID
                AND PERSON_ID = IN_PERSON_ID
                AND TRUNC (IN_SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                               EMP_START_DATE)
                                                        AND TRUNC (
                                                               EMP_END_DATE)
                AND ROWNUM < 2;
      END IF;
   END SCHDLR_INTERSECT_ASSIGNMENT;

   PROCEDURE SCHDLR_INTERSECT_LEAVE (IN_SCHEDULER_ID               NUMBER,
                                     IN_PERSON_ID                  NUMBER,
                                     IN_SCHEDULAR_START_DATE       DATE,
                                     OUT_IS_EXIST              OUT VARCHAR2,
                                     OUT_EXIST_MESSAGE         OUT VARCHAR2)
   IS
      L_COUNT   NUMBER;
   BEGIN
      OUT_IS_EXIST := 'N';
      OUT_EXIST_MESSAGE := NULL;

      SELECT COUNT (1)
        INTO L_COUNT
        FROM per_absence_attendances paa
       WHERE PERSON_ID = IN_PERSON_ID
             AND TRUNC (IN_SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                            NVL (
                                                               paa.date_start,
                                                               paa.date_projected_start))
                                                     AND TRUNC (
                                                            NVL (
                                                               paa.date_end,
                                                               paa.date_projected_end));



      IF L_COUNT > 0
      THEN
         OUT_IS_EXIST := 'Y';

         SELECT 'Employee has leave ' || ' from '
                || TO_CHAR (NVL (paa.date_start, paa.date_projected_start),
                            'dd-MON-rrrr')
                || ' to '
                || TO_CHAR (NVL (paa.date_end, paa.date_projected_end),
                            'dd-MON-rrrr')
           INTO OUT_EXIST_MESSAGE
           FROM per_absence_attendances paa
          WHERE PERSON_ID = IN_PERSON_ID
                AND TRUNC (IN_SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                               NVL (
                                                                  paa.date_start,
                                                                  paa.date_projected_start))
                                                        AND TRUNC (
                                                               NVL (
                                                                  paa.date_end,
                                                                  paa.date_projected_end))
                AND ROWNUM < 2;
      END IF;
   END SCHDLR_INTERSECT_LEAVE;

   PROCEDURE SUBMIT_SCHDLR_TRANS (IN_SCHEDULER_TRANS_HDR_ID    NUMBER,
                                  IN_PERSON_ID                 NUMBER)
   IS
      L_TRANS_TYPE          AS_SCHEDULER_TRANS_HDRS.TRANS_TYPE%TYPE;
      L_FIRST_APPROVER_ID   NUMBER;

      L_NOTIFICATION_ID     NUMBER;
      L_ACCESS_KEY          VARCHAR2 (80);
      L_NTF_SUBJECT         VARCHAR2 (512);
      L__NTF_BODY           CLOB;
      L_OUT_STATUS          VARCHAR2 (256);
   BEGIN
      SELECT TRANS_TYPE
        INTO L_TRANS_TYPE
        FROM AS_SCHEDULER_TRANS_HDRS
       WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

      UPDATE AS_SCHEDULER_TRANS_HDRS
         SET APPROVAL_STATUS = 'WAITING_APPROVAL'
       WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

      INSERT INTO AS_SCHEDULER_ACTION_HISTORY (SCHEDULER_ACTION_HISTORY_ID,
                                               TRANS_TYPE,
                                               TRANS_ID,
                                               ORDER_SEQ,
                                               PERSON_ID,
                                               ACTION_TYPE,
                                               ACTION_TYPE_TAKEN)
           VALUES (AS_SCHEDULER_ACTION_HIST_SEQ.NEXTVAL,
                   'TRANS',
                   IN_SCHEDULER_TRANS_HDR_ID,
                   1,
                   IN_PERSON_ID,
                   'SUBMIT',
                   'Submit for Approval');

      INSERT INTO AS_SCHEDULER_ACTION_HISTORY (SCHEDULER_ACTION_HISTORY_ID,
                                               TRANS_TYPE,
                                               TRANS_ID,
                                               ORDER_SEQ,
                                               PERSON_ID,
                                               ACTION_TYPE)
         SELECT AS_SCHEDULER_ACTION_HIST_SEQ.NEXTVAL,
                'TRANS',
                IN_SCHEDULER_TRANS_HDR_ID,
                ORDER_SEQ + 1,
                PERSON_ID,
                ACTION_TYPE
           FROM (  SELECT ROWNUM ORDER_SEQ, PERSON_ID, ACTION_TYPE
                     FROM AS_APPROVAL_TYPE_STEPS
                    WHERE APPROVAL_TYPE = L_TRANS_TYPE
                 ORDER BY ORDER_SEQ);


      BEGIN
         SELECT PERSON_ID
           INTO L_FIRST_APPROVER_ID
           FROM (  SELECT ROWNUM ORDER_SEQ, PERSON_ID, ACTION_TYPE
                     FROM AS_APPROVAL_TYPE_STEPS
                    WHERE APPROVAL_TYPE = L_TRANS_TYPE
                 ORDER BY ORDER_SEQ)
          WHERE ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;


      L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
      L_ACCESS_KEY := TO_CHAR (FND_CRYPTO.RANDOMNUMBER);

      AS_NTF_CONTENT_PKG.GET_TRANS_NTF (
         IN_SCHEDULER_TRANS_HDR_ID   => IN_SCHEDULER_TRANS_HDR_ID,
         IN_NOTIFICATION_ID          => L_NOTIFICATION_ID,
         IN_ACCESS_KEY               => L_ACCESS_KEY,
         OUT_NTF_SUBJECT             => L_NTF_SUBJECT,
         OUT_NTF_BODY                => L__NTF_BODY);

      AS_NOTIFICATIONS_PKG.INSERT_NOTIFICATION_API (
         P_NOTIFICATION_TYPE   => 'TRANS',
         P_SEND_SYSTEM         => 'SCHEDULAR',
         P_SYSTEM_KEY          => IN_SCHEDULER_TRANS_HDR_ID,
         P_FROM_EMAIL          => 'JASMINA.MUSIC@MBC.NET',
         P_RECEIPTS_PERSONID   => L_FIRST_APPROVER_ID,
         P_RECEIPTS_EMAIL      => 'JASMINA.MUSIC@MBC.NET',
         P_CC_EMAIL            => NULL,
         P_BCC_EMAIL           => NULL,
         P_ACCESS_KEY          => L_ACCESS_KEY,
         P_NTF_SUBJECT         => L_NTF_SUBJECT,
         P_NTF_BODY            => L__NTF_BODY,
         OUT_NOTIFICATION_ID   => L_NOTIFICATION_ID,
         OUT_STATUS            => L_OUT_STATUS);

      INSERT INTO AS_WORKLIST (WORKLIST_ID,
                               WORKLIST_CATEGORY,
                               WORKLIST_ACTION_TYPE,
                               WORKLIST_DATE,
                               WORKLIST_SUBJECT,
                               WORKLIST_STATUS,
                               WORKLIST_TRANS_ID,
                               WORKLIST_PERSON_ID,
                               WORKLIST_DUE_DATE,
                               NOTIFICATION_ID)
           VALUES (AS_WORKLIST_SEQ.NEXTVAL,
                   'TRANS',
                   'RESPOND',
                   SYSTIMESTAMP,
                   L_NTF_SUBJECT,
                   'OPEN',
                   IN_SCHEDULER_TRANS_HDR_ID,
                   L_FIRST_APPROVER_ID,
                   NULL,
                   L_NOTIFICATION_ID);

      COMMIT;
   END SUBMIT_SCHDLR_TRANS;

   PROCEDURE APPLY_SCHDLR_TRANS (IN_SCHEDULER_TRANS_HDR_ID        NUMBER,
                                 OUT_GEN_SCHEDULER_STAFF_ID   OUT NUMBER,
                                 OUT_STATUS                   OUT VARCHAR2)
   IS
      L_SCHEDULER_STAFF_ID       AS_SCHEDULER_TRANS_HDRS.SCHEDULER_STAFF_ID%TYPE;
      L_TRANS_TYPE               AS_SCHEDULER_TRANS_HDRS.TRANS_TYPE%TYPE;
      L_SUGGESTED_PERSON_ID      AS_SCHEDULER_TRANS_HDRS.SUGGESTED_PERSON_ID%TYPE;
      L_NEW_SCHEDULER_STAFF_ID   AS_SCHEDULER_TRANS_HDRS.SCHEDULER_STAFF_ID%TYPE;
      L_START_TIME_HOURS         AS_SCHEDULER_TRANS_HDRS.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES       AS_SCHEDULER_TRANS_HDRS.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS           AS_SCHEDULER_TRANS_HDRS.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES         AS_SCHEDULER_TRANS_HDRS.END_TIME_MINUTES%TYPE;
      L_EFFECTIVE_START_DATE     AS_SCHEDULER_TRANS_HDRS.EFFECTIVE_START_DATE%TYPE;
      L_EFFECTIVE_END_DATE       AS_SCHEDULER_TRANS_HDRS.EFFECTIVE_END_DATE%TYPE;
      L_ASSIGNMENT_ID            AS_SCHEDULER_TRANS_HDRS.ASSIGNMENT_ID%TYPE;
      L_CHANGE_REASON            AS_SCHEDULER_TRANS_HDRS.CHANGE_REASON%TYPE;
      L_FIRST_APPROVER_ID        NUMBER;

      L_SCHEDULER_ID             AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID                AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_NTF_SUBJECT              VARCHAR2 (1024);
      L_NTF_BODY                 CLOB;
      L_OUT_STATUS               VARCHAR2 (512);
      L_COUNT                    NUMBER;
   BEGIN
      OUT_STATUS := 'SUCCESS';

      SELECT SCHEDULER_STAFF_ID,
             TRANS_TYPE,
             SUGGESTED_PERSON_ID,
             START_TIME_HOURS,
             START_TIME_MINUTES,
             END_TIME_HOURS,
             END_TIME_MINUTES,
             EFFECTIVE_START_DATE,
             EFFECTIVE_END_DATE,
             ASSIGNMENT_ID,
             CHANGE_REASON
        INTO L_SCHEDULER_STAFF_ID,
             L_TRANS_TYPE,
             L_SUGGESTED_PERSON_ID,
             L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_EFFECTIVE_START_DATE,
             L_EFFECTIVE_END_DATE,
             L_ASSIGNMENT_ID,
             L_CHANGE_REASON
        FROM AS_SCHEDULER_TRANS_HDRS
       WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

      SELECT SCHEDULER_ID, PERSON_ID
        INTO L_SCHEDULER_ID, L_PERSON_ID
        FROM AS_SCHEDULER_STAFF ASS
       WHERE SCHEDULER_STAFF_ID = L_SCHEDULER_STAFF_ID;

      IF L_CHANGE_REASON = 'ASSIGNMENT' AND L_ASSIGNMENT_ID IS NOT NULL
      THEN
         SELECT COUNT (1)
           INTO L_COUNT
           FROM AS_ASSIGNMENT_EMPS ASE
          WHERE ASE.ASSIGNMENT_ID = L_ASSIGNMENT_ID
                AND ASE.PERSON_ID = L_PERSON_ID
                AND (TRUNC (L_EFFECTIVE_START_DATE) BETWEEN TRUNC (
                                                               ASE.EMP_START_DATE)
                                                        AND TRUNC (
                                                               ASE.EMP_END_DATE)
                     OR TRUNC (L_EFFECTIVE_END_DATE) BETWEEN TRUNC (
                                                                ASE.EMP_START_DATE)
                                                         AND TRUNC (
                                                                ASE.EMP_END_DATE));

         IF L_COUNT = 0
         THEN
            INSERT INTO AS_ASSIGNMENT_EMPS (ASSIGNMENT_EMP_ID,
                                            ASSIGNMENT_ID,
                                            PERSON_ID,
                                            EMP_START_DATE,
                                            EMP_END_DATE,
                                            NOTES)
                 VALUES (AS_ASSIGNMENT_EMPS_SEQ.NEXTVAL,
                         L_ASSIGNMENT_ID,
                         L_PERSON_ID,
                         L_EFFECTIVE_START_DATE,
                         L_EFFECTIVE_END_DATE,
                         'Automatic added because of Request Submission.');
         END IF;
      END IF;

      IF L_TRANS_TYPE = 'REPLACE'
      THEN
         FOR SCHDLR_REC
            IN (SELECT SCHEDULER_STAFF_ID,
                       SCHEDULER_ID,
                       SCHEDULAR_START_DATE,
                       SCHEDULAR_END_DATE,
                       START_TIME_HOURS,
                       START_TIME_MINUTES,
                       END_TIME_HOURS,
                       END_TIME_MINUTES,
                       SHIFT_ID,
                       WORK_GROUP_ID,
                       BULLETIN_FLAG,
                       PARENT_SCHEDULER_STAFF_ID
                  FROM AS_SCHEDULER_STAFF
                 WHERE SCHEDULER_STAFF_ID = L_SCHEDULER_STAFF_ID
                       OR (PERSON_ID = L_PERSON_ID
                           AND SCHEDULER_ID = L_SCHEDULER_ID
                           AND TRUNC (SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                                       L_EFFECTIVE_START_DATE)
                                                                AND TRUNC (
                                                                       L_EFFECTIVE_END_DATE)))
         LOOP
            L_NEW_SCHEDULER_STAFF_ID := AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

            INSERT INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                            SCHEDULER_ID,
                                            PERSON_ID,
                                            SCHEDULAR_START_DATE,
                                            SCHEDULAR_END_DATE,
                                            START_TIME_HOURS,
                                            START_TIME_MINUTES,
                                            END_TIME_HOURS,
                                            END_TIME_MINUTES,
                                            SHIFT_ID,
                                            WORK_GROUP_ID,
                                            RECURRING_FLAG,
                                            BULLETIN_FLAG)
                 VALUES (L_NEW_SCHEDULER_STAFF_ID,
                         SCHDLR_REC.SCHEDULER_ID,
                         L_SUGGESTED_PERSON_ID,
                         SCHDLR_REC.SCHEDULAR_START_DATE,
                         SCHDLR_REC.SCHEDULAR_END_DATE,
                         SCHDLR_REC.START_TIME_HOURS,
                         SCHDLR_REC.START_TIME_MINUTES,
                         SCHDLR_REC.END_TIME_HOURS,
                         SCHDLR_REC.END_TIME_MINUTES,
                         SCHDLR_REC.SHIFT_ID,
                         SCHDLR_REC.WORK_GROUP_ID,
                         'N',
                         SCHDLR_REC.BULLETIN_FLAG);


            IF SCHDLR_REC.BULLETIN_FLAG = 'Y'
            THEN
               INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                              SCHEDULER_STAFF_BULLETIN_ID,
                              SCHEDULER_STAFF_ID,
                              BULLETIN_ID)
                  SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                         L_NEW_SCHEDULER_STAFF_ID,
                         BULLETIN_ID
                    FROM AS_SCHEDULER_STAFF_BULLETINS
                   WHERE SCHEDULER_STAFF_ID = SCHDLR_REC.SCHEDULER_STAFF_ID;
            END IF;

            UPDATE AS_SCHEDULER_STAFF
               SET PARENT_SCHEDULER_STAFF_ID = L_NEW_SCHEDULER_STAFF_ID
             WHERE SCHEDULER_STAFF_ID = SCHDLR_REC.SCHEDULER_STAFF_ID;

            UPDATE AS_SCHEDULER_TRANS_HDRS
               SET GENERATED_SCHEDULER_STAFF_ID = L_NEW_SCHEDULER_STAFF_ID
             WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

            OUT_GEN_SCHEDULER_STAFF_ID := L_NEW_SCHEDULER_STAFF_ID;
         END LOOP;
      ELSIF L_TRANS_TYPE = 'CHANGE'
      THEN
         FOR SCHDLR_REC
            IN (SELECT SCHEDULER_STAFF_ID,
                       SCHEDULER_ID,
                       PERSON_ID,
                       SCHEDULAR_START_DATE,
                       SCHEDULAR_END_DATE,
                       START_TIME_HOURS,
                       START_TIME_MINUTES,
                       END_TIME_HOURS,
                       END_TIME_MINUTES,
                       SHIFT_ID,
                       WORK_GROUP_ID,
                       BULLETIN_FLAG,
                       PARENT_SCHEDULER_STAFF_ID
                  FROM AS_SCHEDULER_STAFF
                 WHERE SCHEDULER_STAFF_ID = L_SCHEDULER_STAFF_ID
                       OR (PERSON_ID = L_PERSON_ID
                           AND SCHEDULER_ID = L_SCHEDULER_ID
                           AND TRUNC (SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                                       L_EFFECTIVE_START_DATE)
                                                                AND TRUNC (
                                                                       L_EFFECTIVE_END_DATE)))
         LOOP
            L_NEW_SCHEDULER_STAFF_ID := AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

            INSERT INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                            SCHEDULER_ID,
                                            PERSON_ID,
                                            SCHEDULAR_START_DATE,
                                            SCHEDULAR_END_DATE,
                                            START_TIME_HOURS,
                                            START_TIME_MINUTES,
                                            END_TIME_HOURS,
                                            END_TIME_MINUTES,
                                            SHIFT_ID,
                                            WORK_GROUP_ID,
                                            RECURRING_FLAG,
                                            BULLETIN_FLAG)
                 VALUES (L_NEW_SCHEDULER_STAFF_ID,
                         SCHDLR_REC.SCHEDULER_ID,
                         SCHDLR_REC.PERSON_ID,
                         SCHDLR_REC.SCHEDULAR_START_DATE,
                         SCHDLR_REC.SCHEDULAR_END_DATE,
                         L_START_TIME_HOURS,
                         L_START_TIME_MINUTES,
                         L_END_TIME_HOURS,
                         L_END_TIME_MINUTES,
                         SCHDLR_REC.SHIFT_ID,
                         SCHDLR_REC.WORK_GROUP_ID,
                         'N',
                         SCHDLR_REC.BULLETIN_FLAG);


            IF SCHDLR_REC.BULLETIN_FLAG = 'Y'
            THEN
               INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                              SCHEDULER_STAFF_BULLETIN_ID,
                              SCHEDULER_STAFF_ID,
                              BULLETIN_ID)
                  SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                         L_NEW_SCHEDULER_STAFF_ID,
                         BULLETIN_ID
                    FROM AS_SCHEDULER_STAFF_BULLETINS
                   WHERE SCHEDULER_STAFF_ID = SCHDLR_REC.SCHEDULER_STAFF_ID;
            END IF;

            UPDATE AS_SCHEDULER_STAFF
               SET PARENT_SCHEDULER_STAFF_ID = L_NEW_SCHEDULER_STAFF_ID
             WHERE SCHEDULER_STAFF_ID = SCHDLR_REC.SCHEDULER_STAFF_ID;

            UPDATE AS_SCHEDULER_TRANS_HDRS
               SET GENERATED_SCHEDULER_STAFF_ID = L_NEW_SCHEDULER_STAFF_ID
             WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;


            OUT_GEN_SCHEDULER_STAFF_ID := L_NEW_SCHEDULER_STAFF_ID;
         END LOOP;
      ELSIF L_TRANS_TYPE = 'CANCEL'
      THEN
         UPDATE AS_SCHEDULER_STAFF
            SET PARENT_SCHEDULER_STAFF_ID = -1
          WHERE SCHEDULER_STAFF_ID = L_SCHEDULER_STAFF_ID
                OR (PERSON_ID = L_PERSON_ID AND SCHEDULER_ID = L_SCHEDULER_ID
                    AND TRUNC (SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                                L_EFFECTIVE_START_DATE)
                                                         AND TRUNC (
                                                                L_EFFECTIVE_END_DATE));

         UPDATE AS_SCHEDULER_TRANS_HDRS
            SET GENERATED_SCHEDULER_STAFF_ID = -1
          WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

         OUT_GEN_SCHEDULER_STAFF_ID := -1;
      END IF;



      COMMIT;
   END APPLY_SCHDLR_TRANS;

   PROCEDURE SUBMIT_SCHEDULER (IN_SCHEDULER_ID       NUMBER,
                               IN_PERSON_ID          NUMBER,
                               OUT_STATUS        OUT VARCHAR2)
   IS
      L_FIRST_APPROVER_ID   NUMBER;

      L_NOTIFICATION_ID     NUMBER;
      L_ACCESS_KEY          VARCHAR2 (80);
      L_NTF_SUBJECT         VARCHAR2 (512);
      L__NTF_BODY           CLOB;
      L_OUT_STATUS          VARCHAR2 (256);
   BEGIN
      OUT_STATUS := 'SUCCESS';

      UPDATE AS_SCHEDULERS
         SET SCHEDULAR_STATUS = 'WAITING_APPROVAL'
       WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      INSERT INTO AS_SCHEDULER_ACTION_HISTORY (SCHEDULER_ACTION_HISTORY_ID,
                                               TRANS_TYPE,
                                               TRANS_ID,
                                               ORDER_SEQ,
                                               PERSON_ID,
                                               ACTION_TYPE,
                                               ACTION_TYPE_TAKEN)
           VALUES (AS_SCHEDULER_ACTION_HIST_SEQ.NEXTVAL,
                   'SCHDLR',
                   IN_SCHEDULER_ID,
                   1,
                   IN_PERSON_ID,
                   'SUBMIT',
                   'Submit for Approval');

      INSERT INTO AS_SCHEDULER_ACTION_HISTORY (SCHEDULER_ACTION_HISTORY_ID,
                                               TRANS_TYPE,
                                               TRANS_ID,
                                               ORDER_SEQ,
                                               PERSON_ID,
                                               ACTION_TYPE)
         SELECT AS_SCHEDULER_ACTION_HIST_SEQ.NEXTVAL,
                'SCHDLR',
                IN_SCHEDULER_ID,
                ORDER_SEQ + 1,
                PERSON_ID,
                ACTION_TYPE
           FROM (  SELECT ROWNUM ORDER_SEQ, PERSON_ID, ACTION_TYPE
                     FROM AS_APPROVAL_TYPE_STEPS
                    WHERE APPROVAL_TYPE = 'SCHEDULAR'
                 ORDER BY ORDER_SEQ);

      BEGIN
         SELECT PERSON_ID
           INTO L_FIRST_APPROVER_ID
           FROM (  SELECT ROWNUM ORDER_SEQ, PERSON_ID, ACTION_TYPE
                     FROM AS_APPROVAL_TYPE_STEPS
                    WHERE APPROVAL_TYPE = 'SCHEDULAR'
                 ORDER BY ORDER_SEQ)
          WHERE ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;

      L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
      L_ACCESS_KEY := TO_CHAR (FND_CRYPTO.RANDOMNUMBER);

      AS_NTF_CONTENT_PKG.GET_SCHDLR_NTF (
         IN_SCHEDULER_ID      => IN_SCHEDULER_ID,
         IN_NOTIFICATION_ID   => L_NOTIFICATION_ID,
         IN_ACCESS_KEY        => L_ACCESS_KEY,
         OUT_NTF_SUBJECT      => L_NTF_SUBJECT,
         OUT_NTF_BODY         => L__NTF_BODY);

      AS_NOTIFICATIONS_PKG.INSERT_NOTIFICATION_API (
         P_NOTIFICATION_TYPE   => 'SCHDLR',
         P_SEND_SYSTEM         => 'SCHEDULAR',
         P_SYSTEM_KEY          => IN_SCHEDULER_ID,
         P_FROM_EMAIL          => 'JASMINA.MUSIC@MBC.NET',
         P_RECEIPTS_PERSONID   => L_FIRST_APPROVER_ID,
         P_RECEIPTS_EMAIL      => 'JASMINA.MUSIC@MBC.NET',
         P_CC_EMAIL            => NULL,
         P_BCC_EMAIL           => NULL,
         P_ACCESS_KEY          => L_ACCESS_KEY,
         P_NTF_SUBJECT         => L_NTF_SUBJECT,
         P_NTF_BODY            => L__NTF_BODY,
         OUT_NOTIFICATION_ID   => L_NOTIFICATION_ID,
         OUT_STATUS            => L_OUT_STATUS);

      INSERT INTO AS_WORKLIST (WORKLIST_ID,
                               WORKLIST_CATEGORY,
                               WORKLIST_ACTION_TYPE,
                               WORKLIST_DATE,
                               WORKLIST_SUBJECT,
                               WORKLIST_STATUS,
                               WORKLIST_TRANS_ID,
                               WORKLIST_PERSON_ID,
                               WORKLIST_DUE_DATE,
                               NOTIFICATION_ID)
           VALUES (AS_WORKLIST_SEQ.NEXTVAL,
                   'SCHDLR',
                   'RESPOND',
                   SYSTIMESTAMP,
                   L_NTF_SUBJECT,
                   'OPEN',
                   IN_SCHEDULER_ID,
                   L_FIRST_APPROVER_ID,
                   NULL,
                   L_NOTIFICATION_ID);

      COMMIT;
   END SUBMIT_SCHEDULER;

   PROCEDURE PUSH_SCHEDULER_CALENDAR (IN_SCHEDULER_ID       NUMBER,
                                      OUT_STATUS        OUT VARCHAR2)
   IS
      CURSOR STAFF_CUR
      IS
         SELECT DISTINCT ASSCHEDULERSTAFFEO.SCHEDULER_ID,
                         ASSCHEDULERSTAFFEO.PERSON_ID,
                         ASSCHEDULERSTAFFEO.SHIFT_ID,
                         ASSCHEDULERSTAFFEO.WORK_GROUP_ID,
                         PAPF.EMPLOYEE_NUMBER,
                         PAPF.FULL_NAME EMPLOYEE_NAME,
                         PAPF.EMAIL_ADDRESS,
                         ASS.SHIFT_NAME,
                         AWGH.WORK_GROUP_NAME
           FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,
                PER_ALL_PEOPLE_F PAPF,
                AS_SCHEDULERS SCHDLR,
                AS_SHIFTS ASS,
                AS_WORK_GROUPS_HDR AWGH
          WHERE ASSCHEDULERSTAFFEO.PERSON_ID = PAPF.PERSON_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = SCHDLR.SCHEDULER_ID
                AND ASSCHEDULERSTAFFEO.SHIFT_ID = ASS.SHIFT_ID(+)
                AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID = AWGH.WORK_GROUP_ID(+)
                AND ASSCHEDULERSTAFFEO.PARENT_SCHEDULER_STAFF_ID IS NULL
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = IN_SCHEDULER_ID;

      CURSOR STAFF_SCHDLR_CUR (
         IN_PERSON_ID NUMBER)
      IS
         SELECT ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID,
                ASSCHEDULERSTAFFEO.SCHEDULER_ID,
                ASSCHEDULERSTAFFEO.PERSON_ID,
                ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE,
                ASSCHEDULERSTAFFEO.SCHEDULAR_END_DATE,
                ASSCHEDULERSTAFFEO.START_TIME_HOURS,
                ASSCHEDULERSTAFFEO.START_TIME_MINUTES,
                ASSCHEDULERSTAFFEO.END_TIME_HOURS,
                ASSCHEDULERSTAFFEO.END_TIME_MINUTES,
                ASSCHEDULERSTAFFEO.SHIFT_ID,
                ASSCHEDULERSTAFFEO.WORK_GROUP_ID,
                ASSCHEDULERSTAFFEO.RECURRING_FLAG,
                ASSCHEDULERSTAFFEO.BULLETIN_FLAG,
                ASSCHEDULERSTAFFEO.PARENT_SCHEDULER_STAFF_ID,
                PAPF.EMPLOYEE_NUMBER,
                PAPF.FULL_NAME EMPLOYEE_NAME,
                PAPF.EMAIL_ADDRESS,
                ASS.SHIFT_NAME,
                AWGH.WORK_GROUP_NAME,
                SCHDLR.SCHEDULER_TITLE
           FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,
                PER_ALL_PEOPLE_F PAPF,
                AS_SCHEDULERS SCHDLR,
                AS_SHIFTS ASS,
                AS_WORK_GROUPS_HDR AWGH
          WHERE ASSCHEDULERSTAFFEO.PERSON_ID = PAPF.PERSON_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = SCHDLR.SCHEDULER_ID
                AND ASSCHEDULERSTAFFEO.SHIFT_ID = ASS.SHIFT_ID(+)
                AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID = AWGH.WORK_GROUP_ID(+)
                AND ASSCHEDULERSTAFFEO.PARENT_SCHEDULER_STAFF_ID IS NULL
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = IN_SCHEDULER_ID
                AND ASSCHEDULERSTAFFEO.PERSON_ID = IN_PERSON_ID; --1442;-- IN_PERSON_ID;


      L_NTF_SUBJECT   VARCHAR2 (1024);
      L_NTF_BODY      CLOB;
   BEGIN
      OUT_STATUS := 'SUCCESS';

      FOR STAFF_REC IN STAFF_CUR
      LOOP
         FOR STAFF_SCHDLR_REC IN STAFF_SCHDLR_CUR (STAFF_REC.PERSON_ID)
         LOOP
            AS_NTF_CONTENT_PKG.GET_SCHDLR_CAL (
               IN_SCHEDULER_ID         => IN_SCHEDULER_ID,
               IN_SCHEDULER_STAFF_ID   => STAFF_SCHDLR_REC.SCHEDULER_STAFF_ID,
               OUT_CAL_SUBJECT         => L_NTF_SUBJECT,
               OUT_CAL_CONTENT         => L_NTF_BODY);



            AS_NOTIFICATIONS_PKG.SEND_CALENDAR (
               P_FROM_EMAIL       => 'rami.abudiab@mbc.net',
               P_RECEIPTS_EMAIL   => 'JASMINA.MUSIC@MBC.NET',
               P_NTF_SUBJECT      => L_NTF_SUBJECT,
               P_NTF_BODY         => L_NTF_BODY,
               OUT_STATUS         => OUT_STATUS);
         END LOOP;
      END LOOP;
   END PUSH_SCHEDULER_CALENDAR;

   PROCEDURE GET_WORKLIST_COUNT (IN_PERSONID        NUMBER,
                                 OUT_APPROVED   OUT NUMBER,
                                 OUT_REJECTED   OUT NUMBER,
                                 OUT_WAITING    OUT NUMBER)
   IS
   BEGIN
      SELECT COUNT (1)
        INTO OUT_APPROVED
        FROM AS_WORKLIST
       WHERE TAKE_ACTION = 'APPROVED' AND WORKLIST_PERSON_ID = IN_PERSONID;

      SELECT COUNT (1)
        INTO OUT_REJECTED
        FROM AS_WORKLIST
       WHERE TAKE_ACTION = 'REJECTED' AND WORKLIST_PERSON_ID = IN_PERSONID;

      SELECT COUNT (1)
        INTO OUT_WAITING
        FROM AS_WORKLIST
       WHERE WORKLIST_STATUS = 'OPEN' AND WORKLIST_PERSON_ID = IN_PERSONID;
   END GET_WORKLIST_COUNT;

   PROCEDURE WORKLIST_ACTION (IN_WORKLIST_ID    NUMBER,
                              IN_ACTION         VARCHAR2,
                              IN_NOTES          VARCHAR2)
   IS
      L_TRANS_TYPE               AS_SCHEDULER_TRANS_HDRS.TRANS_TYPE%TYPE;
      L_WORKLIST_CATEGORY        AS_WORKLIST.WORKLIST_CATEGORY%TYPE;
      L_WORKLIST_TRANS_ID        AS_WORKLIST.WORKLIST_TRANS_ID%TYPE;
      L_WORKLIST_PERSON_ID       AS_WORKLIST.WORKLIST_PERSON_ID%TYPE;
      L_GEN_SCHEDULER_STAFF_ID   NUMBER;
      L_NEXT_APPROVER_ID         NUMBER;
      L_IS_FINAL_APPROVER        VARCHAR2 (1);
      L_STATUS                   VARCHAR2 (1024);

      L_NOTIFICATION_ID          NUMBER;
      L_ACCESS_KEY               VARCHAR2 (80);
      L_NTF_SUBJECT              VARCHAR2 (512);
      L__NTF_BODY                CLOB;
      L_OUT_STATUS               VARCHAR2 (256);
   BEGIN
      SELECT WORKLIST_CATEGORY, WORKLIST_TRANS_ID, WORKLIST_PERSON_ID
        INTO L_WORKLIST_CATEGORY, L_WORKLIST_TRANS_ID, L_WORKLIST_PERSON_ID
        FROM AS_WORKLIST
       WHERE WORKLIST_ID = IN_WORKLIST_ID;

      UPDATE AS_WORKLIST
         SET WORKLIST_STATUS = 'CLOSED',
             TAKE_ACTION = IN_ACTION,
             NOTES = IN_NOTES,
             CLOSE_DATE = SYSTIMESTAMP
       WHERE WORKLIST_ID = IN_WORKLIST_ID;


      UPDATE AS_SCHEDULER_ACTION_HISTORY
         SET ACTION_TYPE_TAKEN = IN_ACTION, NOTES = IN_NOTES
       WHERE     TRANS_ID = L_WORKLIST_TRANS_ID
             AND PERSON_ID = L_WORKLIST_PERSON_ID
             AND TRANS_TYPE = L_WORKLIST_CATEGORY;

      IF L_WORKLIST_CATEGORY = 'SCHDLR'
      THEN
         IF IN_ACTION = 'APPROVED'
         THEN
            AS_SCHDLR_PKG.GET_NEXT_APPROVER (
               IN_WORKLIST_ID          => IN_WORKLIST_ID,
               OUT_IS_FINAL_APPROVER   => L_IS_FINAL_APPROVER,
               OUT_NEXT_APPROVER_ID    => L_NEXT_APPROVER_ID);

            IF L_IS_FINAL_APPROVER = 'Y'
            THEN
               UPDATE AS_SCHEDULERS
                  SET SCHEDULAR_STATUS = 'PUBLISHED'
                WHERE SCHEDULER_ID = L_WORKLIST_TRANS_ID;

               COMMIT;

               PUSH_SCHEDULER_CALENDAR (
                  IN_SCHEDULER_ID   => L_WORKLIST_TRANS_ID,
                  OUT_STATUS        => L_STATUS);
            ELSE
               L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
               L_ACCESS_KEY := TO_CHAR (FND_CRYPTO.RANDOMNUMBER);


               AS_NTF_CONTENT_PKG.GET_SCHDLR_NTF (
                  IN_SCHEDULER_ID      => L_WORKLIST_TRANS_ID,
                  IN_NOTIFICATION_ID   => L_NOTIFICATION_ID,
                  IN_ACCESS_KEY        => L_ACCESS_KEY,
                  OUT_NTF_SUBJECT      => L_NTF_SUBJECT,
                  OUT_NTF_BODY         => L__NTF_BODY);



               AS_NOTIFICATIONS_PKG.INSERT_NOTIFICATION_API (
                  P_NOTIFICATION_TYPE   => 'SCHDLR',
                  P_SEND_SYSTEM         => 'SCHEDULAR',
                  P_SYSTEM_KEY          => L_WORKLIST_TRANS_ID,
                  P_FROM_EMAIL          => 'JASMINA.MUSIC@MBC.NET',
                  P_RECEIPTS_PERSONID   => L_NEXT_APPROVER_ID,
                  P_RECEIPTS_EMAIL      => 'JASMINA.MUSIC@MBC.NET',
                  P_CC_EMAIL            => NULL,
                  P_BCC_EMAIL           => NULL,
                  P_ACCESS_KEY          => L_ACCESS_KEY,
                  P_NTF_SUBJECT         => L_NTF_SUBJECT,
                  P_NTF_BODY            => L__NTF_BODY,
                  OUT_NOTIFICATION_ID   => L_NOTIFICATION_ID,
                  OUT_STATUS            => L_OUT_STATUS);

               INSERT INTO AS_WORKLIST (WORKLIST_ID,
                                        WORKLIST_CATEGORY,
                                        WORKLIST_ACTION_TYPE,
                                        WORKLIST_DATE,
                                        WORKLIST_SUBJECT,
                                        WORKLIST_STATUS,
                                        WORKLIST_TRANS_ID,
                                        WORKLIST_PERSON_ID,
                                        WORKLIST_DUE_DATE,
                                        NOTIFICATION_ID)
                    VALUES (AS_WORKLIST_SEQ.NEXTVAL,
                            'SCHDLR',
                            'RESPOND',
                            SYSTIMESTAMP,
                            L_NTF_SUBJECT,
                            'OPEN',
                            L_WORKLIST_TRANS_ID,
                            L_NEXT_APPROVER_ID,
                            NULL,
                            L_NOTIFICATION_ID);
            END IF;
         ELSIF IN_ACTION = 'REJECTED'
         THEN
            UPDATE AS_SCHEDULERS
               SET SCHEDULAR_STATUS = IN_ACTION
             WHERE SCHEDULER_ID = L_WORKLIST_TRANS_ID;
         END IF;
      ELSIF L_WORKLIST_CATEGORY = 'TRANS'
      THEN
         SELECT TRANS_TYPE
           INTO L_TRANS_TYPE
           FROM AS_SCHEDULER_TRANS_HDRS
          WHERE SCHEDULER_TRANS_HDR_ID = L_WORKLIST_TRANS_ID;


         IF IN_ACTION = 'APPROVED'
         THEN
            AS_SCHDLR_PKG.GET_NEXT_APPROVER (
               IN_WORKLIST_ID          => IN_WORKLIST_ID,
               OUT_IS_FINAL_APPROVER   => L_IS_FINAL_APPROVER,
               OUT_NEXT_APPROVER_ID    => L_NEXT_APPROVER_ID);

            IF L_IS_FINAL_APPROVER = 'Y'
            THEN
               UPDATE AS_SCHEDULER_TRANS_HDRS
                  SET APPROVAL_STATUS = IN_ACTION
                WHERE SCHEDULER_TRANS_HDR_ID = L_WORKLIST_TRANS_ID;

               APPLY_SCHDLR_TRANS (
                  IN_SCHEDULER_TRANS_HDR_ID    => L_WORKLIST_TRANS_ID,
                  OUT_GEN_SCHEDULER_STAFF_ID   => L_GEN_SCHEDULER_STAFF_ID,
                  OUT_STATUS                   => L_STATUS);
            ELSE
               L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
               L_ACCESS_KEY := TO_CHAR (FND_CRYPTO.RANDOMNUMBER);

               AS_NTF_CONTENT_PKG.GET_TRANS_NTF (
                  IN_SCHEDULER_TRANS_HDR_ID   => L_WORKLIST_TRANS_ID,
                  IN_NOTIFICATION_ID          => L_NOTIFICATION_ID,
                  IN_ACCESS_KEY               => L_ACCESS_KEY,
                  OUT_NTF_SUBJECT             => L_NTF_SUBJECT,
                  OUT_NTF_BODY                => L__NTF_BODY);

               AS_NOTIFICATIONS_PKG.INSERT_NOTIFICATION_API (
                  P_NOTIFICATION_TYPE   => 'TRANS',
                  P_SEND_SYSTEM         => 'SCHEDULAR',
                  P_SYSTEM_KEY          => L_WORKLIST_TRANS_ID,
                  P_FROM_EMAIL          => 'JASMINA.MUSIC@MBC.NET',
                  P_RECEIPTS_PERSONID   => L_NEXT_APPROVER_ID,
                  P_RECEIPTS_EMAIL      => 'JASMINA.MUSIC@MBC.NET',
                  P_CC_EMAIL            => NULL,
                  P_BCC_EMAIL           => NULL,
                  P_ACCESS_KEY          => L_ACCESS_KEY,
                  P_NTF_SUBJECT         => L_NTF_SUBJECT,
                  P_NTF_BODY            => L__NTF_BODY,
                  OUT_NOTIFICATION_ID   => L_NOTIFICATION_ID,
                  OUT_STATUS            => L_OUT_STATUS);


               INSERT INTO AS_WORKLIST (WORKLIST_ID,
                                        WORKLIST_CATEGORY,
                                        WORKLIST_ACTION_TYPE,
                                        WORKLIST_DATE,
                                        WORKLIST_SUBJECT,
                                        WORKLIST_STATUS,
                                        WORKLIST_TRANS_ID,
                                        WORKLIST_PERSON_ID,
                                        WORKLIST_DUE_DATE,
                                        NOTIFICATION_ID)
                    VALUES (AS_WORKLIST_SEQ.NEXTVAL,
                            'TRANS',
                            'RESPOND',
                            SYSTIMESTAMP,
                            L_NTF_SUBJECT,
                            'OPEN',
                            L_WORKLIST_TRANS_ID,
                            L_NEXT_APPROVER_ID,
                            NULL,
                            L_NOTIFICATION_ID);
            END IF;
         ELSIF IN_ACTION = 'REJECTED'
         THEN
            UPDATE AS_SCHEDULER_TRANS_HDRS
               SET APPROVAL_STATUS = IN_ACTION
             WHERE SCHEDULER_TRANS_HDR_ID = L_WORKLIST_TRANS_ID;
         END IF;
      END IF;



      COMMIT;
   END WORKLIST_ACTION;

   PROCEDURE VALIDATE_SCHEDULER (IN_SCHEDULER_ID           NUMBER,
                                 OUT_VALIDATE_REPORT   OUT CLOB)
   IS
      L_START_DATE     AS_SCHEDULERS.START_DATE%TYPE;
      L_END_DATE       AS_SCHEDULERS.END_DATE%TYPE;
      L_RET_CLOB       CLOB;
      L_VALIDATE_STR   VARCHAR2 (32000);
      L_COUNT          NUMBER;

      CURSOR SCHDLR_SHIFTS_CUR
      IS
         SELECT SCHDLR_SHFT.SCHEDULER_ID,
                SCHDLR_SHFT.SHIFT_ID,
                SHIFT.SHIFT_NAME,
                SCHDLR_SHFT.WORK_GROUP_ID,
                WRKGRP.WORK_GROUP_NAME,
                SHFT_WRKGRP.EMPLOYEES_COUNT,
                SHFT_WRKGRP.EMPLOYEES_COUNT_MAX
           FROM AS_SCHEDULER_SHIFTS SCHDLR_SHFT,
                AS_SHIFT_WORK_GROUPS SHFT_WRKGRP,
                AS_SHIFTS SHIFT,
                AS_WORK_GROUPS_HDR WRKGRP
          WHERE     SCHDLR_SHFT.SHIFT_ID = SHFT_WRKGRP.SHIFT_ID
                AND SCHDLR_SHFT.WORK_GROUP_ID = SHFT_WRKGRP.WORK_GROUP_ID
                AND SCHDLR_SHFT.SHIFT_ID = SHIFT.SHIFT_ID
                AND SCHDLR_SHFT.WORK_GROUP_ID = WRKGRP.WORK_GROUP_ID
                AND SCHDLR_SHFT.SCHEDULER_ID = IN_SCHEDULER_ID;

      CURSOR ASSIGNMENT_EMPS_CUR (
         IN_ASSIGNMENT_DATE DATE)
      IS
         SELECT PAPF.FULL_NAME EMPLOYEE_NAME
           FROM AS_SCHEDULER_STAFF STAFF, PER_ALL_PEOPLE_F PAPF
          WHERE STAFF.PERSON_ID = PAPF.PERSON_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND STAFF.SCHEDULER_ID = IN_SCHEDULER_ID
                AND STAFF.PARENT_SCHEDULER_STAFF_ID IS NULL
                AND TRUNC (STAFF.SCHEDULAR_START_DATE) =
                       TRUNC (IN_ASSIGNMENT_DATE)
                AND EXISTS
                       (SELECT 1
                          FROM AS_ASSIGNMENT_EMPS ASSIGN
                         WHERE STAFF.PERSON_ID = ASSIGN.PERSON_ID
                               AND TRUNC (IN_ASSIGNMENT_DATE) BETWEEN TRUNC (
                                                                         EMP_START_DATE)
                                                                  AND TRUNC (
                                                                         EMP_END_DATE));
   BEGIN
      DBMS_LOB.CREATETEMPORARY (L_RET_CLOB, TRUE);

      SELECT START_DATE, END_DATE
        INTO L_START_DATE, L_END_DATE
        FROM AS_SCHEDULERS SCHDLR
       WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      FOR DAYS_REC IN (    SELECT L_START_DATE + (LEVEL - 1) CURSOR_DATE
                             FROM DUAL
                       CONNECT BY LEVEL < (L_END_DATE - L_START_DATE + 2))
      LOOP
         L_VALIDATE_STR :=
               CHR (10)
            || CHR (10)
            || TO_CHAR (DAYS_REC.CURSOR_DATE, 'dd-mm-rrrr');
         DBMS_LOB.APPEND (L_RET_CLOB, L_VALIDATE_STR);

         --Checl min and max emps
         FOR SCHDLR_SHIFTS_REC IN SCHDLR_SHIFTS_CUR
         LOOP
            SELECT COUNT (1)
              INTO L_COUNT
              FROM AS_SCHEDULER_STAFF STAFF
             WHERE     STAFF.SCHEDULER_ID = IN_SCHEDULER_ID
                   AND STAFF.SHIFT_ID = SCHDLR_SHIFTS_REC.SHIFT_ID
                   AND STAFF.WORK_GROUP_ID = SCHDLR_SHIFTS_REC.WORK_GROUP_ID
                   AND STAFF.PARENT_SCHEDULER_STAFF_ID IS NULL
                   AND TRUNC (STAFF.SCHEDULAR_START_DATE) =
                          TRUNC (DAYS_REC.CURSOR_DATE);

            IF L_COUNT < SCHDLR_SHIFTS_REC.EMPLOYEES_COUNT
            THEN
               L_VALIDATE_STR :=
                     CHR (10)
                  || 'No. of employees in  '
                  || SCHDLR_SHIFTS_REC.SHIFT_NAME
                  || '-'
                  || SCHDLR_SHIFTS_REC.WORK_GROUP_NAME
                  || ' is '
                  || L_COUNT
                  || ' however is required '
                  || SCHDLR_SHIFTS_REC.EMPLOYEES_COUNT
                  || ' employee(s)';
               DBMS_LOB.APPEND (L_RET_CLOB, L_VALIDATE_STR);
            ELSIF L_COUNT > SCHDLR_SHIFTS_REC.EMPLOYEES_COUNT_MAX
            THEN
               L_VALIDATE_STR :=
                     CHR (10)
                  || 'No. of employees in  '
                  || SCHDLR_SHIFTS_REC.SHIFT_NAME
                  || '-'
                  || SCHDLR_SHIFTS_REC.WORK_GROUP_NAME
                  || ' is '
                  || L_COUNT
                  || ' however maximum number required is  '
                  || SCHDLR_SHIFTS_REC.EMPLOYEES_COUNT_MAX
                  || ' employee(s)';
               DBMS_LOB.APPEND (L_RET_CLOB, L_VALIDATE_STR);
            END IF;
         END LOOP;

         --check emp in assignments
         L_VALIDATE_STR := '';

         FOR ASSIGNMENT_EMPS_REC
            IN ASSIGNMENT_EMPS_CUR (DAYS_REC.CURSOR_DATE)
         LOOP
            L_VALIDATE_STR :=
               L_VALIDATE_STR || ',' || ASSIGNMENT_EMPS_REC.EMPLOYEE_NAME;
         END LOOP;

         IF LENGTH (L_VALIDATE_STR) > 0
         THEN
            L_VALIDATE_STR :=
               CHR (10) || SUBSTR (L_VALIDATE_STR, 2) || ' are in assignment';
            DBMS_LOB.APPEND (L_RET_CLOB, L_VALIDATE_STR);
         END IF;
      END LOOP;

      OUT_VALIDATE_REPORT := L_RET_CLOB;
   END VALIDATE_SCHEDULER;

   FUNCTION CHECK_ASSIGNMENT_OVERLAPPING (IN_ASSIGNMENT_EMP_ID    NUMBER,
                                          IN_PERSON_ID            NUMBER,
                                          IN_START_DATE           DATE,
                                          IN_END_DATE             DATE)
      RETURN VARCHAR2
   IS
      L_COUNT   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO L_COUNT
        FROM AS_ASSIGNMENT_EMPS AE
       WHERE AE.ASSIGNMENT_EMP_ID <> IN_ASSIGNMENT_EMP_ID
             AND AE.PERSON_ID = IN_PERSON_ID
             AND (TRUNC (IN_START_DATE) BETWEEN TRUNC (AE.EMP_START_DATE)
                                            AND TRUNC (AE.EMP_END_DATE)
                  OR TRUNC (IN_END_DATE) BETWEEN TRUNC (AE.EMP_START_DATE)
                                             AND TRUNC (AE.EMP_END_DATE)
                  OR TRUNC (AE.EMP_START_DATE) BETWEEN TRUNC (IN_START_DATE)
                                                   AND TRUNC (IN_END_DATE)
                  OR TRUNC (AE.EMP_END_DATE) BETWEEN TRUNC (IN_START_DATE)
                                                 AND TRUNC (IN_END_DATE));

      IF L_COUNT = 0
      THEN
         RETURN 'N';
      ELSE
         RETURN 'Y';
      END IF;
   END CHECK_ASSIGNMENT_OVERLAPPING;

   PROCEDURE DELETE_AS_SCHEDULER (IN_SCHEDULER_ID NUMBER)
   IS
   BEGIN
      DELETE FROM AS_SCHEDULER_SHIFTS
            WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      DELETE FROM AS_SCHEDULER_APPROVERS
            WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      DELETE FROM AS_SCHEDULER_ACTION_HISTORY
            WHERE TRANS_ID = IN_SCHEDULER_ID AND TRANS_TYPE = 'SCHDLR';

      DELETE FROM AS_SCHEDULERS
            WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      COMMIT;
   END DELETE_AS_SCHEDULER;
   --Added by Heba 11-july for tas integartion --
   function GetDateDifference(pstartdate date,penddate date)return number
   is
   datediff number;
   begin 
    select  (penddate
             - pstartdate) into datediff
       from dual;
       return datediff;
   
   
   end ;
   -- added By Heba 17-july-tas integration
   function VALIDATE_TAS_RECORD(pperson_id Number,p_employeecode varchar2,pshiftcode varchar2,pshiftdate date,pshiftstart number,pshiftend number)return varchar2
   as
   v_recordstatus varchar(200);
   count_person Number;
   count_shift Number ;
   vdateonly Date ;
   begin 
    vdateonly :=to_date(pshiftdate,'dd-mm-rrrr');
    --pshiftdate;
    --to_date(pshiftdate,'dd-mm-rrrr');
      ---first check select employeecode-personid and shiftdate
      select count (person_id) into count_person 
      from AS_SCHEDULAR_TAS
      where person_id=pperson_id and employee_code=p_employeecode and shift_date=vdateonly ;
       --if not exist status new
      if(count_person=0)
      then v_recordstatus:='NEW';
      elsif (count_person>0)
           then 
             --else if exist select shiftcode-shiftstart-shiftend 
        select count (shift_code) into count_shift 
      from AS_SCHEDULAR_TAS
      where person_id=pperson_id and employee_code=p_employeecode and shift_date=vdateonly 
      and shift_code=pshiftcode and shift_start_time=pshiftstart and shift_end_time=pshiftend ;
        --else if exist select shiftcode-shiftstart-shiftend 
      --if equal status no changes 
      if(count_shift=1)
      then 
      v_recordstatus :='NOCHANGES';
      elsif (count_shift=0)
      then
       v_recordstatus :='CHANGES';
      end if ;
              
              end if ;
              
         
         
           
     
     
      -- esle status changed 
      return v_recordstatus;
      --'UNDEFINED';
      -- ;
   end ;
   PROCEDURE INSERT_TAS_RECORD(pperson_id Number,p_employeecode varchar2,pshiftcode varchar2,pshiftdate date,pshiftstart number,pshiftend number,pstatus varchar2 ,poff_flag varchar2)
   is 
 vdateonly Date ;
   begin 
   vdateonly :=to_date(pshiftdate,'dd-mm-rrrr');
  -- insert into AS_SCHEDULAR_TAS (scheduler_tas_id,person_id,employee_code,shift_code,shift_date,shift_start_time,shift_end_time,off_flag) values(tas_seq.nextval,pperson_id,p_employeecode,pshiftcode,pshiftdate,pshiftstart,pshfitend,poff_flag);
   insert into AS_SCHEDULAR_TAS (scheduler_tas_id,person_id,employee_code,shift_code,shift_date,shift_start_time,shift_end_time,tas_status,off_flag) 
   values(tas_seq.nextval,pperson_id,p_employeecode,pshiftcode,vdateonly,pshiftstart,pshiftend,pstatus,poff_flag);
   
   commit;
   end ;

END AS_SCHDLR_PKG;
/

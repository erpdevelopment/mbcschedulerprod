import  {NavController, ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { AppModule } from '../../../app/app.module';
import { Navbar } from 'ionic-angular';
import { ViewChild, Component } from '@angular/core';
import {TeamDetailsPage} from '../myteam/teamdetails';
import {SchedulerProvider} from '../../../providers/scheduler/scheduler'
import { LoadingController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import {AlertController } from 'ionic-angular';
import { AES256 } from '@ionic-native/aes-256';
/**
 * Generated class for the PublicSpacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: 'page-myteam',
  templateUrl: 'myteam.html',
})




export class MyTeamPage {

 
  @ViewChild(Navbar) Navbar: Navbar;
  @ViewChild('pageTop') pageTop;
 public  MyTeamList=[];
 public MyTeamListFixed=[];
  MyTeamListAll;
  fromR=0;
  v_step=12;
  toR=this.v_step;
  characters;
  searchItem :String;

  //public newSubcategories: any[] = [];
  public items= [] ;
   //EvaluationSpendScore=[];
  //items: Array<string> = ['ApprovalCenter', 'Profile',  'Leaves','Payroll', 'HRrequests','Expense' ,'thanksCard'];
  searchDate :string;
  searchemp :string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,public SchedulerData:SchedulerProvider,
    public loadingController: LoadingController ,  public AlertController :AlertController,  private sanitized: DomSanitizer,public aes256:AES256) 
  
  
  {
  
    //this.userId            =AppModule.userId; 
    //this.personId          =AppModule.personId; 
    //this.businessGroup     =AppModule.businessGroup; 
    //this.assignmentId      =AppModule.assignmentId; 
   
//     let datex :Date=new Date();
// this.searchDate=datex.toISOString();

 
  }

  ionViewDidLoad() {
   // 

   this.ShowToday();  
   this.getMyTeamList();
      console.log('ionViewDidLoad MyTeamPage');
  }
 



  getMyTeamList()
  {
    let datex :Date=new Date(this.searchDate);
     
    
   // this.searchDate=datex.toISOString();
    let formattedDate = datex.getFullYear()+"-" + (datex.getMonth() + 1)+"-" +datex.getDate();
    try{

      this.SchedulerData.getMyTeamList(formattedDate)
    
      
      .then(data => {
        
      this.MyTeamListAll= data;
     // this.MyTeamList=this.MyTeamListAll;
      if (this.MyTeamListAll)
      {
      this.MyTeamList=this.MyTeamListAll.slice(this.fromR,this.toR);
      this.MyTeamListFixed=this.MyTeamListAll;
      
      //
    
      }
       });
     
     
    }
    catch (error)
    {
    
      
      AppModule.showMessage(this.AlertController,"error");
    }
  }



  async doRefresh(refresher) {
    this.toR=this.v_step;
    await this.getMyTeamList();
    refresher.complete();

    }



    doInfinite(infiniteScroll) {


      // this.fromR=this.fromR+this.v_step;
       this.toR=this.toR+this.v_step;
   
   
       try{
   
         
           
         if(this.MyTeamListAll)
         this.MyTeamList=(this.MyTeamListAll.slice(0 ,this.toR));
         
         infiniteScroll.complete();
     }
       catch (err)
       {
         
       }
       console.log(' infiniteScroll has ended');
     }



 

  openModal(detail) {

    let modal = this.modalCtrl.create(TeamDetailsPage, detail);
    modal.present();
}

onTabEmployee(detail) {


  //let modal = this.modalCtrl.create(TeamDetailsPage, detail);
  //modal.present();
  this.navCtrl.push(TeamDetailsPage,{info:detail})

}

reloadlist()
{

  this.MyTeamList= this.MyTeamListFixed.slice(this.fromR,this.toR);;
}
getItems(ev: any) {
 
this.reloadlist();

 
 console.log("MyTeam1:"+this.MyTeamList.length);

       let val = ev.target.value;
    console.log('vv '+val);
    if (val && val.trim() !== '') {
      this.MyTeamList= this.MyTeamListFixed.filter(function(item) {
       // return (item.full_name.toLowerCase().includes(val.toLowerCase()));
       // to be able to search with shift in webservice set nvl(shift(---))
        return (item.full_name.toLowerCase().indexOf(val.toLowerCase()) > -1 );
        //||item.shift.trim().toLowerCase().indexOf(val.toLowerCase()) > -1);
     
      });
 
      console.log("MyTeam2:"+this.MyTeamList.length);
//        for(let i in this.MyTeamList)
//    {
//  console.log("employeename:"+this.MyTeamList[i].full_name);     
//    }
      console.log("WholeTeam:"+this.MyTeamListFixed.length);


    }
}






onchangeDate()
{
  this.searchemp="";
  this.getMyTeamList();
  //alert (this.searchDate.toString());
// this.searchDate=this.searchDate+3;
}

goNextDay()
{

  let datex :Date=new Date(this.searchDate);

  datex.setDate( datex.getDate() + 1 );
  this.searchDate=datex.toISOString();
  let formattedDate = datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();
  //alert (formattedDate);
}

goPreviousDay()
{

  let datex :Date=new Date(this.searchDate);

  datex.setDate( datex.getDate() -1 );
  this.searchDate=datex.toISOString();
  let formattedDate = datex.getDate() + "-" + (datex.getMonth() + 1) + "-" + datex.getFullYear();

}

onSwipe(e)
{ 

  //alert (e.direction)
  let t =e.direction;
  if (t==2)
  this.goNextDay();
  else
  this.goPreviousDay();
  // 2 next day
  // 4 previous day
}

 async  geturl(personId)
{
//console.log(personId);

// if (this.xxm=="1")
// return 'http://10.10.131.34:7003/mbcWebservicePG/imageservlet?px='+personId;
// else
// {
// this.xxm="1";
//   return null;
// }

// if (1==1)
// {
//   alert (AppModule.wsURL.substr(0,AppModule.wsURL.lastIndexOf("/")));
//  // return data.substr( data.lastIndexOf("/") +1);

// }




let token=AppModule.getProperty('wsid');
token=token.replace(/#/g,'@@@@')
token=encodeURI(token);
let url=AppModule.wsURL.substr(0,AppModule.wsURL.lastIndexOf("/"));
let t=   AppModule.doHash(this.aes256,AppModule.getProperty('wsid')+personId);
let x=encodeURI(t+'');
x=x+'';
return url+'/imageservlet?px='+personId+'&token='+token+'&signature='+x;



}

ShowToday()
{

 let datex :Date=new Date();
 this.searchDate=datex.toISOString();
 this.pageTop.scrollToTop();
}






}








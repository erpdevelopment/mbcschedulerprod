package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class ApprovalsBean {
    public ApprovalsBean() {
        super();
    }

   

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public String startPatternApproval_action() {
        ADFUtils.findOperation("Commit").execute();
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("startPatternApproval");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        else
        {
                 Refresh_patterns();
                 FacesMessage fm=new FacesMessage("Patterns Submitted for Approval Successfully");
                 fm.setSeverity(FacesMessage.SEVERITY_INFO);
                 FacesContext fc = FacesContext.getCurrentInstance(); 
                 fc.addMessage(null, fm);
            
            
            }
        return null;
    }

    public String Refresh_patterns() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Execute");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }

    public String startShiftApproval_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("startShiftApproval");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        else
        {
               
                 FacesMessage fm=new FacesMessage("Shifts Submitted for Approval Successfully");
                 fm.setSeverity(FacesMessage.SEVERITY_INFO);
                 FacesContext fc = FacesContext.getCurrentInstance(); 
                 fc.addMessage(null, fm);
            
            
            }
        return null;
    }


    public String Refresh_Bulletins() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("ExecuteBulletin");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }
    public String startBulletinapproval_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("startBulletinApproval");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        
        else
        {
                Refresh_Bulletins();
                 FacesMessage fm=new FacesMessage("Bulletins Submitted for Approval Successfully");
                 fm.setSeverity(FacesMessage.SEVERITY_INFO);
                 FacesContext fc = FacesContext.getCurrentInstance(); 
                 fc.addMessage(null, fm);
            
            
            }
        return null;
    }
}

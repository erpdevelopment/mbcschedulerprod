var genderChartColors = ['#30A3D9', '#EEEEEE'];
var genderJsonData = [];
var genderChartData = [];
genderChartData.push( {
    category : 'Male', value : 0, color : genderChartColors[1]
});
genderChartData.push( {
    category : 'Female', value : 0, color : genderChartColors[0]
});

var genderChartIndex = 2;
var genderChartTotal = 0;
var genderChartText1 ;
var genderChartText2 ;

var genderdataSource = new kendo.data.DataSource( {
    transport :  {
        read :  {
            // the remote service url
            url : "http://10.10.131.26:7003/schedulerRest/rest/AASchedules1.0/DashboardGenderVO", 
            // the data type of the returned result
            dataType : "json", type : "GET"
        }
    },
    // describe the result format
    schema :  {
        // the data, which the data source will be bound to is in the "list" field of the response
        data : "items", model :  {
            fields :  {
                Sex :  {
                    type : "string"
                },
                SexCount :  {
                    type : "number"
                },
                links :  {
                    rel :  {
                        type : "string"
                    },
                    href :  {
                        type : "string"
                    },
                    name :  {
                        type : "string"
                    },
                    kind :  {
                        type : "string"
                    }
                }
            }
        }
    }
});

genderdataSource.fetch(function () {
    genderJsonData = genderdataSource.data();
    //alert(genderJsonData[0].SexCount + " and "+genderJsonData[1].SexCount);
    genderChartTotal = (genderJsonData[0].SexCount + genderJsonData[1].SexCount);
    genderChartData[0].value = genderJsonData[0].SexCount;
    genderChartData[1].value = genderJsonData[1].SexCount;
    
    genderChartText1 = 'M: ' + (genderChartData[0].value / genderChartTotal * 100).toFixed(1).toString() + '%';
    genderChartText2 = 'F: ' + (genderChartData[1].value / genderChartTotal * 100).toFixed(1).toString() + '%';

    genderChartCreate();

});

/*genderChartTotal = 864;
genderChartData[0].value = 636;
genderChartData[1].value = 228 ;*/

//alert(genderChartData[0].value + " and " + genderChartData[1].value);



function genderChartCreate() {
    $("#_workforce_Gender_donutchart").kendoChart( {
        theme : 'material', legend :  {
            visible : false
        },
        chartArea :  {
            background : ""
        },
        series : [{type : "donut", startAngle : 90, holeSize : 40, visual : function (e) {center = e.center;cosa = e.center;radius = e.radius;return e.createVisual();},name : "Gender", data : genderChartData}], tooltip :  {
            visible : true, template : "#= category # : #= value # employees"
        },
        render : function (e) {
            var draw = kendo.drawing;
            var geom = kendo.geometry;
            var chart = e.sender;

            center.y = center.y - 0.2 * center.y;
            var circleGeometry = new geom.Circle(center, radius);
            var bbox = circleGeometry.bbox();
            var text1 = new draw.Text(genderChartText1, [0, 0], 
            {
                font : "16px Calibri,Arial"
            });
            draw.align([text1], bbox, "center");
            draw.vAlign([text1], bbox, "center");
            e.sender.surface.draw(text1);

            center.y = center.y + 0.4 * center.y;
            circleGeometry = new geom.Circle(center, radius);
            bbox = circleGeometry.bbox();
            var text2 = new draw.Text(genderChartText2, [0, 0], 
            {
                font : "16px Calibri,Arial"
            });
            draw.align([text2], bbox, "center");
            draw.vAlign([text2], bbox, "center");
            e.sender.surface.draw(text2);

        }
    });
}
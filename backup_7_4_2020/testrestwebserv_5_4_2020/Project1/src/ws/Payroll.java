package ws;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.security.Principal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

@Path("payroll/info")
public class Payroll {
    public Payroll() {
        super();
    }


    @POST
    @Produces("application/json")
    @Path("/getBankInfo")
    public Response getBankInfo(@Context SecurityContext sc,
                                @HeaderParam("wsid") String wsid,
                                @HeaderParam("signature") String signature)
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
           throw new SecurityException("User is unauthorized.");
            
           // Principal p = sc.getUserPrincipal();
            
//            if (1==1)
//                return Response.status(200).entity(sc.getAuthenticationScheme()).type("text/plain").build();
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT  PPPM.PRIORITY, PPPM.PERCENTAGE, POPM.ORG_PAYMENT_METHOD_NAME, POPM.CURRENCY_CODE,\n" + 
            "        DECODE(PAAF.BUSINESS_GROUP_ID, 81,HR_GENERAL.DECODE_LOOKUP('AE_BANK_NAMES',PEA.SEGMENT1)\n" + 
            "                                     ,998,HR_GENERAL.DECODE_LOOKUP('SA_BANKS',PEA.SEGMENT1)\n" + 
            "                                     ,PEA.SEGMENT1) BANK_NAME,\n" + 
            "        DECODE(PAAF.BUSINESS_GROUP_ID, 81,HR_GENERAL.DECODE_LOOKUP('AE_BRANCH_NAMES',PEA.SEGMENT2), PEA.SEGMENT2) BANK_BRANCH,\n" + 
            "        SEGMENT3 BENEFICIARY_NAME,\n" + 
            "        DECODE(PAAF.BUSINESS_GROUP_ID, 205, SEGMENT5, SEGMENT4) ACCOUNT_NUMBER,\n" + 
            "        DECODE(PAAF.BUSINESS_GROUP_ID, 81, SEGMENT10,205,SEGMENT4,998,NULL, SEGMENT5) SWIFT_CODE,\n" + 
            "        DECODE(PAAF.BUSINESS_GROUP_ID, 205, SEGMENT6, SEGMENT4) IBAN,\n" + 
            "        SEGMENT8 CITY,\n" + 
            "        SEGMENT9 COUNTRY,\n" + 
            "        SEGMENT11 BIC_CODE_INT_BANK\n" + 
            "FROM    PAY_PERSONAL_PAYMENT_METHODS_F PPPM, PER_ALL_ASSIGNMENTS_F PAAF, PAY_EXTERNAL_ACCOUNTS PEA, PAY_ORG_PAYMENT_METHODS_F POPM\n" + 
            "WHERE   PPPM.ASSIGNMENT_ID          = PAAF.ASSIGNMENT_ID\n" + 
            "AND     PPPM.EXTERNAL_ACCOUNT_ID    = PEA.EXTERNAL_ACCOUNT_ID\n" + 
            "AND     POPM.ORG_PAYMENT_METHOD_ID  = PPPM.ORG_PAYMENT_METHOD_ID\n" + 
            "AND     PAAF.PERSON_ID              = mbc_mobile_pkg.get_session_data('PERSON_ID')  \n" + 
            "AND     PAAF.PRIMARY_FLAG           = 'Y'\n" + 
            "AND     PEA.ENABLED_FLAG            = 'Y'\n" + 
            "AND     SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE AND PAAF.EFFECTIVE_END_DATE\n" + 
            "AND     SYSDATE BETWEEN PPPM.EFFECTIVE_START_DATE AND PPPM.EFFECTIVE_END_DATE\n" + 
            "ORDER BY PPPM.PRIORITY";
            try {
         
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getBankInfo");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "Open Bank Info", "Open Bank Info" );
            statement = connection.prepareStatement(sqlStemt);
               
               

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    @POST
    @Produces("application/json")
    @Path("/getPayslipList")
    public Response getPayslipList(@Context SecurityContext sc ,
                                   @HeaderParam("bu") String businessGroup,
                                   @HeaderParam("wsid") String wsid,
                                   @HeaderParam("signature") String signature)
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
             
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="";
            
           final String UAE_DATA="  SELECT   TO_NUMBER (SUBSTR (TO_CHAR (action_context_id), 1, 50))\n" + 
           "				  action_context_id,\n" + 
           "                      TO_CHAR(TRUNC(effective_date),'MON-RRRR') PAYSLIP_TXT, \n" +
           "			  SUBSTR (TO_CHAR(pay_us_employee_payslip_web.get_display_list (effective_date,\n" + 
           "						 'AE', 'AE', action_information14, check_count)), 4, 8)\n" + 
           "				  PAYSLIP_TXT_OLD,\n" + 
           "			  TO_CHAR (TRUNC (effective_date), 'DD-MON-RRRR') effective_date\n" + 
           "	 FROM   (  SELECT   pai.action_information14 action_information14,\n" + 
           "							  COUNT ( * ) check_count,\n" + 
           "							  pai.action_context_id action_context_id,\n" + 
           "							  paf.person_id person_id, pai.effective_date effective_date\n" + 
           "					 FROM   pay_action_information pai, pay_action_information pai2,\n" + 
           "							  per_all_assignments_f paf, per_all_assignments_f paf2\n" + 
           "					WHERE 		pai.action_context_id >= pai2.action_context_id\n" + 
           "							  AND paf.assignment_id = pai.assignment_id\n" + 
           "							  AND pai.action_context_type = 'AAP'\n" + 
           "							  AND pai.action_information_category = 'EMPLOYEE DETAILS'\n" + 
           "							  AND paf2.assignment_id = pai2.assignment_id\n" + 
           "							  AND pai2.action_context_type = 'AAP'\n" + 
           "							  AND pai2.action_information_category = 'EMPLOYEE DETAILS'\n" + 
           "							  AND pai.effective_date = pai2.effective_date\n" + 
           "							  AND paf.person_id = paf2.person_id\n" + 
           "							  AND pai.effective_date BETWEEN paf.effective_start_date\n" + 
           "																  AND  paf.effective_end_date\n" + 
           "							  AND pai.effective_date BETWEEN paf2.effective_start_date\n" + 
           "																  AND  paf2.effective_end_date\n" + 
           "							  AND paf.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')   \n" + 
           "							  AND EXISTS\n" + 
           "									  (SELECT	'x'\n" + 
           "										  FROM	pay_assignment_actions paa\n" + 
           "								 		 WHERE	paa.assignment_action_id =\n" + 
           "														pai.action_context_id\n" + 
           "													AND paa.assignment_id = pai.assignment_id)\n" + 
           "							  AND EXISTS\n" + 
           "									  (SELECT	'x'\n" + 
           "										  FROM	pay_assignment_actions paa2\n" + 
           "										 WHERE	paa2.assignment_action_id =\n" + 
           "														pai2.action_context_id\n" + 
           "													AND paa2.assignment_id =\n" + 
           "															pai2.assignment_id)\n" + 
           "							  AND NOT EXISTS\n" + 
           "									  (SELECT	'X'\n" + 
           "										  FROM	pay_legislative_field_info plf,\n" + 
           "													hr_organization_information hoi,\n" + 
           "													pay_payroll_Actions ppa,\n" + 
           "													pay_assignment_actions paa\n" + 
           "										 WHERE	validation_name = 'ITEM_PROPERTY'\n" + 
           "													AND rule_type =\n" + 
           "															'PAYSLIP_EMPLOYEE_DETAILS_EXC'\n" + 
           "													AND field_name = 'CHOOSE_PAYSLIP'\n" + 
           "													AND legislation_code =\n" + 
           "															hoi.org_information9\n" + 
           "													AND org_information_context =\n" + 
           "															'Business Group Information'\n" + 
           "													AND organization_id =\n" + 
           "															ppa.business_group_id\n" + 
           "													AND ppa.report_type = rule_mode\n" + 
           "													AND ppa.payroll_action_id =\n" + 
           "															paa.payroll_Action_id\n" + 
           "													AND paa.assignment_action_id =\n" + 
           "															pai.action_context_id)\n" + 
           "				GROUP BY   pai.effective_date, pai.action_context_id,\n" + 
           "							  pai.action_information14, paf.person_id)\n" + 
           "	WHERE   pay_us_employee_payslip_web.get_term_info ( ?, person_id,\n" + 
           "			  action_context_id) = 'Y'\n" + 
           "ORDER BY   to_date(effective_date) DESC\n";
           
           final String NONE_UAE_DATA="  SELECT   payroll_action_id action_context_id, NAME2 PAYSLIP_TXT,\n" + 
           "			  TO_CHAR (TRUNC(CREATION_DATE), 'DD-MON-RRRR') effective_date\n" + 
           "	 FROM   (SELECT	DISTINCT ppa.payroll_action_id, period_name,\n" + 
           "											TRIM (TO_CHAR (TO_DATE (SUBSTR (period_name, 1,\n" + 
           "																			INSTR (period_name,\n" + 
           "																			' ')\n" + 
           "																			- 1), 'MM'), 'Month'))\n" + 
           "										|| ' '\n" + 
           "										|| SUBSTR (period_name,\n" + 
           "											INSTR (period_name, ' ') + 1)\n" + 
           "											name2, ppa.creation_date creation_date\n" + 
           "				  FROM	per_people_f papf, per_assignments_f paaf,\n" + 
           "							per_periods_of_service ppos, hr_organization_units hou,\n" + 
           "							per_time_periods ptp, pay_payroll_actions ppa,\n" + 
           "							pay_assignment_actions paa,\n" + 
           "							pay_element_classifications pec, pay_run_results prr,\n" + 
           "							pay_run_result_values prrv, pay_people_groups ppg,\n" + 
           "							per_business_groups pbg, per_pay_bases ppb\n" + 
           "				 WHERE		 1 = 1\n" + 
           "							AND papf.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')  \n" + 
           "							AND papf.business_group_id = ?\n" + 
           "							AND papf.person_id = paaf.person_id\n" + 
           "							AND paaf.period_of_service_id = ppos.period_of_service_id\n" + 
           "							AND paa.assignment_id = paaf.assignment_id\n" + 
           "							AND paaf.organization_id = hou.organization_id\n" + 
           "							AND ppa.payroll_id = paaf.payroll_id\n" + 
           "							AND ppa.time_period_id = ptp.time_period_id\n" + 
           "							AND ppa.action_type IN ('R', 'Q')\n" + 
           "							AND paa.payroll_action_id = ppa.payroll_action_id\n" + 
           "							AND paaf.payroll_id = ptp.payroll_id\n" + 
           "							AND prr.assignment_action_id = paa.assignment_action_id\n" + 
           "							AND prr.run_result_id = prrv.run_result_id\n" + 
           "							AND ppa.date_earned BETWEEN ptp.start_date\n" + 
           "															AND  ptp.end_date\n" + 
           "							AND ppa.date_earned BETWEEN paaf.effective_start_date\n" + 
           "															AND  paaf.effective_end_Date\n" + 
           "							AND ppa.date_earned BETWEEN papf.effective_start_date\n" + 
           "															AND  papf.effective_end_Date\n" + 
           "							AND paaf.PAY_BASIS_ID = ppb.PAY_BASIS_ID\n" + 
           "							AND paaf.people_group_id = ppg.people_group_id\n" + 
           "							AND paaf.business_group_id = pbg.business_group_id\n" + 
           "							AND ppa.assignment_set_id IS NULL) QRSLT\n" + 
           "ORDER BY   creation_date DESC\n";
           
           
           if (businessGroup !=null && businessGroup.equalsIgnoreCase("81"))
               sqlStemt=UAE_DATA;
           else
               sqlStemt=NONE_UAE_DATA;
           
            
            
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getPayslipList");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                Helper.logActivity(connection, "Open Payslip", "Open Payslip");
            statement = connection.prepareStatement(sqlStemt);
               
                statement.setString(1, businessGroup);
                
                
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  //  response = Response.status(404).entity("Unable to find record").type("text/plain").build();
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }


    @POST
    @Produces("application/json")
    @Path("/getPayrollEarnings")
    public Response getPayrollEarnings(@Context SecurityContext sc ,
                                       @HeaderParam("acx") String actionContextId,
                                       @HeaderParam("bu") String businessGroup,
                                       @HeaderParam("wsid") String wsid,
                                       @HeaderParam("signature") String signature)
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="";
            final String UAE_DATA_old="  SELECT NVL (pet.REPORTING_NAME, pet.element_name) element_name, TO_CHAR (pai.ACTION_INFORMATION4,'FM999,999,990.00') ||' '||pet.OUTPUT_CURRENCY_CODE PAY_VALUE ,  \n" + 
            " sum (pai.ACTION_INFORMATION4) over (partition by null) total_value ,  \n" +
            " ' '||pet.OUTPUT_CURRENCY_CODE CURRENCY \n"+ 
            "    FROM pay_action_information pai, pay_element_types_f pet\n" + 
            "   WHERE     pet.element_type_id = pai.action_information1\n" + 
            "         AND pai.effective_date BETWEEN pet.effective_start_date\n" + 
            "                                    AND pet.effective_end_date \n" + 
            "         AND pai.ACTION_CONTEXT_ID = ?        \n" + 
            "         AND pai.ACTION_INFORMATION_CATEGORY = 'EMEA ELEMENT INFO'\n" + 
            "         AND pai.ACTION_INFORMATION3 = 'E'\n" + 
            "ORDER BY 1";
            
             final String UAE_DATA="  SELECT NVL (pet.REPORTING_NAME, pet.element_name) element_name, TO_CHAR (sum(pai.ACTION_INFORMATION4),'FM999,999,990.00') ||' '||pet.OUTPUT_CURRENCY_CODE PAY_VALUE  ,\n" + 
            " sum (sum(pai.ACTION_INFORMATION4)) over (partition by null) total_value  ,\n" + 
            " ' '||pet.OUTPUT_CURRENCY_CODE CURRENCY \n" + 
            "    FROM pay_action_information pai, pay_element_types_f pet\n" + 
            "   WHERE     pet.element_type_id = pai.action_information1\n" + 
            "         AND pai.effective_date BETWEEN pet.effective_start_date\n" + 
            "                                    AND pet.effective_end_date \n" + 
            "         AND pai.ACTION_CONTEXT_ID =?    \n" + 
            "         AND pai.ACTION_INFORMATION_CATEGORY = 'EMEA ELEMENT INFO'\n" + 
            "         AND pai.ACTION_INFORMATION3 = 'E' and assignment_id = mbc_mobile_pkg.get_session_data('ASSIGNMENT_ID') \n" + 
            "         group by NVL (pet.REPORTING_NAME, pet.element_name) , pet.OUTPUT_CURRENCY_CODE \n" + 
            "ORDER BY 1";
            
            final String NONE_UAE_DATA="SELECT DISTINCT PETF.ELEMENT_NAME ,to_char(sum(PRRV.RESULT_VALUE),'FM999,999,990.00') ||' '||petf.OUTPUT_CURRENCY_CODE PAY_VALUE ,\n" + 
            " sum (sum (PRRV.RESULT_VALUE)) over (partition by null) total_value  ,  ' '||petf.OUTPUT_CURRENCY_CODE CURRENCY  \n" + 
            "FROM   APPS.PAY_RUN_RESULT_VALUES  PRRV\n" + 
            "      ,APPS.PAY_RUN_RESULTS        PRR\n" + 
            "      ,APPS.PAY_INPUT_VALUES_F     PIVF\n" + 
            "      ,APPS.PAY_ELEMENT_TYPES_F    PETF\n" + 
            "      ,APPS.PAY_ASSIGNMENT_ACTIONS PAA\n" + 
            "      ,APPS.PAY_PAYROLL_ACTIONS    PPA\n" + 
            "     ,APPS.PER_ALL_ASSIGNMENTS_F  PAAF\n" + 
            "      ,PAY_ELEMENT_CLASSIFICATIONS PEC\n" + 
            "WHERE  PRRV.RUN_RESULT_ID = PRR.RUN_RESULT_ID\n" + 
            "AND    PIVF.INPUT_VALUE_ID = PRRV.INPUT_VALUE_ID\n" + 
            "AND    UPPER(PIVF.NAME) = UPPER('Pay Value')\n" + 
            "AND    PIVF.UOM = 'M'\n" + 
            "AND    PETF.ELEMENT_TYPE_ID = PIVF.ELEMENT_TYPE_ID\n" + 
            "AND    PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" + 
            "AND    PEC.CLASSIFICATION_NAME = 'Earnings'\n" + 
            "AND    PAA.ASSIGNMENT_ACTION_ID = PRR.ASSIGNMENT_ACTION_ID\n" + 
            "AND    PAA.ACTION_STATUS = 'C'\n" + 
            "AND    PPA.PAYROLL_ACTION_ID = PAA.PAYROLL_ACTION_ID\n" + 
            "AND    PPA.ACTION_TYPE IN ('R' ,'Q')\n" + 
            "AND    PAAF.ASSIGNMENT_ID = PAA.ASSIGNMENT_ID\n" + 
            "AND PPA.PAYROLL_ACTION_ID = ? paaf.assignment_id =mbc_mobile_pkg.get_session_data('ASSIGNMENT_ID') \n" + 
            "AND    PAAF.PERSON_ID     =  mbc_mobile_pkg.get_session_data('PERSON_ID')  "+"\n"+ 
            "AND    PAAF.EFFECTIVE_START_DATE =\n" + 
            "             (SELECT MAX(PAAF1.EFFECTIVE_START_DATE)\n" + 
            "               FROM   APPS.PER_ALL_ASSIGNMENTS_F PAAF1\n" + 
            "               WHERE  PAAF1.ASSIGNMENT_ID = PAAF.ASSIGNMENT_ID\n" + 
            "               AND    PAAF1.EFFECTIVE_START_DATE <= PPA.EFFECTIVE_DATE)              \n" +
            " group by  PETF.ELEMENT_NAME  , petf.OUTPUT_CURRENCY_CODE " + 
            "ORDER BY 1               \n";
            
            
            if (businessGroup !=null && businessGroup.equalsIgnoreCase("81"))
                sqlStemt=UAE_DATA;
            else
                sqlStemt=NONE_UAE_DATA;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+actionContextId);
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
            statement = connection.prepareStatement(sqlStemt);

                statement.setString(1, actionContextId);
                
                
             
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
             {
                 s.printStackTrace();
                 response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                 }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }


    @POST
    @Produces("application/json")
    @Path("/getPayrollDeductions")
    public Response getPayrollDeductions(@Context SecurityContext sc ,
                                         @HeaderParam("acx") String actionContextId,
                                         @HeaderParam("bu") String businessGroup,
                                         @HeaderParam("wsid") String wsid,
                                         @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="";
            final String UAE_DATA_old="  SELECT NVL (pet.REPORTING_NAME, pet.element_name) element_name, TO_CHAR (pai.ACTION_INFORMATION4,'FM999,999,990.00') ||' '||pet.OUTPUT_CURRENCY_CODE PAY_VALUE ,   \n" + 
            " sum (pai.ACTION_INFORMATION4) over (partition by null) total_value   \n " +
            "    FROM pay_action_information pai, pay_element_types_f pet\n" + 
            "   WHERE     pet.element_type_id = pai.action_information1\n" + 
            "         AND pai.effective_date BETWEEN pet.effective_start_date\n" + 
            "                                    AND pet.effective_end_date \n" + 
            "         AND pai.ACTION_CONTEXT_ID = ?        \n" + 
            "         AND pai.ACTION_INFORMATION_CATEGORY = 'EMEA ELEMENT INFO'\n" + 
            "         AND pai.ACTION_INFORMATION3 = 'D'\n" + 
            "ORDER BY 1";
            
            final String UAE_DATA="  SELECT NVL (pet.REPORTING_NAME, pet.element_name) element_name, TO_CHAR (sum(pai.ACTION_INFORMATION4),'FM999,999,990.00') ||' '||pet.OUTPUT_CURRENCY_CODE PAY_VALUE  ,\n" + 
            " sum (sum(pai.ACTION_INFORMATION4)) over (partition by null) total_value  ,\n" + 
            " ' '||pet.OUTPUT_CURRENCY_CODE CURRENCY \n" + 
            "    FROM pay_action_information pai, pay_element_types_f pet\n" + 
            "   WHERE     pet.element_type_id = pai.action_information1\n" + 
            "         AND pai.effective_date BETWEEN pet.effective_start_date\n" + 
            "                                    AND pet.effective_end_date \n" + 
            "         AND pai.ACTION_CONTEXT_ID =?    \n" + 
            "         AND pai.ACTION_INFORMATION_CATEGORY = 'EMEA ELEMENT INFO'\n" + 
            "         AND pai.ACTION_INFORMATION3 = 'D'  and assignment_id = mbc_mobile_pkg.get_session_data('ASSIGNMENT_ID') \n" + 
            "         group by NVL (pet.REPORTING_NAME, pet.element_name) , pet.OUTPUT_CURRENCY_CODE \n" + 
            "ORDER BY 1";
            
            final String NONE_UAE_DATA="SELECT DISTINCT PETF.ELEMENT_NAME ,to_char(sum(PRRV.RESULT_VALUE),'FM999,999,990.00') ||' '||petf.OUTPUT_CURRENCY_CODE PAY_VALUE ,\n" + 
            " sum (sum (PRRV.RESULT_VALUE)) over (partition by null) total_value  ,  ' '||petf.OUTPUT_CURRENCY_CODE CURRENCY  \n" + 
            "FROM   APPS.PAY_RUN_RESULT_VALUES  PRRV\n" + 
            "      ,APPS.PAY_RUN_RESULTS        PRR\n" + 
            "      ,APPS.PAY_INPUT_VALUES_F     PIVF\n" + 
            "      ,APPS.PAY_ELEMENT_TYPES_F    PETF\n" + 
            "      ,APPS.PAY_ASSIGNMENT_ACTIONS PAA\n" + 
            "      ,APPS.PAY_PAYROLL_ACTIONS    PPA\n" + 
            "     ,APPS.PER_ALL_ASSIGNMENTS_F  PAAF\n" + 
            "      ,PAY_ELEMENT_CLASSIFICATIONS PEC\n" + 
            "WHERE  PRRV.RUN_RESULT_ID = PRR.RUN_RESULT_ID\n" + 
            "AND    PIVF.INPUT_VALUE_ID = PRRV.INPUT_VALUE_ID\n" + 
            "AND    UPPER(PIVF.NAME) = UPPER('Pay Value')\n" + 
            "AND    PIVF.UOM = 'M'\n" + 
            "AND    PETF.ELEMENT_TYPE_ID = PIVF.ELEMENT_TYPE_ID\n" + 
            "AND    PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" + 
            "AND    PEC.CLASSIFICATION_NAME = 'Voluntary Deductions'\n" + 
            "AND    PAA.ASSIGNMENT_ACTION_ID = PRR.ASSIGNMENT_ACTION_ID\n" + 
            "AND    PAA.ACTION_STATUS = 'C'\n" + 
            "AND    PPA.PAYROLL_ACTION_ID = PAA.PAYROLL_ACTION_ID\n" + 
            "AND    PPA.ACTION_TYPE IN ('R' ,'Q')\n" + 
            "AND    PAAF.ASSIGNMENT_ID = PAA.ASSIGNMENT_ID\n" + 
            "AND PPA.PAYROLL_ACTION_ID = ?\n paaf.assignment_id =mbc_mobile_pkg.get_session_data('ASSIGNMENT_ID')" + 
            "AND    PAAF.PERSON_ID     = mbc_mobile_pkg.get_session_data('PERSON_ID')  "  +"\n"+ 
            "AND    PAAF.EFFECTIVE_START_DATE =\n" + 
            "             (SELECT MAX(PAAF1.EFFECTIVE_START_DATE)\n" + 
            "               FROM   APPS.PER_ALL_ASSIGNMENTS_F PAAF1\n" + 
            "               WHERE  PAAF1.ASSIGNMENT_ID = PAAF.ASSIGNMENT_ID\n" + 
            "               AND    PAAF1.EFFECTIVE_START_DATE <= PPA.EFFECTIVE_DATE)              \n" +
            "  group by  PETF.ELEMENT_NAME  , petf.OUTPUT_CURRENCY_CODE \n" + 
            "ORDER BY 1               \n";
            
            
            if (businessGroup !=null && businessGroup.equalsIgnoreCase("81"))
                sqlStemt=UAE_DATA;
            else
                sqlStemt=NONE_UAE_DATA;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+actionContextId);
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
            statement = connection.prepareStatement(sqlStemt);

                statement.setString(1, actionContextId);
                
                
             
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
 
    @POST
    @Produces("application/json")
    @Path("/getSalaryHistory")
    public Response getSalaryHistory(@Context SecurityContext sc ,
                                     @HeaderParam("wsid") String wsid,
                                     @HeaderParam("signature") String signature
                               )
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select x.change_date,\n" + 
            "       x.reason,\n" + 
            "       x.currency_code||' '||to_char(x.current_value,'fm999,999,990.00') current_value,\n" + 
            "       to_char(x.previous_value ,'fm999,999,990.00') previous_value,\n" + 
            "       to_char (   decode(x.previous_value, 0,null,x.current_value - x.previous_value) ,'fm999,999,990.00')  change_value,\n" + 
            "    decode(x.previous_value, 0,null,round(((x.current_value - x.previous_value)/x.previous_value*100),2)||'%') change_percentage\n" + 
            "from (\n" + 
            "select to_char (ppp.change_date,'dd-MON-rrrr') change_date, hl.meaning reason,\n" + 
            "       decode(paaf.business_group_id,81,(ppp.PROPOSED_SALARY_N+(ppp.PROPOSED_SALARY_N*0.30)+greatest((ppp.PROPOSED_SALARY_N*0.05),500))\n" + 
            "                                    ,998,(ppp.PROPOSED_SALARY_N+(ppp.PROPOSED_SALARY_N*0.25)+greatest((ppp.PROPOSED_SALARY_N*0.10),300))\n" + 
            "                                    ,ppp.PROPOSED_SALARY_N) current_value,\n" + 
            "       lead(decode(paaf.business_group_id,81,(ppp.PROPOSED_SALARY_N+(ppp.PROPOSED_SALARY_N*0.30)+greatest((ppp.PROPOSED_SALARY_N*0.05),500))\n" + 
            "                                    ,998,(ppp.PROPOSED_SALARY_N+(ppp.PROPOSED_SALARY_N*0.25)+greatest((ppp.PROPOSED_SALARY_N*0.10),300))\n" + 
            "                                    ,ppp.PROPOSED_SALARY_N), 1, 0) OVER (ORDER BY ppp.change_date desc) AS previous_value,\n" + 
            "       (select pet.OUTPUT_CURRENCY_CODE\n" + 
            "        from pay_element_types_f pet, PAY_INPUT_VALUES_F piv, PER_PAY_BASES ppb, per_all_assignments_f a\n" + 
            "        where pet.ELEMENT_TYPE_ID = piv.ELEMENT_TYPE_ID\n" + 
            "        and piv.INPUT_VALUE_ID = ppb.INPUT_VALUE_ID\n" + 
            "        and a.PAY_BASIS_ID = ppb.PAY_BASIS_ID\n" + 
            "        and a.person_id = paaf.person_id\n" + 
            "        and primary_flag = 'Y'\n" + 
            "        and sysdate between a.effective_start_date and a.effective_end_date) currency_code \n" + 
            "from PER_PAY_PROPOSALS ppp, per_all_assignments_f paaf, hr_lookups hl\n" + 
            "where paaf.assignment_id = ppp.assignment_id\n" + 
            "and paaf.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')  \n" + 
            "and sysdate between paaf.effective_start_date and paaf.effective_end_Date\n" + 
            "and paaf.primary_flag = 'Y'\n" + 
            "and ppp.APPROVED = 'Y'\n" + 
            "and ppp.proposal_reason = hl.lookup_code(+)\n" + 
            "and 'PROPOSAL_REASON' = hl.lookup_type(+)\n" + 
            " and trunc(sysdate)>=trunc(ppp.change_date) \n"+
            "order by ppp.change_date desc) x" ; 
            //"order by 1 desc";
            try {
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getSalaryHistory");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection,"getSalaryHistory", "getSalaryHistory" );
            statement = connection.prepareStatement(sqlStemt);
               
              

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
}

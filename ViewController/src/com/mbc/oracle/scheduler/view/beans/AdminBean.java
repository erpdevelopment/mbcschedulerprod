package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.model.vo.AsShiftsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsWorkGroupsHdrVORowImpl;
import com.mbc.oracle.scheduler.view.util.ADFUtils;
import com.mbc.oracle.scheduler.view.util.DateConversion;
import com.mbc.oracle.scheduler.view.util.JSFUtils;
import com.mbc.oracle.scheduler.view.util.SchedulerUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class AdminBean {
    //private String srchWorkGroup;
    //private String srchShift;
    private HashMap selectedWorkGroupId;


    /*public void setSrchShift(String srchShift) {
        this.srchShift = srchShift;
    }

    public String getSrchShift() {
        return srchShift;
    }*/
    private RichPopup wgPop;
    private RichPopup wrkgrpPop;
    private RichPopup templateBulletinPop;
    private RichButton wgHelperBtn;
    

    /*public void setSrchWorkGroup(String srchWorkGroup) {
        this.srchWorkGroup = srchWorkGroup;
    }

    public String getSrchWorkGroup() {
        return srchWorkGroup;
    }*/

    public AdminBean() {

    }

    /*public void onClickSrchWorkGroupBtn(ActionEvent actionEvent) {
        // Add event code here...
        String workGroupNamePar = this.getSrchWorkGroup();
        DCIteratorBinding workGroupsHdrVOIter = ADFUtils.findIterator("AsWorkGroupsHdrVOIterator");
        ViewObjectImpl workGroupsHdrVO = (ViewObjectImpl) workGroupsHdrVOIter.getViewObject();
        if (workGroupNamePar == null || "".equals(workGroupNamePar)) {
            workGroupsHdrVO.clearViewCriterias();
            workGroupsHdrVO.executeQuery();
        } else {
            ViewCriteria vc = workGroupsHdrVO.getViewCriteria("FilterByWorkGroupNameVC");
            workGroupsHdrVO.applyViewCriteria(vc);
            workGroupsHdrVO.setNamedWhereClauseParam("WorkGroupNamePar", workGroupNamePar);
            workGroupsHdrVO.executeQuery();
        }

    }*/

    /*public void onClickWorkGroupBtn(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("CreateInsertWgHdr").execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getWgPop().show(hints);
    }*/

    public void setWgPop(RichPopup wgPop) {
        this.wgPop = wgPop;
    }

    public RichPopup getWgPop() {
        return wgPop;
    }

    public void onClickCancelWgPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("DeleteWgHdr").execute();
        ADFUtils.findOperation("Commit").execute();
        this.getWgPop().hide();

        SchedulerUtil.showPageMessage("Saving", "Changes has been saved successfully.", FacesMessage.SEVERITY_INFO);
    }

    public void onClickSaveWgPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("Commit").execute();
        this.getWgPop().hide();

        SchedulerUtil.showPageMessage("Saving", "Changes has been saved successfully.", FacesMessage.SEVERITY_INFO);
    }

    /*public void onClickSrchShiftBtn(ActionEvent actionEvent) {
        // Add event code here...
        String srchShiftPar = this.getSrchShift();
        DCIteratorBinding shiftsVOIterator = ADFUtils.findIterator("AsShiftsVOIterator");
        ViewObjectImpl shiftsVO = (ViewObjectImpl) shiftsVOIterator.getViewObject();
        if (srchShiftPar == null || "".equals(srchShiftPar)) {
            shiftsVO.clearViewCriterias();
            shiftsVO.executeQuery();
        } else {
            ViewCriteria vc = shiftsVO.getViewCriteria("FilterByShiftNameVC");
            shiftsVO.applyViewCriteria(vc);
            shiftsVO.setNamedWhereClauseParam("ShiftNamePar", srchShiftPar);
            shiftsVO.executeQuery();
        }
    }*/

    public String initAdminBTF() {


        return "";

    }


    public String initTasSummary() {
        //ADFUtils.findIterator("AllWorkGroupEmpsVOIterator").getViewObject().executeEmptyRowSet();
        //this.setSelectedAbsenceTypesId(new HashMap());
        this.setSelectedWorkGroupId(new HashMap());
        return "";
    }


    public String[] getAbsenceGanttTooltipKeyLabels() {

        return new String[] { "Employee", "Type", "Start Date", "End Date", "Days" };

    }


    public java.util.Date getTasStartDate() {
        java.util.Date retValue = new java.util.Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(retValue);
        cal.add(Calendar.DATE, -7);
        retValue = cal.getTime();
        return retValue;
    }

    public java.util.Date getTasEndDate() {
        java.util.Date retValue = new java.util.Date();
        return retValue;
    }

    public void onClickSearchTasSummaryBtn(ActionEvent actionEvent) {
        // Add event code here...
        ViewObject allWorkGroupEmpsVO = ADFUtils.findIterator("AllWorkGroupEmpsVOIterator").getViewObject();
        String concatenatedWorkGroups = this.getConcatenatedKey(this.getSelectedWorkGroupId());
        if (!"".equals(concatenatedWorkGroups)) {
            String whereClauseWrkEmp =
                " person_id in (select person_id from AS_WORK_GROUPS_LINES awgl where awgl.WORK_GROUP_ID in (" +
                concatenatedWorkGroups + "))";
            allWorkGroupEmpsVO.setWhereClause(whereClauseWrkEmp);
        } else {
            allWorkGroupEmpsVO.setWhereClause(null);
        }
        allWorkGroupEmpsVO.executeQuery();
    }

    public void onDiscloseWrkGrpSpringBoard(DisclosureEvent disclosureEvent) {
        // Add event code here...
        Number workGroupId = (Number) disclosureEvent.getComponent().getAttributes().get("WorkGroupId");
        String whereClause = "work_group_id = " + workGroupId + " AND ATTEND_FLAG='N'";
        ViewObject workGroupsEmpTasAbsenceTodayVO =
            ADFUtils.findIterator("WorkGroupsEmpTasAbsenceTodayVOIterator").getViewObject();
        workGroupsEmpTasAbsenceTodayVO.setNamedWhereClauseParam("AttendFlagPar", "N");
        workGroupsEmpTasAbsenceTodayVO.setWhereClause(whereClause);
        workGroupsEmpTasAbsenceTodayVO.executeQuery();

        whereClause = "work_group_id = " + workGroupId + " AND ATTEND_FLAG='Y'";
        ViewObject workGroupsEmpTasAttendTodayVO =
            ADFUtils.findIterator("WorkGroupsEmpTasAttendTodayVOIterator").getViewObject();
        workGroupsEmpTasAbsenceTodayVO.setNamedWhereClauseParam("AttendFlagPar", "Y");
        workGroupsEmpTasAttendTodayVO.setWhereClause(whereClause);
        workGroupsEmpTasAttendTodayVO.executeQuery();

    }


    public String onClickCancelShiftsBtn() {
        // Add event code here...
        //String dmlMode = (String) JSFUtils.resolveExpression("#{pageFlowScope.dmlMode}");

        ADFUtils.findOperation("Rollback").execute();

        return "shifts";
    }

    public void onClickDeleteShiftBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number ShiftId = (Number) actionEvent.getComponent().getAttributes().get("ShiftId");
        DCIteratorBinding iter = ADFUtils.findIterator("AsShiftsVOIterator");
        Key key = new Key(new Object[] { ShiftId });
        iter.setCurrentRowWithKey(key.toStringFormat(true));

        AsShiftsVORowImpl currRow = (AsShiftsVORowImpl) iter.getCurrentRow();
        int shftWrkgrpCount = currRow.getAsShiftWorkGroupsVO().getAllRowsInRange().length;
        if (shftWrkgrpCount > 0) {
            JSFUtils.addFacesErrorMessage("Please delete workgroups inside shift first.");
        } else {
            ADFUtils.findOperation("DeleteShift").execute();
            ADFUtils.findOperation("Commit").execute();
            JSFUtils.addFacesInformationMessage("Shift has been deleted successfully.");

        }
    }

    public void onClickDeleteWrkgrpBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number workGroupId = (Number) actionEvent.getComponent().getAttributes().get("WorkGroupId");
        DCIteratorBinding iter = ADFUtils.findIterator("AsWorkGroupsHdrVOIterator");
        Key key = new Key(new Object[] { workGroupId });
        iter.setCurrentRowWithKey(key.toStringFormat(true));
        
        AsWorkGroupsHdrVORowImpl currRow =
            (AsWorkGroupsHdrVORowImpl) iter.getCurrentRow();
        int wrkgrpLinesCount = currRow.getAsWorkGroupsLinesVO().getAllRowsInRange().length;
        if (wrkgrpLinesCount > 0) {
            JSFUtils.addFacesErrorMessage("Please delete Staff(s) inside Workgroup first.");
        } else {
            ADFUtils.findOperation("DeleteWgHdr").execute();
            ADFUtils.findOperation("Commit").execute();
            JSFUtils.addFacesInformationMessage("Workgroup has been deleted successfully.");

        }
    }


    public void onClickSaveEditShiftBtn(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("Commit").execute();
        SchedulerUtil.showPageMessage("Saving", "Changes has been saved successfully.", FacesMessage.SEVERITY_INFO);
    }

    public void onClickSaveEditWGBtn(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("Commit").execute();
        SchedulerUtil.showPageMessage("Saving", "Changes has been saved successfully.", FacesMessage.SEVERITY_INFO);
    }

    public void onClickSavePatternsBtn(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("Commit").execute();
        SchedulerUtil.showPageMessage("Saving", "Changes has been saved successfully.", FacesMessage.SEVERITY_INFO);
    }

    public void initTasPanelVO() {
        // Add event code here...
        System.out.println("Calling ::: initTasPanelVO");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date tasDate = null;
        try {
            tasDate = sdf.parse("01-01-2017");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date tasDatePar = DateConversion.getJboDateFromUtilDate(tasDate);
        Date schdlrDatePar = new Date(Date.getCurrentDate());
        ViewObject asWorkgroupSchdlrTasVO = ADFUtils.findIterator("AsWorkgroupSchdlrTasVOIterator").getViewObject();
        //asWorkgroupSchdlrTasVO.setNamedWhereClauseParam("SchdlrDatePar", schdlrDatePar);
        //asWorkgroupSchdlrTasVO.setNamedWhereClauseParam("TasDatePar", tasDatePar);
    }

    public void setSelectedWorkGroupId(HashMap selectedWorkGroupId) {
        this.selectedWorkGroupId = selectedWorkGroupId;
    }

    public HashMap getSelectedWorkGroupId() {
        return selectedWorkGroupId;
    }

    private String getConcatenatedKey(Map map) {
        String retValue = "";
        Iterator iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry pair = (Map.Entry) iter.next();
            retValue = retValue + pair.getKey() + ",";
        }
        if (retValue != null && retValue.length() > 0) {
            retValue = retValue.substring(0, retValue.length() - 1);
        }
        return retValue;
    }

    public void setWrkgrpPop(RichPopup wrkgrpPop) {
        this.wrkgrpPop = wrkgrpPop;
    }

    public RichPopup getWrkgrpPop() {
        return wrkgrpPop;
    }

    public void onClickSelectWrkgrpImageBtn(ActionEvent actionEvent) {
        // Add event code here...
        String lookupCode = (String) ADFUtils.getBoundAttributeValue("LookupCode");
        String meaning = (String) ADFUtils.getBoundAttributeValue("Meaning");
        String description = (String) ADFUtils.getBoundAttributeValue("Description");
        System.out.println("lookupCode=" + lookupCode + " ,meaning=" + meaning + " ,description=" + description);
        ADFUtils.setBoundAttributeValue("ImageName", meaning);
        ADFUtils.setBoundAttributeValue("ImagePath", description);
        ADFUtils.setBoundAttributeValue("ImageLookupCode", lookupCode);

        this.getWrkgrpPop().hide();

    }

    public String getApplicationURL() {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fCtx.getExternalContext();
        String url =
            ectx.getRequestScheme() + "://" + ectx.getRequestServerName() + ":" + ectx.getRequestServerPort() +
            ectx.getRequestContextPath();
        //System.out.println("URL=" + url);
        return url;
    }


    public void onClickWrkgroupLink(ActionEvent actionEvent) {
        // Add event code here...
        Number workGroupId = (Number) actionEvent.getComponent().getAttributes().get("WorkGroupId");
        DCIteratorBinding iter = ADFUtils.findIterator("AsWorkGroupsHdrVOIterator");
        Key key = new Key(new Object[] { workGroupId });
        iter.setCurrentRowWithKey(key.toStringFormat(true));
    }

    public void onClickShiftLink(ActionEvent actionEvent) {
        // Add event code here...
        Number shiftId = (Number) actionEvent.getComponent().getAttributes().get("ShiftId");
        DCIteratorBinding iter = ADFUtils.findIterator("AsShiftsVOIterator");
        Key key = new Key(new Object[] { shiftId });
        iter.setCurrentRowWithKey(key.toStringFormat(true));
    }

    public void setWgHelperBtn(RichButton wgHelperBtn) {
        this.wgHelperBtn = wgHelperBtn;
    }

    public RichButton getWgHelperBtn() {
        return wgHelperBtn;
    }

    public void onChangeEmployeeNameInWG(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        ActionEvent actionEvent=new ActionEvent(this.getWgHelperBtn());
        actionEvent.queue();
    }
    public void onDiscloseSecurityTabs(DisclosureEvent disclosureEvent)
    {        
           ViewObject VO =ADFUtils.findIterator("AsOrganizationVO1Iterator").getViewObject();
            VO.executeQuery();
         
        }


    public void setTemplateBulletinPop(RichPopup templateBulletinPop) {
        this.templateBulletinPop = templateBulletinPop;
    }

    public RichPopup getTemplateBulletinPop() {
        return templateBulletinPop;
    }
    public void onClickEmployeeBulletinsBtn(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getTemplateBulletinPop().show(hints);
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public String SaveTemplateEmployess_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        OperationBinding operationBinding2 = bindings.getOperationBinding("Execute");
        Object result2 = operationBinding2.execute();
        return null;
    }


    public String CloseTemplateBulletinPopup_action() {
        
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Execute");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        this.getTemplateBulletinPop().hide();
       return null;
    }

    public String DeleteTemplateBulletin_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding3 = bindings.getOperationBinding("DeleteTemplateBulletin");
        Object result3 = operationBinding3.execute();
        if (!operationBinding3.getErrors().isEmpty()) {
            return null;
        }
        
      
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
//        OperationBinding operationBinding2 = bindings.getOperationBinding("Execute");
//        Object result2 = operationBinding2.execute();
        return null;
    }

// added 26-August-2018//
    
        public void deleteWorkgroupEmployee_Dialogue_Listener(DialogEvent dialogEvent) {
        // Add event code here...
        
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok)
        {
                deleteworkgroup_action();

            }

        
    }
    public void deleteShiftWorkgroup_Dialogue_Listener(DialogEvent dialogEvent) {
    // Add event code here...
    
    if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok)
    {
           deleteShiftworkgroup_action();

        }

    
    }
 
    public void deleteBulletin_Dialogue_Listener(DialogEvent dialogEvent) {
    // Add event code here...
    
    if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok)
    {
           deleteBulletin_action();

        }

    
    }
public String commit_action()
      {  BindingContainer bindings = getBindings();
           OperationBinding operationBinding = bindings.getOperationBinding("Commit");
           Object result = operationBinding.execute();
           if (!operationBinding.getErrors().isEmpty()) {
               return null;
           }
           return null;
    
       }
    public String deleteworkgroup_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Delete");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        commit_action();
        return null;
    }
    public String deleteShiftworkgroup_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("DeleteShiftWorkGroup");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        commit_action();
        return null;
    }
    public String deleteBulletin_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("DeleteBulletin");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        commit_action();
        return null;
    }

    public String Add_new_Workgroup_for_shift() {
      
        ADFUtils.findOperation("Commit").execute();
//        OperationBinding operationBinding2 = bindings.getOperationBinding("Commit");
//        Object result2 = operationBinding2.execute();
//        if (!operationBinding2.getErrors().isEmpty()) {
//            return null;
//        }
          BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }
}

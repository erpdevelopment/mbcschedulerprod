--------------------------------------------------------
--  DDL for Table AS_WORK_GROUPS_HDR
--------------------------------------------------------

  CREATE TABLE "APPS"."AS_WORK_GROUPS_HDR" ("WORK_GROUP_ID" NUMBER, "WORK_GROUP_NAME" VARCHAR2(256), "ENABLED_FLAG" VARCHAR2(1) DEFAULT 'Y', "CREATION_DATE" DATE, "CREATED_BY" NUMBER, "LAST_UPDATE_DATE" DATE, "LAST_UPDATED_BY" NUMBER, "LAST_UPDATE_LOGIN" NUMBER, "IMAGE_LOOKUP_CODE" VARCHAR2(30))

import  {NavController, ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { AppModule } from '../../../app/app.module';
import { Navbar } from 'ionic-angular';
import { ViewChild, Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AES256 } from '@ionic-native/aes-256';

/**
 * Generated class for the PublicSpacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: 'page-teamdetails',
  templateUrl: 'teamdetails.html',
})







 

 

export class TeamDetailsPage {
  character;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,  private sanitized: DomSanitizer,public aes256:AES256
  ) {
   
     this.character =this.params.get('info');
    
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  ionViewDidLoad() {

 
    console.log('ionViewDidLoad MyTeamPage');
  }


  geturl(personId)
{

  let token=AppModule.getProperty('wsid');
  token=token.replace(/#/g,'@@@@')
  token=encodeURI(token);
  let url=AppModule.wsURL.substr(0,AppModule.wsURL.lastIndexOf("/"));
  let t=   AppModule.doHash(this.aes256,AppModule.getProperty('wsid')+personId);
  let x=encodeURI(t+'');
  x=x+'';
  return url+'/imageservlet?px='+personId+'&token='+token+'&signature='+x;

}


}

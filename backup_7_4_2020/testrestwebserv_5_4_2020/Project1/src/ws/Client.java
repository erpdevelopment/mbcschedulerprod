package ws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Client {

	
	public static void main(String[] args) {

	  try {

		URL url = new URL("https://idcs-1347a225e3e34a54a6f00b36ad2abbe8.identity.oraclecloud.com/oauth2/v1/token");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                conn.setRequestProperty("Authorization", "Basic MmIyMTRjNDc4ZDM2NDIxNWJkY2Y1MmE2NWZlNWMxMzE6M2VjYzg2YWYtZGI2OC00NzZjLTk1ZTktMDU3YzNkNTA0Yzdh");
	     // conn.setRequestProperty("grant_type", "client_credentials");
	     // conn.setRequestProperty("scope", "https://FAEE5CBB8B4248F4BDB7C0B0043D61D9.mobile.ocp.oraclecloud.com:443urn:opc:resource:consumer::all");
	//	String input = "{\"grant_type\":client_credentials,\"scope\":\" https://FAEE5CBB8B4248F4BDB7C0B0043D61D9.mobile.ocp.oraclecloud.com:443urn:opc:resource:consumer::all \"}";
	    //  String input = "{grant_type=client_credentials,scope=https://FAEE5CBB8B4248F4BDB7C0B0043D61D9.mobile.ocp.oraclecloud.com:443urn:opc:resource:consumer::all}";
	      String input = "grant_type=client_credentials&scope=https://FAEE5CBB8B4248F4BDB7C0B0043D61D9.mobile.ocp.oraclecloud.com:443urn:opc:resource:consumer::all";
		System.out.println(input);
                OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (! (conn.getResponseCode() == HttpURLConnection.HTTP_CREATED || conn.getResponseCode()==HttpURLConnection.HTTP_OK)) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ conn.getResponseMessage());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}

		conn.disconnect();

	  } catch (MalformedURLException e) {

		e.printStackTrace();

	  } catch (IOException e) {

		e.printStackTrace();

	 }

	}

}

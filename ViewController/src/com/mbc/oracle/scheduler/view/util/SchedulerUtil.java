package com.mbc.oracle.scheduler.view.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.io.InputStream;

import java.io.OutputStream;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import oracle.adf.controller.ControllerContext;

public class SchedulerUtil {
    public SchedulerUtil() {
        super();
    }

    public static void showPageMessage(String msgTitle, String msgBody, FacesMessage.Severity msgType) {
        FacesMessage fMsg = new FacesMessage(msgTitle, msgBody);
        fMsg.setSeverity(msgType);
        FacesContext.getCurrentInstance().addMessage(null, fMsg);
    }

    public static void redirectToView(String viewId) {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ExternalContext eCtx = fCtx.getExternalContext();

        String activityUrl = ControllerContext.getInstance().getGlobalViewActivityURL(viewId);
        try {
            eCtx.redirect(activityUrl);
        } catch (IOException e) {
            e.printStackTrace();
            JSFUtils.addFacesErrorMessage("Exception when redirect to " + viewId);
        }
    }

    public static void navigateTo(String navigationOutcome) {
        FacesContext context = FacesContext.getCurrentInstance();
        Application application = context.getApplication();
        application.getNavigationHandler().handleNavigation(context, null, navigationOutcome);
    }

    public static byte[] getBytesFromInputStream(InputStream is) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[16384];

        try {
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
                buffer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer.toByteArray();
    }

    public static void copyIsToOs(InputStream is, OutputStream os) {
        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = is.read(buffer)) != -1) {
                os.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

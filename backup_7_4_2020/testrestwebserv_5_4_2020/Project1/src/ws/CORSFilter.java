package ws;

import java.io.IOException;

import javax.ws.rs.POST;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class CORSFilter implements ContainerResponseFilter {

   @Override
    public void filter(final ContainerRequestContext requestContext,
                      final ContainerResponseContext cres) throws IOException {
  
  
      cres.getHeaders().add("Access-Control-Allow-Origin", "*");
      cres.getHeaders().add("Access-Control-Allow-Headers", "userName,userPass ,toUser ,Access-Control-Allow-Headers,Access-Control-Allow-Origin, content-type, accept, authorization , " +
                            "segment1 , segment2 , segment3, segment4, segment5, segment6, segment7, segment8, segment9, segment10, segment11, segment12, segment13, segment14, segment15," +
                            "segment16 , segment17 , segment18, segment19, segment20, segment21, segment22, segment23, segment24, segment25, segment26, segment27, segment28, segment29, segment30," +
                            "status,sitTitle,businessGroupid,businessGroup,personId,attachId,fileData,userId,assignmentId,dateStart,dateEnd,absenceAttendanceTypeId,absenceTypeName,attribute1,userID," +
                            "attribute2,attribute3,attribute4,attribute5,attribute6,attribute7,attribute8,actionContextId,notificationId,itemType,itemKey,activityId,buttonName,additionalParameter,lookUpType," +
                            "attributes,fromUser,generatorApi,processActivity,filterType,User,newRole,comments,fromRole,faceId,applicationId,fromPersonId,toPersonId,appVersion,replacedPerson , cardType ,subjectId ,ruleId,"+
                            "resourceName,oracle-mobile-backend-id," +
                            "directorateType,year,monthNo,organizationId," +
                            "wsid,signature,bu," +
                            "s,a,av,it,ik,ai,bn,ntf,gn,pa,ft,rxd,acx,apx,fcx,atx,fd,absAttx,abst,rp,token,tk,"+
                                                            "prId,orgId,wkId");
      cres.getHeaders().add("Access-Control-Allow-Credentials", "true");
      cres.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
      cres.getHeaders().add("Access-Control-Max-Age", "1209600");
   }

}

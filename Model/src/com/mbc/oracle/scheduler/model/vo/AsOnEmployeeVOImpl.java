package com.mbc.oracle.scheduler.model.vo;

import oracle.jbo.domain.Number;
import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Mar 07 14:37:02 GST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AsOnEmployeeVOImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public AsOnEmployeeVOImpl() {
    }

    /**
     * Returns the bind variable value for org_id.
     * @return bind variable value for org_id
     */
    public Number getorg_id() {
        return (Number) getNamedWhereClauseParam("org_id");
    }

    /**
     * Sets <code>value</code> for bind variable org_id.
     * @param value value to bind as org_id
     */
    public void setorg_id(Number value) {
        setNamedWhereClauseParam("org_id", value);
    }

    /**
     * Returns the bind variable value for pdate.
     * @return bind variable value for pdate
     */
    public String getpdate() {
        return (String) getNamedWhereClauseParam("pdate");
    }

    /**
     * Sets <code>value</code> for bind variable pdate.
     * @param value value to bind as pdate
     */
    public void setpdate(String value) {
        setNamedWhereClauseParam("pdate", value);
    }

    /**
     * Returns the bind variable value for P_fullname.
     * @return bind variable value for P_fullname
     */
    public String getP_fullname() {
        return (String) getNamedWhereClauseParam("P_fullname");
    }

    /**
     * Sets <code>value</code> for bind variable P_fullname.
     * @param value value to bind as P_fullname
     */
    public void setP_fullname(String value) {
        setNamedWhereClauseParam("P_fullname", value);
    }

    /**
     * Returns the bind variable value for p_workgroup_id.
     * @return bind variable value for p_workgroup_id
     */
    public Number getp_workgroup_id() {
        return (Number) getNamedWhereClauseParam("p_workgroup_id");
    }

    /**
     * Sets <code>value</code> for bind variable p_workgroup_id.
     * @param value value to bind as p_workgroup_id
     */
    public void setp_workgroup_id(Number value) {
        setNamedWhereClauseParam("p_workgroup_id", value);
    }

    /**
     * Returns the bind variable value for PSHIFT_ID.
     * @return bind variable value for PSHIFT_ID
     */
    public String getPSHIFT_ID() {
        return (String) getNamedWhereClauseParam("PSHIFT_ID");
    }

    /**
     * Sets <code>value</code> for bind variable PSHIFT_ID.
     * @param value value to bind as PSHIFT_ID
     */
    public void setPSHIFT_ID(String value) {
        setNamedWhereClauseParam("PSHIFT_ID", value);
    }
}


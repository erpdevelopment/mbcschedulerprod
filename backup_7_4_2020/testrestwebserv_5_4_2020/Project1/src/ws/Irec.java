package ws;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

@Path("irec")
public class Irec {
    public Irec() {
        super();
    }
    
    
    @POST
    @Produces("application/json")
    @Path("/getPositions")
    public Response getPositions(@Context SecurityContext sc,
                                @HeaderParam("businessGroup") String businessGroup,
                                @HeaderParam("positionName") String positionName)
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
        
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="Select POSITION_ID , name POSITION_NAME , BUSINESS_GROUP_ID FROM HR_ALL_POSITIONS_F_VL  \n" + 
            "            WHERE NAME  LIKE ? \n" + 
            "            and BUSINESS_GROUP_ID= ? ";
            try {
         
                connection = Helper.getOracleConnection();
                
                
                //Helper.logActivity(connection, "Open Bank Info", "Open Bank Info" );
            statement = connection.prepareStatement(sqlStemt);
               
               statement.setString(1, positionName);
                statement.setString(2, businessGroup);

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    
    @POST
    @Produces("application/json")
    @Path("/createVacancy")
    public Response createVacancy(
    
        @Context SecurityContext sc  
                                                
                                                
                                                ,@HeaderParam("dateFrom") String dateFrom
                                                ,@HeaderParam("businessGroup") String businessGroup
                                                ,@HeaderParam("vacancyName") String vacancyName
                                                ,@HeaderParam("positionId") String positionId
                                                ,@HeaderParam("description") String description
                                                     )
    
    {
    if (!sc.isUserInRole("Authenticated"))  
    throw new SecurityException("User is unauthorized.");
    

    Response response=null;
    Connection connection = null;
    CallableStatement cs=null;
    String result = null;
    

    String block="DECLARE\n" + 
    "  P_BUSINESS_GROUP_ID NUMBER;\n" + 
    "  P_DATE_FROM DATE;\n" + 
    "  P_VACANCY_NAME VARCHAR2(32767);\n" + 
    "  P_POSITION_ID NUMBER;\n" + 
    "  P_DESCRIPTION VARCHAR2(32767);\n" + 
    "  X_VACANCY_ID NUMBER;\n" + 
    "  X_REQUISITION_ID NUMBER;\n" + 
    "  X_SQLERRM VARCHAR2(32767);\n" + 
    " \n" + 
    "  V_OUTPUT  VARCHAR2(32767) :='[ { \"vacancy_id\":\"&vacancy_id\",\"requisition_id\":\"&requisition_id\" ,  \"status\":\"1\" } ]';\n" + 
    " \n" + 
    "BEGIN\n" + 
    "  P_BUSINESS_GROUP_ID := ?;\n" + 
    "  P_DATE_FROM := ? ;\n" + 
    "  P_VACANCY_NAME := ?;\n" + 
    "  P_POSITION_ID := ?;\n" + 
    "  P_DESCRIPTION := ? ;\n" + 
    "  X_VACANCY_ID := NULL;\n" + 
    "  X_REQUISITION_ID := NULL;\n" + 
    "  X_SQLERRM := NULL;\n" + 
    " \n" + 
    "  APPS.XXMBC_IREC_PKG.CREATE_VACANCY ( P_BUSINESS_GROUP_ID, P_DATE_FROM, P_VACANCY_NAME, P_POSITION_ID, P_DESCRIPTION, X_VACANCY_ID, X_REQUISITION_ID, X_SQLERRM );\n" + 
    "    COMMIT;\n" + 
    "    IF X_SQLERRM IS NOT NULL THEN\n" + 
    "     V_OUTPUT  :='[ { \"error\":\"&error\",  \"status\":\"0\" } ]';\n" + 
    "      V_OUTPUT:= REPLACE(V_OUTPUT,'&error',X_SQLERRM);\n" + 
    "     \n" + 
    "    ELSE\n" + 
    "   \n" + 
    "     V_OUTPUT:= REPLACE(V_OUTPUT,'&vacancy_id',X_VACANCY_ID);\n" + 
    "     V_OUTPUT:= REPLACE(V_OUTPUT,'&requisition_id',X_REQUISITION_ID);\n" + 
    "    END IF; \n" + 
    "  \n" + 
    " ?:=V_OUTPUT;" + 
    " \n" + 
    "END;";
    
    try {
        
    if (description!=null)
     description=URLDecoder.decode(description);
        
        connection = Helper.getOracleConnection();
        
        
        
         cs=  connection.prepareCall(block);
        
        cs.setString(1,businessGroup );
        cs.setString(2,dateFrom );
        cs.setString(3,vacancyName );
        cs.setString(4,positionId );
        cs.setString(5,description );
    cs.registerOutParameter(6,Types.VARCHAR);


        cs.executeUpdate();
       
    result  =cs.getString(6);
       
        response=Response.ok(result).build();
        
    
    }
        catch(SecurityException s)
        {
            s.printStackTrace();
            response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            }
    
    catch(Exception e)
    {
        e.printStackTrace();
            response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
        }
    finally {
                try {
                    if ((connection != null) && !(connection.isClosed()))
                        connection.close();
                    if ((cs != null) && !(cs.isClosed()))
                                    cs.close();
                } catch (SQLException sqle) {
                    // TODO: Add catch code
                    sqle.printStackTrace();
                }
            }
    
      return response; 
    }  
    
}

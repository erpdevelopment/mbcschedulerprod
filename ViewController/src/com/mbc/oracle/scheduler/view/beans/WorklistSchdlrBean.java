package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import java.util.Set;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichCalendar;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.CalendarActivityEvent;
import oracle.adf.view.rich.model.CalendarActivity;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.adf.view.rich.util.CalendarActivityRamp;
import oracle.adf.view.rich.util.InstanceStyles;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class WorklistSchdlrBean {
    private String filterEmpStr;
    private RichCalendar schdlrCalendar;
    private HashMap<Number, String> calFilterEmpMap;
    private HashMap calStyles = new HashMap<Set<String>, InstanceStyles>();
    public void setCalFilterEmpMap(HashMap<Number, String> calFilterEmpMap) {
        this.calFilterEmpMap = calFilterEmpMap;
    }

    public HashMap<Number, String> getCalFilterEmpMap() {
        if (calFilterEmpMap == null)
            calFilterEmpMap = new HashMap();
        return calFilterEmpMap;
    }

    

    public void setSchdlrCalendar(RichCalendar schdlrCalendar) {
        this.schdlrCalendar = schdlrCalendar;
    }

    public RichCalendar getSchdlrCalendar() {
        return schdlrCalendar;
    }

    public void setFilterEmpStr(String filterEmpStr) {
        this.filterEmpStr = filterEmpStr;
    }

    public String getFilterEmpStr() {
        return filterEmpStr;
    }

    public WorklistSchdlrBean() {
    }

    public void onClickFilterEmpBtn(ActionEvent actionEvent) {
        // Add event code here...
        ViewObject asSchedulerAvailStaffVO = ADFUtils.findIterator("AsSchedulerAvailStaffVOIterator").getViewObject();
        if (this.getFilterEmpStr() != null) {

            asSchedulerAvailStaffVO.setWhereClause(" upper(employee_name) like upper('%" + this.getFilterEmpStr() +
                                                   "%')");
        } else {
            asSchedulerAvailStaffVO.setWhereClause(null);
        }
        asSchedulerAvailStaffVO.executeQuery();

    }

    public void onClickClearFilterEmpBtn(ActionEvent actionEvent) {
        // Add event code here...
        this.setFilterEmpStr(null);
        ViewObject asSchedulerAvailStaffVO = ADFUtils.findIterator("AsSchedulerAvailStaffVOIterator").getViewObject();
        asSchedulerAvailStaffVO.setWhereClause(null);
        asSchedulerAvailStaffVO.executeQuery();
    }

    public void onChangeShiftWgNavigationList(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        UIComponent comp = valueChangeEvent.getComponent();
        comp.processUpdates(FacesContext.getCurrentInstance());
        Row asSchedulerShiftsRow = ADFUtils.findIterator("AsSchedulerShiftsVOIterator").getCurrentRow();
        Number shiftId = (Number) asSchedulerShiftsRow.getAttribute("ShiftId");
        Number workGroupId = (Number) asSchedulerShiftsRow.getAttribute("WorkGroupId");

        ViewObject asSchedulerAvailStaffVO = ADFUtils.findIterator("AsSchedulerAvailStaffVOIterator").getViewObject();
        asSchedulerAvailStaffVO.setWhereClause("SHIFT_ID=" + shiftId + " and WORK_GROUP_ID=" + workGroupId);
        asSchedulerAvailStaffVO.executeQuery();

        asSchedulerAvailStaffVO.setWhereClause(null);
    }

   

    public void schdlrCalendarActivityListener(CalendarActivityEvent calendarActivityEvent) {
        // Add event code here...
        CalendarActivity activity = calendarActivityEvent.getCalendarActivity();
        this.setCurrentRowUsingCalActivity(activity);
    }

    private void setCurrentRowUsingCalActivity(CalendarActivity activity) {
        if (activity != null) {
            DCIteratorBinding iterator = ADFUtils.findIterator("AsSchedulerStaffVOIterator");
            Key key = new Key(new Object[] { activity.getId() });
            RowSetIterator rsi = iterator.getRowSetIterator();
            Row row = rsi.findByKey(key, 1)[0];
            rsi.setCurrentRow(row);
        }
    }

    public void onClickRemoveTagBtn(ClientEvent clientEvent) {
        // Add event code here...
        String employeeName = (String) clientEvent.getParameters().get("EmployeeNamePar");
        //System.out.println("employeeName=" + employeeName);
        this.getCalFilterEmpMap().values().remove(employeeName);
        this.applyCalendarFilter();

        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSchdlrCalendar());
    }

    public void onClickFilterCalendarCntxtPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number personId = (Number) ADFUtils.getBoundAttributeValue("PersonIdAvailStaff");
        String employeeName = (String) ADFUtils.getBoundAttributeValue("EmployeeNameAvailStaff");
        this.getCalFilterEmpMap().put(personId, employeeName);
        String jsScript = "$('#CalFilterTags').tagit('createTag', '" + employeeName + "');";

        //System.out.println("personId=" + personId + " ,employeeName=" + employeeName + " ,jsScript=" + jsScript);

        this.runJavaScriptCode(jsScript);

        this.applyCalendarFilter();
    }

    private void applyCalendarFilter() {
        ViewObject asSchedulerStaffVO = ADFUtils.findIterator("AsSchedulerStaffVOIterator").getViewObject();
        if (this.getCalFilterEmpMap().isEmpty()) {
            asSchedulerStaffVO.setWhereClause(null);
        } else {
            String personIdExtendedStr = "";
            for (Map.Entry<Number, String> entry : this.getCalFilterEmpMap().entrySet()) {
                personIdExtendedStr = personIdExtendedStr + "," + entry.getKey();
            }
            personIdExtendedStr = personIdExtendedStr.substring(1);
            asSchedulerStaffVO.setWhereClause("PERSON_ID in (" + personIdExtendedStr + ")");
        }
        asSchedulerStaffVO.executeQuery();
    }

    public void runJavaScriptCode(String javascriptCode) {
        FacesContext facesCtx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service = Service.getRenderKitService(facesCtx, ExtendedRenderKitService.class);
        service.addScript(facesCtx, javascriptCode);
    }


    public void setCalStyles(HashMap calStyles) {
        this.calStyles = calStyles;
    }

    public HashMap getCalStyles() {
        // return activityStyles;
         try {
         HashSet LeaveStatus1 = new HashSet<String>();
         HashSet LeaveStatus2= new HashSet<String>();
         HashSet LeaveStatus3 = new HashSet<String>();
         HashSet LeaveStatus4= new HashSet<String>();
        // HashSet event3= new HashSet<String>();

         LeaveStatus1.add("N");
         LeaveStatus2.add("A");
         LeaveStatus3.add("L");
         LeaveStatus4.add("AL");    
        // event3.add("103");
        
         calStyles.put(LeaveStatus1, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.BLUE ));
         calStyles.put(LeaveStatus2, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.GREEN ));
          calStyles.put(LeaveStatus3, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.ORANGE ));
         calStyles.put(LeaveStatus4, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.RED ));
        // activityStyles.put(event3, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.MIDNIGHTBLUE ));

         } catch (Exception e) {
         e.printStackTrace();
         }
         return calStyles;
    }
}

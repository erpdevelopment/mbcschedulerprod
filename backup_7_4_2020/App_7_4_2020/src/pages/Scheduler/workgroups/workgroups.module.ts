import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkgroupsPage } from './workgroups';

@NgModule({
  declarations: [
    WorkgroupsPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkgroupsPage),
  ],
})
export class WorkgroupsPageModule {}

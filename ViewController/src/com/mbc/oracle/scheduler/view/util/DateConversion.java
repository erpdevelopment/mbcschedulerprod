package com.mbc.oracle.scheduler.view.util;

public class DateConversion {
    public static oracle.jbo.domain.Date getJboDateFromUtilDate(java.util.Date utilDate) {
        oracle.jbo.domain.Date jboDate = null;
        if (utilDate != null) {
            jboDate = new oracle.jbo.domain.Date(DateConversion.getSqlDateFromUtilDate(utilDate));
        }
        return jboDate;
    }

    public static oracle.jbo.domain.Date getJboDateFromSqlDate(java.sql.Date sqlDate) {
        oracle.jbo.domain.Date jboDate = null;
        if (sqlDate != null) {
            jboDate = new oracle.jbo.domain.Date(sqlDate);
        }
        return jboDate;
    }

    public static java.sql.Date getSqlDateFromJboDate(oracle.jbo.domain.Date jboDate) {
        return jboDate.dateValue();
    }

    public static java.sql.Date getSqlDateFromUtilDate(java.util.Date utilDate) {
        return new java.sql.Date(utilDate.getTime());
    }

    public static java.util.Date getUtilDateFromJboDate(oracle.jbo.domain.Date jboDate) {
        java.util.Date utilDate = null;
        if (jboDate != null) {
            utilDate = new java.util.Date(jboDate.dateValue().getTime());
        }
        return utilDate;
    }

    public static java.util.Date getUtilDateFromSqlDate(java.sql.Date sqlDate) {
        return new java.util.Date(sqlDate.getTime());
    }
    /*public static void main(String[] args) {
     //#1 : Assign every Date to current Date
     oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(oracle.jbo.domain.Date.getCurrentDate());
     java.util.Date utilDate = new java.util.Date(); //automatically constructor get current date
     java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis());
     //#2 : Test whole method in Class
     //Test getJboDateFromUtilDate
     jboDate = DateConversion.getJboDateFromUtilDate(utilDate);
     System.out.println(jboDate.toString());
     //Test getJboDateFromSqlDate
     jboDate = getJboDateFromSqlDate(sqlDate);
     System.out.println(jboDate.toString());
     //Test getSqlDateFromJboDate
     sqlDate = getSqlDateFromJboDate(jboDate);
     System.out.println(sqlDate.toString());
     //Test getSqlDateFromJboDate
     sqlDate = getSqlDateFromUtilDate(utilDate);
     System.out.println(sqlDate.toString());
     //Test getSqlDateFromJboDate
     utilDate = getUtilDateFromJboDate(jboDate);
     System.out.println(utilDate.toString());
     //Test getSqlDateFromJboDate
     utilDate = getUtilDateFromSqlDate(sqlDate);
     System.out.println(utilDate.toString());
   }  */
}

var ageChartData = [0, 0, 0, 0, 0, 0, 0];
var ageChartCategories = ['<20', '<30', '<40', '<50', '<60', '<65', '&ge;65'];

var ageDistDataSource = new kendo.data.DataSource( {
    transport :  {
        read :  {
            // the remote service url
            url : "http://10.10.131.26:7003/schedulerRest/rest/AASchedules1.0/DashboardAgeDistVO", 
            // the data type of the returned result
            dataType : "json", type : "GET"
        }
    },
    // describe the result format
    schema :  {
        // the data, which the data source will be bound to is in the "list" field of the response
        data : "items", model :  {
            fields :  {
                AgePattern :  {
                    type : "string"
                },
                AgeCount :  {
                    type : "number"
                },
                links :  {
                    rel :  {
                        type : "string"
                    },
                    href :  {
                        type : "string"
                    },
                    name :  {
                        type : "string"
                    },
                    kind :  {
                        type : "string"
                    }
                }
            }
        }
    },
    change: function() {
        var data = this.data(); // or this.view();
        for (var i = 0; i < data.length; i++) {
            if ( data[i].AgePattern == '<20'){ ageChartData[0] = data[i].AgeCount; }
            else if( data[i].AgePattern == '<30'){ ageChartData[1] = data[i].AgeCount; }
            else if( data[i].AgePattern == '<40'){ ageChartData[2] = data[i].AgeCount; }
            else if( data[i].AgePattern == '<50'){ ageChartData[3] = data[i].AgeCount; }
            else if( data[i].AgePattern == '<60'){ ageChartData[4] = data[i].AgeCount; }
            else if( data[i].AgePattern == '<65'){ ageChartData[5] = data[i].AgeCount; }
            else if( data[i].AgePattern == '>65'){ ageChartData[6] = data[i].AgeCount; }
        }
        ageChartCreate();
    }
});

ageDistDataSource.read();

function ageChartCreate() {
    $("#_workforce_demographics_age_bar").kendoChart( {
        theme : 'material', legend :  {
            visible : false
        },
        chartArea :  {
            background : ""
        },
        seriesDefaults :  {
            type : "column"
        },
        series : [{name : "Headcount", /*color: "#30A3D9",*/color : function (point) {if (point.category == "&ge;65") {return "#FF0000";}else {return "#30A3D9"}},data : ageChartData}], valueAxis :  {
            visible : false, line :  {
                visible : false
            },
            majorGridLines :  {
                visible : false
            },
            axisCrossingValue : 0
        },
        categoryAxis :  {
            categories : ageChartCategories, labels :  {
                font : "12px Calibri,Arial"
            },
            line :  {
                visible : false
            },
            majorGridLines :  {
                visible : false
            }
        },
        tooltip :  {
            visible : true, format : "{0}", template : "#= series.name #: #= value #"
        }
    });
}
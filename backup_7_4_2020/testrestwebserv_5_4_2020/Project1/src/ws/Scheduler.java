package ws;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Encoded;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

@Path("aa/scheduler")

public class Scheduler {
    public Scheduler() {
        super();
    }

    
        @POST
    @Produces("application/json")
    @Path("/getMyspace")
    public Response getMyspace(@Context SecurityContext sc ,
                              // @HeaderParam("prId") String prId ,
                               @HeaderParam("dateStart") String dateStart,
                               @HeaderParam("wsid") String wsid,
                               @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="    SELECT \"PROVIDERNAME\",\n" + 
            "            \"SCHEDULER_STAFF_ID\",\n" + 
            "            \"SCHEDULER_ID\",\n" + 
            "            \"PERSON_ID\",\n" + 
            "            \"EMPLOYEE_NAME\",\n" + 
            "            \"SCHDR_CAL_TIME\",\n" + 
            "            \"CSSCLASS\",\n" + 
            "            \"CALENDAR_ACTVITY_TITLE\",\n" + 
            "            \"CALENDAR_AVATAR\",\n" + 
            "            \"SEQ\",\n" + 
            "            \"NOTE\"\n" + 
            "       FROM (SELECT DECODE (FK_REPLACED_BY_ID,\n" + 
            "                            NULL, SCHEDULER_STAFF_ID || 'SH',\n" + 
            "                            SCHEDULER_STAFF_ID || 'SHRD')\n" + 
            "                       ProviderName,\n" + 
            "                    ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID,\n" + 
            "                    ASSCHEDULERSTAFFEO.SCHEDULER_ID,\n" + 
            "                    ASSCHEDULERSTAFFEO.PERSON_ID,\n" + 
            "                    DECODE (FK_REPLACED_BY_ID,\n" + 
            "                            NULL, NULL,\n" + 
            "                            ' Replaced By ' || REPLACEMENT.FULL_NAME)\n" + 
            "                       EMPLOYEE_NAME,\n" + 
            "                       '('\n" + 
            "                    || TO_CHAR (\n" + 
            "                          TO_DATE (\n" + 
            "                                TO_CHAR (\n" + 
            "                                   ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE,\n" + 
            "                                   'dd-mm-rrrr')\n" + 
            "                             || ' '\n" + 
            "                             || NVL (ASSCHEDULERSTAFFEO.START_TIME_HOURS,\n" + 
            "                                     SHFT.START_TIME_HOURS)\n" + 
            "                             || ':'\n" + 
            "                             || ASSCHEDULERSTAFFEO.START_TIME_MINUTES,\n" + 
            "                             'dd-mm-rrrr hh24:mi'),\n" + 
            "                          'HH24:MI')\n" + 
            "                    || ':'\n" + 
            "                    || TO_CHAR (\n" + 
            "                          TO_DATE (\n" + 
            "                                TO_CHAR (ASSCHEDULERSTAFFEO.SCHEDULAR_END_DATE,\n" + 
            "                                         'dd-mm-rrrr')\n" + 
            "                             || ' '\n" + 
            "                             || NVL (ASSCHEDULERSTAFFEO.END_TIME_HOURS,\n" + 
            "                                     SHFT.END_TIME_HOURS)\n" + 
            "                             || ':'\n" + 
            "                             || ASSCHEDULERSTAFFEO.END_TIME_MINUTES,\n" + 
            "                             'dd-mm-rrrr hh24:mi'),\n" + 
            "                          'HH24:MI')\n" + 
            "                    || ')'\n" + 
            "                       SCHDR_CAL_TIME,\n" + 
            "                    'sh' CssClass,\n" + 
            "                    SHFT.SHIFT_NAME CALENDAR_ACTVITY_TITLE,\n" + 
            "                    'SH' CALENDAR_AVATAR,\n" + 
            "                    1 Seq,\n" + 
            "                     ASSCHEDULERSTAFFEO.NOTES NOTE\n" + 
            "               FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,\n" + 
            "                    AS_SCHEDULERS ASS,\n" + 
            "                    PER_ALL_PEOPLE_F PAPF,\n" + 
            "                    PER_ALL_PEOPLE_F REPLACEMENT,\n" + 
            "                    AS_SHIFTS SHFT,\n" + 
            "                    AS_WORK_GROUPS_HDR AWGH\n" + 
            "              WHERE     ASSCHEDULERSTAFFEO.SCHEDULER_ID = ASS.SCHEDULER_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.PERSON_ID = PAPF.PERSON_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE BETWEEN PAPF.EFFECTIVE_START_DATE\n" + 
            "                                                                    AND PAPF.EFFECTIVE_END_DATE\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SHIFT_ID = SHFT.SHIFT_ID(+)\n" + 
            "                    AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID =\n" + 
            "                           AWGH.WORK_GROUP_ID(+)\n" + 
            "                    AND ASS.SCHEDULAR_STATUS = 'PUBLISHED'\n" + 
            "                    AND ASSCHEDULERSTAFFEO.FK_REPLACED_BY_ID =\n" + 
            "                           REPLACEMENT.PERSON_ID(+)\n" + 
            "                    AND (   (ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE BETWEEN REPLACEMENT.EFFECTIVE_START_DATE\n" + 
            "                                                                         AND REPLACEMENT.EFFECTIVE_END_DATE)\n" + 
            "                         OR REPLACEMENT.EFFECTIVE_START_DATE IS NULL\n" + 
            "                         OR REPLACEMENT.EFFECTIVE_END_DATE IS NULL)\n" + 
            "         AND        TO_DATE (? , 'YYYY-MM-DD')=ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE\n" + 
            "                 and ASSCHEDULERSTAFFEO.PERSON_ID = mbc_mobile_pkg.get_session_data('PERSON_ID')\n" + 
            "             UNION ALL\n" + 
            "             SELECT paa.ABSENCE_ATTENDANCE_ID || 'L' ProviderName,\n" + 
            "                    0 SCHEDULER_STAFF_ID,\n" + 
            "                    0 SCHEDULER_ID,\n" + 
            "                    papf.person_id,\n" + 
            "                    NULL,\n" + 
            "                       '('\n" + 
            "                    || TO_CHAR (\n" + 
            "                          TO_DATE (\n" + 
            "                                TO_CHAR (paa.date_start, 'dd-mm-rrrr')\n" + 
            "                             || ' '\n" + 
            "                             || paa.time_start,\n" + 
            "                             'dd-mm-rrrr '),\n" + 
            "                          'dd-Mon-yyyy')\n" + 
            "                    || '/'\n" + 
            "                    || TO_CHAR (\n" + 
            "                          TO_DATE (\n" + 
            "                                TO_CHAR (paa.date_end, 'dd-mm-rrrr')\n" + 
            "                             || ' '\n" + 
            "                             || paa.time_end,\n" + 
            "                             'dd-mm-rrrr '),\n" + 
            "                          'dd-Mon-yyyy')\n" + 
            "                    || ')'\n" + 
            "                       SCHDR_CAL_TIME,\n" + 
            "                    'lv' CssClass,\n" + 
            "                    absence_type.Name CALENDAR_ACTVITY_TITLE,\n" + 
            "                    'LV' CALENDAR_AVATAR,\n" + 
            "                    3 Seq,\n" + 
            "                    '' NOTE\n" + 
            "               FROM per_absence_attendances_v paa,\n" + 
            "                    per_all_people_f papf,\n" + 
            "                    (SELECT paat.absence_attendance_type_id, paattl.NaMe\n" + 
            "                       FROM per_absence_attendance_types paat,\n" + 
            "                            per_abs_attendance_types_tl paattl\n" + 
            "                      WHERE     paat.BUSINESS_GROUP_ID = 81\n" + 
            "                            AND (   paat.date_end IS NULL\n" + 
            "                                 OR paat.date_end > SYSDATE)\n" + 
            "                            AND (paat.date_effective < SYSDATE)\n" + 
            "                            AND paat.absence_attendance_type_id =\n" + 
            "                                   paattl.absence_attendance_type_id\n" + 
            "                            AND paattl.language = USERENV ('LANG')\n" + 
            "                            AND paat.absence_category IS NOT NULL) absence_type\n" + 
            "              WHERE     papf.person_id = paa.person_id\n" + 
            "                    AND absence_type.absence_attendance_type_id =\n" + 
            "                           paa.absence_attendance_type_id\n" + 
            "                    AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE\n" + 
            "                                    AND PAPF.EFFECTIVE_END_DATE\n" + 
            "                    AND paa.BUSINESS_GROUP_ID = 81\n" + 
            "                    AND paa.ABSENCE_ATTENDANCE_TYPE_ID != 66\n" + 
            "                AND ( TO_DATE (? , 'YYYY-MM-DD') BETWEEN paa.date_start AND paa.date_end)\n" + 
            "                 and papf.PERSON_ID = mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "          \n" + 
            "             UNION ALL\n" + 
            "             SELECT DECODE (FK_REPLACED_BY_ID,\n" + 
            "                            NULL, sbt.SCHEDULER_STAFF_BULLETIN_ID || 'BT',\n" + 
            "                            sbt.SCHEDULER_STAFF_BULLETIN_ID || 'BTRD')\n" + 
            "                       ProviderName,\n" + 
            "                    ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID,\n" + 
            "                    ASSCHEDULERSTAFFEO.SCHEDULER_ID,\n" + 
            "                    ASSCHEDULERSTAFFEO.PERSON_ID,\n" + 
            "                    DECODE (FK_REPLACED_BY_ID,\n" + 
            "                            NULL, NULL,\n" + 
            "                            ' Replaced By ' || REPLACEMENT.FULL_NAME)\n" + 
            "                       EMPLOYEE_NAME,\n" + 
            "                       '('\n" + 
            "                    || TO_CHAR (bt.ONAIR_START_TIME, 'hh24:mi')\n" + 
            "                    || DECODE (\n" + 
            "                          NVL (TO_CHAR (bt.ONAIR_END_TIME, 'hh24:mi'), '0'),\n" + 
            "                          '0', ')',\n" + 
            "                          ':' || TO_CHAR (bt.ONAIR_END_TIME, 'hh24:mi') || ')')\n" + 
            "                       SCHDR_CAL_TIME,\n" + 
            "                    'bt' CssClass,\n" + 
            "                    Bulletin_name CALENDAR_ACTVITY_TITLE,\n" + 
            "                    'BT' CALENDAR_AVATAR,\n" + 
            "                    2 Seq,\n" + 
            "                    ASSCHEDULERSTAFFEO.NOTES NOTE\n" + 
            "               FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,\n" + 
            "                    AS_SCHEDULERS ASS,\n" + 
            "                    PER_ALL_PEOPLE_F PAPF,\n" + 
            "                    PER_ALL_PEOPLE_F REPLACEMENT,\n" + 
            "                    AS_SHIFTS SHFT,\n" + 
            "                    AS_WORK_GROUPS_HDR AWGH,\n" + 
            "                    as_scheduler_staff_bulletins sbt,\n" + 
            "                    as_bulletins bt\n" + 
            "              WHERE     ASSCHEDULERSTAFFEO.SCHEDULER_ID = ASS.SCHEDULER_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.PERSON_ID = PAPF.PERSON_ID\n" + 
            "                    AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE\n" + 
            "                                    AND PAPF.EFFECTIVE_END_DATE\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SHIFT_ID = SHFT.SHIFT_ID(+)\n" + 
            "                    AND sbt.SCHEDULER_STAFF_ID =\n" + 
            "                           ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID\n" + 
            "                    AND bt.bulletin_id = SBT.BULLETIN_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID =\n" + 
            "                           AWGH.WORK_GROUP_ID(+)\n" + 
            "                    AND ASS.SCHEDULAR_STATUS = 'PUBLISHED'\n" + 
            "                    AND ASSCHEDULERSTAFFEO.FK_REPLACED_BY_ID =\n" + 
            "                           REPLACEMENT.PERSON_ID(+)\n" + 
            "                    AND (   (ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE BETWEEN REPLACEMENT.EFFECTIVE_START_DATE\n" + 
            "                                                                         AND REPLACEMENT.EFFECTIVE_END_DATE)\n" + 
            "                         OR REPLACEMENT.EFFECTIVE_START_DATE IS NULL\n" + 
            "                         OR REPLACEMENT.EFFECTIVE_END_DATE IS NULL)\n" + 
            "                 AND        TO_DATE (? , 'YYYY-MM-DD')=ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE\n" + 
            "                 and ASSCHEDULERSTAFFEO.PERSON_ID = mbc_mobile_pkg.get_session_data('PERSON_ID')\n" + 
            "\n" + 
            "             UNION ALL\n" + 
            "             SELECT ASSIGNMENT_EMP_ID || 'ASG' ProviderName,\n" + 
            "                    0 SCHEDULER_STAFF_ID,\n" + 
            "                    0 SCHEDULER_ID,\n" + 
            "                    ASEMP.PERSON_ID,\n" + 
            "                    NULL EMPLOYEE_NAME,\n" + 
            "                       '('\n" + 
            "                    || TO_CHAR (EMP_START_DATE, 'dd-mm-rrrr')\n" + 
            "                    || '/'\n" + 
            "                    || TO_CHAR (EMP_END_DATE, 'dd-mm-rrrr')\n" + 
            "                    || ')'\n" + 
            "                       SCHDR_CAL_TIME,\n" + 
            "                    'asg' CSSCLASS,\n" + 
            "                    ASSIGNMENT_NAME CALENDAR_ACTVITY_TITLE,\n" + 
            "                    'ASG' CALENDAR_AVATAR,\n" + 
            "                    4 Seq,\n" + 
            "                    '' NOTE\n" + 
            "               FROM AS_ASSIGNMENT_EMPS ASEMP,\n" + 
            "                    AS_ASSIGNMENTS ASSG,\n" + 
            "                    PER_ALL_PEOPLE_F PAPF\n" + 
            "              WHERE     ASEMP.ASSIGNMENT_ID = ASSG.ASSIGNMENT_ID\n" + 
            "                    AND ASEMP.PERSON_ID = PAPF.PERSON_ID\n" + 
            "                    AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE\n" + 
            "                                    AND PAPF.EFFECTIVE_END_DATE\n" + 
            "                          AND (  TO_DATE (? , 'YYYY-MM-DD') BETWEEN ASEMP.emp_start_date\n" + 
            "                 AND ASEMP.emp_end_date)\n" + 
            "                     and ASEMP.PERSON_ID = mbc_mobile_pkg.get_session_data('PERSON_ID')\n" + 
            "             UNION ALL\n" + 
            "             SELECT SCHEDULER_STAFF_ID || 'SHRG' ProviderName,\n" + 
            "                    ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID,\n" + 
            "                    ASSCHEDULERSTAFFEO.SCHEDULER_ID,\n" + 
            "                    ASSCHEDULERSTAFFEO.FK_REPLACED_BY_ID PERSON_ID,\n" + 
            "                    DECODE (FK_REPLACED_BY_ID,\n" + 
            "                            NULL, NULL,\n" + 
            "                            ' Replacing ' || REPLACING.FULL_NAME)\n" + 
            "                       EMPLOYEE_NAME,\n" + 
            "                       '('\n" + 
            "                    || TO_CHAR (\n" + 
            "                          TO_DATE (\n" + 
            "                                TO_CHAR (\n" + 
            "                                   ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE,\n" + 
            "                                   'dd-mm-rrrr')\n" + 
            "                             || ' '\n" + 
            "                             || NVL (ASSCHEDULERSTAFFEO.START_TIME_HOURS,\n" + 
            "                                     SHFT.START_TIME_HOURS)\n" + 
            "                             || ':'\n" + 
            "                             || ASSCHEDULERSTAFFEO.START_TIME_MINUTES,\n" + 
            "                             'dd-mm-rrrr hh24:mi'),\n" + 
            "                          'HH24:MI')\n" + 
            "                    || ':'\n" + 
            "                    || TO_CHAR (\n" + 
            "                          TO_DATE (\n" + 
            "                                TO_CHAR (ASSCHEDULERSTAFFEO.SCHEDULAR_END_DATE,\n" + 
            "                                         'dd-mm-rrrr')\n" + 
            "                             || ' '\n" + 
            "                             || NVL (ASSCHEDULERSTAFFEO.END_TIME_HOURS,\n" + 
            "                                     SHFT.END_TIME_HOURS)\n" + 
            "                             || ':'\n" + 
            "                             || ASSCHEDULERSTAFFEO.END_TIME_MINUTES,\n" + 
            "                             'dd-mm-rrrr hh24:mi'),\n" + 
            "                          'HH24:MI')\n" + 
            "                    || ')'\n" + 
            "                       SCHDR_CAL_TIME,\n" + 
            "                    'sh' CSSCLASS,\n" + 
            "                    SHFT.SHIFT_NAME CALENDAR_ACTVITY_TITLE,\n" + 
            "                    'SH' CALENDAR_AVATAR,\n" + 
            "                    1 Seq,\n" + 
            "                    ASSCHEDULERSTAFFEO.NOTES NOTE\n" + 
            "               FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,\n" + 
            "                    AS_SCHEDULERS ASS,\n" + 
            "                    PER_ALL_PEOPLE_F PAPF,\n" + 
            "                    PER_ALL_PEOPLE_F REPLACING,\n" + 
            "                    AS_SHIFTS SHFT,\n" + 
            "                    AS_WORK_GROUPS_HDR AWGH\n" + 
            "              WHERE     ASSCHEDULERSTAFFEO.SCHEDULER_ID = ASS.SCHEDULER_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.PERSON_ID = REPLACING.PERSON_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE BETWEEN PAPF.EFFECTIVE_START_DATE\n" + 
            "                                                                    AND PAPF.EFFECTIVE_END_DATE\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SHIFT_ID = SHFT.SHIFT_ID(+)\n" + 
            "                    AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID =\n" + 
            "                           AWGH.WORK_GROUP_ID(+)\n" + 
            "                    AND ASS.SCHEDULAR_STATUS = 'PUBLISHED'\n" + 
            "                    AND ASSCHEDULERSTAFFEO.FK_REPLACED_BY_ID = PAPF.PERSON_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE BETWEEN REPLACING.EFFECTIVE_START_DATE\n" + 
            "                                                                    AND REPLACING.EFFECTIVE_END_DATE\n" + 
            "               \n" + 
            "                    AND        TO_DATE (? , 'YYYY-MM-DD')=ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE\n" + 
            "                   and ASSCHEDULERSTAFFEO.FK_REPLACED_BY_ID= mbc_mobile_pkg.get_session_data('PERSON_ID')\n" + 
            "             UNION ALL\n" + 
            "             SELECT sbt.SCHEDULER_STAFF_BULLETIN_ID || 'BTRG' ProviderName,\n" + 
            "                    ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID,\n" + 
            "                    ASSCHEDULERSTAFFEO.SCHEDULER_ID,\n" + 
            "                    ASSCHEDULERSTAFFEO.FK_REPLACED_BY_ID PERSON_ID,\n" + 
            "                    DECODE (FK_REPLACED_BY_ID,\n" + 
            "                            NULL, NULL,\n" + 
            "                            ' Replacing ' || REPLACING.FULL_NAME)\n" + 
            "                       EMPLOYEE_NAME,\n" + 
            "                       '('\n" + 
            "                    || TO_CHAR (bt.ONAIR_START_TIME, 'hh24:mi')\n" + 
            "                    || DECODE (\n" + 
            "                          NVL (TO_CHAR (bt.ONAIR_END_TIME, 'hh24:mi'), '0'),\n" + 
            "                          '0', ')',\n" + 
            "                          ':' || TO_CHAR (bt.ONAIR_END_TIME, 'hh24:mi') || ')')\n" + 
            "                       SCHDR_CAL_TIME,\n" + 
            "                    'bt' CSSCLASS,\n" + 
            "                    Bulletin_name CALENDAR_ACTVITY_TITLE,\n" + 
            "                    'BT' CALENDAR_AVATAR,\n" + 
            "                    2 Seq,\n" + 
            "                    ASSCHEDULERSTAFFEO.NOTES NOTE\n" + 
            "               FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,\n" + 
            "                    AS_SCHEDULERS ASS,\n" + 
            "                    PER_ALL_PEOPLE_F PAPF,\n" + 
            "                    PER_ALL_PEOPLE_F REPLACING,\n" + 
            "                    AS_SHIFTS SHFT,\n" + 
            "                    AS_WORK_GROUPS_HDR AWGH,\n" + 
            "                    as_scheduler_staff_bulletins sbt,\n" + 
            "                    as_bulletins bt\n" + 
            "              WHERE     ASSCHEDULERSTAFFEO.SCHEDULER_ID = ASS.SCHEDULER_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.PERSON_ID = REPLACING.PERSON_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE BETWEEN PAPF.EFFECTIVE_START_DATE\n" + 
            "                                                                    AND PAPF.EFFECTIVE_END_DATE\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SHIFT_ID = SHFT.SHIFT_ID(+)\n" + 
            "                    AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID =\n" + 
            "                           AWGH.WORK_GROUP_ID(+)\n" + 
            "                    AND ASS.SCHEDULAR_STATUS = 'PUBLISHED'\n" + 
            "                    AND ASSCHEDULERSTAFFEO.FK_REPLACED_BY_ID = PAPF.PERSON_ID\n" + 
            "                    AND ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE BETWEEN REPLACING.EFFECTIVE_START_DATE\n" + 
            "                                                                    AND REPLACING.EFFECTIVE_END_DATE\n" + 
            "                    AND sbt.SCHEDULER_STAFF_ID =\n" + 
            "                           ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID\n" + 
            "                    AND bt.bulletin_id = SBT.BULLETIN_ID\n" + 
            "                \n" + 
            "                        AND        TO_DATE (? , 'YYYY-MM-DD')=ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE\n" + 
            "                 and ASSCHEDULERSTAFFEO.FK_REPLACED_BY_ID = mbc_mobile_pkg.get_session_data('PERSON_ID')\n" + 
            "                 \n" + 
            "                 \n" + 
            "                           )\n" + 
            "   ORDER BY Seq"; 
           
            try {
     
                connection = Helper.getOracleConnection();
               
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getMyspace");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "Open AA My Space", "Open AA My Space" );
                
            statement = connection.prepareStatement(sqlStemt);
            statement.setString(1, dateStart);
            //statement.setString(2, prId);
            statement.setString(2, dateStart);
            //statement.setString(4, prId);
            statement.setString(3, dateStart);
           // statement.setString(6, prId);
            statement.setString(4, dateStart);
            //statement.setString(8, prId);
            statement.setString(5, dateStart);
            //statement.setString(10, prId);
            statement.setString(6, dateStart);
            //statement.setString(12, prId);
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
//    @POST
//    @Produces("application/json")
//    @Path("/getMyspaceCurrent")
        
        // old one
  
//    public Response getMyspaceCurrent(@Context SecurityContext sc ,
//                                     // @HeaderParam("prId") String prId,
//                                      @HeaderParam("wsid") String wsid,
//                                      @HeaderParam("signature") String signature)
//    {
//    
//            if (!sc.isUserInRole("Authenticated"))  
//            throw new SecurityException("User is unauthorized.");
//            
//            if (wsid==null || signature==null )  
//            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
//            
//            wsid=URLDecoder.decode(wsid);
//            signature=URLDecoder.decode(signature);
//            
//        Connection connection = null;
//        PreparedStatement statement = null;
//        Response response = null;
//        ResultSet rs=null;
//        String sqlStemt="select * from XXMBC_AA_MYSPACE where person_id=mbc_mobile_pkg.get_session_data('PERSON_ID')"; 
//       
//        try {
//    
//            connection = Helper.getOracleConnection();
//            String result=Helper.checkSession(connection, wsid, signature , wsid+"getMyspaceCurrent");
//            if (!"OK".equals(result))
//            throw new SecurityException("Unauthorized Access."); 
//            
//            Helper.logActivity(connection, "Open AA My Space", "Open AA My Space" );
//            
//           
//        statement = connection.prepareStatement(sqlStemt);
//           
//           
//
//           
//         rs = statement.executeQuery();
//      
//           // Map<String, Object> m = new HashMap<String, Object>();
//           List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
//            m = ConvertRStoMap.getEntitiesFromResultSet(rs);
//                      
//            System.out.println("size is "+m.size());
//            StringWriter outPut = new StringWriter();
//            ObjectMapper map = new ObjectMapper();
//            map.writeValue(outPut, m);
//            ResponseBuilder builder = Response.ok(outPut.toString());
//            response = builder.build();
//            if (m.size() == 0) {
//             
//                
//            }
//
//        }
//        catch(SecurityException s)
//        {
//            s.printStackTrace();
//            response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
//            }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//                response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
//            }
//        finally {
//                    try {
//                      
//                        if ((connection != null) && !(connection.isClosed()))
//                            connection.close();
//                        if ((statement != null) && !(statement.isClosed()))
//                                      statement.close();
//                        if ((rs != null) && !(rs.isClosed()))
//                        rs.close();
//                    } catch (SQLException sqle) {
//                        // TODO: Add catch code
//                        sqle.printStackTrace();
//                    }
//                }
//                return response;
//    }

    @POST
    @Produces("application/json")
    @Path("/getMyteam")
    public Response getMyteam(@Context SecurityContext sc ,
                            //  @HeaderParam("prId") String prId,
                              @HeaderParam("orgId") String orgId ,
                              @HeaderParam("dateStart") String dateStart,
                              @HeaderParam("wsid") String wsid,
                              @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select    \n" + 
            " emplist.person_id ,emplist.Full_name ,   \n" + 
            "  (select mbc_mobile_pkg.BASE64ENCODE(  image )  from  per_images where parent_id= emplist.person_id) img,emplist.person_id,\n" + 
            "AS_SCHDLR_PKG.PUBLICSPACESHIFT1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),null,null,?) Shift ,   \n" + 
            "                                  \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACEBULLETIN1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),null,null,?) Bulletins ,   \n" + 
            "                                 \n" + 
            " AS_SCHDLR_PKG.PUBLICSPACEASSIGNMENTS(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'))Assginments   \n" + 
            "                       \n" + 
            " ,AS_SCHDLR_PKG.PUBLICSPACELEAVES(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD')) Leaves ,   \n" + 
            "   \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACENOTES1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),null,null,?) Notes ,  \n" + 
            "\n" + 
            "                   \n" + 
            " AS_SCHDLR_PKG.PUBLICSPACEREPLACEMENT1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),null,null,?) ReplacedBy ,   \n" + 
            "  \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACEREPLACEMENTDATA(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),?,null) Relacing \n" + 
            "\n" + 
            "   \n" + 
            "from    \n" + 
            "  \n" + 
            "   \n" + 
            "   \n" + 
            "(select  distinct    papf.PERSON_ID,papf.FULL_NAME   , ? datestart from    \n" + 
            "PER_ALL_PEOPLE_F PAPF ,AS_WORK_GROUPS_HDR wk  ,AS_WORK_GROUPS_LINES wkl       \n" + 
            " where  wk.work_group_id=wkl.work_group_id    and wkl.person_id=papf.person_id         \n" + 
            "  AND TO_DATE (? , 'YYYY-MM-DD') BETWEEN PAPF.EFFECTIVE_START_DATE    AND PAPF.EFFECTIVE_END_DATE    \n" + 
            "  AND ( wkl.work_group_id in (select work_group_id from as_work_groups_lines wgl where person_id=mbc_mobile_pkg.get_session_data('PERSON_ID') )  )\n" + 
            "                                         \n" + 
            " AND (wk.fk_organization_id=? )      ) emplist    \n" + 
            " WHERE PERSON_ID!= mbc_mobile_pkg.get_session_data('PERSON_ID')\n" + 
            "    \n" + 
            " order by decode (Shift , null , 0 , 1) desc, Full_name\n";
      
           
            try {
     
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getMyteam");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "Open AA My Team", "Open AA My Team" );
                
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, orgId);
                statement.setString(2, orgId);
                statement.setString(3, orgId);   
                statement.setString(4, orgId);
                statement.setString(5, orgId);
                statement.setString(6, dateStart);
                statement.setString(7, dateStart);
                //statement.setString(8, prId);
                statement.setString(8, orgId);
                //statement.setString(10, prId);

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    @POST
    @Produces("application/json")
    @Path("/getMyWk")
    public Response getMyWk(@Context SecurityContext sc ,
                            //  @HeaderParam("prId") String prId,
                              @HeaderParam("orgId") String orgId ,
                           @HeaderParam("wsid") String wsid,
                              @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select WORK_GROUP_ID ,WORK_GROUP_NAME,FK_ORGANIZATION_ID,FK_WK_CATEGORY_ID from as_work_groups_hdr\n" + 
            "where FK_ORGANIZATION_ID=?";
      
           
            try {
     
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getMyWk");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access wokgroup."); 
                
                Helper.logActivity(connection, "Open AA Workgroups", "Open AA workgroups" );
                
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, orgId);
              

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    @POST
    @Produces("application/json")
    @Path("/getOrg")
    public Response getOrg(@Context SecurityContext sc ,
                         
                           @HeaderParam("wsid") String wsid,
                              @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select  person_id ,role_name ,organization_name,organization_id ,decode(role_name,'ADMIN','Administrator','MGR','Manager','SYSADMIN','SYS Administrator','GD','Group Director','ED','Editor','OGADMIN','Org Adminstrator','EMP','Employee','ADMIN-EMP','Administrator-Enmployee','MGR-EMP','Manager-Employee','SYSADMIN-EMP','SYS Administrator-Employee','GD-EMP','Group Director-Employee','ED-EMP','Editor-Employee','OGADMIN-EMP','Org Adminstrator-Employee','Undefined') ||'-'||organization_name as  \"OrgRole\"     \n" + 
            " from as_user_roles ur ,as_organization org     \n" + 
            " where  UR.FK_ORGANIZATION_ID=org.organization_id(+) \n" + 
            " and organization_id is not null \n" + 
            " and person_id= mbc_mobile_pkg.get_session_data('PERSON_ID') ";
      
           
            try {
     
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getOrg");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access Organization."); 
                
                Helper.logActivity(connection, "Open AA Organizations", "Open AA Organizations" );
                
            statement = connection.prepareStatement(sqlStemt);
               // statement.setString(1, orgId);
              

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    
    @POST
    @Produces("application/json")
    @Path("/getMyteamWk")
    public Response getMyteamWk(@Context SecurityContext sc ,
                              @HeaderParam("wkId") String wkId,
                              @HeaderParam("orgId") String orgId ,
                              @HeaderParam("dateStart") String dateStart,
                              @HeaderParam("wsid") String wsid,
                              @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select    \n" + 
            " emplist.person_id ,emplist.Full_name ,   \n" + 
            "  (select mbc_mobile_pkg.BASE64ENCODE(  image )  from  per_images where parent_id= emplist.person_id) img,emplist.person_id,\n" + 
            "AS_SCHDLR_PKG.PUBLICSPACESHIFT1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),emplist.work_group_id ,null,?) Shift ,   \n" + 
            "                                  \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACEBULLETIN1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),emplist.work_group_id,null,?) Bulletins ,   \n" + 
            "                                 \n" + 
            " AS_SCHDLR_PKG.PUBLICSPACEASSIGNMENTS(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'))Assginments   \n" + 
            "                       \n" + 
            " ,AS_SCHDLR_PKG.PUBLICSPACELEAVES(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD')) Leaves ,   \n" + 
            "   \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACENOTES1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),emplist.work_group_id,null,?) Notes ,  \n" + 
            "\n" + 
            "                   \n" + 
            " AS_SCHDLR_PKG.PUBLICSPACEREPLACEMENT1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),emplist.work_group_id,null,?) ReplacedBy ,   \n" + 
            "  \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACEREPLACEMENTDATA(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),?,emplist.work_group_id ) Relacing \n" + 
            "\n" + 
            "   \n" + 
            "from    \n" + 
            "  \n" + 
            "   \n" + 
            "   \n" + 
            "(select  distinct    papf.PERSON_ID,papf.FULL_NAME ,wk.work_group_id  , ? datestart from    \n" + 
            "PER_ALL_PEOPLE_F PAPF ,AS_WORK_GROUPS_HDR wk  ,AS_WORK_GROUPS_LINES wkl       \n" + 
            " where  wk.work_group_id=wkl.work_group_id    and wkl.person_id=papf.person_id         \n" + 
            "  AND TO_DATE (? , 'YYYY-MM-DD') BETWEEN PAPF.EFFECTIVE_START_DATE    AND PAPF.EFFECTIVE_END_DATE    \n" + 
            "  AND wkl.work_group_id =?\n" + 
            "                                         \n" + 
            " AND (wk.fk_organization_id=?)      ) emplist    \n" + 
            "\n" + 
            "    \n" + 
            " order by decode (Shift , null , 0 , 1) desc, Full_name";
      
           
            try {
     
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getMyteamWk");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "Open AA My Team by Workgroup", "Open AA My Team by Workgroup" );
                
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, orgId);
                statement.setString(2, orgId);
                statement.setString(3, orgId);   
                statement.setString(4, orgId);
                statement.setString(5, orgId);
                statement.setString(6, dateStart);
                statement.setString(7, dateStart);
                statement.setString(8, wkId);
                statement.setString(9, orgId);
                //statement.setString(10, prId);

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    
 
    
    
}

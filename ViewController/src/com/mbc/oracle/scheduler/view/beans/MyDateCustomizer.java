package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.model.am.SchedulerAMImpl;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.time.Year;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import oracle.adf.view.rich.util.DateCustomizer;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Number;

public class MyDateCustomizer extends DateCustomizer {
    public MyDateCustomizer() {
        super();
    }

    @Override
    public String format(Date date, String string, Locale locale, TimeZone timeZone) {
        // TODO Implement this method
        int result=0;
        
        OperationBinding oper = ADFUtils.findOperation("getStartEndScheduleDates");
        oper.execute();
        HashMap SchedulerDuration =(HashMap)oper.getResult();
        oracle.jbo.domain.Date schStartdate=(oracle.jbo.domain.Date)SchedulerDuration.get("SchedulerStartDate");
        oracle.jbo.domain.Date schEnddate=(oracle.jbo.domain.Date)SchedulerDuration.get("SchedulerEndDate");
        if(this.addDays(date,1).after(new Date(schStartdate.dateValue().getTime()))&& this.addDays(date,-1).before(new Date(schEnddate.dateValue().getTime())))
        {
                DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                 String StartDate=df.format(date);
                 OperationBinding oper1 = ADFUtils.findOperation("validateSchedulerDay");
                oper1.getParamsMap().put("p_Date",StartDate);
                oper1.execute();
                result=  Integer.valueOf(oper1.getResult().toString());
                String HeaderMessage=String.valueOf(result)+" " +"Shifts invalid";
                if("af|calendar::month-grid-cell-header-misc".equals(string))

                {
                  if(result>0)
                  {
                 return HeaderMessage;
                  }
                     else 
                  return null;
                // holidays.get(date)!=null?holidays.get(date).toString():null;
                // return date.toString();

                } 
          
          
            }
      
     
        return null;
        


        
     
 
      
      
       
    }
    public java.util.Date addDays(java.util.Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
    
}

$(function () {
    var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
    
    //-------------------------------
    // Tag-it methods
    //-------------------------------
    var addEvent = function(text) {
                $('#events_container').append(text + '<br>');
    };
    
       
    var calFilterTags =$('#CalFilterTags');
    calFilterTags.tagit( {
        availableTags : sampleTags, readOnly : false, placeholderText : 'Filter Employees', afterTagRemoved : function (evt, ui) {
            //addEvent('afterTagRemoved: ' + calFilterTags.tagit('tagLabel', ui.tag));
            var employeeName=calFilterTags.tagit('tagLabel', ui.tag);
            var proxyButton = AdfPage.PAGE.findComponentByAbsoluteId('pt1:cBodFDC:r1:1:TagRemoveBtn');
            var parameter = {EmployeeNamePar:employeeName};
            //addEvent('proxyButton='+proxyButton);
            //addEvent('parameter='+parameter);
            AdfCustomEvent.queue(proxyButton ,"RemoveTagListener",parameter, true);
            event.cancel(); 
        }
    });

});
package com.mbc.oracle.scheduler.model.vo.lov;

import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jul 04 15:32:10 GST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ShiftsLovRowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ShiftId,
        ShiftName;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int SHIFTID = AttributesEnum.ShiftId.index();
    public static final int SHIFTNAME = AttributesEnum.ShiftName.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ShiftsLovRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute ShiftId.
     * @return the ShiftId
     */
    public Number getShiftId() {
        return (Number) getAttributeInternal(SHIFTID);
    }

    /**
     * Gets the attribute value for the calculated attribute ShiftName.
     * @return the ShiftName
     */
    public String getShiftName() {
        return (String) getAttributeInternal(SHIFTNAME);
    }


}


package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import com.mbc.oracle.scheduler.view.util.JSFUtils;

import com.mbc.oracle.scheduler.view.util.UserHelper;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import java.util.Map;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;

public class LoginBean {
    private RichOutputText invalidMsg;
    private String userName;
    private String password;

    public LoginBean() {
    }

    public void setInvalidMsg(RichOutputText invalidMsg) {
        this.invalidMsg = invalidMsg;
    }

    public RichOutputText getInvalidMsg() {
        return invalidMsg;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    @SuppressWarnings("oracle.jdeveloper.java.unchecked-conversion-or-cast")
    public String onClcikLoginBtn() {
        // Add event code here...
        OperationBinding oper = ADFUtils.findOperation("authenticateUser");
        oper.getParamsMap().put("userName", this.getUserName().toUpperCase());
        oper.getParamsMap().put("password", this.getPassword());
        String isAuthenticatedStr = (String) oper.execute();
        if ("Y".equals(isAuthenticatedStr)) {
           if (this.initSessionData().equals("S"))
           {
              // Map sessionscope = ADFContext.getCurrent().getSessionScope();
            //  int  countorgorle = Integer.valueOf(sessionscope.get("AANumofOrg_Role").toString());
           // if( Integer.valueOf(sessionscope.get("AANumofOrg_Role").toString())>1) 
               //  return "";
                 //    else 
            return "index";
           }
           
           
           
           else if (this.initSessionData().equals("M"))
           {
               
                   this.invalidMsg.setValue("");
               return "multiaccess";
               }
            else if (this.initSessionData().equals("ND"))
            {
                
                
                    this.invalidMsg.setValue("Error In  Processing Organization-Role  ,Please Contact System Adminstrator.");
                }
           else if (this.initSessionData().equals("N"))
           {
            this.invalidMsg.setValue("You are not Authorized to Access Al Arabiya Scheduler ,Please Contact System Adminstrator.");
           }
        }
        else {
            this.invalidMsg.setValue("You have specified an invalid User ID or Password. Please check and try again.");
        }

        return null;
    }
 //Get Logged in Employee Data
    private String initSessionData() {
        OperationBinding oper = ADFUtils.findOperation("initLogonEmployee");
        oper.getParamsMap().put("userName", this.getUserName().toUpperCase());
        oper.execute();
        HashMap employeeDataHashMap = (HashMap) oper.getResult();
     if( Integer.valueOf(employeeDataHashMap.get("ValidUser").toString())==0)
     {
         /// means rows not exist ,not Authorized//
         return "N" ;
         
         }
     else
     {
         
         if( Integer.valueOf(employeeDataHashMap.get("NoOfOrgRole").toString())==1)
         {
        Number personId = (Number) employeeDataHashMap.get("PersonId");
        String employeeNumber = (String) employeeDataHashMap.get("EmployeeNumber");
        String employeeName = (String) employeeDataHashMap.get("FullName");
        String directorateName = (String) employeeDataHashMap.get("DirectorateName");
        Number organizationId = (Number) employeeDataHashMap.get("OrganizationId");
        String organizationName = (String) employeeDataHashMap.get("OrganizationName");
         Number mgrPersonId = (Number) employeeDataHashMap.get("MgrPersonId");
        String mgrFullName = (String) employeeDataHashMap.get("MgrFullName");
        Number mgrOrganizationId = (Number) employeeDataHashMap.get("MgrOrganizationId");
        String roleName = (String) employeeDataHashMap.get("RoleName");
        String AOrgName=(String) employeeDataHashMap.get("AorgName");
        int NumofOrg_Role=Integer.valueOf(employeeDataHashMap.get("NoOfOrgRole").toString());
        if(AOrgName==null)
        {
      AOrgName = "";
        
        }
        
        else 
        {
                AOrgName = "( ".concat((String) employeeDataHashMap.get("AorgName")).concat(" )"); 
            }
        
        Number AOrgId=(Number) employeeDataHashMap.get("AorgId");
        Number AUserId=(Number) employeeDataHashMap.get("UserId");
        String AUsername=(String) employeeDataHashMap.get("UserName");
        UserHelper userHelper = new UserHelper();
        userHelper.setUserName(this.getUserName().toUpperCase());
        userHelper.setPersonId(personId);
        userHelper.setEmployeeNumber(employeeNumber);
        userHelper.setEmployeeName(employeeName);
        userHelper.setOrganizationId(organizationId);
        userHelper.setOrganizationName(organizationName);
        userHelper.setDirectorateName(directorateName);
        userHelper.setMgrPersonId(mgrPersonId);
        userHelper.setMgrName(mgrFullName);
        userHelper.setMgrOrganizationId(mgrOrganizationId);

        
        oper=ADFUtils.findOperation("getWorklistCount");
        oper.getParamsMap().put("personId", personId);
        HashMap ntfMap = (HashMap) oper.execute();
        userHelper.setApprovedNtfCount((Number) ntfMap.get("APPROVED"));
        userHelper.setRejectedNtfCount((Number) ntfMap.get("REJECTED"));
        userHelper.setNotificationCount((Number) ntfMap.get("OPEN"));
     
        JSFUtils.storeOnSession("userHelper", userHelper);
        JSFUtils.storeOnSession("AAOrganization", AOrgId);
         JSFUtils.storeOnSession("AAOrganizationName", AOrgName);
         JSFUtils.storeOnSession("AARoleName", roleName);
        JSFUtils.storeOnSession("roleName", roleName);
        JSFUtils.storeOnSession("AAPersonId", personId);
         // Added 27-August to set created by in tables pattern,shifts and bulletins
         JSFUtils.storeOnSession("AAUserId",AUserId);
         JSFUtils.storeOnSession("AAUsername",AUsername);
         // Added 8-july-2019 to set employee name in public space on login
         JSFUtils.storeOnSession("AAEmpName", employeeName);
         JSFUtils.storeOnSession("AANumofOrg_Role",NumofOrg_Role);
      
        ViewObject asWorklistVO = ADFUtils.findIterator("AsWorklistVOIterator").getViewObject();
        asWorklistVO.setNamedWhereClauseParam("PersonIdPar", personId);
   
   
   return "S" ;
         }
         else if( Integer.valueOf(employeeDataHashMap.get("NoOfOrgRole").toString())>1)
         {
                 Number personId = (Number) employeeDataHashMap.get("PersonId");
                 String employeeNumber = (String) employeeDataHashMap.get("EmployeeNumber");
                 String employeeName = (String) employeeDataHashMap.get("FullName");
                 String directorateName = (String) employeeDataHashMap.get("DirectorateName");
                 Number organizationId = (Number) employeeDataHashMap.get("OrganizationId");
                 String organizationName = (String) employeeDataHashMap.get("OrganizationName");
               
                 Number mgrPersonId = (Number) employeeDataHashMap.get("MgrPersonId");
                 String mgrFullName = (String) employeeDataHashMap.get("MgrFullName");
                 Number mgrOrganizationId = (Number) employeeDataHashMap.get("MgrOrganizationId");
           
                  int NumofOrg_Role=Integer.valueOf(employeeDataHashMap.get("NoOfOrgRole").toString());
             
                 Number AUserId=(Number) employeeDataHashMap.get("UserId");
                 String AUsername=(String) employeeDataHashMap.get("UserName");
                 UserHelper userHelper = new UserHelper();
                 userHelper.setUserName(this.getUserName().toUpperCase());
                 userHelper.setPersonId(personId);
                 userHelper.setEmployeeNumber(employeeNumber);
                 userHelper.setEmployeeName(employeeName);
                 userHelper.setOrganizationId(organizationId);
                 userHelper.setOrganizationName(organizationName);
                 userHelper.setDirectorateName(directorateName);
                 userHelper.setMgrPersonId(mgrPersonId);
                 userHelper.setMgrName(mgrFullName);
                 userHelper.setMgrOrganizationId(mgrOrganizationId);
             
                 
                 oper=ADFUtils.findOperation("getWorklistCount");
                 oper.getParamsMap().put("personId", personId);
                 HashMap ntfMap = (HashMap) oper.execute();
                 userHelper.setApprovedNtfCount((Number) ntfMap.get("APPROVED"));
                 userHelper.setRejectedNtfCount((Number) ntfMap.get("REJECTED"));
                 userHelper.setNotificationCount((Number) ntfMap.get("OPEN"));
                 
                 JSFUtils.storeOnSession("userHelper", userHelper);
               
                 JSFUtils.storeOnSession("AAPersonId", personId);
                  // Added 27-August to set created by in tables pattern,shifts and bulletins
                  JSFUtils.storeOnSession("AAUserId",AUserId);
                  JSFUtils.storeOnSession("AAUsername",AUsername);
                  // Added 8-july-2019 to set employee name in public space on login
                  JSFUtils.storeOnSession("AAEmpName", employeeName);
                  JSFUtils.storeOnSession("AANumofOrg_Role",NumofOrg_Role);
               
                 ViewObject asWorklistVO = ADFUtils.findIterator("AsWorklistVOIterator").getViewObject();
                 asWorklistVO.setNamedWhereClauseParam("PersonIdPar", personId);
                  
             return "M";
             }
         else 
             // Means Not Definied
         return "ND";
     }// end else 
    }
    // added by Heba 12-july 2017 //to get end date with strat day setted to today date and get end date +30
    public  Date addDays(Date date, int days)
      {
          Calendar cal = Calendar.getInstance();
          cal.setTime(date);
          cal.add(Calendar.DATE, days); //minus number would decrement the days
         // cal.
          return cal.getTime();
      }

    public String onClickProcess() {
        // Add event code here...
        Number AOrgId=(Number) ADFUtils.getBoundAttributeValue("OrganizationId"); 
     String roleName = (String) ADFUtils.getBoundAttributeValue("RoleName");
     String AOrgName=(String) ADFUtils.getBoundAttributeValue("OrganizationName");
        
         if(AOrgName==null)
         {
         AOrgName = "";
         
         }
         
         else 
         {
                 AOrgName = "( ".concat((String) ADFUtils.getBoundAttributeValue("OrganizationName")).concat(" )"); 
             }
         
          JSFUtils.storeOnSession("AAOrganization", AOrgId);
          JSFUtils.storeOnSession("AAOrganizationName", AOrgName);
          JSFUtils.storeOnSession("AARoleName", roleName);
          JSFUtils.storeOnSession("roleName", roleName);
        return "index";
    }
}

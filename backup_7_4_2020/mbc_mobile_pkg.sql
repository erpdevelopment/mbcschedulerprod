CREATE OR REPLACE PACKAGE BODY APPS.MBC_MOBILE_PKG
AS
   PROCEDURE INITIALIZE_SESSION (P_USER_NAME IN VARCHAR2)
   IS
      V_RESP_ID   NUMBER;
      V_APP_ID    NUMBER;
      V_USER_ID    NUMBER;
   BEGIN
      SELECT DECODE (BUSINESS_GROUP_ID,
                     169, 50842                                        --Egypt
                               ,
                     205, 52007                                      --Lebanon
                               ,
                     638, 51806                                       --Jordan
                               ,
                     998, 53164                                        --Saudi
                               ,
                     81, 50647                                         --Dubai
                              ,
                     50647),                                --Other take Dubai
             800,FU.USER_ID
        INTO V_RESP_ID, V_APP_ID,V_USER_ID
        FROM FND_USER FU, PER_ALL_PEOPLE_F PAPF
       WHERE     FU.EMPLOYEE_ID = PAPF.PERSON_ID
             AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                             AND PAPF.EFFECTIVE_END_DATE
             AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
             AND FU.USER_NAME = P_USER_NAME;

      FND_GLOBAL.APPS_INITIALIZE (USER_ID        => V_USER_ID,
                                  RESP_ID        => V_RESP_ID,
                                  RESP_APPL_ID   => V_APP_ID);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;
   
      PROCEDURE INITIALIZE_SESSION (P_USER_ID IN NUMBER)
   IS
      V_RESP_ID   NUMBER;
      V_APP_ID    NUMBER;
   BEGIN
      SELECT DECODE (BUSINESS_GROUP_ID,
                     169, 50842                                        --Egypt
                               ,
                     205, 52007                                      --Lebanon
                               ,
                     638, 51806                                       --Jordan
                               ,
                     998, 53164                                        --Saudi
                               ,
                     81, 50647                                         --Dubai
                              ,
                     50647),                                --Other take Dubai
             800
        INTO V_RESP_ID, V_APP_ID
        FROM FND_USER FU, PER_ALL_PEOPLE_F PAPF
       WHERE     FU.EMPLOYEE_ID = PAPF.PERSON_ID
             AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                             AND PAPF.EFFECTIVE_END_DATE
             AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
             AND FU.USER_ID = P_USER_ID;

      FND_GLOBAL.APPS_INITIALIZE (USER_ID        => P_USER_ID,
                                  RESP_ID        => V_RESP_ID,
                                  RESP_APPL_ID   => V_APP_ID);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;
   

   PROCEDURE INITIALIZE_SESSION (P_USER_ID   IN NUMBER,
                                 P_RESP_ID   IN NUMBER,
                                 P_APP_ID    IN NUMBER)
   IS
   BEGIN
      FND_GLOBAL.APPS_INITIALIZE (USER_ID        => P_USER_ID,
                                  RESP_ID        => P_RESP_ID,
                                  RESP_APPL_ID   => P_APP_ID);
   END;

   PROCEDURE INITIALIZE_LEAVE (P_TRANSACTION_ID          OUT NUMBER,
                               P_TRANSACTION_STEP_ID     OUT NUMBER,
                               P_ITEM_KEY                OUT NUMBER,
                               P_ABSENCE_ATTENDANCE_ID   OUT NUMBER)
   IS
   BEGIN
      P_TRANSACTION_ID := HR_API_TRANSACTIONS_S.NEXTVAL;
      P_TRANSACTION_STEP_ID := HR_API_TRANSACTION_STEPS_S.NEXTVAL;
      P_ITEM_KEY := HR_WORKFLOW_ITEM_KEY_S.NEXTVAL;
      P_ABSENCE_ATTENDANCE_ID := PER_ABSENCE_ATTENDANCES_S.NEXTVAL;
   END;
 
    PROCEDURE LOG_MOBILE_ACTIVITY(
                                P_APPLICATION_ID        IN  NUMBER,
                                P_PERSON_ID             IN  NUMBER,
                                P_USER_ID               IN  NUMBER,
                                P_ACTIVITY_NAME         IN  VARCHAR2,
                                P_LOG_TYPE              IN  VARCHAR2,
                                P_DESCRIPTION           IN  VARCHAR2,
                                P_USERNAME              IN  VARCHAR2 := NULL
                               ) IS
        V_USER_ID NUMBER;
        V_PERSON_ID NUMBER;
    BEGIN
    
     if P_PERSON_ID = 0 and P_USER_ID=0 then 
    
        select employee_id person_id, user_id
        INTO V_PERSON_ID, V_USER_ID
        from fnd_user
        where user_name = upper(P_USERNAME);

     elsif P_PERSON_ID = 0 and P_USER_ID!=0 then 
    
        select employee_id person_id, user_id
        INTO V_PERSON_ID, V_USER_ID
        from fnd_user
        where user_id = upper(P_USER_ID);
     
     elsif P_PERSON_ID != 0 and P_USER_ID=0 then 
    
        select employee_id person_id, user_id
        INTO V_PERSON_ID, V_USER_ID
        from fnd_user
        where employee_id = upper(P_PERSON_ID);
        
     elsif P_PERSON_ID != 0 and P_USER_ID !=0 then
        V_USER_ID := P_USER_ID;
        V_PERSON_ID := P_PERSON_ID;
     end if;
    
        INSERT INTO MBC_MOBILE_ACTIVITY (ID,APPLICATION_ID,PERSON_ID,USER_ID,ACTIVITY_NAME,CREATED_BY,CREATION_DATE,LOG_TYPE,DESCRIPTION)  
        VALUES (MBC_MOBILE_ACTIVITY_SEQ.NEXTVAL,1,V_PERSON_ID,V_USER_ID,P_ACTIVITY_NAME ,V_USER_ID,SYSDATE,P_LOG_TYPE,P_DESCRIPTION );
        
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
        NULL;
    END LOG_MOBILE_ACTIVITY ;       
    
    
   FUNCTION LOGIN (P_USER_NAME             IN      VARCHAR2,
                   P_PASSWORD              IN      VARCHAR2,
                    P_VERSION               IN      VARCHAR2,
                    P_PERSON_ID             OUT     NUMBER,
                    P_BUSINESS_GROUP_ID     OUT     NUMBER,
                    P_USER_ID               OUT     NUMBER,
                    P_ASSIGNMENT_ID         OUT     NUMBER,
                    P_NTF_COUNT             OUT     NUMBER
                    ) RETURN VARCHAR2 IS
    L_RETURN        VARCHAR2(1000);
    L_AUTHORIZED    VARCHAR2(10);
    L_MOBILE_USER   NUMBER:=0;
    L_MOBILE_VERSION    VARCHAR2(10);
    L_USER_PERSON_TYPE  varchar2(1000);
    V_DESCRIPTION       varchar2(4000);
    
   BEGIN

/*        
    --Check user is authorized to use mobile or not
    BEGIN
        SELECT 1 
        INTO L_MOBILE_USER
        FROM FND_LOOKUP_VALUES    
        WHERE LOOKUP_TYPE = 'MBC_MOBILE_USERS'
        AND LANGUAGE = 'US'
        AND UPPER(MEANING) = UPPER(P_USER_NAME);
    EXCEPTION
        WHEN OTHERS THEN
        L_MOBILE_USER := 0;
    END;
    
    IF L_MOBILE_USER = 0 THEN
        L_RETURN := 'You are not authorized to use mobile app';
        RETURN L_RETURN;
    END IF;
    */
    
    --Check Version
    BEGIN
        SELECT MEANING , DESCRIPTION
        INTO L_MOBILE_VERSION , V_DESCRIPTION
        FROM FND_LOOKUP_VALUES    
        WHERE LOOKUP_TYPE = 'MBC_MOBILE_USERS'
        AND LANGUAGE = 'US'
        AND UPPER(LOOKUP_CODE) = 'VERSION';
        
        IF L_MOBILE_VERSION <> NVL(P_VERSION,-1) THEN
            L_RETURN := 'You have to update your application' ||V_DESCRIPTION;
            RETURN L_RETURN;
        END IF ;
        
    EXCEPTION
        WHEN OTHERS THEN
        L_RETURN := 'You have to update your application';
        RETURN L_RETURN;
    END;
    
    
    --

    
    --Check Authontication
    L_AUTHORIZED := FND_WEB_SEC.VALIDATE_LOGIN (P_USER_NAME, P_PASSWORD);
    
    IF L_AUTHORIZED = 'Y' THEN           
        
        L_RETURN := 'OK';
        
        SELECT PAPF.PERSON_ID,PAPF.BUSINESS_GROUP_ID,USER_ID , ASSIGNMENT_ID, PPT.USER_PERSON_TYPE,
        (SELECT count(*) ntf FROM MBC_WORKLIST_NOTIFICATION_V MN, MBC_WORKLIST_NTF_CONFIG_V MNB WHERE MN.MESSAGE_TYPE = MNB.MESSAGE_TYPE  AND   MN.MESSAGE_NAME = MNB.MESSAGE_NAME AND  NVL(MN.PROCESS_NAME , 1) = NVL(MNB.PROCESS_NAME ,NVL(MN.PROCESS_NAME , 1) )  AND    MN.RECIPIENT_ROLE =  FU.USER_NAME) ntf_no
        INTO P_PERSON_ID, P_BUSINESS_GROUP_ID, P_USER_ID, P_ASSIGNMENT_ID, L_USER_PERSON_TYPE, P_NTF_COUNT
        FROM FND_USER FU, PER_ALL_PEOPLE_F PAPF , PER_ALL_ASSIGNMENTS_F PAAF, APPS.PER_PERSON_TYPES PPT, APPS.PER_PERSON_TYPE_USAGES_F PPTU
        WHERE FU.EMPLOYEE_ID = PAPF.PERSON_ID
        AND PAPF.PERSON_ID = PAAF.PERSON_ID
        AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
        AND PAAF.PRIMARY_FLAG = 'Y'
        AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE
        AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE AND PAAF.EFFECTIVE_END_DATE
--        AND PPTU.EFFECTIVE_START_DATE BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE--
        and papf.person_type_id = pptu.person_type_id --                
        AND PAPF.BUSINESS_GROUP_ID = PPT.BUSINESS_GROUP_ID
        AND PAPF.PERSON_ID = PPTU.PERSON_ID        
        AND PPTU.PERSON_TYPE_ID = PPT.PERSON_TYPE_ID
        AND PAPF.PERSON_ID = PPTU.PERSON_ID
        AND PPTU.PERSON_TYPE_ID = PPT.PERSON_TYPE_ID
        AND SYSDATE BETWEEN PPTU.EFFECTIVE_START_DATE AND PPTU.EFFECTIVE_END_DATE
        AND FU.USER_NAME = UPPER(P_USER_NAME);
        
        
       -- IF L_USER_PERSON_TYPE <> 'Regular Employee' THEN
        IF INSTR (L_USER_PERSON_TYPE , 'Regular Employee' )=0 THEN
            L_RETURN := 'This App is allowed for Regular Employees only.';
            RETURN L_RETURN;
        END IF ;
        
        --Check user is authorized to use mobile or not (Only Dubai users are allowed)
        -- removed to grant access for all regular employees 10-07-2019
        /* 
        IF P_BUSINESS_GROUP_ID <> 81 THEN
            
            --Check user is authorized to use mobile or not from other locations
            BEGIN
                SELECT 1 
                INTO L_MOBILE_USER
                FROM FND_LOOKUP_VALUES    
                WHERE LOOKUP_TYPE = 'MBC_MOBILE_USERS'
                AND LANGUAGE = 'US'
                AND UPPER(MEANING) = UPPER(P_USER_NAME);
            EXCEPTION
                WHEN OTHERS THEN
                L_MOBILE_USER := 0;
            END;
            
            IF L_MOBILE_USER = 0 THEN
                L_RETURN := 'You are not authorized to use mobile app';
                RETURN L_RETURN;
            END IF;
            
        END IF;
        */

        
        mbc_mobile_pkg.LOG_MOBILE_ACTIVITY(1 , P_PERSON_ID , P_USER_ID , 'LOGIN' , 'DB', 'Successful Login' ,P_USER_NAME );
    
    ELSE
        L_RETURN:= 'Invalid Credentials';
    END IF;
    
    RETURN L_RETURN;
    
   EXCEPTION
    WHEN OTHERS THEN
    RETURN 'Error, Please contact system administrator';
   END LOGIN;                       
   
   
   FUNCTION LOGIN (P_USER_NAME              IN     VARCHAR2,
                   P_PASSWORD               IN     VARCHAR2,
                   P_VERSION               IN      VARCHAR2,
                   P_BUSINESS_GROUP            OUT NUMBER,
                   P_PERSON_ID                 OUT NUMBER,
                   P_EMPLOYEE_NUMBER           OUT VARCHAR2,
                   P_EMPLOYEE_NAME             OUT VARCHAR2,
                   P_HIRE_DATE                 OUT DATE,
                   P_BIRTH_DATE                OUT DATE,
                   P_GENDER                    OUT VARCHAR2,
                   P_PERSON_TYPE               OUT NUMBER,
                   P_PERSON_TYPE_DESC          OUT VARCHAR2,
                   P_NATIONAL_IDENTIFIER       OUT VARCHAR2,
                   P_MARITAL_STATUS            OUT VARCHAR2,
                   P_MARITAL_STATUS_DESC       OUT VARCHAR2,
                   P_EMAIL                     OUT VARCHAR2,
                   P_NATIONALITY               OUT NUMBER,
                   P_NATIONALITY_DESC          OUT VARCHAR2,
                   P_ORGANIZATION_ID           OUT NUMBER,
                   P_ORGANIZATION_NAME         OUT VARCHAR2,
                   P_DIRECTORATE               OUT VARCHAR2,
                   P_WORKING_COMPANY           OUT VARCHAR2,
                   P_COMPANY                   OUT VARCHAR2,
                   P_DIVISION                  OUT VARCHAR2,
                   P_LOCATION_COSTING          OUT VARCHAR2,
                   P_CONTRACT                  OUT VARCHAR2,
                   P_TICKET_OPTION             OUT VARCHAR2,
                   P_JOB_ID                    OUT NUMBER,
                   P_JOB_NAME                  OUT VARCHAR2,
                   P_POSITION_ID               OUT NUMBER,
                   P_POSITION_NAME             OUT VARCHAR2,
                   P_DESIGNATION               OUT VARCHAR2,
                   P_GRADE_ID                  OUT NUMBER,
                   P_GRADE_NAME                OUT VARCHAR2,
                   P_PAYROLL_ID                OUT NUMBER,
                   P_PAYROLL_NAME              OUT VARCHAR2,
                   P_LOCATION_ID               OUT NUMBER,
                   P_LOCATION_NAME             OUT VARCHAR2,
                   P_ASSG_STATUS               OUT VARCHAR2,
                   P_SALARY_BASIS_ID           OUT NUMBER,
                   P_SALARY_BASIS_NAME         OUT VARCHAR2,
                   P_SUPERVISOR_PERSON_ID      OUT NUMBER,
                   P_SUPERVISOR_EMP_NO         OUT VARCHAR2,
                   P_SUPERVISOR_EMP_NAME       OUT VARCHAR2,
                   P_SHOW_PHOTO                OUT NUMBER,
                   P_SHOW_GRADE                OUT NUMBER,
                   P_SHOW_MOBILE               OUT NUMBER,
                   P_SHOW_HIRE_DATE            OUT NUMBER,
                   P_RECEIVE_SMS               OUT NUMBER)
      RETURN VARCHAR2
   IS
      L_AUTHORIZED     VARCHAR2 (10);
      L_COUNT          NUMBER := 0;
      L_PASSWORD       VARCHAR2 (1000);
      L_USER_DATE_TO   DATE;
   BEGIN
      --Check Authontication
      L_AUTHORIZED := FND_WEB_SEC.VALIDATE_LOGIN (P_USER_NAME, P_PASSWORD);

      IF L_AUTHORIZED = 'Y'
      THEN
         SELECT EMPLOYEE_ID
           INTO P_PERSON_ID
           FROM FND_USER
          WHERE UPPER (FND_USER.USER_NAME) = UPPER (P_USER_NAME);

         IF P_PERSON_ID IS NULL
         THEN                                                   --Not Employee
            L_AUTHORIZED := 'N';
         ELSE                                               --Correct Password
            --Get Employee Information
            SELECT PAPF.BUSINESS_GROUP_ID,
                   PAPF.EMPLOYEE_NUMBER,
                   PAPF.FULL_NAME,
                   PAPF.ORIGINAL_DATE_OF_HIRE,
                   PAPF.DATE_OF_BIRTH,
                   PAPF.SEX, -----------------------------------------------------------------------
                   PPTU.PERSON_TYPE_ID,
                   PPT.USER_PERSON_TYPE,
                   PAPF.NATIONAL_IDENTIFIER,
                   PAPF.MARITAL_STATUS,
                   (SELECT MEANING
                      FROM APPS.FND_LOOKUP_VALUES L
                     WHERE     L.LOOKUP_TYPE = 'MAR_STATUS'
                           AND LANGUAGE = 'US'
                           AND L.LOOKUP_CODE = PAPF.MARITAL_STATUS)
                      "Marital Status",
                   PAPF.EMAIL_ADDRESS,
                   PAPF.PER_INFORMATION18,
                   (SELECT MEANING
                      FROM APPS.FND_LOOKUP_VALUES L
                     WHERE     L.LOOKUP_TYPE = 'AE_NATIONALITY'
                           AND LANGUAGE = 'US'
                           AND L.LOOKUP_CODE = PAPF.PER_INFORMATION18)
                      "Nationality Desc",
                   HAO.ORGANIZATION_ID,
                   HAO.NAME ORGANIZATION_NAME,
                   (SELECT SEGMENT1
                      FROM PAY_PEOPLE_GROUPS
                     WHERE PEOPLE_GROUP_ID = PAAF.PEOPLE_GROUP_ID)
                      "Directorate",
                   (SELECT SEGMENT2
                      FROM PAY_PEOPLE_GROUPS
                     WHERE PEOPLE_GROUP_ID = PAAF.PEOPLE_GROUP_ID)
                      "Working Company",
                   (SELECT SEGMENT3
                      FROM PAY_PEOPLE_GROUPS
                     WHERE PEOPLE_GROUP_ID = PAAF.PEOPLE_GROUP_ID)
                      "Company",
                   (SELECT SEGMENT4
                      FROM PAY_PEOPLE_GROUPS
                     WHERE PEOPLE_GROUP_ID = PAAF.PEOPLE_GROUP_ID)
                      "Division",
                   (SELECT SEGMENT13
                      FROM PAY_PEOPLE_GROUPS
                     WHERE PEOPLE_GROUP_ID = PAAF.PEOPLE_GROUP_ID)
                      "Location-Costing",
                   (SELECT SEGMENT10
                      FROM PAY_PEOPLE_GROUPS
                     WHERE PEOPLE_GROUP_ID = PAAF.PEOPLE_GROUP_ID)
                      "Contract",
                   (SELECT SEGMENT6
                      FROM PAY_PEOPLE_GROUPS
                     WHERE PEOPLE_GROUP_ID = PAAF.PEOPLE_GROUP_ID)
                      "Ticket Option",
                   PAAF.JOB_ID,
                   (SELECT JT.NAME
                      FROM APPS.PER_JOBS J, APPS.PER_JOBS_TL JT
                     WHERE     J.JOB_ID = PAAF.JOB_ID
                           AND J.JOB_ID = JT.JOB_ID
                           AND PAAF.BUSINESS_GROUP_ID = J.BUSINESS_GROUP_ID
                           AND JT.LANGUAGE = 'US')
                      "Job Name",
                   PAAF.POSITION_ID,
                   POS.NAME "Position Name",
                   SUBSTR (POS.NAME, 1, INSTR (POS.NAME, '.') - 1)
                      "Position Title",
                   PAAF.GRADE_ID,
                   (SELECT NAME
                      FROM APPS.PER_GRADES_TL
                     WHERE LANGUAGE = 'US' AND GRADE_ID = PAAF.GRADE_ID)
                      "Grade Name",
                   PAAF.PAYROLL_ID,
                   (SELECT PAYROLL_NAME
                      FROM PAY_PAYROLLS_F
                     WHERE     PAYROLL_ID = PAAF.PAYROLL_ID
                           AND SYSDATE BETWEEN EFFECTIVE_START_DATE
                                           AND EFFECTIVE_END_DATE)
                      "Payroll Name",
                   PAAF.LOCATION_ID,
                   (SELECT LOCATION_CODE
                      FROM APPS.HR_LOCATIONS_ALL
                     WHERE LOCATION_ID = PAAF.LOCATION_ID)
                      "Location Name",
                   (SELECT T.USER_STATUS
                      FROM APPS.PER_ASSIGNMENT_STATUS_TYPES_TL T
                     WHERE     T.ASSIGNMENT_STATUS_TYPE_ID =
                                  PAAF.ASSIGNMENT_STATUS_TYPE_ID
                           AND T.LANGUAGE = 'US')
                      "Assignment Status",
                   PAAF.PAY_BASIS_ID,
                   (SELECT NAME
                      FROM APPS.PER_PAY_BASES
                     WHERE PAY_BASIS_ID = PAAF.PAY_BASIS_ID)
                      "Salary Basis",
                   PAAF.SUPERVISOR_ID,
                   (SELECT EMPLOYEE_NUMBER
                      FROM APPS.PER_ALL_PEOPLE_F
                     WHERE     PERSON_ID = PAAF.SUPERVISOR_ID
                           AND SYSDATE BETWEEN EFFECTIVE_START_DATE
                                           AND EFFECTIVE_END_DATE)
                      "Line Manager Number",
                   (SELECT FULL_NAME
                      FROM APPS.PER_ALL_PEOPLE_F
                     WHERE     PERSON_ID = PAAF.SUPERVISOR_ID
                           AND SYSDATE BETWEEN EFFECTIVE_START_DATE
                                           AND EFFECTIVE_END_DATE)
                      "Line Manager Name"
              INTO P_BUSINESS_GROUP,
                   P_EMPLOYEE_NUMBER,
                   P_EMPLOYEE_NAME,
                   P_HIRE_DATE,
                   P_BIRTH_DATE,
                   P_GENDER,
                   P_PERSON_TYPE,
                   P_PERSON_TYPE_DESC,
                   P_NATIONAL_IDENTIFIER,
                   P_MARITAL_STATUS,
                   P_MARITAL_STATUS_DESC,
                   P_EMAIL,
                   P_NATIONALITY,
                   P_NATIONALITY_DESC,
                   P_ORGANIZATION_ID,
                   P_ORGANIZATION_NAME,
                   P_DIRECTORATE,
                   P_WORKING_COMPANY,
                   P_COMPANY,
                   P_DIVISION,
                   P_LOCATION_COSTING,
                   P_CONTRACT,
                   P_TICKET_OPTION,
                   P_JOB_ID,
                   P_JOB_NAME,
                   P_POSITION_ID,
                   P_POSITION_NAME,
                   P_DESIGNATION,
                   P_GRADE_ID,
                   P_GRADE_NAME,
                   P_PAYROLL_ID,
                   P_PAYROLL_NAME,
                   P_LOCATION_ID,
                   P_LOCATION_NAME,
                   P_ASSG_STATUS,
                   P_SALARY_BASIS_ID,
                   P_SALARY_BASIS_NAME,
                   P_SUPERVISOR_PERSON_ID,
                   P_SUPERVISOR_EMP_NO,
                   P_SUPERVISOR_EMP_NAME
              FROM PER_ALL_PEOPLE_F PAPF,
                   PER_ALL_ASSIGNMENTS_F PAAF,
                   PER_PERSON_TYPES PPT,
                   PER_PERSON_TYPE_USAGES_F PPTU,
                   HR_ALL_ORGANIZATION_UNITS HAO,
                   PER_POSITIONS POS
             WHERE     1 = 1
                   AND PAPF.PERSON_ID = PAAF.PERSON_ID
                   AND PAPF.PERSON_ID = P_PERSON_ID
                   AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                   AND PAPF.EFFECTIVE_END_DATE
                   AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE
                                   AND PAAF.EFFECTIVE_END_DATE
                   AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
                   AND PAAF.PRIMARY_FLAG = 'Y'
                   AND PAPF.BUSINESS_GROUP_ID = PPT.BUSINESS_GROUP_ID
                   AND PAPF.PERSON_ID = PPTU.PERSON_ID
                   AND PPTU.PERSON_TYPE_ID = PPT.PERSON_TYPE_ID
                   AND PPTU.PERSON_TYPE_USAGE_ID =
                          (SELECT MAX (PERSON_TYPE_USAGE_ID)
                             FROM PER_PERSON_TYPE_USAGES_F
                            WHERE PERSON_ID = PAPF.PERSON_ID)
                   AND SYSDATE BETWEEN PPTU.EFFECTIVE_START_DATE
                                   AND PPTU.EFFECTIVE_END_DATE
                   AND PAAF.ORGANIZATION_ID = HAO.ORGANIZATION_ID
                   AND PAAF.BUSINESS_GROUP_ID = POS.BUSINESS_GROUP_ID(+)
                   AND PAAF.POSITION_ID = POS.POSITION_ID(+);

            --Get Privacy Info
            BEGIN
               SELECT SHOW_PHOTO,
                      SHOW_GRADE,
                      SHOW_MOBILE,
                      SHOW_HIRE_DATE,
                      RECEIVE_SMS
                 INTO P_SHOW_PHOTO,
                      P_SHOW_GRADE,
                      P_SHOW_MOBILE,
                      P_SHOW_HIRE_DATE,
                      P_RECEIVE_SMS
                 FROM MBC_MOBILE_PRIVACY
                WHERE PERSON_ID = P_PERSON_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  IF P_GENDER = 'M'
                  THEN
                     P_SHOW_PHOTO := 1;
                  ELSE
                     P_SHOW_PHOTO := 0;
                  END IF;

                  P_SHOW_GRADE := 1;
                  P_SHOW_MOBILE := 1;
                  P_SHOW_HIRE_DATE := 1;
                  P_RECEIVE_SMS := 1;

                  --Insert Privacy Record
                  INSERT INTO MBC_MOBILE_PRIVACY (PERSON_ID,
                                                  SHOW_PHOTO,
                                                  SHOW_GRADE,
                                                  SHOW_MOBILE,
                                                  SHOW_HIRE_DATE,
                                                  RECEIVE_SMS)
                       VALUES (P_PERSON_ID,
                               P_SHOW_PHOTO,
                               P_SHOW_GRADE,
                               P_SHOW_MOBILE,
                               P_SHOW_HIRE_DATE,
                               P_RECEIVE_SMS);

                  COMMIT;
            END;

            L_AUTHORIZED := 'Y';
         END IF;
      END IF;

      RETURN L_AUTHORIZED;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END LOGIN;
   
   --Change Password
   FUNCTION CHANGE_PASSWORD(P_USER IN VARCHAR2,
                         P_OLD_PWD IN VARCHAR2,
                         P_NEW_PWD1 IN VARCHAR2,
                         P_NEW_PWD2 IN VARCHAR2,
                         P_AUTONOMOUS IN BOOLEAN DEFAULT TRUE)  RETURN VARCHAR2 IS
      V_OUT     VARCHAR2(100);
   BEGIN
      
      V_OUT := CHANGE_PASSWORD(P_USER, P_OLD_PWD, P_NEW_PWD1, P_NEW_PWD2, P_AUTONOMOUS);
      
      RETURN V_OUT ;
      
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN V_OUT;
   END CHANGE_PASSWORD;            


   FUNCTION GET_EMP_PHOTO (P_PERSON_ID IN NUMBER)
      RETURN BLOB
   IS
      L_PHOTO   BLOB;
      L_FLAG    NUMBER := 0;
   BEGIN
      BEGIN
         SELECT SHOW_PHOTO
           INTO L_FLAG
           FROM MBC_MOBILE_PRIVACY
          WHERE PERSON_ID = P_PERSON_ID;
      EXCEPTION
         WHEN OTHERS
         THEN
            L_FLAG := 0;
      END;

      IF L_FLAG = 1
      THEN
         SELECT IMAGE
           INTO L_PHOTO
           FROM PER_IMAGES
          WHERE     1 = 1
                AND TABLE_NAME = 'PER_PEOPLE_F'
                AND PARENT_ID = P_PERSON_ID;
      ELSE
         --            RETURN UTL_RAW.cast_to_raw (' ');
         L_FLAG := NULL;
      END IF;

      RETURN L_PHOTO;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN UTL_RAW.CAST_TO_RAW (' ');
   END GET_EMP_PHOTO;


   FUNCTION GENERATE_TRANSACTION_DOCUMENT (
      P_ABSENCE_ATTENDANCE_ID         NUMBER,
      P_TRANSACTION_ID                NUMBER,
      P_BUSINESS_GROUP_ID             NUMBER,
      P_ABSENCE_ATTENDANCE_TYPE_ID    NUMBER,
      P_PERSON_ID                     NUMBER,
      P_SUPERVISOR_ID                 NUMBER,
      P_REPLACEMENT_PERSON_ID         NUMBER,
      P_LOCATION_ID                   NUMBER,
      P_POSITION_ID                   NUMBER,
      P_ABSENCE_DAYS                  NUMBER,
      P_ABSENCE_HOURS                 NUMBER,
      P_ASSIGNMENT_ID                 NUMBER,
      P_ORGANIZATION_ID               NUMBER,
      P_PAYROLL_ID                    NUMBER,
      P_JOB_ID                        NUMBER,
      P_GRADE_ID                      NUMBER,
      P_USER_ID                       NUMBER,
      P_ASG_EFFECTIVE_START_DATE      DATE,
      P_DATE_START                    DATE,
      P_DATE_END                      DATE,
      P_FULL_NAME                     VARCHAR2,
      P_JOB_NAME                      VARCHAR2,
      P_POSITION_NAME                 VARCHAR2,
      P_SUPERVISOR_NAME               VARCHAR2,
      P_EMPLOYEE_NUMBER               VARCHAR2,
      P_LEGISLATION_CODE              VARCHAR2,
      P_BG_CURRENCY                   VARCHAR2,
      P_PER_INFORMATION_CATEGORY      VARCHAR2,
      P_ATTRIBUTE1                    VARCHAR2 := NULL,
      P_ATTRIBUTE2                    VARCHAR2 := NULL,
      P_ATTRIBUTE3                    VARCHAR2 := NULL,
      P_ATTRIBUTE4                    VARCHAR2 := NULL,
      P_ATTRIBUTE5                    VARCHAR2 := NULL,
      P_ATTRIBUTE6                    VARCHAR2 := NULL,
      P_ATTRIBUTE7                    VARCHAR2 := NULL,
      P_ATTRIBUTE8                    VARCHAR2 := NULL,
      P_ATTRIBUTE9                    VARCHAR2 := NULL,
      P_ATTRIBUTE10                   VARCHAR2 := NULL,
      P_ATTRIBUTE11                   VARCHAR2 := NULL,
      P_ATTRIBUTE12                   VARCHAR2 := NULL,
      P_ATTRIBUTE13                   VARCHAR2 := NULL,
      P_ATTRIBUTE14                   VARCHAR2 := NULL,
      P_ATTRIBUTE15                   VARCHAR2 := NULL,
      P_ATTRIBUTE16                   VARCHAR2 := NULL,
      P_ATTRIBUTE17                   VARCHAR2 := NULL,
      P_ATTRIBUTE18                   VARCHAR2 := NULL,
      P_ATTRIBUTE19                   VARCHAR2 := NULL,
      P_ATTRIBUTE20                   VARCHAR2 := NULL,
      P_ATTRIBUTE21                   VARCHAR2 := NULL,
      P_ATTRIBUTE22                   VARCHAR2 := NULL,
      P_ATTRIBUTE23                   VARCHAR2 := NULL,
      P_ATTRIBUTE24                   VARCHAR2 := NULL,
      P_ATTRIBUTE25                   VARCHAR2 := NULL,
      P_ATTRIBUTE26                   VARCHAR2 := NULL,
      P_ATTRIBUTE27                   VARCHAR2 := NULL,
      P_ATTRIBUTE28                   VARCHAR2 := NULL,
      P_ATTRIBUTE29                   VARCHAR2 := NULL,
      P_ATTRIBUTE30                   VARCHAR2 := NULL
      ,P_ABS_INFORMATION1 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION2 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION3 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION4 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION5 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION6 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION7 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION8 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION9 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION10 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION11 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION12 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION13 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION14 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION15 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION16 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION17 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION18 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION19 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION20 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION21 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION22 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION23 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION24 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION25 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION26 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION27 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION28 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION29 IN  VARCHAR2 := NULL
      ,P_ABS_INFORMATION30 IN  VARCHAR2 := NULL
)
      RETURN CLOB
   IS
      V_XML_DOC   CLOB;
   BEGIN
      --start here
     
      V_XML_DOC :=
            '<Transaction>'
         || CHR (10)
         || '   <TransCtx>'
         || CHR (10)
         || '      <TransactionGroup>ABSENCE_MGMT</TransactionGroup>'
         || CHR (10)
         || '      <PrsnJobName>'
         || P_JOB_NAME
         || '</PrsnJobName>'
         || CHR (10)
         || '      <PrsnAsgFlag>Y</PrsnAsgFlag>'
         || CHR (10)
         || '      <LoginPrsnNpwFlag>A</LoginPrsnNpwFlag>'
         || CHR (10)
         || '      <FyiDetails>Y</FyiDetails>'
         || CHR (10)
         || '      <PrsnBgId>'
         || P_BUSINESS_GROUP_ID
         || '</PrsnBgId>'
         || CHR (10)
         || '      <EmployeeGeneration>M</EmployeeGeneration>'
         || CHR (10)
         || '      <PrsnNpwFlag>A</PrsnNpwFlag>'
         || CHR (10)
         || '      <ItemType>HRSSA</ItemType>'
         || CHR (10)
         || '      <PrsnJobId>'
         || P_JOB_ID
         || '</PrsnJobId>'
         || CHR (10)
         || '      <AsgStartDate dataType="d">'
         || TO_CHAR (P_ASG_EFFECTIVE_START_DATE, 'YYYY-MM-DD')
         || '</AsgStartDate>'
         || CHR (10)
         ||                     -------------------------------------chr(10)||
           '      <PrsnGradeId>'
         || P_GRADE_ID
         || '</PrsnGradeId>'
         || CHR (10)
         || '      <TransactionRefId dataType="n">'
         || P_ABSENCE_ATTENDANCE_ID
         || '</TransactionRefId>'
         || CHR (10)
         ||                                            -----------------------
           '      <LoginPrsnLegCode>'
         || P_LEGISLATION_CODE
         || '</LoginPrsnLegCode>'
         || CHR (10)
         || '      <PrsnMgrId>'
         || P_SUPERVISOR_ID
         || '</PrsnMgrId>'
         || CHR (10)
         || '      <PrsnAssignmentId>'
         || P_ASSIGNMENT_ID
         || '</PrsnAssignmentId>'
         || CHR (10)
         || '      <PrsnLegCode>'
         || P_LEGISLATION_CODE
         || '</PrsnLegCode>'
         || CHR (10)
         || '      <LoginPrsnId>'
         || P_PERSON_ID
         || '</LoginPrsnId>'
         || CHR (10)
         || '      <LoginPrsnContextSet dataType="b">true</LoginPrsnContextSet>'
         || CHR (10)
         ||                                                 ------------------
           '      <pNtfSubMsg>HR_ABS_NTF_SUB_MSG</pNtfSubMsg>'
         || CHR (10)
         ||                              -------------------------------------
           '      <ProductCode>PER</ProductCode>'
         || CHR (10)
         || '      <EffectiveDate dataType="d">'
         || TO_CHAR (SYSDATE, 'YYYY-MM-DD')
         || '</EffectiveDate>'
         || CHR (10)
         || '      <LoginPrsnEmpFlag>M</LoginPrsnEmpFlag>'
         || CHR (10)
         ||  -----------------------------------------------------------------
           '      <LoginPrsnType>E</LoginPrsnType>'
         || CHR (10)
         || '      <pCalledId dataType="n">56643</pCalledId>'
         || CHR (10)
         ||     --------------------------------------------------------------
           '      <ReviewTemplateRNAttr>HR_ABS_NTF_SS</ReviewTemplateRNAttr>'
         || CHR (10)
         || '      <TransactionId>'
         || P_TRANSACTION_ID
         || '</TransactionId>'
         || CHR (10)
         || '      <PrsnLocationId>'
         || P_LOCATION_ID
         || '</PrsnLocationId>'
         || CHR (10)
         || '      <PrsnKflexStructCode>MBC_Dubai_PG</PrsnKflexStructCode>'
         || CHR (10)
         ||                ---------------------------------------------------
           '      <LoginPrsnMgrName>'
         || P_SUPERVISOR_NAME
         || '</LoginPrsnMgrName>'
         || CHR (10)
         || '      <PrsnContextSet dataType="b">true</PrsnContextSet>'
         || CHR (10)
         || '      <PrsnPositionName>'
         || P_POSITION_NAME
         || '</PrsnPositionName>'
         || CHR (10)
         || '      <PrsnEmpFlag>M</PrsnEmpFlag>'
         || CHR (10)
         || ------------------------------------------------------------------
           '      <PrsnMgrName>'
         || P_SUPERVISOR_NAME
         || '</PrsnMgrName>'
         || CHR (10)
         || '      <PrsnOrganizationId>'
         || P_ORGANIZATION_ID
         || '</PrsnOrganizationId>'
         || CHR (10)
         || '      <PrsnPositionId>'
         || P_POSITION_ID
         || '</PrsnPositionId>'
         || CHR (10)
         || '      <TxnStatus>W</TxnStatus>'
         || CHR (10)
         || '      <LoginWorkerNumber>'
         || P_EMPLOYEE_NUMBER
         || '</LoginWorkerNumber>'
         || CHR (10)
         || '      <PrsnBgCurrencyCode>'
         || P_BG_CURRENCY
         || '</PrsnBgCurrencyCode>'
         || CHR (10)
         || '      <HeaderType>PER_HEADER</HeaderType>'
         || CHR (10)
         || '      <SSHR_WF_BASED dataType="b">true</SSHR_WF_BASED>'
         || CHR (10)
         || '      <PrsnSecurityGroupId>0</PrsnSecurityGroupId>'
         || CHR (10)
         ||                                   --------------------------------
           '      <LoginPrsnMgrId>'
         || P_SUPERVISOR_ID
         || '</LoginPrsnMgrId>'
         || CHR (10)
         || '      <PrsnType>E</PrsnType>'
         || CHR (10)
         || '      <PerzOrganizationId>'
         || P_BUSINESS_GROUP_ID
         || '</PerzOrganizationId>'
         || CHR (10)
         || '      <LoginPrsnName>'
         || P_FULL_NAME
         || '</LoginPrsnName>'
         || CHR (10)
         || '      <CreatorPrsnId dataType="n">'
         || P_PERSON_ID
         || '</CreatorPrsnId>'
         || CHR (10)
         || '      <PrsnId>'
         || P_PERSON_ID
         || '</PrsnId>'
         || CHR (10)
         || '      <TransactionType>WF</TransactionType>'
         || CHR (10)
         || '      <NtfAttachAttr>FND:entity=PQH_SS_ATTACHMENT;pk1name=TransactionId;pk1value='
         || P_TRANSACTION_ID
         || '</NtfAttachAttr>'
         || CHR (10)
         || '      <pApprovalReqd>YD</pApprovalReqd>'
         || CHR (10)
         || '      <pAMETranType>SSHRMS</pAMETranType>'
         || CHR (10)
         || '      <RelaunchFunction>HR_ABS_ENTRY_PAGE_SS</RelaunchFunction>'
         || CHR (10)
         || '      <PerzLocalizationCode>'
         || P_LEGISLATION_CODE
         || '</PerzLocalizationCode>'
         || CHR (10)
         || '      <TransactionRefTable>PER_ABSENCE_ATTENDANCES</TransactionRefTable>'
         || CHR (10)
         || '      <TransactionIdentifier>ABSENCES</TransactionIdentifier>'
         || CHR (10)
         || '      <pCalledFrom>MBC_LEAVE_REQUEST</pCalledFrom>'
         || CHR (10)
         || '      <LoginPrsnBgId>'
         || P_BUSINESS_GROUP_ID
         || '</LoginPrsnBgId>'
         || CHR (10)
         || '      <PerzFunctionName>MBC_LEAVE_REQUEST</PerzFunctionName>'
         || CHR (10)
         || '      <ProcessName>MBC_LEAVE_REQUEST</ProcessName>'
         || CHR (10)
         || '      <PrsnPayrollId>'
         || P_PAYROLL_ID
         || '</PrsnPayrollId>'
         || CHR (10)
         || '      <PrsnName>'
         || P_FULL_NAME
         || '</PrsnName>'
         || CHR (10)
         || '      <CNode name="AbsenceParams" type="Ht">'
         || CHR (10)
         || '         <AbsenceAttdId>'
         || P_ABSENCE_ATTENDANCE_ID
         || '</AbsenceAttdId>'
         || CHR (10)
         || '         <AbsenceAction>CreateMode</AbsenceAction>'
         || CHR (10)
         || '      </CNode>'
         || CHR (10)
         || '      <pAMEAppId>800</pAMEAppId>'
         || CHR (10)
         || '      <SSHR_REVIEW_FLOW_MODE>WF_REVIEW_PAGE</SSHR_REVIEW_FLOW_MODE>'
         || CHR (10)
         || '   </TransCtx>'
         || CHR (10)
         || '   <EoApiMap>'
         || CHR (10)
         || '      <EO Name="oracle.apps.per.schema.server.PerAbsenceAttendancesEO">HR_PERSON_ABSENCE_SWI.PROCESS_API</EO>'
         || CHR (10)
         || '   </EoApiMap>'
         || CHR (10)
         || '   <TransCache>'
         || CHR (10)
         || '      <AM MomVer="1044362310593">'
         || CHR (10)
         || '         <cd/>'
         || CHR (10)
         || '         <TXN Def="0" New="0" Lok="2" pcid="84">'
         || CHR (10)
         ||              -----------------------------------------------------
           '            <EO Name="oracle.apps.per.schema.server.PerAbsenceAttendancesEO">'
         || CHR (10)
         || '               <![CDATA[000100000005C40857031D]]>'
         || CHR (10)
         || '               <PerAbsenceAttendancesEORow PS="0" PK="Y">'
         || CHR (10)
         || '                  <AbsenceAttendanceId>'
         || P_ABSENCE_ATTENDANCE_ID
         || '</AbsenceAttendanceId>'
         || CHR (10)
         || '                  <BusinessGroupId>'
         || P_BUSINESS_GROUP_ID
         || '</BusinessGroupId>'
         || CHR (10)
         || '                  <AbsenceAttendanceTypeId>'
         || P_ABSENCE_ATTENDANCE_TYPE_ID
         || '</AbsenceAttendanceTypeId>'
         || CHR (10)
         || '                  <PersonId>'
         || P_PERSON_ID
         || '</PersonId>'
         || CHR (10)
         || '                  <ReplacementPersonId>'
         || P_REPLACEMENT_PERSON_ID
         || '</ReplacementPersonId>'
         || CHR (10)
         ||        --------------------------****-----------------------------
           '                  <AbsenceDays>'
         || P_ABSENCE_DAYS
         || '</AbsenceDays>'
         || CHR (10)
         || '                  <AbsenceHours null="true"/>'
         || CHR (10)
         || '                  <DateEnd>'
         || TO_CHAR (P_DATE_END, 'YYYY-MM-DD')
         || '</DateEnd>'
         || CHR (10)
         || '                  <DateNotification>'
         || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS')
         || '</DateNotification>'
         || CHR (10)
         || '                  <DateProjectedEnd null="true"/>'
         || CHR (10)
         || '                  <DateProjectedStart null="true"/>'
         || CHR (10)
         || '                  <DateStart>'
         || TO_CHAR (P_DATE_START, 'YYYY-MM-DD')
         || '</DateStart>'
         || CHR (10)
         || '                  <TimeProjectedEnd null="true"/>'
         || CHR (10)
         || '                  <TimeProjectedStart null="true"/>'
         || CHR (10)
         || '                  <Attribute1>'
         || P_ATTRIBUTE1
         || '</Attribute1>'
         || CHR (10)
         || '                  <Attribute2>'
         || P_ATTRIBUTE2
         || '</Attribute2>'
         || CHR (10)
         || '                  <Attribute3>'
         || P_ATTRIBUTE3
         || '</Attribute3>'
         || CHR (10)
         || '                  <Attribute4>'
         || P_ATTRIBUTE4
         || '</Attribute4>'
         || CHR (10)
         || '                  <Attribute5>'
         || P_ATTRIBUTE5
         || '</Attribute5>'
         || CHR (10)
         || '                  <Attribute6>'
         || P_ATTRIBUTE6
         || '</Attribute6>'
         || CHR (10)
         || '                  <Attribute7>'
         || P_ATTRIBUTE7
         || '</Attribute7>'
         || CHR (10)
         || '                  <Attribute8>'
         || P_ATTRIBUTE8
         || '</Attribute8>'
         || CHR (10)
         || '                  <LastUpdateDate>'
         || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS')
         || '</LastUpdateDate>'
         || CHR (10)
         || '                  <LastUpdatedBy>'
         || P_USER_ID
         || '</LastUpdatedBy>'
         || CHR (10)
         || '                  <LastUpdateLogin>-1</LastUpdateLogin>'
         || CHR (10)
         || ----------------------------------***********----------------------------
           '                  <CreatedBy>'
         || P_USER_ID
         || '</CreatedBy>'
         || CHR (10)
         || '                  <CreationDate>'
         || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS')
         || '</CreationDate>'
         || CHR (10)
         || '<AbsInformationCategory>'||P_BUSINESS_GROUP_ID||'</AbsInformationCategory>'
         || CHR (10) 
         || '                  <AbsInformation1>'
         || substr(NVL(P_ABS_INFORMATION1,'NO'),1,1)
         ||'</AbsInformation1>'
         || CHR (10)
         || '                  <AbsInformation2>'
         ||P_ABS_INFORMATION2
         ||'</AbsInformation2>'
         || CHR (10)
         || '                  <AbsInformation3>'
         ||P_ABS_INFORMATION3
         ||'</AbsInformation3>'
         || CHR (10)
         || '                  <AbsInformation4>'
         ||P_ABS_INFORMATION4
         ||'</AbsInformation4>'
         || CHR (10)
         || '                  <AbsInformation5>'
         ||P_ABS_INFORMATION5
         ||'</AbsInformation5>'
         || CHR (10)
         || '                  <AbsInformation6>'
         ||P_ABS_INFORMATION6
         ||'</AbsInformation6>'
         || CHR (10)
         || '                  <AbsInformation7>'
         ||P_ABS_INFORMATION7
         ||'</AbsInformation7>'
         || CHR (10)
         || '                  <AbsInformation8>'
         ||P_ABS_INFORMATION8
         ||'</AbsInformation8>'
         || CHR (10)
         || '                  <AbsInformation9>'
         ||P_ABS_INFORMATION9
         ||'</AbsInformation9>'
         || CHR (10)
         || '                  <AbsInformation10>'
         ||P_ABS_INFORMATION10
         ||'</AbsInformation10>'
         || CHR (10)
         || '                  <AbsInformation11>'
         ||P_ABS_INFORMATION11
         ||'</AbsInformation11>'
         || CHR (10)
         || '                  <AbsInformation12>'
         ||P_ABS_INFORMATION12
         ||'</AbsInformation12>'
         || CHR (10)
         || '                  <AbsInformation13  >'
         ||P_ABS_INFORMATION13
         ||'</AbsInformation13>'
         || CHR (10)
         || '                  <AbsInformation14>'
         ||P_ABS_INFORMATION14
         ||'</AbsInformation14>'
         || CHR (10)
         || '                  <AbsInformation15>'
         ||P_ABS_INFORMATION15
         ||'</AbsInformation15>'
         || CHR (10)
         || '                  <AbsInformation16>'
         ||P_ABS_INFORMATION16
         ||'</AbsInformation16>'
         || CHR (10)
         || '                  <AbsInformation17>'
         ||P_ABS_INFORMATION17
         ||'</AbsInformation17>'
         || CHR (10)
         || '                  <AbsInformation18>'
         ||P_ABS_INFORMATION18
         ||'</AbsInformation18>'
         || CHR (10)
         || '                  <AbsInformation19>'
         ||P_ABS_INFORMATION19
         ||'</AbsInformation19>'
         || CHR (10)
         || '                  <AbsInformation20>'
         ||P_ABS_INFORMATION20
         ||'</AbsInformation20>'
         || CHR (10)
         || '                  <AbsInformation21>'
         ||P_ABS_INFORMATION21
         ||'</AbsInformation21>'
         || CHR (10)
         || '                  <AbsInformation22>'
         ||P_ABS_INFORMATION22
         ||'</AbsInformation22>'
         || CHR (10)
         || '                  <AbsInformation23>'
         ||P_ABS_INFORMATION23
         ||'</AbsInformation23>'
         || CHR (10)
         || '                  <AbsInformation24>'
         ||P_ABS_INFORMATION24
         ||'</AbsInformation24>'
         || CHR (10)
         || '                  <AbsInformation25>'
         ||P_ABS_INFORMATION25
         ||'</AbsInformation25>'
         || CHR (10)
         || '                  <AbsInformation26>'
         ||P_ABS_INFORMATION26
         ||'</AbsInformation26>'
         || CHR (10)
         || '                  <AbsInformation27>'
         ||P_ABS_INFORMATION27
         ||'</AbsInformation27>'
         || CHR (10)
         || '                  <AbsInformation28>'
         ||P_ABS_INFORMATION28
         ||'</AbsInformation28>'
         || CHR (10)
         || '                  <AbsInformation29>'
         ||P_ABS_INFORMATION29
         ||'</AbsInformation29>'
         || CHR (10)
         || '                  <AbsInformation30>'
         ||P_ABS_INFORMATION30
         ||'</AbsInformation30>'
         || CHR (10)
         || '                  <ObjectVersionNumber/>'
         || CHR (10)
         || '                  <LinkedAbsenceIdPerAbsenceAttendancesEO/>'
         || CHR (10)
         || '               </PerAbsenceAttendancesEORow>'
         || CHR (10)
         || '            </EO>'
         || CHR (10)
         || '            <EO Name="oracle.apps.per.schema.server.TransactionsEO">'
         || CHR (10)
         || '               <![CDATA[000100000005C4090E2712]]>'
         || CHR (10)
         || '               <TransactionsEORow PS="0"/>'
         || CHR (10)
         || '            </EO>'
         || CHR (10)
         || '         </TXN>'
         || CHR (10)
         || '      </AM>'
         || CHR (10)
         || '   </TransCache>'
         || CHR (10)
         || '</Transaction>';

      -- end here

      RETURN V_XML_DOC;
   END GENERATE_TRANSACTION_DOCUMENT;

   FUNCTION START_WORKFLOW (P_PERSON_ID                     NUMBER,
                            P_BUSINESS_GROUP_ID             NUMBER,
                            P_ABSENCE_ATTENDANCE_TYPE_ID    NUMBER,
                            P_REPLACEMENT_PERSON_ID         NUMBER,
                            P_ABSENCE_TYPE_NAME             VARCHAR2,
                            P_DATE_START                    DATE,
                            P_DATE_END                      DATE,
                            P_ABSENCE_DAYS                  NUMBER,
                            P_TRANSACTION_ID                NUMBER,
                            P_TRANSACTION_STEP_ID           NUMBER,
                            P_ITEM_KEY                      NUMBER,
                            P_ABSENCE_ATTENDANCE_ID         NUMBER,
                            P_STATUS                        VARCHAR2,
                            P_ATTRIBUTE1                    VARCHAR2 := NULL,
                            P_ATTRIBUTE2                    VARCHAR2 := NULL,
                            P_ATTRIBUTE3                    VARCHAR2 := NULL,
                            P_ATTRIBUTE4                    VARCHAR2 := NULL,
                            P_ATTRIBUTE5                    VARCHAR2 := NULL,
                            P_ATTRIBUTE6                    VARCHAR2 := NULL,
                            P_ATTRIBUTE7                    VARCHAR2 := NULL,
                            P_ATTRIBUTE8                    VARCHAR2 := NULL,
                            P_ATTRIBUTE9                    VARCHAR2 := NULL,
                            P_ATTRIBUTE10                   VARCHAR2 := NULL,
                            P_ATTRIBUTE11                   VARCHAR2 := NULL,
                            P_ATTRIBUTE12                   VARCHAR2 := NULL,
                            P_ATTRIBUTE13                   VARCHAR2 := NULL,
                            P_ATTRIBUTE14                   VARCHAR2 := NULL,
                            P_ATTRIBUTE15                   VARCHAR2 := NULL,
                            P_ATTRIBUTE16                   VARCHAR2 := NULL,
                            P_ATTRIBUTE17                   VARCHAR2 := NULL,
                            P_ATTRIBUTE18                   VARCHAR2 := NULL,
                            P_ATTRIBUTE19                   VARCHAR2 := NULL,
                            P_ATTRIBUTE20                   VARCHAR2 := NULL,
                            P_ATTRIBUTE21                   VARCHAR2 := NULL,
                            P_ATTRIBUTE22                   VARCHAR2 := NULL,
                            P_ATTRIBUTE23                   VARCHAR2 := NULL,
                            P_ATTRIBUTE24                   VARCHAR2 := NULL,
                            P_ATTRIBUTE25                   VARCHAR2 := NULL,
                            P_ATTRIBUTE26                   VARCHAR2 := NULL,
                            P_ATTRIBUTE27                   VARCHAR2 := NULL,
                            P_ATTRIBUTE28                   VARCHAR2 := NULL,
                            P_ATTRIBUTE29                   VARCHAR2 := NULL,
                            P_ATTRIBUTE30                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE1                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE2                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE3                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE4                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE5                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE6                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE7                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE8                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE9                    VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE10                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE11                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE12                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE13                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE14                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE15                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE16                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE17                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE18                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE19                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE20                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE21                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE22                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE23                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE24                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE25                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE26                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE27                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE28                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE29                   VARCHAR2 := NULL,
                            P_ABS_ATTRIBUTE30                   VARCHAR2 := NULL)
      RETURN NUMBER
   IS
      CURSOR CUR_EMP_DATA
      IS
         SELECT PAPF.EMPLOYEE_NUMBER,
                PAPF.FULL_NAME,
                FU.USER_ID,
                PBG.LEGISLATION_CODE,
                PBG.CURRENCY_CODE,
                PBG.SECURITY_GROUP_ID,
                PJ.NAME JOB_NAME,
                PP.NAME POSITION_NAME,
                (SELECT FULL_NAME
                   FROM PER_ALL_PEOPLE_F
                  WHERE     PERSON_ID = PAAF.SUPERVISOR_ID
                        AND SYSDATE BETWEEN EFFECTIVE_START_DATE
                                        AND EFFECTIVE_END_DATE
                        AND CURRENT_EMPLOYEE_FLAG = 'Y')
                   SUPERVISOR_NAME,
                PAAF.*
           FROM APPS.PER_ALL_PEOPLE_F PAPF,
                APPS.PER_ALL_ASSIGNMENTS_F PAAF,
                FND_USER FU,
                PER_BUSINESS_GROUPS PBG,
                PER_JOBS PJ,
                PER_POSITIONS PP
          WHERE     PAPF.PERSON_ID = PAAF.PERSON_ID
                AND PAPF.PERSON_ID = FU.EMPLOYEE_ID
                AND PAPF.BUSINESS_GROUP_ID = PBG.BUSINESS_GROUP_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE
                                AND PAAF.EFFECTIVE_END_DATE
                AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
                AND PAAF.PRIMARY_FLAG = 'Y'
                AND PAAF.JOB_ID = PJ.JOB_ID(+)
                AND PAAF.POSITION_ID = PP.POSITION_ID(+)
                AND PAPF.PERSON_ID = P_PERSON_ID;

      V_TRANSACTION_ID              NUMBER := P_TRANSACTION_ID;
      V_TRANSACTION_STEP_ID         NUMBER := P_TRANSACTION_STEP_ID;
      V_ITEM_KEY                    NUMBER := P_ITEM_KEY;
      V_ABSENCE_ATTENDANCE_ID       NUMBER := P_ABSENCE_ATTENDANCE_ID;
      V_ITEM_TYPE                   VARCHAR2 (10) := 'HRSSA';
      V_TRANSACTION_DOCUMENT        CLOB;



      V_OBJECT_VERSION_NUMBER       NUMBER;

      V_APPROVAL_REQUIRED           VARCHAR2 (10) := 'YD';
      V_NTFSUBMSG                   VARCHAR2 (20) := 'HR_ABS_NTF_SUB_MSG';
      V_AMETRANSTYPE                VARCHAR2 (10) := 'SSHRMS';
      V_AMETRANAPPID                NUMBER := 800;
      V_REVIEW_TEMPLATE_RN          VARCHAR2 (20) := 'HR_ABS_NTF_SS';
      V_NTF_ATTACH_ATTR             VARCHAR2 (10) := NULL;
      V_PERZ_FUNC                   VARCHAR2 (10) := 'HR_LOA_SS'; ------------------------------
      --        V_PERZ_LEG              VARCHAR2(10) := 'AE';
      V_PERZ_ORG                    NUMBER;


      VT_ADDITIONAL_WF_ATTRIBUTES   HR_WF_ATTR_TABLE
         := HR_WF_ATTR_TABLE (HR_WF_ATTR_TYPE ('TRAN_SUBMIT',
                                               'N',
                                               NULL,
                                               NULL));


      V_STATUS                      VARCHAR2 (4000);
      V_ERROR_MESSAGE               VARCHAR2 (4000);
      V_ERRSTACK                    VARCHAR2 (4000);
   BEGIN
      FOR REQUEST_REC IN CUR_EMP_DATA
      LOOP
         --Init Profiles
         HR_TRANSACTION_SWI.INIT_PROFILES (
            P_PERSON_ID           => REQUEST_REC.PERSON_ID,
            P_ASSIGNMENT_ID       => REQUEST_REC.ASSIGNMENT_ID,
            P_BUSINESS_GROUP_ID   => REQUEST_REC.BUSINESS_GROUP_ID,
            P_ORGANIZATION_ID     => REQUEST_REC.ORGANIZATION_ID,
            P_LOCATION_ID         => REQUEST_REC.LOCATION_ID,
            P_PAYROLL_ID          => REQUEST_REC.PAYROLL_ID);

         --Create XML Transaction Document
         V_TRANSACTION_DOCUMENT :=
            MBC_MOBILE_PKG.GENERATE_TRANSACTION_DOCUMENT (
               V_ABSENCE_ATTENDANCE_ID,
               V_TRANSACTION_ID,
               P_BUSINESS_GROUP_ID,
               P_ABSENCE_ATTENDANCE_TYPE_ID,
               P_PERSON_ID,
               REQUEST_REC.SUPERVISOR_ID,
               P_REPLACEMENT_PERSON_ID,
               REQUEST_REC.LOCATION_ID,
               REQUEST_REC.POSITION_ID,
               P_ABSENCE_DAYS,
               0,                        --P_ABSENCE_HOURS                   ,
               REQUEST_REC.ASSIGNMENT_ID,
               REQUEST_REC.ORGANIZATION_ID,
               REQUEST_REC.PAYROLL_ID,
               REQUEST_REC.JOB_ID,
               REQUEST_REC.GRADE_ID,
               REQUEST_REC.USER_ID,
               REQUEST_REC.EFFECTIVE_START_DATE,
               P_DATE_START,
               P_DATE_END,
               REQUEST_REC.FULL_NAME,
               REQUEST_REC.JOB_NAME,
               REQUEST_REC.POSITION_NAME,
               REQUEST_REC.SUPERVISOR_NAME,
               REQUEST_REC.EMPLOYEE_NUMBER,
               REQUEST_REC.LEGISLATION_CODE,
               REQUEST_REC.CURRENCY_CODE,
               NULL,                     --P_PER_INFORMATION_CATEGORY        ,
               P_ATTRIBUTE1,
               P_ATTRIBUTE2,
               P_ATTRIBUTE3,
               P_ATTRIBUTE4,
               P_ATTRIBUTE5,
               P_ATTRIBUTE6,
               P_ATTRIBUTE7,
               P_ATTRIBUTE8,
               P_ATTRIBUTE9,
               P_ATTRIBUTE10,
               P_ATTRIBUTE11,
               P_ATTRIBUTE12,
               P_ATTRIBUTE13,
               P_ATTRIBUTE14,
               P_ATTRIBUTE15,
               P_ATTRIBUTE16,
               P_ATTRIBUTE17,
               P_ATTRIBUTE18,
               P_ATTRIBUTE19,
               P_ATTRIBUTE20,
               P_ATTRIBUTE21,
               P_ATTRIBUTE22,
               P_ATTRIBUTE23,
               P_ATTRIBUTE24,
               P_ATTRIBUTE25,
               P_ATTRIBUTE26,
               P_ATTRIBUTE27,
               P_ATTRIBUTE28,
               P_ATTRIBUTE29,
               P_ATTRIBUTE30,               
               P_ABS_ATTRIBUTE1,
               P_ABS_ATTRIBUTE2,
               P_ABS_ATTRIBUTE3,
               P_ABS_ATTRIBUTE4,
               P_ABS_ATTRIBUTE5,
               P_ABS_ATTRIBUTE6,
               P_ABS_ATTRIBUTE7,
               P_ABS_ATTRIBUTE8,
               P_ABS_ATTRIBUTE9,
               P_ABS_ATTRIBUTE10,
               P_ABS_ATTRIBUTE11,
               P_ABS_ATTRIBUTE12,
               P_ABS_ATTRIBUTE13,
               P_ABS_ATTRIBUTE14,
               P_ABS_ATTRIBUTE15,
               P_ABS_ATTRIBUTE16,
               P_ABS_ATTRIBUTE17,
               P_ABS_ATTRIBUTE18,
               P_ABS_ATTRIBUTE19,
               P_ABS_ATTRIBUTE20,
               P_ABS_ATTRIBUTE21,
               P_ABS_ATTRIBUTE22,
               P_ABS_ATTRIBUTE23,
               P_ABS_ATTRIBUTE24,
               P_ABS_ATTRIBUTE25,
               P_ABS_ATTRIBUTE26,
               P_ABS_ATTRIBUTE27,
               P_ABS_ATTRIBUTE28,
               P_ABS_ATTRIBUTE29,
               P_ABS_ATTRIBUTE30);



         --Create Transaction
         HR_TRANSACTION_SWI.CREATE_TRANSACTION (
            P_ASSIGNMENT_ID                => REQUEST_REC.ASSIGNMENT_ID,
            P_CREATOR_PERSON_ID            => REQUEST_REC.PERSON_ID,
            P_SELECTED_PERSON_ID           => REQUEST_REC.PERSON_ID,
            P_STATUS                       => P_STATUS,        -- status check
            P_TRANSACTION_EFFECTIVE_DATE   => TRUNC (SYSDATE),
            P_TRANSACTION_ID               => V_TRANSACTION_ID,
            P_FUNCTION_ID                  => 56643,
            P_TRANSACTION_PRIVILEGE        => 'PRIVATE',
            P_TRANSACTION_REF_ID           => V_ABSENCE_ATTENDANCE_ID,
            P_PRODUCT_CODE                 => 'PER',
            P_TRANSACTION_REF_TABLE        => 'PER_ABSENCE_ATTENDANCES',
            P_TRANSACTION_GROUP            => 'ABSENCE_MGMT',
            P_TRANSACTION_IDENTIFIER       => 'ABSENCES',
            P_TRANSACTION_TYPE             => 'WF',
            P_ITEM_TYPE                    => 'HRSSA',
            P_ITEM_KEY                     => V_ITEM_KEY,
            P_PROCESS_NAME                 => 'MBC_LEAVE_REQUEST',  --'MBC_LOA_GENERIC_APPROVAL_PRC',
            P_RELAUNCH_FUNCTION            => 'HR_ABS_ENTRY_PAGE_SS',------------------------------------------
            P_TRANSACTION_DOCUMENT         => V_TRANSACTION_DOCUMENT);


         --Create Step
         HR_TRANSACTION_SWI.CREATE_TRANSACTION_STEP (
            P_ACTIVITY_ID             => WF_PROCESS_ACTIVITIES_S.NEXTVAL,
            P_API_NAME                => 'HR_PERSON_ABSENCE_SWI.PROCESS_API',
            P_CREATOR_PERSON_ID       => REQUEST_REC.PERSON_ID,
            P_UPDATE_PERSON_ID        => REQUEST_REC.PERSON_ID,
            P_ITEM_TYPE               => 'HRSSA',
            P_ITEM_KEY                => V_ITEM_KEY,
            P_PROCESSING_ORDER        => 0,
            P_OBJECT_TYPE             => 'ENTITY',
            P_OBJECT_NAME             => 'oracle.apps.per.schema.server.PerAbsenceAttendancesEO',
            P_OBJECT_IDENTIFIER       => '000100000005C408334F40',
            P_PK1                     => V_ABSENCE_ATTENDANCE_ID,
            P_OBJECT_STATE            => '0',
            P_TRANSACTION_ID          => V_TRANSACTION_ID,
            P_TRANSACTION_STEP_ID     => V_TRANSACTION_STEP_ID,
            P_OBJECT_VERSION_NUMBER   => V_OBJECT_VERSION_NUMBER,
            P_INFORMATION1            => TO_CHAR (P_DATE_START, 'YYYY-MM-DD'),
            P_INFORMATION2            => TO_CHAR (P_DATE_END, 'YYYY-MM-DD'),
            P_INFORMATION5            => P_ABSENCE_ATTENDANCE_TYPE_ID,
            P_INFORMATION6            => P_ABSENCE_TYPE_NAME,
            P_INFORMATION8            => P_ABSENCE_DAYS, --TO BE CHANGED AFTER WE CAN CALL BG_ABSENCE_DURATION  -- Done
            P_INFORMATION9            => 'CONFIRMED',
            P_INFORMATION30           => 'ATT');


         --Submit  Workflow


         HR_APPROVAL_SS.STARTGENERICAPPROVALPROCESS (
            V_TRANSACTION_ID,
            V_ITEM_KEY,
            V_NTFSUBMSG,
            'HR_RELAUNCH_SS',
            VT_ADDITIONAL_WF_ATTRIBUTES,
            V_STATUS,
            V_ERROR_MESSAGE,
            V_ERRSTACK);

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_OAF_EDIT_URL_ATTR',
            TEXT_VALUE     => 'HR_RELAUNCH_SS',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         -- set HR_OAF_NAVIGATION_ATTR
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_OAF_NAVIGATION_ATTR',
            TEXT_VALUE     => 'N',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         -- set HR_REVIEW_TEMPLATE_RN_ATTR
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_REVIEW_TEMPLATE_RN_ATTR',
            TEXT_VALUE     => V_REVIEW_TEMPLATE_RN,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_NTF_ATTACHMENTS_ATTR',
            TEXT_VALUE     => V_NTF_ATTACH_ATTR, ------------------------------------------
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_RUNTIME_APPROVAL_REQ_FLAG',
            TEXT_VALUE     => V_APPROVAL_REQUIRED,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         -- set AME params
         -- 'HR_AME_APP_ID_ATTR'
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_AME_APP_ID_ATTR',
            TEXT_VALUE     => NULL,
            NUMBER_VALUE   => V_AMETRANAPPID,
            DATE_VALUE     => NULL);
         -- 'HR_AME_TRAN_TYPE_ATTR'
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_AME_TRAN_TYPE_ATTR',
            TEXT_VALUE     => V_AMETRANSTYPE,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         -- TRANSACTION_ID
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'TRANSACTION_ID',
            TEXT_VALUE     => NULL,
            NUMBER_VALUE   => V_TRANSACTION_ID,
            DATE_VALUE     => NULL);

         -- TRAN_SUBMIT
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'TRAN_SUBMIT',
            TEXT_VALUE     => 'Y',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         -- PROCESS_DISPLAY_NAME
         FND_MESSAGE.SET_NAME ('PER', V_NTFSUBMSG);    -- change the hardcoded

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'PROCESS_DISPLAY_NAME',
            TEXT_VALUE     => FND_MESSAGE.GET,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_PERZ_FUNCTION_NAME_ATTR',
            TEXT_VALUE     => V_PERZ_FUNC,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_PERZ_LOCALIZATION_CODE_ATTR',
            TEXT_VALUE     => REQUEST_REC.LEGISLATION_CODE,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_PERZ_ORGANIZATION_ID_ATTR',
            TEXT_VALUE     => REQUEST_REC.BUSINESS_GROUP_ID,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
            
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'FYI_NTF_DETAILS',
            TEXT_VALUE     => 'Y',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
            
            

         WF_ENGINE.COMPLETEACTIVITY (
            ITEMTYPE   => V_ITEM_TYPE,
            ITEMKEY    => V_ITEM_KEY,
            ACTIVITY   => 'HR_GENERIC_APPROVAL_BLOCK',
            RESULT     => WF_ENGINE.ENG_NULL);
      END LOOP;

      RETURN V_ITEM_KEY;
   EXCEPTION
      WHEN OTHERS
      THEN
         --NULL;
         DBMS_OUTPUT.PUT_LINE (SQLERRM);
         RETURN -1;
   END START_WORKFLOW;



   PROCEDURE CALL_FORMULA (
      P_FORMULA_NAME                 IN     VARCHAR2 DEFAULT 'BG_ABSENCE_DURATION',
      P_BUSINESS_GROUP_ID            IN     NUMBER,
      P_ASSIGNMENT_ID                IN     NUMBER,
      P_CALCULATION_DATE             IN     DATE,
      P_DAYS_OR_HOURS                IN     VARCHAR2,
      P_DATE_START                   IN     DATE,
      P_DATE_END                     IN     DATE,
      P_ABS_INFORMATION1             IN     VARCHAR2,
      P_TIME_START                   IN     VARCHAR2,
      P_TIME_END                     IN     VARCHAR2,
      P_ABSENCE_ATTENDANCE_TYPE_ID   IN     NUMBER,
      P_LANGUAGE                     IN     VARCHAR2,
      P_DURATION                        OUT VARCHAR2,
      P_INVALID_MSG                     OUT VARCHAR2)
   IS
     
      L_FORMULA_ID               NUMBER := NULL;
      L_INPUTS                   FF_EXEC.INPUTS_T;
      L_OUTPUTS                  FF_EXEC.OUTPUTS_T;
      
      L_NEW_SESSION_ID           NUMBER := NULL;
      L_NLS_LANGUAGE             VARCHAR2 (100) := NULL;
      L_NLS_DATE_FORMAT          VARCHAR2 (100) := NULL;
      L_NLS_DATE_LANGUAGE        VARCHAR2 (100) := NULL;
      L_NLS_NUMERIC_CHARACTERS   VARCHAR2 (100) := NULL;
      L_NLS_SORT                 VARCHAR2 (100) := NULL;
      L_NLS_TERRITORY            VARCHAR2 (100) := NULL;
   BEGIN
      SELECT FFF.FORMULA_ID
        INTO L_FORMULA_ID
        FROM FF_FORMULAS_F FFF
       WHERE     FFF.FORMULA_NAME = P_FORMULA_NAME
             AND (   FFF.BUSINESS_GROUP_ID = P_BUSINESS_GROUP_ID
                  OR FFF.BUSINESS_GROUP_ID IS NULL)
             AND TRUNC (SYSDATE) BETWEEN FFF.EFFECTIVE_START_DATE
                                     AND FFF.EFFECTIVE_END_DATE;



      HR_UTILITY.SET_LOCATION ('Call Formula', 5);

      -- Initialise the formula.
      FF_EXEC.INIT_FORMULA (L_FORMULA_ID,
                            P_CALCULATION_DATE,
                            L_INPUTS,
                            L_OUTPUTS);



      FOR L_IN_CNT IN L_INPUTS.FIRST .. L_INPUTS.LAST
      LOOP
         --DBMS_OUTPUT.put_line(l_inputs(l_in_cnt).name );
         IF (L_INPUTS (L_IN_CNT).NAME = 'DAYS_OR_HOURS')
         THEN
            L_INPUTS (L_IN_CNT).VALUE := P_DAYS_OR_HOURS;
         -- no conversion required. Text type and ALready Null Value
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'DATE_START')
         THEN
            L_INPUTS (L_IN_CNT).VALUE :=
               FND_DATE.DATE_TO_CANONICAL (P_DATE_START);
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'DATE_END')
         THEN
            L_INPUTS (L_IN_CNT).VALUE :=
               FND_DATE.DATE_TO_CANONICAL (P_DATE_END);
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'TIME_START')
         THEN
            L_INPUTS (L_IN_CNT).VALUE := P_TIME_START;
         -- no conversion required. Text type and ALready Null Value
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'TIME_END')
         THEN
            L_INPUTS (L_IN_CNT).VALUE := P_TIME_END;
         -- no conversion required. Text type and ALready Null Value
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'ABSENCE_ATTENDANCE_TYPE_ID')
         THEN
            L_INPUTS (L_IN_CNT).VALUE :=
               FND_NUMBER.NUMBER_TO_CANONICAL (P_ABSENCE_ATTENDANCE_TYPE_ID);
         ----- CONTEXT PARAMETERS -------
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'DATE_EARNED')
         THEN
            -- Deal with input2 value. Note(DATE CONTEXT VARIABLES HAVE TO BE CONVERTED)
            --l_inputs(l_in_cnt).value := l_effective_date;
            L_INPUTS (L_IN_CNT).VALUE :=
               FND_DATE.DATE_TO_CANONICAL (P_CALCULATION_DATE);
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'BUSINESS_GROUP_ID')
         THEN
            -- Deal with the ASSIGNMENT_ID context value.
            L_INPUTS (L_IN_CNT).VALUE := P_BUSINESS_GROUP_ID;
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'ASSIGNMENT_ID')
         THEN
            -- Deal with the ASSIGNMENT_ID context value.
            L_INPUTS (L_IN_CNT).VALUE := P_ASSIGNMENT_ID;
         ELSIF (L_INPUTS (L_IN_CNT).NAME = 'ABS_INFORMATION1')
         THEN
            -- Deal with the ASSIGNMENT_ID context value.
            --HALF DAY
            L_INPUTS (L_IN_CNT).VALUE := UPPER(P_ABS_INFORMATION1);
         END IF;
      END LOOP;

      
      ----- END OF LOOP TO CHECK INITIALIZED INPUT VALUES      ---
      FF_EXEC.RUN_FORMULA (L_INPUTS, L_OUTPUTS);

      -- Now we have executed the formula. We are able
      -- to display the results.
      FOR L_OUT_CNT IN L_OUTPUTS.FIRST .. L_OUTPUTS.LAST
      LOOP

         ----------------------------------------------
         IF (L_OUTPUTS (L_OUT_CNT).NAME = 'DURATION')
         THEN
            P_DURATION := L_OUTPUTS (L_OUT_CNT).VALUE;
         --- RETURNS 'FAILED' OR  DURATION
         ELSIF (L_OUTPUTS (L_OUT_CNT).NAME = 'INVALID_MSG')
         THEN
            P_INVALID_MSG := L_OUTPUTS (L_OUT_CNT).VALUE;
         END IF;
      END LOOP;

      HR_UTILITY.SET_LOCATION ('Leaving Call Formula', 10);

   EXCEPTION
      WHEN OTHERS
      THEN
         P_INVALID_MSG := SUBSTR (SQLERRM, 1, 3000);
         P_DURATION := 'FAILED';
   END CALL_FORMULA;

   FUNCTION GET_LEAVE_APPROVER_LIST (P_TRANSACTION_ID IN VARCHAR2)
      RETURN MBC_AME_LEAVE_APPROVERS_SS
   IS
      P_APPLICATION_ID               NUMBER := 800;
      V_TRANSACTION_TYPE             VARCHAR2 (32767) := 'SSHRMS';
      V_APPRS_VIEW_TYPE              VARCHAR2 (32767) := 'Active';
      V_COA_INSERTIONS_FLAG          VARCHAR2 (32767) := 'N';
      V_AME_APPROVERS_LIST           AME_APPROVER_RECORD2_TABLE_SS;
      V_AME_ORDER_TYPE_LIST          AME_INSERTION_RECORD2_TABLE_SS;
      V_ALL_APPROVERS_COUNT          VARCHAR2 (32767);
      V_WARNING_MSG_NAME             VARCHAR2 (32767);
      V_ERROR_MSG_TEXT               VARCHAR2 (32767);
      V_MBC_AME_LEAVE_APPROVERS_SS   MBC_AME_LEAVE_APPROVERS_SS;
   BEGIN
      AME_DYNAMIC_APPROVAL_PKG.GET_AME_APPRS_AND_INS_LIST (
         P_APPLICATION_ID,
         V_TRANSACTION_TYPE,
         P_TRANSACTION_ID,
         V_APPRS_VIEW_TYPE,
         V_COA_INSERTIONS_FLAG,
         V_AME_APPROVERS_LIST,
         V_AME_ORDER_TYPE_LIST,
         V_ALL_APPROVERS_COUNT,
         V_WARNING_MSG_NAME,
         V_ERROR_MSG_TEXT);



      SELECT MBC_AME_APPROVERS_OBJ (ROWNUM,
                                    USERS.MOBILE_NAME,
                                    USER_ID,
                                    PERSON_ID,
                                    JOB_TITLE,
                                    USERS.MOBILE_NAME,
                                    INTIALS,
                                    IMGX)
        BULK COLLECT INTO V_MBC_AME_LEAVE_APPROVERS_SS
        FROM TABLE (V_AME_APPROVERS_LIST) APPR,
             (SELECT  FU.USER_NAME ,
                     FU.USER_ID,
                        SUBSTR (PAPF.LAST_NAME, 1, 1)
                     || SUBSTR (PAPF.FIRST_NAME, 1, 1)
                        INTIALS,
                        PAPF.first_name ||' '|| PAPF.last_name MOBILE_NAME,
                     MBC_MOBILE_PKG.BASE64ENCODE (IMAGE) IMGX,
                     PAPF.PERSON_ID,
                     SUBSTR (POS.NAME, 1, INSTR (POS.NAME, '.') - 1)
                        JOB_TITLE
                FROM FND_USER FU,
                     PER_ALL_PEOPLE_F PAPF,
                     PER_ALL_ASSIGNMENTS_F PAAF,
                     PER_POSITIONS POS,
                     PER_IMAGES PI
               WHERE     PAPF.PERSON_ID = PAAF.PERSON_ID
                     AND FU.EMPLOYEE_ID = PAPF.PERSON_ID
                     AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                     AND PAPF.EFFECTIVE_END_DATE
                     AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE
                                     AND PAAF.EFFECTIVE_END_DATE
                     AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
                     AND PAAF.PRIMARY_FLAG = 'Y'
                     AND PAAF.POSITION_ID = POS.POSITION_ID(+)
                     AND PI.PARENT_ID(+) = PAAF.PERSON_ID
                     AND TABLE_NAME(+) = 'PER_PEOPLE_F') USERS
       WHERE     APPROVER_CATEGORY = 'Approver'
             AND APPROVAL_STATUS IS NULL
             AND USERS.USER_NAME = APPR.NAME
             ORDER BY ROWNUM ;


      RETURN V_MBC_AME_LEAVE_APPROVERS_SS;
   END;

   FUNCTION GET_APPROVER_LIST (
      P_APPLICATION_ID        IN NUMBER,
      P_TRANSACTION_TYPE      IN VARCHAR2 := 'SSHRMS',
      P_TRANSACTION_ID        IN VARCHAR2,
      P_APPRS_VIEW_TYPE       IN VARCHAR2 DEFAULT 'Active',
      P_COA_INSERTIONS_FLAG   IN VARCHAR2 DEFAULT 'N')
      RETURN AME_APPROVERS_SS
   IS
      V_COUNTER                   NUMBER := 0;
      V_USER_ID                   NUMBER;
      V_PERSON_ID                 NUMBER;
      V_JOB_TITLE                 VARCHAR2 (500);

      V_AME_APPROVER_RECORD2_SS   AME_APPROVER_RECORD2_SS;
      V_AME_ORDER_TYPE_LIST       AME_INSERTION_RECORD2_TABLE_SS;
      V_AME_APPROVERS_LIST        AME_APPROVER_RECORD2_TABLE_SS;
      V_ALL_APPROVERS_COUNT       VARCHAR2 (4000);
      V_WARNING_MSG_NAME          VARCHAR2 (4000);
      V_ERROR_MSG_TEXT            VARCHAR2 (4000);


      V_AME_APPROVERS_SS          AME_APPROVERS_SS;
   BEGIN
      AME_DYNAMIC_APPROVAL_PKG.GET_AME_APPRS_AND_INS_LIST (
         P_APPLICATION_ID        => P_APPLICATION_ID,
         P_TRANSACTION_TYPE      => P_TRANSACTION_TYPE,
         P_TRANSACTION_ID        => P_TRANSACTION_ID,
         P_APPRS_VIEW_TYPE       => P_APPRS_VIEW_TYPE,
         P_COA_INSERTIONS_FLAG   => P_COA_INSERTIONS_FLAG,
         P_AME_APPROVERS_LIST    => V_AME_APPROVERS_LIST,
         P_AME_ORDER_TYPE_LIST   => V_AME_ORDER_TYPE_LIST,
         P_ALL_APPROVERS_COUNT   => V_ALL_APPROVERS_COUNT,
         P_WARNING_MSG_NAME      => V_WARNING_MSG_NAME,
         P_ERROR_MSG_TEXT        => V_ERROR_MSG_TEXT);

      FOR INDX IN V_AME_APPROVERS_LIST.FIRST .. V_AME_APPROVERS_LIST.LAST
      LOOP
         V_AME_APPROVER_RECORD2_SS := V_AME_APPROVERS_LIST (INDX);

         IF     V_AME_APPROVER_RECORD2_SS.APPROVER_CATEGORY = 'Approver'
            AND V_AME_APPROVER_RECORD2_SS.APPROVAL_STATUS IS NULL
         THEN                                -- != 'Notified By Repeated' THEN
            BEGIN
               SELECT FU.USER_ID,
                      PAPF.PERSON_ID,
                      SUBSTR (POS.NAME, 1, INSTR (POS.NAME, '.') - 1)
                 INTO V_USER_ID, V_PERSON_ID, V_JOB_TITLE
                 FROM FND_USER FU,
                      PER_ALL_PEOPLE_F PAPF,
                      PER_ALL_ASSIGNMENTS_F PAAF,
                      PER_POSITIONS POS
                WHERE     PAPF.PERSON_ID = PAAF.PERSON_ID
                      AND FU.EMPLOYEE_ID = PAPF.PERSON_ID
                      AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                      AND PAPF.EFFECTIVE_END_DATE
                      AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE
                                      AND PAAF.EFFECTIVE_END_DATE
                      AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
                      AND PAAF.PRIMARY_FLAG = 'Y'
                      AND PAAF.POSITION_ID = POS.POSITION_ID(+)
                      AND FU.USER_NAME = V_AME_APPROVER_RECORD2_SS.NAME;
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_USER_ID := NULL;
                  V_PERSON_ID := NULL;
                  V_JOB_TITLE := NULL;
            END;


            V_COUNTER := V_COUNTER + 1;
            V_AME_APPROVERS_SS (V_COUNTER).LINE_NO := V_COUNTER;
            V_AME_APPROVERS_SS (V_COUNTER).USER_NAME :=
               V_AME_APPROVER_RECORD2_SS.NAME;
            V_AME_APPROVERS_SS (V_COUNTER).USER_ID := V_USER_ID;
            V_AME_APPROVERS_SS (V_COUNTER).PERSON_ID := V_PERSON_ID;
            V_AME_APPROVERS_SS (V_COUNTER).JOB_TITLE := V_JOB_TITLE;
            V_AME_APPROVERS_SS (V_COUNTER).DISPLAY_NAME :=
               V_AME_APPROVER_RECORD2_SS.DISPLAY_NAME;
         END IF;
      END LOOP;

      RETURN V_AME_APPROVERS_SS;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN V_AME_APPROVERS_SS;
   END GET_APPROVER_LIST;

   FUNCTION NEW_LEAVE_REQUEST (P_USER_ID                         NUMBER,
                               P_ASSIGNMENT_ID                   NUMBER,
                               P_BUSINESS_GROUP_ID               NUMBER,
                               P_DATE_START                      DATE,
                               P_DATE_END                        DATE,
                               P_ABSENCE_ATTENDANCE_TYPE_ID      NUMBER,
                               P_TRANSACTION_ID                  NUMBER,
                               P_TRANSACTION_STEP_ID             NUMBER,
                               P_ITEM_KEY                        NUMBER,
                               P_ABSENCE_ATTENDANCE_ID           NUMBER,
                               P_PERSON_ID                       NUMBER,
                               P_REPLACEMENT_PERSON_ID           NUMBER,
                               P_ABSENCE_TYPE_NAME               VARCHAR2,
                               P_STATUS                          VARCHAR2,
                               P_ATTACH_ID                       NUMBER,
                               P_ATTRIBUTE1                      VARCHAR2,
                               P_ATTRIBUTE2                      VARCHAR2,
                               P_ATTRIBUTE3                      VARCHAR2,
                               P_ABS_ATTRIBUTE1                      VARCHAR2, -- halfday
                               P_ATTRIBUTE5                      VARCHAR2,
                               P_ATTRIBUTE6                      VARCHAR2,
                               P_ATTRIBUTE7                   IN VARCHAR2,
                               P_ATTRIBUTE8                   IN VARCHAR2,
                               P_DURATION                     OUT VARCHAR2)
      RETURN VARCHAR2
   IS
      V_DURATION                    VARCHAR2 (50);
      V_INVALID_MSG                 VARCHAR2 (32767);
      V_ATTRIBUTE7                  VARCHAR2 (32767);
      V_ATTRIBUTE8                  VARCHAR2 (32767);
      V_ATTRIBUTE9                  VARCHAR2 (32767);
      V_ATTRIBUTE10                 VARCHAR2 (32767);
      V_ATTRIBUTE11                 VARCHAR2 (32767);
      V_ATTRIBUTE12                 VARCHAR2 (32767);
      V_ATTRIBUTE13                 VARCHAR2 (32767);
      V_ATTRIBUTE14                 VARCHAR2 (32767);
      V_ATTRIBUTE15                 VARCHAR2 (32767);
      V_ATTRIBUTE16                 VARCHAR2 (32767);
      V_ATTRIBUTE17                 VARCHAR2 (32767);
      V_ATTRIBUTE18                 VARCHAR2 (32767);
      V_ATTRIBUTE19                 VARCHAR2 (32767);
      V_ATTRIBUTE20                 VARCHAR2 (32767);
      V_ATTRIBUTE21                 VARCHAR2 (32767);
      V_ATTRIBUTE22                 VARCHAR2 (32767);
      V_ATTRIBUTE23                 VARCHAR2 (32767);
      V_ATTRIBUTE24                 VARCHAR2 (32767);
      V_ATTRIBUTE25                 VARCHAR2 (32767);
      V_ATTRIBUTE26                 VARCHAR2 (32767);
      V_ATTRIBUTE27                 VARCHAR2 (32767);
      V_ATTRIBUTE28                 VARCHAR2 (32767);
      V_ATTRIBUTE29                 VARCHAR2 (32767);
      V_ATTRIBUTE30                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE2                  VARCHAR2 (32767);
      V_ABS_ATTRIBUTE3                  VARCHAR2 (32767);
      V_ABS_ATTRIBUTE4                  VARCHAR2 (32767);
      V_ABS_ATTRIBUTE5                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE6                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE7                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE8                 VARCHAR2 (32767);    
      V_ABS_ATTRIBUTE9                  VARCHAR2 (32767);
      V_ABS_ATTRIBUTE10                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE11                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE12                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE13                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE14                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE15                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE16                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE17                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE18                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE19                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE20                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE21                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE22                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE23                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE24                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE25                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE26                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE27                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE28                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE29                 VARCHAR2 (32767);
      V_ABS_ATTRIBUTE30                 VARCHAR2 (32767);
      V_ITEM_KEY_OUT                NUMBER;
      V_ATTRIBUTE_CATEGORY          VARCHAR2 (1000) := NULL;
      V_EXCEEDS_PTO_ENTIT_WARNING   BOOLEAN := FALSE;
      V_EXCEEDS_RUN_TOTAL_WARNING   BOOLEAN := FALSE;
      V_ABS_OVERLAP_WARNING         BOOLEAN := TRUE;
      V_ABS_DAY_AFTER_WARNING       BOOLEAN := TRUE;
      V_DUR_OVERWRITTEN_WARNING     BOOLEAN := FALSE;
      V_HOOK_OUTPUT                 VARCHAR2 (1000);
      V_ATTCHED_IMAGE               BLOB;
   BEGIN
   P_DURATION:='0';
 --raise_application_error (-20001 , 'val is '||P_ABS_ATTRIBUTE1);
      -- Step 0 Clear messages
      FND_MESSAGE.CLEAR ();

      -- Step 1 Intialize session
      APPS.MBC_MOBILE_PKG.INITIALIZE_SESSION (P_USER_ID);

      --  STEP 2 CALL BG_ABSENCE_DURATION
      APPS.MBC_MOBILE_PKG.CALL_FORMULA (
         P_FORMULA_NAME                 => 'BG_ABSENCE_DURATION',
         P_BUSINESS_GROUP_ID            => P_BUSINESS_GROUP_ID,
         P_ASSIGNMENT_ID                => P_ASSIGNMENT_ID,
         P_CALCULATION_DATE             => P_DATE_START,     -- TO BE REVIEWED
         P_ABS_INFORMATION1             => P_ABS_ATTRIBUTE1,  --HALF_DAY
         P_DAYS_OR_HOURS                => 'DAYS',
         P_DATE_START                   => P_DATE_START,
         P_DATE_END                     => P_DATE_END,
         P_TIME_START                   => NULL,
         P_TIME_END                     => NULL,
         P_ABSENCE_ATTENDANCE_TYPE_ID   => P_ABSENCE_ATTENDANCE_TYPE_ID,
         P_LANGUAGE                     => 'US',
         P_DURATION                     => V_DURATION,
         P_INVALID_MSG                  => V_INVALID_MSG);

      IF (V_DURATION = 'FAILED')
      THEN
         RETURN V_INVALID_MSG;
      END IF;
      P_DURATION:=V_DURATION;
      -- Step 3 Attachment
      --   raise_application_error(-20001,'px'||P_PERSON_ID || 'attch'||p_attach_id);
      IF P_ATTACH_ID IS NOT NULL
      THEN
         FOR I
            IN (SELECT DATA_FILE, ID
                  --into V_ATTCHED_IMAGE
                  FROM MBC_MOBILE_SSHR_BLOB_DATA
                 WHERE PERSON_ID = P_PERSON_ID AND ATTACH_ID = P_ATTACH_ID)
         LOOP
            APPS.MBC_MOBILE_PKG.ADD_ATTACHEMENT (
               P_ABSENCE_ATTENDENCE_ID   => P_ABSENCE_ATTENDANCE_ID,
               P_TRANSACTION_ID          => P_TRANSACTION_ID,
               P_FILE_NAME               =>    'AttachedImage'
                                            || P_PERSON_ID
                                            || P_ATTACH_ID
                                            || I.ID
                                            || '.png',
               P_FILE_CONTENT_TYPE       => 'image/png',
               P_FILE_CONTENT            => I.DATA_FILE,
               P_USER_ID                 => P_USER_ID);
         END LOOP;
      END IF;

      -- Step 4 CALL USER HOOKS
      V_HOOK_OUTPUT :=
         APPS.MBC_MOBILE_PKG.CALL_HOOKS (SYSDATE,
                                         P_PERSON_ID,
                                         P_BUSINESS_GROUP_ID,
                                         P_ABSENCE_ATTENDANCE_TYPE_ID,
                                         P_DATE_START,
                                         P_DATE_END,
                                         TO_NUMBER (V_DURATION),
                                         P_PERSON_ID,
                                         P_REPLACEMENT_PERSON_ID,
                                         V_ATTRIBUTE_CATEGORY,
                                         P_ATTRIBUTE1,
                                         P_ATTRIBUTE2,
                                         P_ATTRIBUTE3,
                                         P_ABS_ATTRIBUTE1,
                                         P_ATTRIBUTE5,
                                         P_ATTRIBUTE6,
                                         P_ATTRIBUTE7,
                                         P_ATTRIBUTE8,
                                         P_ABSENCE_ATTENDANCE_ID,
                                         V_EXCEEDS_PTO_ENTIT_WARNING,
                                         V_EXCEEDS_RUN_TOTAL_WARNING,
                                         V_ABS_OVERLAP_WARNING,
                                         V_ABS_DAY_AFTER_WARNING,
                                         V_DUR_OVERWRITTEN_WARNING);

  
  IF V_HOOK_OUTPUT != 'SUCCESS'
      THEN
         RETURN V_HOOK_OUTPUT;
      END IF;

  
      --  STEP 5 CALL WORKFLOW
      V_ITEM_KEY_OUT :=
         APPS.MBC_MOBILE_PKG.START_WORKFLOW (P_PERSON_ID,
                                             P_BUSINESS_GROUP_ID,
                                             P_ABSENCE_ATTENDANCE_TYPE_ID,
                                             P_REPLACEMENT_PERSON_ID,
                                             P_ABSENCE_TYPE_NAME,
                                             P_DATE_START,
                                             P_DATE_END,
                                             V_DURATION,
                                             P_TRANSACTION_ID,
                                             P_TRANSACTION_STEP_ID,
                                             P_ITEM_KEY,
                                             P_ABSENCE_ATTENDANCE_ID,
                                             P_STATUS,
                                             P_ATTRIBUTE1,
                                             P_ATTRIBUTE2,
                                             P_ATTRIBUTE3,
                                             null,
                                             P_ATTRIBUTE5,
                                             P_ATTRIBUTE6,
                                             P_ATTRIBUTE7,
                                             P_ATTRIBUTE8,
                                             V_ATTRIBUTE9,
                                             V_ATTRIBUTE10,
                                             V_ATTRIBUTE11,
                                             V_ATTRIBUTE12,
                                             V_ATTRIBUTE13,
                                             V_ATTRIBUTE14,
                                             V_ATTRIBUTE15,
                                             V_ATTRIBUTE16,
                                             V_ATTRIBUTE17,
                                             V_ATTRIBUTE18,
                                             V_ATTRIBUTE19,
                                             V_ATTRIBUTE20,
                                             V_ATTRIBUTE21,
                                             V_ATTRIBUTE22,
                                             V_ATTRIBUTE23,
                                             V_ATTRIBUTE24,
                                             V_ATTRIBUTE25,
                                             V_ATTRIBUTE26,
                                             V_ATTRIBUTE27,
                                             V_ATTRIBUTE28,
                                             V_ATTRIBUTE29,
                                             V_ATTRIBUTE30,
                                             P_ABS_ATTRIBUTE1,
                                             V_ABS_ATTRIBUTE2,
                                              V_ABS_ATTRIBUTE3,
                                              V_ABS_ATTRIBUTE4,
                                              V_ABS_ATTRIBUTE5,
                                              V_ABS_ATTRIBUTE6,
                                              V_ABS_ATTRIBUTE7,
                                              V_ABS_ATTRIBUTE8, 
                                              V_ABS_ATTRIBUTE9,
                                              V_ABS_ATTRIBUTE10,
                                              V_ABS_ATTRIBUTE11,
                                              V_ABS_ATTRIBUTE12,
                                              V_ABS_ATTRIBUTE13,
                                              V_ABS_ATTRIBUTE14,
                                              V_ABS_ATTRIBUTE15,
                                              V_ABS_ATTRIBUTE16,
                                              V_ABS_ATTRIBUTE17,
                                              V_ABS_ATTRIBUTE18,
                                              V_ABS_ATTRIBUTE19,
                                              V_ABS_ATTRIBUTE20,
                                              V_ABS_ATTRIBUTE21,
                                              V_ABS_ATTRIBUTE22,
                                              V_ABS_ATTRIBUTE23,
                                              V_ABS_ATTRIBUTE24,
                                              V_ABS_ATTRIBUTE25,
                                              V_ABS_ATTRIBUTE26,
                                              V_ABS_ATTRIBUTE27,
                                              V_ABS_ATTRIBUTE28,
                                              V_ABS_ATTRIBUTE29,
                                              V_ABS_ATTRIBUTE30);

      RETURN 'SUCCESS';
      EXCEPTION WHEN OTHERS
       THEN RETURN SQLERRM;
   END NEW_LEAVE_REQUEST;


   FUNCTION CALL_HOOKS (P_EFFECTIVE_DATE               IN DATE,
                        P_PERSON_ID                    IN NUMBER,
                        P_BUSINESS_GROUP_ID            IN NUMBER,
                        P_ABSENCE_ATTENDANCE_TYPE_ID   IN NUMBER,
                        P_DATE_START                   IN DATE,
                        P_DATE_END                     IN DATE,
                        P_ABSENCE_DAYS                 IN NUMBER,
                        P_AUTHORISING_PERSON_ID        IN NUMBER,
                        P_REPLACEMENT_PERSON_ID        IN NUMBER,
                        P_ATTRIBUTE_CATEGORY           IN VARCHAR2,
                        P_ATTRIBUTE1                   IN VARCHAR2,
                        P_ATTRIBUTE2                   IN VARCHAR2,
                        P_ATTRIBUTE3                   IN VARCHAR2,
                        P_ATTRIBUTE4                   IN VARCHAR2,
                        P_ATTRIBUTE5                   IN VARCHAR2,
                        P_ATTRIBUTE6                   IN VARCHAR2,
                        P_ATTRIBUTE7                   IN VARCHAR2,
                        P_ATTRIBUTE8                   IN VARCHAR2,
                        P_ABSENCE_ATTENDANCE_ID        IN NUMBER,
                        P_EXCEEDS_PTO_ENTIT_WARNING    IN BOOLEAN,
                        P_EXCEEDS_RUN_TOTAL_WARNING    IN BOOLEAN,
                        P_ABS_OVERLAP_WARNING          IN BOOLEAN,
                        P_ABS_DAY_AFTER_WARNING        IN BOOLEAN,
                        P_DUR_OVERWRITTEN_WARNING      IN BOOLEAN)
      RETURN VARCHAR2
   IS
      V_EFFECTIVE_DATE                 DATE;
      V_PERSON_ID                      NUMBER;
      V_BUSINESS_GROUP_ID              NUMBER;
      V_ABSENCE_ATTENDANCE_TYPE_ID     NUMBER;
      V_ABS_ATTENDANCE_REASON_ID       NUMBER;
      V_COMMENTS                       CLOB;
      V_DATE_NOTIFICATION              DATE;
      V_DATE_PROJECTED_START           DATE;
      V_TIME_PROJECTED_START           VARCHAR2 (32767);
      V_DATE_PROJECTED_END             DATE;
      V_TIME_PROJECTED_END             VARCHAR2 (32767);
      V_DATE_START                     DATE;
      V_TIME_START                     VARCHAR2 (32767);
      V_DATE_END                       DATE;
      V_TIME_END                       VARCHAR2 (32767);
      V_ABSENCE_DAYS                   NUMBER;
      V_ABSENCE_HOURS                  NUMBER;
      V_AUTHORISING_PERSON_ID          NUMBER;
      V_REPLACEMENT_PERSON_ID          NUMBER;
      V_ATTRIBUTE_CATEGORY             VARCHAR2 (32767);
      V_ATTRIBUTE1                     VARCHAR2 (32767);
      V_ATTRIBUTE2                     VARCHAR2 (32767);
      V_ATTRIBUTE3                     VARCHAR2 (32767);
      V_ATTRIBUTE4                     VARCHAR2 (32767);
      V_ATTRIBUTE5                     VARCHAR2 (32767);
      V_ATTRIBUTE6                     VARCHAR2 (32767);
      V_ATTRIBUTE7                     VARCHAR2 (32767);
      V_ATTRIBUTE8                     VARCHAR2 (32767);
      V_ATTRIBUTE9                     VARCHAR2 (32767);
      V_ATTRIBUTE10                    VARCHAR2 (32767);
      V_ATTRIBUTE11                    VARCHAR2 (32767);
      V_ATTRIBUTE12                    VARCHAR2 (32767);
      V_ATTRIBUTE13                    VARCHAR2 (32767);
      V_ATTRIBUTE14                    VARCHAR2 (32767);
      V_ATTRIBUTE15                    VARCHAR2 (32767);
      V_ATTRIBUTE16                    VARCHAR2 (32767);
      V_ATTRIBUTE17                    VARCHAR2 (32767);
      V_ATTRIBUTE18                    VARCHAR2 (32767);
      V_ATTRIBUTE19                    VARCHAR2 (32767);
      V_ATTRIBUTE20                    VARCHAR2 (32767);
      V_OCCURRENCE                     NUMBER;
      V_PERIOD_OF_INCAPACITY_ID        NUMBER;
      V_SSP1_ISSUED                    VARCHAR2 (32767);
      V_MATERNITY_ID                   NUMBER;
      V_SICKNESS_START_DATE            DATE;
      V_SICKNESS_END_DATE              DATE;
      V_PREGNANCY_RELATED_ILLNESS      VARCHAR2 (32767);
      V_REASON_FOR_NOTIFICATION_DELA   VARCHAR2 (32767);
      V_ACCEPT_LATE_NOTIFICATION_FLA   VARCHAR2 (32767);
      V_LINKED_ABSENCE_ID              NUMBER;
      V_BATCH_ID                       NUMBER;
      V_CREATE_ELEMENT_ENTRY           BOOLEAN;
      V_ABS_INFORMATION_CATEGORY       VARCHAR2 (32767);
      V_ABS_INFORMATION1               VARCHAR2 (32767);
      V_ABS_INFORMATION2               VARCHAR2 (32767);
      V_ABS_INFORMATION3               VARCHAR2 (32767);
      V_ABS_INFORMATION4               VARCHAR2 (32767);
      V_ABS_INFORMATION5               VARCHAR2 (32767);
      V_ABS_INFORMATION6               VARCHAR2 (32767);
      V_ABS_INFORMATION7               VARCHAR2 (32767);
      V_ABS_INFORMATION8               VARCHAR2 (32767);
      V_ABS_INFORMATION9               VARCHAR2 (32767);
      V_ABS_INFORMATION10              VARCHAR2 (32767);
      V_ABS_INFORMATION11              VARCHAR2 (32767);
      V_ABS_INFORMATION12              VARCHAR2 (32767);
      V_ABS_INFORMATION13              VARCHAR2 (32767);
      V_ABS_INFORMATION14              VARCHAR2 (32767);
      V_ABS_INFORMATION15              VARCHAR2 (32767);
      V_ABS_INFORMATION16              VARCHAR2 (32767);
      V_ABS_INFORMATION17              VARCHAR2 (32767);
      V_ABS_INFORMATION18              VARCHAR2 (32767);
      V_ABS_INFORMATION19              VARCHAR2 (32767);
      V_ABS_INFORMATION20              VARCHAR2 (32767);
      V_ABS_INFORMATION21              VARCHAR2 (32767);
      V_ABS_INFORMATION22              VARCHAR2 (32767);
      V_ABS_INFORMATION23              VARCHAR2 (32767);
      V_ABS_INFORMATION24              VARCHAR2 (32767);
      V_ABS_INFORMATION25              VARCHAR2 (32767);
      V_ABS_INFORMATION26              VARCHAR2 (32767);
      V_ABS_INFORMATION27              VARCHAR2 (32767);
      V_ABS_INFORMATION28              VARCHAR2 (32767);
      V_ABS_INFORMATION29              VARCHAR2 (32767);
      V_ABS_INFORMATION30              VARCHAR2 (32767);
      V_ABSENCE_CASE_ID                NUMBER;
      V_ABSENCE_ATTENDANCE_ID          NUMBER;
      V_OBJECT_VERSION_NUMBER          NUMBER;
      V_DUR_DYS_LESS_WARNING           BOOLEAN;
      V_DUR_HRS_LESS_WARNING           BOOLEAN;
      V_EXCEEDS_PTO_ENTIT_WARNING      BOOLEAN;
      V_EXCEEDS_RUN_TOTAL_WARNING      BOOLEAN;
      V_ABS_OVERLAV_WARNING            BOOLEAN;
      V_ABS_DAY_AFTER_WARNING          BOOLEAN;
      V_DUR_OVERWRITTEN_WARNING        BOOLEAN;
   BEGIN
      V_EFFECTIVE_DATE := P_EFFECTIVE_DATE;
      V_PERSON_ID := P_PERSON_ID;
      V_BUSINESS_GROUP_ID := P_BUSINESS_GROUP_ID;
      V_ABSENCE_ATTENDANCE_TYPE_ID := P_ABSENCE_ATTENDANCE_TYPE_ID;
      V_ABS_ATTENDANCE_REASON_ID := NULL;
      V_DATE_NOTIFICATION := SYSDATE;
      V_DATE_PROJECTED_START := NULL;
      V_TIME_PROJECTED_START := NULL;
      V_DATE_PROJECTED_END := NULL;
      V_TIME_PROJECTED_END := NULL;
      V_DATE_START := P_DATE_START;
      V_TIME_START := NULL;
      V_DATE_END := P_DATE_END;
      V_TIME_END := NULL;
      V_ABSENCE_DAYS := P_ABSENCE_DAYS;
      V_ABSENCE_HOURS := NULL;
      V_AUTHORISING_PERSON_ID := P_AUTHORISING_PERSON_ID;
      V_REPLACEMENT_PERSON_ID := P_REPLACEMENT_PERSON_ID;
      V_ATTRIBUTE_CATEGORY := P_ATTRIBUTE_CATEGORY;
      V_ATTRIBUTE1 := P_ATTRIBUTE1;
      V_ATTRIBUTE2 := P_ATTRIBUTE2;
      V_ATTRIBUTE3 := P_ATTRIBUTE3;
      V_ATTRIBUTE4 := P_ATTRIBUTE4;
      V_ATTRIBUTE5 := P_ATTRIBUTE5;
      V_ATTRIBUTE6 := P_ATTRIBUTE6;
      V_ATTRIBUTE7 := P_ATTRIBUTE7;
      V_ATTRIBUTE8 := P_ATTRIBUTE8;
      V_ATTRIBUTE9 := NULL;
      V_ATTRIBUTE10 := NULL;
      V_ATTRIBUTE11 := NULL;
      V_ATTRIBUTE12 := NULL;
      V_ATTRIBUTE13 := NULL;
      V_ATTRIBUTE14 := NULL;
      V_ATTRIBUTE15 := NULL;
      V_ATTRIBUTE16 := NULL;
      V_ATTRIBUTE17 := NULL;
      V_ATTRIBUTE18 := NULL;
      V_ATTRIBUTE19 := NULL;
      V_ATTRIBUTE20 := NULL;
      V_OCCURRENCE := NULL;
      V_PERIOD_OF_INCAPACITY_ID := NULL;
      V_SSP1_ISSUED := NULL;
      V_MATERNITY_ID := NULL;
      V_SICKNESS_START_DATE := NULL;
      V_SICKNESS_END_DATE := NULL;
      V_PREGNANCY_RELATED_ILLNESS := NULL;
      V_REASON_FOR_NOTIFICATION_DELA := NULL;
      V_ACCEPT_LATE_NOTIFICATION_FLA := NULL;
      V_LINKED_ABSENCE_ID := NULL;
      V_BATCH_ID := NULL;
      V_CREATE_ELEMENT_ENTRY := NULL;
      V_ABS_INFORMATION_CATEGORY := NULL;
      V_ABS_INFORMATION1 := NULL;
      V_ABS_INFORMATION2 := NULL;
      V_ABS_INFORMATION3 := NULL;
      V_ABS_INFORMATION4 := NULL;
      V_ABS_INFORMATION5 := NULL;
      V_ABS_INFORMATION6 := NULL;
      V_ABS_INFORMATION7 := NULL;
      V_ABS_INFORMATION8 := NULL;
      V_ABS_INFORMATION9 := NULL;
      V_ABS_INFORMATION10 := NULL;
      V_ABS_INFORMATION11 := NULL;
      V_ABS_INFORMATION12 := NULL;
      V_ABS_INFORMATION13 := NULL;
      V_ABS_INFORMATION14 := NULL;
      V_ABS_INFORMATION15 := NULL;
      V_ABS_INFORMATION16 := NULL;
      V_ABS_INFORMATION17 := NULL;
      V_ABS_INFORMATION18 := NULL;
      V_ABS_INFORMATION19 := NULL;
      V_ABS_INFORMATION20 := NULL;
      V_ABS_INFORMATION21 := NULL;
      V_ABS_INFORMATION22 := NULL;
      V_ABS_INFORMATION23 := NULL;
      V_ABS_INFORMATION24 := NULL;
      V_ABS_INFORMATION25 := NULL;
      V_ABS_INFORMATION26 := NULL;
      V_ABS_INFORMATION27 := NULL;
      V_ABS_INFORMATION28 := NULL;
      V_ABS_INFORMATION29 := NULL;
      V_ABS_INFORMATION30 := NULL;
      V_ABSENCE_CASE_ID := NULL;
      V_ABSENCE_ATTENDANCE_ID := P_ABSENCE_ATTENDANCE_ID;
      V_OBJECT_VERSION_NUMBER := NULL;
      V_DUR_DYS_LESS_WARNING := NULL;
      V_DUR_HRS_LESS_WARNING := NULL;
      V_EXCEEDS_PTO_ENTIT_WARNING := FALSE;
      V_EXCEEDS_RUN_TOTAL_WARNING := FALSE;
      V_ABS_OVERLAV_WARNING := FALSE;                           --------------
      V_ABS_DAY_AFTER_WARNING := TRUE;
      V_DUR_OVERWRITTEN_WARNING := FALSE;

      APPS.HR_PERSON_ABSENCE_BK1.CREATE_PERSON_ABSENCE_A (
         V_EFFECTIVE_DATE,
         V_PERSON_ID,
         V_BUSINESS_GROUP_ID,
         V_ABSENCE_ATTENDANCE_TYPE_ID,
         V_ABS_ATTENDANCE_REASON_ID,
         V_COMMENTS,
         V_DATE_NOTIFICATION,
         V_DATE_PROJECTED_START,
         V_TIME_PROJECTED_START,
         V_DATE_PROJECTED_END,
         V_TIME_PROJECTED_END,
         V_DATE_START,
         V_TIME_START,
         V_DATE_END,
         V_TIME_END,
         V_ABSENCE_DAYS,
         V_ABSENCE_HOURS,
         V_AUTHORISING_PERSON_ID,
         V_REPLACEMENT_PERSON_ID,
         V_ATTRIBUTE_CATEGORY,
         V_ATTRIBUTE1,
         V_ATTRIBUTE2,
         V_ATTRIBUTE3,
         V_ATTRIBUTE4,
         V_ATTRIBUTE5,
         V_ATTRIBUTE6,
         V_ATTRIBUTE7,
         V_ATTRIBUTE8,
         V_ATTRIBUTE9,
         V_ATTRIBUTE10,
         V_ATTRIBUTE11,
         V_ATTRIBUTE12,
         V_ATTRIBUTE13,
         V_ATTRIBUTE14,
         V_ATTRIBUTE15,
         V_ATTRIBUTE16,
         V_ATTRIBUTE17,
         V_ATTRIBUTE18,
         V_ATTRIBUTE19,
         V_ATTRIBUTE20,
         V_OCCURRENCE,
         V_PERIOD_OF_INCAPACITY_ID,
         V_SSP1_ISSUED,
         V_MATERNITY_ID,
         V_SICKNESS_START_DATE,
         V_SICKNESS_END_DATE,
         V_PREGNANCY_RELATED_ILLNESS,
         V_REASON_FOR_NOTIFICATION_DELA,
         V_ACCEPT_LATE_NOTIFICATION_FLA,
         V_LINKED_ABSENCE_ID,
         V_BATCH_ID,
         V_CREATE_ELEMENT_ENTRY,
         V_ABS_INFORMATION_CATEGORY,
         V_ABS_INFORMATION1,
         V_ABS_INFORMATION2,
         V_ABS_INFORMATION3,
         V_ABS_INFORMATION4,
         V_ABS_INFORMATION5,
         V_ABS_INFORMATION6,
         V_ABS_INFORMATION7,
         V_ABS_INFORMATION8,
         V_ABS_INFORMATION9,
         V_ABS_INFORMATION10,
         V_ABS_INFORMATION11,
         V_ABS_INFORMATION12,
         V_ABS_INFORMATION13,
         V_ABS_INFORMATION14,
         V_ABS_INFORMATION15,
         V_ABS_INFORMATION16,
         V_ABS_INFORMATION17,
         V_ABS_INFORMATION18,
         V_ABS_INFORMATION19,
         V_ABS_INFORMATION20,
         V_ABS_INFORMATION21,
         V_ABS_INFORMATION22,
         V_ABS_INFORMATION23,
         V_ABS_INFORMATION24,
         V_ABS_INFORMATION25,
         V_ABS_INFORMATION26,
         V_ABS_INFORMATION27,
         V_ABS_INFORMATION28,
         V_ABS_INFORMATION29,
         V_ABS_INFORMATION30,
         V_ABSENCE_CASE_ID,
         V_ABSENCE_ATTENDANCE_ID,
         V_OBJECT_VERSION_NUMBER,
         V_DUR_DYS_LESS_WARNING,
         V_DUR_HRS_LESS_WARNING,
         V_EXCEEDS_PTO_ENTIT_WARNING,
         V_EXCEEDS_RUN_TOTAL_WARNING,
         V_ABS_OVERLAV_WARNING,
         V_ABS_DAY_AFTER_WARNING,
         V_DUR_OVERWRITTEN_WARNING);
      RETURN 'SUCCESS';
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Error ' || SQLERRM);
         RETURN FND_MESSAGE.GET;                                    --SQLERRM;
   END CALL_HOOKS;

   FUNCTION TRANSALATE_MESSAGE (P_MESSAGE_NAME   IN VARCHAR2,
                                P_LANGUAGE       IN VARCHAR2 := 'US')
      RETURN VARCHAR2
   IS
      V_MESSAGE_TEXT   VARCHAR2 (4000);
   BEGIN
      SELECT MESSAGE_TEXT
        INTO V_MESSAGE_TEXT
        FROM FND_NEW_MESSAGES
       WHERE     1 = 1
             AND MESSAGE_NAME = P_MESSAGE_NAME
             AND LANGUAGE_CODE = P_LANGUAGE;

      RETURN V_MESSAGE_TEXT;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN P_MESSAGE_NAME;
   END TRANSALATE_MESSAGE;

   PROCEDURE ADD_ATTACHEMENT (P_ABSENCE_ATTENDENCE_ID    NUMBER,
                              P_TRANSACTION_ID           NUMBER,
                              P_FILE_NAME                VARCHAR2,
                              P_FILE_CONTENT_TYPE        VARCHAR2,
                              P_FILE_CONTENT             BLOB,
                              P_USER_ID                  NUMBER)
   IS
      V_DOCUMENT_ID            NUMBER;
      V_ATTACHED_DOCUMENT_ID   NUMBER;
      V_SEQ_NUM                NUMBER;
      V_MEDIA_ID               NUMBER;
      V_ROWID                  VARCHAR2 (1000);
   BEGIN
      V_DOCUMENT_ID := FND_DOCUMENTS_S.NEXTVAL;
      V_ATTACHED_DOCUMENT_ID := FND_ATTACHED_DOCUMENTS_S.NEXTVAL;
      V_MEDIA_ID := FND_LOBS_S.NEXTVAL;
      V_SEQ_NUM := 1;


      INSERT INTO FND_LOBS (FILE_ID,
                            FILE_NAME,
                            FILE_CONTENT_TYPE,
                            UPLOAD_DATE,
                            EXPIRATION_DATE,
                            PROGRAM_NAME,
                            PROGRAM_TAG,                    ------------------
                            FILE_DATA,
                            LANGUAGE,
                            ORACLE_CHARSET,
                            FILE_FORMAT)
           VALUES (V_MEDIA_ID,
                   P_FILE_NAME,
                   P_FILE_CONTENT_TYPE, --'application/pdf', --'text/plain',--'image/png'
                   SYSDATE,
                   NULL,
                   'FNDATTCH',
                   NULL,
                   P_FILE_CONTENT,
                   'US',
                   'UTF8',
                   'binary');


      FND_DOCUMENTS_PKG.INSERT_ROW (
         X_ROWID               => V_ROWID,
         X_DOCUMENT_ID         => V_DOCUMENT_ID,
         X_CREATION_DATE       => SYSDATE,
         X_CREATED_BY          => P_USER_ID,
         X_LAST_UPDATE_DATE    => SYSDATE,
         X_LAST_UPDATED_BY     => P_USER_ID,
         X_LAST_UPDATE_LOGIN   => P_USER_ID,
         X_DATATYPE_ID         => 6,                                   -- FILE
         X_SECURITY_ID         => NULL,
         X_PUBLISH_FLAG        => 'Y',                                  --'N',
         X_CATEGORY_ID         => 1,
         X_SECURITY_TYPE       => 4,
         X_USAGE_TYPE          => 'O',                                   --'S'
         X_LANGUAGE            => 'US',
         X_DESCRIPTION         => 'Attach ' || V_ATTACHED_DOCUMENT_ID, --DESCRIPTION,
         X_FILE_NAME           => P_FILE_NAME,
         X_MEDIA_ID            => V_MEDIA_ID,
         X_DM_NODE             => 0);

      --        FND_DOCUMENTS_PKG.INSERT_TL_ROW (
      --                    X_DOCUMENT_ID         => V_DOCUMENT_ID,
      --                    X_CREATION_DATE       => SYSDATE,
      --                    X_CREATED_BY          => P_USER_ID,
      --                    X_LAST_UPDATE_DATE    => SYSDATE,
      --                    X_LAST_UPDATED_BY     => P_USER_ID,
      --                    X_LAST_UPDATE_LOGIN   => P_USER_ID,
      --                    X_LANGUAGE            => 'US',
      --                    X_DESCRIPTION         => 'Attach '||V_ATTACHED_DOCUMENT_ID,
      --                    X_TITLE               => 'Attach '||V_ATTACHED_DOCUMENT_ID); --TITLE


      FND_ATTACHED_DOCUMENTS_PKG.INSERT_ROW (
         X_ROWID                      => V_ROWID,
         X_ATTACHED_DOCUMENT_ID       => V_ATTACHED_DOCUMENT_ID,
         X_DOCUMENT_ID                => V_DOCUMENT_ID,
         X_CREATION_DATE              => SYSDATE,
         X_CREATED_BY                 => P_USER_ID,
         X_LAST_UPDATE_DATE           => SYSDATE,
         X_LAST_UPDATED_BY            => P_USER_ID,
         X_LAST_UPDATE_LOGIN          => P_USER_ID,
         X_SEQ_NUM                    => V_SEQ_NUM,
         X_ENTITY_NAME                => 'PER_ABSENCE_ATTENDANCES',
         X_COLUMN1                    => NULL,
         X_PK1_VALUE                  =>    P_ABSENCE_ATTENDENCE_ID
                                         || '_'
                                         || P_TRANSACTION_ID,    --IN_FND_PK1,
         X_PK2_VALUE                  => NULL,
         X_PK3_VALUE                  => NULL,
         X_PK4_VALUE                  => NULL,
         X_PK5_VALUE                  => NULL,
         X_AUTOMATICALLY_ADDED_FLAG   => 'N',
         X_DATATYPE_ID                => 6,
         X_CATEGORY_ID                => 1,
         X_SECURITY_TYPE              => 4,
         X_SECURITY_ID                => NULL,
         X_PUBLISH_FLAG               => 'Y',
         X_LANGUAGE                   => 'US',
         X_DESCRIPTION                => 'Attach ' || V_ATTACHED_DOCUMENT_ID,
         X_TITLE                      => 'Attach ' || V_ATTACHED_DOCUMENT_ID, --TITLE,
         X_FILE_NAME                  => P_FILE_NAME,
         X_MEDIA_ID                   => V_MEDIA_ID);
   /*
       EXCEPTION
           WHEN OTHERS THEN
           NULL;*/
   END;

   PROCEDURE CANCEL_REQUEST (
      P_ITEM_TYPE           VARCHAR2,
      P_ITEM_KEY            NUMBER,
      P_PROCESS_INT_NAME    VARCHAR2,           --MBC_LOA_GENERIC_APPROVAL_PRC
      P_TRANSACTION_ID      NUMBER,
      P_VALIDATE            NUMBER DEFAULT HR_API.G_FALSE_NUM)
   IS
   BEGIN
      WF_ENGINE.ABORTPROCESS (ITEMTYPE   => P_ITEM_TYPE,
                              ITEMKEY    => P_ITEM_KEY,
                              PROCESS    => P_PROCESS_INT_NAME);


      HR_TRANSACTION_SWI.DELETE_TRANSACTION (
         P_TRANSACTION_ID   => P_TRANSACTION_ID,
         P_VALIDATE         => P_VALIDATE);
   END CANCEL_REQUEST;

   FUNCTION DECODE_BASE64 (P_CLOB_IN IN CLOB)
      RETURN BLOB
   IS
      V_BLOB             BLOB;
      V_RESULT           BLOB;
      V_OFFSET           INTEGER;
      V_BUFFER_SIZE      BINARY_INTEGER := 48;
      V_BUFFER_VARCHAR   VARCHAR2 (48);
      V_BUFFER_RAW       RAW (48);
   BEGIN
      IF P_CLOB_IN IS NULL
      THEN
         RETURN NULL;
      END IF;

      DBMS_LOB.CREATETEMPORARY (V_BLOB, TRUE);
      V_OFFSET := 1;

      FOR I IN 1 .. CEIL (DBMS_LOB.GETLENGTH (P_CLOB_IN) / V_BUFFER_SIZE)
      LOOP
         DBMS_LOB.READ (P_CLOB_IN,
                        V_BUFFER_SIZE,
                        V_OFFSET,
                        V_BUFFER_VARCHAR);
         V_BUFFER_RAW := UTL_RAW.CAST_TO_RAW (V_BUFFER_VARCHAR);
         V_BUFFER_RAW := UTL_ENCODE.BASE64_DECODE (V_BUFFER_RAW);
         DBMS_LOB.WRITEAPPEND (V_BLOB,
                               UTL_RAW.LENGTH (V_BUFFER_RAW),
                               V_BUFFER_RAW);
         V_OFFSET := V_OFFSET + V_BUFFER_SIZE;
      END LOOP;

      V_RESULT := V_BLOB;
      DBMS_LOB.FREETEMPORARY (V_BLOB);
      RETURN V_RESULT;
   END DECODE_BASE64;

   FUNCTION BASE64ENCODE (P_BLOB IN BLOB)
      RETURN CLOB
   IS
      L_CLOB   CLOB;
      vSizedImage BLOB;
      L_STEP   PLS_INTEGER := 12000; -- make sure you set a multiple of 3 not higher than 24573
   BEGIN
      IF (P_BLOB IS NULL)
      THEN
         RETURN NULL;
      END IF;
        DBMS_Lob.createTemporary(vSizedImage, FALSE, DBMS_LOB.CALL);
       -- vSizedImage:=P_BLOB;
       ORDSYS.OrdImage.processCopy(P_BLOB, 'maxScale=70 70', vSizedImage);
       
      FOR I IN 0 .. TRUNC ( (DBMS_LOB.GETLENGTH (vSizedImage) - 1) / L_STEP)
      LOOP
         L_CLOB :=
               L_CLOB
            || UTL_RAW.CAST_TO_VARCHAR2 (
                  UTL_ENCODE.BASE64_ENCODE (
                     DBMS_LOB.SUBSTR (vSizedImage, L_STEP, I * L_STEP + 1)));
      END LOOP;

      RETURN L_CLOB;
   END BASE64ENCODE;

   FUNCTION UPLOAD_IMAGE (P_PERSON_ID    NUMBER,
                          P_ATTACH_ID    NUMBER,
                          P_DATA_FILE    CLOB)
      RETURN NUMBER
   IS
   BEGIN
      INSERT INTO MBC_MOBILE_SSHR_BLOB_DATA (PERSON_ID,
                                             ATTACH_ID,
                                             DATA_FILE,
                                             CLOB_DATA)
           VALUES (P_PERSON_ID,
                   P_ATTACH_ID,
                   MBC_MOBILE_PKG.DECODE_BASE64 (P_DATA_FILE),
                   P_DATA_FILE);

      RETURN 1;
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END UPLOAD_IMAGE;
-- PRIVATE PROCEDURE TO SET WORKFLOW   ATTRIBUTES 
PROCEDURE SET_NOTIFICATION_ATTRIBUTES ( P_NOTIFICATION_ID    NUMBER,
                                        P_ATTRRIBUTES       VARCHAR2,
                                        P_ITEM_TYPE         VARCHAR2,
                                        P_ITEM_KEY          VARCHAR2) 
IS 
V_TYPE_SECTION VARCHAR2(500);
V_VALUE_SECTION VARCHAR2(500);
V_NAME_SECTION  VARCHAR2(500);

V_TYPE VARCHAR2(500);
V_VALUE VARCHAR2(500);
V_NAME VARCHAR2(500);
CURSOR ATTRIBUTES 
IS 
 select regexp_substr(P_ATTRRIBUTES, '[^;]+', 1, level) ATTRIBUTE from dual  
   connect by regexp_substr(P_ATTRRIBUTES, '[^;]+', 1, level) is not null;
BEGIN

FOR I IN ATTRIBUTES LOOP
V_TYPE_SECTION   := Regexp_substr (I.ATTRIBUTE ,'[^,]+', 1, 1);
V_NAME_SECTION  := Regexp_substr (I.ATTRIBUTE ,'[^,]+', 1, 2);
V_VALUE_SECTION   := Regexp_substr (I.ATTRIBUTE ,'[^,]+', 1, 3);

V_TYPE   := TRIM ( Regexp_substr (V_TYPE_SECTION ,'[^:]+', 1, 2)  );
V_VALUE  := TRIM ( Regexp_substr (V_VALUE_SECTION ,'[^:]+', 1, 2) );
V_NAME   := TRIM ( Regexp_substr (V_NAME_SECTION ,'[^:]+', 1, 2) );


--raise_application_error(-20001,'V_VALUE :'||V_VALUE||' V_NAME :'||V_NAME);


IF (UPPER(V_TYPE)='NUMBER') THEN 
    WF_NOTIFICATION.SetAttrNumber (P_NOTIFICATION_ID,V_NAME ,V_VALUE   );
    begin
        WF_ENGINE.SETITEMATTRNUMBER (P_ITEM_TYPE ,P_ITEM_KEY ,V_NAME ,V_VALUE   );
     exception
    when others then null;
    end;
ELSIF  (UPPER(V_TYPE)='DATE') THEN 
WF_NOTIFICATION.SetAttrDate (P_NOTIFICATION_ID,V_NAME ,TO_DATE (V_VALUE,'DD-MM-YYYY'));
ELSE
WF_NOTIFICATION.SetAttrText (P_NOTIFICATION_ID,V_NAME ,V_VALUE   );
    begin
        WF_ENGINE.SETITEMATTRTEXT (P_ITEM_TYPE ,P_ITEM_KEY ,V_NAME ,V_VALUE   );
    exception
    when others then null;
    end;


END IF;



END LOOP;

COMMIT;

END SET_NOTIFICATION_ATTRIBUTES;

   --Worklist
   FUNCTION NOTIFICATION_ACTION (P_NOTIFICATION_ID    NUMBER,
                                 P_ITEM_TYPE          VARCHAR2,
                                 P_ITEM_KEY           VARCHAR2,
                                 P_ACTIVITY_ID        NUMBER,
                                 P_BUTTON_NAME        VARCHAR2,
                                 P_FROM_USER          VARCHAR2,
                                 P_ATTRIBUTES         VARCHAR2)
      RETURN VARCHAR2
   IS
      V_ACTIVITY_NAME   VARCHAR2 (1000);
      V_BUTTON_NAME     VARCHAR2 (1000) := P_BUTTON_NAME;
      v_activity_FUNCTION wf_activities_vl.function%type;
      v_call_function   varchar2(2048);
      V_ERROR_MSG       varchar2(2048); 
   BEGIN
--     raise_application_error (-20001,'P_NOTIFICATION_ID ' || P_NOTIFICATION_ID ||'-' || 'P_ITEM_TYPE ' || P_ITEM_TYPE ||'-' || 'P_ITEM_KEY '|| P_ITEM_KEY ||'-' || 'P_ACTIVITY_ID ' || P_ACTIVITY_ID ||'-'|| 'P_BUTTON_NAME ' || P_BUTTON_NAME    );
   --raise_application_error (-20001,'attrs is '||P_ATTRIBUTES );
 INITIALIZE_SESSION(P_FROM_USER);

         --Respond to more_info_request (Answer) 
      IF P_BUTTON_NAME = 'SUBMIT'
      THEN
         
         wf_notification.UpdateInfo(nid   =>   P_NOTIFICATION_ID,
          comment => TRIM ( Regexp_substr (Regexp_substr (P_ATTRIBUTES ,'[^,]+', 1, 3) ,'[^:]+', 1, 2) ));
                     
           RETURN 'Success';
     END IF;  

 IF  P_ATTRIBUTES <> 'none' THEN 
       BEGIN
           MBC_MOBILE_PKG.SET_NOTIFICATION_ATTRIBUTES(P_NOTIFICATION_ID,P_ATTRIBUTES,P_ITEM_TYPE ,P_ITEM_KEY );
       EXCEPTION 
           WHEN OTHERS THEN 
           RETURN SQLERRM;
       END;
   END IF;
   
   


      --Take Action :(
      IF P_BUTTON_NAME = 'OK'
      THEN
         V_BUTTON_NAME := NULL;
         WF_NOTIFICATION.CLOSE(P_NOTIFICATION_ID,'SYSADMIN');
           RETURN 'Success';
     END IF;
   
    -- Added by Mahmoud to set Result when respond
      wf_notification.SETattrtext (P_NOTIFICATION_ID, 'RESULT' , P_BUTTON_NAME);
   
     --Get Activity Name
      SELECT wias.ACTIVITY_NAME ,wal.FUNCTION
        INTO V_ACTIVITY_NAME ,v_activity_FUNCTION
        FROM WF_ITEM_ACTIVITY_STATUSES_V wias , wf_activities_vl wal 
       WHERE     wias.ITEM_TYPE = wal.ITEM_TYPE
       and   wias.ACTIVITY_NAME = wal.NAME 
                AND wias.ACTIVITY_BEGIN_DATE BETWEEN wal.BEGIN_DATE
                                    AND NVL (wal.END_DATE, greatest( wias.ACTIVITY_BEGIN_DATE , wal.BEGIN_DATE   )      )
             AND wias.ACTIVITY_ID =   P_ACTIVITY_ID     
             AND  wias.ITEM_KEY = P_ITEM_KEY
             AND  wias.ITEM_TYPE = P_ITEM_TYPE
             AND ROWNUM < 2;
      
      -- Handle ixpense child process
      if P_ITEM_TYPE ='APEXP' and V_ACTIVITY_NAME = 'AME_APPROVAL_REQUEST' then
        V_ACTIVITY_NAME := 'AME_REQUEST_APPROVAL_FROM_APPR';
      end if;
      
      if v_activity_FUNCTION is not null then
        v_call_function :='DECLARE V_RESULTOUT varchar2(512); BEGIN WF_ENGINE.CONTEXT_NID := '||P_NOTIFICATION_ID ||';'|| v_activity_FUNCTION ||'('''||P_ITEM_TYPE  ||  ''','''  || P_ITEM_KEY || ''',''' ||P_ACTIVITY_ID||''',''RESPOND'',V_RESULTOUT);  END; ';
        
        dbms_output.put_line('V_ACTIVITY_NAME='||V_ACTIVITY_NAME || CHR(10) || ' v_call_function='||v_call_function);
        execute immediate v_call_function ;
      end if;

      IF P_ITEM_TYPE = 'HRSSA'
      THEN
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => P_ITEM_TYPE,
            ITEMKEY        => P_ITEM_KEY,
            ANAME          => 'HR_CONTEXT_NID_ATTR',
            TEXT_VALUE     => NULL,
            NUMBER_VALUE   => P_NOTIFICATION_ID,
            DATE_VALUE     => NULL);
            
     
            
      END IF;



    
      WF_ENGINE.COMPLETEACTIVITY (ITEMTYPE   => P_ITEM_TYPE,
                                  ITEMKEY    => P_ITEM_KEY,
                                  ACTIVITY   => V_ACTIVITY_NAME,
                                  RESULT     => V_BUTTON_NAME); --WF_ENGINE.ENG_NULL);
      
      
      RETURN 'Success';
   EXCEPTION
      WHEN OTHERS
      THEN
      V_ERROR_MSG:= regexp_replace (SQLERRM,'((ORA.)*[[:digit:]])+: ', '');  
      RETURN V_ERROR_MSG;
   END NOTIFICATION_ACTION;
   
   FUNCTION      START_SIT_WORKFLOW(              P_PERSON_ID                          NUMBER,
                                                  P_BUSINESS_GROUP_ID                   NUMBER,
                                                  P_SIT_TITLE                           VARCHAR2,
                                                  P_STATUS                              VARCHAR2,
                                                  P_SEGMENT1                            VARCHAR2 := NULL,
                                                  P_SEGMENT2                            VARCHAR2 := NULL,
                                                  P_SEGMENT3                            VARCHAR2 := NULL,
                                                  P_SEGMENT4                            VARCHAR2 := NULL,
                                                  P_SEGMENT5                            VARCHAR2 := NULL,
                                                  P_SEGMENT6                            VARCHAR2 := NULL,
                                                  P_SEGMENT7                            VARCHAR2 := NULL,
                                                  P_SEGMENT8                            VARCHAR2 := NULL,
                                                  P_SEGMENT9                            VARCHAR2 := NULL,
                                                  P_SEGMENT10                           VARCHAR2 := NULL,
                                                  P_SEGMENT11                           VARCHAR2 := NULL,
                                                  P_SEGMENT12                           VARCHAR2 := NULL,
                                                  P_SEGMENT13                           VARCHAR2 := NULL,
                                                  P_SEGMENT14                           VARCHAR2 := NULL,
                                                  P_SEGMENT15                           VARCHAR2 := NULL,
                                                  P_SEGMENT16                           VARCHAR2 := NULL,
                                                  P_SEGMENT17                           VARCHAR2 := NULL,
                                                  P_SEGMENT18                           VARCHAR2 := NULL,
                                                  P_SEGMENT19                           VARCHAR2 := NULL,
                                                  P_SEGMENT20                           VARCHAR2 := NULL,
                                                  P_SEGMENT21                           VARCHAR2 := NULL,
                                                  P_SEGMENT22                           VARCHAR2 := NULL,
                                                  P_SEGMENT23                           VARCHAR2 := NULL,
                                                  P_SEGMENT24                           VARCHAR2 := NULL,
                                                  P_SEGMENT25                           VARCHAR2 := NULL,
                                                  P_SEGMENT26                           VARCHAR2 := NULL,
                                                  P_SEGMENT27                           VARCHAR2 := NULL,
                                                  P_SEGMENT28                           VARCHAR2 := NULL,
                                                  P_SEGMENT29                           VARCHAR2 := NULL,
                                                  P_SEGMENT30                           VARCHAR2 := NULL,
                                                  P_TRANSACTION_ID                    OUT  NUMBER,
                                                  P_ITEM_KEY                          OUT  NUMBER,
                                                  P_PROCESS_NAME                      OUT  VARCHAR2) RETURN VARCHAR2 IS
    
    CURSOR CUR_EMP_DATA
      IS
         SELECT PAPF.EMPLOYEE_NUMBER,
                PAPF.FULL_NAME,
                FU.USER_ID,
                PBG.LEGISLATION_CODE,
                PBG.CURRENCY_CODE,
                PBG.SECURITY_GROUP_ID,
                PJ.NAME JOB_NAME,
                PP.NAME POSITION_NAME,
                (SELECT FULL_NAME
                   FROM PER_ALL_PEOPLE_F
                  WHERE     PERSON_ID = PAAF.SUPERVISOR_ID
                        AND SYSDATE BETWEEN EFFECTIVE_START_DATE
                                        AND EFFECTIVE_END_DATE
                        AND CURRENT_EMPLOYEE_FLAG = 'Y')
                   SUPERVISOR_NAME,
                PAAF.*
           FROM APPS.PER_ALL_PEOPLE_F PAPF,
                APPS.PER_ALL_ASSIGNMENTS_F PAAF,
                FND_USER FU,
                PER_BUSINESS_GROUPS PBG,
                PER_JOBS PJ,
                PER_POSITIONS PP
          WHERE     PAPF.PERSON_ID = PAAF.PERSON_ID
                AND PAPF.PERSON_ID = FU.EMPLOYEE_ID
                AND PAPF.BUSINESS_GROUP_ID = PBG.BUSINESS_GROUP_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE
                                AND PAAF.EFFECTIVE_END_DATE
                AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
                AND PAAF.PRIMARY_FLAG = 'Y'
                AND PAAF.JOB_ID = PJ.JOB_ID(+)
                AND PAAF.POSITION_ID = PP.POSITION_ID(+)
                AND PAPF.PERSON_ID = P_PERSON_ID;

    V_APPROVAL_REQUIRED             VARCHAR2 (10) := 'YD';    
    V_AMETRANSTYPE                  VARCHAR2 (10) := 'SSHRMS';
    V_AMETRANAPPID                  NUMBER := 800;    
    V_NTF_ATTACH_ATTR               VARCHAR2 (10) := NULL;            
    V_STATUS                        VARCHAR2 (4000);
    V_ERROR_MESSAGE                 VARCHAR2 (4000);
    V_ERRSTACK                      VARCHAR2 (4000);
    VT_ADDITIONAL_WF_ATTRIBUTES     HR_WF_ATTR_TABLE         := HR_WF_ATTR_TABLE (HR_WF_ATTR_TYPE ('TRAN_SUBMIT','N',NULL,NULL));      
    V_TRANSACTION_ID                NUMBER;
    V_TRANSACTION_STEP_ID           NUMBER;
    V_ITEM_KEY                      NUMBER;
    V_ITEM_TYPE                     VARCHAR2 (10) := 'HRSSA';
    V_ANALYSIS_CRITERIA_ID          NUMBER;    
    V_OBJECT_VERSION_NUMBER         NUMBER;
    
    
    V_PERZ_FUNC                     VARCHAR2 (64); 
    V_NTFSUBMSG                     VARCHAR2 (64);
    V_REVIEW_TEMPLATE_RN            VARCHAR2 (64);
    V_ID_FLEX_NUM                   NUMBER ;
    V_PROCESS_NAME                  VARCHAR2(1000);
    V_FUNCTION_ID                   NUMBER ;
    V_FUNCTION_NAME                 VARCHAR2(1000);
    V_SIT_CODE                      VARCHAR2(1000);
    
BEGIN
    
  -- raise_application_error (-20001,'error : '||P_SEGMENT12);
 IF P_SIT_TITLE = 'Request Letters from HR' THEN
        V_PERZ_FUNC             := 'MBC3_HR_SIT_SS';
        V_NTFSUBMSG             := 'MBC_HR_SIT_JSP_PRC';
        V_REVIEW_TEMPLATE_RN    := 'HR_REVIEWRN_NTF';
        V_FUNCTION_NAME         := 'MBC3_HR_SIT_SS';
        V_FUNCTION_ID           := 40160;
        V_PROCESS_NAME          := 'MBC_LETTER_REQUEST';
        V_ID_FLEX_NUM           := 50388;
        V_SIT_CODE              := 'MBC 23';        
    ELSIF P_SIT_TITLE = 'Request Letters from Saudi HR' THEN-----------------------------------
        V_PERZ_FUNC             := '';
        V_NTFSUBMSG             := 'MBC_HR_SIT_JSP_PRC';
        V_REVIEW_TEMPLATE_RN    := '';
        V_FUNCTION_NAME         := 'MBC3_SAUDI_HR_SIT_SS';
        V_FUNCTION_ID           := 50338;
        V_PROCESS_NAME          := 'MBC_LETTER_REQUEST';
        V_ID_FLEX_NUM           := 50689;
        V_SIT_CODE              := 'MBC 23_SAUDI';
    ELSIF P_SIT_TITLE = 'Egypt Letter Request' THEN--------------------------------------------
        V_PERZ_FUNC             := '';
        V_NTFSUBMSG             := 'MBC_HR_SIT_JSP_PRC';
        V_REVIEW_TEMPLATE_RN    := '';
        V_FUNCTION_NAME         := 'MBC_EGY_LETT_REQ';
        V_FUNCTION_ID           := 49458;
        V_PROCESS_NAME          := 'MBC_LETTER_REQUEST';
        V_ID_FLEX_NUM           := 50649;
        V_SIT_CODE              := 'MBC_EGY_LETT_REQ';
    ELSIF P_SIT_TITLE = 'Housing Advance Request' THEN
        V_PERZ_FUNC             := 'MBC5_HR_SIT_SS';
        V_NTFSUBMSG             := 'MBC_HR_SIT_JSP_PRC';
        V_REVIEW_TEMPLATE_RN    := 'HR_REVIEWRN_NTF';
        V_FUNCTION_NAME         := 'MBC5_HR_SIT_SS';
        V_FUNCTION_ID           := 41195;
        V_PROCESS_NAME          := 'MBC_HR_SIT_JSP_PRC';
        V_ID_FLEX_NUM           := 50429;
        V_SIT_CODE              := 'MBC25';
    ELSIF P_SIT_TITLE = 'Submit Resignation' THEN
        V_PERZ_FUNC             := 'MBC2_HR_SIT_SS';
        V_NTFSUBMSG             := 'MBC_HR_SIT_JSP_PRC';
        V_REVIEW_TEMPLATE_RN    := 'HR_REVIEWRN_NTF';
        V_FUNCTION_NAME         := 'MBC2_HR_SIT_SS';
        V_FUNCTION_ID           := 40144;
        V_PROCESS_NAME          := 'MBC_HR_SIT_JSP_PRC';
        V_ID_FLEX_NUM           := 50355;
        V_SIT_CODE              := 'MBC22';
    ELSIF P_SIT_TITLE = 'Business Card' THEN
        V_PERZ_FUNC             := 'MBC_BUSINESS_CARD';
        V_NTFSUBMSG             := 'MBC_BUSINESS_CARD';
        V_REVIEW_TEMPLATE_RN    := 'HR_REVIEWRN_NTF';
        V_FUNCTION_NAME         := 'MBC_BUSINESS_CARD';
        V_FUNCTION_ID           := 56543;
        V_PROCESS_NAME          := 'MBC_BUSINESS_CARD';
        V_ID_FLEX_NUM           := 50749;
        V_SIT_CODE              := 'MBC 33';
    ELSE
        RETURN 'Not valid input';
    END IF;
    
    V_TRANSACTION_ID        := HR_API_TRANSACTIONS_S.NEXTVAL;
    V_TRANSACTION_STEP_ID   := HR_API_TRANSACTION_STEPS_S.NEXTVAL;
    V_ITEM_KEY              := HR_WORKFLOW_ITEM_KEY_S.NEXTVAL;
    
    BEGIN
        SELECT MIN(ANALYSIS_CRITERIA_ID)
        INTO  V_ANALYSIS_CRITERIA_ID
        FROM  PER_ANALYSIS_CRITERIA
        WHERE ID_FLEX_NUM = V_ID_FLEX_NUM
        AND   NVL(SEGMENT1 ,'-X') = NVL(P_SEGMENT1 ,'-X') 
        AND   NVL(SEGMENT2 ,'-X') = NVL(P_SEGMENT2 ,'-X')
        AND   NVL(SEGMENT3 ,'-X') = NVL(P_SEGMENT3 ,'-X')
        AND   NVL(SEGMENT4 ,'-X') = NVL(P_SEGMENT4 ,'-X')
        AND   NVL(SEGMENT5 ,'-X') = NVL(P_SEGMENT5 ,'-X')
        AND   NVL(SEGMENT6 ,'-X') = NVL(P_SEGMENT6 ,'-X')
        AND   NVL(SEGMENT7 ,'-X') = NVL(P_SEGMENT7 ,'-X')
        AND   NVL(SEGMENT8 ,'-X') = NVL(P_SEGMENT8 ,'-X')
        AND   NVL(SEGMENT9 ,'-X') = NVL(P_SEGMENT9 ,'-X')
        AND   NVL(SEGMENT10,'-X') = NVL(P_SEGMENT10,'-X')
        AND   NVL(SEGMENT11,'-X') = NVL(P_SEGMENT11,'-X')
        AND   NVL(SEGMENT12,'-X') = NVL(P_SEGMENT12,'-X')
        AND   NVL(SEGMENT13,'-X') = NVL(P_SEGMENT13,'-X')
        AND   NVL(SEGMENT14,'-X') = NVL(P_SEGMENT14,'-X')
        AND   NVL(SEGMENT15,'-X') = NVL(P_SEGMENT15,'-X')
        AND   NVL(SEGMENT16,'-X') = NVL(P_SEGMENT16,'-X')
        AND   NVL(SEGMENT17,'-X') = NVL(P_SEGMENT17,'-X')
        AND   NVL(SEGMENT18,'-X') = NVL(P_SEGMENT18,'-X')
        AND   NVL(SEGMENT19,'-X') = NVL(P_SEGMENT19,'-X')
        AND   NVL(SEGMENT20,'-X') = NVL(P_SEGMENT20,'-X')
        AND   NVL(SEGMENT21,'-X') = NVL(P_SEGMENT21,'-X')
        AND   NVL(SEGMENT22,'-X') = NVL(P_SEGMENT22,'-X')
        AND   NVL(SEGMENT23,'-X') = NVL(P_SEGMENT23,'-X')
        AND   NVL(SEGMENT24,'-X') = NVL(P_SEGMENT24,'-X')
        AND   NVL(SEGMENT25,'-X') = NVL(P_SEGMENT25,'-X')
        AND   NVL(SEGMENT26,'-X') = NVL(P_SEGMENT26,'-X')
        AND   NVL(SEGMENT27,'-X') = NVL(P_SEGMENT27,'-X')
        AND   NVL(SEGMENT28,'-X') = NVL(P_SEGMENT28,'-X')
        AND   NVL(SEGMENT29,'-X') = NVL(P_SEGMENT29,'-X')
        AND   NVL(SEGMENT30,'-X') = NVL(P_SEGMENT30,'-X');
      dbms_output.put_line(' VAL IS '||V_ANALYSIS_CRITERIA_ID);
      
     -- to raise the Exception 23-05-2019
    
            if ( V_ANALYSIS_CRITERIA_ID is null) then 
                RAISE_APPLICATION_ERROR(-20001,' V_ANALYSIS_CRITERIA_ID IS Null');
            end if; 
 

      
    EXCEPTION
        WHEN OTHERS THEN
        V_ANALYSIS_CRITERIA_ID  := PER_ANALYSIS_CRITERIA_S.NEXTVAL;
          INSERT INTO PER_ANALYSIS_CRITERIA (ANALYSIS_CRITERIA_ID,
                                            ID_FLEX_NUM,
                                            SUMMARY_FLAG,
                                            ENABLED_FLAG,
                                            SEGMENT1,
                                            SEGMENT2,
                                            SEGMENT3,
                                            SEGMENT4,
                                            SEGMENT5,
                                            SEGMENT6,
                                            SEGMENT7,
                                            SEGMENT8,
                                            SEGMENT9,
                                            SEGMENT10,
                                            SEGMENT11,
                                            SEGMENT12,
                                            SEGMENT13,
                                            SEGMENT14,
                                            SEGMENT15,
                                            SEGMENT16,
                                            SEGMENT17,
                                            SEGMENT18,
                                            SEGMENT19,
                                            SEGMENT20,
                                            SEGMENT21,
                                            SEGMENT22,
                                            SEGMENT23,
                                            SEGMENT24,
                                            SEGMENT25,
                                            SEGMENT26,
                                            SEGMENT27,
                                            SEGMENT28,
                                            SEGMENT29,
                                            SEGMENT30)
              VALUES (V_ANALYSIS_CRITERIA_ID,
                      V_ID_FLEX_NUM,
                      'N',
                      'Y',
                      P_SEGMENT1,
                      P_SEGMENT2,
                      P_SEGMENT3,
                      P_SEGMENT4,
                      P_SEGMENT5,
                      P_SEGMENT6,
                      P_SEGMENT7,
                      P_SEGMENT8,
                      P_SEGMENT9,
                      P_SEGMENT10,
                      P_SEGMENT11,
                      P_SEGMENT12,
                      P_SEGMENT13,
                      P_SEGMENT14,
                      P_SEGMENT15,
                      P_SEGMENT16,
                      P_SEGMENT17,
                      P_SEGMENT18,
                      P_SEGMENT19,
                      P_SEGMENT20,
                      P_SEGMENT21,
                      P_SEGMENT22,
                      P_SEGMENT23,
                      P_SEGMENT24,
                      P_SEGMENT25,
                      P_SEGMENT26,
                      P_SEGMENT27,
                      P_SEGMENT28,
                      P_SEGMENT29,
                      P_SEGMENT30);
         -- RAISE_APPLICATION_ERROR(-20001,' VAL IS '||V_ANALYSIS_CRITERIA_ID);
    END;

    FOR REQUEST_REC IN CUR_EMP_DATA LOOP
 INITIALIZE_SESSION (REQUEST_REC.USER_ID);
         HR_TRANSACTION_SWI.INIT_PROFILES (
            P_PERSON_ID           => REQUEST_REC.PERSON_ID,
            P_ASSIGNMENT_ID       => REQUEST_REC.ASSIGNMENT_ID,
            P_BUSINESS_GROUP_ID   => REQUEST_REC.BUSINESS_GROUP_ID,
            P_ORGANIZATION_ID     => REQUEST_REC.ORGANIZATION_ID,
            P_LOCATION_ID         => REQUEST_REC.LOCATION_ID,
            P_PAYROLL_ID          => REQUEST_REC.PAYROLL_ID);


         HR_TRANSACTION_SWI.CREATE_TRANSACTION (
            P_ASSIGNMENT_ID                => REQUEST_REC.ASSIGNMENT_ID,
            P_CREATOR_PERSON_ID            => REQUEST_REC.PERSON_ID,
            P_SELECTED_PERSON_ID           => REQUEST_REC.PERSON_ID,
            P_STATUS                       => P_STATUS, ------------'Y',
            P_TRANSACTION_EFFECTIVE_DATE   => TRUNC (SYSDATE),
            P_TRANSACTION_ID               => V_TRANSACTION_ID,
            P_TRANSACTION_PRIVILEGE        => 'PRIVATE',
            P_TRANSACTION_REF_ID           => V_TRANSACTION_ID,
            P_FUNCTION_ID                  => V_FUNCTION_ID,-----------------------------------------------------------------------TO BE UPDATED------------------------------------
            P_PRODUCT_CODE                 => 'PER',
            P_TRANSACTION_REF_TABLE        => 'HR_API_TRANSACTIONS',
            --P_TRANSACTION_GROUP            => 'ABSENCE_MGMT',
            --P_TRANSACTION_IDENTIFIER       => 'ABSENCES',
            P_TRANSACTION_TYPE             => 'WF',
            P_ITEM_TYPE                    => V_ITEM_TYPE,
            P_ITEM_KEY                     => V_ITEM_KEY,
            P_PROCESS_NAME                 => V_PROCESS_NAME
                                                                  );

         HR_TRANSACTION_SWI.CREATE_TRANSACTION_STEP (
            P_ACTIVITY_ID             => 387147, --WF_PROCESS_ACTIVITIES_S.NEXTVAL,
            P_API_NAME                => 'HR_PROCESS_SIT_SS.PROCESS_API',
            P_CREATOR_PERSON_ID       => REQUEST_REC.PERSON_ID,
            P_UPDATE_PERSON_ID        => REQUEST_REC.PERSON_ID,
            P_ITEM_TYPE               => V_ITEM_TYPE,
            P_ITEM_KEY                => V_ITEM_KEY,
            P_PROCESSING_ORDER        => 0,
            --P_OBJECT_TYPE             => 'ENTITY',
            --P_OBJECT_NAME             => 'oracle.apps.per.schema.server.PerAbsenceAttendancesEO',
            --P_OBJECT_IDENTIFIER       => '000100000005C408334F40',
            --P_PK1                     => '7508872_7754454',
            P_OBJECT_STATE            => '0',
            P_TRANSACTION_ID          => V_TRANSACTION_ID,
            P_TRANSACTION_STEP_ID     => V_TRANSACTION_STEP_ID,
            P_OBJECT_VERSION_NUMBER   => V_OBJECT_VERSION_NUMBER);

         
--------------------------------------------------------------------------------------------------------------------------HERE-----------------------
       


         HR_TRANSACTION_API.SET_NUMBER_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_PERSON_ID',
            P_VALUE                 => REQUEST_REC.PERSON_ID);
         HR_TRANSACTION_API.SET_NUMBER_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_LOGIN_PERSON_ID',
            P_VALUE                 => REQUEST_REC.PERSON_ID);

         HR_TRANSACTION_API.SET_NUMBER_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_PERSON_ANALYSIS_ID',
            P_VALUE                 => 0);

         HR_TRANSACTION_API.SET_NUMBER_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_PEA_OBJECT_VERSION_NUMBER',
            P_VALUE                 => 0);

         HR_TRANSACTION_API.SET_DATE_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_DATE_FROM',
            P_VALUE                 => SYSDATE);


         HR_TRANSACTION_API.SET_DATE_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_DATE_TO',
            P_VALUE                 => NULL);


         HR_TRANSACTION_API.SET_DATE_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_EFFECTIVE_DATE',
            P_VALUE                 => SYSDATE);


         HR_TRANSACTION_API.SET_NUMBER_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ANALYSIS_CRITERIA_ID',
            P_VALUE                 => V_ANALYSIS_CRITERIA_ID);

         HR_TRANSACTION_API.SET_NUMBER_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_OLD_ANALYSIS_CRITERIA_ID',
            P_VALUE                 => V_ANALYSIS_CRITERIA_ID);

         HR_TRANSACTION_API.SET_NUMBER_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_BUSINESS_GROUP_ID',
            P_VALUE                 => REQUEST_REC.BUSINESS_GROUP_ID);
         HR_TRANSACTION_API.SET_NUMBER_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ID_FLEX_NUM',
            P_VALUE                 => V_ID_FLEX_NUM);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_STRUCTURE_CODE',
            P_VALUE                 => V_SIT_CODE);


         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_STRUCTURE_NAME',
            P_VALUE                 => P_SIT_TITLE); 

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ACTION',
            P_VALUE                 => 'INSERT');

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_REVIEW_ACTID',
            P_VALUE                 => '313903');-----------------------------------------------------------------------

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE_CATEGORY',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE1',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE2',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE3',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE4',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE5',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE6',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE7',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE8',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE9',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE10',
            P_VALUE                 => NULL);


         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE11',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE12',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE13',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE14',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE15',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE16',
            P_VALUE                 => NULL);


         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE17',
            P_VALUE                 => NULL);


         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE18',
            P_VALUE                 => NULL);


         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE19',
            P_VALUE                 => NULL);

         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_ATTRIBUTE20',
            P_VALUE                 => NULL);


         HR_TRANSACTION_API.SET_VARCHAR2_VALUE (
            P_TRANSACTION_STEP_ID   => V_TRANSACTION_STEP_ID,
            P_PERSON_ID             => REQUEST_REC.PERSON_ID,
            P_NAME                  => 'P_REVIEW_PROC_CALL',
            P_VALUE                 => 'HrSit');

        ----------------------------------------------------------Start Workflow----------------------------------------------------------------------
        HR_APPROVAL_SS.STARTGENERICAPPROVALPROCESS (
            V_TRANSACTION_ID,
            V_ITEM_KEY,
            V_NTFSUBMSG,
            'HR_RELAUNCH_SS',
            VT_ADDITIONAL_WF_ATTRIBUTES,
            V_STATUS,
            V_ERROR_MESSAGE,
            V_ERRSTACK);

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_OAF_EDIT_URL_ATTR',
            TEXT_VALUE     => 'HR_RELAUNCH_SS',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         -- set HR_OAF_NAVIGATION_ATTR
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_OAF_NAVIGATION_ATTR',
            TEXT_VALUE     => 'N',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         -- set HR_REVIEW_TEMPLATE_RN_ATTR
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_REVIEW_TEMPLATE_RN_ATTR',
            TEXT_VALUE     => V_REVIEW_TEMPLATE_RN,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_NTF_ATTACHMENTS_ATTR',
            TEXT_VALUE     => V_NTF_ATTACH_ATTR,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_RUNTIME_APPROVAL_REQ_FLAG',
            TEXT_VALUE     => V_APPROVAL_REQUIRED,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         -- set AME params
         -- 'HR_AME_APP_ID_ATTR'
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_AME_APP_ID_ATTR',
            TEXT_VALUE     => NULL,
            NUMBER_VALUE   => V_AMETRANAPPID,
            DATE_VALUE     => NULL);
         -- 'HR_AME_TRAN_TYPE_ATTR'
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_AME_TRAN_TYPE_ATTR',
            TEXT_VALUE     => V_AMETRANSTYPE,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         -- TRANSACTION_ID
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'TRANSACTION_ID',
            TEXT_VALUE     => NULL,
            NUMBER_VALUE   => V_TRANSACTION_ID,
            DATE_VALUE     => NULL);

         -- TRAN_SUBMIT
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'TRAN_SUBMIT',
            TEXT_VALUE     => 'Y',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         
         -- PROCESS_DISPLAY_NAME
         FND_MESSAGE.SET_NAME ('PER', V_NTFSUBMSG);    

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'PROCESS_DISPLAY_NAME',
            --TEXT_VALUE     => FND_MESSAGE.GET,  commented by Amr Helaly 31-03-2020 to correct notify owner 
              TEXT_VALUE     => P_SIT_TITLE,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_PERZ_FUNCTION_NAME_ATTR',
            TEXT_VALUE     => V_PERZ_FUNC,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_PERZ_LOCALIZATION_CODE_ATTR',
            TEXT_VALUE     => REQUEST_REC.LEGISLATION_CODE,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);

         HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_PERZ_ORGANIZATION_ID_ATTR',
            TEXT_VALUE     => REQUEST_REC.BUSINESS_GROUP_ID,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
            
        HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'PROCESS_NAME',
            TEXT_VALUE     => V_PROCESS_NAME,
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
            
        HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'P_EFFECTIVE_DATE',
            TEXT_VALUE     => TO_CHAR(SYSDATE,'YYYY-MM-DD'),
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
             
              HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'HR_REVIEW_HEADER_STYLE',
            TEXT_VALUE     => 'PER_HEADER',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
            
              
       /*  HR_APPROVAL_SS.CREATE_ITEM_ATTRIB_IF_NOTEXIST (
            ITEMTYPE       => V_ITEM_TYPE,
            ITEMKEY        => V_ITEM_KEY,
            ANAME          => 'FYI_NTF_DETAILS',
            TEXT_VALUE     => 'Y',
            NUMBER_VALUE   => NULL,
            DATE_VALUE     => NULL);
         */   

         WF_ENGINE.COMPLETEACTIVITY (
            ITEMTYPE   => V_ITEM_TYPE,
            ITEMKEY    => V_ITEM_KEY,
            ACTIVITY   => 'HR_BLOCK',
            RESULT     => WF_ENGINE.ENG_NULL);
                
         COMMIT;


    END LOOP;
    P_TRANSACTION_ID:=V_TRANSACTION_ID;
    P_PROCESS_NAME:=V_PROCESS_NAME;
    P_ITEM_KEY :=V_ITEM_KEY;
  --  RETURN V_ITEM_KEY;
      RETURN 'SUCCESS';
EXCEPTION
    WHEN OTHERS THEN
    --NULL;
    DBMS_OUTPUT.PUT_LINE (SQLERRM);
    RETURN -1;
  -- RETURN SQLERRM;
END START_SIT_WORKFLOW;

     FUNCTION GET_ACTION_HISTORY_LIST(
                                    P_NOTIFICATION_ID     NUMBER,
                                    P_ITEM_TYPE           VARCHAR2,
                                    P_ITEM_KEY            VARCHAR2,
                                    P_ACTIVITY_ID         NUMBER,
                                    P_FILTER_TYPE         VARCHAR2  ) RETURN MBC_ACTION_HISTORY_LIST_SS
                                    
    IS 
    V_MBC_ACTION_HISTORY_LIST_SS                MBC_ACTION_HISTORY_LIST_SS;
    BEGIN
    
    
    
    
     
    IF (P_FILTER_TYPE='PO')
    THEN 
    SELECT MBC_ACTION_HISTORY_LIST_OBJ ( Sequence    , who  ,Action    , Note  , Action_date  )
    BULK COLLECT INTO V_MBC_ACTION_HISTORY_LIST_SS
    FROM ( SELECT *
              FROM (SELECT poh.SEQUENCE_NUM Sequence,
                              per.first_name ||' '|| per.last_name Who,
                             poh.ACTION_DATE Action_date,
                             polc.displayed_field Action,
                             poh.NOTE Note
                        FROM po_action_history poh,
                             per_all_people_f per,
                             po_lookup_codes polc,
                             po_headers_all pha
                       WHERE     poh.action_code = polc.lookup_code
                             AND POLC.LOOKUP_TYPE IN ('APPROVER ACTIONS', 'CONTROL ACTIONS')
                             AND Object_type_code = 'PO'--REQUISITION  --PA
                             AND object_sub_type_code = pha.type_lookup_code
                             AND object_id = pha.po_header_id
                             AND per.person_id(+) = poh.employee_id
                             AND TRUNC (SYSDATE) BETWEEN per.effective_start_date(+)
                                                     AND per.effective_end_date(+)
                             AND OBJECT_ID = (wf_notification.getattrnumber ( P_NOTIFICATION_ID  , 'DOCUMENT_ID'))) QRSLT
            ORDER BY 1-- DESC
            );
    
    ELSIF (P_FILTER_TYPE='Requisition')
    THEN
    SELECT MBC_ACTION_HISTORY_LIST_OBJ ( sequence_num    , approver   ,approval_status_label    ,  notes  , approval_date  )
     BULK COLLECT INTO V_MBC_ACTION_HISTORY_LIST_SS
  FROM (SELECT document_id,
               approver,
               sequence_num,
               approval_status,
               approval_status_label,
               approval_date,
               notes,
               business_group_name,
               DECODE (approval_status,
                       'PENDING', 'OraDataText',
                       'OraInstructionText')
                  AS css_style,
               approver_email,
               NVL2 (
                  approval_group_id,
                  por_ame_approval_list.get_approval_group_name (
                     approval_group_id),
                  '')
                  AS approval_group_name,
               NULL AS approver_display_name,
               DECODE (approval_status,
                       'SUBMIT', 1,
                       'SUBMIT CHANGE', 1,
                       'RESERVE', 2,
                       'APPROVE', 3,
                       'REJECT', 4,
                       'NO ACTION', 5,
                       'PENDING', 6,
                       7)
                  AS action_sort_column
          FROM por_approval_status_lines_v
         WHERE document_id = (wf_notification.getattrnumber (P_NOTIFICATION_ID, 'DOCUMENT_ID'))) ;




    
    ELSIF (P_FILTER_TYPE='HR')
    THEN 
    
       
    
  
   SELECT MBC_ACTION_HISTORY_LIST_OBJ (   rownum    ,l_role  ,l_action   , l_comments  ,l_date   )
   BULK COLLECT INTO V_MBC_ACTION_HISTORY_LIST_SS
   FROM    (
        SELECT  DISTINCT
                wn.notification_id l_notification_id
               ,wf_directory.getroledisplayname (nvl (wn.more_info_role
                                                     ,wn.recipient_role)) l_role
               ,decode (l.lookup_code
                       ,'SFL'
                       ,(hr_general.decode_lookup ('PQH_SS_APPROVAL_STATUS'
                                                 ,'PENDING'))
                       ,decode (wn.status
                               ,'CANCELED'
                               ,(hr_general.decode_lookup ('PQH_SS_APPROVAL_STATUS'
                                                         ,'BEATEN'))
                               ,l.meaning)) l_action
               ,na.text_value l_comments
               ,ias.end_date l_date
        FROM    wf_activities a
               ,wf_notification_attributes na
               ,wf_process_activities pa
               ,wf_items i
               ,wf_item_activity_statuses ias
               ,wf_lookups_tl l
               ,wf_user_roles wur
               ,wf_notifications wn
        WHERE   i.item_type = P_item_type
        AND     i.item_key = P_item_key
        AND     ias.item_type = i.item_type
        AND     ias.item_key = i.item_key
        AND     ias.notification_id IS NOT NULL
        AND     wn.notification_id = na.notification_id (+)
        AND     na.name (+) = 'WF_NOTE'
        AND     nvl (ias.activity_result_code
                    ,'A') NOT IN ('SFL','RESUBMIT')
        AND     ias.process_activity = pa.instance_id
        AND     pa.activity_name = a.name
        AND     pa.activity_item_type = a.item_type
        AND     a.result_type NOT IN ('*','HR_DONE')
        AND     i.begin_date BETWEEN a.begin_date
                             AND     nvl (a.end_date
                                         ,i.begin_date)
        AND     a.result_type = l.lookup_type
        AND     nvl (ias.activity_result_code
                    ,'SFL') = l.lookup_code
        AND     l.language = userenv('LANG')
        AND     ias.assigned_user = wur.role_name
        AND     ias.notification_id = wn.group_id       
        UNION
        SELECT  DISTINCT
                wn.notification_id l_notification_id
               ,wf_directory.getroledisplayname (wn.recipient_role) l_role
               ,decode (l.lookup_code
                       ,'SFL'
                       ,(hr_general.decode_lookup ('PQH_SS_APPROVAL_STATUS'
                                                 ,'PENDING'))
                       ,decode (wn.status
                               ,'CANCELED'
                               ,(hr_general.decode_lookup ('PQH_SS_APPROVAL_STATUS'
                                                         ,'BEATEN'))
                               ,l.meaning)) l_action
               ,decode (wn.status
                       ,'CANCELED'
                       ,NULL
                       ,nvl (na.text_value
                            ,
                             (
                             SELECT  user_comment
                             FROM    wf_comments
                             WHERE   notification_id = wn.notification_id
                             AND     action = l.lookup_code
                             AND     rownum = 1
                             ))) l_comments
               ,nvl (ias.end_date
                    ,ias.begin_date) l_date
        FROM    wf_activities a
               ,wf_notification_attributes na
               ,wf_process_activities pa
               ,wf_items i
               ,wf_item_activity_statuses_h ias
               ,wf_lookups_tl l
               ,wf_user_roles wur
               ,wf_notifications wn
        WHERE   i.item_type = P_item_type
        AND     i.item_key = P_item_key
        AND     ias.item_type = i.item_type
        AND     ias.item_key = i.item_key
        AND     ias.notification_id IS NOT NULL
        AND     wn.notification_id = na.notification_id (+)
        AND     na.name (+) = 'WF_NOTE'
        AND     nvl (ias.activity_result_code
                    ,'A') NOT IN ('SFL','RESUBMIT')
        AND     ias.process_activity = pa.instance_id
        AND     pa.activity_name = a.name
        AND     pa.activity_item_type = a.item_type
        AND     a.result_type NOT IN ('*','HR_DONE')
        AND     i.begin_date BETWEEN a.begin_date
                             AND     nvl (a.end_date
                                         ,i.begin_date)
        AND     a.result_type = l.lookup_type
        AND     nvl (ias.activity_result_code
                    ,'SFL') = l.lookup_code
        AND     l.language = userenv('LANG')
        AND     ias.assigned_user = wur.role_name
        AND     ias.notification_id = wn.group_id
        UNION
        SELECT  wn.notification_id l_notification_id
               ,wf_directory.getroledisplayname (nvl (wn.more_info_role
                                                     ,wn.recipient_role)) l_role
              ,decode(pah.action
                    ,'SFL'
                    ,(hr_general.decode_lookup ('PQH_SS_APPROVAL_STATUS'
                                 ,pah.action))        
                    ,decode (wn.status
                       ,'CANCELED'
                       ,decode (pah.action
                               ,'TIMEOUT'
                               ,(hr_general.decode_lookup ('PQH_SS_APPROVAL_STATUS'
                                                         ,pah.action))
                               ,(hr_general.decode_lookup ('PQH_SS_APPROVAL_STATUS'
                                                         ,'BEATEN')))
                       ,(hr_general.decode_lookup ('PQH_SS_APPROVAL_STATUS'
                                                 ,pah.action)))) l_action
               ,decode (ame.notification_id
                       ,wn.notification_id
                       ,ame.user_comments
                       ,NULL) l_comments
               ,pah.last_update_date l_date
        FROM    pqh_ss_approval_history pah
               ,wf_notifications wn
               ,ame_trans_approval_history ame
        WHERE   pah.transaction_item_type = P_item_type
        AND     pah.transaction_item_key = P_item_key
        AND     pah.action IN ('TIMEOUT','RESUBMIT','SFL')
             AND  ame.notification_id=pah.notification_id
             AND  ame.name=pah.user_name
        AND     wn.group_id =  
                (
                SELECT  group_id
                FROM    wf_notifications
                WHERE   notification_id = pah.notification_id
                AND     rownum = 1
                )
        UNION 
    select wn.notification_id l_notification_id
               ,wf_directory.getroledisplayname (C.FROM_ROLE) l_role
              -- ,WF_CORE.TRANSLATE(C.ACTION) l_action
                             ,C.ACTION l_action
               ,C.USER_COMMENT l_comments
              -- ,nvl (ias.end_date,ias.begin_date) l_date
                         ,c.comment_date l_date
        from WF_ITEM_ACTIVITY_STATUSES_H IAS,
             WF_COMMENTS C,
             wf_notifications wn
       where IAS.ITEM_TYPE = P_ITEM_TYPE
          and IAS.ITEM_KEY = P_ITEM_KEY
          and IAS.NOTIFICATION_ID = wn.group_id
          and wn.notification_id = c.notification_id
          and C.ACTION in('QUESTION', 'ANSWER')
    UNION 
    select wn.notification_id l_notification_id
               ,wf_directory.getroledisplayname (C.FROM_ROLE) l_role
              -- ,WF_CORE.TRANSLATE(C.ACTION) l_action
                             ,C.ACTION l_action
               ,C.USER_COMMENT l_comments
              -- ,nvl (ias.end_date,ias.begin_date) l_date
                         ,c.comment_date l_date
        from WF_ITEM_ACTIVITY_STATUSES IAS,
             WF_COMMENTS C,
             wf_notifications wn
       where IAS.ITEM_TYPE = P_ITEM_TYPE
          and IAS.ITEM_KEY = P_ITEM_KEY
          and IAS.NOTIFICATION_ID = wn.group_id
          and wn.notification_id = c.notification_id
          and C.ACTION in('QUESTION', 'ANSWER')          
        UNION
        SELECT  0
               ,wf_directory.getroledisplayname (owner_role) l_role
               ,wf_core.translate ('SUBMIT')
               ,appr.text_value note
               ,begin_date l_date
        FROM    wf_items i
               ,wf_item_attribute_values appr
        WHERE   i.item_type = P_item_type
        AND     i.item_key = P_item_key
        AND     i.item_type = appr.item_type (+)
        AND     i.item_key = appr.item_key (+)
        AND     appr.name (+) = 'SUBMIT_COMMENTS'
        ORDER BY l_date, l_notification_id
        ) a ;

    ELSIF (UPPER(P_FILTER_TYPE)='OVERTIME') THEN 
    
        SELECT MBC_ACTION_HISTORY_LIST_OBJ ( STEP_NUMBER, PAPF.FIRST_NAME ||' '|| PAPF.LAST_NAME , ACTION_CODE, APPROVER_COMMENT, ACTION_DATE)
        BULK COLLECT INTO V_MBC_ACTION_HISTORY_LIST_SS
        FROM XXOT_REQUEST_APPROVAL_HISTORY hstr, per_all_people_f papf 
        WHERE     hstr.approver_per_id = papf.person_id 
        AND SYSDATE BETWEEN papf.effective_start_date AND papf.effective_end_date 
        AND OT_REQUEST_ID = TO_NUMBER(P_ITEM_KEY)
        AND ACTION_CODE IS NOT NULL
        ORDER BY OT_REQUEST_ID, step_number;

    ELSE   -- OTHERS 
     
        
       
 SELECT MBC_ACTION_HISTORY_LIST_OBJ (   rownum      ,H_FROM_USER   ,H_ACTION    , H_COMMENT   , H_ACTION_DATE   )
 BULK COLLECT INTO V_MBC_ACTION_HISTORY_LIST_SS
  FROM (  SELECT H_NOTIFICATION_ID,
                 H_FROM_ROLE,
                 H_FROM_USER,
                 H_TO_ROLE,
                 H_TO_USER,
                 H_ACTION,
                 H_COMMENT,
                 H_ACTION_DATE
            FROM (SELECT 99999999 H_SEQUENCE,
                         IAS.NOTIFICATION_ID H_NOTIFICATION_ID,
                         C.FROM_ROLE H_FROM_ROLE,
                         C.FROM_USER H_FROM_USER,
                         'WF_SYSTEM' H_TO_ROLE,
                         WF_CORE.TRANSLATE ('WF_SYSTEM') H_TO_USER,
                         wf_core.activity_result (A.RESULT_TYPE,
                                                  IAS.ACTIVITY_RESULT_CODE)
                            H_ACTION,
                         (SELECT text_value
                            FROM wf_notification_attributes
                           WHERE     notification_id = IAS.NOTIFICATION_ID
                                 AND name = 'WF_NOTE')
                            H_COMMENT,
                         NVL (IAS.END_DATE, IAS.BEGIN_DATE) H_ACTION_DATE
                    FROM WF_ITEM_ACTIVITY_STATUSES IAS,
                         WF_ACTIVITIES A,
                         WF_PROCESS_ACTIVITIES PA,
                         WF_ITEMS I,
                         WF_COMMENTS C
                   WHERE     IAS.ITEM_TYPE = P_ITEM_TYPE
                         AND IAS.ITEM_KEY = P_ITEM_KEY
                         AND IAS.PROCESS_ACTIVITY = P_ACTIVITY_ID
                         AND IAS.ITEM_TYPE = I.ITEM_TYPE
                         AND IAS.ITEM_KEY = I.ITEM_KEY
                         AND IAS.ACTIVITY_RESULT_CODE IS NOT NULL
                         AND IAS.ACTIVITY_RESULT_CODE NOT IN ('#EXCEPTION',
                                                              '#FORCE',
                                                              '#MAIL',
                                                              '#NULL',
                                                              '#STUCK',
                                                              '#TIMEOUT')
                         AND I.BEGIN_DATE BETWEEN A.BEGIN_DATE
                                              AND NVL (A.END_DATE,
                                                       I.BEGIN_DATE)
                         AND IAS.PROCESS_ACTIVITY = PA.INSTANCE_ID
                         AND PA.ACTIVITY_NAME = A.NAME
                         AND PA.ACTIVITY_ITEM_TYPE = A.ITEM_TYPE
                         AND IAS.NOTIFICATION_ID = C.NOTIFICATION_ID
                         AND C.ACTION IN ('RESPOND',
                                          'RESPOND_WA',
                                          'RESPOND_RULE')
                  UNION ALL
                  SELECT 99999999 H_SEQUENCE,
                         IAS.NOTIFICATION_ID H_NOTIFICATION_ID,
                         C.FROM_ROLE H_FROM_ROLE,
                         C.FROM_USER H_FROM_USER,
                         'WF_SYSTEM' H_TO_ROLE,
                         WF_CORE.TRANSLATE ('WF_SYSTEM') H_TO_USER,
                         wf_core.activity_result (A.RESULT_TYPE,
                                                  IAS.ACTIVITY_RESULT_CODE)
                            H_ACTION,
                         (SELECT text_value
                            FROM wf_notification_attributes
                           WHERE     notification_id = IAS.NOTIFICATION_ID
                                 AND name = 'WF_NOTE')
                            H_COMMENT,
                         NVL (IAS.END_DATE, IAS.BEGIN_DATE) H_ACTION_DATE
                    FROM WF_ITEM_ACTIVITY_STATUSES_H IAS,
                         WF_ACTIVITIES A,
                         WF_PROCESS_ACTIVITIES PA,
                         WF_ITEMS I,
                         WF_COMMENTS C
                   WHERE     IAS.ITEM_TYPE = P_ITEM_TYPE
                         AND IAS.ITEM_KEY = P_ITEM_KEY
                         AND IAS.PROCESS_ACTIVITY = P_ACTIVITY_ID
                         AND IAS.ITEM_TYPE = I.ITEM_TYPE
                         AND IAS.ITEM_KEY = I.ITEM_KEY
                         AND IAS.ACTIVITY_RESULT_CODE IS NOT NULL
                         AND IAS.ACTIVITY_RESULT_CODE NOT IN ('#EXCEPTION',
                                                              '#FORCE',
                                                              '#MAIL',
                                                              '#NULL',
                                                              '#STUCK',
                                                              '#TIMEOUT')
                         AND I.BEGIN_DATE BETWEEN A.BEGIN_DATE
                                              AND NVL (A.END_DATE,
                                                       I.BEGIN_DATE)
                         AND IAS.PROCESS_ACTIVITY = PA.INSTANCE_ID
                         AND PA.ACTIVITY_NAME = A.NAME
                         AND PA.ACTIVITY_ITEM_TYPE = A.ITEM_TYPE
                         AND IAS.NOTIFICATION_ID = C.NOTIFICATION_ID
                         AND C.ACTION IN ('RESPOND',
                                          'RESPOND_WA',
                                          'RESPOND_RULE')
                  UNION ALL
                  SELECT C.SEQUENCE H_SEQUENCE,
                         C.NOTIFICATION_ID H_NOTIFICATION_ID,
                         C.FROM_ROLE H_FROM_ROLE,
                         C.FROM_USER H_FROM_USER,
                         C.TO_ROLE H_TO_ROLE,
                         C.TO_USER H_TO_USER,
                         WF_CORE.TRANSLATE (C.ACTION) H_ACTION,
                         C.USER_COMMENT H_COMMENT,
                         C.COMMENT_DATE H_ACTION_DATE
                    FROM WF_ITEM_ACTIVITY_STATUSES IAS, WF_COMMENTS C
                   WHERE     IAS.ITEM_TYPE = P_ITEM_TYPE
                         AND IAS.ITEM_KEY = P_ITEM_KEY
                         AND IAS.PROCESS_ACTIVITY = P_ACTIVITY_ID
                         AND IAS.NOTIFICATION_ID = C.NOTIFICATION_ID
                         AND C.ACTION NOT IN ('RESPOND',
                                              'RESPOND_WA',
                                              'RESPOND_RULE',
                                              'SEND')
                  UNION ALL
                  SELECT C.SEQUENCE H_SEQUENCE,
                         C.NOTIFICATION_ID H_NOTIFICATION_ID,
                         C.FROM_ROLE H_FROM_ROLE,
                         C.FROM_USER H_FROM_USER,
                         C.TO_ROLE H_TO_ROLE,
                         C.TO_USER H_TO_USER,
                         WF_CORE.TRANSLATE (C.ACTION) H_ACTION,
                         C.USER_COMMENT H_COMMENT,
                         C.COMMENT_DATE H_ACTION_DATE
                    FROM WF_ITEM_ACTIVITY_STATUSES_H IAS, WF_COMMENTS C
                   WHERE     IAS.ITEM_TYPE = P_ITEM_TYPE
                         AND IAS.ITEM_KEY = P_ITEM_KEY
                         AND IAS.PROCESS_ACTIVITY = P_ACTIVITY_ID
                         AND IAS.NOTIFICATION_ID = C.NOTIFICATION_ID
                         AND C.ACTION NOT IN ('RESPOND',
                                              'RESPOND_WA',
                                              'RESPOND_RULE',
                                              'SEND'))
        ORDER BY H_ACTION_DATE, H_NOTIFICATION_ID, H_SEQUENCE) ;


    
    END IF;
    
    RETURN V_MBC_ACTION_HISTORY_LIST_SS;
    
    END GET_ACTION_HISTORY_LIST;
    
    

    PROCEDURE INSERT_HAPPINESS(
                                P_APPLICATION_ID        IN  NUMBER,
                                P_PERSON_ID             IN  NUMBER,
                                P_FACE_ID               IN  NUMBER,
                                P_COMMENTS              IN  VARCHAR2,
                                P_USER_ID               IN  NUMBER
                               ) IS
    BEGIN
        INSERT INTO MBC_HAPPINESS_METER (REQUEST_ID,APPLICATION_ID,PERSON_ID,FACE_ID,COMMENTS,CREATED_BY,CREATION_DATE)  
        VALUES (MBC_HAPPINESS_METER_SEQ.NEXTVAL,1,P_PERSON_ID,P_FACE_ID,P_COMMENTS ,P_USER_ID,SYSDATE );
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
        NULL;
    END INSERT_HAPPINESS ;                                
    
    PROCEDURE INSERT_CARD(
                            P_FROM_PERSON_ID    IN  NUMBER,
                            P_TO_PERSON_ID      IN  NUMBER,
                            P_SUBJECT_ID        IN  NUMBER,
                            P_COMMENTS          IN  VARCHAR2,
                            P_USER_ID           IN  NUMBER
                           ) IS
    BEGIN
        INSERT INTO MBC_THANK_YOU_CARDS (CARD_ID,FROM_PERSON_ID,TO_PERSON_ID,SUBJECT_ID, COMMENTS,CREATED_BY,CREATION_DATE)
        VALUES (MBC_THANK_YOU_CARDS_SEQ.NEXTVAL,P_FROM_PERSON_ID,P_TO_PERSON_ID,P_SUBJECT_ID, P_COMMENTS,P_USER_ID,SYSDATE);
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
        NULL;
    END INSERT_CARD; 
    
       FUNCTION CKECK_GIVEN_THANKS_CARD (P_PERSON_ID             IN  NUMBER
                                )
      RETURN VARCHAR2
   IS
   V_COUNT NUMBER;

   
   BEGIN
    SELECT COUNT(*)
    INTO V_COUNT 
    FROM MBC_THANK_YOU_CARDS 
    WHERE FROM_PERSON_ID=P_PERSON_ID
    AND TO_CHAR (CREATION_DATE,'RRRR')  = TO_CHAR (SYSDATE,'RRRR');
    
    IF NVL(V_COUNT,0) >= CONSTANT_MAX_THANKS THEN  -- TEMP 
     RETURN 'You can''t add more than '||CONSTANT_MAX_THANKS;
    END IF;
    RETURN 'Success';
   
   END CKECK_GIVEN_THANKS_CARD;
   
    FUNCTION GET_MAX_THANKS_CONST   RETURN NUMBER
    IS 
    
    BEGIN 
    RETURN CONSTANT_MAX_THANKS;
    
    END GET_MAX_THANKS_CONST;
   

    FUNCTION ADD_VACATION_RULE (P_USER_NAME        VARCHAR2,
                                 P_FROM_DATE         VARCHAR2,
                                 P_TO_DATE           VARCHAR2,
                                 P_ITEM_TYPE         VARCHAR2,
                                 P_TO_USERNAME       VARCHAR2,
                                 P_COMMENT           VARCHAR2) RETURN VARCHAR2
    IS
      V_RULE_ID     NUMBER;
      
    BEGIN

        SELECT WF_ROUTING_RULES_S.NEXTVAL INTO V_RULE_ID FROM SYS.DUAL;
        
        INSERT INTO WF_ROUTING_RULES (RULE_ID,
                                      ROLE,
                                      ACTION,
                                      BEGIN_DATE,
                                      END_DATE,
                                      MESSAGE_TYPE,
                                      MESSAGE_NAME,
                                      ACTION_ARGUMENT,
                                      RULE_COMMENT)
        VALUES                       (V_RULE_ID,
                                      P_USER_NAME,
                                      'FORWARD',
                                      FND_CONC_DATE.STRING_TO_DATE (P_FROM_DATE),
                                      FND_CONC_DATE.STRING_TO_DATE (P_TO_DATE),
                                      P_ITEM_TYPE,
                                      NULL,
                                      P_TO_USERNAME,
                                      P_COMMENT);
                                      
        COMMIT;

        Return 'Success';

    EXCEPTION
        WHEN OTHERS THEN
        return sqlerrm;
    END ADD_VACATION_RULE;

    FUNCTION UPDATE_VACATION_RULE (
                                    P_RULE_ID           NUMBER,
                                    P_FROM_DATE         VARCHAR2,
                                    P_TO_DATE           VARCHAR2,
                                    P_TO_USERNAME       VARCHAR2,
                                    P_COMMENT           VARCHAR2) RETURN VARCHAR2
    IS
    BEGIN
          
      
       UPDATE WF_ROUTING_RULES
          SET BEGIN_DATE = FND_CONC_DATE.STRING_TO_DATE (P_FROM_DATE),
              END_DATE = FND_CONC_DATE.STRING_TO_DATE (P_TO_DATE),
              ACTION_ARGUMENT = P_TO_USERNAME,
              RULE_COMMENT = P_COMMENT
        WHERE RULE_ID = P_RULE_ID;
                    
        COMMIT;
    Return 'Success';

    EXCEPTION
      WHEN OTHERS THEN
     return sqlerrm;
    END UPDATE_VACATION_RULE;

    FUNCTION DELETE_VACATION_RULE (P_RULE_ID          NUMBER) RETURN VARCHAR2
    IS
    BEGIN
          
        DELETE FROM WF_ROUTING_RULES
        WHERE RULE_ID = P_RULE_ID;
            
        COMMIT; 
        Return 'Success';
              EXCEPTION
      WHEN OTHERS THEN
     return sqlerrm;
    END DELETE_VACATION_RULE;
       
    -- at the begging
FUNCTION do_reverse (p_str VARCHAR)
    RETURN VARCHAR
IS
    len  NUMBER;
    str1     VARCHAR (4000);
BEGIN
    len := LENGTH (p_str);

    FOR i IN REVERSE 1 .. len
    LOOP
        str1 := str1 || SUBSTR (p_str, i, 1);
    END LOOP;

    RETURN str1;
END;


 PROCEDURE LOG_MOBILE_ACTIVITY(
                                P_APPLICATION_ID        IN  NUMBER,
                                P_ACTIVITY_NAME         IN  VARCHAR2,
                                P_LOG_TYPE              IN  VARCHAR2,
                                P_DESCRIPTION           IN  VARCHAR2
                               ) IS
       
    BEGIN
    

    
        INSERT INTO MBC_MOBILE_ACTIVITY (ID,APPLICATION_ID,PERSON_ID,USER_ID,ACTIVITY_NAME,CREATED_BY,CREATION_DATE,LOG_TYPE,DESCRIPTION,SESSION_ID)  
        VALUES (MBC_MOBILE_ACTIVITY_SEQ.NEXTVAL,P_APPLICATION_ID,MBC_MOBILE_PKG.PERSON_ID,MBC_MOBILE_PKG.USER_ID,P_ACTIVITY_NAME ,MBC_MOBILE_PKG.USER_ID,SYSDATE,P_LOG_TYPE,P_DESCRIPTION,MBC_MOBILE_PKG.SESSION_ID );
        
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
        NULL;
    END LOG_MOBILE_ACTIVITY ;   
       
 FUNCTION ENCRYPT_DATA (P_KEY VARCHAR2 , P_IV VARCHAR2 , P_DATA VARCHAR2) RETURN VARCHAR2
 IS 
        V_VAL RAW(4000);
        V_ENCRYPTED RAW(4000);
        V_CHARACTER_SET VARCHAR2(10);
        V_STRING VARCHAR2(32);
        V_KEY RAW(250);
        V_ENCRYPTION_TYPE PLS_INTEGER;
        V_IV RAW(250);
      BEGIN


        V_KEY := UTL_I18N.STRING_TO_RAW
                            ( DATA => P_KEY,
                              DST_CHARSET => 'AL32UTF8' );
        V_ENCRYPTION_TYPE := DBMS_CRYPTO.ENCRYPT_AES256 
                                        + DBMS_CRYPTO.CHAIN_CBC 
                                        + DBMS_CRYPTO.PAD_PKCS5;
        V_VAL := UTL_I18N.STRING_TO_RAW
                  ( DATA => P_DATA,
                    DST_CHARSET => 'AL32UTF8' );


     V_IV := UTL_I18N.STRING_TO_RAW
                            ( DATA => P_IV,
                              DST_CHARSET => 'AL32UTF8' );

        V_ENCRYPTED := DBMS_CRYPTO.ENCRYPT
                       ( SRC => V_VAL,
                         TYP => V_ENCRYPTION_TYPE,
                         KEY => V_KEY , IV=> V_IV );

   RETURN  replace ( replace ( UTL_RAW.CAST_TO_VARCHAR2 ( UTL_ENCODE.BASE64_enCODE(V_ENCRYPTED) ) , chr(13) ,'' ) , chr(10) ,'' );
   
   --EXCEPTION WHEN OTHERS THEN RETURN sqlerrm;
       
          
    END; 
    
     FUNCTION DECRYPT_DATA (P_KEY VARCHAR2 , P_IV VARCHAR2 , P_DATA RAW) RETURN VARCHAR2
 IS 
        V_VAL VARCHAR2(4000);
        V_DECRYPTED RAW(500);
        V_CHARACTER_SET VARCHAR2(10);
        V_STRING VARCHAR2(32);
        V_KEY RAW(250);
        V_ENCRYPTION_TYPE PLS_INTEGER;
        V_IV RAW(250);
      BEGIN


        V_KEY := UTL_I18N.STRING_TO_RAW
                            ( DATA => P_KEY,
                              DST_CHARSET => 'AL32UTF8' );
        V_ENCRYPTION_TYPE := DBMS_CRYPTO.ENCRYPT_AES256 
                                        + DBMS_CRYPTO.CHAIN_CBC 
                                        + DBMS_CRYPTO.PAD_PKCS5;
       
     V_IV := UTL_I18N.STRING_TO_RAW
                            ( DATA => P_IV,
                              DST_CHARSET => 'AL32UTF8' );

        V_DECRYPTED := DBMS_CRYPTO.DECRYPT
                       ( SRC => P_DATA,
                         TYP => V_ENCRYPTION_TYPE,
                         KEY => V_KEY , IV=> V_IV );

   V_VAL := UTL_I18N.RAW_TO_CHAR
                  ( DATA => V_DECRYPTED,
                    SRC_CHARSET => 'AL32UTF8' );
   
        RETURN V_VAL;
         EXCEPTION WHEN OTHERS THEN RETURN 'ERROR';
               
    END DECRYPT_DATA; 
   
   function is_locked_user ( p_user_name VARCHAR2 ) return boolean
   is 
   V_COUNT NUMBER;
   begin
   
   
   SELECT NVL(COUNT (*) , 0)
   INTO V_COUNT 
   FROM xx_mobile_locked_users
   WHERE user_name=p_user_name
   AND TYPE='L'; 
   
   IF V_COUNT=0 THEN
   RETURN FALSE;
   ELSE
   RETURN TRUE;
   END IF;
   
   EXCEPTION 
   WHEN OTHERS THEN RETURN TRUE;
   end is_locked_user;
   
   PROCEDURE UNLOCK_USER (P_USER_NAME VARCHAR2)
    IS 
    BEGIN
             UPDATE XX_MOBILE_LOCKED_USERS
             SET TYPE='X'
             WHERE USER_NAME=P_USER_NAME ;
             COMMIT;
   END UNLOCK_USER;
   
   procedure delete_login_attemps(p_user_name VARCHAR2)
   is begin
  update xx_mobile_locked_users
  set type='X'
  where user_name=p_user_name and type='T';
  commit;
   end delete_login_attemps;
   
   procedure lock_user(p_user_name VARCHAR2)
   is
   V_COUNT number;
    begin
   
    SELECT NVL(COUNT (*) , 0)
   INTO V_COUNT 
   FROM xx_mobile_locked_users
   WHERE user_name=p_user_name
   AND TYPE='T';
   
       if (V_COUNT>=2) then
       INSERT INTO xx_mobile_locked_users (log_id ,user_name , type , CREATION_DATE  )  VALUES (xx_mobile_locked_users_s.nextval ,p_user_name , 'L' , SYSDATE );
       else
       INSERT INTO xx_mobile_locked_users (log_id ,user_name , type , CREATION_DATE )  VALUES (xx_mobile_locked_users_s.nextval ,p_user_name , 'T' , SYSDATE );
       end if;
   
   end lock_user;
   
   FUNCTION CREATE_SESSION (P_USER_NAME VARCHAR2) RETURN VARCHAR2
   IS 
   V_USER_ID NUMBER ;
   V_PERSON_ID NUMBER;
   V_ASSIGNMENT_ID  NUMBER;
   V_GLOBAL_NAME VARCHAR2(1000) ;
   V_BUSINESS_GROUP_ID NUMBER;
   V_EMPLOYEE_NUMBER VARCHAR2(500) ;
   V_USER_PERSON_TYPE  VARCHAR2(500) ;
   V_MOBILE_USER NUMBER;
   v_return VARCHAR2(500);
   v_reference XX_MOBILE_SESSION.ws_reference%TYPE;
   V_WSID      XX_MOBILE_SESSION.WSID%TYPE;
   v_iv    XX_MOBILE_SESSION.initialization_vector%TYPE;
   
   BEGIN
   
   
        SELECT PAPF.PERSON_ID,PAPF.BUSINESS_GROUP_ID,USER_ID , ASSIGNMENT_ID,  hr_person_type_usage_info.get_user_person_type (papf.effective_start_date,PAPF.person_id) USER_PERSON_TYPE , PAPF.GLOBAL_NAME , PAPF.EMPLOYEE_NUMBER
        INTO V_PERSON_ID, V_BUSINESS_GROUP_ID, V_USER_ID, V_ASSIGNMENT_ID, V_USER_PERSON_TYPE , V_GLOBAL_NAME , V_EMPLOYEE_NUMBER
        FROM FND_USER FU, PER_ALL_PEOPLE_F PAPF , PER_ALL_ASSIGNMENTS_F PAAF
        WHERE FU.EMPLOYEE_ID = PAPF.PERSON_ID
        AND PAPF.PERSON_ID = PAAF.PERSON_ID
        AND PAPF.CURRENT_EMPLOYEE_FLAG = 'Y'
        AND PAAF.PRIMARY_FLAG = 'Y'
        AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE
        AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE AND PAAF.EFFECTIVE_END_DATE
        AND FU.USER_NAME = UPPER(P_USER_NAME);
        
        
        --commented by heba 1-4-2020
        --IF V_USER_PERSON_TYPE <> 'Regular Employee' THEN
--          IF INSTR ( V_USER_PERSON_TYPE , 'Regular Employee') = 0 THEN
--            v_return:= 'This App is allowed for Regular Employees only.';
--            
--        END IF ;
        ---end commented by Heba 1-4-2020---

   if v_return is not NULL then 
   raise_application_error ( -20001, v_return );
   end if;
   
   v_wsid:='Bearer '||dbms_random.string('p',2048); --1024 p
   v_wsid:=replace (v_wsid , '+',dbms_random.string('a',1) );
   v_reference:=dbms_random.string('x',32);
   v_iv:=dbms_random.string('x',16);
   
   
   insert into xx_mobile_session (session_id , user_id ,  person_id , assignment_id , user_name , global_name , BUSINESS_GROUP_ID , employee_number ,is_active ,wsid , first_connect , last_connect ,ws_reference, initialization_vector, creation_date, CREATED_BY )
   values (xx_mobile_session_s.nextval ,v_user_id ,  v_person_id , v_assignment_id ,  UPPER(P_USER_NAME) , v_global_name , V_BUSINESS_GROUP_ID , v_employee_number,1 ,v_wsid , sysdate , sysdate ,v_reference,v_iv, sysdate, -1  );
   
    
   
   return v_wsid;
   
   --exception when others then return sqlerrm;
   
   end create_session;
   
   Function get_Timout_value return number
   is
   begin
   return (5*60);
   end get_Timout_value;
   
   FUNCTION CHECK_SESSION (P_WSID VARCHAR2 , P_SIGNATURE  VARCHAR2 , P_COMAPRE_VALUE VARCHAR2 ) RETURN VARCHAR2
   IS 
   
    V_REFERENCE XX_MOBILE_SESSION.WS_REFERENCE%TYPE;
    V_BUSINESS_GROUP_ID  XX_MOBILE_SESSION.BUSINESS_GROUP_ID%TYPE;
    V_USER_ID XX_MOBILE_SESSION.USER_ID%TYPE;
    V_LAST_CONNECT XX_MOBILE_SESSION.LAST_CONNECT%TYPE;
    V_PERSON_ID XX_MOBILE_SESSION.PERSON_ID%TYPE;
    V_ASSIGNMENT_ID XX_MOBILE_SESSION.ASSIGNMENT_ID%TYPE;
    V_EMPLOYEE_NUMBER XX_MOBILE_SESSION.EMPLOYEE_NUMBER%TYPE;
    V_USER_NAME XX_MOBILE_SESSION.USER_NAME%TYPE;
    V_GLOBAL_NAME XX_MOBILE_SESSION.GLOBAL_NAME%TYPE;
    V_IV XX_MOBILE_SESSION.INITIALIZATION_VECTOR%TYPE;
    V_SESSION_ID XX_MOBILE_SESSION.SESSION_ID%TYPE;
    v_x varchar2(4000);
    
    
    
    
   BEGIN

   
   SELECT SESSION_ID,WS_REFERENCE , BUSINESS_GROUP_ID , USER_ID  ,    LAST_CONNECT , PERSON_ID , ASSIGNMENT_ID , EMPLOYEE_NUMBER , USER_NAME , GLOBAL_NAME , INITIALIZATION_VECTOR
   INTO   V_SESSION_ID, V_REFERENCE , V_BUSINESS_GROUP_ID , V_USER_ID , V_LAST_CONNECT , V_PERSON_ID , V_ASSIGNMENT_ID , V_EMPLOYEE_NUMBER , V_USER_NAME , V_GLOBAL_NAME , V_IV
   FROM  XX_MOBILE_SESSION 
   WHERE WSID=P_WSID
   AND IS_ACTIVE=1;
     
    IF TO_NUMBER (TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'))-TO_NUMBER (TO_CHAR(V_LAST_CONNECT,'YYYYMMDDHH24MISS')) >  get_Timout_value THEN 
    RETURN 'Session Timed out';
    END IF;
    v_x:=ENCRYPT_DATA(V_REFERENCE,V_IV,P_COMAPRE_VALUE);
  

   
  
    IF  ENCRYPT_DATA(V_REFERENCE,V_IV,P_COMAPRE_VALUE)!=replace ( P_SIGNATURE ,' ','+' ) THEN 
    RETURN 'Matching false';
    END IF;
    
     
    UPDATE XX_MOBILE_SESSION
    SET LAST_CONNECT=SYSDATE
    WHERE WSID=P_WSID;
    
    -- INTILAIZAE SESSION VARIABLE
     MBC_MOBILE_PKG.USER_ID:=V_USER_ID;
     MBC_MOBILE_PKG.PERSON_ID:=V_PERSON_ID;
     MBC_MOBILE_PKG.ASSIGNMENT_ID:=V_ASSIGNMENT_ID;
     MBC_MOBILE_PKG.BUSINESS_GROUP_ID:=V_BUSINESS_GROUP_ID;
     MBC_MOBILE_PKG.EMPLOYEE_NUMBER:=V_EMPLOYEE_NUMBER;
     MBC_MOBILE_PKG.USER_NAME:=V_USER_NAME;
     MBC_MOBILE_PKG.GLOBAL_NAME:=V_GLOBAL_NAME;
     MBC_MOBILE_PKG.SESSION_ID:=V_SESSION_ID;
         
     
     
     
    
   COMMIT;
   RETURN 'OK';
   
   EXCEPTION WHEN OTHERS THEN
   v_x :=sqlerrm;
   

    RETURN 0;
   
   END CHECK_SESSION;
   
   
 FUNCTION GET_SESSION_INFO (P_WSID VARCHAR2) RETURN MBC_MOBILE_SESSION_INFO_LIST
   
   
   IS 
   
    V_MBC_MOBILE_SESSION_INFO_LIST   MBC_MOBILE_SESSION_INFO_LIST :=MBC_MOBILE_SESSION_INFO_LIST();
    V_REFERENCE XX_MOBILE_SESSION.WS_REFERENCE%TYPE;
    V_IV XX_MOBILE_SESSION.INITIALIZATION_VECTOR%TYPE;
    V_BUSINESS_GROUP_ID  XX_MOBILE_SESSION.BUSINESS_GROUP_ID%TYPE;
    V_USER_NAME XX_MOBILE_SESSION.USER_NAME%TYPE;
    V_USER_ID XX_MOBILE_SESSION.USER_ID%TYPE;
    V_PERSON_ID XX_MOBILE_SESSION.PERSON_ID%TYPE;
    V_NTF_COUNT NUMBER;
    V_IS_EXECUTIVE VARCHAR2(50);
    V_IS_AL_ARABYIA VARCHAR2(50);
    V_AA_ORG_COUNT NUMBER ;
    V_AA_ORGANIZATION NUMBER:=0;
    --Added By Heba 1-4-2020//
    V_AL_ARABYIA_APP_ROLE VARCHAR2(200):='None';
    V_PERSON_TYPE VARCHAR2(250);
    V_SHOW_MY_SPACE VARCHAR2(50):='N';
    V_SHOW_MY_TEAM VARCHAR2(50):='N';
    V_SHOW_MY_WK VARCHAR2(50):='N';
    ---End added By Heba 1-4-2020--
   
   BEGIN
   
   SELECT WS_REFERENCE , BUSINESS_GROUP_ID , USER_ID  , USER_NAME , INITIALIZATION_VECTOR , PERSON_ID
   INTO    V_REFERENCE , V_BUSINESS_GROUP_ID , V_USER_ID , V_USER_NAME , V_IV , V_PERSON_ID
   FROM  XX_MOBILE_SESSION 
   WHERE WSID=P_WSID;
   
   
   SELECT COUNT(*) NTF 
   INTO V_NTF_COUNT
   FROM MBC_WORKLIST_NOTIFICATION_V MN, MBC_WORKLIST_NTF_CONFIG_V MNB 
   WHERE MN.MESSAGE_TYPE = MNB.MESSAGE_TYPE  
   AND   MN.MESSAGE_NAME = MNB.MESSAGE_NAME 
   AND  NVL(MN.PROCESS_NAME , 1) = NVL(MNB.PROCESS_NAME ,NVL(MN.PROCESS_NAME , 1) )  
   AND    MN.RECIPIENT_ROLE =  V_USER_NAME;
   
   ----- Added By Heba  1-4/5-4-2020---------
    SELECT nvl(COUNT(FK_ORGANIZATION_ID ),0)
           INTO V_AA_ORG_COUNT 
    FROM AS_USER_ROLES
    WHERE PERSON_ID=V_PERSON_ID ;

    --AND ROLE_NAME in ('EMP','ADMIN','ADMIN-EMP','MGR-EMP','MGR');
  
 
    IF(V_AA_ORG_COUNT=1)
    then 
      SELECT nvl(MAX(FK_ORGANIZATION_ID ),0)
     INTO V_AA_ORGANIZATION 
    FROM AS_USER_ROLES
    WHERE PERSON_ID=V_PERSON_ID ;
       SELECT nvl(MAX(ROLE_NAME ),'None')
           INTO V_AL_ARABYIA_APP_ROLE  
    FROM AS_USER_ROLES
    WHERE PERSON_ID=V_PERSON_ID and FK_ORGANIZATION_ID=V_AA_ORGANIZATION;
    
    IF(V_AL_ARABYIA_APP_ROLE  like '%EMP')
    THEN 
    
    V_SHOW_MY_SPACE:='Y';
    END IF ;
    
     IF(V_AL_ARABYIA_APP_ROLE ='EMP')
    THEN 
    
    V_SHOW_MY_TEAM:='Y';
    END IF ;
       IF(V_AL_ARABYIA_APP_ROLE !='EMP')
    THEN 
    
    V_SHOW_MY_WK:='Y';
    END IF ;
    
    
    END IF ;
    
    

select hr_person_type_usage_info.get_user_person_type(sysdate,V_PERSON_ID) into V_PERSON_TYPE from dual;
IF V_PERSON_TYPE like '%Regular Employee%' THEN
V_PERSON_TYPE:='EMP';
else 
V_PERSON_TYPE:='UNEMP';

END IF ;


   ----end Commented added By Heba 1-4-2020/
   
    SELECT MAX(  NVL( (  SELECT attribute1 FROM FND_LOOKUP_VALUES    WHERE LOOKUP_TYPE = 'MBC_MOBILE_USERS' AND LANGUAGE = 'US' AND UPPER(MEANING) = UPPER(FU.USER_NAME) ) ,'N')  )
           ,MAX( DECODE ( NVL ((SELECT attribute2 FROM FND_LOOKUP_VALUES    WHERE LOOKUP_TYPE = 'MBC_MOBILE_USERS' AND LANGUAGE = 'US' AND UPPER(MEANING) =UPPER(FU.USER_NAME)),'N'  )  ,'Y', 'Arabiya' , 'NotArabiya') )
           INTO V_IS_EXECUTIVE,V_IS_AL_ARABYIA 
 FROM FND_USER FU
  WHERE  UPPER(FU.USER_NAME)=UPPER(V_USER_NAME);   
   
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (1,'wsid',P_WSID);
   
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (2,'ref1',do_reverse(V_REFERENCE)||V_IV||dbms_random.string('x',976));
   
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (3,'ref2',dbms_random.string('x',1024));
   
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (4,'bu',V_BUSINESS_GROUP_ID);
   
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (5,'NTF_NO',V_NTF_COUNT);
   
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (6,'isExecutive',V_IS_EXECUTIVE);
   
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (7,'isArabyia',V_IS_AL_ARABYIA);
   
   
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (8,'AAorganization',V_AA_ORGANIZATION);
   --Added By Heba 1-4-2020//
    V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (9,'AArole',V_AL_ARABYIA_APP_ROLE );                               
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (10,'EmpType',V_PERSON_TYPE );
     V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (11,'myspace',V_SHOW_MY_SPACE );                               
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (12,'myteam',V_SHOW_MY_TEAM ); 
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (13,'workgroups',V_SHOW_MY_WK ); 
   V_MBC_MOBILE_SESSION_INFO_LIST.EXTEND;
   V_MBC_MOBILE_SESSION_INFO_LIST(V_MBC_MOBILE_SESSION_INFO_LIST.COUNT):=MBC_MOBILE_SESSION_INFO_OBJ (14,'AAOrgCount',V_AA_ORG_COUNT );                              
  --V_SHOW_MY_SPACE
  
  -- End added By Heba 1-4-2020//
   RETURN V_MBC_MOBILE_SESSION_INFO_LIST;
   
   END GET_SESSION_INFO;
   
   FUNCTION GET_SESSION_DATA ( P_PROPERTY_NAME VARCHAR2 ) RETURN VARCHAR2
   IS
   V_PROPERTY_NAME VARCHAR2(500);
   BEGIN
   V_PROPERTY_NAME := UPPER(P_PROPERTY_NAME);
   
   IF    V_PROPERTY_NAME='USER_ID' THEN 
            RETURN MBC_MOBILE_PKG.USER_ID;
   ELSIF V_PROPERTY_NAME='PERSON_ID' THEN 
            RETURN MBC_MOBILE_PKG.PERSON_ID;
   ELSIF V_PROPERTY_NAME='ASSIGNMENT_ID' THEN 
            RETURN MBC_MOBILE_PKG.ASSIGNMENT_ID;
   ELSIF V_PROPERTY_NAME='BUSINESS_GROUP_ID' THEN 
            RETURN MBC_MOBILE_PKG.BUSINESS_GROUP_ID;
   ELSIF V_PROPERTY_NAME='EMPLOYEE_NUMBER' THEN 
            RETURN MBC_MOBILE_PKG.EMPLOYEE_NUMBER;
   ELSIF V_PROPERTY_NAME='USER_NAME' THEN 
            RETURN MBC_MOBILE_PKG.USER_NAME;
   ELSIF V_PROPERTY_NAME='GLOBAL_NAME' THEN 
            RETURN MBC_MOBILE_PKG.GLOBAL_NAME;
   ELSIF V_PROPERTY_NAME='SESSION_ID' THEN 
            RETURN MBC_MOBILE_PKG.SESSION_ID;
   
   END IF;
   
   RETURN NULL;
   END;
   
   FUNCTION LOGIN (P_USER_NAME         IN       VARCHAR2,
                    P_PASSWORD             IN       VARCHAR2,
                    P_VERSION              IN       VARCHAR2,
                    P_SIGNATURE            IN       VARCHAR2,
                    P_WSID                OUT      VARCHAR2
                    ) RETURN VARCHAR2 IS
    L_RETURN        VARCHAR2(1000);
    L_AUTHORIZED    VARCHAR2(10);
    L_MOBILE_USER   NUMBER:=0;
    L_MOBILE_VERSION    VARCHAR2(10);
    L_USER_PERSON_TYPE  varchar2(1000);
    V_DESCRIPTION       varchar2(4000);
    V_IS_EXECUTIVE      VARCHAR2(1);
    V_IS_AL_ARABYIA     VARCHAR2(20);
    v_session_id        xx_mobile_session.wsid%type;
    
    V_KEY                VARCHAR2(32);
    V_IV                 VARCHAR2(16); 
    V_PASSWORD          VARCHAR2(10000); 
    p_loginID number;
    p_expired VARCHAR2 (100);
   BEGIN

     
    --Check Version
    BEGIN
        SELECT MEANING , DESCRIPTION
        INTO L_MOBILE_VERSION , V_DESCRIPTION
        FROM FND_LOOKUP_VALUES    
        WHERE LOOKUP_TYPE = 'MBC_MOBILE_USERS'
        AND LANGUAGE = 'US'
        AND UPPER(LOOKUP_CODE) = 'VERSION';
        
        IF L_MOBILE_VERSION <> NVL(P_VERSION,-1) THEN
            L_RETURN := 'You have to update your application' ||V_DESCRIPTION;
           -- RETURN L_RETURN;
        END IF ;
        
    EXCEPTION
        WHEN OTHERS THEN
        L_RETURN := 'You have to update your application';
       -- RETURN L_RETURN;
    END;
    
    
    
    
  
    
      -- CHECK LOCKED 
        /*IF  ( is_locked_user(UPPER(P_USER_NAME)) )   THEN
        RETURN 'This user is locked ,Please contact system administrator.';
        END IF;
        */
       
          V_KEY := SUBSTR (P_SIGNATURE,1,32);
          V_IV :=  SUBSTR (P_SIGNATURE,33,16);
 -- return v_IV;
            V_PASSWORD:=DECRYPT_DATA(do_reverse(V_KEY),V_IV, UTL_ENCODE.BASE64_DECODE( UTL_RAW.CAST_TO_RAW(  replace(P_PASSWORD,' ','+')))  );
         
      
          IF (V_PASSWORD='ERROR') then 
            return 'errorx';
             end if;
    
    --Check Authontication
   -- L_AUTHORIZED := FND_WEB_SEC.VALIDATE_LOGIN (P_USER_NAME, V_PASSWORD);
    --changed by Amr Helaly 13-01-2020 to use auto lock feature 
     L_AUTHORIZED := FND_WEB_SEC.VALIDATE_LOGIN (P_USER_NAME, V_PASSWORD,p_loginID,p_expired , null);

    
    IF L_AUTHORIZED = 'Y' THEN           
        
        L_RETURN := 'OK';
        
        begin
         -- GENERATE SESSION_ID   
        v_session_id:=create_session(UPPER(P_USER_NAME));
        -- DELETE PREVIOUS LOGIN ATTEMPS
        --delete_login_attemps( UPPER(P_USER_NAME));
         -- log
       -- mbc_mobile_pkg.LOG_MOBILE_ACTIVITY(1 , P_PERSON_ID , P_USER_ID , 'LOGIN' , 'DB', 'Successful Login' ,P_USER_NAME );
         P_WSID:=v_session_id;
         commit;
        exception when others then 
        L_RETURN := regexp_replace (SQLERRM,'((ORA.)*[[:digit:]])+: ', '');  
        
        end;

      
    
    ELSE
       --L_RETURN:= 'Invalid Credentials';
       L_RETURN :='Login failed. Please verify your login information or contact the system administrator.';
          -- LOCK USER IF MOR THAN N TIMES 
           --lock_user(UPPER(P_USER_NAME));
    END IF;
    
    RETURN L_RETURN;
    
   EXCEPTION
    WHEN OTHERS THEN
    RETURN 'Error, Please contact system administrator' || regexp_replace (SQLERRM,'((ORA.)*[[:digit:]])+: ', '');
   END LOGIN;    
  
  
  
                      
 FUNCTION REGISTER_AIO (P_WSID VARCHAR2 , P_SIGNATURE VARCHAR2  )
 RETURN  VARCHAR2  IS 


V_REFERENCE XX_MOBILE_SESSION.WS_REFERENCE%TYPE;
V_IV XX_MOBILE_SESSION.INITIALIZATION_VECTOR%TYPE;
V_USER_NAME XX_MOBILE_SESSION.USER_NAME%TYPE;

V_TOKEN XX_MOBILE_AIO_USERS.TOKEN%TYPE;
V_AIO_REFERENCE XX_MOBILE_SESSION.WS_REFERENCE%TYPE;
V_AIO_IV XX_MOBILE_SESSION.INITIALIZATION_VECTOR%TYPE;

V_PASSWORD  VARCHAR2(4000);
V_ENCRYPED_PASSWORD  VARCHAR2(4000);

V_Result  VARCHAR2(4000):='[ { "ref1":"&Token","ref2":"&key" ,  "message":"1" } ]';
begin


  

   
   SELECT WS_REFERENCE , USER_NAME      , INITIALIZATION_VECTOR
   INTO   V_REFERENCE ,  V_USER_NAME    , V_IV
   FROM  XX_MOBILE_SESSION 
   WHERE WSID=P_WSID
   AND IS_ACTIVE=1
   And TO_NUMBER (TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'))-TO_NUMBER (TO_CHAR(LAST_CONNECT,'YYYYMMDDHH24MISS')) <  300;

  update XX_MOBILE_AIO_USERS
  set is_active=0,
  last_update_date=sysdate
  where user_name=V_USER_NAME
  and is_active=1;



   V_PASSWORD:=DECRYPT_DATA(V_REFERENCE,V_IV, UTL_ENCODE.BASE64_DECODE( UTL_RAW.CAST_TO_RAW( replace(P_SIGNATURE,' ','+')  ))  );
   
   IF (V_PASSWORD='ERROR') then 
   return 'Error:Invalid Signature';
   end if;
  
   
   V_TOKEN:=DBMS_RANDOM.STRING('a',2048);
   V_TOKEN:=REPLACE (V_TOKEN , '+',DBMS_RANDOM.STRING('a',1) );
   V_AIO_REFERENCE:=DBMS_RANDOM.STRING('x',32);
   V_AIO_IV:=DBMS_RANDOM.STRING('x',16);
   
   
   V_ENCRYPED_PASSWORD:=ENCRYPT_DATA(V_AIO_REFERENCE,V_AIO_IV,V_PASSWORD);
   IF (V_ENCRYPED_PASSWORD='errorx') then 
   return 'Error:Invalid Input';
   end if;
    
   
   
   
   
   INSERT INTO XX_MOBILE_AIO_USERS (AIO_ID,USER_NAME,ENCRYPTED_PASSWORD,TOKEN,IS_ACTIVE,CREATION_DATE )
   VALUES                          (XX_MOBILE_AIO_USERS_S.NEXTVAL ,V_USER_NAME ,  V_ENCRYPED_PASSWORD , V_TOKEN,  1 ,SYSDATE );
   
    
   V_Result:= REPLACE(V_Result,'&Token',V_TOKEN); 
   V_Result:= REPLACE(V_Result,'&key',do_reverse(V_AIO_REFERENCE)||V_AIO_IV||DBMS_RANDOM.STRING('x',976)); 
   V_Result:= REPLACE(V_Result,'&xx',V_ENCRYPED_PASSWORD);  
      RETURN V_Result;
      
   EXCEPTION 
    WHEN NO_DATA_FOUND THEN RETURN 'Ivalid Session';
   WHEN OTHERS THEN RETURN 'Error:'||regexp_replace (SQLERRM,'((ORA.)*[[:digit:]])+: ', '');
      
end REGISTER_AIO;

FUNCTION LOGIN_AIO (P_TOKEN VARCHAR2, P_KEY VARCHAR2 , P_IV VARCHAR2  , P_VERSOIN VARCHAR2 , P_WSID OUT VARCHAR2 ) RETURN VARCHAR2
IS

V_USER_NAME XX_MOBILE_AIO_USERS.USER_NAME%TYPE;
V_TOKEN XX_MOBILE_AIO_USERS.TOKEN%TYPE;
V_ENCRYPED_PASSWORD  XX_MOBILE_AIO_USERS.ENCRYPTED_PASSWORD%TYPE;
V_PASSWORD  VARCHAR2(4000);

V_WSID XX_MOBILE_SESSION.WSID%TYPE;
V_RESULT VARCHAR2(4000);
BEGIN

--raise_application_error (-20001 , 'val is '||P_KEY);

SELECT USER_NAME,ENCRYPTED_PASSWORD
INTO V_USER_NAME,V_ENCRYPED_PASSWORD
FROM XX_MOBILE_AIO_USERS
WHERE TOKEN=P_TOKEN
AND IS_ACTIVE=1;

 

/*V_PASSWORD:=DECRYPT_DATA(do_reverse(P_KEY),P_IV, UTL_ENCODE.BASE64_DECODE( UTL_RAW.CAST_TO_RAW(V_ENCRYPED_PASSWORD))  );
   IF (V_PASSWORD='ERROR') then 
   return 'error';
   end if;
   */
   
  
   
   V_RESULT:=LOGIN(V_USER_NAME,V_ENCRYPED_PASSWORD,P_VERSOIN,(P_KEY)||P_IV,V_WSID);
   P_WSID:=V_WSID;
   RETURN V_RESULT;
 EXCEPTION 
 WHEN NO_DATA_FOUND THEN RETURN 'Biometric data does not exist';
 WHEN OTHERS THEN RETURN  regexp_replace (SQLERRM,'((ORA.)*[[:digit:]])+: ', '');
END LOGIN_AIO;



 FUNCTION LOGOUT (P_WSID VARCHAR2 , P_SIGNATURE VARCHAR2  )
 RETURN  VARCHAR2  IS 
 BEGIN
            UPDATE  XX_MOBILE_SESSION 
            SET IS_ACTIVE=2
            WHERE WSID=P_WSID;
            COMMIT;
            RETURN 'OK';
 END LOGOUT;
      
END;
/
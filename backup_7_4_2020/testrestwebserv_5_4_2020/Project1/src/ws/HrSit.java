package ws;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

@Path("sshr/sit")
public class HrSit {
    public HrSit() {
        super();
    }

    @POST
    @Produces("application/json")
    @Path("/getLettersTypes")
    public Response getLettersTypes(@Context SecurityContext sc ,
                                    @HeaderParam("bu") String businessGroup,
                                    @HeaderParam("wsid") String wsid,
                                    @HeaderParam("signature") String signature)
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
        
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlstmt ="";
            
            final String UAE_LETTERS="select meaning letter_type, description, meaning letter_code\n" + 
            "from apps.hr_lookups h\n" + 
            "where 1=1\n" + 
            "and lookup_type = 'MBC LETTERS UAE'\n" + 
            "order by to_number(lookup_code)";
            
            final String KSA_LETTERS="Select meaning letter_type , lookup_code letter_code\n" + 
            "From FND_LOOKUP_values\n" + 
            "Where lookup_type = 'MBC_SAUDI_LETTER_TYPES' \n" + 
            "and language = 'US' \n" + 
            "and ENABLED_FLAG = 'Y'";
            
            final String OTHERS_LETTERS="select flex_value letter_type , flex_value letter_code\n" + 
            "from FND_FLEX_VALUES\n" + 
            "where FLEX_VALUE_SET_ID = 1018241";
           
           
            
            
            if (businessGroup!=null)
            {
                                 if (businessGroup.equalsIgnoreCase("81"))
                                 {
                                    sqlstmt=UAE_LETTERS;
                                 }
                                 else if (businessGroup.equalsIgnoreCase("998"))
                                 {
                                     sqlstmt=KSA_LETTERS;
                                 }
                                 else
                                 {
                                     sqlstmt=OTHERS_LETTERS;
                                 }
            
            
            }
            
            
            
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getLettersTypes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
               
            statement = connection.prepareStatement(sqlstmt);
                //statement.setString(1, depId);  
              
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))              
                            statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    


    @POST
    @Produces("application/json")
    @Path("/getTecomSalatUpdateTypes")
    public Response getTecomSalatUpdateTypes(@Context SecurityContext sc,
                                             @HeaderParam("wsid") String wsid,
                                             @HeaderParam("signature") String signature)
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getTecomSalatUpdateTypes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement("select FLEX_VALUE\n" + 
            "from FND_FLEX_VALUES\n" + 
            "where FLEX_VALUE_SET_ID = 1017263\n" +  // was 1013527
            "and ENABLED_FLAG = 'Y'\n");
                //statement.setString(1, depId);  
              
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                                                   connection.close();
                                                     if ((statement != null) && !(statement.isClosed()))
                                                               statement.close();
                                                      if ((rs != null) && !(rs.isClosed()))
                                                                  rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    

    //@GET
    @Produces("application/json")
    @Path("/applySITRequest")
    public Response applySITRequest(
                                @QueryParam("personId") String personId,
                                                               @QueryParam("businessGroupid") String businessGroupid,
                                                               @QueryParam("sitTitle") String sitTitle,
                                                               @QueryParam("status") String status,
                                                               @QueryParam("segment1") String segment1,
                                                               @QueryParam("segment2") String segment2 ,
                                                               @QueryParam("segment3") String segment3,
                                                               @QueryParam("segment4") String segment4,
                                                               @QueryParam("segment5") String segment5,
                                                               @QueryParam("segment6") String segment6,
                                                               @QueryParam("segment7") String segment7,
                                                               @QueryParam("segment8") String segment8,
                                                               @QueryParam("segment9") String segment9,
                                                               @QueryParam("segment10") String segment10,
                                                               @QueryParam("segment11") String segment11,
                                                               @QueryParam("segment12") String segment12,
                                                               @QueryParam("segment13") String segment13,
                                                               @QueryParam("segment14") String segment14,
                                                               @QueryParam("segment15") String segment15,
                                                               @QueryParam("segment16") String segment16,
                                                               @QueryParam("segment17") String segment17,
                                                               @QueryParam("segment18") String segment18,
                                                               @QueryParam("segment19") String segment19,
                                                               @QueryParam("segment20") String segment20,
                                                               @QueryParam("segment21") String segment21,
                                                               @QueryParam("segment22") String segment22,
                                                               @QueryParam("segment23") String segment23,
                                                               @QueryParam("segment24") String segment24,
                                                               @QueryParam("segment25") String segment25,
                                                               @QueryParam("segment26") String segment26,
                                                               @QueryParam("segment27") String segment27,
                                                               @QueryParam("segment28") String segment28,
                                                               @QueryParam("segment29") String segment29,
                                                               @QueryParam("segment30") String segment30)
                                 
    {
           
          
            
          
            Response response=null;
            Connection connection = null;
            CallableStatement cs=null;
            String result=null;
            String transactionId=null;
            String processName=null;
           
        String block=" begin ?:=APPS.MBC_MOBILE_PKG.START_SIT_WORKFLOW(" +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?" +
            "); end;";
            try {
                connection = Helper.getOracleConnection();
               // System.out.println(block);
                cs=  connection.prepareCall(block);
                cs.registerOutParameter(1, Types.VARCHAR);
                cs.setString(2,personId );
                cs.setString(3,businessGroupid );
                cs.setString(4,sitTitle );
                cs.setString(5,status );
                cs.setString(6,segment1 );
                cs.setString(7,segment2 );
                cs.setString(8,segment3 );
                cs.setString(9,segment4 );
                cs.setString(10,segment5 );
                cs.setString(11,segment6 );
                cs.setString(12,segment7 );
                cs.setString(13,segment8 );
                cs.setString(14,segment9 );
                cs.setString(15,segment10 );
                cs.setString(16,segment11 );
                cs.setString(17,segment12 );
                cs.setString(18,segment13 );
                cs.setString(19,segment14 );
                cs.setString(20,segment15 );
                cs.setString(21,segment16 );
                cs.setString(22,segment17 );
                cs.setString(23,segment18 );
                cs.setString(24,segment19 );
                cs.setString(25,segment20 );
                cs.setString(26,segment21 );
                cs.setString(27,segment22 );
                cs.setString(28,segment23 );
                cs.setString(29,segment24 );
                cs.setString(30,segment25 );
                cs.setString(31,segment26 );
                cs.setString(32,segment27 );
                cs.setString(33,segment28 );
                cs.setString(34,segment29 );
                cs.setString(35,segment30 );
                cs.registerOutParameter(36, Types.VARCHAR);
                cs.registerOutParameter(37, Types.VARCHAR);
                
               
                
                
                

                cs.executeUpdate();
               
               result=cs.getString(1);
                transactionId =cs.getString(36);
                processName=cs.getString(37);
           System.out.println("result is "+result);
              
              if ("-1".equalsIgnoreCase(result))
              {
                  
                      response = Helper.StringtoReponse("error","2");
                  
                  }
                else
              {
                  
                     
                  
                    //  response=Helper.StringtoReponse(result , "1");
                  
                  
                      if( status.equalsIgnoreCase("Y") )
                      {
                          connection.commit();
                          
                          response = Helper.StringtoReponse(result,"1");
                          
                      }
                      else
                      {
                             
                              response= getApproverList(connection,transactionId );
                              
                              connection.rollback();
                            
                           cancelTransaction(connection, transactionId, result , processName);
                          
                          }
                  
                  }

                
            
            }
            catch(Exception e)
            {
                e.printStackTrace();
                }
            finally {
                        try {
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                            cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
            
              return response; 
       
        
        }



    @POST
    @Produces("application/json")
    @Path("/applySITRequestPost")
    public Response applySITRequestPost(@Context SecurityContext sc ,
                                @HeaderParam("sitTitle") String sitTitle,
                                @HeaderParam("status") String status,
                                @HeaderParam("segment1") String segment1,
                                @HeaderParam("segment2") String segment2 ,
                                @HeaderParam("segment3") String segment3,
                                @HeaderParam("segment4") String segment4,
                                @HeaderParam("segment5") String segment5,
                                @HeaderParam("segment6") String segment6,
                                @HeaderParam("segment7") String segment7,
                                @HeaderParam("segment8") String segment8,
                                @HeaderParam("segment9") String segment9,
                                @HeaderParam("segment10") String segment10,
                                @HeaderParam("segment11") String segment11,
                                @HeaderParam("segment12") String segment12 ,
                                @HeaderParam("segment13") String segment13,
                                @HeaderParam("segment14") String segment14,
                                @HeaderParam("segment15") String segment15,
                                @HeaderParam("segment16") String segment16,
                                @HeaderParam("segment17") String segment17,
                                @HeaderParam("segment18") String segment18,
                                @HeaderParam("segment19") String segment19,
                                @HeaderParam("segment20") String segment20,
                                @HeaderParam("segment21") String segment21,
                                @HeaderParam("segment22") String segment22,
                                @HeaderParam("segment23") String segment23,
                                @HeaderParam("segment24") String segment24,
                                @HeaderParam("segment25") String segment25,
                                @HeaderParam("segment26") String segment26,
                                @HeaderParam("segment27") String segment27,
                                @HeaderParam("segment28") String segment28,
                                @HeaderParam("segment29") String segment29,
                                @HeaderParam("segment30") String segment30,
                                @HeaderParam("wsid") String wsid,
                                @HeaderParam("signature") String signature        )
                                 
    {
           
          
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Response response=null;
            Connection connection = null;
            CallableStatement cs=null;
            String result=null;
            String itemKey=null;
            String transactionId=null;
            String processName=null;
           
        String block=" begin ?:=APPS.MBC_MOBILE_PKG.START_SIT_WORKFLOW(" +
            "mbc_mobile_pkg.get_session_data('PERSON_ID') , mbc_mobile_pkg.get_session_data('BUSINESS_GROUP_ID'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?" +
            "); end;";
            try {
                if (segment1!=null)
                    segment1= URLDecoder.decode(segment1);
                
                if (segment2!=null)
                    segment2= URLDecoder.decode(segment2);
                if (segment3!=null)
                    segment3= URLDecoder.decode(segment3);
                if (segment4!=null)
                    segment4= URLDecoder.decode(segment4);
                if (segment12!=null)
                    segment12= URLDecoder.decode(segment12);
                if (segment11!=null)
                    segment11= URLDecoder.decode(segment11);
                connection = Helper.getOracleConnection();
                Helper.logActivity(connection, "apply SIT Request", "Title is "+sitTitle);
               // System.out.println(block);
                cs=  connection.prepareCall(block);
                cs.registerOutParameter(1, Types.VARCHAR);
                cs.setString(2,sitTitle );
                cs.setString(3,status );
                cs.setString(4,segment1 );
                cs.setString(5,segment2 );
                cs.setString(6,segment3 );
                cs.setString(7,segment4 );
                cs.setString(8,segment5 );
                cs.setString(9,segment6 );
                cs.setString(10,segment7 );
                cs.setString(11,segment8 );
                cs.setString(12,segment9 );
                cs.setString(13,segment10 );
                cs.setString(14,segment11 );
                cs.setString(15,segment12 );
                cs.setString(16,segment13 );
                cs.setString(17,segment14 );
                cs.setString(18,segment15 );
                cs.setString(19,segment16 );
                cs.setString(20,segment17 );
                cs.setString(21,segment18 );
                cs.setString(22,segment19 );
                cs.setString(23,segment20 );
                cs.setString(24,segment21 );
                cs.setString(25,segment22 );
                cs.setString(26,segment23 );
                cs.setString(27,segment24 );
                cs.setString(28,segment25 );
                cs.setString(29,segment26 );
                cs.setString(30,segment27 );
                cs.setString(31,segment28 );
                cs.setString(32,segment29 );
                cs.setString(33,segment30 );
                cs.registerOutParameter(34, Types.VARCHAR);
                cs.registerOutParameter(35, Types.VARCHAR);
                cs.registerOutParameter(36, Types.VARCHAR);
                
             
                
              
                
            
                

                cs.executeUpdate();
               
               result=cs.getString(1);
                transactionId =cs.getString(34);
                itemKey =cs.getString(35);
                processName=cs.getString(36);
           System.out.println("result is "+result);
              
                if (!"success".equalsIgnoreCase(result))
              {
                  
                      response = Helper.StringtoReponse(result.replace("\n", "").replace("\r", "").replace("\"", ""),"2");
                  
                  }
                else
              {
                  
                     
                  
                    //  response=Helper.StringtoReponse(result , "1");
                  
                  
                      if( status.equalsIgnoreCase("Y") )
                      {
                          connection.commit();
                          
                          response = Helper.StringtoReponse(result,"1");
                          
                      }
                      else
                      {
                             
                              response= getApproverList(connection,transactionId );
                              
                              connection.rollback();
                            
                           cancelTransaction(connection, transactionId, itemKey , processName);
                          
                          }
                  
                  }

                
            
            }
            catch(Exception e)
            {
                e.printStackTrace();
                response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((cs != null) && !(cs.isClosed()))
                                            cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
            
              return response; 
       
        
        }

    @POST
    @Produces("application/json")
    @Path("/getHousingRequestypes")
    public Response getHousingRequestypes(@Context SecurityContext sc ,
                                          @HeaderParam("wsid") String wsid,
                                          @HeaderParam("signature") String signature)
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            try {

                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getHousingRequestypes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement("select hl.meaning\n" + 
            "from hr_lookups hl, per_all_people_f papf\n" + 
            "where lookup_type = 'MBC HRA LOAN TYPES'\n" + 
            "and trunc(sysdate) between papf.effective_start_date and papf.effective_end_date\n" + 
            "and person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')  \n" + 
            "    and person_id not in (\n" + 
            "    select person_id from\n" + 
            "    (select papf.person_id,peef.element_entry_id, max(decode(pivf.name,'Tenure in Months',to_number(peevf.SCREEN_ENTRY_VALUE),null)) tenure,\n" + 
            "    max(decode(pivf.name,'Repayment Start Date',to_date(peevf.SCREEN_ENTRY_VALUE,'YYYY/MM/DD HH24:MI:SS'),null)) repayment,\n" + 
            "    max(decode(pivf.name,'Hold Start',to_date(substr(peevf.SCREEN_ENTRY_VALUE,1,10),'YYYY/MM/DD'),null)) hold_start,\n" + 
            "    max(decode(pivf.name,'Hold End',to_date(substr(peevf.SCREEN_ENTRY_VALUE,1,10),'YYYY/MM/DD'),null)) hold_end\n" + 
            "             from\n" + 
            "               pay_element_entries_f peef,\n" + 
            "               pay_element_entry_values_f peevf,\n" + 
            "               pay_input_values_f pivf,\n" + 
            "               pay_element_types_f petf,\n" + 
            "               per_all_assignments_f paaf,\n" + 
            "               per_all_people_f papf,\n" + 
            "               hr_all_organization_units hou\n" + 
            "        where\n" + 
            "              peef.ELEMENT_ENTRY_ID = peevf.ELEMENT_ENTRY_ID\n" + 
            "          and peef.ELEMENT_TYPE_ID = petf.ELEMENT_TYPE_ID\n" + 
            "          and peevf.INPUT_VALUE_ID = pivf.INPUT_VALUE_ID\n" + 
            "          and peef.ASSIGNMENT_ID = paaf.ASSIGNMENT_ID\n" + 
            "          and paaf.PERSON_ID = papf.PERSON_ID\n" + 
            "          and trunc(sysdate) between paaf.EFFECTIVE_START_DATE and paaf.EFFECTIVE_END_DATE\n" + 
            "          and trunc(sysdate) between papf.EFFECTIVE_START_DATE and papf.EFFECTIVE_END_DATE\n" + 
            "          and paaf.organization_id = hou.organization_id\n" + 
            "          and hou.BUSINESS_GROUP_ID = 81\n" + 
            "          and petf.element_name in ('Housing Loan Recovery','Housing Loan Recovery 2')\n" + 
            "          and pivf.name in ('Tenure in Months','Repayment Start Date','Hold Start','Hold End')\n" + 
            "          and (trunc(sysdate) between peef.effective_start_date and peef.effective_end_date or peef.effective_start_date > trunc(sysdate))\n" + 
            "          group by papf.person_id,peef.element_entry_id)\n" + 
            "    where trunc(add_months(repayment,tenure-1+nvl(months_between(hold_end,hold_start),0)),'month') > trunc(sysdate,'month')\n" + 
            "    and person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')  \n" + 
            "    )\n" + 
            "and decode(round(months_between(sysdate,(select max(DATE_START) from PER_PERIODS_OF_SERVICE_V where person_id =  papf.person_id))),0,0,1,0,2,0,3,0,4,0,5,0,6,0,1) <= (select tag from fnd_lookup_values f where f.lookup_type = 'MBC HRA LOAN TYPES' and f.lookup_code = hl.lookup_code and f.language = userenv('LANG'))\n" + 
            "and decode(hl.meaning,'Initial',1,0) in decode(hl.meaning,'Standard','0',nvl((Select distinct d.person_id\n" + 
            "            from pay_element_entries_f a,\n" + 
            "              pay_element_links_f b,\n" + 
            "              pay_element_types_f c,\n" + 
            "              per_all_assignments_f d\n" + 
            "          where a.element_link_id = b.element_link_Id\n" + 
            "          and b.element_type_id = c.element_type_id\n" + 
            "          and c.business_group_id = 81\n" + 
            "          and a.assignment_id = d.assignment_Id\n" + 
            "          and trunc(sysdate) between d.effective_start_date and d.effective_end_date\n" + 
            "          and c.element_name in ('Housing Loan Recovery','Housing Loan Recovery 2')\n" + 
            "         and d.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID')  \n" + 
            "          ),1))");
              
              
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }
                
                

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                                                     connection.close();
                                                       if ((statement != null) && !(statement.isClosed()))
                                                                 statement.close();
                                                        if ((rs != null) && !(rs.isClosed()))
                                                                    rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    
    
    public void cancelTransaction( Connection connection ,String transactionId , String itemKey , String processsName )
    {
        
        
            //Connection connection=null;
            CallableStatement cs=null;
           
            String sqlStemt="BEGIN\n" + 
            "  apps.mbc_mobile_pkg.cancel_request(\n" + 
            "    p_item_type=>'HRSSA',\n" + 
            "    p_item_key=> ?,\n" + 
            "    p_process_int_name=>?,\n" + 
            "    p_transaction_id=> ?\n" + 
            "    );\n" + 
            "END;";
            try {
         //   connection = getConnection();
              //  connection = Helper.getOracleConnection();
              cs=  connection.prepareCall(sqlStemt);
          
                cs.setString(1, itemKey);  
                cs.setString(2, processsName);  
                cs.setString(3, transactionId);  
                cs.executeUpdate();;
            }
          catch (Exception e)
                {
                e.printStackTrace();
                }
            finally {
                try {
                  
                    if ((cs != null) && !(cs.isClosed()))
                     //   connection.close();
                                  cs.close();
                } catch (SQLException sqle) {
                    // TODO: Add catch code
                    sqle.printStackTrace();
                }
            }
        }
    
    public Response getApproverList(   Connection connection, String transactionId )
    {
        
        
            //Connection connection=null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select line_no,job_title,display_name,intials,imge from  table (MBC_MOBILE_PKG.GET_LEAVE_APPROVER_LIST(?))";
            try {
         //   connection = getConnection();
             //  connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, transactionId);  
               
               
             rs = statement.executeQuery();
       
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"");
                    
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
               
                map.writeValue(outPut, m);
                
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    response=getNoApproverList(connection);
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                }
            finally {
                        try {
                          
                            if ((statement != null) && !(statement.isClosed()))
                              
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                                                   rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    public Response getNoApproverList(   Connection connection )
    {
        
        
            //Connection connection=null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select  null line_no, null user_name, null user_id, null person_id, 'No Approvers' job_title, null display_name, null intials, null imge from dual";
            try {
         //   connection = getConnection();
             //  connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
                
               
               
             rs = statement.executeQuery();
       
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"");
                    
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
               
                map.writeValue(outPut, m);
                
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                }
            finally {
                        try {
                          
                            if ((statement != null) && !(statement.isClosed()))
                              
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                                                                   rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    @POST
    @Produces("application/json")
    @Path("/getCardTypes")
    public Response getCardTypes(@Context SecurityContext sc ,
                                 @HeaderParam("wsid") String wsid,
                                 @HeaderParam("signature") String signature)
    {
        
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getCardTypes");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement("select meaning, lookup_code\n" + 
            "from fnd_lookup_values\n" + 
            "where 1=1\n" + 
            "and language = 'US'\n" + 
            "and lookup_type = 'MBC_BUSSINESS_CARD_TYPE'\n" + 
            "and meaning in (SELECT decode(SEGMENT2,'MEN','Al Arabiya','Wanasah','MBC',SEGMENT2) \n" + 
            "FROM PAY_PEOPLE_GROUPS ppg, per_all_assignments_f paaf \n" + 
            "where ppg.PEOPLE_GROUP_ID = paaf.PEOPLE_GROUP_ID\n" + 
            "and paaf.primary_flag= 'Y'\n" + 
            "and sysdate between paaf.effective_start_date and paaf.effective_end_date \n" + 
            "and paaf.person_id = mbc_mobile_pkg.get_session_data('PERSON_ID')  )");
                
              
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))              
                            statement.close();
                            if ((rs != null) && !(rs.isClosed()))  
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
  
 
    
    @POST
    @Produces("application/json")
    @Path("/getCardLanguages")
    public Response getCardLanguages(@Context SecurityContext sc ,
                                    @HeaderParam("wsid") String wsid,
                                    @HeaderParam("signature") String signature)
    {
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
        
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getCardLanguages");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
            statement = connection.prepareStatement("select meaning, lookup_code , tag\n" + 
            "from fnd_lookup_values\n" + 
            "where 1=1\n" + 
            "and lookup_type = 'MBC_BUSSINESS_LANGUAGE'\n" + 
            "and language = 'US'\n" + 
            "and ENABLED_FLAG = 'Y'");
                 
              
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                 
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))              
                            statement.close();
                            if ((rs != null) && !(rs.isClosed()))  
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    @POST
    @Produces("application/json")
    @Path("/getCardTDefualtData")
    public Response getCardTDefualtData( @Context SecurityContext sc ,
                                         @HeaderParam("wsid") String wsid,
                                         @HeaderParam("signature") String signature )
    {
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getCardTDefualtData");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                
                Helper.logActivity(connection,  "Open Bussiness Card", "Bussiness Card Defualt Data");
            statement = connection.prepareStatement("select papf.employee_number, papf.full_name, substr(name,1,instr(pos.name,'.')-1) job_title, '04 391 9999' phone, papf.email_address ,\n" + 
            "    (select Phone_number from per_phones where parent_table = 'PER_ALL_PEOPLE_F' and phone_type = 'M' and sysdate between date_from and nvl(date_to,sysdate+1) and parent_id = papf.PERSON_ID and rownum=1) mobile\n" + 
            "from per_all_people_f papf, per_all_assignments_f paaf, per_positions pos\n" + 
            "where 1=1\n" + 
            "and papf.person_id = paaf.person_id\n" + 
            "and sysdate between papf.effective_start_date and papf.effective_end_Date\n" + 
            "and sysdate between paaf.effective_start_date and paaf.effective_end_Date\n" + 
            "and papf.current_employee_flag = 'Y'\n" + 
            "and paaf.primary_flag = 'Y'\n" + 
            "and paaf.position_id = pos.position_id(+)\n" + 
            "and papf.person_id = mbc_mobile_pkg.get_session_data('PERSON_ID')");
               
              
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                   
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))              
                            statement.close();
                            if ((rs != null) && !(rs.isClosed()))  
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    


}

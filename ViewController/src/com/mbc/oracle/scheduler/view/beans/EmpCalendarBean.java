package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import com.mbc.oracle.scheduler.view.util.JSFUtils;

import java.awt.Color;

import java.util.HashMap;

import java.util.HashSet;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichCalendar;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.dnd.DnDAction;
import oracle.adf.view.rich.event.CalendarActivityEvent;
import oracle.adf.view.rich.event.DropEvent;
import oracle.adf.view.rich.model.CalendarActivity;

import oracle.adf.view.rich.util.CalendarActivityRamp;
import oracle.adf.view.rich.util.InstanceStyles;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;

public class EmpCalendarBean {
    private RichCalendar empCalendar;
    private RichPopup transPop;
    private  String printview="YES";
    private HashMap calStyles = new HashMap<Set<String>, InstanceStyles>();
    private RichButton printButton;

    public EmpCalendarBean() {
    }

    public void setEmpCalendar(RichCalendar empCalendar) {
        this.empCalendar = empCalendar;
    }

    public RichCalendar getEmpCalendar() {
        return empCalendar;
    }

//    public void empCalendarActivityListener(CalendarActivityEvent calendarActivityEvent) {
//        // Add event code here...
//        CalendarActivity activity = calendarActivityEvent.getCalendarActivity();
//        this.setCurrentRowUsingCalActivity(activity);
//    }
//
//    private void setCurrentRowUsingCalActivity(CalendarActivity activity) {
//        if (activity != null) {
//            DCIteratorBinding iterator = ADFUtils.findIterator("EmpSchedulesCalVOIterator");
//            Key key = new Key(new Object[] { activity.getId() });
//            RowSetIterator rsi = iterator.getRowSetIterator();
//            Row row = rsi.findByKey(key, 1)[0];
//            rsi.setCurrentRow(row);
//        }
//    }

//    public void onClickCancelCalCntxPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        this.createTransRecordShowPop("CANCEL");
//    }
//
//    public void onClickReplaceCalCntxPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        this.createTransRecordShowPop("REPLACE");
//    }
//
//    public void onClickChangeTimingCalCntxPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        this.createTransRecordShowPop("CHANGE");
//    }
//
//    private void createTransRecordShowPop(String transType) {
//        Number schedulerStaffId = (Number) ADFUtils.getBoundAttributeValue("SchedulerStaffId");
//        Date schedularStartDate =
//            (Date) ADFUtils.findIterator("EmpSchedulesCalVOIterator").getCurrentRow().getAttribute("SchedularStartDate");
//
//        ADFUtils.findOperation("CreateInsertTrans").execute();
//        ADFUtils.setBoundAttributeValue("SchedulerStaffIdTrans", schedulerStaffId);
//        ADFUtils.setBoundAttributeValue("TransTypeAttr", transType);
//        Number personId = (Number) ADFUtils.evaluateEL("#{sessionScope.userHelper.personId}");
//        ADFUtils.setBoundAttributeValue("RequesterId", personId);
//        ADFUtils.setBoundAttributeValue("EffectiveStartDate", schedularStartDate);
//        ADFUtils.setBoundAttributeValue("EffectiveEndDate", schedularStartDate);
//
//        DCIteratorBinding iter = ADFUtils.findIterator("AsApprovalTypesVOIterator");
//        Key key = null;
//        if ("CHANGE".equals(transType)) {
//            key = new Key(new Object[] { "CHANGE" });
//
//            ADFUtils.setBoundAttributeValue("StartTimeHoursTrans",
//                                            (Number) ADFUtils.getBoundAttributeValue("StartTimeHours"));
//            ADFUtils.setBoundAttributeValue("StartTimeMinutesTrans",
//                                            (Number) ADFUtils.getBoundAttributeValue("StartTimeMinutes"));
//            ADFUtils.setBoundAttributeValue("EndTimeHoursTrans",
//                                            (Number) ADFUtils.getBoundAttributeValue("EndTimeHours"));
//            ADFUtils.setBoundAttributeValue("EndTimeMinutesTrans",
//                                            (Number) ADFUtils.getBoundAttributeValue("EndTimeMinutes"));
//        } else if ("REPLACE".equals(transType)) {
//            key = new Key(new Object[] { "REPLACE" });
//        } else if ("CANCEL".equals(transType)) {
//            key = new Key(new Object[] { "CANCEL" });
//        }
//        iter.setCurrentRowWithKey(key.toStringFormat(true));
//
//        Number oldPersonId = (Number) ADFUtils.getBoundAttributeValue("PersonId");
//        Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
//
//        RichPopup.PopupHints hints = new RichPopup.PopupHints();
//        this.getTransPop().show(hints);
//    }
//
//    public void setTransPop(RichPopup transPop) {
//        this.transPop = transPop;
//    }
//
//    public RichPopup getTransPop() {
//        return transPop;
//    }
//
//    public void onClickSaveTransPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        ADFUtils.findOperation("Commit").execute();
//        Number schedulerTransHdrId = (Number) ADFUtils.getBoundAttributeValue("SchedulerTransHdrId");
//        OperationBinding oper = ADFUtils.findOperation("submitSchdlrTrans");
//        oper.getParamsMap().put("schedulerTransHdrId", schedulerTransHdrId);
//        oper.execute();
//
//        ADFUtils.findIterator("AsSchedulerTransHdrsVOIterator").getCurrentRow().refresh(Row.REFRESH_WITH_DB_FORGET_CHANGES);
//
//        this.getTransPop().hide();
//    }

//    public void onClickCancelTransPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        ADFUtils.findOperation("DeleteTrans").execute();
//        ADFUtils.findOperation("Commit").execute();
//        this.getTransPop().hide();
//    }
//
//    public DnDAction empDropTargetLsnr(DropEvent dropEvent) {
//        return DnDAction.NONE;
//    }

    public void initEmpView() {
        // Add event code here...
        Number personId = (Number) JSFUtils.resolveExpression("#{sessionScope.userHelper.personId}");
        ViewObject empSchdlrVo = ADFUtils.findIterator("EmpSchedulesCalVOIterator").getViewObject();
        empSchdlrVo.setWhereClause("PERSON_ID=" + personId);
    }

    public void initMgrView() {
        // Add event code here...
        Number personId = (Number) JSFUtils.resolveExpression("#{sessionScope.userHelper.personId}");
        ViewObject empSchdlrVo = ADFUtils.findIterator("EmpSchedulesCalVOIterator").getViewObject();
        empSchdlrVo.setWhereClause("PERSON_ID in (select paaf.person_id from per_all_assignments_f paaf where sysdate between paaf.effective_start_date and paaf.effective_end_date  and paaf.supervisor_id = " +
                                   personId + ")");
    }

    public void setCalStyles(HashMap calStyles) {
        this.calStyles = calStyles;
    }

    public HashMap getCalStyles() {
       // return calStyles;
       //return calStyles;
       try {
       HashSet ActvivityColor1 = new HashSet<String>();
       //HashSet ActvivityColor2= new HashSet<String>();
       HashSet ActvivityColor2 = new HashSet<String>();
       HashSet ActvivityColor3= new HashSet<String>();
       HashSet ActvivityColor4 = new HashSet<String>();
       HashSet ActvivityColor5 = new HashSet<String>();
       HashSet ActvivityColor6= new HashSet<String>();
       HashSet ActvivityColor7 = new HashSet<String>();
       HashSet ActvivityColor8= new HashSet<String>();
       // HashSet event3= new HashSet<String>();

        ActvivityColor1.add("SH");
        ActvivityColor2.add("L");
 
       ActvivityColor3.add("BT");
       ActvivityColor4.add("ASG");    
       ActvivityColor5.add("SHRD");  
       ActvivityColor6.add("BTRD"); 
           ActvivityColor7.add("SHRG");  
           ActvivityColor8.add("BTRG");
    // ActvivityColor6.add("NT"); 
          
       calStyles.put(ActvivityColor1, CalendarActivityRamp.getActivityRamp(CalendarActivityRamp.RampKey.LIME ));
       calStyles.put(ActvivityColor2, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.RED));
       calStyles.put(ActvivityColor3, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.LIME));
       calStyles.put(ActvivityColor4, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.ORANGE ));
       calStyles.put(ActvivityColor5, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.LAVENDAR));
       calStyles.put(ActvivityColor6, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.LAVENDAR));
       calStyles.put(ActvivityColor7, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.SEAWEED ));
       calStyles.put(ActvivityColor8, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.SEAWEED ));
       // activityStyles.put(event3, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.MIDNIGHTBLUE ));

       } catch (Exception e) {
       e.printStackTrace();
       }
       return calStyles;
    }
    
    public String printPage() {
           // Add event code here...
           FacesContext facesContext = FacesContext.getCurrentInstance();
           org.apache.myfaces.trinidad.render.ExtendedRenderKitService service =
                      org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext,   ExtendedRenderKitService.class);
           
           //     this.printButton.setVisible(false);
        //   AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
        //   adffctx.addPartialTarget(this.printButton);
                //  service.addScript(facesContext, " $('#caldiv').print();");
        
           return null;
       }

    public void setPrintview(String printview) {
        this.printview = printview;
    }

    public String getPrintview() {
        return printview;
    }

    public void setPrintButton(RichButton printButton) {
        this.printButton = printButton;
    }

    public RichButton getPrintButton() {
        return printButton;
    }
}

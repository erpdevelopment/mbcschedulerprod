

CREATE OR REPLACE PACKAGE BODY APPS.AS_SCHDLR_PKG
IS
   PROCEDURE GET_NEXT_APPROVER (IN_WORKLIST_ID          IN     NUMBER,
                                OUT_IS_FINAL_APPROVER      OUT VARCHAR2,
                                OUT_NEXT_APPROVER_ID       OUT VARCHAR2)
   IS
      L_WORKLIST_CATEGORY       AS_WORKLIST.WORKLIST_CATEGORY%TYPE;
      L_WORKLIST_TRANS_ID       AS_WORKLIST.WORKLIST_TRANS_ID%TYPE;
      L_WORKLIST_PERSON_ID      AS_WORKLIST.WORKLIST_PERSON_ID%TYPE;
      L_NXT_ACTION_HISTORY_ID   NUMBER;
   BEGIN
      SELECT WORKLIST_CATEGORY, WORKLIST_TRANS_ID, WORKLIST_PERSON_ID
        INTO L_WORKLIST_CATEGORY, L_WORKLIST_TRANS_ID, L_WORKLIST_PERSON_ID
        FROM AS_WORKLIST
       WHERE WORKLIST_ID = IN_WORKLIST_ID;

      IF L_WORKLIST_CATEGORY = 'SCHDLR'
      THEN
         SELECT MIN (SCHEDULER_ACTION_HISTORY_ID)
           INTO L_NXT_ACTION_HISTORY_ID
           FROM AS_SCHEDULER_ACTION_HISTORY
          WHERE     TRANS_ID = L_WORKLIST_TRANS_ID
                AND TRANS_TYPE = L_WORKLIST_CATEGORY
                AND ACTION_TYPE_TAKEN IS NULL;

         IF L_NXT_ACTION_HISTORY_ID IS NULL
         THEN
            OUT_IS_FINAL_APPROVER := 'Y';
         ELSE
            OUT_IS_FINAL_APPROVER := 'N';

            SELECT PERSON_ID
              INTO OUT_NEXT_APPROVER_ID
              FROM AS_SCHEDULER_ACTION_HISTORY
             WHERE SCHEDULER_ACTION_HISTORY_ID = L_NXT_ACTION_HISTORY_ID;
         END IF;
      ELSIF L_WORKLIST_CATEGORY = 'TRANS'
      THEN
         SELECT MIN (SCHEDULER_ACTION_HISTORY_ID)
           INTO L_NXT_ACTION_HISTORY_ID
           FROM AS_SCHEDULER_ACTION_HISTORY
          WHERE     TRANS_ID = L_WORKLIST_TRANS_ID
                AND TRANS_TYPE = L_WORKLIST_CATEGORY
                AND ACTION_TYPE_TAKEN IS NULL;

         IF L_NXT_ACTION_HISTORY_ID IS NULL
         THEN
            OUT_IS_FINAL_APPROVER := 'Y';
         ELSE
            OUT_IS_FINAL_APPROVER := 'N';

            SELECT PERSON_ID
              INTO OUT_NEXT_APPROVER_ID
              FROM AS_SCHEDULER_ACTION_HISTORY
             WHERE SCHEDULER_ACTION_HISTORY_ID = L_NXT_ACTION_HISTORY_ID;
         END IF;
      END IF;
   END;

   PROCEDURE REPEAT_PATTERN (IN_SCHEDULER_STAFF_ID       NUMBER,
                             OUT_RESULT              OUT VARCHAR2)
   IS
      L_START_TIME_HOURS       AS_SCHEDULER_STAFF.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES     AS_SCHEDULER_STAFF.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS         AS_SCHEDULER_STAFF.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES       AS_SCHEDULER_STAFF.END_TIME_MINUTES%TYPE;
      L_SHIFT_ID               AS_SCHEDULER_STAFF.SHIFT_ID%TYPE;
      L_WORK_GROUP_ID          AS_SCHEDULER_STAFF.WORK_GROUP_ID%TYPE;
      L_SCHEDULAR_START_DATE   AS_SCHEDULER_STAFF.SCHEDULAR_START_DATE%TYPE;
      L_SCHEDULER_ID           AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID              AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_BULLETIN_FLAG          AS_SCHEDULER_STAFF.BULLETIN_FLAG%TYPE;
      L_PATTERN_ID             NUMBER;
      L_SCHDLR_END_DATE        DATE;
      L_SKIP_FIRST             BOOLEAN := FALSE;
      L_SCHEDULER_STAFF_ID     NUMBER;
      L_RECORD_EXISTS          NUMBER;
   BEGIN
      SELECT ASS.START_TIME_HOURS,
             ASS.START_TIME_MINUTES,
             ASS.END_TIME_HOURS,
             ASS.END_TIME_MINUTES,
             ASS.SHIFT_ID,
             ASS.WORK_GROUP_ID,
             ASS.SCHEDULAR_START_DATE,
             ASS.SCHEDULER_ID,
             ASS.PERSON_ID,
             ASS.BULLETIN_FLAG,
             ASCH.END_DATE
        INTO L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_SHIFT_ID,
             L_WORK_GROUP_ID,
             L_SCHEDULAR_START_DATE,
             L_SCHEDULER_ID,
             L_PERSON_ID,
             L_BULLETIN_FLAG,
             L_SCHDLR_END_DATE
        FROM AS_SCHEDULER_STAFF ASS, AS_SCHEDULERS ASCH
       WHERE ASS.SCHEDULER_ID = ASCH.SCHEDULER_ID
             AND ASS.SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
     ----Changed by Heba 31-july-2017
--      SELECT PATTERN_ID
--        INTO L_PATTERN_ID
--        FROM AS_SHIFTS
--       WHERE SHIFT_ID = L_SHIFT_ID;
---- End Changed By Heba
---- Added By Heba 31-7-2017---
 SELECT FK_PATTERN_ID
        INTO L_PATTERN_ID
        from AS_WORK_GROUPS_LINES
        where person_id=L_PERSON_ID and work_group_id=L_WORK_GROUP_ID;

----End Added By Heba 31 -7-2017
      WHILE TRUNC (L_SCHEDULAR_START_DATE) <= TRUNC (L_SCHDLR_END_DATE)
      LOOP
         FOR PTRN_REC IN (  SELECT ON_OFF_FLAG, NO_OF_DAYS
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = L_PATTERN_ID
                          ORDER BY ORDER_SEQ ASC)
         LOOP
            FOR DAYS_REC IN (    SELECT LEVEL
                                   FROM DUAL
                             CONNECT BY LEVEL <= PTRN_REC.NO_OF_DAYS)
            LOOP
               IF L_SKIP_FIRST
               THEN
                  L_SCHEDULAR_START_DATE := L_SCHEDULAR_START_DATE + 1;

                  IF TRUNC (L_SCHEDULAR_START_DATE) <=
                        TRUNC (L_SCHDLR_END_DATE)
                  THEN
                     IF PTRN_REC.ON_OFF_FLAG = 'ON'
                     THEN
                        L_RECORD_EXISTS :=
                           GET_SCHDLR_COUNT_ON_DATE (L_SCHEDULER_ID,
                                                     L_PERSON_ID,
                                                     L_SCHEDULAR_START_DATE);


                        IF L_RECORD_EXISTS = 0
                        THEN
                           L_SCHEDULER_STAFF_ID :=
                              AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

                           INSERT
                             INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                                      SCHEDULER_ID,
                                                      PERSON_ID,
                                                      SCHEDULAR_START_DATE,
                                                      SCHEDULAR_END_DATE,
                                                      START_TIME_HOURS,
                                                      START_TIME_MINUTES,
                                                      END_TIME_HOURS,
                                                      END_TIME_MINUTES,
                                                      SHIFT_ID,
                                                      WORK_GROUP_ID,
                                                      BULLETIN_FLAG)
                           VALUES (L_SCHEDULER_STAFF_ID,
                                   L_SCHEDULER_ID,
                                   L_PERSON_ID,
                                   L_SCHEDULAR_START_DATE,
                                   L_SCHEDULAR_START_DATE,
                                   L_START_TIME_HOURS,
                                   L_START_TIME_MINUTES,
                                   L_END_TIME_HOURS,
                                   L_END_TIME_MINUTES,
                                   L_SHIFT_ID,
                                   L_WORK_GROUP_ID,
                                   L_BULLETIN_FLAG);

                           INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                                          SCHEDULER_STAFF_BULLETIN_ID,
                                          SCHEDULER_STAFF_ID,
                                          BULLETIN_ID)
                              SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                                     L_SCHEDULER_STAFF_ID,
                                     BULLETIN_ID
                                FROM AS_SCHEDULER_STAFF_BULLETINS
                               WHERE SCHEDULER_STAFF_ID =
                                        IN_SCHEDULER_STAFF_ID;
                        END IF;
                     --
                     END IF;
                  END IF;
               ELSE
                  L_SKIP_FIRST := TRUE;
               END IF;
            END LOOP;
         END LOOP;
      END LOOP;

      COMMIT;
      OUT_RESULT := 'SUCCESS';
   EXCEPTION
      WHEN OTHERS
      THEN
         OUT_RESULT := 'FAIL';
   END REPEAT_PATTERN;
   
   
      PROCEDURE REPEAT_PATTERN_RANGE (IN_SCHEDULER_STAFF_ID       NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date
                            , OUT_RESULT              OUT VARCHAR2)
   IS
      L_START_TIME_HOURS       AS_SCHEDULER_STAFF.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES     AS_SCHEDULER_STAFF.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS         AS_SCHEDULER_STAFF.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES       AS_SCHEDULER_STAFF.END_TIME_MINUTES%TYPE;
      L_SHIFT_ID               AS_SCHEDULER_STAFF.SHIFT_ID%TYPE;
      L_WORK_GROUP_ID          AS_SCHEDULER_STAFF.WORK_GROUP_ID%TYPE;
      L_SCHEDULAR_START_DATE   AS_SCHEDULER_STAFF.SCHEDULAR_START_DATE%TYPE;
      L_SCHEDULER_ID           AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID              AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_BULLETIN_FLAG          AS_SCHEDULER_STAFF.BULLETIN_FLAG%TYPE;
      L_PATTERN_ID             NUMBER;
     -- L_SCHDLR_END_DATE        DATE;
      L_SKIP_FIRST             BOOLEAN := FALSE;
      L_SCHEDULER_STAFF_ID     NUMBER;
      L_RECORD_EXISTS          NUMBER;
      L_employees_count Number:=0;
   BEGIN
      SELECT ASS.START_TIME_HOURS,
             ASS.START_TIME_MINUTES,
             ASS.END_TIME_HOURS,
             ASS.END_TIME_MINUTES,
             ASS.SHIFT_ID,
             ASS.WORK_GROUP_ID,
             ASS.SCHEDULAR_START_DATE,
             ASS.SCHEDULER_ID,
             ASS.PERSON_ID,
             ASS.BULLETIN_FLAG
            -- ASCH.END_DATE
        INTO L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_SHIFT_ID,
             L_WORK_GROUP_ID,
             L_SCHEDULAR_START_DATE,
             L_SCHEDULER_ID,
             L_PERSON_ID,
             L_BULLETIN_FLAG
            -- L_SCHDLR_END_DATE
        FROM AS_SCHEDULER_STAFF ASS, AS_SCHEDULERS ASCH
       WHERE ASS.SCHEDULER_ID = ASCH.SCHEDULER_ID
             AND ASS.SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
     ----Changed by Heba 31-july-2017
--      SELECT PATTERN_ID
--        INTO L_PATTERN_ID
--        FROM AS_SHIFTS
--       WHERE SHIFT_ID = L_SHIFT_ID;
---- End Changed By Heba
---- Added By Heba 31-7-2017---
select count (SCHEDULER_STAFF_ID) into L_employees_count from as_scheduler_staff
where scheduler_id= L_SCHEDULER_ID 
and PERSON_ID=L_PERSON_ID
and schedular_start_date  between IN_SCHEDULAR_DATE +1 and IN_SCHEDULAR_END_DATE ;
if( L_employees_count=0)
then 
 SELECT FK_PATTERN_ID
        INTO L_PATTERN_ID
        from AS_WORK_GROUPS_LINES
        where person_id=L_PERSON_ID and work_group_id=L_WORK_GROUP_ID;

----End Added By Heba 31 -7-2017
      WHILE TRUNC (L_SCHEDULAR_START_DATE) <= TRUNC (IN_SCHEDULAR_END_DATE)
      LOOP
         FOR PTRN_REC IN (  SELECT ON_OFF_FLAG, NO_OF_DAYS
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = L_PATTERN_ID
                          ORDER BY ORDER_SEQ ASC)
         LOOP
            FOR DAYS_REC IN (    SELECT LEVEL
                                   FROM DUAL
                             CONNECT BY LEVEL <= PTRN_REC.NO_OF_DAYS)
            LOOP
               IF L_SKIP_FIRST
               THEN
                  L_SCHEDULAR_START_DATE := L_SCHEDULAR_START_DATE + 1;

                  IF TRUNC (L_SCHEDULAR_START_DATE) <=
                        TRUNC (IN_SCHEDULAR_END_DATE)
                  THEN
                     IF PTRN_REC.ON_OFF_FLAG = 'ON'
                     THEN
                        L_RECORD_EXISTS :=
                           GET_SCHDLR_COUNT_ON_DATE (L_SCHEDULER_ID,
                                                     L_PERSON_ID,
                                                     L_SCHEDULAR_START_DATE);


                        IF L_RECORD_EXISTS = 0
                        THEN
                           L_SCHEDULER_STAFF_ID :=
                              AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

                           INSERT
                             INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                                      SCHEDULER_ID,
                                                      PERSON_ID,
                                                      SCHEDULAR_START_DATE,
                                                      SCHEDULAR_END_DATE,
                                                      START_TIME_HOURS,
                                                      START_TIME_MINUTES,
                                                      END_TIME_HOURS,
                                                      END_TIME_MINUTES,
                                                      SHIFT_ID,
                                                      WORK_GROUP_ID,
                                                      BULLETIN_FLAG)
                           VALUES (L_SCHEDULER_STAFF_ID,
                                   L_SCHEDULER_ID,
                                   L_PERSON_ID,
                                   L_SCHEDULAR_START_DATE,
                                   L_SCHEDULAR_START_DATE,
                                   L_START_TIME_HOURS,
                                   L_START_TIME_MINUTES,
                                   L_END_TIME_HOURS,
                                   L_END_TIME_MINUTES,
                                   L_SHIFT_ID,
                                   L_WORK_GROUP_ID,
                                   L_BULLETIN_FLAG);

                           INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                                          SCHEDULER_STAFF_BULLETIN_ID,
                                          SCHEDULER_STAFF_ID,
                                          BULLETIN_ID)
                              SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                                     L_SCHEDULER_STAFF_ID,
                                     BULLETIN_ID
                                FROM AS_SCHEDULER_STAFF_BULLETINS
                               WHERE SCHEDULER_STAFF_ID =
                                        IN_SCHEDULER_STAFF_ID;
                        END IF;
                     --
                     END IF;
                  END IF;
               ELSE
                  L_SKIP_FIRST := TRUE;
               END IF;
            END LOOP;
         END LOOP;
      END LOOP;

      COMMIT;
      OUT_RESULT := 'SUCCESS';
       else 
         OUT_RESULT := 'OVERLAP';
         end if ;
   EXCEPTION
      WHEN OTHERS
      THEN
         OUT_RESULT := 'FAIL';
   END REPEAT_PATTERN_RANGE;
   PROCEDURE REPEAT_PATTERNNEWSROOM (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date, IN_FK_SHIFT_ID Number ,IN_FK_SHIFT_ATTRIBUTE_ID Number
                             ,OUT_RESULT              OUT VARCHAR2)
   IS
      L_START_TIME_HOURS       AS_SCHEDULER_STAFF.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES     AS_SCHEDULER_STAFF.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS         AS_SCHEDULER_STAFF.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES       AS_SCHEDULER_STAFF.END_TIME_MINUTES%TYPE;
      L_SHIFT_ID               AS_SCHEDULER_STAFF.SHIFT_ID%TYPE;
      L_WORK_GROUP_ID          AS_SCHEDULER_STAFF.WORK_GROUP_ID%TYPE;
      L_SCHEDULAR_START_DATE   AS_SCHEDULER_STAFF.SCHEDULAR_START_DATE%TYPE;
      L_SCHEDULER_ID           AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID              AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_BULLETIN_FLAG          AS_SCHEDULER_STAFF.BULLETIN_FLAG%TYPE;
     L_FK_SCHDLR_SHIFT_ATTRIBUTE_ID  AS_SCHEDULER_STAFF.FK_SCHDLR_SHIFT_ATTRIBUTE_ID%TYPE;
      L_PATTERN_ID             NUMBER;
      L_SCHDLR_END_DATE        DATE;
      L_SKIP_FIRST             BOOLEAN := FALSE;
      L_SCHEDULER_STAFF_ID     NUMBER;
      L_RECORD_EXISTS          NUMBER;
      L_employees_count Number:=0;
   BEGIN
      SELECT ASS.START_TIME_HOURS,
             ASS.START_TIME_MINUTES,
             ASS.END_TIME_HOURS,
             ASS.END_TIME_MINUTES,
             ASS.SHIFT_ID,
             ASS.WORK_GROUP_ID,
             ASS.SCHEDULAR_START_DATE,
             ASS.SCHEDULER_ID,
             ASS.PERSON_ID,
             ASS.BULLETIN_FLAG,
             ASCH.END_DATE,
             ASS.FK_SCHDLR_SHIFT_ATTRIBUTE_ID
        INTO L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_SHIFT_ID,
             L_WORK_GROUP_ID,
             L_SCHEDULAR_START_DATE,
             L_SCHEDULER_ID,
             L_PERSON_ID,
             L_BULLETIN_FLAG,
             L_SCHDLR_END_DATE,
             L_FK_SCHDLR_SHIFT_ATTRIBUTE_ID
        FROM AS_SCHEDULER_STAFF ASS, AS_SCHEDULERS ASCH
       WHERE ASS.SCHEDULER_ID = ASCH.SCHEDULER_ID
             AND ASS.SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
     ----Changed by Heba 31-july-2017
--      SELECT PATTERN_ID
--        INTO L_PATTERN_ID
--        FROM AS_SHIFTS
--       WHERE SHIFT_ID = L_SHIFT_ID;
---- End Changed By Heba
---- Added By Heba 31-7-2017---
-----Added By Heba 20 -11-2017 to check if other employees exist in the shift in the date range -----


select count (SCHEDULER_STAFF_ID) into L_employees_count from as_scheduler_staff
where scheduler_id= L_SCHEDULER_ID and shift_id=IN_FK_SHIFT_ID
and FK_SCHDLR_SHIFT_ATTRIBUTE_ID=IN_FK_SHIFT_ATTRIBUTE_ID
and schedular_start_date  between IN_SCHEDULAR_DATE +1 and L_SCHDLR_END_DATE ;
--
if( L_employees_count=0)
then 

 SELECT FK_PATTERN_ID
        INTO L_PATTERN_ID
        from AS_WORK_GROUPS_LINES
        where person_id=L_PERSON_ID and work_group_id=L_WORK_GROUP_ID;

----End Added By Heba 31 -7-2017
      WHILE TRUNC (L_SCHEDULAR_START_DATE) <= TRUNC (L_SCHDLR_END_DATE)
      LOOP
         FOR PTRN_REC IN (  SELECT ON_OFF_FLAG, NO_OF_DAYS
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = L_PATTERN_ID
                          ORDER BY ORDER_SEQ ASC)
         LOOP
            FOR DAYS_REC IN (    SELECT LEVEL
                                   FROM DUAL
                             CONNECT BY LEVEL <= PTRN_REC.NO_OF_DAYS)
            LOOP
               IF L_SKIP_FIRST
               THEN
                  L_SCHEDULAR_START_DATE := L_SCHEDULAR_START_DATE + 1;

                  IF TRUNC (L_SCHEDULAR_START_DATE) <=
                        TRUNC (L_SCHDLR_END_DATE)
                  THEN
                     IF PTRN_REC.ON_OFF_FLAG = 'ON'
                     THEN
                        L_RECORD_EXISTS :=
                           SCHDLRNEWSROOM_COUNT_ON_DATE(L_SCHEDULER_ID,IN_FK_SHIFT_ATTRIBUTE_ID,
                                                     L_PERSON_ID,
                                                     L_SCHEDULAR_START_DATE);


                        IF L_RECORD_EXISTS = 0
                        THEN
                           L_SCHEDULER_STAFF_ID :=
                              AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

                           INSERT
                             INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                                      SCHEDULER_ID,
                                                      PERSON_ID,
                                                      SCHEDULAR_START_DATE,
                                                      SCHEDULAR_END_DATE,
                                                      START_TIME_HOURS,
                                                      START_TIME_MINUTES,
                                                      END_TIME_HOURS,
                                                      END_TIME_MINUTES,
                                                      SHIFT_ID,
                                                      WORK_GROUP_ID,
                                                      BULLETIN_FLAG,FK_SCHDLR_SHIFT_ATTRIBUTE_ID)
                           VALUES (L_SCHEDULER_STAFF_ID,
                                   L_SCHEDULER_ID,
                                   L_PERSON_ID,
                                   L_SCHEDULAR_START_DATE,
                                   L_SCHEDULAR_START_DATE,
                                   L_START_TIME_HOURS,
                                   L_START_TIME_MINUTES,
                                   L_END_TIME_HOURS,
                                   L_END_TIME_MINUTES,
                                   L_SHIFT_ID,
                                   L_WORK_GROUP_ID,
                                   L_BULLETIN_FLAG,L_FK_SCHDLR_SHIFT_ATTRIBUTE_ID);

                           INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                                          SCHEDULER_STAFF_BULLETIN_ID,
                                          SCHEDULER_STAFF_ID,
                                          BULLETIN_ID)
                              SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                                     L_SCHEDULER_STAFF_ID,
                                     BULLETIN_ID
                                FROM AS_SCHEDULER_STAFF_BULLETINS
                               WHERE SCHEDULER_STAFF_ID =
                                        IN_SCHEDULER_STAFF_ID;
                        END IF;
                     --
                     END IF;
                  END IF;
               ELSE
                  L_SKIP_FIRST := TRUE;
               END IF;
            END LOOP;
         END LOOP;
      END LOOP;

      COMMIT;
      OUT_RESULT := 'SUCCESS';
      
        else 
         OUT_RESULT := 'OVERLAP';
         end if ;
      
      --OUT_RESULT :=to_number(L_employees_count);
   EXCEPTION
      WHEN OTHERS
      THEN
         OUT_RESULT := 'FAIL';
--       ;
   END REPEAT_PATTERNNEWSROOM;
   
   
   PROCEDURE  REPEATRANGE_PATTERNNEWSROOM (IN_SCHEDULER_STAFF_ID       NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date, IN_FK_SHIFT_ID Number ,IN_FK_SHIFT_ATTRIBUTE_ID Number
                             , OUT_RESULT OUT VARCHAR2)
   IS
      L_START_TIME_HOURS       AS_SCHEDULER_STAFF.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES     AS_SCHEDULER_STAFF.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS         AS_SCHEDULER_STAFF.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES       AS_SCHEDULER_STAFF.END_TIME_MINUTES%TYPE;
      L_SHIFT_ID               AS_SCHEDULER_STAFF.SHIFT_ID%TYPE;
      L_WORK_GROUP_ID          AS_SCHEDULER_STAFF.WORK_GROUP_ID%TYPE;
      L_SCHEDULAR_START_DATE   AS_SCHEDULER_STAFF.SCHEDULAR_START_DATE%TYPE;
      L_SCHEDULER_ID           AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID              AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_BULLETIN_FLAG          AS_SCHEDULER_STAFF.BULLETIN_FLAG%TYPE;
      L_FK_SCHDLR_SHIFT_ATTRIBUTE_ID  AS_SCHEDULER_STAFF.FK_SCHDLR_SHIFT_ATTRIBUTE_ID%TYPE;
      L_NOTES  AS_SCHEDULER_STAFF.NOTES%TYPE;
      L_PATTERN_ID             NUMBER;
    --  L_SCHDLR_END_DATE        DATE;
      L_SKIP_FIRST             BOOLEAN := FALSE;
      L_SCHEDULER_STAFF_ID     NUMBER;
      L_RECORD_EXISTS          NUMBER;
      L_employees_count Number:=0;
   BEGIN
      SELECT ASS.START_TIME_HOURS,
             ASS.START_TIME_MINUTES,
             ASS.END_TIME_HOURS,
             ASS.END_TIME_MINUTES,
             ASS.SHIFT_ID,
             ASS.WORK_GROUP_ID,
             ASS.SCHEDULAR_START_DATE,
             ASS.SCHEDULER_ID,
             ASS.PERSON_ID,
             ASS.BULLETIN_FLAG,
            -- ASCH.END_DATE,
             ASS.FK_SCHDLR_SHIFT_ATTRIBUTE_ID ,
             NOTES
        INTO L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_SHIFT_ID,
             L_WORK_GROUP_ID,
             L_SCHEDULAR_START_DATE,
             L_SCHEDULER_ID,
             L_PERSON_ID,
             L_BULLETIN_FLAG,
            -- L_SCHDLR_END_DATE,
             L_FK_SCHDLR_SHIFT_ATTRIBUTE_ID,
             L_NOTES
        FROM AS_SCHEDULER_STAFF ASS, AS_SCHEDULERS ASCH
       WHERE ASS.SCHEDULER_ID = ASCH.SCHEDULER_ID
             AND ASS.SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
     ----Changed by Heba 31-july-2017
--      SELECT PATTERN_ID
--        INTO L_PATTERN_ID
--        FROM AS_SHIFTS
--       WHERE SHIFT_ID = L_SHIFT_ID;
---- End Changed By Heba
---- Added By Heba 31-7-2017---
-----Added By Heba 20 -11-2017 to check if other employees exist in the shift in the date range -----


select count (SCHEDULER_STAFF_ID) into L_employees_count from as_scheduler_staff
where scheduler_id= L_SCHEDULER_ID and shift_id=IN_FK_SHIFT_ID
and FK_SCHDLR_SHIFT_ATTRIBUTE_ID=IN_FK_SHIFT_ATTRIBUTE_ID
and schedular_start_date  between IN_SCHEDULAR_DATE +1 and IN_SCHEDULAR_END_DATE ;
--
if( L_employees_count=0)
then 

 SELECT FK_PATTERN_ID
        INTO L_PATTERN_ID
        from AS_WORK_GROUPS_LINES
        where person_id=L_PERSON_ID and work_group_id=L_WORK_GROUP_ID;

----End Added By Heba 31 -7-2017
      WHILE TRUNC (L_SCHEDULAR_START_DATE) <= TRUNC (IN_SCHEDULAR_END_DATE)
      LOOP
         FOR PTRN_REC IN (  SELECT ON_OFF_FLAG, NO_OF_DAYS
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = L_PATTERN_ID
                          ORDER BY ORDER_SEQ ASC)
         LOOP
            FOR DAYS_REC IN (    SELECT LEVEL
                                   FROM DUAL
                             CONNECT BY LEVEL <= PTRN_REC.NO_OF_DAYS)
            LOOP
               IF L_SKIP_FIRST
               THEN
                  L_SCHEDULAR_START_DATE := L_SCHEDULAR_START_DATE + 1;

                  IF TRUNC (L_SCHEDULAR_START_DATE) <=
                        TRUNC (IN_SCHEDULAR_END_DATE)
                  THEN
                     IF PTRN_REC.ON_OFF_FLAG = 'ON'
                     THEN
                        L_RECORD_EXISTS :=
                           SCHDLRNEWSROOM_COUNT_ON_DATE(L_SCHEDULER_ID,IN_FK_SHIFT_ATTRIBUTE_ID,
                                                     L_PERSON_ID,
                                                     L_SCHEDULAR_START_DATE);


                        IF L_RECORD_EXISTS = 0
                        THEN
                           L_SCHEDULER_STAFF_ID :=
                              AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

                           INSERT
                             INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                                      SCHEDULER_ID,
                                                      PERSON_ID,
                                                      SCHEDULAR_START_DATE,
                                                      SCHEDULAR_END_DATE,
                                                      START_TIME_HOURS,
                                                      START_TIME_MINUTES,
                                                      END_TIME_HOURS,
                                                      END_TIME_MINUTES,
                                                      SHIFT_ID,
                                                      WORK_GROUP_ID,
                                                      BULLETIN_FLAG,FK_SCHDLR_SHIFT_ATTRIBUTE_ID,NOTES,REPLACE_TYPE)
                           VALUES (L_SCHEDULER_STAFF_ID,
                                   L_SCHEDULER_ID,
                                   L_PERSON_ID,
                                   L_SCHEDULAR_START_DATE,
                                   L_SCHEDULAR_START_DATE,
                                   L_START_TIME_HOURS,
                                   L_START_TIME_MINUTES,
                                   L_END_TIME_HOURS,
                                   L_END_TIME_MINUTES,
                                   L_SHIFT_ID,
                                   L_WORK_GROUP_ID,
                                   L_BULLETIN_FLAG,L_FK_SCHDLR_SHIFT_ATTRIBUTE_ID,L_NOTES,'No Replacement');

                           INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                                          SCHEDULER_STAFF_BULLETIN_ID,
                                          SCHEDULER_STAFF_ID,
                                          BULLETIN_ID)
                              SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                                     L_SCHEDULER_STAFF_ID,
                                     BULLETIN_ID
                                FROM AS_SCHEDULER_STAFF_BULLETINS
                               WHERE SCHEDULER_STAFF_ID =
                                        IN_SCHEDULER_STAFF_ID;
                        END IF;
                     --
                     END IF;
                  END IF;
               ELSE
                  L_SKIP_FIRST := TRUE;
               END IF;
            END LOOP;
         END LOOP;
      END LOOP;

      COMMIT;
      OUT_RESULT := 'SUCCESS';
      
        else 
         OUT_RESULT := 'OVERLAP';
         end if ;
      
      --OUT_RESULT :=to_number(L_employees_count);
   EXCEPTION
      WHEN OTHERS
      THEN
         OUT_RESULT := 'FAIL';
--       ;
   END REPEATRANGE_PATTERNNEWSROOM ;
   
                  PROCEDURE REPEAT_PATTERNPRESENTER (IN_SCHEDULER_STAFF_ID       NUMBER,IN_SCHEDULAR_DATE date,IN_FK_SECTION_ID Number
                             , OUT_RESULT              OUT VARCHAR2)
                             
                               IS
      L_START_TIME_HOURS       AS_SCHEDULER_STAFF.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES     AS_SCHEDULER_STAFF.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS         AS_SCHEDULER_STAFF.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES       AS_SCHEDULER_STAFF.END_TIME_MINUTES%TYPE;
       L_WORK_GROUP_ID          AS_SCHEDULER_STAFF.WORK_GROUP_ID%TYPE;
      L_SCHEDULAR_START_DATE   AS_SCHEDULER_STAFF.SCHEDULAR_START_DATE%TYPE;
      L_SCHEDULER_ID           AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_SHIFT_ID                  AS_SCHEDULER_STAFF.SHIFT_ID%TYPE;
    L_PERSON_ID              AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_BULLETIN_FLAG          AS_SCHEDULER_STAFF.BULLETIN_FLAG%TYPE;
     L_FK_SCHEDULER_SECTION_ID  AS_SCHEDULER_STAFF.FK_SCHEDULER_SECTION_ID%TYPE;
      L_PATTERN_ID             NUMBER;
      L_SCHDLR_END_DATE        DATE;
      L_SKIP_FIRST             BOOLEAN := FALSE;
      L_SCHEDULER_STAFF_ID     NUMBER;
      L_RECORD_EXISTS          NUMBER;
      L_employees_count Number:=0;
   BEGIN
      SELECT ASS.START_TIME_HOURS,
             ASS.START_TIME_MINUTES,
             ASS.END_TIME_HOURS,
             ASS.END_TIME_MINUTES,
             ASS.SHIFT_ID,
             ASS.WORK_GROUP_ID,
             ASS.SCHEDULAR_START_DATE,
             ASS.SCHEDULER_ID,
             ASS.PERSON_ID,
             ASS.BULLETIN_FLAG,
             ASCH.END_DATE,
             ASS.FK_SCHEDULER_SECTION_ID
        INTO L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_SHIFT_ID,
             L_WORK_GROUP_ID,
             L_SCHEDULAR_START_DATE,
             L_SCHEDULER_ID,
             L_PERSON_ID,
             L_BULLETIN_FLAG,
             L_SCHDLR_END_DATE,
             L_FK_SCHEDULER_SECTION_ID
        FROM AS_SCHEDULER_STAFF ASS, AS_SCHEDULERS ASCH
       WHERE ASS.SCHEDULER_ID = ASCH.SCHEDULER_ID
             AND ASS.SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
  


select count (SCHEDULER_STAFF_ID) into L_employees_count from as_scheduler_staff
where scheduler_id= L_SCHEDULER_ID 
and FK_SCHEDULER_SECTION_ID=IN_FK_SECTION_ID
and schedular_start_date  between IN_SCHEDULAR_DATE +1 and L_SCHDLR_END_DATE ;
--
if( L_employees_count=0)
then 

 SELECT FK_PATTERN_ID
        INTO L_PATTERN_ID
        from AS_WORK_GROUPS_LINES
        where person_id=L_PERSON_ID and work_group_id=L_WORK_GROUP_ID;

      WHILE TRUNC (L_SCHEDULAR_START_DATE) <= TRUNC (L_SCHDLR_END_DATE)
      LOOP
         FOR PTRN_REC IN (  SELECT ON_OFF_FLAG, NO_OF_DAYS
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = L_PATTERN_ID
                          ORDER BY ORDER_SEQ ASC)
         LOOP
            FOR DAYS_REC IN (    SELECT LEVEL
                                   FROM DUAL
                             CONNECT BY LEVEL <= PTRN_REC.NO_OF_DAYS)
            LOOP
               IF L_SKIP_FIRST
               THEN
                  L_SCHEDULAR_START_DATE := L_SCHEDULAR_START_DATE + 1;

                  IF TRUNC (L_SCHEDULAR_START_DATE) <=
                        TRUNC (L_SCHDLR_END_DATE)
                  THEN
                     IF PTRN_REC.ON_OFF_FLAG = 'ON'
                     THEN
                        L_RECORD_EXISTS :=
                           SCHDLRPRESENTER_COUNT_ON_DATE (L_SCHEDULER_ID,IN_FK_SECTION_ID,
                                                     L_PERSON_ID,
                                                     L_SCHEDULAR_START_DATE);


                        IF L_RECORD_EXISTS = 0
                        THEN
                           L_SCHEDULER_STAFF_ID :=
                              AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

                           INSERT
                             INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                                      SCHEDULER_ID,
                                                      PERSON_ID,
                                                      SCHEDULAR_START_DATE,
                                                      SCHEDULAR_END_DATE,
                                                      START_TIME_HOURS,
                                                      START_TIME_MINUTES,
                                                      END_TIME_HOURS,
                                                      END_TIME_MINUTES,
                                                      SHIFT_ID,
                                                      WORK_GROUP_ID,
                                                      BULLETIN_FLAG,FK_SCHEDULER_SECTION_ID)
                           VALUES (L_SCHEDULER_STAFF_ID,
                                   L_SCHEDULER_ID,
                                   L_PERSON_ID,
                                   L_SCHEDULAR_START_DATE,
                                   L_SCHEDULAR_START_DATE,
                                   L_START_TIME_HOURS,
                                   L_START_TIME_MINUTES,
                                   L_END_TIME_HOURS,
                                   L_END_TIME_MINUTES,
                                   L_SHIFT_ID,
                                   L_WORK_GROUP_ID,
                                   L_BULLETIN_FLAG,L_FK_SCHEDULER_SECTION_ID);

                           INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                                          SCHEDULER_STAFF_BULLETIN_ID,
                                          SCHEDULER_STAFF_ID,
                                          BULLETIN_ID)
                              SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                                     L_SCHEDULER_STAFF_ID,
                                     BULLETIN_ID
                                FROM AS_SCHEDULER_STAFF_BULLETINS
                               WHERE SCHEDULER_STAFF_ID =
                                        IN_SCHEDULER_STAFF_ID;
                        END IF;
                     --
                     END IF;
                  END IF;
               ELSE
                  L_SKIP_FIRST := TRUE;
               END IF;
            END LOOP;
         END LOOP;
      END LOOP;

      COMMIT;
      OUT_RESULT := 'SUCCESS';
      
        else 
         OUT_RESULT := 'OVERLAP';
         end if ;
      
      --OUT_RESULT :=to_number(L_employees_count);
   EXCEPTION
      WHEN OTHERS
      THEN
         OUT_RESULT := 'FAIL';
--       ;
   END REPEAT_PATTERNPRESENTER;
   
            PROCEDURE REPEATRANGE_PATTERNPRESENTER (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date,IN_FK_SECTION_ID Number
                             , OUT_RESULT              OUT VARCHAR2)
                             
                               IS
      L_START_TIME_HOURS       AS_SCHEDULER_STAFF.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES     AS_SCHEDULER_STAFF.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS         AS_SCHEDULER_STAFF.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES       AS_SCHEDULER_STAFF.END_TIME_MINUTES%TYPE;
       L_WORK_GROUP_ID          AS_SCHEDULER_STAFF.WORK_GROUP_ID%TYPE;
      L_SCHEDULAR_START_DATE   AS_SCHEDULER_STAFF.SCHEDULAR_START_DATE%TYPE;
      L_SCHEDULER_ID           AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_SHIFT_ID                  AS_SCHEDULER_STAFF.SHIFT_ID%TYPE;
    L_PERSON_ID              AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_BULLETIN_FLAG          AS_SCHEDULER_STAFF.BULLETIN_FLAG%TYPE;
     L_FK_SCHEDULER_SECTION_ID  AS_SCHEDULER_STAFF.FK_SCHEDULER_SECTION_ID%TYPE;
      L_NOTES                  AS_SCHEDULER_STAFF.NOTES%TYPE;
      L_PATTERN_ID             NUMBER;
     -- L_SCHDLR_END_DATE        DATE;
      L_SKIP_FIRST             BOOLEAN := FALSE;
      L_SCHEDULER_STAFF_ID     NUMBER;
      L_RECORD_EXISTS          NUMBER;
      L_employees_count Number:=0;
   BEGIN
      SELECT ASS.START_TIME_HOURS,
             ASS.START_TIME_MINUTES,
             ASS.END_TIME_HOURS,
             ASS.END_TIME_MINUTES,
             ASS.SHIFT_ID,
             ASS.WORK_GROUP_ID,
             ASS.SCHEDULAR_START_DATE,
             ASS.SCHEDULER_ID,
             ASS.PERSON_ID,
             ASS.BULLETIN_FLAG,
            
             ASS.FK_SCHEDULER_SECTION_ID ,
             ASS.NOTES
        INTO L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_SHIFT_ID,
             L_WORK_GROUP_ID,
             L_SCHEDULAR_START_DATE,
             L_SCHEDULER_ID,
             L_PERSON_ID,
             L_BULLETIN_FLAG,
            -- L_SCHDLR_END_DATE,
             L_FK_SCHEDULER_SECTION_ID ,
             L_NOTES 
        FROM AS_SCHEDULER_STAFF ASS, AS_SCHEDULERS ASCH
       WHERE ASS.SCHEDULER_ID = ASCH.SCHEDULER_ID
             AND ASS.SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
  


select count (SCHEDULER_STAFF_ID) into L_employees_count from as_scheduler_staff
where scheduler_id= L_SCHEDULER_ID 
and FK_SCHEDULER_SECTION_ID=IN_FK_SECTION_ID
and schedular_start_date  between IN_SCHEDULAR_DATE +1 and IN_SCHEDULAR_END_DATE ;
--
if( L_employees_count=0)
then 

 SELECT FK_PATTERN_ID
        INTO L_PATTERN_ID
        from AS_WORK_GROUPS_LINES
        where person_id=L_PERSON_ID and work_group_id=L_WORK_GROUP_ID;

      WHILE TRUNC (L_SCHEDULAR_START_DATE) <= TRUNC (IN_SCHEDULAR_END_DATE)
      LOOP
         FOR PTRN_REC IN (  SELECT ON_OFF_FLAG, NO_OF_DAYS
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = L_PATTERN_ID
                          ORDER BY ORDER_SEQ ASC)
         LOOP
            FOR DAYS_REC IN (    SELECT LEVEL
                                   FROM DUAL
                             CONNECT BY LEVEL <= PTRN_REC.NO_OF_DAYS)
            LOOP
               IF L_SKIP_FIRST
               THEN
                  L_SCHEDULAR_START_DATE := L_SCHEDULAR_START_DATE + 1;

                  IF TRUNC (L_SCHEDULAR_START_DATE) <=
                        TRUNC (IN_SCHEDULAR_END_DATE)
                  THEN
                     IF PTRN_REC.ON_OFF_FLAG = 'ON'
                     THEN
                        L_RECORD_EXISTS :=
                           SCHDLRPRESENTER_COUNT_ON_DATE (L_SCHEDULER_ID,IN_FK_SECTION_ID,
                                                     L_PERSON_ID,
                                                     L_SCHEDULAR_START_DATE);


                        IF L_RECORD_EXISTS = 0
                        THEN
                           L_SCHEDULER_STAFF_ID :=
                              AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

                           INSERT
                             INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                                      SCHEDULER_ID,
                                                      PERSON_ID,
                                                      SCHEDULAR_START_DATE,
                                                      SCHEDULAR_END_DATE,
                                                      START_TIME_HOURS,
                                                      START_TIME_MINUTES,
                                                      END_TIME_HOURS,
                                                      END_TIME_MINUTES,
                                                      SHIFT_ID,
                                                      WORK_GROUP_ID,
                                                      BULLETIN_FLAG,FK_SCHEDULER_SECTION_ID,NOTES,REPLACE_TYPE)
                           VALUES (L_SCHEDULER_STAFF_ID,
                                   L_SCHEDULER_ID,
                                   L_PERSON_ID,
                                   L_SCHEDULAR_START_DATE,
                                   L_SCHEDULAR_START_DATE,
                                   L_START_TIME_HOURS,
                                   L_START_TIME_MINUTES,
                                   L_END_TIME_HOURS,
                                   L_END_TIME_MINUTES,
                                   L_SHIFT_ID,
                                   L_WORK_GROUP_ID,
                                   L_BULLETIN_FLAG,L_FK_SCHEDULER_SECTION_ID,L_NOTES,'No Replacement');

                           INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                                          SCHEDULER_STAFF_BULLETIN_ID,
                                          SCHEDULER_STAFF_ID,
                                          BULLETIN_ID)
                              SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                                     L_SCHEDULER_STAFF_ID,
                                     BULLETIN_ID
                                FROM AS_SCHEDULER_STAFF_BULLETINS
                               WHERE SCHEDULER_STAFF_ID =
                                        IN_SCHEDULER_STAFF_ID;
                        END IF;
                     --
                     END IF;
                  END IF;
               ELSE
                  L_SKIP_FIRST := TRUE;
               END IF;
            END LOOP;
         END LOOP;
      END LOOP;

      COMMIT;
      OUT_RESULT := 'SUCCESS';
      
        else 
         OUT_RESULT := 'OVERLAP';
         end if ;
      
     -- OUT_RESULT :=to_number(L_employees_count);
--   EXCEPTION
--      WHEN OTHERS
--      THEN
--         OUT_RESULT := 'FAIL';
--       ;
   END REPEATRANGE_PATTERNPRESENTER;
                             
   PROCEDURE DELETE_ALL_SCHDLR (IN_SCHEDULER_STAFF_ID NUMBER)
   IS
      L_SCHEDULER_ID   AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID      AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
   BEGIN
      SELECT SCHEDULER_ID, PERSON_ID
        INTO L_SCHEDULER_ID, L_PERSON_ID
        FROM AS_SCHEDULER_STAFF
       WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
       
          DELETE FROM AS_SCHEDULER_STAFF_BULLETINS
        WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = L_SCHEDULER_ID AND PERSON_ID = L_PERSON_ID;

      COMMIT;
   END DELETE_ALL_SCHDLR;
      PROCEDURE DELETE_SCHDLR_RANGE (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date)
   IS
      L_SCHEDULER_ID   AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID      AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
   BEGIN
      SELECT SCHEDULER_ID, PERSON_ID
        INTO L_SCHEDULER_ID, L_PERSON_ID
        FROM AS_SCHEDULER_STAFF
       WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
       
       
       DELETE FROM AS_SCHEDULER_STAFF_BULLETINS
        WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = L_SCHEDULER_ID AND PERSON_ID = L_PERSON_ID
           and SCHEDULAR_START_DATE between IN_SCHEDULAR_DATE and IN_SCHEDULAR_END_DATE;  

      COMMIT;
   END DELETE_SCHDLR_RANGE;
   /* Added By Heba 19-Nov-2017*/
       PROCEDURE DELETE_ALL_SCHDLR_NEWSROOM (IN_SCHEDULER_STAFF_ID NUMBER)
       
        IS
      L_SCHEDULER_ID   AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_SHIFTATTR_ID      AS_SCHEDULER_STAFF.FK_SCHDLR_SHIFT_ATTRIBUTE_ID%TYPE;
   BEGIN
      SELECT SCHEDULER_ID, FK_SCHDLR_SHIFT_ATTRIBUTE_ID
        INTO L_SCHEDULER_ID, L_SHIFTATTR_ID
        FROM AS_SCHEDULER_STAFF
       WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
          DELETE FROM AS_SCHEDULER_STAFF_BULLETINS
        WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = L_SCHEDULER_ID AND fk_schdlr_shift_attribute_id = L_SHIFTATTR_ID;

      COMMIT;  
       
       
       END DELETE_ALL_SCHDLR_NEWSROOM;
                  PROCEDURE DELETE_ALL_SCHDLR_NROM_RANGE  (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date)
       
        IS
      L_SCHEDULER_ID   AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_SHIFTATTR_ID      AS_SCHEDULER_STAFF.FK_SCHDLR_SHIFT_ATTRIBUTE_ID%TYPE;
   BEGIN
      SELECT SCHEDULER_ID, FK_SCHDLR_SHIFT_ATTRIBUTE_ID
        INTO L_SCHEDULER_ID, L_SHIFTATTR_ID
        FROM AS_SCHEDULER_STAFF
       WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
       
          DELETE FROM AS_SCHEDULER_STAFF_BULLETINS
        WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = L_SCHEDULER_ID AND fk_schdlr_shift_attribute_id = L_SHIFTATTR_ID
            and SCHEDULAR_START_DATE between IN_SCHEDULAR_DATE and IN_SCHEDULAR_END_DATE;

      COMMIT;  
       
       
       END  DELETE_ALL_SCHDLR_NROM_RANGE ;
        PROCEDURE DELETE_ALL_SCHDLR_PRESENTER (IN_SCHEDULER_STAFF_ID NUMBER)
       
        IS
      L_SCHEDULER_ID   AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_SECTION_ID      AS_SCHEDULER_STAFF.FK_SCHEDULER_SECTION_ID%TYPE;
   BEGIN
      SELECT SCHEDULER_ID, FK_SCHEDULER_SECTION_ID
        INTO L_SCHEDULER_ID, L_SECTION_ID
        FROM AS_SCHEDULER_STAFF
       WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
       
          DELETE FROM AS_SCHEDULER_STAFF_BULLETINS
        WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = L_SCHEDULER_ID AND FK_SCHEDULER_SECTION_ID = L_SECTION_ID;

      COMMIT;  
       
       
       END DELETE_ALL_SCHDLR_PRESENTER;
               PROCEDURE DELETE_RANGE_SCHDLR_PRSNTR (IN_SCHEDULER_STAFF_ID NUMBER,IN_SCHEDULAR_DATE date,IN_SCHEDULAR_END_DATE date)
       
        IS
      L_SCHEDULER_ID   AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_SECTION_ID      AS_SCHEDULER_STAFF.FK_SCHEDULER_SECTION_ID%TYPE;
   BEGIN
      SELECT SCHEDULER_ID, FK_SCHEDULER_SECTION_ID
        INTO L_SCHEDULER_ID, L_SECTION_ID
        FROM AS_SCHEDULER_STAFF
       WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;
       
          DELETE FROM AS_SCHEDULER_STAFF_BULLETINS
        WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = L_SCHEDULER_ID AND FK_SCHEDULER_SECTION_ID = L_SECTION_ID
            and SCHEDULAR_START_DATE between IN_SCHEDULAR_DATE and IN_SCHEDULAR_END_DATE;

      COMMIT;  
       
       
       END DELETE_RANGE_SCHDLR_PRSNTR;
    PROCEDURE DELETE_SCHDLR (IN_SCHEDULER_STAFF_ID NUMBER)
   IS
  
   BEGIN
     
      DELETE FROM AS_SCHEDULER_STAFF_BULLETINS
        WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_STAFF_ID = IN_SCHEDULER_STAFF_ID;

      COMMIT;
   END DELETE_SCHDLR;

   FUNCTION GET_WRKGRP_ANNUAL_LEAVE_USED (IN_WORK_GROUP_ID NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      RETURN 0;
   END GET_WRKGRP_ANNUAL_LEAVE_USED;

   FUNCTION GET_SCHDLR_COUNT_ON_DATE (IN_SCHEDULER_ID            NUMBER,
                                      IN_PERSON_ID               NUMBER,
                                      IN_SCHEDULAR_START_DATE    DATE)
      RETURN NUMBER
   IS
      L_RECORD_EXISTS   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO L_RECORD_EXISTS
        FROM AS_SCHEDULER_STAFF
       WHERE PERSON_ID = IN_PERSON_ID AND SCHEDULER_ID = IN_SCHEDULER_ID
             AND TRUNC (SCHEDULAR_START_DATE) =
                    TRUNC (IN_SCHEDULAR_START_DATE);

      RETURN L_RECORD_EXISTS;
   END GET_SCHDLR_COUNT_ON_DATE;
   
        FUNCTION SCHDLRNEWSROOM_COUNT_ON_DATE (IN_SCHEDULER_ID            NUMBER,INSHIFTATTRIBUTE_ID Number,
                                      IN_PERSON_ID               NUMBER,
                                      IN_SCHEDULAR_START_DATE    DATE)RETURN NUMBER
      
   IS
      L_RECORD_EXISTS   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO L_RECORD_EXISTS
        FROM AS_SCHEDULER_STAFF
       WHERE PERSON_ID = IN_PERSON_ID AND SCHEDULER_ID = IN_SCHEDULER_ID
       and FK_SCHDLR_SHIFT_ATTRIBUTE_ID=INSHIFTATTRIBUTE_ID
             AND TRUNC (SCHEDULAR_START_DATE) =
                    TRUNC (IN_SCHEDULAR_START_DATE);

      RETURN L_RECORD_EXISTS;
   END SCHDLRNEWSROOM_COUNT_ON_DATE;
    FUNCTION SCHDLRPRESENTER_COUNT_ON_DATE (IN_SCHEDULER_ID            NUMBER,INSECTION_ID Number,
                                      IN_PERSON_ID               NUMBER,
                                      IN_SCHEDULAR_START_DATE    DATE)RETURN NUMBER
     
   IS
      L_RECORD_EXISTS   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO L_RECORD_EXISTS
        FROM AS_SCHEDULER_STAFF
       WHERE PERSON_ID = IN_PERSON_ID AND SCHEDULER_ID = IN_SCHEDULER_ID
       and FK_SCHEDULER_SECTION_ID=INSECTION_ID
             AND TRUNC (SCHEDULAR_START_DATE) =
                    TRUNC (IN_SCHEDULAR_START_DATE);

      RETURN L_RECORD_EXISTS;
   END SCHDLRPRESENTER_COUNT_ON_DATE;

   PROCEDURE SCHDLR_INTERSECT_ASSIGNMENT (
      IN_SCHEDULER_ID               NUMBER,
      IN_PERSON_ID                  NUMBER,
      IN_SCHEDULAR_START_DATE       DATE,
      OUT_IS_EXIST              OUT VARCHAR2,
      OUT_EXIST_MESSAGE         OUT VARCHAR2)
   IS
      L_COUNT   NUMBER;
   BEGIN
      OUT_IS_EXIST := 'N';
      OUT_EXIST_MESSAGE := NULL;

      SELECT COUNT (1)
        INTO L_COUNT
        FROM AS_ASSIGNMENT_EMPS ASE
       WHERE PERSON_ID = IN_PERSON_ID
             AND TRUNC (IN_SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                            EMP_START_DATE)
                                                     AND TRUNC (EMP_END_DATE);

      IF L_COUNT > 0
      THEN
         OUT_IS_EXIST := 'Y';

         SELECT    'Employee is entered on assignment '
                || ASSIGN.ASSIGNMENT_NAME
                || ' from '
                || TO_CHAR (ASE.EMP_START_DATE, 'dd-MON-rrrr')
                || ' to '
                || TO_CHAR (ASE.EMP_END_DATE, 'dd-MON-rrrr')
           INTO OUT_EXIST_MESSAGE
           FROM AS_ASSIGNMENT_EMPS ASE, AS_ASSIGNMENTS ASSIGN
          WHERE ASE.ASSIGNMENT_ID = ASSIGN.ASSIGNMENT_ID
                AND PERSON_ID = IN_PERSON_ID
                AND TRUNC (IN_SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                               EMP_START_DATE)
                                                        AND TRUNC (
                                                               EMP_END_DATE)
                AND ROWNUM < 2;
      END IF;
   END SCHDLR_INTERSECT_ASSIGNMENT;

   PROCEDURE SCHDLR_INTERSECT_LEAVE (IN_SCHEDULER_ID               NUMBER,
                                     IN_PERSON_ID                  NUMBER,
                                     IN_SCHEDULAR_START_DATE       DATE,
                                     OUT_IS_EXIST              OUT VARCHAR2,
                                     OUT_EXIST_MESSAGE         OUT VARCHAR2)
   IS
      L_COUNT   NUMBER;
   BEGIN
      OUT_IS_EXIST := 'N';
      OUT_EXIST_MESSAGE := NULL;

      SELECT COUNT (1)
        INTO L_COUNT
        FROM per_absence_attendances paa
       WHERE PERSON_ID = IN_PERSON_ID
             AND TRUNC (IN_SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                            NVL (
                                                               paa.date_start,
                                                               paa.date_projected_start))
                                                     AND TRUNC (
                                                            NVL (
                                                               paa.date_end,
                                                               paa.date_projected_end));



      IF L_COUNT > 0
      THEN
         OUT_IS_EXIST := 'Y';

         SELECT 'Employee has leave ' || ' from '
                || TO_CHAR (NVL (paa.date_start, paa.date_projected_start),
                            'dd-MON-rrrr')
                || ' to '
                || TO_CHAR (NVL (paa.date_end, paa.date_projected_end),
                            'dd-MON-rrrr')
           INTO OUT_EXIST_MESSAGE
           FROM per_absence_attendances paa
          WHERE PERSON_ID = IN_PERSON_ID
                AND TRUNC (IN_SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                               NVL (
                                                                  paa.date_start,
                                                                  paa.date_projected_start))
                                                        AND TRUNC (
                                                               NVL (
                                                                  paa.date_end,
                                                                  paa.date_projected_end))
                AND ROWNUM < 2;
      END IF;
   END SCHDLR_INTERSECT_LEAVE;

   PROCEDURE SUBMIT_SCHDLR_TRANS (IN_SCHEDULER_TRANS_HDR_ID    NUMBER,
                                  IN_PERSON_ID                 NUMBER)
   IS
      L_TRANS_TYPE          AS_SCHEDULER_TRANS_HDRS.TRANS_TYPE%TYPE;
      L_FIRST_APPROVER_ID   NUMBER;

      L_NOTIFICATION_ID     NUMBER;
      L_ACCESS_KEY          VARCHAR2 (80);
      L_NTF_SUBJECT         VARCHAR2 (512);
      L__NTF_BODY           CLOB;
      L_OUT_STATUS          VARCHAR2 (256);
   BEGIN
      SELECT TRANS_TYPE
        INTO L_TRANS_TYPE
        FROM AS_SCHEDULER_TRANS_HDRS
       WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

      UPDATE AS_SCHEDULER_TRANS_HDRS
         SET APPROVAL_STATUS = 'WAITING_APPROVAL'
       WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

      INSERT INTO AS_SCHEDULER_ACTION_HISTORY (SCHEDULER_ACTION_HISTORY_ID,
                                               TRANS_TYPE,
                                               TRANS_ID,
                                               ORDER_SEQ,
                                               PERSON_ID,
                                               ACTION_TYPE,
                                               ACTION_TYPE_TAKEN)
           VALUES (AS_SCHEDULER_ACTION_HIST_SEQ.NEXTVAL,
                   'TRANS',
                   IN_SCHEDULER_TRANS_HDR_ID,
                   1,
                   IN_PERSON_ID,
                   'SUBMIT',
                   'Submit for Approval');

      INSERT INTO AS_SCHEDULER_ACTION_HISTORY (SCHEDULER_ACTION_HISTORY_ID,
                                               TRANS_TYPE,
                                               TRANS_ID,
                                               ORDER_SEQ,
                                               PERSON_ID,
                                               ACTION_TYPE)
         SELECT AS_SCHEDULER_ACTION_HIST_SEQ.NEXTVAL,
                'TRANS',
                IN_SCHEDULER_TRANS_HDR_ID,
                ORDER_SEQ + 1,
                PERSON_ID,
                ACTION_TYPE
           FROM (  SELECT ROWNUM ORDER_SEQ, PERSON_ID, ACTION_TYPE
                     FROM AS_APPROVAL_TYPE_STEPS
                    WHERE APPROVAL_TYPE = L_TRANS_TYPE
                 ORDER BY ORDER_SEQ);


      BEGIN
         SELECT PERSON_ID
           INTO L_FIRST_APPROVER_ID
           FROM (  SELECT ROWNUM ORDER_SEQ, PERSON_ID, ACTION_TYPE
                     FROM AS_APPROVAL_TYPE_STEPS
                    WHERE APPROVAL_TYPE = L_TRANS_TYPE
                 ORDER BY ORDER_SEQ)
          WHERE ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;


      L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
      L_ACCESS_KEY := TO_CHAR (FND_CRYPTO.RANDOMNUMBER);

      AS_NTF_CONTENT_PKG.GET_TRANS_NTF (
         IN_SCHEDULER_TRANS_HDR_ID   => IN_SCHEDULER_TRANS_HDR_ID,
         IN_NOTIFICATION_ID          => L_NOTIFICATION_ID,
         IN_ACCESS_KEY               => L_ACCESS_KEY,
         OUT_NTF_SUBJECT             => L_NTF_SUBJECT,
         OUT_NTF_BODY                => L__NTF_BODY);

      AS_NOTIFICATIONS_PKG.INSERT_NOTIFICATION_API (
         P_NOTIFICATION_TYPE   => 'TRANS',
         P_SEND_SYSTEM         => 'SCHEDULAR',
         P_SYSTEM_KEY          => IN_SCHEDULER_TRANS_HDR_ID,
         P_FROM_EMAIL          => 'JASMINA.MUSIC@MBC.NET',
         P_RECEIPTS_PERSONID   => L_FIRST_APPROVER_ID,
         P_RECEIPTS_EMAIL      => 'JASMINA.MUSIC@MBC.NET',
         P_CC_EMAIL            => NULL,
         P_BCC_EMAIL           => NULL,
         P_ACCESS_KEY          => L_ACCESS_KEY,
         P_NTF_SUBJECT         => L_NTF_SUBJECT,
         P_NTF_BODY            => L__NTF_BODY,
         OUT_NOTIFICATION_ID   => L_NOTIFICATION_ID,
         OUT_STATUS            => L_OUT_STATUS);

      INSERT INTO AS_WORKLIST (WORKLIST_ID,
                               WORKLIST_CATEGORY,
                               WORKLIST_ACTION_TYPE,
                               WORKLIST_DATE,
                               WORKLIST_SUBJECT,
                               WORKLIST_STATUS,
                               WORKLIST_TRANS_ID,
                               WORKLIST_PERSON_ID,
                               WORKLIST_DUE_DATE,
                               NOTIFICATION_ID)
           VALUES (AS_WORKLIST_SEQ.NEXTVAL,
                   'TRANS',
                   'RESPOND',
                   SYSTIMESTAMP,
                   L_NTF_SUBJECT,
                   'OPEN',
                   IN_SCHEDULER_TRANS_HDR_ID,
                   L_FIRST_APPROVER_ID,
                   NULL,
                   L_NOTIFICATION_ID);

      COMMIT;
   END SUBMIT_SCHDLR_TRANS;

   PROCEDURE APPLY_SCHDLR_TRANS (IN_SCHEDULER_TRANS_HDR_ID        NUMBER,
                                 OUT_GEN_SCHEDULER_STAFF_ID   OUT NUMBER,
                                 OUT_STATUS                   OUT VARCHAR2)
   IS
      L_SCHEDULER_STAFF_ID       AS_SCHEDULER_TRANS_HDRS.SCHEDULER_STAFF_ID%TYPE;
      L_TRANS_TYPE               AS_SCHEDULER_TRANS_HDRS.TRANS_TYPE%TYPE;
      L_SUGGESTED_PERSON_ID      AS_SCHEDULER_TRANS_HDRS.SUGGESTED_PERSON_ID%TYPE;
      L_NEW_SCHEDULER_STAFF_ID   AS_SCHEDULER_TRANS_HDRS.SCHEDULER_STAFF_ID%TYPE;
      L_START_TIME_HOURS         AS_SCHEDULER_TRANS_HDRS.START_TIME_HOURS%TYPE;
      L_START_TIME_MINUTES       AS_SCHEDULER_TRANS_HDRS.START_TIME_MINUTES%TYPE;
      L_END_TIME_HOURS           AS_SCHEDULER_TRANS_HDRS.END_TIME_HOURS%TYPE;
      L_END_TIME_MINUTES         AS_SCHEDULER_TRANS_HDRS.END_TIME_MINUTES%TYPE;
      L_EFFECTIVE_START_DATE     AS_SCHEDULER_TRANS_HDRS.EFFECTIVE_START_DATE%TYPE;
      L_EFFECTIVE_END_DATE       AS_SCHEDULER_TRANS_HDRS.EFFECTIVE_END_DATE%TYPE;
      L_ASSIGNMENT_ID            AS_SCHEDULER_TRANS_HDRS.ASSIGNMENT_ID%TYPE;
      L_CHANGE_REASON            AS_SCHEDULER_TRANS_HDRS.CHANGE_REASON%TYPE;
      L_FIRST_APPROVER_ID        NUMBER;

      L_SCHEDULER_ID             AS_SCHEDULER_STAFF.SCHEDULER_ID%TYPE;
      L_PERSON_ID                AS_SCHEDULER_STAFF.PERSON_ID%TYPE;
      L_NTF_SUBJECT              VARCHAR2 (1024);
      L_NTF_BODY                 CLOB;
      L_OUT_STATUS               VARCHAR2 (512);
      L_COUNT                    NUMBER;
   BEGIN
      OUT_STATUS := 'SUCCESS';

      SELECT SCHEDULER_STAFF_ID,
             TRANS_TYPE,
             SUGGESTED_PERSON_ID,
             START_TIME_HOURS,
             START_TIME_MINUTES,
             END_TIME_HOURS,
             END_TIME_MINUTES,
             EFFECTIVE_START_DATE,
             EFFECTIVE_END_DATE,
             ASSIGNMENT_ID,
             CHANGE_REASON
        INTO L_SCHEDULER_STAFF_ID,
             L_TRANS_TYPE,
             L_SUGGESTED_PERSON_ID,
             L_START_TIME_HOURS,
             L_START_TIME_MINUTES,
             L_END_TIME_HOURS,
             L_END_TIME_MINUTES,
             L_EFFECTIVE_START_DATE,
             L_EFFECTIVE_END_DATE,
             L_ASSIGNMENT_ID,
             L_CHANGE_REASON
        FROM AS_SCHEDULER_TRANS_HDRS
       WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

      SELECT SCHEDULER_ID, PERSON_ID
        INTO L_SCHEDULER_ID, L_PERSON_ID
        FROM AS_SCHEDULER_STAFF ASS
       WHERE SCHEDULER_STAFF_ID = L_SCHEDULER_STAFF_ID;

      IF L_CHANGE_REASON = 'ASSIGNMENT' AND L_ASSIGNMENT_ID IS NOT NULL
      THEN
         SELECT COUNT (1)
           INTO L_COUNT
           FROM AS_ASSIGNMENT_EMPS ASE
          WHERE ASE.ASSIGNMENT_ID = L_ASSIGNMENT_ID
                AND ASE.PERSON_ID = L_PERSON_ID
                AND (TRUNC (L_EFFECTIVE_START_DATE) BETWEEN TRUNC (
                                                               ASE.EMP_START_DATE)
                                                        AND TRUNC (
                                                               ASE.EMP_END_DATE)
                     OR TRUNC (L_EFFECTIVE_END_DATE) BETWEEN TRUNC (
                                                                ASE.EMP_START_DATE)
                                                         AND TRUNC (
                                                                ASE.EMP_END_DATE));

         IF L_COUNT = 0
         THEN
            INSERT INTO AS_ASSIGNMENT_EMPS (ASSIGNMENT_EMP_ID,
                                            ASSIGNMENT_ID,
                                            PERSON_ID,
                                            EMP_START_DATE,
                                            EMP_END_DATE,
                                            NOTES)
                 VALUES (AS_ASSIGNMENT_EMPS_SEQ.NEXTVAL,
                         L_ASSIGNMENT_ID,
                         L_PERSON_ID,
                         L_EFFECTIVE_START_DATE,
                         L_EFFECTIVE_END_DATE,
                         'Automatic added because of Request Submission.');
         END IF;
      END IF;

      IF L_TRANS_TYPE = 'REPLACE'
      THEN
         FOR SCHDLR_REC
            IN (SELECT SCHEDULER_STAFF_ID,
                       SCHEDULER_ID,
                       SCHEDULAR_START_DATE,
                       SCHEDULAR_END_DATE,
                       START_TIME_HOURS,
                       START_TIME_MINUTES,
                       END_TIME_HOURS,
                       END_TIME_MINUTES,
                       SHIFT_ID,
                       WORK_GROUP_ID,
                       BULLETIN_FLAG,
                       PARENT_SCHEDULER_STAFF_ID
                  FROM AS_SCHEDULER_STAFF
                 WHERE SCHEDULER_STAFF_ID = L_SCHEDULER_STAFF_ID
                       OR (PERSON_ID = L_PERSON_ID
                           AND SCHEDULER_ID = L_SCHEDULER_ID
                           AND TRUNC (SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                                       L_EFFECTIVE_START_DATE)
                                                                AND TRUNC (
                                                                       L_EFFECTIVE_END_DATE)))
         LOOP
            L_NEW_SCHEDULER_STAFF_ID := AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

            INSERT INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                            SCHEDULER_ID,
                                            PERSON_ID,
                                            SCHEDULAR_START_DATE,
                                            SCHEDULAR_END_DATE,
                                            START_TIME_HOURS,
                                            START_TIME_MINUTES,
                                            END_TIME_HOURS,
                                            END_TIME_MINUTES,
                                            SHIFT_ID,
                                            WORK_GROUP_ID,
                                            RECURRING_FLAG,
                                            BULLETIN_FLAG)
                 VALUES (L_NEW_SCHEDULER_STAFF_ID,
                         SCHDLR_REC.SCHEDULER_ID,
                         L_SUGGESTED_PERSON_ID,
                         SCHDLR_REC.SCHEDULAR_START_DATE,
                         SCHDLR_REC.SCHEDULAR_END_DATE,
                         SCHDLR_REC.START_TIME_HOURS,
                         SCHDLR_REC.START_TIME_MINUTES,
                         SCHDLR_REC.END_TIME_HOURS,
                         SCHDLR_REC.END_TIME_MINUTES,
                         SCHDLR_REC.SHIFT_ID,
                         SCHDLR_REC.WORK_GROUP_ID,
                         'N',
                         SCHDLR_REC.BULLETIN_FLAG);


            IF SCHDLR_REC.BULLETIN_FLAG = 'Y'
            THEN
               INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                              SCHEDULER_STAFF_BULLETIN_ID,
                              SCHEDULER_STAFF_ID,
                              BULLETIN_ID)
                  SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                         L_NEW_SCHEDULER_STAFF_ID,
                         BULLETIN_ID
                    FROM AS_SCHEDULER_STAFF_BULLETINS
                   WHERE SCHEDULER_STAFF_ID = SCHDLR_REC.SCHEDULER_STAFF_ID;
            END IF;

            UPDATE AS_SCHEDULER_STAFF
               SET PARENT_SCHEDULER_STAFF_ID = L_NEW_SCHEDULER_STAFF_ID
             WHERE SCHEDULER_STAFF_ID = SCHDLR_REC.SCHEDULER_STAFF_ID;

            UPDATE AS_SCHEDULER_TRANS_HDRS
               SET GENERATED_SCHEDULER_STAFF_ID = L_NEW_SCHEDULER_STAFF_ID
             WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

            OUT_GEN_SCHEDULER_STAFF_ID := L_NEW_SCHEDULER_STAFF_ID;
         END LOOP;
      ELSIF L_TRANS_TYPE = 'CHANGE'
      THEN
         FOR SCHDLR_REC
            IN (SELECT SCHEDULER_STAFF_ID,
                       SCHEDULER_ID,
                       PERSON_ID,
                       SCHEDULAR_START_DATE,
                       SCHEDULAR_END_DATE,
                       START_TIME_HOURS,
                       START_TIME_MINUTES,
                       END_TIME_HOURS,
                       END_TIME_MINUTES,
                       SHIFT_ID,
                       WORK_GROUP_ID,
                       BULLETIN_FLAG,
                       PARENT_SCHEDULER_STAFF_ID
                  FROM AS_SCHEDULER_STAFF
                 WHERE SCHEDULER_STAFF_ID = L_SCHEDULER_STAFF_ID
                       OR (PERSON_ID = L_PERSON_ID
                           AND SCHEDULER_ID = L_SCHEDULER_ID
                           AND TRUNC (SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                                       L_EFFECTIVE_START_DATE)
                                                                AND TRUNC (
                                                                       L_EFFECTIVE_END_DATE)))
         LOOP
            L_NEW_SCHEDULER_STAFF_ID := AS_SCHEDULER_STAFF_SEQ.NEXTVAL;

            INSERT INTO AS_SCHEDULER_STAFF (SCHEDULER_STAFF_ID,
                                            SCHEDULER_ID,
                                            PERSON_ID,
                                            SCHEDULAR_START_DATE,
                                            SCHEDULAR_END_DATE,
                                            START_TIME_HOURS,
                                            START_TIME_MINUTES,
                                            END_TIME_HOURS,
                                            END_TIME_MINUTES,
                                            SHIFT_ID,
                                            WORK_GROUP_ID,
                                            RECURRING_FLAG,
                                            BULLETIN_FLAG)
                 VALUES (L_NEW_SCHEDULER_STAFF_ID,
                         SCHDLR_REC.SCHEDULER_ID,
                         SCHDLR_REC.PERSON_ID,
                         SCHDLR_REC.SCHEDULAR_START_DATE,
                         SCHDLR_REC.SCHEDULAR_END_DATE,
                         L_START_TIME_HOURS,
                         L_START_TIME_MINUTES,
                         L_END_TIME_HOURS,
                         L_END_TIME_MINUTES,
                         SCHDLR_REC.SHIFT_ID,
                         SCHDLR_REC.WORK_GROUP_ID,
                         'N',
                         SCHDLR_REC.BULLETIN_FLAG);


            IF SCHDLR_REC.BULLETIN_FLAG = 'Y'
            THEN
               INSERT INTO AS_SCHEDULER_STAFF_BULLETINS (
                              SCHEDULER_STAFF_BULLETIN_ID,
                              SCHEDULER_STAFF_ID,
                              BULLETIN_ID)
                  SELECT AS_SCHDLR_STAFF_BULLETINS_SEQ.NEXTVAL,
                         L_NEW_SCHEDULER_STAFF_ID,
                         BULLETIN_ID
                    FROM AS_SCHEDULER_STAFF_BULLETINS
                   WHERE SCHEDULER_STAFF_ID = SCHDLR_REC.SCHEDULER_STAFF_ID;
            END IF;

            UPDATE AS_SCHEDULER_STAFF
               SET PARENT_SCHEDULER_STAFF_ID = L_NEW_SCHEDULER_STAFF_ID
             WHERE SCHEDULER_STAFF_ID = SCHDLR_REC.SCHEDULER_STAFF_ID;

            UPDATE AS_SCHEDULER_TRANS_HDRS
               SET GENERATED_SCHEDULER_STAFF_ID = L_NEW_SCHEDULER_STAFF_ID
             WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;


            OUT_GEN_SCHEDULER_STAFF_ID := L_NEW_SCHEDULER_STAFF_ID;
         END LOOP;
      ELSIF L_TRANS_TYPE = 'CANCEL'
      THEN
         UPDATE AS_SCHEDULER_STAFF
            SET PARENT_SCHEDULER_STAFF_ID = -1
          WHERE SCHEDULER_STAFF_ID = L_SCHEDULER_STAFF_ID
                OR (PERSON_ID = L_PERSON_ID AND SCHEDULER_ID = L_SCHEDULER_ID
                    AND TRUNC (SCHEDULAR_START_DATE) BETWEEN TRUNC (
                                                                L_EFFECTIVE_START_DATE)
                                                         AND TRUNC (
                                                                L_EFFECTIVE_END_DATE));

         UPDATE AS_SCHEDULER_TRANS_HDRS
            SET GENERATED_SCHEDULER_STAFF_ID = -1
          WHERE SCHEDULER_TRANS_HDR_ID = IN_SCHEDULER_TRANS_HDR_ID;

         OUT_GEN_SCHEDULER_STAFF_ID := -1;
      END IF;



      COMMIT;
   END APPLY_SCHDLR_TRANS;

   PROCEDURE SUBMIT_SCHEDULER (IN_SCHEDULER_ID       NUMBER,
                               IN_PERSON_ID          NUMBER,
                               OUT_STATUS        OUT VARCHAR2)
   IS
      L_FIRST_APPROVER_ID   NUMBER;

      L_NOTIFICATION_ID     NUMBER;
      L_ACCESS_KEY          VARCHAR2 (80);
      L_NTF_SUBJECT         VARCHAR2 (512);
      L__NTF_BODY           CLOB;
      L_OUT_STATUS          VARCHAR2 (256);
   BEGIN
      OUT_STATUS := 'SUCCESS';

      UPDATE AS_SCHEDULERS
         SET SCHEDULAR_STATUS = 'WAITING_APPROVAL'
       WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      INSERT INTO AS_SCHEDULER_ACTION_HISTORY (SCHEDULER_ACTION_HISTORY_ID,
                                               TRANS_TYPE,
                                               TRANS_ID,
                                               ORDER_SEQ,
                                               PERSON_ID,
                                               ACTION_TYPE,
                                               ACTION_TYPE_TAKEN)
           VALUES (AS_SCHEDULER_ACTION_HIST_SEQ.NEXTVAL,
                   'SCHDLR',
                   IN_SCHEDULER_ID,
                   1,
                   IN_PERSON_ID,
                   'SUBMIT',
                   'Submit for Approval');

      INSERT INTO AS_SCHEDULER_ACTION_HISTORY (SCHEDULER_ACTION_HISTORY_ID,
                                               TRANS_TYPE,
                                               TRANS_ID,
                                               ORDER_SEQ,
                                               PERSON_ID,
                                               ACTION_TYPE)
         SELECT AS_SCHEDULER_ACTION_HIST_SEQ.NEXTVAL,
                'SCHDLR',
                IN_SCHEDULER_ID,
                ORDER_SEQ + 1,
                PERSON_ID,
                ACTION_TYPE
           FROM (  SELECT ROWNUM ORDER_SEQ, PERSON_ID, ACTION_TYPE
                     FROM AS_APPROVAL_TYPE_STEPS
                    WHERE APPROVAL_TYPE = 'SCHEDULAR'
                 ORDER BY ORDER_SEQ);

      BEGIN
         SELECT PERSON_ID
           INTO L_FIRST_APPROVER_ID
           FROM (  SELECT ROWNUM ORDER_SEQ, PERSON_ID, ACTION_TYPE
                     FROM AS_APPROVAL_TYPE_STEPS
                    WHERE APPROVAL_TYPE = 'SCHEDULAR'
                 ORDER BY ORDER_SEQ)
          WHERE ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;

      L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
      L_ACCESS_KEY := TO_CHAR (FND_CRYPTO.RANDOMNUMBER);

      AS_NTF_CONTENT_PKG.GET_SCHDLR_NTF (
         IN_SCHEDULER_ID      => IN_SCHEDULER_ID,
         IN_NOTIFICATION_ID   => L_NOTIFICATION_ID,
         IN_ACCESS_KEY        => L_ACCESS_KEY,
         OUT_NTF_SUBJECT      => L_NTF_SUBJECT,
         OUT_NTF_BODY         => L__NTF_BODY);

      AS_NOTIFICATIONS_PKG.INSERT_NOTIFICATION_API (
         P_NOTIFICATION_TYPE   => 'SCHDLR',
         P_SEND_SYSTEM         => 'SCHEDULAR',
         P_SYSTEM_KEY          => IN_SCHEDULER_ID,
         P_FROM_EMAIL          => 'JASMINA.MUSIC@MBC.NET',
         P_RECEIPTS_PERSONID   => L_FIRST_APPROVER_ID,
         P_RECEIPTS_EMAIL      => 'JASMINA.MUSIC@MBC.NET',
         P_CC_EMAIL            => NULL,
         P_BCC_EMAIL           => NULL,
         P_ACCESS_KEY          => L_ACCESS_KEY,
         P_NTF_SUBJECT         => L_NTF_SUBJECT,
         P_NTF_BODY            => L__NTF_BODY,
         OUT_NOTIFICATION_ID   => L_NOTIFICATION_ID,
         OUT_STATUS            => L_OUT_STATUS);

      INSERT INTO AS_WORKLIST (WORKLIST_ID,
                               WORKLIST_CATEGORY,
                               WORKLIST_ACTION_TYPE,
                               WORKLIST_DATE,
                               WORKLIST_SUBJECT,
                               WORKLIST_STATUS,
                               WORKLIST_TRANS_ID,
                               WORKLIST_PERSON_ID,
                               WORKLIST_DUE_DATE,
                               NOTIFICATION_ID)
           VALUES (AS_WORKLIST_SEQ.NEXTVAL,
                   'SCHDLR',
                   'RESPOND',
                   SYSTIMESTAMP,
                   L_NTF_SUBJECT,
                   'OPEN',
                   IN_SCHEDULER_ID,
                   L_FIRST_APPROVER_ID,
                   NULL,
                   L_NOTIFICATION_ID);

      COMMIT;
   END SUBMIT_SCHEDULER;

   PROCEDURE PUSH_SCHEDULER_CALENDAR (IN_SCHEDULER_ID       NUMBER,
                                      OUT_STATUS        OUT VARCHAR2)
   IS
      CURSOR STAFF_CUR
      IS
         SELECT DISTINCT ASSCHEDULERSTAFFEO.SCHEDULER_ID,
                         ASSCHEDULERSTAFFEO.PERSON_ID,
                         ASSCHEDULERSTAFFEO.SHIFT_ID,
                         ASSCHEDULERSTAFFEO.WORK_GROUP_ID,
                         PAPF.EMPLOYEE_NUMBER,
                         PAPF.FULL_NAME EMPLOYEE_NAME,
                         PAPF.EMAIL_ADDRESS,
                         ASS.SHIFT_NAME,
                         AWGH.WORK_GROUP_NAME
           FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,
                PER_ALL_PEOPLE_F PAPF,
                AS_SCHEDULERS SCHDLR,
                AS_SHIFTS ASS,
                AS_WORK_GROUPS_HDR AWGH
          WHERE ASSCHEDULERSTAFFEO.PERSON_ID = PAPF.PERSON_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = SCHDLR.SCHEDULER_ID
                AND ASSCHEDULERSTAFFEO.SHIFT_ID = ASS.SHIFT_ID(+)
                AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID = AWGH.WORK_GROUP_ID(+)
                AND ASSCHEDULERSTAFFEO.PARENT_SCHEDULER_STAFF_ID IS NULL
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = IN_SCHEDULER_ID;

      CURSOR STAFF_SCHDLR_CUR (
         IN_PERSON_ID NUMBER)
      IS
         SELECT ASSCHEDULERSTAFFEO.SCHEDULER_STAFF_ID,
                ASSCHEDULERSTAFFEO.SCHEDULER_ID,
                ASSCHEDULERSTAFFEO.PERSON_ID,
                ASSCHEDULERSTAFFEO.SCHEDULAR_START_DATE,
                ASSCHEDULERSTAFFEO.SCHEDULAR_END_DATE,
                ASSCHEDULERSTAFFEO.START_TIME_HOURS,
                ASSCHEDULERSTAFFEO.START_TIME_MINUTES,
                ASSCHEDULERSTAFFEO.END_TIME_HOURS,
                ASSCHEDULERSTAFFEO.END_TIME_MINUTES,
                ASSCHEDULERSTAFFEO.SHIFT_ID,
                ASSCHEDULERSTAFFEO.WORK_GROUP_ID,
                ASSCHEDULERSTAFFEO.RECURRING_FLAG,
                ASSCHEDULERSTAFFEO.BULLETIN_FLAG,
                ASSCHEDULERSTAFFEO.PARENT_SCHEDULER_STAFF_ID,
                PAPF.EMPLOYEE_NUMBER,
                PAPF.FULL_NAME EMPLOYEE_NAME,
                PAPF.EMAIL_ADDRESS,
                ASS.SHIFT_NAME,
                AWGH.WORK_GROUP_NAME,
                SCHDLR.SCHEDULER_TITLE
           FROM AS_SCHEDULER_STAFF ASSCHEDULERSTAFFEO,
                PER_ALL_PEOPLE_F PAPF,
                AS_SCHEDULERS SCHDLR,
                AS_SHIFTS ASS,
                AS_WORK_GROUPS_HDR AWGH
          WHERE ASSCHEDULERSTAFFEO.PERSON_ID = PAPF.PERSON_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = SCHDLR.SCHEDULER_ID
                AND ASSCHEDULERSTAFFEO.SHIFT_ID = ASS.SHIFT_ID(+)
                AND ASSCHEDULERSTAFFEO.WORK_GROUP_ID = AWGH.WORK_GROUP_ID(+)
                AND ASSCHEDULERSTAFFEO.PARENT_SCHEDULER_STAFF_ID IS NULL
                AND ASSCHEDULERSTAFFEO.SCHEDULER_ID = IN_SCHEDULER_ID
                AND ASSCHEDULERSTAFFEO.PERSON_ID = IN_PERSON_ID; --1442;-- IN_PERSON_ID;


      L_NTF_SUBJECT   VARCHAR2 (1024);
      L_NTF_BODY      CLOB;
   BEGIN
      OUT_STATUS := 'SUCCESS';

      FOR STAFF_REC IN STAFF_CUR
      LOOP
         FOR STAFF_SCHDLR_REC IN STAFF_SCHDLR_CUR (STAFF_REC.PERSON_ID)
         LOOP
            AS_NTF_CONTENT_PKG.GET_SCHDLR_CAL (
               IN_SCHEDULER_ID         => IN_SCHEDULER_ID,
               IN_SCHEDULER_STAFF_ID   => STAFF_SCHDLR_REC.SCHEDULER_STAFF_ID,
               OUT_CAL_SUBJECT         => L_NTF_SUBJECT,
               OUT_CAL_CONTENT         => L_NTF_BODY);



            AS_NOTIFICATIONS_PKG.SEND_CALENDAR (
               P_FROM_EMAIL       => 'rami.abudiab@mbc.net',
               P_RECEIPTS_EMAIL   => 'JASMINA.MUSIC@MBC.NET',
               P_NTF_SUBJECT      => L_NTF_SUBJECT,
               P_NTF_BODY         => L_NTF_BODY,
               OUT_STATUS         => OUT_STATUS);
         END LOOP;
      END LOOP;
   END PUSH_SCHEDULER_CALENDAR;

   PROCEDURE GET_WORKLIST_COUNT (IN_PERSONID        NUMBER,
                                 OUT_APPROVED   OUT NUMBER,
                                 OUT_REJECTED   OUT NUMBER,
                                 OUT_WAITING    OUT NUMBER)
   IS
   BEGIN
      SELECT COUNT (1)
        INTO OUT_APPROVED
        FROM AS_WORKLIST
       WHERE TAKE_ACTION = 'APPROVED' AND WORKLIST_PERSON_ID = IN_PERSONID;

      SELECT COUNT (1)
        INTO OUT_REJECTED
        FROM AS_WORKLIST
       WHERE TAKE_ACTION = 'REJECTED' AND WORKLIST_PERSON_ID = IN_PERSONID;

      SELECT COUNT (1)
        INTO OUT_WAITING
        FROM AS_WORKLIST
       WHERE WORKLIST_STATUS = 'OPEN' AND WORKLIST_PERSON_ID = IN_PERSONID;
   END GET_WORKLIST_COUNT;

   PROCEDURE WORKLIST_ACTION (IN_WORKLIST_ID    NUMBER,
                              IN_ACTION         VARCHAR2,
                              IN_NOTES          VARCHAR2)
   IS
      L_TRANS_TYPE               AS_SCHEDULER_TRANS_HDRS.TRANS_TYPE%TYPE;
      L_WORKLIST_CATEGORY        AS_WORKLIST.WORKLIST_CATEGORY%TYPE;
      L_WORKLIST_TRANS_ID        AS_WORKLIST.WORKLIST_TRANS_ID%TYPE;
      L_WORKLIST_PERSON_ID       AS_WORKLIST.WORKLIST_PERSON_ID%TYPE;
      L_GEN_SCHEDULER_STAFF_ID   NUMBER;
      L_NEXT_APPROVER_ID         NUMBER;
      L_IS_FINAL_APPROVER        VARCHAR2 (1);
      L_STATUS                   VARCHAR2 (1024);

      L_NOTIFICATION_ID          NUMBER;
      L_ACCESS_KEY               VARCHAR2 (80);
      L_NTF_SUBJECT              VARCHAR2 (512);
      L__NTF_BODY                CLOB;
      L_OUT_STATUS               VARCHAR2 (256);
   BEGIN
      SELECT WORKLIST_CATEGORY, WORKLIST_TRANS_ID, WORKLIST_PERSON_ID
        INTO L_WORKLIST_CATEGORY, L_WORKLIST_TRANS_ID, L_WORKLIST_PERSON_ID
        FROM AS_WORKLIST
       WHERE WORKLIST_ID = IN_WORKLIST_ID;

      UPDATE AS_WORKLIST
         SET WORKLIST_STATUS = 'CLOSED',
             TAKE_ACTION = IN_ACTION,
             NOTES = IN_NOTES,
             CLOSE_DATE = SYSTIMESTAMP
       WHERE WORKLIST_ID = IN_WORKLIST_ID;


      UPDATE AS_SCHEDULER_ACTION_HISTORY
         SET ACTION_TYPE_TAKEN = IN_ACTION, NOTES = IN_NOTES
       WHERE     TRANS_ID = L_WORKLIST_TRANS_ID
             AND PERSON_ID = L_WORKLIST_PERSON_ID
             AND TRANS_TYPE = L_WORKLIST_CATEGORY;

      IF L_WORKLIST_CATEGORY = 'SCHDLR'
      THEN
         IF IN_ACTION = 'APPROVED'
         THEN
            AS_SCHDLR_PKG.GET_NEXT_APPROVER (
               IN_WORKLIST_ID          => IN_WORKLIST_ID,
               OUT_IS_FINAL_APPROVER   => L_IS_FINAL_APPROVER,
               OUT_NEXT_APPROVER_ID    => L_NEXT_APPROVER_ID);

            IF L_IS_FINAL_APPROVER = 'Y'
            THEN
               UPDATE AS_SCHEDULERS
                  SET SCHEDULAR_STATUS = 'PUBLISHED'
                WHERE SCHEDULER_ID = L_WORKLIST_TRANS_ID;

               COMMIT;

               PUSH_SCHEDULER_CALENDAR (
                  IN_SCHEDULER_ID   => L_WORKLIST_TRANS_ID,
                  OUT_STATUS        => L_STATUS);
            ELSE
               L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
               L_ACCESS_KEY := TO_CHAR (FND_CRYPTO.RANDOMNUMBER);


               AS_NTF_CONTENT_PKG.GET_SCHDLR_NTF (
                  IN_SCHEDULER_ID      => L_WORKLIST_TRANS_ID,
                  IN_NOTIFICATION_ID   => L_NOTIFICATION_ID,
                  IN_ACCESS_KEY        => L_ACCESS_KEY,
                  OUT_NTF_SUBJECT      => L_NTF_SUBJECT,
                  OUT_NTF_BODY         => L__NTF_BODY);



               AS_NOTIFICATIONS_PKG.INSERT_NOTIFICATION_API (
                  P_NOTIFICATION_TYPE   => 'SCHDLR',
                  P_SEND_SYSTEM         => 'SCHEDULAR',
                  P_SYSTEM_KEY          => L_WORKLIST_TRANS_ID,
                  P_FROM_EMAIL          => 'JASMINA.MUSIC@MBC.NET',
                  P_RECEIPTS_PERSONID   => L_NEXT_APPROVER_ID,
                  P_RECEIPTS_EMAIL      => 'JASMINA.MUSIC@MBC.NET',
                  P_CC_EMAIL            => NULL,
                  P_BCC_EMAIL           => NULL,
                  P_ACCESS_KEY          => L_ACCESS_KEY,
                  P_NTF_SUBJECT         => L_NTF_SUBJECT,
                  P_NTF_BODY            => L__NTF_BODY,
                  OUT_NOTIFICATION_ID   => L_NOTIFICATION_ID,
                  OUT_STATUS            => L_OUT_STATUS);

               INSERT INTO AS_WORKLIST (WORKLIST_ID,
                                        WORKLIST_CATEGORY,
                                        WORKLIST_ACTION_TYPE,
                                        WORKLIST_DATE,
                                        WORKLIST_SUBJECT,
                                        WORKLIST_STATUS,
                                        WORKLIST_TRANS_ID,
                                        WORKLIST_PERSON_ID,
                                        WORKLIST_DUE_DATE,
                                        NOTIFICATION_ID)
                    VALUES (AS_WORKLIST_SEQ.NEXTVAL,
                            'SCHDLR',
                            'RESPOND',
                            SYSTIMESTAMP,
                            L_NTF_SUBJECT,
                            'OPEN',
                            L_WORKLIST_TRANS_ID,
                            L_NEXT_APPROVER_ID,
                            NULL,
                            L_NOTIFICATION_ID);
            END IF;
         ELSIF IN_ACTION = 'REJECTED'
         THEN
            UPDATE AS_SCHEDULERS
               SET SCHEDULAR_STATUS = IN_ACTION
             WHERE SCHEDULER_ID = L_WORKLIST_TRANS_ID;
         END IF;
      ELSIF L_WORKLIST_CATEGORY = 'TRANS'
      THEN
         SELECT TRANS_TYPE
           INTO L_TRANS_TYPE
           FROM AS_SCHEDULER_TRANS_HDRS
          WHERE SCHEDULER_TRANS_HDR_ID = L_WORKLIST_TRANS_ID;


         IF IN_ACTION = 'APPROVED'
         THEN
            AS_SCHDLR_PKG.GET_NEXT_APPROVER (
               IN_WORKLIST_ID          => IN_WORKLIST_ID,
               OUT_IS_FINAL_APPROVER   => L_IS_FINAL_APPROVER,
               OUT_NEXT_APPROVER_ID    => L_NEXT_APPROVER_ID);

            IF L_IS_FINAL_APPROVER = 'Y'
            THEN
               UPDATE AS_SCHEDULER_TRANS_HDRS
                  SET APPROVAL_STATUS = IN_ACTION
                WHERE SCHEDULER_TRANS_HDR_ID = L_WORKLIST_TRANS_ID;

               APPLY_SCHDLR_TRANS (
                  IN_SCHEDULER_TRANS_HDR_ID    => L_WORKLIST_TRANS_ID,
                  OUT_GEN_SCHEDULER_STAFF_ID   => L_GEN_SCHEDULER_STAFF_ID,
                  OUT_STATUS                   => L_STATUS);
            ELSE
               L_NOTIFICATION_ID := AS_NOTIFICATIONS_SEQ.NEXTVAL;
               L_ACCESS_KEY := TO_CHAR (FND_CRYPTO.RANDOMNUMBER);

               AS_NTF_CONTENT_PKG.GET_TRANS_NTF (
                  IN_SCHEDULER_TRANS_HDR_ID   => L_WORKLIST_TRANS_ID,
                  IN_NOTIFICATION_ID          => L_NOTIFICATION_ID,
                  IN_ACCESS_KEY               => L_ACCESS_KEY,
                  OUT_NTF_SUBJECT             => L_NTF_SUBJECT,
                  OUT_NTF_BODY                => L__NTF_BODY);

               AS_NOTIFICATIONS_PKG.INSERT_NOTIFICATION_API (
                  P_NOTIFICATION_TYPE   => 'TRANS',
                  P_SEND_SYSTEM         => 'SCHEDULAR',
                  P_SYSTEM_KEY          => L_WORKLIST_TRANS_ID,
                  P_FROM_EMAIL          => 'JASMINA.MUSIC@MBC.NET',
                  P_RECEIPTS_PERSONID   => L_NEXT_APPROVER_ID,
                  P_RECEIPTS_EMAIL      => 'JASMINA.MUSIC@MBC.NET',
                  P_CC_EMAIL            => NULL,
                  P_BCC_EMAIL           => NULL,
                  P_ACCESS_KEY          => L_ACCESS_KEY,
                  P_NTF_SUBJECT         => L_NTF_SUBJECT,
                  P_NTF_BODY            => L__NTF_BODY,
                  OUT_NOTIFICATION_ID   => L_NOTIFICATION_ID,
                  OUT_STATUS            => L_OUT_STATUS);


               INSERT INTO AS_WORKLIST (WORKLIST_ID,
                                        WORKLIST_CATEGORY,
                                        WORKLIST_ACTION_TYPE,
                                        WORKLIST_DATE,
                                        WORKLIST_SUBJECT,
                                        WORKLIST_STATUS,
                                        WORKLIST_TRANS_ID,
                                        WORKLIST_PERSON_ID,
                                        WORKLIST_DUE_DATE,
                                        NOTIFICATION_ID)
                    VALUES (AS_WORKLIST_SEQ.NEXTVAL,
                            'TRANS',
                            'RESPOND',
                            SYSTIMESTAMP,
                            L_NTF_SUBJECT,
                            'OPEN',
                            L_WORKLIST_TRANS_ID,
                            L_NEXT_APPROVER_ID,
                            NULL,
                            L_NOTIFICATION_ID);
            END IF;
         ELSIF IN_ACTION = 'REJECTED'
         THEN
            UPDATE AS_SCHEDULER_TRANS_HDRS
               SET APPROVAL_STATUS = IN_ACTION
             WHERE SCHEDULER_TRANS_HDR_ID = L_WORKLIST_TRANS_ID;
         END IF;
      END IF;



      COMMIT;
   END WORKLIST_ACTION;

   PROCEDURE VALIDATE_SCHEDULER (IN_SCHEDULER_ID           NUMBER,
                                 OUT_VALIDATE_REPORT   OUT CLOB)
   IS
      L_START_DATE     AS_SCHEDULERS.START_DATE%TYPE;
      L_END_DATE       AS_SCHEDULERS.END_DATE%TYPE;
      L_RET_CLOB       CLOB;
      L_VALIDATE_STR   VARCHAR2 (32000);
      L_COUNT          NUMBER;

      CURSOR SCHDLR_SHIFTS_CUR
      IS
         SELECT SCHDLR_SHFT.SCHEDULER_ID,
                SCHDLR_SHFT.SHIFT_ID,
                SHIFT.SHIFT_NAME,
                SCHDLR_SHFT.WORK_GROUP_ID,
                WRKGRP.WORK_GROUP_NAME,
                SHFT_WRKGRP.EMPLOYEES_COUNT,
                SHFT_WRKGRP.EMPLOYEES_COUNT_MAX
           FROM AS_SCHEDULER_SHIFTS SCHDLR_SHFT,
                AS_SHIFT_WORK_GROUPS SHFT_WRKGRP,
                AS_SHIFTS SHIFT,
                AS_WORK_GROUPS_HDR WRKGRP
          WHERE     SCHDLR_SHFT.SHIFT_ID = SHFT_WRKGRP.SHIFT_ID
                AND SCHDLR_SHFT.WORK_GROUP_ID = SHFT_WRKGRP.WORK_GROUP_ID
                AND SCHDLR_SHFT.SHIFT_ID = SHIFT.SHIFT_ID
                AND SCHDLR_SHFT.WORK_GROUP_ID = WRKGRP.WORK_GROUP_ID
                AND SCHDLR_SHFT.SCHEDULER_ID = IN_SCHEDULER_ID;

      CURSOR ASSIGNMENT_EMPS_CUR (
         IN_ASSIGNMENT_DATE DATE)
      IS
         SELECT PAPF.FULL_NAME EMPLOYEE_NAME
           FROM AS_SCHEDULER_STAFF STAFF, PER_ALL_PEOPLE_F PAPF
          WHERE STAFF.PERSON_ID = PAPF.PERSON_ID
                AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                AND PAPF.EFFECTIVE_END_DATE
                AND STAFF.SCHEDULER_ID = IN_SCHEDULER_ID
                AND STAFF.PARENT_SCHEDULER_STAFF_ID IS NULL
                AND TRUNC (STAFF.SCHEDULAR_START_DATE) =
                       TRUNC (IN_ASSIGNMENT_DATE)
                AND EXISTS
                       (SELECT 1
                          FROM AS_ASSIGNMENT_EMPS ASSIGN
                         WHERE STAFF.PERSON_ID = ASSIGN.PERSON_ID
                               AND TRUNC (IN_ASSIGNMENT_DATE) BETWEEN TRUNC (
                                                                         EMP_START_DATE)
                                                                  AND TRUNC (
                                                                         EMP_END_DATE));
   BEGIN
      DBMS_LOB.CREATETEMPORARY (L_RET_CLOB, TRUE);

      SELECT START_DATE, END_DATE
        INTO L_START_DATE, L_END_DATE
        FROM AS_SCHEDULERS SCHDLR
       WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      FOR DAYS_REC IN (    SELECT L_START_DATE + (LEVEL - 1) CURSOR_DATE
                             FROM DUAL
                       CONNECT BY LEVEL < (L_END_DATE - L_START_DATE + 2))
      LOOP
         L_VALIDATE_STR :=
               CHR (10)
            || CHR (10)
            || TO_CHAR (DAYS_REC.CURSOR_DATE, 'dd-mm-rrrr');
         DBMS_LOB.APPEND (L_RET_CLOB, L_VALIDATE_STR);

         --Checl min and max emps
         FOR SCHDLR_SHIFTS_REC IN SCHDLR_SHIFTS_CUR
         LOOP
            SELECT COUNT (1)
              INTO L_COUNT
              FROM AS_SCHEDULER_STAFF STAFF
             WHERE     STAFF.SCHEDULER_ID = IN_SCHEDULER_ID
                   AND STAFF.SHIFT_ID = SCHDLR_SHIFTS_REC.SHIFT_ID
                   AND STAFF.WORK_GROUP_ID = SCHDLR_SHIFTS_REC.WORK_GROUP_ID
                   AND STAFF.PARENT_SCHEDULER_STAFF_ID IS NULL
                   AND TRUNC (STAFF.SCHEDULAR_START_DATE) =
                          TRUNC (DAYS_REC.CURSOR_DATE);

            IF L_COUNT < SCHDLR_SHIFTS_REC.EMPLOYEES_COUNT
            THEN
               L_VALIDATE_STR :=
                     CHR (10)
                  || 'No. of employees in  '
                  || SCHDLR_SHIFTS_REC.SHIFT_NAME
                  || '-'
                  || SCHDLR_SHIFTS_REC.WORK_GROUP_NAME
                  || ' is '
                  || L_COUNT
                  || ' however is required '
                  || SCHDLR_SHIFTS_REC.EMPLOYEES_COUNT
                  || ' employee(s)';
               DBMS_LOB.APPEND (L_RET_CLOB, L_VALIDATE_STR);
            ELSIF L_COUNT > SCHDLR_SHIFTS_REC.EMPLOYEES_COUNT_MAX
            THEN
               L_VALIDATE_STR :=
                     CHR (10)
                  || 'No. of employees in  '
                  || SCHDLR_SHIFTS_REC.SHIFT_NAME
                  || '-'
                  || SCHDLR_SHIFTS_REC.WORK_GROUP_NAME
                  || ' is '
                  || L_COUNT
                  || ' however maximum number required is  '
                  || SCHDLR_SHIFTS_REC.EMPLOYEES_COUNT_MAX
                  || ' employee(s)';
               DBMS_LOB.APPEND (L_RET_CLOB, L_VALIDATE_STR);
            END IF;
         END LOOP;

         --check emp in assignments
         L_VALIDATE_STR := '';

         FOR ASSIGNMENT_EMPS_REC
            IN ASSIGNMENT_EMPS_CUR (DAYS_REC.CURSOR_DATE)
         LOOP
            L_VALIDATE_STR :=
               L_VALIDATE_STR || ',' || ASSIGNMENT_EMPS_REC.EMPLOYEE_NAME;
         END LOOP;

         IF LENGTH (L_VALIDATE_STR) > 0
         THEN
            L_VALIDATE_STR :=
               CHR (10) || SUBSTR (L_VALIDATE_STR, 2) || ' are in assignment';
            DBMS_LOB.APPEND (L_RET_CLOB, L_VALIDATE_STR);
         END IF;
      END LOOP;

      OUT_VALIDATE_REPORT := L_RET_CLOB;
   END VALIDATE_SCHEDULER;

   FUNCTION CHECK_ASSIGNMENT_OVERLAPPING (IN_ASSIGNMENT_EMP_ID    NUMBER,
                                          IN_PERSON_ID            NUMBER,
                                          IN_START_DATE           DATE,
                                          IN_END_DATE             DATE)
      RETURN VARCHAR2
   IS
      L_COUNT   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO L_COUNT
        FROM AS_ASSIGNMENT_EMPS AE
       WHERE AE.ASSIGNMENT_EMP_ID <> IN_ASSIGNMENT_EMP_ID
             AND AE.PERSON_ID = IN_PERSON_ID
             AND (TRUNC (IN_START_DATE) BETWEEN TRUNC (AE.EMP_START_DATE)
                                            AND TRUNC (AE.EMP_END_DATE)
                  OR TRUNC (IN_END_DATE) BETWEEN TRUNC (AE.EMP_START_DATE)
                                             AND TRUNC (AE.EMP_END_DATE)
                  OR TRUNC (AE.EMP_START_DATE) BETWEEN TRUNC (IN_START_DATE)
                                                   AND TRUNC (IN_END_DATE)
                  OR TRUNC (AE.EMP_END_DATE) BETWEEN TRUNC (IN_START_DATE)
                                                 AND TRUNC (IN_END_DATE));

      IF L_COUNT = 0
      THEN
         RETURN 'N';
      ELSE
         RETURN 'Y';
      END IF;
   END CHECK_ASSIGNMENT_OVERLAPPING;

   PROCEDURE DELETE_AS_SCHEDULER (IN_SCHEDULER_ID NUMBER)
   IS
   BEGIN
   
   
    DELETE FROM AS_SHIFT_ATTRIBUTES
            WHERE FKSCHEDULER_ID = IN_SCHEDULER_ID;
            
            DELETE FROM AS_SCHEDULER_SECTIONS
             WHERE FKSCHEDULER_ID= IN_SCHEDULER_ID;
             
      DELETE FROM AS_SCHEDULER_SHIFTS
            WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      DELETE FROM AS_SCHEDULER_STAFF
            WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      DELETE FROM AS_SCHEDULER_APPROVERS
            WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      DELETE FROM AS_SCHEDULER_ACTION_HISTORY
            WHERE TRANS_ID = IN_SCHEDULER_ID AND TRANS_TYPE = 'SCHDLR';

      DELETE FROM AS_SCHEDULERS
            WHERE SCHEDULER_ID = IN_SCHEDULER_ID;

      COMMIT;
   END DELETE_AS_SCHEDULER;
   --Start  Added by Heba 11-july --
   function GetDateDifference(pstartdate date,penddate date)return number
   is
   datediff number;
   begin 
    select  (penddate
             - pstartdate) into datediff
       from dual;
       return datediff;
   
   
   end ;
   -- added By Heba 17-july-tas integration
   function VALIDATE_TAS_RECORD(pperson_id Number,p_employeecode varchar2,pshiftcode varchar2,pshiftdate date,pshiftstart number,pshiftend number)return varchar2
   as
   v_recordstatus varchar(200);
   count_person Number;
   count_shift Number ;
   vdateonly Date ;
   begin 
    vdateonly :=to_date(pshiftdate,'dd-mm-rrrr');
    --pshiftdate;
    --to_date(pshiftdate,'dd-mm-rrrr');
      ---first check select employeecode-personid and shiftdate
      select count (person_id) into count_person 
      from AS_SCHEDULAR_TAS
      where person_id=pperson_id and employee_code=p_employeecode and shift_date=vdateonly ;
       --if not exist status new
      if(count_person=0)
      then v_recordstatus:='NEW';
      elsif (count_person>0)
           then 
             --else if exist select shiftcode-shiftstart-shiftend 
        select count (shift_code) into count_shift 
      from AS_SCHEDULAR_TAS
      where person_id=pperson_id and employee_code=p_employeecode and shift_date=vdateonly 
      and shift_code=pshiftcode and shift_start_time=pshiftstart and shift_end_time=pshiftend ;
        --else if exist select shiftcode-shiftstart-shiftend 
      --if equal status no changes 
      if(count_shift=1)
      then 
      v_recordstatus :='NOCHANGES';
      elsif (count_shift=0)
      then
       v_recordstatus :='CHANGES';
      end if ;
              
              end if ;
              
         
         
           
     
     
      -- esle status changed 
      return v_recordstatus;
      --'UNDEFINED';
      -- ;
   end ;
   PROCEDURE INSERT_TAS_RECORD(pperson_id Number,p_employeecode varchar2,pshiftcode varchar2,pshiftdate date,pshiftstart number,pshiftend number,pstatus varchar2 ,poff_flag varchar2)
   is 
 vdateonly Date ;
   begin 
   vdateonly :=to_date(pshiftdate,'dd-mm-rrrr');
  -- insert into AS_SCHEDULAR_TAS (scheduler_tas_id,person_id,employee_code,shift_code,shift_date,shift_start_time,shift_end_time,off_flag) values(tas_seq.nextval,pperson_id,p_employeecode,pshiftcode,pshiftdate,pshiftstart,pshfitend,poff_flag);
   insert into AS_SCHEDULAR_TAS (scheduler_tas_id,person_id,employee_code,shift_code,shift_date,shift_start_time,shift_end_time,tas_status,off_flag) 
   values(tas_seq.nextval,pperson_id,p_employeecode,pshiftcode,vdateonly,pshiftstart,pshiftend,pstatus,poff_flag);
   
   commit;
   end ;
 function GET_ASSIGNMENT_DURATION(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2
 as
 cursor assign (cperson_id  in number ,cstartdate  in Date ,cendddate  in Date)
 is    SELECT '('|| to_char(ASE.EMP_START_DATE,'dd-MON-yyyy')||'/'||to_char(ASE.EMP_END_DATE,'dd-MON-yyyy')||')' Assigndur  
                 FROM AS_ASSIGNMENT_EMPS ASE   
                WHERE ASE.PERSON_ID = cperson_id   
                      AND (TRUNC (cstartdate) BETWEEN TRUNC (   
                                                                ASE.EMP_START_DATE)   
                                                         AND TRUNC (   
                                                                ASE.EMP_END_DATE)   
                           OR TRUNC (cendddate) BETWEEN TRUNC (   
                                                                 ASE.EMP_START_DATE)   
                                                          AND TRUNC (   
                                                                 ASE.EMP_END_DATE)   
                           OR TRUNC (ASE.EMP_START_DATE) BETWEEN TRUNC (   
                                                                   cstartdate)   
                                                             AND TRUNC (   
                                                                    cendddate)   
                           OR TRUNC (ASE.EMP_END_DATE) BETWEEN TRUNC (   
                                                                  cstartdate)   
                                                           AND TRUNC (   
                                                                  cendddate)); 
          concated_assignments varchar2(4000) ;
 begin
 --open assign (pperson_id  ,pstartdate  ,pendddate );
 --loop 
   FOR REC IN  assign (pperson_id  ,pstartdate  ,pendddate )
   loop
   
   concated_assignments :=concated_assignments||' '||REC.Assigndur;
   end loop ;
 return concated_assignments;
 end ;
 
  function GET_LEAVE_DURATION(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2
 as
  cursor lv (cperson_id  in number ,cstartdate  in Date ,cendddate  in Date)
-- is    SELECT distinct '('|| to_char(date_start,'dd/MON/yyyy')||' - '||to_char(date_end,'dd/MON/yyyy')||')'  leavedur     
 is    SELECT distinct  to_char(date_start,'dd/MON/yyyy')||' - '||to_char(date_end,'dd/MON/yyyy')||chr(10)  leavedur
                 FROM per_absence_attendances_v paa     
          where  paa.person_id=cperson_id
          and   cstartdate <= paa.date_end
          and   cendddate  >= paa.date_start 
        and    ABSENCE_ATTENDANCE_TYPE_ID != 66  
          union
--          select '('||to_char(to_date(LEAVE.LEAVE_START_DATE,'yyyy-MM-dd'),'dd/MON/yyyy')||' - '||to_char(to_date(LEAVE.LEAVE_END_DATE,'yyyy-MM-dd'),'dd/MON/yyyy')||')' 
          select to_char(to_date(LEAVE.LEAVE_START_DATE,'yyyy-MM-dd'),'dd/MON/yyyy')||' - '||to_char(to_date(LEAVE.LEAVE_END_DATE,'yyyy-MM-dd'),'dd/MON/yyyy')||chr(10)
          from
            (
            Select A.*
              from apps.PQH_SS_APPROVAL_HISTORY A 
            where 1=1
            and A.transaction_ITEM_TYPE ='HRSSA'
          and A.transaction_effective_date between cstartdate and cendddate
            ) HISTORY , 
            (
            SELECT 
            per.Employee_Number,
            per.full_name,
            a.transaction_history_id,
            c.leave_type Leave_Type
                  ,to_char(a.date_start) Leave_Start_Date
                  ,to_char(b.date_end) Leave_End_Date
                  ,to_char(d.duration) Leave_Duration     
            FROM   apps.per_all_people_f per,
            (SELECT c.value date_start
                          ,a.transaction_history_id
                          ,a.item_key
                          ,a.selected_person_id
                    FROM   apps.PQH_SS_TRANSACTION_HISTORY       a
                          ,apps.PQH_SS_STEP_HISTORY  b
                          ,apps.PQH_SS_VALUE_HISTORY c
                    WHERE  1=1
                    and a.selected_person_id = cperson_id
                    AND    a.transaction_history_id = b.transaction_history_id              
                    AND    b.step_history_id = c.step_history_id
                    AND    NAME = 'P_DATE_START'     
                    ) a ,
            (SELECT c.value EFFECTIVE_DATE
                          ,a.transaction_history_id
                          ,a.item_key
                          ,a.selected_person_id
                    FROM   apps.PQH_SS_TRANSACTION_HISTORY       a
                          ,apps.PQH_SS_STEP_HISTORY  b
                          ,apps.PQH_SS_VALUE_HISTORY c
                    WHERE  1=1
                    and a.selected_person_id = cperson_id
                    AND    a.transaction_history_id = b.transaction_history_id              
                    AND    b.step_history_id = c.step_history_id
                    AND    NAME = 'P_EFFECTIVE_DATE'
                    ) aa               
                    ,
                   (SELECT c.value date_end
                          ,a.transaction_history_id
                          ,a.item_key
                    FROM   apps.PQH_SS_TRANSACTION_HISTORY       a
                          ,apps.PQH_SS_STEP_HISTORY  b
                          ,apps.PQH_SS_VALUE_HISTORY c
                    WHERE  1=1
                    and a.selected_person_id = cperson_id
                    AND    a.transaction_history_id = b.transaction_history_id              
                    AND    b.step_history_id = c.step_history_id
                    AND    NAME = 'P_DATE_END') b       
                    ,
                    (SELECT d.NAME leave_type,d.absence_attendance_type_id
                          ,a.transaction_history_id
                          ,a.item_key
                    FROM   apps.PQH_SS_TRANSACTION_HISTORY       a
                          ,apps.PQH_SS_STEP_HISTORY  b
                          ,apps.PQH_SS_VALUE_HISTORY c
                          ,apps.per_absence_attendance_types d
                    WHERE  1=1
                    and a.selected_person_id = cperson_id
                    AND    a.transaction_history_id = b.transaction_history_id              
                    AND    b.step_history_id = c.step_history_id
                    AND    c.NAME = 'P_ABSENCE_ATTENDANCE_TYPE_ID'
                    AND    d.absence_attendance_type_id = c.value) c
                     ,
                    (SELECT c.value duration
                          ,a.transaction_history_id
                          ,a.item_key
                    FROM   apps.PQH_SS_TRANSACTION_HISTORY       a
                          ,apps.PQH_SS_STEP_HISTORY  b
                          ,apps.PQH_SS_VALUE_HISTORY c              
                    WHERE  1=1
                    and a.selected_person_id = cperson_id
                    AND    a.transaction_history_id = b.transaction_history_id              
                    AND    b.step_history_id = c.step_history_id
                    AND    NAME = 'P_ABSENCE_DAYS') d
            WHERE  1=1
            and    a.transaction_history_id = b.transaction_history_id
            AND    a.transaction_history_id = c.transaction_history_id
            AND    a.transaction_history_id = d.transaction_history_id
            AND    a.transaction_history_id = aa.transaction_history_id
            and a.selected_person_id = per.person_id
            and sysdate between per.effective_start_date and  per.effective_end_date
            Union all
            SELECT 
            per.Employee_Number,
            per.full_name,
            a.transaction_history_id,
            t.name Leave_Name,
            to_char(b.information1) Leave_Start_Date,
            to_char(b.information2) Leave_End_Date,
            to_char(b.information8) Leave_Duration
            FROM   apps.per_all_people_f per,
            apps.PQH_SS_TRANSACTION_HISTORY       a
            ,apps.PQH_SS_STEP_HISTORY  b
            , apps.per_absence_attendance_types t
            WHERE  1=1
            and a.selected_person_id = cperson_id
            AND    a.transaction_history_id = b.transaction_history_id
            AND    b.information5   = t.absence_attendance_type_id
            AND    b.information30 = 'ATT'
            AND    a.item_Type = 'HRSSA'
            and a.selected_person_id = per.person_id
            and sysdate between per.effective_start_date and  per.effective_end_date
            ) LEAVE
            where 1=1
            and LEAVE.transaction_history_id = HISTORY.transaction_history_id
            and history.transaction_item_key is not null
          and  to_char(cstartdate,'yyyy-MM-dd')  <= LEAVE.LEAVE_END_DATE
            and  to_char(cendddate,'yyyy-MM-dd')   >= LEAVE.LEAVE_START_DATE
            and initcap(HISTORY.ACTION) = 'Approved';
               
                                                      
            
         
          concated_lv varchar2(4000) ;
 begin
 --open assign (pperson_id  ,pstartdate  ,pendddate );
 --loop 
   FOR REC IN  lv (pperson_id  ,pstartdate  ,pendddate )
   loop
   
   concated_lv :=concated_lv||' '||REC.leavedur;
   end loop ;
   
 return concated_lv;
 end ;
 
 
   function GET_LEAVE_DURATION_NW(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2
 as
 cursor lv (cperson_id  in number ,cstartdate  in Date ,cendddate  in Date)
 is    SELECT'('|| to_char(date_start,'dd-MON-yyyy')||'/'||to_char(date_end,'dd-MON-yyyy')||')'  leavedur    
                 FROM per_absence_attendances_v paa     
          where  paa.person_id=cperson_id 
           and    ABSENCE_ATTENDANCE_TYPE_ID != 66     
            AND (TRUNC (cstartdate) BETWEEN TRUNC (paa.date_start )AND TRUNC (paa.date_end)                                             
                 OR TRUNC (cendddate) BETWEEN TRUNC ( paa.date_start)AND TRUNC ( paa.date_end)                                                      
                  OR TRUNC (paa.date_start) BETWEEN TRUNC (cstartdate)AND TRUNC (cendddate)                                                       
                    OR TRUNC (paa.date_end) BETWEEN TRUNC (cstartdate)AND TRUNC (cendddate) 
                                                             
                                                           
                                                          )order by date_start ;     
                                                                      
                                                                         
             
         
          concated_lv varchar2(4000) ;
 begin
 --open assign (pperson_id  ,pstartdate  ,pendddate );
 --loop 
   FOR REC IN  lv (pperson_id  ,pstartdate  ,pendddate )
   loop
   
   concated_lv :=concated_lv||' '||REC.leavedur;
   end loop ;
   
 return concated_lv;
 end ;
     function GET_LEAVE_STATUS(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2
     is 
        LeaveCount number ;
     AssignmentCount Number;
     LeaveStatus varchar2(200);
     AssignmentStatus varchar2(200);
     status varchar2(200);
  
     begin 
        SELECT COUNT (1) into LeaveCount        
                 FROM per_absence_attendances paa         
                WHERE paa.PERSON_ID = pperson_id   
                AND ABSENCE_ATTENDANCE_TYPE_ID != 66        
                      AND (TRUNC (pstartdate) BETWEEN TRUNC (         
                                                                NVL (         
                                                                   paa.date_start,         
                                                                   paa.date_projected_start))         
                                                         AND TRUNC (         
                                                                NVL (         
                                                                   paa.date_end,         
                                                                   paa.date_projected_end))         
                           OR TRUNC (pendddate) BETWEEN TRUNC (         
                                                                 NVL (         
                                                                    paa.date_start,         
                                                                    paa.date_projected_start))         
                                                          AND TRUNC (         
                                                                 NVL (         
                                                                    paa.date_end,         
                                                                    paa.date_projected_end))); 
                   if(LeaveCount=1)
                   then 
                   LeaveStatus:='Y'  ;
                   elsif (LeaveCount=0)
                   then
                   LeaveStatus:='N'  ;
                   end if ; 
                   
                   
                   SELECT COUNT (1)   into AssignmentCount        
                 FROM AS_ASSIGNMENT_EMPS ASE          
                WHERE ASE.PERSON_ID = pperson_id          
                      AND (TRUNC (pstartdate) BETWEEN TRUNC (          
                                                                ASE.EMP_START_DATE)          
                                                         AND TRUNC (          
                                                                ASE.EMP_END_DATE)          
                           OR TRUNC (pendddate) BETWEEN TRUNC (          
                                                                 ASE.EMP_START_DATE)          
                                                          AND TRUNC (          
                                                                 ASE.EMP_END_DATE)          
                           OR TRUNC (ASE.EMP_START_DATE) BETWEEN TRUNC (          
                                                                    pstartdate)          
                                                             AND TRUNC (          
                                                                    pendddate)          
                           OR TRUNC (ASE.EMP_END_DATE) BETWEEN TRUNC (          
                                                                  pstartdate)          
                                                           AND TRUNC (          
                                                                  pendddate)); 
                   if(AssignmentCount=1)
                   then 
                   AssignmentStatus:='Y'  ;
                   elsif (AssignmentCount=0)
                   then
                   AssignmentStatus:='N'  ;
                   end if ; 
                   
                   if(AssignmentStatus='Y' and LeaveStatus='Y')
                   then
                   Status:='AL';
                   elsif(AssignmentStatus='N' and LeaveStatus='Y')
                   then Status:='L';
                   elsif (AssignmentStatus='Y' and LeaveStatus='N')
                     then Status:='A';    
                      elsif (AssignmentStatus='N' and LeaveStatus='N')
                     then Status:='N';
                     end if;       
            
     
     return Status;
     end ;
       function GET_LEAVE_STATUS_COLOR(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2
     is 
        LeaveCount number ;
     AssignmentCount Number;
     LeaveStatus varchar2(200);
     AssignmentStatus varchar2(200);
     status varchar2(200);
  
     begin 
     if(pperson_id is null)
     then status :=' ';
     elsif(pperson_id is not  null) 
     then 
     
        SELECT COUNT (1) into LeaveCount        
                 FROM per_absence_attendances paa 
                  where ABSENCE_ATTENDANCE_TYPE_ID != 66         
               and paa.PERSON_ID = pperson_id         
                      AND (TRUNC (pstartdate) BETWEEN TRUNC (         
                                                                NVL (         
                                                                   paa.date_start,         
                                                                   paa.date_projected_start))         
                                                         AND TRUNC (         
                                                                NVL (         
                                                                   paa.date_end,         
                                                                   paa.date_projected_end))         
                           OR TRUNC (pendddate) BETWEEN TRUNC (         
                                                                 NVL (         
                                                                    paa.date_start,         
                                                                    paa.date_projected_start))         
                                                          AND TRUNC (         
                                                                 NVL (         
                                                                    paa.date_end,         
                                                                    paa.date_projected_end))); 
                   if(LeaveCount=1)
                   then 
                   LeaveStatus:='Y'  ;
                   elsif (LeaveCount=0)
                   then
                   LeaveStatus:='N'  ;
                   end if ; 
                   
                   
                   SELECT COUNT (1)   into AssignmentCount        
                 FROM AS_ASSIGNMENT_EMPS ASE          
                WHERE ASE.PERSON_ID = pperson_id          
                      AND (TRUNC (pstartdate) BETWEEN TRUNC (          
                                                                ASE.EMP_START_DATE)          
                                                         AND TRUNC (          
                                                                ASE.EMP_END_DATE)          
                           OR TRUNC (pendddate) BETWEEN TRUNC (          
                                                                 ASE.EMP_START_DATE)          
                                                          AND TRUNC (          
                                                                 ASE.EMP_END_DATE)          
                           OR TRUNC (ASE.EMP_START_DATE) BETWEEN TRUNC (          
                                                                    pstartdate)          
                                                             AND TRUNC (          
                                                                    pendddate)          
                           OR TRUNC (ASE.EMP_END_DATE) BETWEEN TRUNC (          
                                                                  pstartdate)          
                                                           AND TRUNC (          
                                                                  pendddate)); 
                   if(AssignmentCount=1)
                   then 
                   AssignmentStatus:='Y'  ;
                   elsif (AssignmentCount=0)
                   then
                   AssignmentStatus:='N'  ;
                   end if ; 
                   
                   if(AssignmentStatus='Y' and LeaveStatus='Y')
                   then
                   Status:='blue';
                   elsif(AssignmentStatus='N' and LeaveStatus='Y')
                   then Status:='red';
                   elsif (AssignmentStatus='Y' and LeaveStatus='N')
                     then Status:='#ffb134';    
                      elsif (AssignmentStatus='N' and LeaveStatus='N')
                     then Status:='black';
                     end if;       
            
     end if;
     return Status;
     end ;
     
     
        function GET_LEAVE_STATUS_COLOR_NW(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2
     is 
        LeaveCount number ;
     AssignmentCount Number;
     LeaveStatus varchar2(200);
     AssignmentStatus varchar2(200);
     status varchar2(200);
  
     begin 
        SELECT COUNT (1) into LeaveCount        
                 FROM per_absence_attendances paa 
                   
               where  paa.PERSON_ID = pperson_id   
                  and    ABSENCE_ATTENDANCE_TYPE_ID != 66       
                      AND (TRUNC (pstartdate) BETWEEN TRUNC (         
                                                                NVL (         
                                                                   paa.date_start,         
                                                                   paa.date_projected_start))         
                                                         AND TRUNC (         
                                                                NVL (         
                                                                   paa.date_end,         
                                                                   paa.date_projected_end))         
                           OR TRUNC (pendddate) BETWEEN TRUNC (         
                                                                 NVL (         
                                                                    paa.date_start,         
                                                                    paa.date_projected_start))         
                                                          AND TRUNC (         
                                                                 NVL (         
                                                                    paa.date_end,         
                                                                    paa.date_projected_end))); 
                   if(LeaveCount=1)
                   then 
                   LeaveStatus:='Y'  ;
                   elsif (LeaveCount=0)
                   then
                   LeaveStatus:='N'  ;
                   end if ; 
                   
                   
                   SELECT COUNT (1)   into AssignmentCount        
                 FROM AS_ASSIGNMENT_EMPS ASE          
                WHERE ASE.PERSON_ID = pperson_id          
                      AND (TRUNC (pstartdate) BETWEEN TRUNC (          
                                                                ASE.EMP_START_DATE)          
                                                         AND TRUNC (          
                                                                ASE.EMP_END_DATE)          
                           OR TRUNC (pendddate) BETWEEN TRUNC (          
                                                                 ASE.EMP_START_DATE)          
                                                          AND TRUNC (          
                                                                 ASE.EMP_END_DATE)          
                           OR TRUNC (ASE.EMP_START_DATE) BETWEEN TRUNC (          
                                                                    pstartdate)          
                                                             AND TRUNC (          
                                                                    pendddate)          
                           OR TRUNC (ASE.EMP_END_DATE) BETWEEN TRUNC (          
                                                                  pstartdate)          
                                                           AND TRUNC (          
                                                                  pendddate)); 
                   if(AssignmentCount=1)
                   then 
                   AssignmentStatus:='Y'  ;
                   elsif (AssignmentCount=0)
                   then
                   AssignmentStatus:='N'  ;
                   end if ; 
                   
                   if(AssignmentStatus='Y' and LeaveStatus='Y')
                   then
                   Status:='red';
                   elsif(AssignmentStatus='N' and LeaveStatus='Y')
                   then Status:='#ffb134';
                   elsif (AssignmentStatus='Y' and LeaveStatus='N')
                     then Status:='#81BB5F';    
                      elsif (AssignmentStatus='N' and LeaveStatus='N')
                     then Status:='#74307e';
                     end if;       
            
     
     return Status;
     end ;
     
      function GET_FIRSTLEAVE_STATUS_COLOR_NA(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2
     is 
        LeaveCount number ;
     AssignmentCount Number;
     LeaveStatus varchar2(200);
     AssignmentStatus varchar2(200);
     status varchar2(200);
  
     begin 
    
 
 select count (person_id) into LeaveCount from 
 (
 SELECT paa.person_id
        FROM per_absence_attendances_v paa
       WHERE     paa.person_id = pperson_id
             AND pstartdate <= paa.date_end
             AND pendddate>= paa.date_start
      UNION
      SELECT  p.person_id
        FROM hr_api_transactions hat,
             per_all_people_f p,
             "HR"."AME_APPROVALS_HISTORY" atph
       WHERE     hat.TRANSACTION_REF_TABLE = 'PER_ABSENCE_ATTENDANCES'
             AND hat.TRANSACTION_GROUP = 'ABSENCE_MGMT'
             AND hat.TRANSACTION_IDENTIFIER = 'ABSENCES'
             AND hat.TRANSACTION_REF_ID IS NOT NULL
             AND (NVL (start_date, SYSDATE) BETWEEN p.effective_start_date
                                                AND p.effective_end_date)
             AND hat.SELECTED_PERSON_ID = p.person_id
             AND hat.status NOT IN ('D', 'E', 'AC')
             AND NOT (    hr_absutil_ss.getabsencetype (hat.transaction_id,
                                                        NULL)
                             IS NULL
                      AND hat.status = 'W')
             AND atph.application_id = -91
             AND hat.transaction_id = atph.transaction_id
             AND atph.APPROVAL_STATUS = 'APPROVE'
             AND atph.OCCURRENCE = 1
             AND p.person_id = pperson_id
               and pstartdate 
                between    hr_absutil_ss.getStartDate (hat.transaction_id, NULL)
               and   hr_absutil_ss.getEndDate (hat.transaction_id, NULL)
             
             );
             
             
             
          
                   if(LeaveCount=1)
                   then 
                   LeaveStatus:='Y'  ;
                   elsif (LeaveCount=0)
                   then
                   LeaveStatus:='N'  ;
                   end if ; 
                   
                   
                   SELECT COUNT (1)   into AssignmentCount        
                 FROM AS_ASSIGNMENT_EMPS ASE          
                WHERE ASE.PERSON_ID = pperson_id          
                      AND (TRUNC (pstartdate) BETWEEN TRUNC (          
                                                                ASE.EMP_START_DATE)          
                                                         AND TRUNC (          
                                                                ASE.EMP_END_DATE)          
                           OR TRUNC (pendddate) BETWEEN TRUNC (          
                                                                 ASE.EMP_START_DATE)          
                                                          AND TRUNC (          
                                                                 ASE.EMP_END_DATE)          
                           OR TRUNC (ASE.EMP_START_DATE) BETWEEN TRUNC (          
                                                                    pstartdate)          
                                                             AND TRUNC (          
                                                                    pendddate)          
                           OR TRUNC (ASE.EMP_END_DATE) BETWEEN TRUNC (          
                                                                  pstartdate)          
                                                           AND TRUNC (          
                                                                  pendddate)); 
                   if(AssignmentCount=1)
                   then 
                   AssignmentStatus:='Y'  ;
                   elsif (AssignmentCount=0)
                   then
                   AssignmentStatus:='N'  ;
                   end if ; 
                   
                    if(AssignmentStatus='Y' and LeaveStatus='Y')
                   then
                   Status:='blue';
                   elsif(AssignmentStatus='N' and LeaveStatus='Y')
                   then Status:='red';
                   elsif (AssignmentStatus='Y' and LeaveStatus='N')
                     then Status:='#ffb134';    
                      elsif (AssignmentStatus='N' and LeaveStatus='N')
                     then Status:='black';
                     end if;        
            
     
     return Status;
     end ;
          function GET_LEAVE_STATUS2(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2
     is 
        LeaveCount number ;
    
     LeaveStatus varchar2(200);
     AssignmentStatus varchar2(200);
  
  
     begin 
        SELECT COUNT (1) into LeaveCount        
                 FROM per_absence_attendances paa         
                WHERE paa.PERSON_ID = pperson_id
                 AND ABSENCE_ATTENDANCE_TYPE_ID != 66           
                      AND (TRUNC (pstartdate) BETWEEN TRUNC (         
                                                                NVL (         
                                                                   paa.date_start,         
                                                                   paa.date_projected_start))         
                                                         AND TRUNC (         
                                                                NVL (         
                                                                   paa.date_end,         
                                                                   paa.date_projected_end))         
                           OR TRUNC (pendddate) BETWEEN TRUNC (         
                                                                 NVL (         
                                                                    paa.date_start,         
                                                                    paa.date_projected_start))         
                                                          AND TRUNC (         
                                                                 NVL (         
                                                                    paa.date_end,         
                                                                    paa.date_projected_end))); 
                   if(LeaveCount=1)
                   then 
                   LeaveStatus:='Y'  ;
                   elsif (LeaveCount=0)
                   then
                   LeaveStatus:='N'  ;
                   end if ; 
                   
                   
                     
            
     
     return LeaveStatus;
     end ;
     function EXIST_IN_LEAVE(pperson_id number ,pstartdate Date ,pendddate Date) return varchar2

 is 
 

        count_leave Number ; 
       -- count_leave_inner Number ;
        result varchar2(200) :='N';
     begin 
     
     
       SELECT count (1)   into  count_leave
                 FROM per_absence_attendances_v paa     
          where  paa.person_id=pperson_id     
            AND (TRUNC (pstartdate) BETWEEN TRUNC (paa.date_start )AND TRUNC (paa.date_end)                                             
                 OR TRUNC (pstartdate) BETWEEN TRUNC ( paa.date_start)AND TRUNC ( paa.date_end)                                                      
                  OR TRUNC (paa.date_start) BETWEEN TRUNC (pstartdate)AND TRUNC (pendddate)                                                       
                    OR TRUNC (paa.date_end) BETWEEN TRUNC (pstartdate)AND TRUNC (pendddate) ) ;                                          
                                                           
                     if(count_leave>0)
                     then 
                     result:='Y';
                     end if ;
                     return result ;                                      
--     SELECT COUNT (1)  into count_leave         
--                 FROM per_absence_attendances paa         
--                WHERE paa.PERSON_ID =pperson_id        
--                      AND (TRUNC (pstartdate) BETWEEN TRUNC (         
--                                                                NVL (         
--                                                                   paa.date_start,         
--                                                                   paa.date_projected_start))         
--                                                         AND TRUNC (         
--                                                                NVL (         
--                                                                   paa.date_end,         
--                                                                   paa.date_projected_end))         
--                           OR TRUNC (pendddate) BETWEEN TRUNC (         
--                                                                 NVL (         
--                                                                    paa.date_start,         
--                                                                    paa.date_projected_start))         
--                                                          AND TRUNC (         
--                                                                 NVL (         
--                                                                    paa.date_end,         
--                                                                    paa.date_projected_end)));
--          if(count_leave=1)  
--          then                                                        
--          result:='Y';
--          else if(count_leave=0)
--          then 
--          --- here if the leave period exist inside the scheduler duartion 
--          --for example schedule from 1 june to 30 june /and leave between 2 and 16 june
--          SELECT COUNT (1)  into count_leave_inner         
--                 FROM per_absence_attendances paa         
--                WHERE paa.PERSON_ID =pperson_id        
--                      AND (TRUNC (nvl(paa.date_start,paa.date_projected_start)) BETWEEN TRUNC ( pstartdate ) 
--                      
--                                                                          
--                                                         AND TRUNC (pendddate)  
--                                                               
--                                                                     
--                           OR TRUNC (nvl(paa.date_end,paa.date_projected_end)) BETWEEN TRUNC ( pstartdate ) 
--                      AND TRUNC (pendddate)  );
--                   if(count_leave_inner=1)
--                   then 
--                   result:='Y';                                                       
--                     end if;                                      
--        
--          end if ;
--          end if ;
--          return result;
 
                                                      
     
     end ;
       function getOnSchedul_Leave_employee(pschedulerid Number) return Number 
       is  
       count_emp Number ;
       begin 
      
       
     select count( distinct employee_name) into count_emp
from 

(SELECT      
       papf.full_name employee_name ,    
            AS_SCHDLR_PKG.GET_LEAVE_STATUS2(PAPF.PERSON_ID  ,SCHEDULAR_START_DATE ,SCHEDULAR_END_DATE) LEAVE_STATUS    
  FROM AS_SCHEDULER_STAFF AsSchedulerStaffEO,     
       PER_ALL_PEOPLE_F PAPF    
         
 WHERE AsSchedulerStaffEO.PERSON_ID = PAPF.PERSON_ID     
       AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE     
                       AND PAPF.EFFECTIVE_END_DATE     
    
        and  AsSchedulerStaffEO.SCHEDULER_ID=pschedulerid
        and PARENT_SCHEDULER_STAFF_ID is   null)
        where LEAVE_STATUS ='Y';
        return count_emp;
  end ;
    function getValidationDetails(pschedulerid Number ,pdate Date) return varchar2
    is 
    validation_text varchar2(32000);
    cursor  val(cschedulerid in number ,cdate in date) 
    is SELECT  
    SCHEDULAR_SHIFT_ID ,scheduler_id  ,SHIFT_NAME ,sum(totalinshift)total ,WORK_GROUP_NAME ,EMPLOYEES_COUNT ,EMPLOYEES_COUNT_MAX 
                from    
  (SELECT  
        SCHEDULAR_SHIFT_ID,  SCHDLR_SHFT.SCHEDULER_ID,   count(SHIFT.SHIFT_NAME)totalinshift,SHIFT.SHIFT_NAME ,WRKGRP.WORK_GROUP_NAME, 
                 SHFT_WRKGRP.EMPLOYEES_COUNT, 
                SHFT_WRKGRP.EMPLOYEES_COUNT_MAX 
           FROM AS_SCHEDULER_SHIFTS SCHDLR_SHFT, 
                AS_SHIFT_WORK_GROUPS SHFT_WRKGRP, 
                AS_SHIFTS SHIFT, 
                AS_WORK_GROUPS_HDR WRKGRP , 
                AS_SCHEDULER_STAFF ss 
          WHERE     SCHDLR_SHFT.SHIFT_ID = SHFT_WRKGRP.SHIFT_ID 
                AND SCHDLR_SHFT.WORK_GROUP_ID = SHFT_WRKGRP.WORK_GROUP_ID 
                AND SCHDLR_SHFT.SHIFT_ID = SHIFT.SHIFT_ID 
                AND SCHDLR_SHFT.WORK_GROUP_ID = WRKGRP.WORK_GROUP_ID 
                and SCHDLR_SHFT.scheduler_id=ss.scheduler_id 
                and SHFT_WRKGRP.work_group_id=ss.work_group_id 
                and SCHDLR_SHFT.shift_id=SS.SHIFT_ID 
                AND SCHDLR_SHFT.SCHEDULER_ID = cschedulerid 
                and ss.schedular_start_date=cdate 
                and parent_scheduler_staff_id is null 
                group by  SCHEDULAR_SHIFT_ID,SCHDLR_SHFT.SCHEDULER_ID, shift_name ,WRKGRP.WORK_GROUP_NAME , SHFT_WRKGRP.EMPLOYEES_COUNT, 
                SHFT_WRKGRP.EMPLOYEES_COUNT_MAX 
                 
                 
                union SELECT SCHEDULAR_SHIFT_ID, SCHDLR_SHFT.SCHEDULER_ID,0 , 
                 
                SHIFT.SHIFT_NAME, 
                 
                WRKGRP.WORK_GROUP_NAME, 
                SHFT_WRKGRP.EMPLOYEES_COUNT, 
                SHFT_WRKGRP.EMPLOYEES_COUNT_MAX 
           FROM AS_SCHEDULER_SHIFTS SCHDLR_SHFT, 
                AS_SHIFT_WORK_GROUPS SHFT_WRKGRP, 
                AS_SHIFTS SHIFT, 
                AS_WORK_GROUPS_HDR WRKGRP 
          WHERE     SCHDLR_SHFT.SHIFT_ID = SHFT_WRKGRP.SHIFT_ID 
                AND SCHDLR_SHFT.WORK_GROUP_ID = SHFT_WRKGRP.WORK_GROUP_ID 
                AND SCHDLR_SHFT.SHIFT_ID = SHIFT.SHIFT_ID 
                AND SCHDLR_SHFT.WORK_GROUP_ID = WRKGRP.WORK_GROUP_ID 
                AND SCHDLR_SHFT.SCHEDULER_ID = cschedulerid) 
                  
               group by  SCHEDULAR_SHIFT_ID ,scheduler_id  ,SHIFT_NAME  ,WORK_GROUP_NAME ,EMPLOYEES_COUNT ,EMPLOYEES_COUNT_MAX ;
                 
        
    begin 
   validation_text:='<h5 style="text-decoration: underline;color:black;font-weight:bold;font-size:12px;">'||pdate|| '</h5><p style="margin-top:-15px;color:black;font-size:12px;">';
     FOR v IN  val (pschedulerid ,pdate)
   loop
   if(v.total<v.employees_count)
   then
  validation_text :=validation_text||'No. of employees in '||' '||v.SHIFT_NAME ||'' ||v.WORK_GROUP_NAME||' is '||v.total||' however is required '||v.employees_count||' employees'||'<br/>';
  end if ;
   end loop ;
    validation_text:= validation_text||'</p>';
   return validation_text;
   
    end ;
    
     function getFirstdayofweekschedule(pdate Date) return date 
     is
   beginofweek Date;
     begin
       SELECT to_date(sysdate - to_char(sysdate, 'D') + 1,'dd/mon/yy')  into beginofweek
FROM dual ;
return beginofweek;

     end ;
       procedure Insertlookup 
       is
       ln_rowid1 varchar(500);
       
       begin
        
--         .DELETE_ROW(
--  X_LOOKUP_TYPE => 'XXMBC_AA_SCHDLR_TRANS_REASONS',
--  X_SECURITY_GROUP_ID =>0,
--  X_VIEW_APPLICATION_ID=>0,
--  X_LOOKUP_CODE => 'LEAVE'
--);
         APPS.FND_LOOKUP_VALUES_PKG .insert_row
 (x_rowid                   => ln_rowid1,
 x_lookup_type              => 'XXMBC_AA_SCHDLR_TRANS_REASONS', 
 x_security_group_id        => 0,
 x_view_application_id      => 0,
 x_lookup_code              => 'LEAVE',
 x_tag                      => NULL,
 x_attribute_category       => NULL,
 x_attribute1               => NULL,
 x_attribute2               => NULL,
 x_attribute3               => NULL,
 x_attribute4               => NULL,
 x_enabled_flag             => 'Y',
 x_start_date_active        => TO_DATE ('10-SEP-2017','DD-MON-YYYY'),
 x_end_date_active          => NULL,
 x_territory_code           => NULL,
 x_attribute5               => NULL,
 x_attribute6               => NULL,
 x_attribute7               => NULL,
 x_attribute8               => NULL,
 x_attribute9               => NULL,
 x_attribute10              => NULL,
 x_attribute11              => NULL,
 x_attribute12              => NULL,
 x_attribute13              => NULL,
 x_attribute14              => NULL,
 x_attribute15              => NULL,
 x_meaning                  => 'Leave',--Lookup Meaning
 x_description              => NULL,
 x_creation_date            => SYSDATE,
 x_created_by               => -1,
 x_last_update_date         => SYSDATE,
 x_last_updated_by          => -1,
 x_last_update_login        => -1
 );

--      
      end;
      
            procedure WorkGroupValidation 
            is 
            total_Pattern_days Number ;
            StartDate Date :=to_date('4/Feb/2018');
            begin 
            --loop for 7 days starts with sysdate 
            --select distinct employee pattern where scheduelr_staff_date=:date  and employee not exist in scheduler_staff
            ---for every pattern if day is on 
            ---select * from employee where not in todayschedule and patternid=patternid
            --insert in workgroupNotification tables
            ----end loop 7 days 
            --insert into worklist for admin users 
            
                SELECT SUM (NO_OF_DAYS)  into total_Pattern_days
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = 8
                             --PPattern_id
                          ORDER BY ORDER_SEQ ASC;
            end ;
            
      
        function CHECK_PATTERN_ON_OFF( PPattern_id Number , pdate Date )return varchar2
      is 

     StartDate Date :=to_date('4/Feb/2018');
     Num_Of_days Number ;
     CurrentPattern_Num Number:=0 ;
     On_OffFlag varchar2(200);
     total_Pattern_days Number;
   
      begin 
       
      
       SELECT SUM (NO_OF_DAYS)  into total_Pattern_days
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = PPattern_id
                          ORDER BY ORDER_SEQ ASC;
      select trunc(pdate+1)- trunc(StartDate) into Num_Of_days  from dual ;
      ---Check if Num_of_days >
      if(Num_Of_days>total_Pattern_days)
      then 
         
      if(REMAINDER(Num_Of_days, total_Pattern_days)=0)
      then 
      Num_of_days:=total_Pattern_days;
      --total_Pattern_days;
      else if(REMAINDER(Num_Of_days, total_Pattern_days)<0)
      then 
      Num_of_days:=REMAINDER(Num_Of_days, total_Pattern_days)+total_Pattern_days;
     else 
      Num_of_days:=REMAINDER(Num_Of_days, total_Pattern_days);
      end if ;
      
      end if ;
      
      end if ;

         FOR PTRN_REC IN ( SELECT ON_OFF_FLAG, NO_OF_DAYS 
                              FROM AS_PATTERN_LINES
                             WHERE PATTERN_ID = PPattern_id
                          ORDER BY ORDER_SEQ ASC)
         LOOP
         
         On_OffFlag:=PTRN_REC.ON_OFF_FLAG;
          CurrentPattern_Num:= CurrentPattern_Num+PTRN_REC.NO_OF_DAYS;
            EXIT WHEN CurrentPattern_Num>=Num_Of_days;
            

--                             
                            END LOOP;
--                         ;
 
      return On_OffFlag;
     
      end ;
      
       procedure WORKGROUP_VALIDATION 
       is
 sdate date :=trunc(sysdate);
 versionid Number ;
 wkgp_notification_id Number ;
 employee_notification_id Number;
 cursor patterns (cdate Date)
 is 
  SELECT  distinct FK_PATTERN_ID       
 from
    AS_WORK_GROUPS_LINES wgl,          
    PER_ALL_PEOPLE_F PAPF,          
    PER_ALL_ASSIGNMENTS_F PAAF,          
    PAY_PEOPLE_GROUPS PPG,          
    PER_JOBS PJ,          
    PER_GRADES PG,          
    HR_ALL_POSITIONS_F HAPF          
WHERE          
  wgl.PERSON_ID = PAPF.PERSON_ID 
   AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE 
   AND PAPF.PERSON_ID = PAAF.PERSON_ID 
   AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE AND PAAF.EFFECTIVE_END_DATE 
   AND PAAF.PEOPLE_GROUP_ID = PPG.PEOPLE_GROUP_ID AND PAAF.JOB_ID = PJ.JOB_ID(+) 
   AND PAAF.GRADE_ID = PG.GRADE_ID(+) AND PAAF.POSITION_ID = HAPF.POSITION_ID(+)
    AND SYSDATE BETWEEN HAPF.EFFECTIVE_START_DATE(+) AND HAPF.EFFECTIVE_END_DATE(+) AND PAAF.PRIMARY_FLAG = 'Y'  
 --and work_group_id=13
 and  wgl.person_id not in (select person_id from  as_schedulers s,as_scheduler_staff ss where  s.scheduler_id=ss.scheduler_id and S.SCHEDULAR_STATUS='PUBLISHED' and ss.schedular_start_date=trunc(cdate));
 cursor employees(cpattern_id Number ,cpdate date)
 is
 SELECT   PAPF.PERSON_ID      
 from
    AS_WORK_GROUPS_LINES wgl,          
    PER_ALL_PEOPLE_F PAPF,          
    PER_ALL_ASSIGNMENTS_F PAAF,          
    PAY_PEOPLE_GROUPS PPG,          
    PER_JOBS PJ,          
    PER_GRADES PG,          
    HR_ALL_POSITIONS_F HAPF          
WHERE          
  wgl.PERSON_ID = PAPF.PERSON_ID 
   AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE 
   AND PAPF.PERSON_ID = PAAF.PERSON_ID 
   AND SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE AND PAAF.EFFECTIVE_END_DATE 
   AND PAAF.PEOPLE_GROUP_ID = PPG.PEOPLE_GROUP_ID AND PAAF.JOB_ID = PJ.JOB_ID(+) 
   AND PAAF.GRADE_ID = PG.GRADE_ID(+) AND PAAF.POSITION_ID = HAPF.POSITION_ID(+)
    AND SYSDATE BETWEEN HAPF.EFFECTIVE_START_DATE(+) AND HAPF.EFFECTIVE_END_DATE(+) AND PAAF.PRIMARY_FLAG = 'Y'  
 --and work_group_id=13
 and  wgl.person_id not in (select person_id from  as_schedulers s,as_scheduler_staff ss where  s.scheduler_id=ss.scheduler_id and S.SCHEDULAR_STATUS='PUBLISHED' and ss.schedular_start_date=trunc(cpdate) )
 and fk_pattern_id=cpattern_id
  order by  PERSON_ID Asc ;
  cursor admins
  is select * from as_user_roles
where role_name ='ADMIN' or role_name='SYSADMIN';
begin

select nvl(max(version_id ),0)+1 into versionid from AS_WORKGROUP_NOTIFICATIONS;
for i in 0..6

loop
if(i!=0)
then 
sdate:=sdate+1;
end if ;
select workgroup_notification_seq.nextval into wkgp_notification_id from dual ;
insert into AS_WORKGROUP_NOTIFICATIONS values(wkgp_notification_id,versionid,trunc(sysdate),sdate);
for p in patterns(sdate)
loop
if(as_schdlr_pkg.CHECK_PATTERN_ON_OFF(p.FK_PATTERN_ID ,sdate )='ON')

then 
for m in employees(p.FK_PATTERN_ID,sdate)
loop
select notification_employee_seq.nextval into employee_notification_id from dual ;
insert into AS_WORKGROUP_NOTIF_EMPLOYEES values(employee_notification_id,wkgp_notification_id,m.person_id);
end loop ;

end if ;
end loop ;

end loop;
 for l in admins
 loop
 insert into as_worklist (WORKLIST_ID,WORKLIST_CATEGORY,WORKLIST_ACTION_TYPE,WORKLIST_DATE,WORKLIST_SUBJECT,WORKLIST_STATUS,WORKLIST_PERSON_ID,NOTIFICATION_OF_VALIDATION_ID)values(AS_WORKLIST_SEQ.nextval,'NOTIFICATION','NOTIFY',sysdate,'AA Schedules :'|| trunc(sysdate) ||' WorkGoups Employess Validation','OPEN',l.person_id,versionid);
 end loop;
commit ;

end ;

   PROCEDURE DELETE_SCHDLR_STAFF_FROM_TEMP(IN_SCHEDULER_ID NUMBER, SCHEDULER_STAFF_DATE Date )
   IS
    
      cursor st is 
      select scheduler_staff_id 
      from as_scheduler_staff where SCHEDULER_ID=IN_SCHEDULER_ID and SCHEDULAR_START_DATE=SCHEDULER_STAFF_DATE;
   BEGIN
   
   for i in st
   loop 
   
   
   delete from as_scheduler_staff_bulletins 
    WHERE scheduler_staff_id = i.scheduler_staff_id;
    DELETE FROM AS_SCHEDULER_STAFF
        WHERE scheduler_staff_id = i.scheduler_staff_id;
   end loop 
         COMMIT;
   END DELETE_SCHDLR_STAFF_FROM_TEMP;
   
    FUNCTION SCHEDULER_STAFF_BULLETIN(IN_SCHEDULER_STAFF_ID NUMBER) return varchar2
    is 
    staffbulletin varchar2(2000):='';
    v_bulletins varchar2(2000):='';
      bFirst_row      BOOLEAN := TRUE;
    cursor bulletins is 
    select bulletin_name from  as_scheduler_staff_bulletins  sbt,as_bulletins bt
    where  sbt.bulletin_id=bt.bulletin_id 
    and SCHEDULER_STAFF_ID=IN_SCHEDULER_STAFF_ID;
    begin 
    for b in bulletins 
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    staffbulletin:=staffbulletin||' '||b.bulletin_name;
    else
        staffbulletin:=staffbulletin||'/'||b.bulletin_name;
    end if ;
    end loop ;
    
 --   staffbulletin:=staffbulletin;
   select  decode (nvl(staffbulletin,'N'),'N',staffbulletin,'('||staffbulletin||')') into v_bulletins from dual ;
   return v_bulletins;
    end ;
    
          PROCEDURE LOAD_TEMPLATE_BULLETINS (IN_SCHEDULER_ID NUMBER,IN_SCHEDULER_TITLE varchar2,IN_TEMPLATE_ID NUMBER ,IN_PERSON_ID NUMBER,IN_COLUMN_DATE DATE)
    is 
    --- below curosr to get the template row with bulletins which has order not exist in the selected scheduler
    cursor st is 
    select distinct ORDER_NUM from AS_TEMPLATE_EMPLOYEE  te,AS_TEMPLATE_EMP_BULLETINS tb  where te.FK_TEMPLATE_ID=IN_TEMPLATE_ID
    and  ORDER_NUM not in (select order_id from as_scheduler_sections  where FKSCHEDULER_ID=IN_SCHEDULER_ID) 
     and tb.FK_TEMPLATE_EMP_ID =te.TEMPLATE_EMPLOYEE_ID ;
  
cursor tpbulletins is 
select distinct TEMPLATE_EMPLOYEE_ID ,ORDER_NUM from AS_TEMPLATE_EMPLOYEE  te,AS_TEMPLATE_EMP_BULLETINS tb 
 where te.FK_TEMPLATE_ID=IN_TEMPLATE_ID
 and tb.FK_TEMPLATE_EMP_ID =te.TEMPLATE_EMPLOYEE_ID ;
 vscheduler_staff_id Number :=0;
 vsection_id Number:=0;
 vcount Number :=0 ;
  begin 
    
    for i in st
    loop 
    --  create rows in schedulers with orders missing 
    
  insert into as_scheduler_sections (section_id,fkscheduler_id,section_name,order_id) values(section_seq.nextval,IN_SCHEDULER_ID
  ,IN_SCHEDULER_TITLE,i.order_num);
   
    end loop;
         commit ;
         for ii in tpbulletins
         loop
         vscheduler_staff_id:=0;
          select  count(scheduler_staff_id ) into vcount
           from as_scheduler_sections ssection  ,as_scheduler_staff ss
                 where ssection.FKSCHEDULER_ID=IN_SCHEDULER_ID 
           and ss.scheduler_id=ssection.FKSCHEDULER_ID
           and section_id=FK_SCHEDULER_SECTION_ID
         and ssection.order_id=ii.order_num
            and ss.SCHEDULAR_START_DATE=IN_COLUMN_DATE;
         ---get scheduler_staff_id 
         -- if exist delete bulletins
         

         if (vcount!=0)
         then 
           select scheduler_staff_id  into vscheduler_staff_id
           from as_scheduler_sections ssection  ,as_scheduler_staff ss
                 where ssection.FKSCHEDULER_ID=IN_SCHEDULER_ID 
           and ss.scheduler_id=ssection.FKSCHEDULER_ID
           and section_id=FK_SCHEDULER_SECTION_ID
         and ssection.order_id=ii.order_num
            and ss.SCHEDULAR_START_DATE=IN_COLUMN_DATE;
           delete from as_Scheduler_staff_bulletins where SCHEDULER_STAFF_ID=vscheduler_staff_id;
         --- not exist create scheduler_staff_row
         INSERT_BULLETINS(ii.TEMPLATE_EMPLOYEE_ID ,vscheduler_staff_id);
          elsif (vcount=0)
          then 
          select section_id into vsection_id 
          from as_scheduler_sections  
          where order_id=ii.order_num and FKSCHEDULER_ID=IN_SCHEDULER_ID ;
          vscheduler_staff_id:=AS_SCHEDULER_STAFF_SEQ.nextval;
          insert into as_Scheduler_staff (SCHEDULER_STAFF_ID,SCHEDULER_ID,SCHEDULAR_START_DATE,SCHEDULAR_END_DATE,FK_SCHEDULER_SECTION_ID,CREATED_BY,LAST_UPDATED_BY,LAST_UPDATE_LOGIN,CREATION_DATE,LAST_UPDATE_DATE)
          values(vscheduler_staff_id,IN_SCHEDULER_ID,IN_COLUMN_DATE,IN_COLUMN_DATE,vsection_id ,IN_PERSON_ID,IN_PERSON_ID,IN_PERSON_ID,sysdate,sysdate);  
          commit ;
          INSERT_BULLETINS(ii.TEMPLATE_EMPLOYEE_ID ,vscheduler_staff_id);
          end if ; 
           
        
        
         end loop ;
         

    end ;
    
       PROCEDURE INSERT_BULLETINS (IN_TEMPLATE_EMP_ID NUMBER,IN_SCHEDULER_STAFF_ID NUMBER)
       is 
       cursor c is select * from AS_TEMPLATE_EMP_BULLETINS
       where FK_TEMPLATE_EMP_ID=IN_TEMPLATE_EMP_ID;
       begin 
       for i in c
       loop
       insert into AS_SCHEDULER_STAFF_BULLETINS  (SCHEDULER_STAFF_BULLETIN_ID,SCHEDULER_STAFF_ID,BULLETIN_ID)
       values( AS_SCHDLR_STAFF_BULLETINS_SEQ.nextval, IN_SCHEDULER_STAFF_ID,i.BULLETIN_ID);
       
       end loop ;
       commit ;
       end ;
       
          FUNCTION GETPUBLICSPACEREPLACEMENT(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2)RETURN VARCHAR2
               IS
          v_full_name varchar2(2000);
          BEGIN
          select full_name into v_full_name from as_scheduler_staff st ,PER_ALL_PEOPLE_F REPLACEMENT  where st.person_id=PPERSON_ID and st.FK_REPLACED_BY_ID = REPLACEMENT.PERSON_ID(+) And SYSDATE BETWEEN NVL (REPLACEMENT.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (REPLACEMENT.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY');
          if v_full_name is not null
          THEN
          return v_full_name;
          else 
          return 'NO';
          end if ;
          --'TEST';
          --v_full_name;
          END ;
          
          
          
                 
           FUNCTION PUBLICSPACEREPLACEMENT(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID NUMBER,P_ORG_ID NUMBER)RETURN VARCHAR2 
             IS
       
       v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  full_name  from as_scheduler_staff st ,PER_ALL_PEOPLE_F REPLACEMENT ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk   
           where st.person_id=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
            and st.FK_REPLACED_BY_ID = REPLACEMENT.PERSON_ID(+)
          AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
          AND ( wk.work_group_id = PWORKGROUP_ID OR PWORKGROUP_ID  IS NULL )                                       
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
  AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
 and (st.shift_id=PSHIFT_ID or PSHIFT_ID is null)
             And SYSDATE BETWEEN NVL (REPLACEMENT.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (REPLACEMENT.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY')
          order by st.scheduler_staff_id
          ;
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.full_name;
    else
        v:=v||' '||b.full_name;
    end if ;
    end loop ;
    

   return v ;
   
          END ;
FUNCTION PUBLICSPACEBULLETIN(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID NUMBER,P_ORG_ID NUMBER)RETURN VARCHAR2 
   IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  bulletin_name  from as_scheduler_staff st ,PER_ALL_PEOPLE_F PAPF ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk   , as_scheduler_staff_bulletins  sbt,as_bulletins bt
           where st.person_id=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
  AND        sbt.bulletin_id=bt.bulletin_id 
            and st.PERSON_ID = PAPF.PERSON_ID(+)
           AND sbt.SCHEDULER_STAFF_ID=ST.SCHEDULER_STAFF_ID
   AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
           AND (bt.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
          AND ( wk.work_group_id = PWORKGROUP_ID OR PWORKGROUP_ID  IS NULL )                                       
   AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
 and (st.shift_id=PSHIFT_ID or PSHIFT_ID is null)

             And SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (PAPF.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY')
          order by st.scheduler_staff_id ;
         
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.bulletin_name;
    else
        v:=v||'/'||b.bulletin_name;
    end if ;
    end loop ;
       return v ;
    END ;
FUNCTION PUBLICSPACESHIFT(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID NUMBER,P_ORG_ID NUMBER)RETURN VARCHAR2 
   IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  shift_name  from as_scheduler_staff st ,PER_ALL_PEOPLE_F PAPF ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk   ,as_shifts sh
           where st.person_id=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
 
            and st.PERSON_ID = PAPF.PERSON_ID(+)
           AND sh.SHIFT_ID=ST.SHIFT_ID
   AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
           AND (sh.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
          AND ( wk.work_group_id = PWORKGROUP_ID OR PWORKGROUP_ID  IS NULL )                                       
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
  AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
 -- and st.shift_id in (PSHIFT_ID) or PSHIFT_ID is null
and (st.shift_id=PSHIFT_ID or PSHIFT_ID is null)

             And SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (PAPF.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY')  order by st.scheduler_staff_id;
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.shift_name;
    else
        v:=v||'/'||b.shift_name;
    end if ;
    end loop ;
       return v ;
    END ;
    
    FUNCTION PUBLICSPACENOTES(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID NUMBER,P_ORG_ID NUMBER)RETURN VARCHAR2 
   IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  Notes  from as_scheduler_staff st ,PER_ALL_PEOPLE_F PAPF ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk 
           where st.person_id=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
 
            and st.PERSON_ID = PAPF.PERSON_ID(+)
      
   AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
  
          AND ( wk.work_group_id = PWORKGROUP_ID OR PWORKGROUP_ID  IS NULL )                                       
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
   AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
 and (st.shift_id=PSHIFT_ID or PSHIFT_ID is null)

             And SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (PAPF.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY') order by st.scheduler_staff_id;
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.Notes;
    else
        v:=v||' '||b.Notes;
    end if ;
    end loop ;
       return v ;
    END ;
FUNCTION PUBLICSPACEASSIGNMENTS(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2)RETURN VARCHAR2 
    IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
              select  ASSIGNMENT_NAME from as_assignments assg ,as_assignment_emps assg_emp
where assg.ASSIGNMENT_ID=assg_emp.ASSIGNMENT_ID  and person_id=PPERSON_ID
and to_date(PSTART_DATE,'dd/mm/YYYY') between emp_start_date and emp_end_date ;

        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.ASSIGNMENT_NAME;
    else
        v:=v||'/'||b.ASSIGNMENT_NAME;
    end if ;
    end loop ;
       return v ;
    END ;
FUNCTION PUBLICSPACELEAVES(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2)RETURN VARCHAR2 
    IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
SELECT distinct  paa.date_start,paa.date_end ,absence_type.Name   
     
                 FROM per_absence_attendances_v paa  ,per_all_people_f papf      
                 ,(select  paat.absence_attendance_type_id,paattl.NaMe    
    
from per_absence_attendance_types paat, per_abs_attendance_types_tl paattl    
where paat.BUSINESS_GROUP_ID=81    
 and  (paat.date_end is null  OR  paat.date_end > sysdate)    
 and (paat.date_effective < sysdate)    
 and paat.absence_attendance_type_id = paattl.absence_attendance_type_id    
 and paattl.language = userenv('LANG') and paat.absence_category is not null) absence_type      
          where  papf.person_id=paa.person_id      
          and absence_type.absence_attendance_type_id=paa.absence_attendance_type_id    
             AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE            
                       AND PAPF.EFFECTIVE_END_DATE     
                      and paa.BUSINESS_GROUP_ID=81      
                       and papf.person_id=PPERSON_ID    
            AND (PSTART_DATE BETWEEN TRUNC (          
                                                                NVL (          
                                                                   paa.date_start,          
                                                                   paa.date_projected_start))          
                                                         AND TRUNC (          
                                                                NVL (          
                                                                   paa.date_end,          
                                                                   paa.date_projected_end))          
                          ) ;

        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.Name;
    else
        v:=v||'/'||b.Name;
    end if ;
    end loop ;
       return v ;
    END ;
    
    FUNCTION PUBLICSPACEREPLACEMENTDATA(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 
   IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  shift_name ,Notes ,SCHEDULER_STAFF_BULLETIN(st.scheduler_staff_id) Bulletins  from as_scheduler_staff st ,PER_ALL_PEOPLE_F PAPF ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk   ,as_shifts sh
           where st.FK_REPLACED_BY_ID=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
 
            and st.FK_REPLACED_BY_ID = PAPF.PERSON_ID(+)
           AND sh.SHIFT_ID=ST.SHIFT_ID
   AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
           AND (sh.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
                  AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)                     
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  


             And SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (PAPF.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY')  order by st.scheduler_staff_id;
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.shift_name||'-'||b.Notes||'-'||b.Bulletins;
    else
        v:=v||'/'||b.shift_name||'- '||b.Notes||'-'||b.Bulletins;
    end if ;
    end loop ;
       return v ;
    END ;
    
    FUNCTION PUBLICSPACESHIFT1(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 
   IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  shift_name  from as_scheduler_staff st ,PER_ALL_PEOPLE_F PAPF ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk   ,as_shifts sh
           where st.person_id=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
 
            and st.PERSON_ID = PAPF.PERSON_ID(+)
           AND sh.SHIFT_ID=ST.SHIFT_ID
   AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
           AND (sh.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
          AND ( wk.work_group_id = PWORKGROUP_ID OR PWORKGROUP_ID  IS NULL )                                       
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
  AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  

--and (st.shift_id=PSHIFT_ID or PSHIFT_ID is null)
 AND (REGEXP_LIKE(st.shift_id,PSHIFT_ID) or PSHIFT_ID is null)
             And SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (PAPF.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY')  order by st.scheduler_staff_id;
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.shift_name;
    else
        v:=v||'/'||b.shift_name;
    end if ;
    end loop ;
       return v ;
    END ;
     FUNCTION PUBLICSPACEREPLACEMENT1(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 
             IS
       
       v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  full_name  from as_scheduler_staff st ,PER_ALL_PEOPLE_F REPLACEMENT ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk   
           where st.person_id=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
            and st.FK_REPLACED_BY_ID = REPLACEMENT.PERSON_ID(+)
          AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
          AND ( wk.work_group_id = PWORKGROUP_ID OR PWORKGROUP_ID  IS NULL )                                       
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
  AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
--and (st.shift_id=PSHIFT_ID or PSHIFT_ID is null)
 AND (REGEXP_LIKE(st.shift_id,PSHIFT_ID) or PSHIFT_ID is null)
             And SYSDATE BETWEEN NVL (REPLACEMENT.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (REPLACEMENT.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY')
          order by st.scheduler_staff_id
          ;
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.full_name;
    else
        v:=v||' '||b.full_name;
    end if ;
    end loop ;
    

   return v ;
   
          END ;
FUNCTION PUBLICSPACEBULLETIN1(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 
   IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  bulletin_name  from as_scheduler_staff st ,PER_ALL_PEOPLE_F PAPF ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk   , as_scheduler_staff_bulletins  sbt,as_bulletins bt
           where st.person_id=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
  AND        sbt.bulletin_id=bt.bulletin_id 
            and st.PERSON_ID = PAPF.PERSON_ID(+)
           AND sbt.SCHEDULER_STAFF_ID=ST.SCHEDULER_STAFF_ID
   AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
           AND (bt.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
          AND ( wk.work_group_id = PWORKGROUP_ID OR PWORKGROUP_ID  IS NULL )                                       
   AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
--and (st.shift_id=PSHIFT_ID or PSHIFT_ID is null)
 AND (REGEXP_LIKE(st.shift_id,PSHIFT_ID) or PSHIFT_ID is null)

             And SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (PAPF.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY')
          order by st.scheduler_staff_id ;
         
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.bulletin_name;
    else
        v:=v||'/'||b.bulletin_name;
    end if ;
    end loop ;
       return v ;
    END ;
     FUNCTION PUBLICSPACENOTES1(PPERSON_ID NUMBER,PSTART_DATE VARCHAR2,PWORKGROUP_ID NUMBER,PSHIFT_ID VARCHAR2,P_ORG_ID NUMBER)RETURN VARCHAR2 
   IS
         v  varchar2(5000) ;
        vv varchar2(5000);
           bFirst_row      BOOLEAN := TRUE;
          cursor xx
          is
               select  Notes  from as_scheduler_staff st ,PER_ALL_PEOPLE_F PAPF ,AS_SCHEDULERs schdlr
                ,  AS_WORK_GROUPS_HDR wk 
           where st.person_id=PPERSON_ID
          AND   schdlr.SCHEDULAR_STATUS = 'PUBLISHED'
          AND schdlr.SCHEDULER_TYPE = 'Layout_A'
 
            and st.PERSON_ID = PAPF.PERSON_ID(+)
      
   AND   st.SCHEDULER_ID = schdlr.SCHEDULER_ID
          and st.work_group_id=wk.work_group_id
  
          AND ( wk.work_group_id = PWORKGROUP_ID OR PWORKGROUP_ID  IS NULL )                                       
 AND (wk.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
   AND (schdlr.fk_organization_id=P_ORG_ID or P_ORG_ID is null)  
--and (st.shift_id=PSHIFT_ID or PSHIFT_ID is null)
 AND (REGEXP_LIKE(st.shift_id,PSHIFT_ID) or PSHIFT_ID is null)

             And SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)   
          AND NVL (PAPF.EFFECTIVE_END_DATE,SYSDATE + 1)
          and SCHEDULAR_START_DATE=to_date(PSTART_DATE,'dd/mm/YYYY') order by st.scheduler_staff_id;
        
      begin 
    for b in xx
    loop
    if(bFirst_row )
    then
    bFirst_row := FALSE;
    v:=v||' '||b.Notes;
    else
        v:=v||' '||b.Notes;
    end if ;
    end loop ;
       return v ;
    END ;
    
END AS_SCHDLR_PKG;
/

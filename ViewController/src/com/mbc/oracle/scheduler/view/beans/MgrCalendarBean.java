package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;
import com.mbc.oracle.scheduler.view.util.JSFUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichCalendar;
import oracle.adf.view.rich.dnd.DnDAction;
import oracle.adf.view.rich.event.CalendarActivityEvent;
import oracle.adf.view.rich.event.DropEvent;
import oracle.adf.view.rich.model.CalendarActivity;

import oracle.adf.view.rich.util.CalendarActivityRamp;
import oracle.adf.view.rich.util.InstanceStyles;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;

public class MgrCalendarBean {
    public MgrCalendarBean() {
    }
    private RichCalendar mgrCalendar;
    private RichPopup transPop;

    public void setCalStyles(HashMap calStyles) {
        this.calStyles = calStyles;
    }

    public HashMap getCalStyles() {
        //return calStyles;
        try {
        HashSet LeaveStatus1 = new HashSet<String>();
    HashSet LeaveStatus2= new HashSet<String>();
//        HashSet LeaveStatus3 = new HashSet<String>();
//        HashSet LeaveStatus4= new HashSet<String>();
        // HashSet event3= new HashSet<String>();

        LeaveStatus1.add("SH");
  LeaveStatus2.add("A");
//        LeaveStatus3.add("L");
//        LeaveStatus4.add("AL");    
        // event3.add("103");
       // CalendarActivityRamp.RampKey
      // CalendarActivityRamp.getActivityRamp(CalendarActivityRamp.RampKey.valueOf("LeaveStatus1"));
        calStyles.put(LeaveStatus1,CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.GREEN ));
      calStyles.put(LeaveStatus2, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.ORANGE));
//         calStyles.put(LeaveStatus3, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.ORANGE ));
//        calStyles.put(LeaveStatus4, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.RED ));
        // activityStyles.put(event3, CalendarActivityRamp.getActivityRamp( CalendarActivityRamp.RampKey.MIDNIGHTBLUE ));

        } catch (Exception e) {
        e.printStackTrace();
        }
        return calStyles;
    }
    private HashMap calStyles = new HashMap<Set<String>, InstanceStyles>();

    public void setMgrCalendar(RichCalendar mgrCalendar) {
        this.mgrCalendar = mgrCalendar;
    }

    public RichCalendar getMgrCalendar() {
        return mgrCalendar;
    }

//    public void mgrCalendarActivityListener(CalendarActivityEvent calendarActivityEvent) {
//        // Add event code here...
//        CalendarActivity activity = calendarActivityEvent.getCalendarActivity();
//        this.setCurrentRowUsingCalActivity(activity);
//    }

//    private void setCurrentRowUsingCalActivity(CalendarActivity activity) {
//        if (activity != null) {
//            DCIteratorBinding iterator = ADFUtils.findIterator("MgrSchedulesCalVOIterator");
//            Key key = new Key(new Object[] { activity.getId() });
//            RowSetIterator rsi = iterator.getRowSetIterator();
//            Row row = rsi.findByKey(key, 1)[0];
//            rsi.setCurrentRow(row);
//        }
//    }
//
//    public void onClickCancelCalCntxPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        this.createTransRecordShowPop("CANCEL");
//    }
//
//    public void onClickReplaceCalCntxPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        this.createTransRecordShowPop("REPLACE");
//    }
//
//    public void onClickChangeTimingCalCntxPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        this.createTransRecordShowPop("CHANGE");
//    }
//
//    private void createTransRecordShowPop(String transType) {
//        Number schedulerStaffId = (Number) ADFUtils.getBoundAttributeValue("SchedulerStaffId");
//        Date schedularStartDate =
//            (Date) ADFUtils.findIterator("MgrSchedulesCalVOIterator").getCurrentRow().getAttribute("SchedularStartDate");
//
//        ADFUtils.findOperation("CreateInsertTrans").execute();
//        ADFUtils.setBoundAttributeValue("SchedulerStaffIdTrans", schedulerStaffId);
//        ADFUtils.setBoundAttributeValue("TransTypeAttr", transType);
//        Number personId = (Number) ADFUtils.evaluateEL("#{sessionScope.userHelper.personId}");
//        ADFUtils.setBoundAttributeValue("RequesterId", personId);
//        ADFUtils.setBoundAttributeValue("EffectiveStartDate", schedularStartDate);
//        ADFUtils.setBoundAttributeValue("EffectiveEndDate", schedularStartDate);
//
//        DCIteratorBinding iter = ADFUtils.findIterator("AsApprovalTypesVOIterator");
//        Key key = null;
//        if ("CHANGE".equals(transType)) {
//            key = new Key(new Object[] { "CHANGE" });
//
//            ADFUtils.setBoundAttributeValue("StartTimeHoursTrans",
//                                            (Number) ADFUtils.getBoundAttributeValue("StartTimeHours"));
//            ADFUtils.setBoundAttributeValue("StartTimeMinutesTrans",
//                                            (Number) ADFUtils.getBoundAttributeValue("StartTimeMinutes"));
//            ADFUtils.setBoundAttributeValue("EndTimeHoursTrans",
//                                            (Number) ADFUtils.getBoundAttributeValue("EndTimeHours"));
//            ADFUtils.setBoundAttributeValue("EndTimeMinutesTrans",
//                                            (Number) ADFUtils.getBoundAttributeValue("EndTimeMinutes"));
//        } else if ("REPLACE".equals(transType)) {
//            key = new Key(new Object[] { "REPLACE" });
//        } else if ("CANCEL".equals(transType)) {
//            key = new Key(new Object[] { "CANCEL" });
//        }
//        iter.setCurrentRowWithKey(key.toStringFormat(true));
//
//        Number oldPersonId = (Number) ADFUtils.getBoundAttributeValue("PersonId");
//        Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
//
//        RichPopup.PopupHints hints = new RichPopup.PopupHints();
//        this.getTransPop().show(hints);
//    }
//
//    public void setTransPop(RichPopup transPop) {
//        this.transPop = transPop;
//    }
//
//    public RichPopup getTransPop() {
//        return transPop;
//    }
//
//    public void onClickSaveTransPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        ADFUtils.findOperation("Commit").execute();
//        Number schedulerTransHdrId = (Number) ADFUtils.getBoundAttributeValue("SchedulerTransHdrId");
//        OperationBinding oper = ADFUtils.findOperation("submitSchdlrTrans");
//        oper.getParamsMap().put("schedulerTransHdrId", schedulerTransHdrId);
//        oper.execute();
//
//        ADFUtils.findIterator("AsSchedulerTransHdrsVOIterator").getCurrentRow().refresh(Row.REFRESH_WITH_DB_FORGET_CHANGES);
//
//        this.getTransPop().hide();
//    }
//
//    public void onClickCancelTransPopBtn(ActionEvent actionEvent) {
//        // Add event code here...
//        ADFUtils.findOperation("DeleteTrans").execute();
//        ADFUtils.findOperation("Commit").execute();
//        this.getTransPop().hide();
//    }
//
//    public DnDAction empDropTargetLsnr(DropEvent dropEvent) {
//        return DnDAction.NONE;
//    }
//
//    public void initEmpView() {
//        // Add event code here...
//        Number personId = (Number) JSFUtils.resolveExpression("#{sessionScope.userHelper.personId}");
//        ViewObject empSchdlrVo = ADFUtils.findIterator("MgrSchedulesCalVOIterator").getViewObject();
//        empSchdlrVo.setWhereClause("PERSON_ID=" + personId);
//    }
//
//    public void initMgrView() {
//        // Add event code here...
//        Number personId = (Number) JSFUtils.resolveExpression("#{sessionScope.userHelper.personId}");
//        ViewObject empSchdlrVo = ADFUtils.findIterator("MgrSchedulesCalVOIterator").getViewObject();
//        empSchdlrVo.setWhereClause("PERSON_ID in (select paaf.person_id from per_all_assignments_f paaf where sysdate between paaf.effective_start_date and paaf.effective_end_date  and paaf.supervisor_id = " +
//                                   personId + ")");
//    }
}

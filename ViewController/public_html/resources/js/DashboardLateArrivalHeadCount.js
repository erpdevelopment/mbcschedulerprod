var vioChartData = [];
var leaveCount = 0;
var bucket1 = 20;
var bucket2 = 30;
var bucket3 = 75;
var bucket4 = 40;


vioChartData.push({value: bucket1, valueColor: "#609480"});
vioChartData.push({value: bucket2, valueColor: "#F8DD46"});
vioChartData.push({value: bucket3, valueColor: "#DEAF43"});
vioChartData.push({value: bucket4, valueColor: "#DE4A43"});

 function empVioChartCreate() {
            $("#_absence_employee_vio_bar").kendoChart({
                theme: 'material',
                legend: {
                    visible: false
                },
               
                chartArea: {
                    background: ""
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [{
                    name: "#Employees",
                    colorField: "valueColor",
                     data: vioChartData
                }],
               categoryAxis: {
                    categories: ["< 5", "5 - 10", "10 - 15", "> 15"],
                    labels: {
                        font: "12px Calibri,Arial"
                    },
                    majorGridLines: {
                        visible: false
                    }
                },
                valueAxis: {
                    visible:true,
					min:0,
                    line: {
                        visible: false
                    },
                    majorGridLines: {
                        visible: false
                    },
                    axisCrossingValue: 0
                },                
                tooltip: {
                    visible: true,
                    format: "{0}",
                    template: "#= series.name #: #= value #"
                }
            });
        }
package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import java.util.Map;

import oracle.adf.model.BindingContext;

import oracle.adf.share.ADFContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Number;
import oracle.jbo.uicli.binding.JUCtrlListBinding;

public class OnEmployeeBean {
    
    public Date WeekStartDate ;
    public String EmpFullName ;
    public String firstDay;
    
    
    
    public OnEmployeeBean() {
        super();
    }

    public void setWeekStartDate(Date WeekStartDate) {
        this.WeekStartDate = WeekStartDate;
    }

    public Date getWeekStartDate() {
        if(WeekStartDate!= null && (!WeekStartDate.equals("")))
         return WeekStartDate;
            
         else 
        {

            return  new Date();
            }

    }

    public void setEmpFullName(String EmpFullName) {
        this.EmpFullName = EmpFullName;
    }

    public String getEmpFullName() {
        return EmpFullName;
    }
    public String SearchonEmployee_Btn() {
        // Add event code here...
     DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
      String StartDate=df.format(this.getWeekStartDate());
     Number cWorkgroupId = (Number) ADFUtils.getBoundAttributeValue("WorkGroupId");
     // String EndDate=df.format(this.addDays(this.getWeekStartDate(), 6));
       OperationBinding oper = ADFUtils.findOperation("onEmployeeSearch");
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        String EmpFullName = (String) sessionscope.get("AAEmpName");
       oper.getParamsMap().put("pstart_Date",StartDate);
      oper.getParamsMap().put("ShiftId",getSelectedShifts());
        oper.getParamsMap().put("WorkGroupId",cWorkgroupId);
        oper.getParamsMap().put("pfullname",EmpFullName);
                                //this.getEmpFullName());
       oper.execute();
  

               return null;
    }
    public String getSelectedShifts()
    {
            BindingContainer bc=getBindings();
            JUCtrlListBinding ListBinding = 
            (JUCtrlListBinding) bc.get("ShiftsLov1");
             Object []str=ListBinding.getSelectedValues();
             StringBuilder Selected=new StringBuilder();
            for (int i=0;i<str.length;i++)
             {
               if(i==0)
               {
                 Selected.append(str[i]);
                 }
                 else 
                {
                  Selected.append("|");
                  Selected.append(str[i]);
                   }
                 
                      } 
        
        
        return Selected.toString();
        }
    
    public void initonEmployeeData() {
        // Add event code here...
        ADFUtils.findOperation("LoadonEmployee").execute();
    }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setFirstDay(String firstDay) {
        this.firstDay = firstDay;
    }

    public String getFirstDay() {
      //  return firstDay;
      return   this.DayOfWeek(this.getWeekStartDate());
    }
    //Method to retrive day of week and month like thursday-march
      public  String DayOfWeek(Date dt)
      {
                 Calendar cal = Calendar.getInstance();
                 cal.setTime(dt);
                // int year = cal.get(Calendar.YEAR);
                 int month = cal.get(Calendar.MONTH);
                 int day = cal.get(Calendar.DAY_OF_WEEK);
                 int dayofmonth=cal.get(Calendar.DAY_OF_MONTH);
                 String DayofWeek="";
                 String MonthofYear="";
                 if(day==1)
                     DayofWeek="Sun";
               else  if(day==2)
                     DayofWeek= "Mon";
            else  if(day==3)
                  DayofWeek= "Tue";
            else  if(day==4)
                  DayofWeek= "Wed";
           else   if(day==5)
                  DayofWeek= "Thurs";
           else   if(day==6)
                  DayofWeek="Fri";
         else     if(day==7)
                  DayofWeek= "Sat";
              //------------Month-------
              if(month==0)
                  MonthofYear="Jan";
            else  if(month==1)
                  MonthofYear="Feb";
              else  if(month==2)
                    MonthofYear="March";
              else  if(month==3)
                    MonthofYear="Apr";
              else  if(month==4)
                    MonthofYear="May";
              else  if(month==5)
                    MonthofYear="Jun";
              else  if(month==6)
                    MonthofYear="Jul";
                else  if(month==7)
                      MonthofYear="Aug";
                else  if(month==8)
                      MonthofYear="Sept";
                else  if(month==9)
                      MonthofYear="Oct";
                else  if(month==10)
                      MonthofYear="Nov";
              else  if(month==11)
                    MonthofYear="Dec";
              return DayofWeek+" "+MonthofYear+" "+String.valueOf(dayofmonth);
            
          }
}

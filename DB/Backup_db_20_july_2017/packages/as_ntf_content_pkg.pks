DROP PACKAGE APPS.AS_NTF_CONTENT_PKG;

CREATE OR REPLACE PACKAGE APPS.AS_NTF_CONTENT_PKG
IS
  FUNCTION BUILD_CAL_EVENT (
      P_SUMMARY           IN VARCHAR2,
      P_ORGANIZER_NAME    IN VARCHAR2 DEFAULT 'AA Schedules',
      P_ORGANIZER_EMAIL   IN VARCHAR2,
      P_START_DATE        IN DATE,
      P_END_DATE          IN DATE)
      RETURN VARCHAR2;
   PROCEDURE GET_SCHDLR_NTF (IN_SCHEDULER_ID          NUMBER,
                             IN_NOTIFICATION_ID       NUMBER,
                             IN_ACCESS_KEY            VARCHAR2,
                             OUT_NTF_SUBJECT      OUT VARCHAR2,
                             OUT_NTF_BODY         OUT CLOB);

   PROCEDURE GET_SCHDLR_CAL (IN_SCHEDULER_ID       NUMBER,
                             in_SCHEDULER_STAFF_ID          NUMBER,
                             OUT_CAL_SUBJECT     OUT   VARCHAR2,
                             OUT_CAL_CONTENT   OUT CLOB);

   PROCEDURE GET_TRANS_NTF (IN_SCHEDULER_TRANS_HDR_ID       NUMBER,
                            IN_NOTIFICATION_ID              NUMBER,
                            IN_ACCESS_KEY                   VARCHAR2,
                            OUT_NTF_SUBJECT             OUT VARCHAR2,
                            OUT_NTF_BODY                OUT CLOB);
END AS_NTF_CONTENT_PKG;
/

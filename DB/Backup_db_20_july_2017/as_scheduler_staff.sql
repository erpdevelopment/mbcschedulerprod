ALTER TABLE APPS.AS_SCHEDULER_STAFF
 DROP PRIMARY KEY CASCADE;

DROP TABLE APPS.AS_SCHEDULER_STAFF CASCADE CONSTRAINTS;

CREATE TABLE APPS.AS_SCHEDULER_STAFF
(
  SCHEDULER_STAFF_ID         NUMBER,
  SCHEDULER_ID               NUMBER,
  PERSON_ID                  NUMBER,
  SCHEDULAR_START_DATE       DATE,
  SCHEDULAR_END_DATE         DATE,
  START_TIME_HOURS           NUMBER,
  START_TIME_MINUTES         NUMBER,
  END_TIME_HOURS             NUMBER,
  END_TIME_MINUTES           NUMBER,
  CREATION_DATE              DATE,
  CREATED_BY                 NUMBER,
  LAST_UPDATE_DATE           DATE,
  LAST_UPDATED_BY            NUMBER,
  LAST_UPDATE_LOGIN          NUMBER,
  RECURRING_FLAG             VARCHAR2(1 BYTE)   DEFAULT 'N',
  BULLETIN_FLAG              VARCHAR2(1 BYTE)   DEFAULT 'N',
  PARENT_SCHEDULER_STAFF_ID  NUMBER,
  SHIFT_ID                   NUMBER,
  WORK_GROUP_ID              NUMBER
)
TABLESPACE APPS_TS_TX_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX APPS.AS_SCHEDULER_STAFF_PK ON APPS.AS_SCHEDULER_STAFF
(SCHEDULER_STAFF_ID)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


ALTER TABLE APPS.AS_SCHEDULER_STAFF ADD (
  CONSTRAINT AS_SCHEDULER_STAFF_PK
  PRIMARY KEY
  (SCHEDULER_STAFF_ID)
  USING INDEX APPS.AS_SCHEDULER_STAFF_PK
  ENABLE VALIDATE);

ALTER TABLE APPS.AS_NTF_TEMPLATES
 DROP PRIMARY KEY CASCADE;

DROP TABLE APPS.AS_NTF_TEMPLATES CASCADE CONSTRAINTS;

CREATE TABLE APPS.AS_NTF_TEMPLATES
(
  TEMPLATE_KEY      VARCHAR2(32 BYTE),
  TEMPLATE_NAME     VARCHAR2(256 BYTE),
  TEMPLATE_CONTENT  CLOB
)
LOB (TEMPLATE_CONTENT) STORE AS (
  TABLESPACE  APPS_TS_TX_DATA
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          128K
                  NEXT             128K
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ))
TABLESPACE APPS_TS_TX_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX APPS.AS_NTF_TEMPLATES_PK ON APPS.AS_NTF_TEMPLATES
(TEMPLATE_KEY)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


ALTER TABLE APPS.AS_NTF_TEMPLATES ADD (
  CONSTRAINT AS_NTF_TEMPLATES_PK
  PRIMARY KEY
  (TEMPLATE_KEY)
  USING INDEX APPS.AS_NTF_TEMPLATES_PK
  ENABLE VALIDATE);

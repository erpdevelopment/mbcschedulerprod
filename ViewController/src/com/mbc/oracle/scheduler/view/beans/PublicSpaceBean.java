package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

public class PublicSpaceBean {
    public PublicSpaceBean() {
    }

    public void initPublicSpaceData() {
        // Add event code here...
        ADFUtils.findOperation("LoadPublicSpace").execute();
    }
}

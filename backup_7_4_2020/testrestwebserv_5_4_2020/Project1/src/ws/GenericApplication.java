package ws;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import oracle.wsm.metadata.annotation.PolicyReference;
import oracle.wsm.metadata.annotation.PolicySet;

@ApplicationPath("resources")
//@PolicySet(references = { @PolicyReference(value = "oracle/multi_token_rest_service_policy") })
public class GenericApplication extends Application {
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();

        // Register root resources.
        classes.add(DashBoard.class);
        classes.add(HrSit.class);
        classes.add(NewLeave.class);
        classes.add(WorkFlowNotification.class);
       // classes.add(ThanksCard.class);
        classes.add(LeavesWS.class);
        classes.add(SocialWS.class);
        classes.add(Profile.class);
        classes.add(Payroll.class);
        classes.add(Login.class);
        
        
        // AA
        classes.add(Scheduler.class);
        
      //  classes.add(Irec.class);

        // Register provider classes.
        classes.add(CORSFilter.class);

        return classes;
    }
}

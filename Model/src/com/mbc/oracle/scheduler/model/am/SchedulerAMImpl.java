package com.mbc.oracle.scheduler.model.am;

import com.mbc.oracle.scheduler.model.am.common.SchedulerAM;
import com.mbc.oracle.scheduler.model.vo.AcSchedulerFirstApprovalLeaveVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsAdminRoleVoImpl;
import com.mbc.oracle.scheduler.model.vo.AsAllSchedulerShiftsVoImpl;
import com.mbc.oracle.scheduler.model.vo.AsAssignmentEmpsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsAssignmentsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsDummyMonthDaysImpl;
import com.mbc.oracle.scheduler.model.vo.AsEditorRoleVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsEmail_configurationvoImpl;
import com.mbc.oracle.scheduler.model.vo.AsEmployeeContactInformationvoImpl;
import com.mbc.oracle.scheduler.model.vo.AsEmployeeContactInformationvoRowImpl;
import com.mbc.oracle.scheduler.model.vo.AsEmployee_TasVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsMultiOrgRoleVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsOnEmployeeVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceAssignmentsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceAssignmentsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceBulletinsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceBulletinsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceEmployeeVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdeulerStaffTableVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdlrStaffBulletinsHistVoImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdlrStaffBulletinsHistVoRowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdulerCurrentShiftworkrgroupvoImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerMonthVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerSectionsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerSectionsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerShiftsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerShiftsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffBulletinsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffBulletinsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffEmailsmsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffHistoryVoImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffHistoryVoRowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffShiftTableVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffShiftTableVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulersVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulersVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulertotalValidateVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulertotalValidateVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSendEmailSMSVoImpl;
import com.mbc.oracle.scheduler.model.vo.AsShiftAttributesVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsShiftAttributesVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsShiftWorkGroupsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsShiftWorkgroupOrganizationVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsTemplateEmpBulletinsVOImpl;

import com.mbc.oracle.scheduler.model.vo.AsTemplateEmployeeVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsTemplateEmployeeVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsTemplatesVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsTemplatesVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsWorkGroupsHdrVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsWorkGroupsLinesVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsWorkgroupNotificationsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsWorklistVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsWorklistVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AspublicSpaceLeavesVOImpl;
import com.mbc.oracle.scheduler.model.vo.AspublicSpaceLeavesVORowImpl;
import com.mbc.oracle.scheduler.model.vo.EmpSchedulesCalVOImpl;
import com.mbc.oracle.scheduler.model.vo.lov.AsShiftCodevoImpl;
import com.mbc.oracle.scheduler.model.vo.lov.DefaultWorkGroupvaluevoImpl;
import com.mbc.oracle.scheduler.model.vo.lov.DefaultWorkGroupvaluevoRowImpl;
import com.mbc.oracle.scheduler.model.vo.lov.ShiftsLovImpl;
import com.mbc.oracle.scheduler.model.vo.lov.WorkGroupLovImpl;
import com.mbc.oracle.scheduler.model.vo.lov.WorkGroupLovRowImpl;
import com.mbc.oracle.scheduler.model.vo.lov.WorkGroupShiftVOImpl;

import com.sun.org.apache.xpath.internal.operations.And;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import oracle.adf.share.ADFContext;

import oracle.jbo.AttributeDef;
import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.DBTransaction;
import oracle.jbo.server.ViewLinkImpl;
import oracle.jbo.server.ViewObjectImpl;


// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jan 31 13:55:52 GST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SchedulerAMImpl extends ApplicationModuleImpl implements SchedulerAM {
    /**
     * This is the default constructor (do not remove).
     */
    public SchedulerAMImpl() {
    }

    public void submitChanges() {
        this.getDBTransaction().validate();
    }
    //-----Added By Heba ////


    public void PublicSpaceSearch(String pstart_Date, String ShiftId, Number WorkGroupId, String pfullname)

        {


        AsPublicSpaceEmployeeVOImpl publicspace = this.getAsPublicSpaceEmployeeVO1();
        publicspace.setNamedWhereClauseParam("p_start_date", pstart_Date);
       // publicspace.setNamedWhereClauseParam("p_end_date", pend_Date);
        if (ShiftId != null && !ShiftId.isEmpty())
        //(ShiftId.intValue() != 0)
            publicspace.setNamedWhereClauseParam("p_shift_id", ShiftId);
        else
            publicspace.setNamedWhereClauseParam("p_shift_id", null);
        if (WorkGroupId.intValue() != 0)
            publicspace.setNamedWhereClauseParam("p_workgroup_id", WorkGroupId);
        else
            publicspace.setNamedWhereClauseParam("p_workgroup_id", null);

        if (pfullname != null && !pfullname.isEmpty())
            publicspace.setNamedWhereClauseParam("P_fullname", pfullname);
        else
            publicspace.setNamedWhereClauseParam("P_fullname", null);
        publicspace.executeQuery();
       // publicspace.scrollToRangePage(1);
       // publicspace.first();

    }

    public void onEmployeeSearch(String pstart_Date, String ShiftId, Number WorkGroupId, String pfullname)

        {

 
        AsOnEmployeeVOImpl onEmployee =  this.getAsOnEmployeeVO1();
       onEmployee.setNamedWhereClauseParam("pdate", pstart_Date);

        if (ShiftId != null && !ShiftId.isEmpty())
        //(ShiftId.intValue() != 0)
            onEmployee.setNamedWhereClauseParam("PSHIFT_ID", ShiftId);
        else
            onEmployee.setNamedWhereClauseParam("PSHIFT_ID", null);
        if (WorkGroupId.intValue() != 0)
           onEmployee.setNamedWhereClauseParam("p_workgroup_id", WorkGroupId);
        else
            onEmployee.setNamedWhereClauseParam("p_workgroup_id", null);

        if (pfullname != null && !pfullname.isEmpty())
           onEmployee.setNamedWhereClauseParam("P_fullname", pfullname);
        else
            onEmployee.setNamedWhereClauseParam("P_fullname", null);
      onEmployee.executeQuery();
    

    }

    public int validateSchedulerDay(String p_Date)

    {
        int result = 0;
        Number SchedulerId;
        AsSchedulersVOImpl sch = this.getAsSchedulersVO();
        AsSchedulersVORowImpl rs = (AsSchedulersVORowImpl) this.getAsSchedulersVO().getCurrentRow();
        AsSchedulertotalValidateVOImpl validate = this.getAsSchedulertotalValidateVO1();
        validate.setWhereClauseParams(null);
        validate.setNamedWhereClauseParam("pdate", p_Date);
        validate.setNamedWhereClauseParam("pscheduleid", rs.getSchedulerId());
        validate.executeQuery();
        if (validate.first() != null) {

            AsSchedulertotalValidateVORowImpl r = (AsSchedulertotalValidateVORowImpl) validate.first();
            result = r.getTotalinvalid().intValue();
        }
        return result;

    }

    public HashMap getStartEndScheduleDates() {

        HashMap schdates = new HashMap<String, Object>();
        AsSchedulersVOImpl sch = this.getAsSchedulersVO();
        AsSchedulersVORowImpl rs = (AsSchedulersVORowImpl) this.getAsSchedulersVO().getCurrentRow();
        schdates.put("SchedulerStartDate", rs.getStartDate());
        schdates.put("SchedulerEndDate", rs.getEndDate());
        return schdates;
    }

    public Number getTotalOnScheduleLeave(Number _SchedulerID) {

        Number count = new Number();
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  :1 := as_schdlr_pkg.getOnSchedul_Leave_employee (:2); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {

            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.setInt(2, _SchedulerID.intValue());
            callStmt.execute();
            count = new Number(callStmt.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        return count;


    }

    public String getScheduleValidateDetail(java.util.Date activeday, Number SchedulerId) {
        String validation = "";

        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  :1 := AS_SCHDLR_PKG.getValidationDetails (:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {

            callStmt.registerOutParameter(1, Types.VARCHAR);
            callStmt.setInt(2, SchedulerId.intValue());
            callStmt.setDate(3, new java.sql.Date(activeday.getTime()));

            callStmt.execute();
            validation = callStmt.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }

        return validation;

    }

    public void LoadPublicSpace()

    {
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String StartDate = df.format(new java.util.Date());
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
       // Number workgpId = (Number) sessionscope.get("wkId");
        String EmpFullName = (String) sessionscope.get("AAEmpName");
        AsPublicSpaceEmployeeVOImpl publicspace = this.getAsPublicSpaceEmployeeVO1();
        publicspace.setNamedWhereClauseParam("p_start_date", StartDate);
       // publicspace.setNamedWhereClauseParam("p_workgroup_id", workgpId);
        publicspace.setNamedWhereClauseParam("P_fullname", EmpFullName);
        publicspace.executeQuery();


    }
    public void LoadonEmployee()

    {
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String StartDate = df.format(new java.util.Date());
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
       // Number workgpId = (Number) sessionscope.get("wkId");
       String EmpFullName = (String) sessionscope.get("AAEmpName");
        AsOnEmployeeVOImpl onEmployee =  this.getAsOnEmployeeVO1();
        onEmployee.setNamedWhereClauseParam("pdate", StartDate);
        onEmployee.setNamedWhereClauseParam("P_fullname", EmpFullName);
       // onEmployee.setNamedWhereClauseParam("p_workgroup_id", workgpId);
       onEmployee.executeQuery();


    }


    public java.util.Date addDays(java.util.Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    // Custom Methods for Transient attribute in AsPublicSpaceEmployeeVO


    public String GetScheduleShiftWorkgroup(Number PersonId, String StartDate, int Daynum) {

        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        String value="";
        if (Daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, Daynum));
        }
        StringBuilder sh = new StringBuilder();
        String replace=this.getPublicSpaceReplacement(PersonId,StartDate, Daynum);
        
        if (!replace.equals("NO"))
        {
            sh.append(replace);
            sh.append(" ");
         sh.append("/ ");
        
        
        }
        AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
        ps.setWhereClauseParams(null);
        ps.setWhereClause(null);
        ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
        ps.setWhereClauseParams(new Object[] { PersonId, WeekDay });
        ps.executeQuery();
        
        int count = ps.getRowCount();
        if (count > 0) {
            
          //  sh.append("(");
            for (int i = 1; i <= count; i++) {

                AsPublicSpaceVORowImpl r = (AsPublicSpaceVORowImpl) ps.next();
                String ShiftName = r.getShiftName();
                if (ShiftName.equals("N"))
                    sh.append("");
                else {
                    sh.append(ShiftName);
                    if (i != count)
                        sh.append("/");
                }

            }
          //  sh.append(")");
          

        } 
        
       
   
       
      
       value=sh.toString(); 
        return value;
        //else
        //  return  String.valueOf(count)+ this.getPublicSpaceReplacement(PersonId,StartDate, Daynum);


    }


    public String GetScheduleNotes(Number PersonId, String StartDate, int Daynum) {


        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        if (Daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, Daynum));
        }
        AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
        ps.setWhereClauseParams(null);
        ps.setWhereClause(null);
        ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
        ps.setWhereClauseParams(new Object[] { PersonId, WeekDay });
        ps.executeQuery();
        StringBuilder sh = new StringBuilder();
        int count = ps.getRowCount();
        if (count > 0) {
            sh.append("(");
            for (int i = 1; i <= count; i++) {

                AsPublicSpaceVORowImpl r = (AsPublicSpaceVORowImpl) ps.next();
                String Notes = r.getNotes();
                if (Notes.equals("N"))
                    return "";
                else {
                    sh.append(Notes);
                    if (i != count)
                        sh.append("/");
                }

            }
            sh.append(")");
            return sh.toString();

        } else
            return "";


    }

    public String GetScheduleSection(Number PersonId, String StartDate, int Daynum) {
        StringBuilder st = new StringBuilder();

        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        if (Daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, Daynum));
        }
        AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
        ps.setWhereClauseParams(null);
        ps.setWhereClause(null);
        ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
        ps.setWhereClauseParams(new Object[] { PersonId, WeekDay });
        ps.executeQuery();
        StringBuilder sh = new StringBuilder();
        int count = ps.getRowCount();
        if (count > 0) {
            sh.append("(");
            for (int i = 1; i <= count; i++) {

                AsPublicSpaceVORowImpl r = (AsPublicSpaceVORowImpl) ps.next();
                String Section = r.getSectionName();
                if (Section.equals("N"))
                    return "";
                else {
                    sh.append(Section);
                    if (i != count)
                        sh.append("/");
                }

            }
            sh.append(")");
            return sh.toString();

        } else
            return "";


    }

    public String GetPublicSpaceBulletin(Number PersonId, String StartDate, int Daynum) {
        StringBuilder bt = new StringBuilder();

        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        if (Daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, Daynum));
        } //end week day if //
        AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
        ps.setWhereClauseParams(null);
        ps.setWhereClause(null);
        ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
        ps.setWhereClauseParams(new Object[] { PersonId, WeekDay });
        ps.executeQuery();
        int countsh = ps.getRowCount();
        if (countsh > 0) {

            for (int l = 1; l <= countsh; l++) {
                AsPublicSpaceVORowImpl row = (AsPublicSpaceVORowImpl) ps.next();
                // here get schedular_staff_id and select from publicspacevobulletin to get bulletin name ,onair star/onair end
                Number Scedular_staff_id = row.getSchedulerStaffId();
                AsPublicSpaceBulletinsVOImpl b = this.getAsPublicSpaceBulletinsVO1();
                b.setWhereClauseParams(null);
                b.setWhereClause(null);
                b.setNamedWhereClauseParam("p_schedular_staff_id", Scedular_staff_id);
                b.executeQuery();
                int count = b.getRowCount();
                if (count > 0) {
                    bt.append("(");
                    for (int i = 1; i <= count; i++) {
                        AsPublicSpaceBulletinsVORowImpl r = (AsPublicSpaceBulletinsVORowImpl) b.next();

                        bt.append(r.getBulletinName());
                        bt.append("");
                        if (i != count) {
                            bt.append("/");
                            bt.append("");
                        }


                    }
                    bt.append(")");
                }
                //childe loop//


            } //parent loop
            return bt.toString();
        } else
            return "";
    }


    public String GetPublicSpaceassignments(Number PersonId, String StartDate, int Daynum) {
        StringBuilder st = new StringBuilder();
        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        if (Daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, Daynum));
        }


        AsPublicSpaceAssignmentsVOImpl ps = this.getAsPublicSpaceAssignmentsVO1();
        ps.setNamedWhereClauseParam("pdate", WeekDay);
        ps.setNamedWhereClauseParam("pperson_id", PersonId);
        ps.executeQuery();
        if (ps.first() != null) {

            AsPublicSpaceAssignmentsVORowImpl row = (AsPublicSpaceAssignmentsVORowImpl) ps.first();
            st.append(row.getAssignmentName());
            st.append("(");
            st.append(row.getEmpStartDate());
            st.append("/");
            st.append(row.getEmpEndDate());
            st.append(")");
            return st.toString();
        } else
            return "";


    }

    public String getPublicSpaceLeave(Number PersonId, String StartDate, int Daynum) {
        StringBuilder st = new StringBuilder();
        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        if (Daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, Daynum));
        }


        AspublicSpaceLeavesVOImpl ps = this.getAspublicSpaceLeavesVO1();
        ps.setNamedWhereClauseParam("pdate", WeekDay);
        ps.setNamedWhereClauseParam("pperson_id", PersonId);
        ps.executeQuery();
        if (ps.first() != null) {

            AspublicSpaceLeavesVORowImpl row = (AspublicSpaceLeavesVORowImpl) ps.first();
            // row.getDateStart()


            return row.getName();
        } else
            return "";


    }
    // Added 27-August-2018 //to view replacement person if exist in the public space page
    public String getPublicSpaceReplacement(Number PersonId, String StartDate, int Daynum)
    {
        
                    String Replacement="NO";
                        StringBuilder st = new StringBuilder();
                        java.util.Date sDate = new java.util.Date();
                        String WeekDay = "";
                        if (Daynum == 0) {
                            WeekDay = StartDate;
                        } else {
                            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                            // get start date as date from string start date
                            try {
                                sDate = df.parse(StartDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            WeekDay = df.format(this.addDays(sDate, Daynum));
                        }
        
        
        
            String sqlStmt = "begin  :1 := AS_SCHDLR_PKG.GETPUBLICSPACEREPLACEMENT (:2,:3); end;";
            CallableStatement callStmt = this.getDBTransaction().createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
            try {

                callStmt.registerOutParameter(1, Types.VARCHAR);
                callStmt.setInt(2, PersonId.intValue());
                callStmt.setString(3, WeekDay);
                 callStmt.execute();
                Replacement = callStmt.getString(1);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (callStmt != null) {
                    try {
                        callStmt.close();
                    } catch (SQLException ex) {
                        // TODO
                    }
                }
            }
            return Replacement;
        
      
        
        }

    //-------------------Tas Integration    11-July--2017
    public String GetTasShiftName(Number p_person_id, Number _day) {
        int range = this.GetDateRange(this.GetStartDateSession(), this.GetEndDateSession());
        String ShiftName = "";
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String sday;
        java.util.Date daynum;
        if (range > _day.intValue()) {
            //get day
            if (_day.intValue() == 0) {
                daynum = this.GetStartDateSessionUtil();
                sday = df.format(daynum);
            } else {
                daynum = this.addDays(this.GetStartDateSessionUtil(), _day.intValue());
                sday = df.format(daynum);
            }

            // Call vo public of get ShiftName
            AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
            ps.setWhereClauseParams(null);
            ps.setWhereClause(null);
            ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
            ps.setWhereClauseParams(new Object[] { p_person_id, sday });
            ps.executeQuery();
            StringBuilder st = new StringBuilder();
            if (ps.first() != null) {
                AsPublicSpaceVORowImpl row = (AsPublicSpaceVORowImpl) ps.first();

                st.append(row.getShiftName());
                ShiftName = st.toString();
                //ShiftName="On";
                //
            } //end if first is not null
            else { // No Schedule Shift in this Day//
                ShiftName = "Off";


            }

        } //end if in range date selection
        // Not in Selected Date Range
        else {

            ShiftName = "NA";
        }
        return ShiftName;


    }

    public String GetTasShiftDate(Number p_person_id, Number _day) {
        int range = this.GetDateRange(this.GetStartDateSession(), this.GetEndDateSession());
        String ShiftDate = "";
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String sday;
        java.util.Date daynum;
        if (range > _day.intValue()) {
            //get day
            if (_day.intValue() == 0) {
                daynum = this.GetStartDateSessionUtil();
                sday = df.format(daynum);
            } else {
                daynum = this.addDays(this.GetStartDateSessionUtil(), _day.intValue());
                sday = df.format(daynum);
            }

            // Call vo public of get ShiftName
            AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
            ps.setWhereClauseParams(null);
            ps.setWhereClause(null);
            ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
            ps.setWhereClauseParams(new Object[] { p_person_id, sday });
            ps.executeQuery();
            StringBuilder st = new StringBuilder();
            if (ps.first() != null) {
                AsPublicSpaceVORowImpl row = (AsPublicSpaceVORowImpl) ps.first();

                st.append(row.getSchedularStartDate());
                ShiftDate = st.toString();
                //ShiftName="On";
                //
            } //end if first is not null
            else { // No Schedule Shift in this Day//
                ShiftDate = sday;


            }

        } //end if in range date selection
        // Not in Selected Date Range
        else {

            ShiftDate = "NA";
        }
        return ShiftDate;


    }

    public String GetTasScheduleShiftStartTime(Number p_person_id, Number _day) {
        int range = this.GetDateRange(this.GetStartDateSession(), this.GetEndDateSession());
        String SrtartTime = "";
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String sday;
        java.util.Date daynum;
        if (range > _day.intValue()) {
            //get day
            if (_day.intValue() == 0) {
                daynum = this.GetStartDateSessionUtil();
                sday = df.format(daynum);
            } else {
                daynum = this.addDays(this.GetStartDateSessionUtil(), _day.intValue());
                sday = df.format(daynum);
            }

            // Call vo public of get ShiftName
            AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
            ps.setWhereClauseParams(null);
            ps.setWhereClause(null);
            ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
            ps.setWhereClauseParams(new Object[] { p_person_id, sday });
            ps.executeQuery();
            StringBuilder st = new StringBuilder();
            if (ps.first() != null) {
                AsPublicSpaceVORowImpl row = (AsPublicSpaceVORowImpl) ps.first();

                st.append(row.getStartTimeHours());
                SrtartTime = st.toString();
                //ShiftName="On";
                //
            } //end if first is not null
            else { // No Schedule Shift in this Day means it's off//
                SrtartTime = "00";


            }

        } //end if in range date selection
        // Not in Selected Date Range
        else {

            SrtartTime = "NA";
        }
        return SrtartTime;


    }

    public String GetTasScheduleShiftEndTime(Number p_person_id, Number _day) {
        int range = this.GetDateRange(this.GetStartDateSession(), this.GetEndDateSession());
        String EndTime = "";
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String sday;
        java.util.Date daynum;
        if (range > _day.intValue()) {
            //get day
            if (_day.intValue() == 0) {
                daynum = this.GetStartDateSessionUtil();
                sday = df.format(daynum);
            } else {
                daynum = this.addDays(this.GetStartDateSessionUtil(), _day.intValue());
                sday = df.format(daynum);
            }

            // Call vo public of get ShiftName
            AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
            ps.setWhereClauseParams(null);
            ps.setWhereClause(null);
            ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
            ps.setWhereClauseParams(new Object[] { p_person_id, sday });
            ps.executeQuery();
            StringBuilder st = new StringBuilder();
            if (ps.first() != null) {
                AsPublicSpaceVORowImpl row = (AsPublicSpaceVORowImpl) ps.first();

                st.append(row.getEndTimeHours());
                EndTime = st.toString();
                //ShiftName="On";
                //
            } //end if first is not null
            else { // No Schedule Shift in this Day means it's off//
                EndTime = "00";


            }

        } //end if in range date selection
        // Not in Selected Date Range
        else {

            EndTime = "NA";
        }
        return EndTime;


    }

    public String GetStartEndDateFromSession() {

        // fir testing pruposes//
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");

        java.util.Date st = (java.util.Date) sessionscope.get("TasStartDate");
        String sdt = df.format(st);
        java.util.Date et = (java.util.Date) sessionscope.get("TasEndDate");
        String edt = df.format(et);
        return sdt + "/" + edt;

    }

    public Date GetStartDateSession() {

       
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        java.util.Date uts = (java.util.Date) sessionscope.get("TasStartDate");
        return new oracle.jbo.domain.Date(new java.sql.Date(uts.getTime()));

    }

    public String GetTasShiftStatus(Number _personid, String _employeecode, String _pshiftcode, int _day,
                                    String _pshiftstart, String _pshiftend) {
        String rowstatus = "";
        // java.util.Date shdate= new java.util.Date();
        DBTransaction txn = this.getDBTransaction();
        java.util.Date shdate = this.GetStartDateSessionUtil();
        java.util.Date v = this.addDays(shdate, _day);
        // DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        //            try {
        //           shdate= df.parse(_pshiftdate);
        //            } catch (ParseException e) {
        //                e.printStackTrace();
        //            }

        String sqlStmt = "begin  :1 := AS_SCHDLR_PKG.VALIDATE_TAS_RECORD (:2,:3,:4,:5,:6,:7); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {

            callStmt.registerOutParameter(1, Types.VARCHAR);
            callStmt.setInt(2, _personid.intValue());
            callStmt.setString(3, _employeecode);
            callStmt.setString(4, _pshiftcode);
            callStmt.setDate(5, new java.sql.Date(v.getTime()));
            callStmt.setInt(6, Integer.valueOf(_pshiftstart));
            callStmt.setInt(7, Integer.valueOf(_pshiftend));
            callStmt.execute();
            rowstatus = callStmt.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        return rowstatus;

    }

    public java.util.Date GetStartDateSessionUtil() {


        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        java.util.Date st = (java.util.Date) sessionscope.get("TasStartDate");
        return st;

    }

    public Date GetEndDateSession() {
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        java.util.Date ets = (java.util.Date) sessionscope.get("TasEndDate");
        return new oracle.jbo.domain.Date(new java.sql.Date(ets.getTime()));

    }

    public void TasSearch(Number WorkGroupId, String pfullname)

        {

        AsEmployee_TasVOImpl tas = this.getAsEmployee_TasVO1();
        if (WorkGroupId.intValue() != 0)
            tas.setNamedWhereClauseParam("p_workgroup_id", WorkGroupId);
        else
            tas.setNamedWhereClauseParam("p_workgroup_id", null);
        if (pfullname != null && !pfullname.isEmpty())
            tas.setNamedWhereClauseParam("P_fullname", pfullname);
        else
            tas.setNamedWhereClauseParam("P_fullname", null);
        tas.executeQuery();

    }

    public void SelectAllChecked() {
        AsEmployee_TasVOImpl tas = this.getAsEmployee_TasVO1();
        RowSetIterator rs = tas.createRowSetIterator("newiterar");
        if (rs != null)

        {
            rs.reset();
            while (rs.hasNext()) {
                Row currentrow = rs.next();
                currentrow.setAttribute("SelectRow", Boolean.TRUE);
            } //while close

        } //if close
        rs.closeRowSetIterator();
    }

    public void SelectAllUnChecked() {
        AsEmployee_TasVOImpl tas = this.getAsEmployee_TasVO1();
        // tas.crea
        RowSetIterator rs = tas.createRowSetIterator("newiterar");

        if (rs != null)

        {

            //  rs.reset();
            while (rs.hasNext()) {
                Row currentrow = rs.next();
                currentrow.setAttribute("SelectRow", Boolean.FALSE);

            } //while close

        } //if close
        rs.closeRowSetIterator();
    }
    // Method to get Day and month  for header column of table in tas integration page//
    public String GetHeaderCoulmnDate(int day)

    {

        if (day == 0) {
            return this.DayOfWeek(this.GetStartDateSessionUtil());

        } else

        {
            int range = this.GetDateRange(this.GetStartDateSession(), this.GetEndDateSession());

            if (range > day)

                return this.DayOfWeek(this.addDays(this.GetStartDateSessionUtil(), day));
            else
                return "N";

        }


    }

    public String DayOfWeek(java.util.Date dt) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        // int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_WEEK);
        int dayofmonth = cal.get(Calendar.DAY_OF_MONTH);
        String DayofWeek = "";
        String MonthofYear = "";
        if (day == 1)
            DayofWeek = "Sun";
        else if (day == 2)
            DayofWeek = "Mon";
        else if (day == 3)
            DayofWeek = "Tue";
        else if (day == 4)
            DayofWeek = "Wed";
        else if (day == 5)
            DayofWeek = "Thurs";
        else if (day == 6)
            DayofWeek = "Fri";
        else if (day == 7)
            DayofWeek = "Sat";
        //------------Month-------
        if (month == 0)
            MonthofYear = "Jan";
        else if (month == 1)
            MonthofYear = "Feb";
        else if (month == 2)
            MonthofYear = "March";
        else if (month == 3)
            MonthofYear = "Apr";
        else if (month == 4)
            MonthofYear = "May";
        else if (month == 5)
            MonthofYear = "Jun";
        else if (month == 6)
            MonthofYear = "Jul";
        else if (month == 7)
            MonthofYear = "Aug";
        else if (month == 8)
            MonthofYear = "Sept";
        else if (month == 9)
            MonthofYear = "Oct";
        else if (month == 10)
            MonthofYear = "Nov";
        else if (month == 11)
            MonthofYear = "Dec";
        return String.valueOf(dayofmonth) + "-" + String.valueOf(month + 1);
        // DayofWeek+" "+MonthofYear+" "+String.valueOf(dayofmonth);

    }

    public int GetDateRange(oracle.jbo.domain.Date StartDate, oracle.jbo.domain.Date EndDate) {
        int datediff = 0;
        DBTransaction txn = this.getDBTransaction();
        java.util.Date ActualStartDate = this.addDays(new java.util.Date(StartDate.dateValue().getTime()), -1);
        String sqlStmt = "begin  :1 := AS_SCHDLR_PKG.GetDateDifference (:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {

            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.setDate(2, new java.sql.Date(ActualStartDate.getTime()));
            // callStmt.setDate(2,StartDate.dateValue());
            callStmt.setDate(3, EndDate.dateValue());
            callStmt.execute();
            datediff = callStmt.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        return datediff;
    }

    public String pushDataTas() {
        //  ArrayList<String> result = new ArrayList<String>();
        int count_new;
        int count_nochanges;
        int count_changes;
        int countselected = 0;
        int countna = 0;
        int countnochanges = 0;
        AsEmployee_TasVOImpl vo = this.getAsEmployee_TasVO1();
        Row[] selectedRolesRows = vo.getFilteredRows("SelectRow", true);
        int shiftnamestart = 3;
        int starttimestart = 34;
        int endtimestart = 65;
        int statusstart = 127;
        int countnew = 0;
        int countchanged = 0;
        ///first get selected rows
        for (int ii = 0; ii < selectedRolesRows.length; ii++)


        {
            shiftnamestart = 3;
            starttimestart = 34;
            endtimestart = 65;
            statusstart = 127;

            // and loop through attributes of day if value=NA so it's not in the range
            for (int i = 0; i < 31; i++) {
                if (selectedRolesRows[ii].getAttribute(shiftnamestart).equals("NA"))

                {
                    countna++;
                }

                else {

                    // for each row get personid,employee_number
                    //if it's in the range and status not process no changes so call procedure of insertion
                    // check status not nochanges
                    // in procedue of insertion if it's new will be added to tas mirror table with status new
                    //if it's processed ith changes will be added to mirror with status process changed
                    if (selectedRolesRows[ii].getAttribute(statusstart).equals("NOCHANGES"))

                    {
                        countnochanges++;
                    }

                    else {

                        countselected++;
                        DBTransaction txn = this.getDBTransaction();
                        java.util.Date shdate = this.GetStartDateSessionUtil();
                        // issue here if there is more than one employee selected
                        java.util.Date v = this.addDays(shdate, i);
                        //DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                        String sqlStmt = "begin  AS_SCHDLR_PKG.INSERT_TAS_RECORD (:1,:2,:3,:4,:5,:6,:7,:8); end;";
                        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
                        try {
                            //  1 personid ,2 employeecode,3 shiftcode ,4 shift date ,5 shiftstart,6 shiftend,7 row status,8 off_flag
                            callStmt.setInt(1,
                                            Integer.valueOf(selectedRolesRows[ii].getAttribute("PersonId").toString()));
                            callStmt.setString(2, selectedRolesRows[ii].getAttribute("EmployeeNumber").toString());
                            callStmt.setString(3, selectedRolesRows[ii].getAttribute(shiftnamestart).toString());

                            /////-------------Bug-----------//
                            // need to get date only without time
                            // not using tansient shiftdata it has bug//
                            callStmt.setDate(4, new java.sql.Date(v.getTime()));
                            callStmt.setInt(5,
                                            Integer.valueOf(selectedRolesRows[ii].getAttribute(starttimestart).toString()));
                            callStmt.setInt(6,
                                            Integer.valueOf(selectedRolesRows[ii].getAttribute(endtimestart).toString()));
                            if (selectedRolesRows[ii].getAttribute(statusstart).equals("NEW"))

                            {
                                callStmt.setString(7, "NEW");
                                countnew++;
                            } else {
                                callStmt.setString(7, "CHANGES");
                                countchanged++;
                            }
                            if (selectedRolesRows[ii].getAttribute(shiftnamestart).equals("Off"))
                                callStmt.setString(8, "Off");
                            else
                                callStmt.setString(8, "On");

                            callStmt.execute();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        } finally {
                            if (callStmt != null) {
                                try {
                                    callStmt.close();
                                } catch (SQLException ex) {
                                    // TODO
                                }
                            }
                        }


                    } //end  if status is new or changes//


                } ///end if in range
                shiftnamestart++;
                starttimestart++;
                endtimestart++;
                // shiftdatestart++;
                statusstart++;

            } //end for loop on the 31 day to check which days are on range
            // countselected++;

        }
        // return GetStartDateSession().toString();
        return "Total Rows Pushed to Tas (" + String.valueOf(countselected) + ") ," + String.valueOf(countchanged) +
               " Changed Records , " + String.valueOf(countnew) + " New Records ";

        // return String.valueOf(countselected)+"----"+String.valueOf(countna)+"----"+String.valueOf(countchanges);
        //


    }

    ////-------------------End Tas Integration -------------//

    // Add 12-September ---for page of Schduler After changing layout from calended to table
    // Added By Heba //
    public Date getStartWeekDaySchedule(Date SchedulerStartDate) {

        Date startWeekDate = new Date();
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  :1 := AS_SCHDLR_PKG.getFirstdayofweekschedule (:2); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {

            callStmt.registerOutParameter(1, Types.DATE);
            callStmt.setDate(2, SchedulerStartDate.dateValue());


            callStmt.execute();
            startWeekDate = new oracle.jbo.domain.Date(callStmt.getDate(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }

        return startWeekDate;


    }

    public String getemployeeScheduleShift(Number PersonId, String StartDate, int Daynum) {
        StringBuilder st = new StringBuilder();
        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        if (Daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, Daynum));
        }
        AsPublicSpaceVOImpl ps = this.getAsPublicSpaceVO1();
        ps.setWhereClauseParams(null);
        ps.setWhereClause(null);
        ps.setWhereClause("person_id=:1 and schedular_start_date=:2 ");
        ps.setWhereClauseParams(new Object[] { PersonId, WeekDay });
        ps.executeQuery();
        if (ps.first() != null) {
            AsPublicSpaceVORowImpl row = (AsPublicSpaceVORowImpl) ps.first();

            st.append(row.getShiftName());
            st.append("(");
            int ShiftStartTime = row.getStartTimeHours().intValue();
            int ShiftEndTime = row.getEndTimeHours().intValue();
            if (ShiftStartTime < 12 || ShiftStartTime == 24) {
                st.append(row.getStartTimeHours()).append(" ").append("AM");
            } else
                st.append(row.getStartTimeHours()).append(" ").append("PM");

            st.append("-");
            if (ShiftEndTime < 12 || ShiftEndTime == 24) {

                st.append(row.getEndTimeHours()).append(" ").append("AM");
            } else {
                st.append(row.getEndTimeHours()).append(" ").append("PM");
            }
            st.append(")");


            return st.toString();
        } else
            return "";


    }
    ///------------End Added By Heba //

    ////----- Added By Heba --- 17-October-2017 For News Room Schedule //////

    public void createShiftAttributeRow(String SchedulerShiftId, String ShiftId, String ShiftName,String wkId) {
        AsShiftAttributesVOImpl vo = this.getAsShiftAttributesVO2();
        AsShiftAttributesVORowImpl r = (AsShiftAttributesVORowImpl) vo.createRow();
        //       int  x=Integer.parseInt(SchedulerShiftId);
        //       oracle.jbo.domain.Number n=;
        r.setFkSchedulerShiftId(new oracle.jbo.domain.Number(Integer.parseInt(SchedulerShiftId)));
        r.setFkShiftId(new oracle.jbo.domain.Number(Integer.parseInt(ShiftId)));
        r.setShiftAttributeName(ShiftName);
        r.setFkWorkGroupId(new oracle.jbo.domain.Number(Integer.parseInt(wkId)));
        vo.insertRow(r);
        this.getDBTransaction().commit();
        this.getAsSchedulerShiftsVO().executeQuery();
        vo.executeQuery();

    }
    ///----- NewsRoom Section ----
//    public String getNewRoomSearchDate(String pstart_Date)
//    {
//        return pstart_Date;
//        
//        }
    public void ScheduleNewsRoomsearch(String pstart_Date)

    {
        
       // this.getNewRoomSearchDate(pstart_Date);
       // this.getAsSc
//        AsShiftAttributesVOImpl pr = (AsShiftAttributesVOImpl) this.getAsShiftAttributesVO2();
//        pr.setp1("27-March-2018");
//    //  pr.setNamedWhereClauseParam("p1","27-Mar");
//       // pr.setPStartDate(pstart_Date);
  //  this.getAsShiftAttributesVO2().executeQuery();
       // this.getAsSchedulerShiftsVO().getPStartDate()
        
//        AsSchedulerShiftsVOImpl pr = (AsSchedulerShiftsVOImpl) this.getAsSchedulerShiftsVO();
//      
//                                                                                                 
//      pr.setPStartDate(pstart_Date);
////        AsShiftAttributesVOImpl st = (AsShiftAttributesVOImpl) this.getAsShiftAttributesVO2();
////        st.setNamedWhereClauseParam("p1","27-Mar-2018");
//               this.getAsShiftAttributesVO2().executeQuery();
    }

    public String getShiftDayNewsRoom(Number SchdeulerId, Number ShiftAttributeId, int daynum) {

//        AsSchedulerShiftsVOImpl pr = (AsSchedulerShiftsVOImpl) this.getAsSchedulerShiftsVO();
//
//        String StartDate = pr.getPStartDate();
//        AsSchedulerStaffShiftTableVOImpl vo = this.getAsSchedulerStaffShiftTableVO();
//        vo.setWhereClauseParams(null);
//        vo.setWhereClause(null);
//        // DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//        // and schedular_start_date=:2
//        java.util.Date sDate = new java.util.Date();
//        String WeekDay = "";
//        if (daynum == 0) {
//            WeekDay = StartDate;
//        } else {
//            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//            // get start date as date from string start date
//            try {
//                sDate = df.parse(StartDate);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            WeekDay = df.format(this.addDays(sDate, daynum));
//        }
//        vo.setWhereClause("FK_SCHDLR_SHIFT_ATTRIBUTE_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
//        vo.setWhereClauseParams(new Object[] { ShiftAttributeId, WeekDay, SchdeulerId });
//        vo.executeQuery();
//        if (vo.first() != null) {
//
//            AsSchedulerStaffShiftTableVORowImpl r = (AsSchedulerStaffShiftTableVORowImpl) vo.first();
//            return r.getEmployeeName();
//        }
//        //000000000000000
        return " ";
    }

    public String getShiftBulletinNotesNewsRoom(Number SchdeulerId, Number ShiftAttributeId, int daynum) {

//        AsSchedulerShiftsVOImpl pr = (AsSchedulerShiftsVOImpl) this.getAsSchedulerShiftsVO();
//
//        String StartDate = pr.getPStartDate();
//        AsSchedulerStaffShiftTableVOImpl vo = this.getAsSchedulerStaffShiftTableVO();
//        vo.setWhereClauseParams(null);
//        vo.setWhereClause(null);
//        java.util.Date sDate = new java.util.Date();
//        String WeekDay = "";
//        if (daynum == 0) {
//            WeekDay = StartDate;
//        } else {
//            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//            // get start date as date from string start date
//            try {
//                sDate = df.parse(StartDate);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            WeekDay = df.format(this.addDays(sDate, daynum));
//        }
//        vo.setWhereClause("FK_SCHDLR_SHIFT_ATTRIBUTE_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
//        vo.setWhereClauseParams(new Object[] { ShiftAttributeId, WeekDay, SchdeulerId });
//        vo.executeQuery();
//        if (vo.first() != null) {
//
//            AsSchedulerStaffShiftTableVORowImpl r = (AsSchedulerStaffShiftTableVORowImpl) vo.first();
//            // First Get Bulletins
//            String SchedulerBulletin = getNesRoomShiftBulletins(r.getSchedulerStaffId());
//            // return r.getNotes();
//            String Note = r.getNotes();
//
//            if (SchedulerBulletin.equals("None")) {
//                if (Note.equals("N"))
//                    return "";
//                else
//                    return Note;
//            } else
//
//            {
//                if (Note.equals("N"))
//                    return SchedulerBulletin;
//                else
//                    return SchedulerBulletin.concat(" | ").concat(r.getNotes());
//
//
//            }
//        }
//        //000000000000000
        return " ";
    }

    public String getNesRoomShiftBulletins(Number Scedular_staff_id)

    {
        StringBuilder Bulletins = new StringBuilder();

        AsPublicSpaceBulletinsVOImpl b = this.getAsPublicSpaceBulletinsVO1();
        b.setWhereClauseParams(null);
        b.setWhereClause(null);
        b.setNamedWhereClauseParam("p_schedular_staff_id", Scedular_staff_id);
        b.executeQuery();
        int count = b.getRowCount();
        if (count > 0) {
            Bulletins.append("(");
            for (int i = 1; i <= count; i++) {
                AsPublicSpaceBulletinsVORowImpl r = (AsPublicSpaceBulletinsVORowImpl) b.next();

                Bulletins.append(r.getBulletinName());

                if (i != count) {
                    Bulletins.append("/");
                    Bulletins.append("");
                }

            }

            Bulletins.append(")");
        } else
            Bulletins.append("None");
        return Bulletins.toString();

    }

    public String getShiftColorNewsRoom(Number SchdeulerId, Number ShiftAttributeId, int daynum) {

//        String color = "transparent";
//        AsSchedulerShiftsVOImpl pr = (AsSchedulerShiftsVOImpl) this.getAsSchedulerShiftsVO();
//        String StartDate = pr.getPStartDate();
//
//        //AcSchedulerFirstApprovalLeaveVOImpl vo=this.getAcSchedulerFirstApprovalLeaveVO1();
//        AsSchedulerStaffShiftTableVOImpl vo = this.getAsSchedulerStaffShiftTableVO();
//        vo.setWhereClauseParams(null);
//        vo.setWhereClause(null);
//
//        java.util.Date sDate = new java.util.Date();
//        String WeekDay = "";
//        if (daynum == 0) {
//            WeekDay = StartDate;
//        } else {
//            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//            // get start date as date from string start date
//            try {
//                sDate = df.parse(StartDate);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            WeekDay = df.format(this.addDays(sDate, daynum));
//        }
//        vo.setWhereClause("FK_SCHDLR_SHIFT_ATTRIBUTE_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
//        vo.setWhereClauseParams(new Object[] { ShiftAttributeId, WeekDay, SchdeulerId });
//        vo.executeQuery();
//        if (vo.first() != null) {
//
//            //  AcSchedulerFirstApprovalLeaveVORowImpl r=( AcSchedulerFirstApprovalLeaveVORowImpl)vo.first();
//            AsSchedulerStaffShiftTableVORowImpl r = (AsSchedulerStaffShiftTableVORowImpl) vo.first();
//            color = r.getStatusColorNr();
//
//            //r.getStatusColor();
//        }
//        return color;
        
        return "";
    }
    ///------ End NewsRoom Section -----//
    ///---- End Added By Heba ------//

    public String authenticateUser(String userName, String password) {
        String retValue = "N";
        // added By Heba 21-Feb-2018//
        // this.getAsUserRolesVO()
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin :1 := APPS.FND_WEB_SEC.VALIDATE_LOGIN (:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.registerOutParameter(1, Types.VARCHAR);
            callStmt.setString(2, userName);
            callStmt.setString(3, password);
            callStmt.execute();
            retValue = callStmt.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }

    public HashMap initLogonEmployee(String userName) {
        HashMap retValue = new HashMap<String, Object>();

        ViewObject usersVO = this.getUsersVO();
        usersVO.setNamedWhereClauseParam("UserNamePar", userName);
        usersVO.executeQuery();
        if (usersVO.getRowCount() == 0) {
            retValue.put("ValidUser", 0);
        } else {
            retValue.put("ValidUser", 1);
            Row employeeRow = usersVO.first();

            for (AttributeDef attrDef : usersVO.getAttributeDefs()) {
                String attrName = attrDef.getName();
                Object attrValue = employeeRow.getAttribute(attrName);
                retValue.put(attrName, attrValue);
            }
        }
        return retValue;
    }

    public String repeatPattern(Number schedulerStaffId,Date schedulerdate,Date schedulerenddate ) {
        String retValue = "FAIL";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.REPEAT_PATTERN_RANGE (:1,:2,:3,:4); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerStaffId.doubleValue());
            callStmt.setDate(2, schedulerdate.dateValue());
            callStmt.setDate(3, schedulerenddate.dateValue());
            callStmt.registerOutParameter(4, Types.VARCHAR);
            callStmt.execute();
            retValue = callStmt.getString(4);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }

    public String validateScheduler(Number schedulerId) {
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.VALIDATE_SCHEDULER (:1,:2); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerId.doubleValue());
            callStmt.registerOutParameter(2, Types.VARCHAR);
            callStmt.execute();
            retValue = callStmt.getString(2);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        return retValue;
    }

    public void deleteSchdlr(Number schedulerStaffId,String Type,Number PersonID  ) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.DELETE_SCHDLR (:1,:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerStaffId.doubleValue());
            callStmt.setString(2, Type);
            callStmt.setDouble(3, PersonID.doubleValue());
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }

    public void deleteAllSchdlr(Number schedulerStaffId , Date schedulerdate,  Date schedulerenddate)
       {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.DELETE_SCHDLR_RANGE (:1,:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerStaffId.doubleValue());
            callStmt.setDate(2, schedulerdate.dateValue());
            callStmt.setDate(3, schedulerenddate.dateValue());
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }

    public void deleteAllSchdlrPresenter(Number schedulerStaffId , Date schedulerdate, Date schedulerenddate) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.DELETE_RANGE_SCHDLR_PRSNTR (:1,:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerStaffId.doubleValue());
            callStmt.setDate(2, schedulerdate.dateValue());
            callStmt.setDate(3, schedulerenddate.dateValue());
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }
    // Added By Heba 19-Nov-2017


    public String repeatPatternNewsRoom(Number schedulerStaffId, Date schedulerdate, Date schedulerenddate,
                                        Number fkshiftid, Number fkshiftattrbuteid) {
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.REPEATRANGE_PATTERNNEWSROOM (:1,:2,:3,:4,:5,:6,:7); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerStaffId.doubleValue());
            callStmt.setDate(2, schedulerdate.dateValue());
            callStmt.setDate(3, schedulerenddate.dateValue());
            callStmt.setInt(4, fkshiftid.intValue());
            callStmt.setInt(5, fkshiftattrbuteid.intValue());
            callStmt.setInt(6, this.getLoggedPersonId().intValue());
            callStmt.registerOutParameter(7, Types.VARCHAR);
            callStmt.execute();
            retValue = callStmt.getString(7);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }


    public String repeatPatternPresenter(Number schedulerStaffId, Date schedulerdate, Date schedulerenddate,
                                         Number fksectionId) {
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.REPEATRANGE_PATTERNPRESENTER (:1,:2,:3,:4,:5); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerStaffId.doubleValue());
            callStmt.setDate(2, schedulerdate.dateValue());
            callStmt.setDate(3, schedulerenddate.dateValue());
            callStmt.setInt(4, fksectionId.intValue());
            callStmt.registerOutParameter(5, Types.VARCHAR);
            callStmt.execute();
            retValue = callStmt.getString(5);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }

    public void deleteAllSchdlrNewsRoom(Number schedulerStaffId,Date schedulerdate, Date schedulerenddate) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.DELETE_ALL_SCHDLR_NROM_RANGE (:1,:2,:3,:4); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerStaffId.doubleValue());
            callStmt.setDate(2, schedulerdate.dateValue());
            callStmt.setDate(3, schedulerenddate.dateValue());
            callStmt.setDouble(4, this.getLoggedPersonId().doubleValue());
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }

    public int deleteShiftAttibute(Number ShiftAttributeId) {
        AsSchedulerStaffVOImpl vo = this.getAsSchedulerStaffVO1();
        vo.setWhereClause("FK_SCHDLR_SHIFT_ATTRIBUTE_ID=:1");
        vo.setWhereClauseParams(new Object[] { ShiftAttributeId });
        vo.executeQuery();
        if (vo.first() != null) {
            return 1;
        } else {
            AsShiftAttributesVOImpl shiftattr = this.getAsShiftAttributesVO1();
            shiftattr.setWhereClause("SHIFT_ATTRIBUTE_ID=:1");
            shiftattr.setWhereClauseParams(new Object[] { ShiftAttributeId });
            shiftattr.executeQuery();
            AsShiftAttributesVORowImpl r = (AsShiftAttributesVORowImpl) shiftattr.first();
            r.remove();
            this.getDBTransaction().commit();
            return 0;
        }
    }


    public void deleteScheduler(Number schedulerId) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.DELETE_AS_SCHEDULER (:1); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerId.doubleValue());
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }


    ///---- End Added By Heba 19-11-2017----//


    public String schdlrExistsOnDate(Number schedulerId, Number personId, Date schdlrStartDate) {
        String retValue = "N";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  :1 := AS_SCHDLR_PKG.GET_SCHDLR_COUNT_ON_DATE (:2,:3 ,:4); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.registerOutParameter(1, Types.NUMERIC);
            callStmt.setDouble(2, schedulerId.doubleValue());
            callStmt.setDouble(3, personId.doubleValue());
            callStmt.setDate(4, schdlrStartDate.dateValue());

            callStmt.execute();
            double schdlrCount = callStmt.getDouble(1);
            if (schdlrCount == 0) {
                retValue = "N";
            } else {
                retValue = "Y";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }

    public HashMap getSchdlrAssignIntesection(Number schedulerId, Number personId, Date schdlrStartDate) {
        HashMap retValue = new HashMap();
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin   AS_SCHDLR_PKG.SCHDLR_INTERSECT_ASSIGNMENT (:1 ,:2 , :3 , :4 ,:5); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerId.doubleValue());
            callStmt.setDouble(2, personId.doubleValue());
            callStmt.setDate(3, schdlrStartDate.dateValue());
            callStmt.registerOutParameter(4, Types.VARCHAR);
            callStmt.registerOutParameter(5, Types.VARCHAR);
            callStmt.execute();
            String status = callStmt.getString(4);
            String statusMessage = callStmt.getString(5);
            System.out.println("status=" + status + " ,statusMessage=" + statusMessage);
            retValue.put("STATUS", status);
            retValue.put("STATUS_MESSAGE", statusMessage);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        return retValue;
    }

    public HashMap getSchdlrLeaveIntesection(Number schedulerId, Number personId, Date schdlrStartDate) {
        HashMap retValue = new HashMap();
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin   AS_SCHDLR_PKG.SCHDLR_INTERSECT_LEAVE (:1 ,:2 , :3 , :4 ,:5); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerId.doubleValue());
            callStmt.setDouble(2, personId.doubleValue());
            callStmt.setDate(3, schdlrStartDate.dateValue());
            callStmt.registerOutParameter(4, Types.VARCHAR);
            callStmt.registerOutParameter(5, Types.VARCHAR);
            callStmt.execute();
            String status = callStmt.getString(4);
            String statusMessage = callStmt.getString(5);
            System.out.println("status=" + status + " ,statusMessage=" + statusMessage);
            retValue.put("STATUS", status);
            retValue.put("STATUS_MESSAGE", statusMessage);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        return retValue;
    }

    public void submitSchdlrTrans(Number schedulerTransHdrId, Number personId) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.SUBMIT_SCHDLR_TRANS (:1,:2); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerTransHdrId.doubleValue());
            callStmt.setDouble(2, personId.doubleValue());
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }

    public void applySchdlrTrans(Number schedulerTransHdrId) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.APPLY_SCHDLR_TRANS (:1,:2,:3 ); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerTransHdrId.doubleValue());
            callStmt.registerOutParameter(2, Types.NUMERIC);
            callStmt.registerOutParameter(3, Types.VARCHAR);
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }

    public void submitScheduler(Number schedulerId, Number personId) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.SUBMIT_SCHEDULER (:1,:2 ,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerId.doubleValue());
            callStmt.setDouble(2, personId.doubleValue());
            callStmt.registerOutParameter(3, Types.VARCHAR);
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }

    public HashMap getWorklistCount(Number personId) {
        HashMap retValue = new HashMap();
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin AS_SCHDLR_PKG.GET_WORKLIST_COUNT(:1 ,:2 , :3 , :4); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, personId.doubleValue());
            callStmt.registerOutParameter(2, Types.NUMERIC);
            callStmt.registerOutParameter(3, Types.NUMERIC);
            callStmt.registerOutParameter(4, Types.NUMERIC);
            callStmt.execute();
            retValue.put("APPROVED", new Number(callStmt.getDouble(2)));
            retValue.put("REJECTED", new Number(callStmt.getDouble(3)));
            retValue.put("OPEN", new Number(callStmt.getDouble(4)));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        return retValue;
    }
    
    public Number getFirstWorkGroup()
    {
        
        DefaultWorkGroupvaluevoImpl v=   this.getDefaultWorkGroupvaluevo1();
        DefaultWorkGroupvaluevoRowImpl r=(DefaultWorkGroupvaluevoRowImpl)v.first();
       return r.getWorkGroupId();
        }

    public void worklistAction(Number worklistId, String actionTaken, String notes) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.WORKLIST_ACTION (:1,:2,:3 ); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, worklistId.doubleValue());
            callStmt.setString(2, actionTaken);
            callStmt.setString(3, notes);
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }


    /////----- Presenter Layout  Added By Heba 24-1-2018------///


    public String getShiftBulletinPresenter(Number SchdeulerId, Number SectionId, int daynum) {

        AsSchedulerSectionsVOImpl pr = (AsSchedulerSectionsVOImpl) this.getAsSchedulerSectionsVO2();

        String StartDate = pr.getPStart_Date();
        AsSchedulerStaffVOImpl vo = this.getAsSchedulerStaffVO1();
        vo.setWhereClauseParams(null);
        vo.setWhereClause(null);
        // DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        // and schedular_start_date=:2
        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        if (daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, daynum));
        }
        vo.setWhereClause("FK_SCHEDULER_SECTION_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
        vo.setWhereClauseParams(new Object[] { SectionId, WeekDay, SchdeulerId });
        vo.executeQuery();
        StringBuilder employeedata = new StringBuilder();
        if (vo.first() != null) {

            AsSchedulerStaffVORowImpl r = (AsSchedulerStaffVORowImpl) vo.first();
            if (r.getDefaultshift().equals("S0930E1830")) {
                employeedata.append(r.getEmployeeName());
            } else {
                employeedata.append(r.getEmployeeName() + "-" + r.getDefaultshift());
            }
            employeedata.append("-").append(getPresenterBulletins(r.getSchedulerStaffId()));
            return employeedata.toString();

        }

        //000000000000000
        else
            return " ";

    }

    public String getEmployeePresenterColor(Number SchdeulerId, Number SectionId, int daynum) {
        String color = "transparent";
        AsSchedulerSectionsVOImpl pr = (AsSchedulerSectionsVOImpl) this.getAsSchedulerSectionsVO2();

        String StartDate = pr.getPStart_Date();
        //  AcSchedulerFirstApprovalLeaveVOImpl vo=this.getAcSchedulerFirstApprovalLeaveVO1();
        AsSchedulerStaffVOImpl vo = this.getAsSchedulerStaffVO1();
        vo.setWhereClauseParams(null);
        vo.setWhereClause(null);
        // DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        // and schedular_start_date=:2
        java.util.Date sDate = new java.util.Date();
        String WeekDay = "";
        if (daynum == 0) {
            WeekDay = StartDate;
        } else {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            // get start date as date from string start date
            try {
                sDate = df.parse(StartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            WeekDay = df.format(this.addDays(sDate, daynum));
        }
        vo.setWhereClause("FK_SCHEDULER_SECTION_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
        vo.setWhereClauseParams(new Object[] { SectionId, WeekDay, SchdeulerId });
        vo.executeQuery();
        StringBuilder employeedata = new StringBuilder();
        if (vo.first() != null) {

            // AcSchedulerFirstApprovalLeaveVORowImpl r=(AcSchedulerFirstApprovalLeaveVORowImpl)vo.first();
            AsSchedulerStaffVORowImpl r = (AsSchedulerStaffVORowImpl) vo.first();
            color = r.getStatusColor();
            // color=r.getStatusColorNr();

        }

        return color;

    }

    public String getPresenterBulletins(Number fk_Scheduler_staff_id) {


        AsPublicSpaceBulletinsVOImpl vo = this.getAsPublicSpaceBulletinsVO1();
        vo.setWhereClauseParams(null);
        vo.setWhereClause(null);

        vo.setNamedWhereClauseParam("p_schedular_staff_id", fk_Scheduler_staff_id);
        //b.setWhereClauseParams(new Object[]{Scedular_staff_id});
        vo.executeQuery();
        int count = vo.getRowCount();
        StringBuilder bt = new StringBuilder();
        if (count > 0) {
            bt.append("(");
            for (int i = 1; i <= count; i++)

            {
                AsPublicSpaceBulletinsVORowImpl r = (AsPublicSpaceBulletinsVORowImpl) vo.next();
                bt.append(r.getBulletinName());
                bt.append("");
                if (i != count) {
                    bt.append("/");
                    bt.append("");
                }
            }
            bt.append(" )");
        }

        return bt.toString();
    }

//    public int createTemplateSectionRow(Number templateId, String Sdate)
//
//        {
//
//        // First Delete any data in Scheduler Section in the Selected Date
//        int result = 1;
//        String createrows = "Y";
//
//        AsSchedulerSectionsVOImpl vo = this.getAsSchedulerSectionsVO2();
//        int tempemployeecount = Integer.valueOf(((AsTemplateEmployeeVORowImpl)this.getAsTemplateEmployeeVO1().last()).getOrderNum().toString());
//        int schedulersectioncount = this.getAsSchedulerSectionsVO2().getRowCount();
//        Number mxorder = new Number(1);
//        if (schedulersectioncount > 0) {
//
//            mxorder = ((AsSchedulerSectionsVORowImpl) vo.last()).getOrderId();
//            if (Integer.valueOf(mxorder.toString()) != schedulersectioncount) {
//
//                createrows = "N";
//                result = 0;
//            } 
//          
//        } //close of has Scheduler Sections
//
//
//        // Second Check that template employeesrow equal or less than section rows
//
//
//        if (createrows.equals("Y")) {
//            
//            if (tempemployeecount > schedulersectioncount) {
//
//                // call method  to create the difference between rows
//                int diffsec = tempemployeecount - schedulersectioncount;
//                for (int i = 0; i < diffsec; i++) {
//                    createSectionRow();
//
//                }
//
//            }
//            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//            try {
//            oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(getSqlDateFromUtilDate(df.parse(Sdate)));
//            DeleteSchedulerStaffRow(jboDate);
//            }
//            catch (ParseException e) {}
//            // Third Apply Template
//   // create rows of template employees-bulletins
//          CreateTemplateSchedulerStaff(Sdate);
//          vo.executeQuery();
//        }
//
//
//      
//     
//        return result;
//
//
//    }
    
    
        public void loadTemplatePresenter(Number schedulerId,String SchedulerTitle,Number TemplateId,Date schedulerdate) {
        DBTransaction txn = this.getDBTransaction();
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        Number personId = (Number) sessionscope.get("AAPersonId");
        String sqlStmt = "begin  AS_SCHDLR_PKG.LOAD_TEMPLATE_BULLETINS (:1,:2,:3,:4,:5); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
           
            callStmt.setDouble(1,schedulerId.doubleValue());
            callStmt.setString(2,SchedulerTitle);
            callStmt.setDouble(3,TemplateId.doubleValue());
            callStmt.setDouble(4,personId.doubleValue());
            callStmt.setDate(5,schedulerdate.dateValue());
            callStmt.execute();
            this.getAsSchedulerSectionsVO2().executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }

    public static java.sql.Date getSqlDateFromUtilDate(java.util.Date utilDate) {
        return new java.sql.Date(utilDate.getTime());
    }

    public void CreateTemplateSchedulerStaff(String Sdate) {
        Row scheduler = this.getAsSchedulersVO().getCurrentRow();
        Number schedulerId = (Number) scheduler.getAttribute("SchedulerId");
        RowSetIterator rsection = this.getAsSchedulerSectionsVO2().createRowSetIterator(null);
        rsection.reset();
        Row currsect = null;
        while (rsection.hasNext())
        {
            //check if template employees has row with the current section order //
            currsect = rsection.next();
            Number OrderId=(Number) currsect.getAttribute("OrderId");
            Number TemplateId=((AsTemplatesVORowImpl)this.getAsTemplatesVO1().getCurrentRow()).getTemplateId();
            Number WorkGroupId=((AsTemplatesVORowImpl)this.getAsTemplatesVO1().getCurrentRow()).getFkWorkGroupId();
            AsTemplateEmployeeVOImpl vo=this.getAsTemplateEmployeeVO2();
             vo.setWhereClause(null);
            vo.setWhereClauseParams(null);
            vo.setWhereClause("FK_TEMPLATE_ID=:1 and ORDER_NUM=:2 ");
            vo.setWhereClauseParams(new Object[] { TemplateId,OrderId  });
            vo.executeQuery();
             if(vo.first()!=null)
             {
              
               AsTemplateEmployeeVORowImpl emptemp=(AsTemplateEmployeeVORowImpl)vo.first();
               AsSchedulerStaffVOImpl staff = this.getAsSchedulerStaffCurrentVO();
               AsSchedulerStaffVORowImpl r = (AsSchedulerStaffVORowImpl) staff.createRow();
               DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                try {
                         oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(getSqlDateFromUtilDate(df.parse(Sdate)));
                         r.setSchedulerId(schedulerId);
                         r.setSchedularStartDate(jboDate);
                        r.setSchedularEndDate(jboDate);
                    r.setWorkGroupId(WorkGroupId);
                    if(emptemp.getPersonId()!=null)
                         r.setPersonId(emptemp.getPersonId());
                         r.setFkSchedulerSectionId((Number) currsect.getAttribute("SectionId"));
                         staff.insertRow(r);
                         createTemplateBulletin(r.getSchedulerStaffId());
                    //add bulletin of each template employee row
                    //
                     } 
                     catch (ParseException e) {}


                     
                
                 }// close of vo first //
             
          
         
        } // while close //
       
        rsection.closeRowSetIterator();
        this.getDBTransaction().commit();


    }
     public void createTemplateBulletin( Number Scheduler_staff_id )
     
     {
         AsTemplateEmpBulletinsVOImpl empbulletin=   this.getAsTemplateEmpBulletinsVO3();
         RowSetIterator rsi=empbulletin.createRowSetIterator(null);
         while (rsi.hasNext())
         
         {
         Row row=rsi.next();
        createBulletinRow(Scheduler_staff_id, (Number)row.getAttribute("BulletinId"));
             
         }
         rsi.closeRowSetIterator();
         
         }
    public void  DeleteSchedulerStaffRow(Date SchdeulerStaffDate) {
        // get the selected date and select from scheduler staff where schedulerid and selected date
        //delete all data
        //refresh schedulersection

        Row scheduler = this.getAsSchedulersVO().getCurrentRow();
        Number schedulerId = (Number) scheduler.getAttribute("SchedulerId");
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.DELETE_SCHDLR_STAFF_FROM_TEMP(:1,:2); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerId.doubleValue());
            callStmt.setDate(2, SchdeulerStaffDate.dateValue());
       
            callStmt.execute();
       
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        




    }

    public void createTemplateEmployeeRow()

    {

        AsTemplateEmployeeVOImpl vo = this.getAsTemplateEmployeeVO1();
        Number mxorder = new Number(1);
        try {
            mxorder = ((AsTemplateEmployeeVORowImpl) vo.last()).getOrderNum().add(new Number(1));
        } catch (NullPointerException ex) {


        }
        AsTemplateEmployeeVORowImpl r = (AsTemplateEmployeeVORowImpl) vo.createRow();
        r.setOrderNum(mxorder);
        vo.insertRow(r);
        this.getDBTransaction().commit();
        vo.executeQuery();

    }

    public void createSectionRow() {

        AsSchedulerSectionsVOImpl vo = this.getAsSchedulerSectionsVO2();
        Number mxorder = new Number(1);
        try {
            mxorder = ((AsSchedulerSectionsVORowImpl) vo.last()).getOrderId().add(new Number(1));
        } catch (NullPointerException ex) {
        }

        AsSchedulersVOImpl sch = this.getAsSchedulersVO();
        AsSchedulersVORowImpl rs = (AsSchedulersVORowImpl) this.getAsSchedulersVO().getCurrentRow();
        AsSchedulerSectionsVORowImpl r = (AsSchedulerSectionsVORowImpl) vo.createRow();
        r.setSectionName(rs.getSchedulerTitle());
        r.setOrderId(mxorder);
        vo.insertRow(r);
        this.getDBTransaction().commit();

        vo.executeQuery();

    }
    
    public void ExceuteTemplateVo()
    {
        
         //to refresh the template vo to get all data  with out scheduler id sorting
   AsTemplatesVOImpl vo=     this.getAsTemplatesVO1();
   vo.setWhereClause(null);
   vo.setWhereClauseParams(null);
   vo.executeQuery();
        
        
        }

   public void createSectionRowOrder() {
        // Will be Implemented Later //
     
       
        AsSchedulersVOImpl sch = this.getAsSchedulersVO();
        AsSchedulersVORowImpl rs = (AsSchedulersVORowImpl) this.getAsSchedulersVO().getCurrentRow();
        // Get Current Row and get it''s order 
        AsSchedulerSectionsVOImpl vo = this.getAsSchedulerSectionsVO2();
        AsSchedulerSectionsVORowImpl currentsection=  (AsSchedulerSectionsVORowImpl) vo.getCurrentRow();
        AsSchedulerSectionsVORowImpl r = (AsSchedulerSectionsVORowImpl) vo.createRow();
        ///set new order with current row order 
        Number maxorder=currentsection.getOrderId().add(1);
        Number CurrentSectionID=currentsection.getSectionId();
        r.setFkschedulerId(rs.getSchedulerId());
        r.setSectionName(rs.getSchedulerTitle());
        r.setOrderId(currentsection.getOrderId());
        // set current order +1
        currentsection.setOrderId(maxorder);
        vo.insertRow(r);
        RowSetIterator rst = vo.createRowSetIterator("sectionrows");
        if (rs != null)

        {
            rst.reset();
            while (rst.hasNext()) {
                Row currentrow = rst.next();
                if((currentrow.getAttribute("SectionId")!=CurrentSectionID) &&  (Integer.valueOf(currentrow.getAttribute("OrderId").toString())>=maxorder.intValue()))
                {
                  Number  CurrentOrder =(Number) currentrow.getAttribute("OrderId");
                    currentrow.setAttribute("OrderId", CurrentOrder.add(1));
                    }
               // currentrow.setAttribute("SelectRow", Boolean.TRUE);
            } //while close

        } //if close
        rst.closeRowSetIterator();
        this.getDBTransaction().commit();
        vo.executeQuery();

    }

    public void createBulletinRow(Number fkscheduler_staff_id, Number fkBulletinID) {
        AsSchedulerStaffBulletinsVOImpl vo = this.getAsSchedulerStaffBulletinsVO2();
        AsSchedulerStaffBulletinsVORowImpl r = (AsSchedulerStaffBulletinsVORowImpl) vo.createRow();

        r.setSchedulerStaffId(fkscheduler_staff_id);
        r.setBulletinId(fkBulletinID);
        vo.insertRow(r);
        //this.getDBTransaction().commit();

        // vo.executeQuery();

    }

    public void SchedulePresenterSearch(String pstart_Date)

    {


        AsSchedulerSectionsVOImpl pr = (AsSchedulerSectionsVOImpl) this.getAsSchedulerSectionsVO2();

       // pr.setPStart_Date(pstart_Date);
       pr.setNamedWhereClauseParam("p1", pstart_Date);
        this.getAsSchedulerSectionsVO2().executeQuery();


        // this.getAsShiftAttributesVO2().executeQuery();
    }


    public int deleteSchedulerSection(Number SectionId) {

        AsSchedulerStaffVOImpl vo = this.getAsSchedulerStaffVO1();
        vo.setWhereClause("FK_SCHEDULER_SECTION_ID=:1");
        vo.setWhereClauseParams(new Object[] { SectionId });
        vo.executeQuery();
        if (vo.first() != null) {
            return 1;
        } else {
            AsSchedulerSectionsVOImpl section = this.getAsSchedulerSectionsVO1();
            section.setWhereClause("SECTION_ID=:1");
            section.setWhereClauseParams(new Object[] { SectionId });
            section.executeQuery();
            AsSchedulerSectionsVORowImpl r = (AsSchedulerSectionsVORowImpl) section.first();
            r.remove();
            this.getDBTransaction().commit();
            return 0;
        }
    }

    ////------End Presenter Layout ----///
    ////------Notification -------////

    public void UpdateWorkGroupNotification() {
        AsWorklistVORowImpl r = (AsWorklistVORowImpl) this.getAsWorklistVO().getCurrentRow();
        r.setTakeAction("NOTIFIED");
        r.setWorklistStatus("VIEWED");
        this.getDBTransaction().commit();

    }

    public void LoadWorkGroupNotification()

    {
        // this.getAsWorklistVO()

        AsWorklistVORowImpl rs = (AsWorklistVORowImpl) this.getAsWorklistVO().getCurrentRow();
        Number Notificationid = rs.getNotificationOfValidationId();
        AsWorkgroupNotificationsVOImpl vo = this.getAsWorkgroupNotificationsVO1();
        vo.setWhereClauseParams(null);
        vo.setWhereClause(null);
        vo.setWhereClause("Version_Id=:1");
        vo.setWhereClauseParams(new Object[] { Notificationid });
        vo.executeQuery();


    }
    ////------Notifications---------///
    /////------SMS-EMAIL///
    public String insertSmsMessageStatus(Number personid,String phone,String statusmessage ) {
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_NOTIFICATIONS_PKG.INSERT_SMS_MESSAGE_STATUS (:1,:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, personid.doubleValue());
            callStmt.setString(2, phone);
            callStmt.setString(3, statusmessage);
            callStmt.execute();
         
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }
    
    
    public String sendemail(String toemail,String employeename,String emailmessage ) {
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_NOTIFICATIONS_PKG.SEND_MAIL (:1,:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setString(1, toemail);
            callStmt.setString(2, employeename);
            callStmt.setString(3, emailmessage);
            callStmt.execute();
         
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }
    
    ////-----------------------Approval Worfklow--------------------------//
    //-----------28-August-2018 Author :Heba Salah ----------------------//
    ///-------As per end Users Request :Pattern/Shifts/Bulletins have to be approved by managers and Erp Admin //
    public void startPatternApproval( )
    {
        
            Map sessionscope = ADFContext.getCurrent().getSessionScope();
            Number OrgId = (Number) sessionscope.get("AAOrganization");
             String userName=(String) sessionscope.get("AAUsername");
            String sqlStmt = "begin  AS_SCDLR_WF_PKG.XXSCDLR_RUN_PATTERN_PROCESS (:1,:2); end;";
            CallableStatement callStmt = this.getDBTransaction().createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
            try {

               
                callStmt.setInt(1, OrgId.intValue());
                callStmt.setString(2, userName);
                 callStmt.execute();
             
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (callStmt != null) {
                    try {
                        callStmt.close();
                    } catch (SQLException ex) {
                        // TODO
                    }
                }
            }
            
        
      
        
        }
    
    public void startShiftApproval( )
    {
        
            Map sessionscope = ADFContext.getCurrent().getSessionScope();
            Number OrgId = (Number) sessionscope.get("AAOrganization");
             String userName=(String) sessionscope.get("AAUsername");
            String sqlStmt = "begin  AS_SCDLR_WF_PKG.XXSCDLR_RUN_SHIFT_PROCESS (:1,:2); end;";
            CallableStatement callStmt = this.getDBTransaction().createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
            try {

               
                callStmt.setInt(1, OrgId.intValue());
                callStmt.setString(2, userName);
                 callStmt.execute();
             
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (callStmt != null) {
                    try {
                        callStmt.close();
                    } catch (SQLException ex) {
                        // TODO
                    }
                }
            }
        
        
        }
    
    
    public void startBulletinApproval( )
    {
        
            Map sessionscope = ADFContext.getCurrent().getSessionScope();
            Number OrgId = (Number) sessionscope.get("AAOrganization");
             String userName=(String) sessionscope.get("AAUsername");
            String sqlStmt = "begin  AS_SCDLR_WF_PKG.XXSCDLR_RUN_BULLETIN_PROCESS (:1,:2); end;";
            CallableStatement callStmt = this.getDBTransaction().createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
            try {

               
                callStmt.setInt(1, OrgId.intValue());
                callStmt.setString(2, userName);
                 callStmt.execute();
             
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (callStmt != null) {
                    try {
                        callStmt.close();
                    } catch (SQLException ex) {
                        // TODO
                    }
                }
            }
        
        
        }
    //-----------Send updates Notification to Employees ------//
    public void sendUpdateNotification(Number schedulerStaffId) {
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_NOTIFICATIONS_PKG.SEND_SCHDLR_UPDATES (:1); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerStaffId.doubleValue());
            callStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
    }
    // Create Shoftworkgroup Automatically according to sleected schedule workgroup category 
    public String createSchedulerShiftWorkGroup(Number schedulerId ,Number fkCategoryId  )
    {
        
          
          String retValue="0";
            String sqlStmt = "begin  AS_SCHDLR_PKG.CREATE_SCHEDULER_SHIFT (:1,:2,:3); end;";
            CallableStatement callStmt = this.getDBTransaction().createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
            try {

               
                callStmt.setInt(1, schedulerId.intValue());
                callStmt.setInt(2, fkCategoryId.intValue());
                callStmt.registerOutParameter(3, Types.VARCHAR);
                callStmt.execute();
                retValue = callStmt.getString(3);
                this.getAsSchedulerShiftsVO().executeQuery();
             
            } catch (SQLException e) {
                e.printStackTrace();
               // retValue="C";
            } finally {
                if (callStmt != null) {
                    try {
                        callStmt.close();
                       // retValue="T";
                    } catch (SQLException ex) {
                        // TODO
                      //  retValue="C2";
                    }
                }
            }
            
        
      return retValue;
        
        }
    
    
    public String check_leave_pending_validation(Number personId, Number replceid, Date vstartdate) {
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
    
        String sqlStmt = "begin  :1 := AS_SCHDLR_PKG.check_leave_pending_validation (:2,:3,:4); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.registerOutParameter(1, Types.VARCHAR);
            callStmt.setDouble(2, personId.doubleValue());
            callStmt.setDouble(3, replceid.doubleValue());
            callStmt.setDate(4, vstartdate.dateValue());

         
            callStmt.execute();
            retValue = callStmt.getString(1);
        } 
        catch (SQLException e) 
        {
            retValue=e.getMessage();
            //throw new JboException(e.getMessage());
          e.printStackTrace();
          
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) 
                
                
                {
                    
                  //  throw new JboException(ex.getMessage());
                    
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }
    public String sendOutLookcalendar(Number schedulerStaffId,Number schedulerId ) {
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.PUSH_OUTLOOK_CALENDAR (:1,:2,:3); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setDouble(1, schedulerId.doubleValue());
            callStmt.setDouble(2, schedulerStaffId.doubleValue());
          
            callStmt.registerOutParameter(3, Types.VARCHAR);
            callStmt.execute();
            retValue = callStmt.getString(3);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }
    public String showHideScheduler(String Status) {
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
        String sqlStmt = "begin  AS_SCHDLR_PKG.SHOWHIDE_SCHEDULER (:1); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.setString(1, Status);
            callStmt.execute();
            retValue = callStmt.getString(6);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) {
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }
    
    public String getSmsUrl() {
        String ConfigName="SMS_URL";
        String retValue = "";
        DBTransaction txn = this.getDBTransaction();
    
        String sqlStmt = "begin  :1 := AS_NOTIFICATIONS_PKG.GET_CONFIG_VALUE (:2); end;";
        CallableStatement callStmt = txn.createCallableStatement(sqlStmt, DBTransaction.DEFAULT);
        try {
            callStmt.registerOutParameter(1, Types.VARCHAR);
            callStmt.setString(2, ConfigName);
            

         
            callStmt.execute();
            retValue = callStmt.getString(1);
        } 
        catch (SQLException e) 
        {
            retValue=e.getMessage();
            //throw new JboException(e.getMessage());
          e.printStackTrace();
          
        } finally {
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (SQLException ex) 
                
                
                {
                    
                  //  throw new JboException(ex.getMessage());
                    
                    // TODO
                }
            }
        }
        //retValue = "Y";
        return retValue;
    }
    public String[] GetEmployeeContanctInfo(String PersonId, String ReplaceId)

        {


        AsEmployeeContactInformationvoImpl vo = this.getAsEmployeeContactInformationvo1();
        vo.setNamedWhereClauseParam("person_id", PersonId);
        vo.setNamedWhereClauseParam("replacement_id", ReplaceId);
        vo.executeQuery();
        String[] empattr=new String[2] ;
   
   if(vo.first()!=null)
   {
      AsEmployeeContactInformationvoRowImpl r=(AsEmployeeContactInformationvoRowImpl)vo.first();
      empattr[0]=r.getPhoneNumber();
      empattr[1]=r.getReplacePhoneNumber();
   }
       else 
   {
       empattr[0]="0"; empattr[1]="0";}
          
   return empattr;

    }
    public String GetEmployeePhoneNumber(String PersonId, String ReplaceId)

        {


        AsEmployeeContactInformationvoImpl vo = this.getAsEmployeeContactInformationvo1();
        vo.setNamedWhereClauseParam("person_id", PersonId);
        vo.setNamedWhereClauseParam("replacement_id", ReplaceId);
        vo.executeQuery();
        String empattr="30";
    
    if(vo.first()!=null)
    {
      AsEmployeeContactInformationvoRowImpl r=(AsEmployeeContactInformationvoRowImpl)vo.first();
      empattr=r.getPhoneNumber();
          //r.getPhoneNumber();
     // empattr[1]=r.getReplacePhoneNumber();
    }
       else 
    {
       empattr="0"; }
        //empattr[1]="0";}
          
    return empattr;
        //empattr;

    }

    /**
     * Container's getter for UsersVO1.
     * @return UsersVO1
     */
    public ViewObjectImpl getUsersVO() {
        return (ViewObjectImpl) findViewObject("UsersVO");
    }

    /**
     * Container's getter for UsersVO1.
     * @return UsersVO1
     */
    public ViewObjectImpl getDirectUsersVO() {
        return (ViewObjectImpl) findViewObject("DirectUsersVO");
    }

    /**
     * Container's getter for UsersSelfJoinVL1.
     * @return UsersSelfJoinVL1
     */
    public ViewLinkImpl getUsersSelfJoinVL1() {
        return (ViewLinkImpl) findViewLink("UsersSelfJoinVL1");
    }

    /**
     * Container's getter for WorkGroupEmpCountVO1.
     * @return WorkGroupEmpCountVO1
     */
    public ViewObjectImpl getWorkGroupEmpCountVO() {
        return (ViewObjectImpl) findViewObject("WorkGroupEmpCountVO");
    }

    /**
     * Container's getter for AsWorkGroupsHdrVO1.
     * @return AsWorkGroupsHdrVO1
     */
    public AsWorkGroupsHdrVOImpl getAsWorkGroupsHdrVO() {
        return (AsWorkGroupsHdrVOImpl) findViewObject("AsWorkGroupsHdrVO");
    }

    /**
     * Container's getter for AsWorkGroupsLinesVO1.
     * @return AsWorkGroupsLinesVO1
     */
    public AsWorkGroupsLinesVOImpl getAsWorkGroupsLinesVO() {
        return (AsWorkGroupsLinesVOImpl) findViewObject("AsWorkGroupsLinesVO");
    }

    /**
     * Container's getter for WorkGroupHdrDtlVL1.
     * @return WorkGroupHdrDtlVL1
     */
    public ViewLinkImpl getWorkGroupHdrDtlVL1() {
        return (ViewLinkImpl) findViewLink("WorkGroupHdrDtlVL1");
    }

    /**
     * Container's getter for AsShiftsVO1.
     * @return AsShiftsVO1
     */
    public ViewObjectImpl getAsShiftsVO() {
        return (ViewObjectImpl) findViewObject("AsShiftsVO");
    }

    /**
     * Container's getter for AsShiftWorkGroupsVO1.
     * @return AsShiftWorkGroupsVO1
     */
    public ViewObjectImpl getAsShiftWorkGroupsVO1() {
        return (ViewObjectImpl) findViewObject("AsShiftWorkGroupsVO");
    }

    /**
     * Container's getter for ShiftsWorkGroupVL1.
     * @return ShiftsWorkGroupVL1
     */
    public ViewLinkImpl getShiftsWorkGroupVL1() {
        return (ViewLinkImpl) findViewLink("ShiftsWorkGroupVL1");
    }

    /**
     * Container's getter for AsShiftWorkGroupsVO.
     * @return AsShiftWorkGroupsVO
     */
    public AsShiftWorkGroupsVOImpl getAsShiftWorkGroupsVO() {
        return (AsShiftWorkGroupsVOImpl) findViewObject("AsShiftWorkGroupsVO");
    }

    /**
     * Container's getter for ShiftAvailabilityVO1.
     * @return ShiftAvailabilityVO1
     */
    public ViewObjectImpl getShiftAvailabilityVO() {
        return (ViewObjectImpl) findViewObject("ShiftAvailabilityVO");
    }

    /**
     * Container's getter for AsWorkGroupsLinesVO1.
     * @return AsWorkGroupsLinesVO1
     */
    public AsWorkGroupsLinesVOImpl getAsShiftWorkGroupEmpsVO() {
        return (AsWorkGroupsLinesVOImpl) findViewObject("AsShiftWorkGroupEmpsVO");
    }

    /**
     * Container's getter for ShiftWorkGroupsEmpsVL1.
     * @return ShiftWorkGroupsEmpsVL1
     */
    public ViewLinkImpl getShiftWorkGroupsEmpsVL1() {
        return (ViewLinkImpl) findViewLink("ShiftWorkGroupsEmpsVL1");
    }

    /**
     * Container's getter for AbsenceTypeStatVO1.
     * @return AbsenceTypeStatVO1
     */
    public ViewObjectImpl getAbsenceTypeStatVO() {
        return (ViewObjectImpl) findViewObject("AbsenceTypeStatVO");
    }

    /**
     * Container's getter for AbsenceWorkGroupsVO1.
     * @return AbsenceWorkGroupsVO1
     */
    public ViewObjectImpl getAbsenceWorkGroupsVO() {
        return (ViewObjectImpl) findViewObject("AbsenceWorkGroupsVO");
    }


    /**
     * Container's getter for AllWorkGroupEmpsVO1.
     * @return AllWorkGroupEmpsVO1
     */
    public ViewObjectImpl getAllWorkGroupEmpsVO() {
        return (ViewObjectImpl) findViewObject("AllWorkGroupEmpsVO");
    }

    /**
     * Container's getter for AbsenceSummaryVO1.
     * @return AbsenceSummaryVO1
     */
    public ViewObjectImpl getAbsenceSummaryVO() {
        return (ViewObjectImpl) findViewObject("AbsenceSummaryVO");
    }

    /**
     * Container's getter for WorkGroupEmpAbsenceTypesVL1.
     * @return WorkGroupEmpAbsenceTypesVL1
     */
    public ViewLinkImpl getWorkGroupEmpAbsenceTypesVL1() {
        return (ViewLinkImpl) findViewLink("WorkGroupEmpAbsenceTypesVL1");
    }

    /**
     * Container's getter for AbsenceTypesVO1.
     * @return AbsenceTypesVO1
     */
    public ViewObjectImpl getAbsenceTypesVO() {
        return (ViewObjectImpl) findViewObject("AbsenceTypesVO");
    }

    /**
     * Container's getter for TasTrxDataVO1.
     * @return TasTrxDataVO1
     */
    public ViewObjectImpl getTasTrxDataVO() {
        return (ViewObjectImpl) findViewObject("TasTrxDataVO");
    }

    /**
     * Container's getter for WorkGroupEmpTasTrxVL1.
     * @return WorkGroupEmpTasTrxVL1
     */
    public ViewLinkImpl getWorkGroupEmpTasTrxVL1() {
        return (ViewLinkImpl) findViewLink("WorkGroupEmpTasTrxVL1");
    }

    /**
     * Container's getter for WorkGroupsEmpTasTodayVO1.
     * @return WorkGroupsEmpTasTodayVO1
     */
    public ViewObjectImpl getWorkGroupsEmpTasAttendTodayVO() {
        return (ViewObjectImpl) findViewObject("WorkGroupsEmpTasAttendTodayVO");
    }

    /**
     * Container's getter for WorkGroupsEmpTasTodayVO2.
     * @return WorkGroupsEmpTasTodayVO2
     */
    public ViewObjectImpl getWorkGroupsEmpTasAbsenceTodayVO() {
        return (ViewObjectImpl) findViewObject("WorkGroupsEmpTasAbsenceTodayVO");
    }

    /**
     * Container's getter for AsSchedulersVO1.
     * @return AsSchedulersVO1
     */
    public AsSchedulersVOImpl getAsSchedulersVO() {
        return (AsSchedulersVOImpl) findViewObject("AsSchedulersVO");
    }

    /**
     * Container's getter for AsSchedulerShiftsVO1.
     * @return AsSchedulerShiftsVO1
     */
    public AsSchedulerShiftsVOImpl getAsSchedulerShiftsVO() {
        return (AsSchedulerShiftsVOImpl) findViewObject("AsSchedulerShiftsVO");
    }

    /**
     * Container's getter for AsSchedularShiftsVL1.
     * @return AsSchedularShiftsVL1
     */
    public ViewLinkImpl getAsSchedularShiftsVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedularShiftsVL1");
    }


    /**
     * Container's getter for AsSchedulerStaffVO1.
     * @return AsSchedulerStaffVO1
     */
    public AsSchedulerStaffVOImpl getAsSchedulerStaffVO() {
        return (AsSchedulerStaffVOImpl) findViewObject("AsSchedulerStaffVO");
    }

    /**
     * Container's getter for AsSchedularStaffVL1.
     * @return AsSchedularStaffVL1
     */
    public ViewLinkImpl getAsSchedularStaffVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedularStaffVL1");
    }


    /**
     * Container's getter for AsSchedulerAvailStaffVO1.
     * @return AsSchedulerAvailStaffVO1
     */
    public ViewObjectImpl getAsSchedulerAvailStaffVO() {
        return (ViewObjectImpl) findViewObject("AsSchedulerAvailStaffVO");
    }

    /**
     * Container's getter for AsSchedulerAvailStaffVL1.
     * @return AsSchedulerAvailStaffVL1
     */
    public ViewLinkImpl getAsSchedulerAvailStaffVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedulerAvailStaffVL1");
    }

    /**
     * Container's getter for AsPatternHdrsVO1.
     * @return AsPatternHdrsVO1
     */
    public ViewObjectImpl getAsPatternHdrsVO() {
        return (ViewObjectImpl) findViewObject("AsPatternHdrsVO");
    }

    /**
     * Container's getter for AsPatternLinesVO1.
     * @return AsPatternLinesVO1
     */
    public ViewObjectImpl getAsPatternLinesVO() {
        return (ViewObjectImpl) findViewObject("AsPatternLinesVO");
    }

    /**
     * Container's getter for AsPatternVL1.
     * @return AsPatternVL1
     */
    public ViewLinkImpl getAsPatternVL1() {
        return (ViewLinkImpl) findViewLink("AsPatternVL1");
    }


    /**
     * Container's getter for AdinDataStatVO1.
     * @return AdinDataStatVO1
     */
    public ViewObjectImpl getAdminDataStatVO() {
        return (ViewObjectImpl) findViewObject("AdminDataStatVO");
    }


    /**
     * Container's getter for AsWorkGroupsLinesActiveVO1.
     * @return AsWorkGroupsLinesActiveVO1
     */
    public ViewObjectImpl getAsWorkGroupsLinesActiveVO() {
        return (ViewObjectImpl) findViewObject("AsWorkGroupsLinesActiveVO");
    }

    /**
     * Container's getter for WorkGroupHdrDtlActiveVL1.
     * @return WorkGroupHdrDtlActiveVL1
     */
    public ViewLinkImpl getWorkGroupHdrDtlActiveVL1() {
        return (ViewLinkImpl) findViewLink("WorkGroupHdrDtlActiveVL1");
    }

    /**
     * Container's getter for AsWorkGroupsHdrVO1.
     * @return AsWorkGroupsHdrVO1
     */
    public AsWorkGroupsHdrVOImpl getAsWorkGroupsHdrActiveVO() {
        return (AsWorkGroupsHdrVOImpl) findViewObject("AsWorkGroupsHdrActiveVO");
    }

    /**
     * Container's getter for AsWorkgroupSchdlrTasVO1.
     * @return AsWorkgroupSchdlrTasVO1
     */
    public ViewObjectImpl getAsWorkgroupSchdlrTasVO() {
        return (ViewObjectImpl) findViewObject("AsWorkgroupSchdlrTasVO");
    }

    /**
     * Container's getter for WorkGroupEmpSchdltTasVL1.
     * @return WorkGroupEmpSchdltTasVL1
     */
    public ViewLinkImpl getWorkGroupEmpSchdltTasVL1() {
        return (ViewLinkImpl) findViewLink("WorkGroupEmpSchdltTasVL1");
    }

    /**
     * Container's getter for AsWorkGroupSchdlrTasDataVO1.
     * @return AsWorkGroupSchdlrTasDataVO1
     */
    public ViewObjectImpl getAsWorkGroupSchdlrTasDataVO() {
        return (ViewObjectImpl) findViewObject("AsWorkGroupSchdlrTasDataVO");
    }

    /**
     * Container's getter for FndLokkupValuesVO1.
     * @return FndLokkupValuesVO1
     */
    public ViewObjectImpl getAsWorkGroupImagesVO() {
        return (ViewObjectImpl) findViewObject("AsWorkGroupImagesVO");
    }

    /**
     * Container's getter for AsBulletinsVO1.
     * @return AsBulletinsVO1
     */
    public ViewObjectImpl getAsBulletinsVO() {
        return (ViewObjectImpl) findViewObject("AsBulletinsVO");
    }

    /**
     * Container's getter for AsSchedulerStaffBulletinsVO1.
     * @return AsSchedulerStaffBulletinsVO1
     */
    public AsSchedulerStaffBulletinsVOImpl getAsSchedulerStaffBulletinsVO() {
        return (AsSchedulerStaffBulletinsVOImpl) findViewObject("AsSchedulerStaffBulletinsVO");
    }

    /**
     * Container's getter for AsSchedulerStaffBulletinsVL1.
     * @return AsSchedulerStaffBulletinsVL1
     */
    public ViewLinkImpl getAsSchedulerStaffBulletinsVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedulerStaffBulletinsVL1");
    }

    /**
     * Container's getter for WorkGroupDMVO1.
     * @return WorkGroupDMVO1
     */
    public ViewObjectImpl getWorkGroupDMVO() {
        return (ViewObjectImpl) findViewObject("WorkGroupDMVO");
    }

    /**
     * Container's getter for WorkGroupLinesDMVO1.
     * @return WorkGroupLinesDMVO1
     */
    public ViewObjectImpl getWorkGroupLinesDMVO() {
        return (ViewObjectImpl) findViewObject("WorkGroupLinesDMVO");
    }

    /**
     * Container's getter for WorkGroupDMVL1.
     * @return WorkGroupDMVL1
     */
    public ViewLinkImpl getWorkGroupDMVL() {
        return (ViewLinkImpl) findViewLink("WorkGroupDMVL");
    }


    /**
     * Container's getter for AsSchedulerTransHdrsVO1.
     * @return AsSchedulerTransHdrsVO1
     */
    public ViewObjectImpl getAsSchedulerTransHdrsVO() {
        return (ViewObjectImpl) findViewObject("AsSchedulerTransHdrsVO");
    }

    /**
     * Container's getter for AsSchedulerStaffVO1.
     * @return AsSchedulerStaffVO1
     */
    public AsSchedulerStaffVOImpl getGenSchedulerStaffVO() {
        return (AsSchedulerStaffVOImpl) findViewObject("GenSchedulerStaffVO");
    }

    /**
     * Container's getter for AsSchedulerTransHdrsGeneratedVL1.
     * @return AsSchedulerTransHdrsGeneratedVL1
     */
    public ViewLinkImpl getAsSchedulerTransHdrsGeneratedVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedulerTransHdrsGeneratedVL1");
    }

    /**
     * Container's getter for AsNotificationsVO1.
     * @return AsNotificationsVO1
     */
    public ViewObjectImpl getAsNotificationsVO() {
        return (ViewObjectImpl) findViewObject("AsNotificationsVO");
    }

    /**
     * Container's getter for AsApprovalTypesVO1.
     * @return AsApprovalTypesVO1
     */
    public ViewObjectImpl getAsApprovalTypesVO() {
        return (ViewObjectImpl) findViewObject("AsApprovalTypesVO");
    }

    /**
     * Container's getter for AsApprovalTypeStepsVO1.
     * @return AsApprovalTypeStepsVO1
     */
    public ViewObjectImpl getAsApprovalTypeStepsVO() {
        return (ViewObjectImpl) findViewObject("AsApprovalTypeStepsVO");
    }

    /**
     * Container's getter for ApprovalTypesVL1.
     * @return ApprovalTypesVL1
     */
    public ViewLinkImpl getApprovalTypesVL1() {
        return (ViewLinkImpl) findViewLink("ApprovalTypesVL1");
    }


    /**
     * Container's getter for AsSchedulerTransHdrsVO1.
     * @return AsSchedulerTransHdrsVO1
     */
    public ViewObjectImpl getSchedulerTransHdrsVO() {
        return (ViewObjectImpl) findViewObject("SchedulerTransHdrsVO");
    }

    /**
     * Container's getter for SchedularTransVL1.
     * @return SchedularTransVL1
     */
    public ViewLinkImpl getSchedularTransVL1() {
        return (ViewLinkImpl) findViewLink("SchedularTransVL1");
    }

    /**
     * Container's getter for EmpSchedulesCalVO1.
     * @return EmpSchedulesCalVO1
     */
    public EmpSchedulesCalVOImpl getEmpSchedulesCalVO() {
        return (EmpSchedulesCalVOImpl) findViewObject("EmpSchedulesCalVO");
    }

    /**
     * Container's getter for AsAssignmentsVO1.
     * @return AsAssignmentsVO1
     */
    public AsAssignmentsVOImpl getAsAssignmentsVO() {
        return (AsAssignmentsVOImpl) findViewObject("AsAssignmentsVO");
    }

    /**
     * Container's getter for AsWorklistVO1.
     * @return AsWorklistVO1
     */
    public AsWorklistVOImpl getAsWorklistVO() {
        return (AsWorklistVOImpl) findViewObject("AsWorklistVO");
    }

    /**
     * Container's getter for AsSchedulerStaffVO1.
     * @return AsSchedulerStaffVO1
     */
    public AsSchedulerStaffVOImpl getSrcSchedulerStaffVO() {
        return (AsSchedulerStaffVOImpl) findViewObject("SrcSchedulerStaffVO");
    }

    /**
     * Container's getter for AsSchedulerTransHdrsSourceVL1.
     * @return AsSchedulerTransHdrsSourceVL1
     */
    public ViewLinkImpl getAsSchedulerTransHdrsSourceVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedulerTransHdrsSourceVL1");
    }

    /**
     * Container's getter for SchedularActionHistoryVO1.
     * @return SchedularActionHistoryVO1
     */
    public ViewObjectImpl getTransActionHistoryVO() {
        return (ViewObjectImpl) findViewObject("TransActionHistoryVO");
    }

    /**
     * Container's getter for SchedularActionHistoryTransVL1.
     * @return SchedularActionHistoryTransVL1
     */
    public ViewLinkImpl getSchedularActionHistoryTransVL1() {
        return (ViewLinkImpl) findViewLink("SchedularActionHistoryTransVL1");
    }

    /**
     * Container's getter for SchedularActionHistoryVO1.
     * @return SchedularActionHistoryVO1
     */
    public ViewObjectImpl getSchdlrActionHistoryVO() {
        return (ViewObjectImpl) findViewObject("SchdlrActionHistoryVO");
    }

    /**
     * Container's getter for SchedularActionHistorySchdlrVL1.
     * @return SchedularActionHistorySchdlrVL1
     */
    public ViewLinkImpl getSchedularActionHistorySchdlrVL1() {
        return (ViewLinkImpl) findViewLink("SchedularActionHistorySchdlrVL1");
    }

    /**
     * Container's getter for SchedularActionHistoryVO1.
     * @return SchedularActionHistoryVO1
     */
    public ViewObjectImpl getTransActionHistorySubVO() {
        return (ViewObjectImpl) findViewObject("TransActionHistorySubVO");
    }

    /**
     * Container's getter for SchedularActionHistoryTransVL2.
     * @return SchedularActionHistoryTransVL2
     */
    public ViewLinkImpl getSchedularActionHistoryTransVL2() {
        return (ViewLinkImpl) findViewLink("SchedularActionHistoryTransVL2");
    }

    /**
     * Container's getter for AsAssignmentEmpsVO1.
     * @return AsAssignmentEmpsVO1
     */
    public AsAssignmentEmpsVOImpl getAsAssignmentEmpsVO() {
        return (AsAssignmentEmpsVOImpl) findViewObject("AsAssignmentEmpsVO");
    }

    /**
     * Container's getter for AsAssignmentsVL1.
     * @return AsAssignmentsVL1
     */
    public ViewLinkImpl getAsAssignmentsVL1() {
        return (ViewLinkImpl) findViewLink("AsAssignmentsVL1");
    }

    /**
     * Container's getter for NtfWorklistVO1.
     * @return NtfWorklistVO1
     */
    public ViewObjectImpl getNtfWorklistVO() {
        return (ViewObjectImpl) findViewObject("NtfWorklistVO");
    }

    /**
     * Container's getter for DashboardTilesVO1.
     * @return DashboardTilesVO1
     */
    public ViewObjectImpl getDashboardTilesVO() {
        return (ViewObjectImpl) findViewObject("DashboardTilesVO");
    }

    /**
     * Container's getter for DashboardGenderVO1.
     * @return DashboardGenderVO1
     */
    public ViewObjectImpl getDashboardGenderVO() {
        return (ViewObjectImpl) findViewObject("DashboardGenderVO");
    }

    /**
     * Container's getter for DashboardAgeDistVO1.
     * @return DashboardAgeDistVO1
     */
    public ViewObjectImpl getDashboardAgeDistVO() {
        return (ViewObjectImpl) findViewObject("DashboardAgeDistVO");
    }

    /**
     * Container's getter for DashboardShiftDistVO1.
     * @return DashboardShiftDistVO1
     */
    public ViewObjectImpl getDashboardShiftDistVO() {
        return (ViewObjectImpl) findViewObject("DashboardShiftDistVO");
    }

    /**
     * Container's getter for DashboardAnnualLeavePtrnVO1.
     * @return DashboardAnnualLeavePtrnVO1
     */
    public ViewObjectImpl getDashboardAnnualLeavePtrnVO() {
        return (ViewObjectImpl) findViewObject("DashboardAnnualLeavePtrnVO");
    }

    /**
     * Container's getter for DashboardOvertimeDeptVO1.
     * @return DashboardOvertimeDeptVO1
     */
    public ViewObjectImpl getDashboardOvertimeDeptVO() {
        return (ViewObjectImpl) findViewObject("DashboardOvertimeDeptVO");
    }

    /**
     * Container's getter for HomeStatVO1.
     * @return HomeStatVO1
     */
    public ViewObjectImpl getHomeStatVO() {
        return (ViewObjectImpl) findViewObject("HomeStatVO");
    }

    /**
     * Container's getter for HomePictoChartVO1.
     * @return HomePictoChartVO1
     */
    public ViewObjectImpl getHomePictoChartVO() {
        return (ViewObjectImpl) findViewObject("HomePictoChartVO");
    }

    /**
     * Container's getter for AsSchedulerStaffBulletinsVO1.
     * @return AsSchedulerStaffBulletinsVO1
     */
    public AsSchedulerStaffBulletinsVOImpl getEmpSchedulerStafffBulletinsVO() {
        return (AsSchedulerStaffBulletinsVOImpl) findViewObject("EmpSchedulerStafffBulletinsVO");
    }

    /**
     * Container's getter for EmpStaffCalBulletinsVL1.
     * @return EmpStaffCalBulletinsVL1
     */
    public ViewLinkImpl getEmpStaffCalBulletinsVL1() {
        return (ViewLinkImpl) findViewLink("EmpStaffCalBulletinsVL1");
    }

    /**
     * Container's getter for MgrSchedulesCalVO1.
     * @return MgrSchedulesCalVO1
     */
    public ViewObjectImpl getMgrSchedulesCalVO() {
        return (ViewObjectImpl) findViewObject("MgrSchedulesCalVO");
    }

    /**
     * Container's getter for DummyVO1.
     * @return DummyVO1
     */
    public ViewObjectImpl getDummyVO() {
        return (ViewObjectImpl) findViewObject("DummyVO");
    }

    /**
     * Container's getter for AsUserRolesVO1.
     * @return AsUserRolesVO1
     */
    public ViewObjectImpl getAsUserRolesVO() {
        return (ViewObjectImpl) findViewObject("AsUserRolesVO");
    }

    /**
     * Container's getter for DashboardBulletinsVO1.
     * @return DashboardBulletinsVO1
     */
    public ViewObjectImpl getDashboardBulletinsVO() {
        return (ViewObjectImpl) findViewObject("DashboardBulletinsVO");
    }

    /**
     * Container's getter for AsPublicSpaceVO1.
     * @return AsPublicSpaceVO1
     */
    public AsPublicSpaceVOImpl getAsPublicSpaceVO1() {
        return (AsPublicSpaceVOImpl) findViewObject("AsPublicSpaceVO1");
    }

    /**
     * Container's getter for AsPublicSpaceEmployeeVO1.
     * @return AsPublicSpaceEmployeeVO1
     */
    public AsPublicSpaceEmployeeVOImpl getAsPublicSpaceEmployeeVO1() {
        return (AsPublicSpaceEmployeeVOImpl) findViewObject("AsPublicSpaceEmployeeVO1");
    }

    /**
     * Container's getter for AsPublicSpaceBulletinsVO1.
     * @return AsPublicSpaceBulletinsVO1
     */
    public AsPublicSpaceBulletinsVOImpl getAsPublicSpaceBulletinsVO1() {
        return (AsPublicSpaceBulletinsVOImpl) findViewObject("AsPublicSpaceBulletinsVO1");
    }

    /**
     * Container's getter for AsPublicSpaceAssignmentsVO1.
     * @return AsPublicSpaceAssignmentsVO1
     */
    public AsPublicSpaceAssignmentsVOImpl getAsPublicSpaceAssignmentsVO1() {
        return (AsPublicSpaceAssignmentsVOImpl) findViewObject("AsPublicSpaceAssignmentsVO1");
    }

    /**
     * Container's getter for workGroupShiftVO1.
     * @return workGroupShiftVO1
     */

    public WorkGroupShiftVOImpl getworkGroupShiftVO1() {
        return (WorkGroupShiftVOImpl) findViewObject("workGroupShiftVO1");
    }


    /**
     * Container's getter for WorkGroupLov1.
     * @return WorkGroupLov1
     */
    public WorkGroupLovImpl getWorkGroupLov1() {
        return (WorkGroupLovImpl) findViewObject("WorkGroupLov1");
    }

    /**
     * Container's getter for ShiftsLov1.
     * @return ShiftsLov1
     */
    public ShiftsLovImpl getShiftsLov1() {
        return (ShiftsLovImpl) findViewObject("ShiftsLov1");
    }

    /**
     * Container's getter for AsEmployee_TasVO1.
     * @return AsEmployee_TasVO1
     */
    public AsEmployee_TasVOImpl getAsEmployee_TasVO1() {
        return (AsEmployee_TasVOImpl) findViewObject("AsEmployee_TasVO1");
    }

    /**
     * Container's getter for AsDummyMonthDays1.
     * @return AsDummyMonthDays1
     */
    public AsDummyMonthDaysImpl getAsDummyMonthDays1() {
        return (AsDummyMonthDaysImpl) findViewObject("AsDummyMonthDays1");
    }

    /**
     * Container's getter for AspublicSpaceLeavesVO1.
     * @return AspublicSpaceLeavesVO1
     */
    public AspublicSpaceLeavesVOImpl getAspublicSpaceLeavesVO1() {
        return (AspublicSpaceLeavesVOImpl) findViewObject("AspublicSpaceLeavesVO1");
    }

    /**
     * Container's getter for EmployeeAssignmentReplaceVO1.
     * @return EmployeeAssignmentReplaceVO1
     */
    public ViewObjectImpl getEmployeeAssignmentReplaceVO1() {
        return (ViewObjectImpl) findViewObject("EmployeeAssignmentReplaceVO1");
    }

    /**
     * Container's getter for AsSchedulertotalValidateVO1.
     * @return AsSchedulertotalValidateVO1
     */
    public AsSchedulertotalValidateVOImpl getAsSchedulertotalValidateVO1() {
        return (AsSchedulertotalValidateVOImpl) findViewObject("AsSchedulertotalValidateVO1");
    }

    /**
     * Container's getter for AsSchedulerMonthVO1.
     * @return AsSchedulerMonthVO1
     */
    public AsSchedulerMonthVOImpl getAsSchedulerMonthVO1() {
        return (AsSchedulerMonthVOImpl) findViewObject("AsSchedulerMonthVO1");
    }

    /**
     * Container's getter for AsSchdeulerStaffTableVO1.
     * @return AsSchdeulerStaffTableVO1
     */
    public AsSchdeulerStaffTableVOImpl getAsSchdeulerStaffTableVO1() {
        return (AsSchdeulerStaffTableVOImpl) findViewObject("AsSchdeulerStaffTableVO1");
    }


    /**
     * Container's getter for AsSchdeulerStaffTableVO2.
     * @return AsSchdeulerStaffTableVO2
     */
    public AsSchdeulerStaffTableVOImpl getAsSchdeulerStaffTableVO() {
        return (AsSchdeulerStaffTableVOImpl) findViewObject("AsSchdeulerStaffTableVO");
    }

    /**
     * Container's getter for AsScheduelStaffTableVL1.
     * @return AsScheduelStaffTableVL1
     */
    public ViewLinkImpl getAsScheduelStaffTableVL1() {
        return (ViewLinkImpl) findViewLink("AsScheduelStaffTableVL1");
    }


    /**
     * Container's getter for AsSchedulerStaffShiftTableVO.
     * @return AsSchedulerStaffShiftTableVO
     */
    public AsSchedulerStaffShiftTableVOImpl getAsSchedulerStaffShiftTableVO() {
        return (AsSchedulerStaffShiftTableVOImpl) findViewObject("AsSchedulerStaffShiftTableVO");
    }

    /**
     * Container's getter for AsSchedulerStaffShiftVL1.
     * @return AsSchedulerStaffShiftVL1
     */
    public ViewLinkImpl getAsSchedulerStaffShiftVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedulerStaffShiftVL1");
    }

    /**
     * Container's getter for AsSchedulerStaffVO1.
     * @return AsSchedulerStaffVO1
     */
    public AsSchedulerStaffVOImpl getAsSchedulerStaffCurrentVO() {
        return (AsSchedulerStaffVOImpl) findViewObject("AsSchedulerStaffCurrentVO");
    }

    /**
     * Container's getter for LeaveTypesVO1.
     * @return LeaveTypesVO1
     */
    public ViewObjectImpl getLeaveTypesVO1() {
        return (ViewObjectImpl) findViewObject("LeaveTypesVO1");
    }

    /**
     * Container's getter for AsShiftAttributesVO1.
     * @return AsShiftAttributesVO1
     */
    public AsShiftAttributesVOImpl getAsShiftAttributesVO1() {
        return (AsShiftAttributesVOImpl) findViewObject("AsShiftAttributesVO1");
    }

    /**
     * Container's getter for AsShiftAttributesVO2.
     * @return AsShiftAttributesVO2
     */
    public AsShiftAttributesVOImpl getAsShiftAttributesVO2() {
        return (AsShiftAttributesVOImpl) findViewObject("AsShiftAttributesVO2");
    }

    /**
     * Container's getter for AsSchedulerShiftatrribuesVL1.
     * @return AsSchedulerShiftatrribuesVL1
     */
    public ViewLinkImpl getAsSchedulerShiftatrribuesVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedulerShiftatrribuesVL1");
    }

    /**
     * Container's getter for AsSchedulerTypesVO1.
     * @return AsSchedulerTypesVO1
     */
    public ViewObjectImpl getAsSchedulerTypesVO1() {
        return (ViewObjectImpl) findViewObject("AsSchedulerTypesVO1");
    }

    /**
     * Container's getter for AsSchedulerStaffVO1.
     * @return AsSchedulerStaffVO1
     */
    public AsSchedulerStaffVOImpl getAsSchedulerStaffVO1() {
        return (AsSchedulerStaffVOImpl) findViewObject("AsSchedulerStaffVO1");
    }

    /**
     * Container's getter for AsSchedulerSectionsVO1.
     * @return AsSchedulerSectionsVO1
     */
    public AsSchedulerSectionsVOImpl getAsSchedulerSectionsVO1() {
        return (AsSchedulerSectionsVOImpl) findViewObject("AsSchedulerSectionsVO1");
    }


    /**
     * Container's getter for AsSchedulerSectionsVO3.
     * @return AsSchedulerSectionsVO3
     */
    public AsSchedulerSectionsVOImpl getAsSchedulerSectionsVO2() {
        return (AsSchedulerSectionsVOImpl) findViewObject("AsSchedulerSectionsVO2");
    }

    /**
     * Container's getter for AsSchedulersEctionVl1.
     * @return AsSchedulersEctionVl1
     */
    public ViewLinkImpl getAsSchedulersEctionVl1() {
        return (ViewLinkImpl) findViewLink("AsSchedulersEctionVl1");
    }

    /**
     * Container's getter for AsSchedulerStaffBulletinsVO1.
     * @return AsSchedulerStaffBulletinsVO1
     */
    public AsSchedulerStaffBulletinsVOImpl getAsSchedulerStaffBulletinsVO1() {
        return (AsSchedulerStaffBulletinsVOImpl) findViewObject("AsSchedulerStaffBulletinsVO1");
    }

    /**
     * Container's getter for AcSchedulerFiratApprovalLeaveVO1.
     * @return AcSchedulerFiratApprovalLeaveVO1
     */
    public AcSchedulerFirstApprovalLeaveVOImpl getAcSchedulerFirstApprovalLeaveVO1() {
        return (AcSchedulerFirstApprovalLeaveVOImpl) findViewObject("AcSchedulerFirstApprovalLeaveVO1");
    }

    /**
     * Container's getter for AsWorkgroupNotificationsVO1.
     * @return AsWorkgroupNotificationsVO1
     */
    public AsWorkgroupNotificationsVOImpl getAsWorkgroupNotificationsVO1() {
        return (AsWorkgroupNotificationsVOImpl) findViewObject("AsWorkgroupNotificationsVO1");
    }


    /**
     * Container's getter for AsWorkgroupNotifEmployeesVO1.
     * @return AsWorkgroupNotifEmployeesVO1
     */
    public ViewObjectImpl getAsWorkgroupNotifEmployeesVO1() {
        return (ViewObjectImpl) findViewObject("AsWorkgroupNotifEmployeesVO1");
    }


    /**
     * Container's getter for AsWorkgroupNotifEmployeesVO2.
     * @return AsWorkgroupNotifEmployeesVO2
     */
    public ViewObjectImpl getAsWorkgroupNotifEmployeesVO2() {
        return (ViewObjectImpl) findViewObject("AsWorkgroupNotifEmployeesVO2");
    }

    /**
     * Container's getter for AsWorkgroupNotifyEmployeeVL1.
     * @return AsWorkgroupNotifyEmployeeVL1
     */
    public ViewLinkImpl getAsWorkgroupNotifyEmployeeVL1() {
        return (ViewLinkImpl) findViewLink("AsWorkgroupNotifyEmployeeVL1");
    }

    /**
     * Container's getter for AsSchedulerStaffBulletinsVO2.
     * @return AsSchedulerStaffBulletinsVO2
     */
    public AsSchedulerStaffBulletinsVOImpl getAsSchedulerStaffBulletinsVO2() {
        return (AsSchedulerStaffBulletinsVOImpl) findViewObject("AsSchedulerStaffBulletinsVO2");
    }

    /**
     * Container's getter for AsOrganizationVO1.
     * @return AsOrganizationVO1
     */
    public ViewObjectImpl getAsOrganizationVO1() {
        return (ViewObjectImpl) findViewObject("AsOrganizationVO1");
    }

    /**
     * Container's getter for AsApprovalTypeStepsVO1.
     * @return AsApprovalTypeStepsVO1
     */
    public ViewObjectImpl getAsApprovalTypeStepsVO1() {
        return (ViewObjectImpl) findViewObject("AsApprovalTypeStepsVO1");
    }


    /**
     * Container's getter for TemplatesEmployeesVO1.
     * @return TemplatesEmployeesVO1
     */
    public ViewObjectImpl getTemplatesEmployeesVO1() {
        return (ViewObjectImpl) findViewObject("TemplatesEmployeesVO1");
    }


    /**
     * Container's getter for AsTemplatesVO2.
     * @return AsTemplatesVO2
     */
    public AsTemplatesVOImpl getAsTemplatesVO1() {
        return (AsTemplatesVOImpl) findViewObject("AsTemplatesVO1");
    }

    /**
     * Container's getter for AsTemplateEmployeeVO1.
     * @return AsTemplateEmployeeVO1
     */
    public AsTemplateEmployeeVOImpl getAsTemplateEmployeeVO1() {
        return (AsTemplateEmployeeVOImpl) findViewObject("AsTemplateEmployeeVO1");
    }

    /**
     * Container's getter for ASTempEmployeesVL.
     * @return ASTempEmployeesVL
     */
    public ViewLinkImpl getASTempEmployeesVL() {
        return (ViewLinkImpl) findViewLink("ASTempEmployeesVL");
    }

    /**
     * Container's getter for AsTemplateEmpBulletinsVO1.
     * @return AsTemplateEmpBulletinsVO1
     */
    public AsTemplateEmpBulletinsVOImpl getAsTemplateEmpBulletinsVO1() {
        return (AsTemplateEmpBulletinsVOImpl) findViewObject("AsTemplateEmpBulletinsVO1");
    }

    /**
     * Container's getter for AsTemplateEmpBulletinsVO2.
     * @return AsTemplateEmpBulletinsVO2
     */
    public AsTemplateEmpBulletinsVOImpl getAsTemplateEmpBulletinsVO2() {
        return (AsTemplateEmpBulletinsVOImpl) findViewObject("AsTemplateEmpBulletinsVO2");
    }

    /**
     * Container's getter for AsTemplateBulletinVL1.
     * @return AsTemplateBulletinVL1
     */
    public ViewLinkImpl getAsTemplateBulletinVL1() {
        return (ViewLinkImpl) findViewLink("AsTemplateBulletinVL1");
    }

    /**
     * Container's getter for AsTemplateEmployeeVO2.
     * @return AsTemplateEmployeeVO2
     */
    public AsTemplateEmployeeVOImpl getAsTemplateEmployeeVO2() {
        return (AsTemplateEmployeeVOImpl) findViewObject("AsTemplateEmployeeVO2");
    }

    /**
     * Container's getter for AsTemplateEmpBulletinsVO3.
     * @return AsTemplateEmpBulletinsVO3
     */
    public AsTemplateEmpBulletinsVOImpl getAsTemplateEmpBulletinsVO3() {
        return (AsTemplateEmpBulletinsVOImpl) findViewObject("AsTemplateEmpBulletinsVO3");
    }

    /**
     * Container's getter for AsTemplateBulletinVL2.
     * @return AsTemplateBulletinVL2
     */
    public ViewLinkImpl getAsTemplateBulletinVL2() {
        return (ViewLinkImpl) findViewLink("AsTemplateBulletinVL2");
    }

    /**
     * Container's getter for AsDummyPublicSpaceHeaderVO1.
     * @return AsDummyPublicSpaceHeaderVO1
     */
    public ViewObjectImpl getAsDummyPublicSpaceHeaderVO1() {
        return (ViewObjectImpl) findViewObject("AsDummyPublicSpaceHeaderVO1");
    }

    /**
     * Container's getter for AsMonitorLayoutA1.
     * @return AsMonitorLayoutA1
     */
    public ViewObjectImpl getAsMonitorLayoutA1() {
        return (ViewObjectImpl) findViewObject("AsMonitorLayoutA1");
    }

    /**
     * Container's getter for AsMonitorSchedulerBulletinsVO1.
     * @return AsMonitorSchedulerBulletinsVO1
     */
    public ViewObjectImpl getAsMonitorSchedulerBulletinsVO1() {
        return (ViewObjectImpl) findViewObject("AsMonitorSchedulerBulletinsVO1");
    }

    /**
     * Container's getter for AsMonitorAlrabyiaEmployeesVO1.
     * @return AsMonitorAlrabyiaEmployeesVO1
     */
    public ViewObjectImpl getAsMonitorAlrabyiaEmployeesVO1() {
        return (ViewObjectImpl) findViewObject("AsMonitorAlrabyiaEmployeesVO1");
    }

    /**
     * Container's getter for AsSchedulerMonitorLayoutACount1.
     * @return AsSchedulerMonitorLayoutACount1
     */
    public ViewObjectImpl getAsSchedulerMonitorLayoutACount1() {
        return (ViewObjectImpl) findViewObject("AsSchedulerMonitorLayoutACount1");
    }

    /**
     * Container's getter for AsMonitorLayoutC1.
     * @return AsMonitorLayoutC1
     */
    public ViewObjectImpl getAsMonitorLayoutC1() {
        return (ViewObjectImpl) findViewObject("AsMonitorLayoutC1");
    }

    /**
     * Container's getter for AsSchedulerLayoutCCount1.
     * @return AsSchedulerLayoutCCount1
     */
    public ViewObjectImpl getAsSchedulerLayoutCCount1() {
        return (ViewObjectImpl) findViewObject("AsSchedulerLayoutCCount1");
    }

    /**
     * Container's getter for AsSendEmailSMSVo1.
     * @return AsSendEmailSMSVo1
     */
    public AsSendEmailSMSVoImpl getAsSendEmailSMSVo1() {
        return (AsSendEmailSMSVoImpl) findViewObject("AsSendEmailSMSVo1");
    }

    /**
     * Container's getter for AsTrackEmailmessagesvo1.
     * @return AsTrackEmailmessagesvo1
     */
    public ViewObjectImpl getAsTrackEmailmessagesvo1() {
        return (ViewObjectImpl) findViewObject("AsTrackEmailmessagesvo1");
    }

    /**
     * Container's getter for AsTasMontioring1.
     * @return AsTasMontioring1
     */
    public ViewObjectImpl getAsTasMontioring1() {
        return (ViewObjectImpl) findViewObject("AsTasMontioring1");
    }

    /**
     * Container's getter for ActiveBulletinsVO1.
     * @return ActiveBulletinsVO1
     */
    public ViewObjectImpl getActiveBulletinsVO1() {
        return (ViewObjectImpl) findViewObject("ActiveBulletinsVO1");
    }


    /**
     * Container's getter for AsSchedulerStaffEmailsmsVO1.
     * @return AsSchedulerStaffEmailsmsVO1
     */
    public AsSchedulerStaffEmailsmsVOImpl getAsSchedulerStaffEmailsmsVO1() {
        return (AsSchedulerStaffEmailsmsVOImpl) findViewObject("AsSchedulerStaffEmailsmsVO1");
    }

    /**
     * Container's getter for AsSchedulerStaffEmailSmsVL1.
     * @return AsSchedulerStaffEmailSmsVL1
     */
    public ViewLinkImpl getAsSchedulerStaffEmailSmsVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedulerStaffEmailSmsVL1");
    }

    /**
     * Container's getter for AsShiftWorkgroupOrganizationVO1.
     * @return AsShiftWorkgroupOrganizationVO1
     */
    public AsShiftWorkgroupOrganizationVOImpl getAsShiftWorkgroupOrganizationVO1() {
        return (AsShiftWorkgroupOrganizationVOImpl) findViewObject("AsShiftWorkgroupOrganizationVO1");
    }

    /**
     * Container's getter for AsShiftCodevo1.
     * @return AsShiftCodevo1
     */
    public AsShiftCodevoImpl getAsShiftCodevo1() {
        return (AsShiftCodevoImpl) findViewObject("AsShiftCodevo1");
    }

    /**
     * Container's getter for AsWorkGroupsLinesVO1.
     * @return AsWorkGroupsLinesVO1
     */
    public AsWorkGroupsLinesVOImpl getAsWorkGroupsLinesVO1() {
        return (AsWorkGroupsLinesVOImpl) findViewObject("AsWorkGroupsLinesVO1");
    }

    /**
     * Container's getter for AsSchedulerStaffVO2.
     * @return AsSchedulerStaffVO2
     */
    public AsSchedulerStaffVOImpl getAsSchedulerStaffVO2() {
        return (AsSchedulerStaffVOImpl) findViewObject("AsSchedulerStaffVO2");
    }

    /**
     * Container's getter for SchedulerStaffWorkgroupViewLink.
     * @return SchedulerStaffWorkgroupViewLink
     */
    public ViewLinkImpl getSchedulerStaffWorkgroupViewLink() {
        return (ViewLinkImpl) findViewLink("SchedulerStaffWorkgroupViewLink");
    }

    /**
     * Container's getter for AsWorkGroupsLinesVO2.
     * @return AsWorkGroupsLinesVO2
     */
    public AsWorkGroupsLinesVOImpl getAsWorkGroupsLinesVO2() {
        return (AsWorkGroupsLinesVOImpl) findViewObject("AsWorkGroupsLinesVO2");
    }

    /**
     * Container's getter for AsSchedulerStaffVO3.
     * @return AsSchedulerStaffVO3
     */
    public AsSchedulerStaffVOImpl getAsSchedulerStaffVO3() {
        return (AsSchedulerStaffVOImpl) findViewObject("AsSchedulerStaffVO3");
    }

    /**
     * Container's getter for AsSchedulerStaffReplacementWkpVl.
     * @return AsSchedulerStaffReplacementWkpVl
     */
    public ViewLinkImpl getAsSchedulerStaffReplacementWkpVl() {
        return (ViewLinkImpl) findViewLink("AsSchedulerStaffReplacementWkpVl");
    }


    /**
     * Container's getter for DefaultWorkGroupvaluevo1.
     * @return DefaultWorkGroupvaluevo1
     */
    public DefaultWorkGroupvaluevoImpl getDefaultWorkGroupvaluevo1() {
        return (DefaultWorkGroupvaluevoImpl) findViewObject("DefaultWorkGroupvaluevo1");
    }

    /**
     * Container's getter for AsWorkgroupCategorylov1.
     * @return AsWorkgroupCategorylov1
     */
    public ViewObjectImpl getAsWorkgroupCategorylov1() {
        return (ViewObjectImpl) findViewObject("AsWorkgroupCategorylov1");
    }

    /**
     * Container's getter for AsSchdulerCurrentShiftworkrgroupvo1.
     * @return AsSchdulerCurrentShiftworkrgroupvo1
     */
    public AsSchdulerCurrentShiftworkrgroupvoImpl getAsSchdulerCurrentShiftworkrgroupvo1() {
        return (AsSchdulerCurrentShiftworkrgroupvoImpl) findViewObject("AsSchdulerCurrentShiftworkrgroupvo1");
    }

    /**
     * Container's getter for AsOnEmployeeVO1.
     * @return AsOnEmployeeVO1
     */
    public AsOnEmployeeVOImpl getAsOnEmployeeVO1() {
        return (AsOnEmployeeVOImpl) findViewObject("AsOnEmployeeVO1");
    }


    /**
     * Container's getter for AsAdminRoleVo1.
     * @return AsAdminRoleVo1
     */
    public AsAdminRoleVoImpl getAsAdminRoleVo1() {
        return (AsAdminRoleVoImpl) findViewObject("AsAdminRoleVo1");
    }

    /**
     * Container's getter for AsEmail_configurationvo1.
     * @return AsEmail_configurationvo1
     */
    public AsEmail_configurationvoImpl getAsEmail_configurationvo1() {
        return (AsEmail_configurationvoImpl) findViewObject("AsEmail_configurationvo1");
    }

    /**
     * Container's getter for AsEditorRoleVO1.
     * @return AsEditorRoleVO1
     */
    public AsEditorRoleVOImpl getAsEditorRoleVO1() {
        return (AsEditorRoleVOImpl) findViewObject("AsEditorRoleVO1");
    }

    /**
     * Container's getter for AsSchedulerShiftsVO1.
     * @return AsSchedulerShiftsVO1
     */
    public AsSchedulerShiftsVOImpl getAsSchedulerShiftsVO1() {
        return (AsSchedulerShiftsVOImpl) findViewObject("AsSchedulerShiftsVO1");
    }

    /**
     * Container's getter for AsSchedulerShiftWorkgroupvl.
     * @return AsSchedulerShiftWorkgroupvl
     */
    public ViewLinkImpl getAsSchedulerShiftWorkgroupvl() {
        return (ViewLinkImpl) findViewLink("AsSchedulerShiftWorkgroupvl");
    }


    /**
     * Container's getter for AsAllSchedulerShiftsVo1.
     * @return AsAllSchedulerShiftsVo1
     */
    public AsAllSchedulerShiftsVoImpl getAsAllSchedulerShiftsVo1() {
        return (AsAllSchedulerShiftsVoImpl) findViewObject("AsAllSchedulerShiftsVo1");
    }

    /**
     * Container's getter for AsAllSchedulerShiftVL1.
     * @return AsAllSchedulerShiftVL1
     */
    public ViewLinkImpl getAsAllSchedulerShiftVL1() {
        return (ViewLinkImpl) findViewLink("AsAllSchedulerShiftVL1");
    }

    /**
     * Container's getter for AsEmployeeContactInformationvo1.
     * @return AsEmployeeContactInformationvo1
     */
    public AsEmployeeContactInformationvoImpl getAsEmployeeContactInformationvo1() {
        return (AsEmployeeContactInformationvoImpl) findViewObject("AsEmployeeContactInformationvo1");
    }

    /**
     * Container's getter for AsSchedulerStaffHistoryVo1.
     * @return AsSchedulerStaffHistoryVo1
     */
    public AsSchedulerStaffHistoryVoImpl getAsSchedulerStaffHistoryVo1() {
        return (AsSchedulerStaffHistoryVoImpl) findViewObject("AsSchedulerStaffHistoryVo1");
    }


    /**
     * Container's getter for AsSchedulerStaffHistoryVo2.
     * @return AsSchedulerStaffHistoryVo2
     */
    public AsSchedulerStaffHistoryVoImpl getAsSchedulerStaffHistoryVo2() {
        return (AsSchedulerStaffHistoryVoImpl) findViewObject("AsSchedulerStaffHistoryVo2");
    }

    /**
     * Container's getter for AsSchedulerHistoryVL1.
     * @return AsSchedulerHistoryVL1
     */
    public ViewLinkImpl getAsSchedulerHistoryVL1() {
        return (ViewLinkImpl) findViewLink("AsSchedulerHistoryVL1");
    }
    
    // Scheduler Tracking //
    public void createTrackingSchedulerStaff(Number SchedulerStaffId, String ActionType, String Source) {
       
        AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)this.getAsSchedulerStaffVO().first();
  AsSchedulerStaffHistoryVoImpl vo=     this.getAsSchedulerStaffHistoryVo1();

   if(r.getSchedulerStaffId().intValue()!=0)
   {

  AsSchedulerStaffHistoryVoRowImpl h = (AsSchedulerStaffHistoryVoRowImpl) vo.createRow();
 
   h.setSchedulerId(r.getSchedulerId());
   h.setSchedulerStaffId(r.getSchedulerStaffId());
   h.setPersonId(r.getPersonId());
   h.setSchedularStartDate(r.getSchedularStartDate());
   h.setSchedularEndDate(r.getSchedularEndDate());
   h.setStartTimeHours(r.getStartTimeHours());
   h.setEndTimeHours(r.getEndTimeHours());
   h.setStartTimeMinutes(r.getStartTimeMinutes());
   h.setEndTimeMinutes(r.getEndTimeMinutes());
   // get created by from session 
         h.setCreatedBy(this.getLoggedPersonId());
   
   h.setRecurringFlag(r.getRecurringFlag());
   h.setBulletinFlag(r.getBulletinFlag());
   h.setParentSchedulerStaffId(r.getParentSchedulerStaffId());
   h.setWorkGroupId(r.getWorkGroupId());
   h.setShiftId(r.getShiftId());
   h.setNotes(r.getNotes());
   h.setFkSchdlrShiftAttributeId(r.getFkSchdlrShiftAttributeId());
   h.setFkSchedulerSectionId(r.getFkSchedulerSectionId());
        h.setReplaceType(r.getReplaceType());
   if(r.getReplaceType().equals("No Replacement"))
   {
       h.setFkReplacedById(null);
       h.setFkWkReplacedId(null );
   }
   else 
   {
        h.setFkReplacedById(r.getFkReplacedById());
        h.setFkWkReplacedId(r.getFkWkReplacedId() );
   }
  
   h.setEmailNotification(r.getEmailNotification());
   h.setSmsNotifiction(r.getSmsNotifiction());

   h.setActionType(ActionType);  
   /// Bulletins tracking History //

  if( r.getAsSchedulerStaffBulletinsVO().getRowCount()>0)
  {
          AsSchdlrStaffBulletinsHistVoImpl bulletinvo=    this.getAsSchdlrStaffBulletinsHistVo1();

         RowSet rs = (RowSet) r.getAttribute("AsSchedulerStaffBulletinsVO");
  
          //Iterate over child viewObject rows for corresponding master record
          while (rs.hasNext()) {
          Row rb = rs.next();
              AsSchdlrStaffBulletinsHistVoRowImpl bulletin= (AsSchdlrStaffBulletinsHistVoRowImpl) bulletinvo.createRow();
              bulletin.setCreatedBy(this.getLoggedPersonId());
              bulletin.setSchedulerStaffHistoryId(h.getHistoryId());
              bulletin.setSchedulerStaffId(h.getSchedulerStaffId());
              bulletin.setBulletinId((Number)rb.getAttribute("BulletinId"));
              bulletin.setSchedulerStaffBulletinId((Number)rb.getAttribute("SchedulerStaffBulletinId"));
          System.out.println(" Bulletin : " + rb.getAttribute("BulletinId") + " Scheduler Staff id  " +
          rb.getAttribute("SchedulerStaffId"));
          }
          rs.closeRowSet();
    
      
 

      
      
      
      
      
      
      
      }
  else 
  {
      
          System.out.println("No Bulletins");
      }
   
   
   }

    }
    
    
 
    public void ViewTrackHistory(String pstart_Date, Number ShiftattributeId)

        {


        AsSchedulerStaffHistoryVoImpl viewhidtory = this.getAsSchedulerStaffHistoryVo2();
        viewhidtory.setNamedWhereClauseParam("pstartdate", pstart_Date);
          viewhidtory.setNamedWhereClauseParam("pshiftattrid", ShiftattributeId);
              viewhidtory.executeQuery();
     

    }
    public Number getLoggedPersonId()
    {
            Map sessionscope = ADFContext.getCurrent().getSessionScope();
            Number personId = (Number) sessionscope.get("AAPersonId");
         return personId;
        
        }

    /**
     * Container's getter for AsSchdlrStaffBulletinsHistVo1.
     * @return AsSchdlrStaffBulletinsHistVo1
     */
    public AsSchdlrStaffBulletinsHistVoImpl getAsSchdlrStaffBulletinsHistVo1() {
        return (AsSchdlrStaffBulletinsHistVoImpl) findViewObject("AsSchdlrStaffBulletinsHistVo1");
    }

    /**
     * Container's getter for AsMultiOrgRoleVO1.
     * @return AsMultiOrgRoleVO1
     */
    public AsMultiOrgRoleVOImpl getAsMultiOrgRoleVO1() {
        return (AsMultiOrgRoleVOImpl) findViewObject("AsMultiOrgRoleVO1");
    }
}


package ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.StringWriter;

import java.net.URLDecoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Path("profile/info")
public class Profile {
    public Profile() {
        super();
    }

//    @POST
//    @Produces("application/json")
//    @Path("/getPicture")
    public Response getPicture (@Context SecurityContext sc, @HeaderParam("personId") String personId
                                )
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
//            if (1==1)  
//            throw new SecurityException("User is unauthorized.");
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select  mbc_mobile_pkg.BASE64ENCODE(  image ) image from per_images\n" + 
            "where table_name = 'PER_PEOPLE_F'\n" + 
            "and parent_id = :1\n" + 
            " ";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                Helper.logActivity(connection, personId, "0", "Profile Picture", "Picture" , null);
            statement = connection.prepareStatement(sqlStemt);
               
                statement.setString(1, personId);

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    @POST
    @Produces("application/json")
    @Path("/getEmploymentSummary")
    public Response getEmploymentSummary(@Context SecurityContext sc   
                                         ,@HeaderParam("wsid") String wsid
                                        ,@HeaderParam("signature") String signature
                               )
    {
        
        
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemtold="select papf.employee_number, papf.full_name, to_char(paaf.effective_start_date,'DD-MON-RRRR') assignment_start_date, pg.name grade, hao.name organization_name, papf.email_address, pp.name position_name, substr(pp.name,1,instr(pp.name,'.')-1) job_name, mgr.full_name manager\n" + 
            "from per_all_people_f papf, per_all_assignments_f paaf, per_grades pg, HR_ALL_ORGANIZATION_UNITS hao, PER_POSITIONS pp, per_all_people_f mgr\n" + 
            "where papf.person_id = paaf.person_id\n" + 
            "and papf.person_id = :1\n" + 
            "and paaf.grade_id = pg.grade_id(+)\n" + 
            "and paaf.organization_id = hao.organization_id(+)\n" + 
            "and paaf.position_id = pp.position_id(+)\n" + 
            "and paaf.supervisor_id = mgr.person_id(+)\n" + 
            "and papf.current_employee_flag = 'Y'\n" + 
            "and mgr.current_employee_flag(+) = 'Y'\n" + 
            "and paaf.primary_flag = 'Y'\n" + 
            "and sysdate between papf.effective_start_date and papf.effective_end_date\n" + 
            "and sysdate between mgr.effective_start_date and mgr.effective_end_date\n" + 
            "order by paaf.effective_start_date desc";
            String sqlStmt="with emp_summary as\n" + 
            "(select papf.employee_number, papf.full_name, to_char(paaf.effective_start_date,'DD-MON-RRRR') assignment_start_date, pg.name grade, hao.name department, papf.email_address, pp.name position_name, substr(pp.name,1,instr(pp.name,'.')-1) job_name, mgr.full_name manager,\n" + 
            "       ppg.segment1 directorate, ppg.segment2 working_company, ppg.segment3 company, ppg.segment4 division\n" + 
            "from per_all_people_f papf, per_all_assignments_f paaf, per_grades pg, HR_ALL_ORGANIZATION_UNITS hao, PER_POSITIONS pp, per_all_people_f mgr, pay_people_groups ppg\n" + 
            "where papf.person_id = paaf.person_id\n" + 
            "and papf.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and paaf.grade_id = pg.grade_id(+)\n" + 
            "and paaf.organization_id = hao.organization_id(+)\n" + 
            "and paaf.position_id = pp.position_id(+)\n" + 
            "and paaf.supervisor_id = mgr.person_id(+)\n" + 
            "and paaf.people_group_id = ppg.people_group_id(+)\n" + 
            "and papf.current_employee_flag = 'Y'\n" + 
            "and mgr.current_employee_flag(+) = 'Y'\n" + 
            "and paaf.primary_flag = 'Y'\n" + 
            "and sysdate between papf.effective_start_date and papf.effective_end_date\n" + 
            "and sysdate between mgr.effective_start_date and mgr.effective_end_date\n" + 
            "order by paaf.effective_start_date desc\n" + 
            ")\n" + 
            "select * from emp_summary\n" + 
            "where assignment_start_date in (select max(assignment_start_date) over(partition by job_name , grade , department , manager order by null) from emp_summary  )";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getEmploymentSummary");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open Employment Summary", "Open Employment Summary" );
              
            statement = connection.prepareStatement(sqlStmt);
               
              

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    @POST
    @Produces("application/json")
    @Path("/getBasicDetails")
    public Response getBasicDetails(@Context SecurityContext sc ,
                                @HeaderParam("wsid") String wsid,
                                @HeaderParam("signature") String signature
                               )
    {
        
        
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select papf.employee_number, papf.full_name, to_char (trunc(papf.date_of_birth),'DD-MON-RRRR') date_of_birth, TO_CHAR (papf.original_date_of_hire ,'DD-MON-RRRR') original_date_of_hire, papf.email_address, PAPF.NATIONAL_IDENTIFIER, trunc((sysdate-papf.date_of_birth)/365) age,\n" + 
            "    (select TO_CHAR(DATE_START,'DD-MON-RRRR') from per_periods_of_service where person_id = papf.person_id and  ACTUAL_TERMINATION_DATE is null ) hire_date,\n" + 
            "    (SELECT MEANING FROM APPS.FND_LOOKUP_VALUES L WHERE L.LOOKUP_TYPE = 'MAR_STATUS' AND LANGUAGE = 'US' AND L.LOOKUP_CODE = PAPF.MARITAL_STATUS) Marital_Status,\n" + 
            "    decode(papf.business_group_id,81,(SELECT MEANING FROM APPS.FND_LOOKUP_VALUES L WHERE L.LOOKUP_TYPE = 'AE_NATIONALITY' AND LANGUAGE = 'US' AND L.LOOKUP_CODE = PAPF.PER_INFORMATION18)\n" + 
            "                                 ,(SELECT MEANING FROM APPS.FND_LOOKUP_VALUES L WHERE L.LOOKUP_TYPE = 'NATIONALITY' AND LANGUAGE = 'US' AND L.LOOKUP_CODE = papf.nationality)) Nationality ,\n" + 
            "    decode(papf.business_group_id,81,(SELECT MEANING FROM APPS.FND_LOOKUP_VALUES L WHERE L.LOOKUP_TYPE = 'AE_RELIGION' AND LANGUAGE = 'US' AND L.LOOKUP_CODE = PAPF.PER_INFORMATION10)\n" + 
            "                                 ,(SELECT MEANING FROM APPS.FND_LOOKUP_VALUES L WHERE L.LOOKUP_TYPE = 'SA_RELIGION' AND LANGUAGE = 'US' AND L.LOOKUP_CODE = papf.PER_INFORMATION10 ),null) Religion ,\n" + 
            "                                 (SELECT MEANING FROM APPS.FND_LOOKUP_VALUES L WHERE L.LOOKUP_TYPE = 'SEX' AND LANGUAGE = 'US' AND L.LOOKUP_CODE = PAPF.sex and rownum=1) gender,\n" + 
            "                                 (select mbc_mobile_pkg.base64encode( image) from per_images where table_name = 'PER_PEOPLE_F' and parent_id = papf.person_id) picture\n" + 
            "from Per_all_people_f papf\n" + 
            "where 1=1\n" + 
            "and person_id = mbc_mobile_pkg.get_session_data('PERSON_ID')\n" + 
            "and sysdate between papf.effective_start_date and papf.effective_end_Date\n" + 
            "and papf.current_employee_flag = 'Y'";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
               
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getBasicDetails");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open Basic Details", "Open Basic Details" );
            statement = connection.prepareStatement(sqlStemt);
               
              

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs,"1");
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    @POST
    @Produces("application/json")
    @Path("getEducation")
    public Response getEducation (@Context SecurityContext sc
                                 ,@HeaderParam("wsid") String wsid
                                 ,@HeaderParam("signature") String signature
                               )
    {
        
        
      
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select Distinct to_number(p.employee_number) \"EmpNumber\",p.full_name,q.Name Qual_Name, q.title,\n" + 
            "    q.status, (select meaning from apps.fnd_lookup_values where lookup_type = 'PER_SUBJECT_STATUSES' and language = 'US' and lookup_code = q.status) status,\n" + 
            "    q.Qualification_Type_Id, Q.ESTABLISHMENT ,q.Awarded_date,q.End_Date, p.effective_start_date, p.original_date_of_hire, q.creation_date \"Qualification Creation Date\"\n" + 
            "from apps.PER_QUALIFICATIONS_V q, apps.per_all_people_f p\n" + 
            "where p.business_group_id = q.business_group_id\n" + 
            "and p.person_id = q.person_id\n" + 
            "and p.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and p.current_employee_flag= 'Y'\n" + 
            "and sysdate between p.effective_start_date and p.effective_end_date\n" + 
            "order by 1";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getEducation");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open get Education", "Open get Education " );
                
                
            statement = connection.prepareStatement(sqlStemt);
               
               

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    

    
    //////////////////////////////
    @POST
    @Produces("application/json")
    @Path("/getPhones")
    public Response getPhones (@Context SecurityContext sc 
                               ,@HeaderParam("wsid") String wsid
                               ,@HeaderParam("signature") String signature)
                                
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="SELECT hl.meaning PHONE_TYPE, PHONE_NUMBER, to_char (pp.date_from,'DD-MON-RRRR') date_from, to_char (pp.date_to,'DD-MON-RRRR')  date_to\n" + 
            "FROM APPS.PER_PHONES pp , hr_lookups hl\n" + 
            "WHERE PARENT_TABLE = 'PER_ALL_PEOPLE_F'\n" + 
            "AND SYSDATE BETWEEN DATE_FROM\n" + 
            "AND NVL(DATE_TO,TO_DATE('01-01-3000','dd-mm-yyyy'))\n" + 
            "and parent_id = mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and hl.lookup_type = 'PHONE_TYPE'\n" + 
            "and hl.lookup_code = pp.PHONE_TYPE";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getPhones");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open get Phones" , "Open get Phones " );

              
            statement = connection.prepareStatement(sqlStemt);
               
              

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    ///////////////////////////
    
    
    //////////////////////////////
    @POST
    @Produces("application/json")
    @Path("/getAddresses")
    public Response getAddresses(@Context SecurityContext sc
                                ,@HeaderParam("wsid") String wsid
                                ,@HeaderParam("signature") String signature
                                ,@HeaderParam("bu") String businessGroup)
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            
            final String UAE="select decode(PRIMARY_FLAG,'Y','Yes','No') Primary_address, ADDRESS_LINE1, ADDRESS_LINE2,\n" + 
            "       (select meaning from HR_LOOKUPS WHERE lookup_type = 'AE_EMIRATE' AND enabled_flag = 'Y' AND  lookup_code = pa.ADDRESS_LINE3 and application_id = 800) Emirate,\n" + 
            "       REGION_2 Street,\n" + 
            "       REGION_3 Building,\n" + 
            "       ADD_INFORMATION14 flat_number,\n" + 
            "       ADD_INFORMATION15 PO_BOX,\n" + 
            "       (select TERRITORY_SHORT_NAME from FND_TERRITORIES_VL WHERE OBSOLETE_FLAG<>'Y' and TERRITORY_CODE = pa.COUNTRY) COUNTRY\n" + 
            "from PER_ADDRESSES pa\n" + 
            "where pa.person_id = :1\n" + 
            "and sysdate between pa.date_from and nvl(pa.date_to, to_date('31-12-3000','dd-mm-yyyy'))";
            
            final String KSA="select decode(PRIMARY_FLAG,'Y','Yes','No') Primary_address, ADDRESS_LINE1, ADDRESS_LINE2,\n" + 
            "       REGION_1 Street,\n" + 
            "       REGION_2 Area,\n" + 
            "       REGION_3 PO_Box,\n" + 
            "       (select meaning from HR_LOOKUPS WHERE LOOKUP_TYPE = 'SA_CITY' AND ENABLED_FLAG = 'Y' AND APPLICATION_ID = 800 and lookup_code = TOWN_OR_CITY) City,\n" + 
            "       POSTAL_CODE ,\n" + 
            "       (select TERRITORY_SHORT_NAME from FND_TERRITORIES_VL WHERE OBSOLETE_FLAG<>'Y' and TERRITORY_CODE = pa.COUNTRY) COUNTRY\n" + 
            "from PER_ADDRESSES pa\n" + 
            "where pa.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and sysdate between pa.date_from and nvl(pa.date_to, to_date('31-12-3000','dd-mm-yyyy'))";
            
            final String JORDAN="select decode(PRIMARY_FLAG,'Y','Yes','No') Primary_address,\n" + 
            "       (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1020095 and FLEX_VALUE_ID = REGION_1) Governorate,\n" + 
            "       TOWN_OR_CITY City_English,\n" + 
            "       ADDRESS_LINE2 City_arabic,\n" + 
            "       ADDRESS_LINE1 Street_English,\n" + 
            "       ADDRESS_LINE3 Street_Arabic,\n" + 
            "       ADD_INFORMATION13 po_box,\n" + 
            "       TELEPHONE_NUMBER_1 phone\n" + 
            "from PER_ADDRESSES pa\n" + 
            "where pa.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and sysdate between pa.date_from and nvl(pa.date_to, to_date('31-12-3000','dd-mm-yyyy'))";
            
            final String EGYPT_LEBANON="select decode(PRIMARY_FLAG,'Y','Yes','No') Primary_address,\n" + 
            "       ADDRESS_LINE1, ADDRESS_LINE2, ADDRESS_LINE3, TOWN_OR_CITY, REGION_1, REGION_2, REGION_3, COUNTRY, POSTAL_CODE, TELEPHONE_NUMBER_1, TELEPHONE_NUMBER_2\n" + 
            "from PER_ADDRESSES pa\n" + 
            "where pa.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and sysdate between pa.date_from and nvl(pa.date_to, to_date('31-12-3000','dd-mm-yyyy'))\n";
            String sqlStemt="";
            
            if ("81".equalsIgnoreCase(businessGroup))
                sqlStemt=UAE;
            else if ("998".equalsIgnoreCase(businessGroup))
                sqlStemt=KSA;
            else if ("638".equalsIgnoreCase(businessGroup))
                sqlStemt=JORDAN;
            else if ("169".equalsIgnoreCase(businessGroup)||"205".equalsIgnoreCase(businessGroup) )
                sqlStemt=EGYPT_LEBANON;
            else
                return response;
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getAddresses");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open get Addresses", "Open get Addresses" );
               
            statement = connection.prepareStatement(sqlStemt);
               
               

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    ///////////////////////////
    
    
    //////////////////////////////
    @POST
    @Produces("application/json")
    @Path("/getPassportInfo")
    public Response getPassportInfo (@Context SecurityContext sc 
                                    ,@HeaderParam("wsid") String wsid
                                    ,@HeaderParam("signature") String signature)
                                
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select distinct to_number(x.employee_number) \"EmpNumber\", x.full_name,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013456 and FLEX_VALUE = y.SEGMENT1) Passport_For,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013467 and FLEX_VALUE = y.SEGMENT2) Passport_Type,\n" + 
            "        SEGMENT3 Name_as_passport,\n" + 
            "        SEGMENT4 Passport_Number,\n" + 
            "        to_char( to_date (SEGMENT5   ,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR')  Issue_Date,\n" + 
            "        to_char( to_date (SEGMENT6   ,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Expiry_Date,\n" + 
            "        SEGMENT7 Place_of_Issue,          \n" + 
            "        (Select distinct lv.meaning from apps.fnd_lookup_values lv where lv.LOOKUP_CODE = y.SEGMENT8 and upper(lv.LOOKUP_TYPE) = 'AE_NATIONALITY' AND lv.LANGUAGE = 'US')    Passport_Country    \n" + 
            "from apps.per_all_people_f x ,\n" + 
            "    (select p.employee_number, c.SEGMENT1, c.SEGMENT2, c.SEGMENT3, c.SEGMENT4, c.SEGMENT5, c.SEGMENT6, c.SEGMENT7, c.SEGMENT8\n" + 
            "    from apps.per_all_people_f p, apps.PER_PERSON_ANALYSES n, apps.PER_ANALYSIS_CRITERIA c\n" + 
            "    where 1=1\n" + 
            "    and p.person_id = n.person_id\n" + 
            "    and p.current_employee_flag = 'Y'\n" + 
            "    and p.person_id = mbc_mobile_pkg.get_session_data('PERSON_ID')\n" + 
            "    and p.business_group_id = n.business_group_id\n" + 
            "    and sysdate between p.effective_start_date and p.effective_end_date\n" + 
            "    and sysdate between n.date_from and nvl(n.date_to,to_date('01-01-3000','dd-mm-yyyy'))\n" + 
            "    AND n.ANALYSIS_CRITERIA_ID = c.ANALYSIS_CRITERIA_ID\n" + 
            "    and n.Id_Flex_Num = 50349\n" + 
            "    and n.id_flex_num = c.id_flex_num) y\n" + 
            "where 1=1\n" + 
            "and   x.current_employee_flag = 'Y'\n" + 
            "and   x.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and   sysdate between x.effective_start_date and x.effective_end_date\n" + 
            "and   x.employee_number = y.employee_number(+)\n" + 
            "order by 1";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getPassportInfo");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open Profile Passport Info", "Open Profile Passport Info " );
                
               
            statement = connection.prepareStatement(sqlStemt);
            

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    ///////////////////////////
    
    
    //////////////////////////////
    @POST
    @Produces("application/json")
    @Path("/getDependentVisa")
    public Response getDependentVisa(@Context SecurityContext sc
                                    ,@HeaderParam("wsid") String wsid
                                    ,@HeaderParam("signature") String signature)
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select distinct to_number(x.employee_number) \"EmpNumber\", x.full_name,\n" + 
            "        SEGMENT13 Dependent_Name,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013488 and FLEX_VALUE = y.SEGMENT1) Visa_Type,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013527 and FLEX_VALUE = y.SEGMENT9) Other,\n" + 
            "        SEGMENT10 Sponsor_Name,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013456 and FLEX_VALUE = y.SEGMENT11) Relation_to_sponsor,\n" + 
            "        SEGMENT3 Visa_Number,\n" + 
            "        SEGMENT12 Visa_Issue_Place,\n" + 
            "       to_char( to_date ( SEGMENT4   ,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Visa_Issue_Date,\n" + 
            "       to_char( to_date ( SEGMENT5   ,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR')  Visa_Expiry_Date,\n" + 
            "        SEGMENT6 Visa_Cost,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013490 and FLEX_VALUE = y.SEGMENT7) Visa_Cancellation_Type,\n" + 
            "        SEGMENT8 Comments       \n" + 
            "from apps.per_all_people_f x ,\n" + 
            "    (select p.employee_number, c.SEGMENT1, c.SEGMENT2, c.SEGMENT3, c.SEGMENT4, c.SEGMENT5, c.SEGMENT6, c.SEGMENT7, c.SEGMENT8,c.SEGMENT9,c.SEGMENT10,c.SEGMENT11,c.SEGMENT12,c.SEGMENT13\n" + 
            "    from apps.per_all_people_f p, apps.PER_PERSON_ANALYSES n, apps.PER_ANALYSIS_CRITERIA c\n" + 
            "    where 1=1\n" + 
            "    and p.person_id = n.person_id\n" + 
            "    and p.current_employee_flag = 'Y'\n" + 
            "    and p.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "    and p.business_group_id = n.business_group_id\n" + 
            "    and sysdate between p.effective_start_date and p.effective_end_date\n" + 
            "    and sysdate between n.date_from and nvl(n.date_to,to_date('01-01-3000','dd-mm-yyyy'))\n" + 
            "    AND n.ANALYSIS_CRITERIA_ID = c.ANALYSIS_CRITERIA_ID\n" + 
            "    and n.Id_Flex_Num = 50371\n" + 
            "    and n.id_flex_num = c.id_flex_num) y\n" + 
            "where 1=1\n" + 
            "and   x.current_employee_flag = 'Y'\n" + 
            "and   x.person_id = mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and   sysdate between x.effective_start_date and x.effective_end_date\n" + 
            "and   x.employee_number = y.employee_number(+)" +
            "order by 1";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                

                String result=Helper.checkSession(connection, wsid, signature , wsid+"getDependentVisa");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open Employee Dependent Visa" , "Open Employee Dependent Visa" );
                
                
              
            statement = connection.prepareStatement(sqlStemt);
               
             
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    ///////////////////////////
    
    
    //////////////////////////////
    @POST
    @Produces("application/json")
    @Path("/getDependentTickets")
    public Response getDependentTickets(@Context SecurityContext sc 
                                       ,@HeaderParam("wsid") String wsid
                                       ,@HeaderParam("signature") String signature)
                               
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select distinct to_number(x.employee_number) \"EmpNumber\", x.full_name,\n" + 
            "        SEGMENT1 Name,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013456 and FLEX_VALUE = y.SEGMENT2) Relationship,\n" + 
            "          to_char( to_date (SEGMENT3   ,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Date_of_Birth,\n" + 
            "        to_char( to_date (SEGMENT4  ,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Effective_Payment_Date\n" + 
            "from apps.per_all_people_f x ,\n" + 
            "    (select p.employee_number, c.SEGMENT1, c.SEGMENT2, c.SEGMENT3, c.SEGMENT4, c.SEGMENT5, c.SEGMENT6, c.SEGMENT7, c.SEGMENT8,c.SEGMENT9,c.SEGMENT10,c.SEGMENT11,c.SEGMENT12,c.SEGMENT13\n" + 
            "    from apps.per_all_people_f p, apps.PER_PERSON_ANALYSES n, apps.PER_ANALYSIS_CRITERIA c\n" + 
            "    where 1=1\n" + 
            "    and p.person_id = n.person_id\n" + 
            "    and p.current_employee_flag = 'Y'\n" + 
            "    and p.person_id = mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "    and p.business_group_id = n.business_group_id\n" + 
            "    and sysdate between p.effective_start_date and p.effective_end_date\n" + 
            "    and sysdate between n.date_from and nvl(n.date_to,to_date('01-01-3000','dd-mm-yyyy'))\n" + 
            "    AND n.ANALYSIS_CRITERIA_ID = c.ANALYSIS_CRITERIA_ID\n" + 
            "    and n.Id_Flex_Num = 50322\n" + 
            "    and n.id_flex_num = c.id_flex_num) y\n" + 
            "where 1=1\n" + 
            "and   x.current_employee_flag = 'Y'\n" + 
            "and   x.person_id = mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and   sysdate between x.effective_start_date and x.effective_end_date\n" + 
            "and   x.employee_number = y.employee_number(+)\n" + 
            "order by 1";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getDependentTickets");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open Dependent Tickets" , "Open Dependent Tickets" );
               
            statement = connection.prepareStatement(sqlStemt);
               
              


               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    ///////////////////////////
    
    
    //////////////////////////////
    @POST
    @Produces("application/json")
    @Path("/getVisaInfo")
    public Response getVisaInfo (@Context SecurityContext sc 
                                ,@HeaderParam("wsid") String wsid
                                ,@HeaderParam("signature") String signature
                                    )
                                
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select distinct to_number(x.employee_number) \"EmpNumber\", x.full_name,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013488 and FLEX_VALUE = y.SEGMENT5) Visa_Type,\n" + 
            "        SEGMENT6 Sponsored_By_Others,\n" + 
            "        SEGMENT7 Sponsor_Name,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013456 and FLEX_VALUE = y.SEGMENT11) Relation_to_sponsor,\n" + 
            "        SEGMENT1 Visa_Number,\n" + 
            "        SEGMENT12 Visa_Issue_Place,\n" + 
            "       to_char( to_date (SEGMENT2,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Visa_Issue_Date,\n" + 
            "       to_char( to_date (SEGMENT3,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Visa_Expiry_Date,\n" + 
            "        SEGMENT9 Visa_Cost,\n" + 
            "        (select FLEX_VALUE from fnd_flex_values where flex_value_set_id = 1013490 and FLEX_VALUE = y.SEGMENT10) Visa_Cancellation_Type,\n" + 
            "        SEGMENT4 Comments       \n" + 
            "from apps.per_all_people_f x ,\n" + 
            "    (select p.employee_number, c.SEGMENT1, c.SEGMENT2, c.SEGMENT3, c.SEGMENT4, c.SEGMENT5, c.SEGMENT6, c.SEGMENT7, c.SEGMENT8,c.SEGMENT9,c.SEGMENT10,c.SEGMENT11,c.SEGMENT12,c.SEGMENT13\n" + 
            "    from apps.per_all_people_f p, apps.PER_PERSON_ANALYSES n, apps.PER_ANALYSIS_CRITERIA c\n" + 
            "    where 1=1\n" + 
            "    and p.person_id = n.person_id\n" + 
            "    and p.current_employee_flag = 'Y'\n" + 
            "    and p.person_id = mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "    and p.business_group_id = n.business_group_id\n" + 
            "    and sysdate between p.effective_start_date and p.effective_end_date\n" + 
            "    and sysdate between n.date_from and nvl(n.date_to,to_date('01-01-3000','dd-mm-yyyy'))\n" + 
            "    AND n.ANALYSIS_CRITERIA_ID = c.ANALYSIS_CRITERIA_ID\n" + 
            "    and n.Id_Flex_Num = 50352\n" + 
            "    and n.id_flex_num = c.id_flex_num) y\n" + 
            "where 1=1\n" + 
            "and   x.current_employee_flag = 'Y'\n" + 
            "and   x.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and   sysdate between x.effective_start_date and x.effective_end_date\n" + 
            "and   x.employee_number = y.employee_number(+)\n" + 
            "order by 1";
            try {
        
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getVisaInfo");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open Employee Visa Info" , "Open  Employee Visa Info" );
            
              
            statement = connection.prepareStatement(sqlStemt);
               
             
               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    ///////////////////////////
    
    //////////////////////////////
    @POST
    @Produces("application/json")
    @Path("/getLaborCardInfo")
    public Response getLaborCardInfo (@Context SecurityContext sc 
                                     ,@HeaderParam("wsid") String wsid
                                     ,@HeaderParam("signature") String signature)
                              
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select distinct to_number(x.employee_number) \"EmpNumber\", x.full_name,\n" + 
            "        SEGMENT1 Labor_card_Number,\n" + 
            "         to_char( to_date ( SEGMENT2,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR')  Labor_card_Issue_Date,\n" + 
            "         to_char( to_date (SEGMENT3 ,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Labor_card_Expiry_Date,\n" + 
            "        SEGMENT4 Comments\n" + 
            "from apps.per_all_people_f x ,\n" + 
            "    (select p.employee_number, c.SEGMENT1, c.SEGMENT2, c.SEGMENT3, c.SEGMENT4, c.SEGMENT5, c.SEGMENT6, c.SEGMENT7, c.SEGMENT8,c.SEGMENT9,c.SEGMENT10,c.SEGMENT11,c.SEGMENT12,c.SEGMENT13\n" + 
            "    from apps.per_all_people_f p, apps.PER_PERSON_ANALYSES n, apps.PER_ANALYSIS_CRITERIA c\n" + 
            "    where 1=1\n" + 
            "    and p.person_id = n.person_id\n" + 
            "    and p.current_employee_flag = 'Y'\n" + 
            "    and p.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "    and p.business_group_id = n.business_group_id\n" + 
            "    and sysdate between p.effective_start_date and p.effective_end_date\n" + 
            "    and sysdate between n.date_from and nvl(n.date_to,to_date('01-01-3000','dd-mm-yyyy'))\n" + 
            "    AND n.ANALYSIS_CRITERIA_ID = c.ANALYSIS_CRITERIA_ID\n" + 
            "    and n.Id_Flex_Num = 50326\n" + 
            "    and n.id_flex_num = c.id_flex_num) y\n" + 
            "where 1=1\n" + 
            "and   x.current_employee_flag = 'Y'\n" + 
            "and   x.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and   sysdate between x.effective_start_date and x.effective_end_date\n" + 
            "and   x.employee_number = y.employee_number(+)\n" + 
            "order by 1";
            try {
      
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getLaborCardInfo");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open Labor Card Info" , "Open Labor Card Info" );
                                                                      
                
              
            statement = connection.prepareStatement(sqlStemt);
               

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    ///////////////////////////
    
    //////////////////////////////
    @POST
    @Produces("application/json")
    @Path("/getRelationsInMBC")
    public Response getRelationsInMBC(@Context SecurityContext sc
                                     ,@HeaderParam("wsid") String wsid
                                     ,@HeaderParam("signature") String signature)
                              
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select distinct to_number(x.employee_number) \"EmpNumber\", x.full_name,\n" + 
            "        SEGMENT1 NAME,\n" + 
            "        SEGMENT2 Relationship,\n" + 
            "        SEGMENT3 Department_Directorate\n" + 
            "from apps.per_all_people_f x ,\n" + 
            "    (select p.employee_number, c.SEGMENT1, c.SEGMENT2, c.SEGMENT3, c.SEGMENT4, c.SEGMENT5, c.SEGMENT6, c.SEGMENT7, c.SEGMENT8,c.SEGMENT9,c.SEGMENT10,c.SEGMENT11,c.SEGMENT12,c.SEGMENT13\n" + 
            "    from apps.per_all_people_f p, apps.PER_PERSON_ANALYSES n, apps.PER_ANALYSIS_CRITERIA c\n" + 
            "    where 1=1\n" + 
            "    and p.person_id = n.person_id\n" + 
            "    and p.current_employee_flag = 'Y'\n" + 
            "    and p.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "    and p.business_group_id = n.business_group_id\n" + 
            "    and sysdate between p.effective_start_date and p.effective_end_date\n" + 
            "    and sysdate between n.date_from and nvl(n.date_to,to_date('01-01-3000','dd-mm-yyyy'))\n" + 
            "    AND n.ANALYSIS_CRITERIA_ID = c.ANALYSIS_CRITERIA_ID\n" + 
            "    and n.Id_Flex_Num = 50351\n" + 
            "    and n.id_flex_num = c.id_flex_num) y\n" + 
            "where 1=1\n" + 
            "and   x.current_employee_flag = 'Y'\n" + 
            "and   x.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and   sysdate between x.effective_start_date and x.effective_end_date\n" + 
            "and   x.employee_number = y.employee_number(+)\n" + 
            "order by 1";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getRelationsInMBC");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open Relations In MBC" , "Open Relations In MBC" );
                                                                      
              
            statement = connection.prepareStatement(sqlStemt);
               
              

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

    
    ///////////////////////////



    @POST
    @Produces("application/json")
    @Path("/getNationalIdCard")
    public Response getNationalIdCard(@Context SecurityContext sc 
                                    ,@HeaderParam("wsid") String wsid
                                    ,@HeaderParam("signature") String signature)
                               
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select distinct to_number(x.employee_number) \"EmpNumber\", x.full_name,\n" + 
            "        SEGMENT1 NAME,\n" + 
            "        SEGMENT2 Passport_Number,\n" + 
            "      to_char( to_date (SEGMENT3,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Issue_Date,\n" + 
            "      to_char( to_date (SEGMENT4,'RRRR/MM/DD HH24:MI:SS'),'DD-MON-RRRR') Expiry_Date,\n" + 
            "        SEGMENT5 Place_of_Issue,\n" + 
            "        (select TERRITORY_SHORT_NAME from FND_TERRITORIES_VL WHERE OBSOLETE_FLAG<>'Y' and TERRITORY_CODE = SEGMENT6) Nationality ,\n" + 
            "        SEGMENT7 ID_Card_Number\n" + 
            "from apps.per_all_people_f x ,\n" + 
            "    (select p.employee_number, c.SEGMENT1, c.SEGMENT2, c.SEGMENT3, c.SEGMENT4, c.SEGMENT5, c.SEGMENT6, c.SEGMENT7, c.SEGMENT8,c.SEGMENT9,c.SEGMENT10,c.SEGMENT11,c.SEGMENT12,c.SEGMENT13\n" + 
            "    from apps.per_all_people_f p, apps.PER_PERSON_ANALYSES n, apps.PER_ANALYSIS_CRITERIA c\n" + 
            "    where 1=1\n" + 
            "    and p.person_id = n.person_id\n" + 
            "    and p.current_employee_flag = 'Y'\n" + 
            "    and p.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "    and p.business_group_id = n.business_group_id\n" + 
            "    and sysdate between p.effective_start_date and p.effective_end_date\n" + 
            "    and sysdate between n.date_from and nvl(n.date_to,to_date('01-01-3000','dd-mm-yyyy'))\n" + 
            "    AND n.ANALYSIS_CRITERIA_ID = c.ANALYSIS_CRITERIA_ID\n" + 
            "    and n.Id_Flex_Num = 50329\n" + 
            "    and n.id_flex_num = c.id_flex_num) y\n" + 
            "where 1=1\n" + 
            "and   x.current_employee_flag = 'Y'\n" + 
            "and   x.person_id =  mbc_mobile_pkg.get_session_data('PERSON_ID') \n" + 
            "and   sysdate between x.effective_start_date and x.effective_end_date\n" + 
            "and   x.employee_number = y.employee_number(+)\n" + 
            "order by 1";
            try {
         //   connection = getConnection();
                connection = Helper.getOracleConnection();
                
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getNationalIdCard");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open National Id Card" , "Open National Id Card" );
                                                                      
                
            statement = connection.prepareStatement(sqlStemt);
               
               

               
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

}

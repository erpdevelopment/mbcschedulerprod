ALTER TABLE APPS.AS_ASSIGNMENT_EMPS
 DROP PRIMARY KEY CASCADE;

DROP TABLE APPS.AS_ASSIGNMENT_EMPS CASCADE CONSTRAINTS;

CREATE TABLE APPS.AS_ASSIGNMENT_EMPS
(
  ASSIGNMENT_EMP_ID  NUMBER,
  ASSIGNMENT_ID      NUMBER,
  PERSON_ID          NUMBER,
  EMP_START_DATE     DATE,
  EMP_END_DATE       DATE,
  NOTES              VARCHAR2(256 BYTE),
  CREATION_DATE      DATE,
  CREATED_BY         NUMBER,
  LAST_UPDATE_DATE   DATE,
  LAST_UPDATED_BY    NUMBER,
  LAST_UPDATE_LOGIN  NUMBER
)
TABLESPACE APPS_TS_TX_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX APPS.AS_ASSIGNMENT_EMPS_PK ON APPS.AS_ASSIGNMENT_EMPS
(ASSIGNMENT_EMP_ID)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


ALTER TABLE APPS.AS_ASSIGNMENT_EMPS ADD (
  CONSTRAINT AS_ASSIGNMENT_EMPS_PK
  PRIMARY KEY
  (ASSIGNMENT_EMP_ID)
  USING INDEX APPS.AS_ASSIGNMENT_EMPS_PK
  ENABLE VALIDATE);

package com.mbc.oracle.scheduler.model.eo;

import java.util.Map;

import oracle.adf.share.ADFContext;

import oracle.jbo.AttributeList;
import oracle.jbo.Key;
import oracle.jbo.RowIterator;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.SequenceImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Mar 05 16:20:44 GST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AsTemplatesEOImpl extends EntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        TemplateId,
        TemplateName,
        FkOrganizationId,
        FkWorkGroupId;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AsTemplatesEOImpl.AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AsTemplatesEOImpl.AttributesEnum.firstIndex() +
                   AsTemplatesEOImpl.AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AsTemplatesEOImpl.AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int TEMPLATEID = AttributesEnum.TemplateId.index();
    public static final int TEMPLATENAME = AttributesEnum.TemplateName.index();
    public static final int FKORGANIZATIONID = AttributesEnum.FkOrganizationId.index();
    public static final int FKWORKGROUPID = AttributesEnum.FkWorkGroupId.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AsTemplatesEOImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("com.mbc.oracle.scheduler.model.eo.AsTemplatesEO");
    }


    /**
     * Gets the attribute value for TemplateId, using the alias name TemplateId.
     * @return the value of TemplateId
     */
    public Number getTemplateId() {
        return (Number) getAttributeInternal(TEMPLATEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for TemplateId.
     * @param value value to set the TemplateId
     */
    public void setTemplateId(Number value) {
        setAttributeInternal(TEMPLATEID, value);
    }

    /**
     * Gets the attribute value for TemplateName, using the alias name TemplateName.
     * @return the value of TemplateName
     */
    public String getTemplateName() {
        return (String) getAttributeInternal(TEMPLATENAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for TemplateName.
     * @param value value to set the TemplateName
     */
    public void setTemplateName(String value) {
        setAttributeInternal(TEMPLATENAME, value);
    }

    /**
     * Gets the attribute value for FkOrganizationId, using the alias name FkOrganizationId.
     * @return the value of FkOrganizationId
     */
    public Number getFkOrganizationId() {
        return (Number) getAttributeInternal(FKORGANIZATIONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FkOrganizationId.
     * @param value value to set the FkOrganizationId
     */
    public void setFkOrganizationId(Number value) {
        setAttributeInternal(FKORGANIZATIONID, value);
    }


    /**
     * Gets the attribute value for FkWorkGroupId, using the alias name FkWorkGroupId.
     * @return the value of FkWorkGroupId
     */
    public Number getFkWorkGroupId() {
        return (Number) getAttributeInternal(FKWORKGROUPID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FkWorkGroupId.
     * @param value value to set the FkWorkGroupId
     */
    public void setFkWorkGroupId(Number value) {
        setAttributeInternal(FKWORKGROUPID, value);
    }


    /**
     * @param templateId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(Number templateId) {
        return new Key(new Object[] { templateId });
    }

    /**
     * Add attribute defaulting logic in this method.
     * @param attributeList list of attribute names/values to initialize the row
     */
    protected void create(AttributeList attributeList) {
        super.create(attributeList);
        
        SequenceImpl seq = new SequenceImpl("as_templates_seq", this.getDBTransaction());
        this.setTemplateId(seq.getSequenceNumber());
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        // Set Org ID 
        Number OrgId = (Number) sessionscope.get("AAOrganization");
        this.setFkOrganizationId(OrgId);
    }

    /**
     * Add entity remove logic in this method.
     */
    public void remove() {
        super.remove();
    }

    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        super.doDML(operation, e);
    }
}




CREATE OR REPLACE PACKAGE APPS.AS_NOTIFICATIONS_PKG
IS
   GC$CRLF                           VARCHAR2 (2) := CHR (13) || CHR (10);
   GC$MIME_TYPE                      VARCHAR2 (128) := 'text/html; charset="windows-1256"';
   SMTP_HOST                         VARCHAR2 (256) := 'smtp.mbc.net'; -- '10.32.209.246';--'10.32.208.27'; --' '192.168.1.30'; --
   SMTP_PORT                         PLS_INTEGER := 25;
   SMTP_DOMAIN                       VARCHAR2 (256) := 'smtp.mbc.net'; --'10.32.209.246'; --'10.32.208.27'; -- '192.168.1.30'; --
   MAILER_ID                CONSTANT VARCHAR2 (256) := 'Mailer by Oracle UTL_SMTP';
   GC$BOUNDARY              CONSTANT VARCHAR2 (256) := '__7D81B75CCC90D2974F7A1CBD__';
   FIRST_BOUNDARY           CONSTANT VARCHAR2 (256)
                                        := GC$CRLF || '--' || GC$BOUNDARY || GC$CRLF ;
   LAST_BOUNDARY            CONSTANT VARCHAR2 (256)
      := GC$CRLF || '--' || GC$BOUNDARY || '--' || GC$CRLF ;
   GC$MULTIPART_MIME_TYPE   CONSTANT VARCHAR2 (256)
      :=    'multipart/mixed; '
         || UTL_TCP.CRLF
         || ' boundary= "'
         || GC$BOUNDARY
         || '"'
         || GC$CRLF ;
   MAX_BASE64_LINE_WIDTH    CONSTANT PLS_INTEGER := 76 / 4 * 3;

   FUNCTION GET_ADDRESS (ADDR_LIST IN OUT VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION INIT_MAIL (SENDER       IN VARCHAR2,
                       RECIPIENTS   IN VARCHAR2,
                       CC           IN VARCHAR2 DEFAULT NULL,
                       BCC          IN VARCHAR2 DEFAULT NULL,
                       SUBJECT      IN VARCHAR2,
                       MIME_TYPE    IN VARCHAR2 DEFAULT GC$MIME_TYPE,
                       PRIORITY     IN PLS_INTEGER DEFAULT NULL)
      RETURN UTL_SMTP.CONNECTION;

   PROCEDURE SEND_EMAIL (
      P_FROM_EMAIL       IN     VARCHAR2,
      P_RECEIPTS_EMAIL   IN     VARCHAR2,
      P_CC_EMAIL         IN     VARCHAR2,
      P_BCC_EMAIL        IN     VARCHAR2,
      P_NTF_SUBJECT      IN     VARCHAR2,
      P_NTF_BODY         IN     CLOB,
      P_MIME_TYPE        IN     VARCHAR2 DEFAULT GC$MULTIPART_MIME_TYPE,
      OUT_STATUS            OUT VARCHAR2);
      
         PROCEDURE SEND_GMAIL (
      P_FROM_EMAIL       IN     VARCHAR2,
      P_RECEIPTS_EMAIL   IN     VARCHAR2,
      P_CC_EMAIL         IN     VARCHAR2,
      P_BCC_EMAIL        IN     VARCHAR2,
      P_NTF_SUBJECT      IN     VARCHAR2,
      P_NTF_BODY         IN     CLOB,
      P_MIME_TYPE        IN     VARCHAR2 DEFAULT GC$MULTIPART_MIME_TYPE,
      OUT_STATUS            OUT VARCHAR2
 );

   PROCEDURE PUSH_NOTIFICATION (IN_NOTIFICATION_ID NUMBER);

   PROCEDURE MAIL_JOB;

    PROCEDURE SEND_CALENDAR (P_FROM_EMAIL       IN     VARCHAR2,
                            P_RECEIPTS_EMAIL   IN     VARCHAR2,
                            P_NTF_SUBJECT      IN     VARCHAR2,
                            P_NTF_BODY         IN     CLOB,
                            OUT_STATUS            OUT VARCHAR2);

   PROCEDURE INSERT_NOTIFICATION_API (P_NOTIFICATION_TYPE          VARCHAR2,
                                      P_SEND_SYSTEM                VARCHAR2,
                                      P_SYSTEM_KEY                 VARCHAR2,
                                      P_FROM_EMAIL                 VARCHAR2,
                                      P_RECEIPTS_PERSONID          NUMBER,
                                      P_RECEIPTS_EMAIL             VARCHAR2,
                                      P_CC_EMAIL                   VARCHAR2,
                                      P_BCC_EMAIL                  VARCHAR2,
                                      P_ACCESS_KEY                 VARCHAR2,
                                      P_NTF_SUBJECT                VARCHAR2,
                                      P_NTF_BODY                   CLOB,
                                      OUT_NOTIFICATION_ID   IN OUT NUMBER,
                                      OUT_STATUS               OUT VARCHAR2);
                                      
                                        PROCEDURE SEND_MAIL (pto_email varchar2,pemployeename varchar2 ,pbodymessage varchar2  );
          PROCEDURE SEND_MAIL_SHIFT_WK_ALERT(pto_email varchar2,psubject varchar2 ,pbody varchar2 ) ;
          PROCEDURE  GET_UNMATCHED_SHIFT_WORKGROUPS ;
          PROCEDURE SEND_DAILY_UPDATES ;
            PROCEDURE SEND_SCHDLR_UPDATES(PSCHEDULER_STAFF_ID  NUMBER );
PROCEDURE INSERT_SMS_MESSAGE_STATUS (PPERSON_ID NUMBER ,PPOHONE varchar2,PMESSAGE_STATUS varchar2);

END AS_NOTIFICATIONS_PKG;
/

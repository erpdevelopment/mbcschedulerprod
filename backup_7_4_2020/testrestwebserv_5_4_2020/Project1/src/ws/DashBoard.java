package ws;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

@Path("dash/board")
public class DashBoard {
    public DashBoard() {
        super();
    }
    
    // new Excectutibve dashbord  31032019
    @POST
    @Produces("application/json")
    @Path("/getEmployeeCountBUDirectorate")
    public Response getEmployeeCountBUDirectorate(@Context SecurityContext sc,
                                                  @HeaderParam("directorateType") String directorateType,
                                                  @HeaderParam("wsid") String wsid,
                                                  @HeaderParam("signature") String signature
                                       )
    {
        
            
                if (!sc.isUserInRole("Authenticated"))  
                throw new SecurityException("User is unauthorized.");

                if (wsid==null || signature==null )  
                return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

                wsid=URLDecoder.decode(wsid);
                signature=URLDecoder.decode(signature);
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select *\n" + 
            "from xxmbc_employee_count_per_bu ";
        
            final String AL_ARABIYA="     where directorate ='Al Arabiya' "; 
            final String NON_AL_ARABIYA=" where directorate!='Al Arabiya' "; 
            if ("Arabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ AL_ARABIYA;
            if ("NotArabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ NON_AL_ARABIYA;
            try {
      
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getEmployeeCountBUDirectorate");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "Open getEmployeeCountBUDirectorate", "Open getEmployeeCountBUDirectorate" );
                statement = connection.prepareStatement(sqlStemt);

            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    
    @POST
    @Produces("application/json")
    @Path("/getEmployeeHeadcountGender")
    public Response getEmployeeHeadcountGender(@Context SecurityContext sc,
                                               @HeaderParam("directorateType") String directorateType,
                                               @HeaderParam("wsid") String wsid,
                                               @HeaderParam("signature") String signature
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select business_groups , male,female , employee_count\n" + 
            "from xxmbc_head_count_per_bu_gender  ";
        
            final String AL_ARABIYA="     where directorate_type ='Al Arabiya' "; 
            final String NON_AL_ARABIYA=" where directorate_type !='Al Arabiya' "; 
            if ("Arabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ AL_ARABIYA;
            if ("NotArabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ NON_AL_ARABIYA;
            try {
      
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getEmployeeHeadcountGender");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, "Open getEmployeeHeadcountGender", "Open getEmployeeHeadcountGender" );
            statement = connection.prepareStatement(sqlStemt);

            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
   
    @POST
    @Produces("application/json")
    @Path("/getEmployeeCountPersonTypeDirectorate")
    public Response getEmployeeCountPersonTypeDirectorate(
                                  @Context SecurityContext sc
                                  ,@HeaderParam("directorateType") String directorateType
                                  ,@HeaderParam("businessGroup") String businessGroup,
                                  @HeaderParam("wsid") String wsid,
                                  @HeaderParam("signature") String signature
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");

            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select  *\n" + 
            "from xxmbc_employee_count_per_type\n" + 
            "where business_group_id=? ";
        
            final String AL_ARABIYA=" and directorate='Al Arabiya'"; 
            final String NON_AL_ARABIYA=" and directorate!='Al Arabiya'"; 
            if ("Arabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ AL_ARABIYA;
            if ("NotArabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ NON_AL_ARABIYA;

            try {
      
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getEmployeeCountPersonTypeDirectorate");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open getEmployeeCountPersonTypeDirectorate", "Open getEmployeeCountPersonTypeDirectorate" );


            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1, businessGroup);
            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
   
   
    @POST
    @Produces("application/json")
    @Path("/getOverTimeMonthsDirectorateType")
    public Response getOverTimeMonthsDirectorateType(@Context SecurityContext sc,
                                                     @HeaderParam("directorateType") String directorateType,
                                                     @HeaderParam("wsid") String wsid,
                                                     @HeaderParam("signature") String signature
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");

            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select year,month,month_no, amount\n" + 
            "from xxmbc_ot_per_month ";
        
            final String AL_ARABIYA="     where directorate_type  ='Al Arabiya' "; 
            final String NON_AL_ARABIYA=" where directorate_type !='Al Arabiya' "; 
            if ("Arabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ AL_ARABIYA;
            if ("NotArabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ NON_AL_ARABIYA;
            try {
      
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getOverTimeMonthsDirectorateType");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                

                Helper.logActivity(connection, "Open getOverTimeMonthsDirectorateType", "Open getOverTimeMonthsDirectorateType" );
                
            statement = connection.prepareStatement(sqlStemt);

            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    @POST
    @Produces("application/json")
    @Path("/getOverTimeMonthDepartments")
    public Response getOverTimeMonthDepartments(@Context SecurityContext sc,
                                                @HeaderParam("directorateType") String directorateType ,
                                                @HeaderParam("year") String year,
                                                @HeaderParam("monthNo") String monthNo,
                                                @HeaderParam("wsid") String wsid,
                                                @HeaderParam("signature") String signature
                                       )
    {
        
            
                
                if (!sc.isUserInRole("Authenticated"))  
                throw new SecurityException("User is unauthorized.");

                if (wsid==null || signature==null )  
                return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

                wsid=URLDecoder.decode(wsid);
                signature=URLDecoder.decode(signature);

            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select year,month,month_no,dept_name, amount\n" + 
            "from xxmbc_ot_per_month_dept_name\n" + 
            "where year=?\n" + 
            "and month_no=? ";
        
            final String AL_ARABIYA="     and directorate_type  ='Al Arabiya' "; 
            final String NON_AL_ARABIYA=" and directorate_type !='Al Arabiya' "; 
            if ("Arabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ AL_ARABIYA;
            if ("NotArabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ NON_AL_ARABIYA;
            try {
      
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getOverTimeMonthDepartments");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open getOverTimeMonthDepartments", "Open getOverTimeMonthDepartments" );


            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1,year);
                statement.setString(2,monthNo);
            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
   
   
    @POST
    @Produces("application/json")
    @Path("/getExtraTimeWorkedBGDierctorateType")
    public Response getExtraTimeWorkedBGDierctorateType(@Context SecurityContext sc,
                                                        @HeaderParam("directorateType") String directorateType,
                                                        @HeaderParam("wsid") String wsid,
                                                        @HeaderParam("signature") String signature
                                       )
    {
        
            
                if (!sc.isUserInRole("Authenticated"))  
                throw new SecurityException("User is unauthorized.");

                if (wsid==null || signature==null )  
                return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

                wsid=URLDecoder.decode(wsid);
                signature=URLDecoder.decode(signature);

            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select business_group_id,business_groups, extra_time_worked\n" + 
            "from xxmbc_extra_time_worked_per_bu ";
        
            final String AL_ARABIYA="     where directorate_type ='Al Arabiya' "; 
            final String NON_AL_ARABIYA=" where directorate_type !='Al Arabiya' "; 
            if ("Arabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ AL_ARABIYA;
            if ("NotArabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ NON_AL_ARABIYA;
            try {
      
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getExtraTimeWorkedBGDierctorateType");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open getExtraTimeWorkedBGDierctorateType", "Open getExtraTimeWorkedBGDierctorateType" );

            statement = connection.prepareStatement(sqlStemt);

            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    @POST
    @Produces("application/json")
    @Path("/getExtraTimeWorkedDepartmentDierctorateType")
    public Response getExtraTimeWorkedDepartmentDierctorateType(@Context SecurityContext sc,
                                                @HeaderParam("directorateType") String directorateType 
                                                ,@HeaderParam("businessGroup") String businessGroup,
                                                @HeaderParam("wsid") String wsid,
                                                @HeaderParam("signature") String signature
                                               
                                       )
    {
        
            
                
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");

            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();

            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select business_group_id,business_groups, dept_name, extra_time_worked\n" + 
            "from xxmbc_ex_time_worked_bu_dept\n" + 
            "where business_group_id=? ";
        
            final String AL_ARABIYA="     and directorate_type  ='Al Arabiya' "; 
            final String NON_AL_ARABIYA=" and directorate_type !='Al Arabiya' "; 
            if ("Arabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ AL_ARABIYA;
            if ("NotArabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ NON_AL_ARABIYA;
            try {
      
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature , wsid+"getExtraTimeWorkedDepartmentDierctorateType");
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 

                Helper.logActivity(connection, "Open getExtraTimeWorkedDepartmentDierctorateType", "Open getExtraTimeWorkedDepartmentDierctorateType" );


            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1,businessGroup);
               
            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
   
   //////////////// 
//    @POST
//    @Produces("application/json")
//    @Path("/getBUheadCount")
    public Response getBUheadCount(@Context SecurityContext sc
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
//            String sqlStemt="select count(person_id) cnt , BUSINESS_GROUP_ID ,replace(BUSINESS_GROUP_Name,'MBC - ',NULL) BUSINESS_GROUP_Name\n" + 
//            "from xxmbc_mob_dash_current_emp\n" + 
//            "group by  BUSINESS_GROUP_ID ,BUSINESS_GROUP_Name";

            String sqlStemt="select * from xxmbc_mobile_bu_head_count";
        
            try {
      
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);

            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
//    @POST
//    @Produces("application/json")
//    @Path("/getDiectorateHeadCount")
    public Response getDiectorateHeadCount(@Context SecurityContext sc ,  @HeaderParam("businessGroup") String businessGroup
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
            String sqlStemt="select count(person_id) cnt , nvl (directorate   ,'Undefined ') directorate\n" + 
            "from xxmbc_mobile_current_emp\n" + 
            "where business_group_id=?\n" + 
            "group by  directorate \n" +
            " order by  cnt desc";

            
        
            try {
      
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);

                statement.setString(1, businessGroup);
            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
//    @POST
//    @Produces("application/json")
//    @Path("/getOverTimeMonths")
    public Response getOverTimeMonths(@Context SecurityContext sc
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select year,month,month_no , amt, total\n" + 
            "from xx_overtime_ytd_month";
        
            try {
      
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);

            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
//    @POST
//    @Produces("application/json")
//    @Path("/getOverTimeMonthDirctorates")
    public Response getOverTimeMonthDirctorates(@Context SecurityContext sc,@HeaderParam("attribute1") String monthNo
                                       )
    {
        // name is now is attrribute one until we register new headed on MCS
            
                
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select year,month,month_no , amount,directorate\n" + 
            "from xx_overtime_ytd_directorate where month_no=?";
        
            try {
      
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
                statement.setString(1,monthNo);
            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
    
    
   
   
   
    
   
   
    
  
    
    /////////////////////////// SCM 
   // @POST
   // @Produces("application/json")
   // @Path("/getApprovedPo")
    public Response getApprovedPo(@Context SecurityContext sc,@HeaderParam("directorateType") String directorateType 
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt="select operatin_unit_id,operatin_unit, amt amount , bu_shordt_name\n" + 
            "from xxmbc_apprvd_po_dirctorate_con ";
        
        sqlStemt="select organization_id operatin_unit_id,ou_name operatin_unit, trunc( sum(nvl(unit_price,0)*nvl(quantity,0)*nvl(conversion_rate,1))) amount , to_char ( trunc( sum(nvl(unit_price,0)*nvl(quantity,0)*nvl(conversion_rate,1))),'999,999,999.00') amt ,  bu_short_name , trunc( sum(nvl(amount_billed,0)*nvl(conversion_rate,1))) amount_billed , xxmobile_dashboard.get_organization_budget_amt(organization_id,2018) budget_Amount \n" + 
        "            from xx_po_dashboard\n" + 
        "            where instr(upper(dirctorate),'AL ARABIYA') =?\n" + 
        "            group by ou_name,organization_id,bu_short_name ";
        
            /*final String AL_ARABIYA="     where directorate_type ='Al Arabiya' "; 
            final String NON_AL_ARABIYA=" where directorate_type !='Al Arabiya' "; 
            if ("Arabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ AL_ARABIYA;
            if ("NotArabiya".equalsIgnoreCase(directorateType))
                sqlStemt=sqlStemt+ NON_AL_ARABIYA;*/
            try {
      
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);
                if ("Arabiya".equalsIgnoreCase(directorateType))
                    statement.setString(1, "1");
                else
                    statement.setString(1, "0");
            
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
//    @POST
//    @Produces("application/json")
//    @Path("/getOperatinUnitBudgetPerAccount")
    public Response getOperatinUnitBudgetPerAccount(@Context SecurityContext sc,@HeaderParam("organizationId") String organizationId 
                                       )
    {
        
            
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;

            String sqlStemt=" select  name  ,  case    when rank>=5 then 'Others' else account end account , sum (amount) amount ,sum(amount_billed) amount_billed, sum(budget_amount) budget_amount \n" + 
            "from xx_po_dashboard_accounts_v\n" + 
            "where organization_id=?\n" + 
            "group by name ,case when rank>=5 then 'Others' else account end\n" + 
            "order by case when account='Others' then '1' else '0' end ";
        
           
        
            try {
      
                connection = Helper.getOracleConnection();
            statement = connection.prepareStatement(sqlStemt);

                statement.setString(1, organizationId);
             rs = statement.executeQuery();
          
               // Map<String, Object> m = new HashMap<String, Object>();
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                  
                }

            }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }
    
}

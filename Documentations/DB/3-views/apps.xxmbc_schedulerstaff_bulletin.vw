

/* Formatted on 10/Dec/2018 2:43:36 PM (QP5 v5.215.12089.38647) */
CREATE OR REPLACE FORCE VIEW APPS.XXMBC_SCHEDULERSTAFF_BULLETIN
(
   SECTION_ID,
   SCHEDULER_STAFF_ID,
   SCHEDULER_ID,
   FK_SCHEDULER_SECTION_ID,
   SCHEDULAR_START_DATE,
   FULL_NAME,
   PERSON_ID,
   EMPSHIFT,
   NOTES
)
AS
   SELECT section_id,
          scheduler_staff_id,
          AsSchedulerStaffEO.SCHEDULER_ID,
          FK_SCHEDULER_SECTION_ID,
          SCHEDULAR_START_DATE,
          papf.full_name,
          papf.person_id,
             DECODE (NVL (us.INTIAL_NAME, 'NNN'),
                     'NNN', NVL (papf.full_name, 'XXX'),
                     NVL (us.INTIAL_NAME, 'XXX'))
          || DECODE (NVL (us_replacement.INTIAL_NAME, 'N'),
                     'N', REPLACEMENT.full_name,
                     '(' || us_replacement.INTIAL_NAME || ')')
          || NVL (ASS.SHIFT_NAME, ' ')
          || '-'
          || as_schdlr_pkg.SCHEDULER_STAFF_BULLETIN (scheduler_staff_id)
             empshift,
          DECODE (Notes, 'N', '', Notes) Notes
     FROM AS_SCHEDULER_STAFF AsSchedulerStaffEO,
          AS_SCHEDULER_SECTIONS AsSchedulerSectionsEO,
          PER_ALL_PEOPLE_F PAPF,
          PER_ALL_PEOPLE_F REPLACEMENT,
          AS_SCHEDULERs schdlr,
          AS_SHIFTS ASS,
          AS_WORK_GROUPS_HDR AWGH,
          as_user_roles us,
          as_user_roles us_replacement
    WHERE     AsSchedulerStaffEO.PERSON_ID = PAPF.PERSON_ID(+)
          AND AsSchedulerStaffEO.FK_REPLACED_BY_ID = REPLACEMENT.PERSON_ID(+)
          AND AsSchedulerStaffEO.FK_REPLACED_BY_ID =
                 us_replacement.PERSON_Id(+)
          AND AsSchedulerStaffEO.PERSON_ID = us.PERSON_Id(+)
          AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1)
          AND SYSDATE BETWEEN NVL (REPLACEMENT.EFFECTIVE_START_DATE,
                                   SYSDATE - 1)
                          AND NVL (REPLACEMENT.EFFECTIVE_END_DATE,
                                   SYSDATE + 1)
          AND AsSchedulerStaffEO.SCHEDULER_ID = schdlr.SCHEDULER_ID
          AND AsSchedulerStaffEO.SHIFT_ID = ASS.SHIFT_ID(+)
          AND AsSchedulerStaffEO.WORK_GROUP_ID = AWGH.WORK_GROUP_ID(+)
          AND AsSchedulerStaffEO.SCHEDULER_ID =
                 AsSchedulerSectionsEO.FKSCHEDULER_ID
          AND AsSchedulerStaffEO.FK_SCHEDULER_SECTION_ID =
                 AsSchedulerSectionsEO.SECTION_ID
-- and replace_type is not null
-- ORDER BY SCHEDULER_STAFF_ID DESC
;

package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.view.util.ADFUtils;

import com.tangosol.dev.compiler.java.ShiftExpression;

import java.text.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichChooseDate;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichGridCell;
import oracle.adf.view.rich.component.rich.layout.RichGridRow;
import oracle.adf.view.rich.component.rich.layout.RichPanelBox;
import oracle.adf.view.rich.component.rich.layout.RichPanelGridLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;

import oracle.jbo.uicli.binding.JUCtrlListBinding;

import org.apache.myfaces.trinidad.event.AttributeChangeEvent;
import org.apache.myfaces.trinidad.model.RowKeySet;

public class PublicSpacePanel {
    private RichPanelBox pb1;
    private RichInputDate id1;
  public Date WeekStartDate ;
   public String FirstDay ;
    public String SecondDay ;
    public String ThirdDay ;
    public String FourthDay ;
    public String FifthDay ;
    public String SixDay ;
    public String SevenDay ;
    public String EmpFullName ;
    private RichSelectOneChoice soc2;
    private UISelectItems si2;
    private RichSelectOneChoice soc3;
    private UISelectItems si3;
    private RichPanelGridLayout pgl5;
    private RichGridRow gr8;
    private RichGridCell gc11;
    private RichGridCell gc12;
    private RichGridCell gc13;
    private RichPanelHeader ph1;
    private RichPanelGroupLayout pgl6;

    public void setEmpFullName(String EmpFullName) {
        this.EmpFullName = EmpFullName;
    }

    public String getEmpFullName() {
        
        return EmpFullName;
    }
    private RichButton b1;
    private RichPanelBox pb2;
    private RichTable t1;
    private RichTable t2;
    private RichInputText it1;
    private RichSelectOneChoice soc1;
    private UISelectItems si1;
    
    private RichPanelGridLayout pgl4;
    private RichGridRow gr6;
    private RichGridCell gc7;
    private RichGridCell gc8;
    private RichGridRow gr7;
    private RichGridCell gc9;
    private RichGridCell gc10;
    private RichInputText it2;


    public void setFirstDay(String FirstDay) {
        this.FirstDay = FirstDay;
    }
// Seven Columns of Week//
    public String getFirstDay() {
      //  return FirstDay;
    return   this.DayOfWeek(this.getWeekStartDate());
    }
    public void setSecondDay(String SecondDay) {
        this.SecondDay = SecondDay;
    }

    public String getSecondDay() {
       // return SecondDay;
       // Date dt =this.getWeekStartDate();
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 1));
    }

    public void setThirdDay(String ThirdDay) {
        this.ThirdDay = ThirdDay;
    }

    public String getThirdDay() {
      //  return ThirdDay;
      return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 2));
    }

    public void setFourthDay(String FourthDay) {
        this.FourthDay = FourthDay;
    }

    public String getFourthDay() {
        //return FourthDay;
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 3));
    }

    public void setFifthDay(String FifthDay) {
        this.FifthDay = FifthDay;
    }

    public String getFifthDay() {
      //  return FifthDay;
      return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 4));
    }

    public void setSixDay(String SixDay) {
        this.SixDay = SixDay;
    }

    public String getSixDay() {
       // return SixDay;
       return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 5));
    }

    public void setSevenDay(String SevenDay) {
        this.SevenDay = SevenDay;
    }

    public String getSevenDay() {
      //  return SevenDay;
      return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 6));
    }
    // InputDate field
    public void setWeekStartDate(Date WeekStartDate) {
       
       this.WeekStartDate = WeekStartDate;
    }

    public Date getWeekStartDate() {
       //
        
       if(WeekStartDate!= null && (!WeekStartDate.equals("")))
        return WeekStartDate;
           
        else 
       {

           return  new Date();
           }

    }

    public void setPb1(RichPanelBox pb1) {
        this.pb1 = pb1;
    }

    public RichPanelBox getPb1() {
        return pb1;
    }


    public void setId1(RichInputDate id1) {
        this.id1 = id1;
    }

    public RichInputDate getId1() {
        return id1;
    }

  

    public void setB1(RichButton b1) {
        this.b1 = b1;
    }

    public RichButton getB1() {
        return b1;
    }
    //Date :22-6-2017
    //Author:HebatAllah Salah 
    
   //---------------Custom Methods -------------------//
    //----Method for Search of Schedule  with Different Paramters ,Start Day of The Week /Workgroup/Shift/Employee----
    public String SearchPublicSpace_Btn() {
        // Add event code here...
     DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
      String StartDate=df.format(this.getWeekStartDate());
      String EndDate=df.format(this.addDays(this.getWeekStartDate(), 6));
       OperationBinding oper = ADFUtils.findOperation("PublicSpaceSearch");
       oper.getParamsMap().put("pstart_Date",StartDate);
          //  Number cShiftId = (Number) ADFUtils.getBoundAttributeValue("ShiftId");
        Number cWorkgroupId = (Number) ADFUtils.getBoundAttributeValue("WorkGroupId");
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        String EmpFullName = (String) sessionscope.get("AAEmpName");
        oper.getParamsMap().put("ShiftId",getSelectedShifts());
        oper.getParamsMap().put("WorkGroupId",cWorkgroupId);
        oper.getParamsMap().put("pfullname",EmpFullName);
                                //this.getEmpFullName());
       oper.execute();
///Scroll to First Page//
    //  this.getT2().setRowIndex(0);
     //   RowKeySet ps = this.getT2().getSelectedRowKeys();
      //  ps.clear();
        //ps.add(this.getT2().getRowKey());
       
   //  AdfFacesContext.getCurrentInstance().addPartialTarget(t2);
        //(pb2);
            

               return null;
    }
    public String getSelectedShifts()
    {
                       BindingContainer bc=getBindings();
                    JUCtrlListBinding ListBinding = 
                             (JUCtrlListBinding) bc.get("ShiftsLov1");
                    Object []str=ListBinding.getSelectedValues();
                    StringBuilder Selected=new StringBuilder();
                    for (int i=0;i<str.length;i++)
                   {
                        
                        if(i==0)
                        {
                            
                            Selected.append(str[i]);
                            }
                        else 
                        {
                                Selected.append("|");
                                Selected.append(str[i]);
                            }
                            
                
                       // System.out.print(str[i]);
//                       FacesMessage Message = new FacesMessage(Selected.toString());   
//                                   Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                                   FacesContext fc = FacesContext.getCurrentInstance();   
//                                   fc.addMessage(null, Message);                              
                      } 
        
        
        return Selected.toString();
        }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
// Testing shiftid /workgroupid
    public void onChangeShiftWorkGroup(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        //DummyShiftId
        //DummyWorkgroupId
       // valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
       // FacesMessage fm=new FacesMessage(cShiftId.toString()+cWorkgroupId.toString());
       //  fm.setSeverity(FacesMessage.SEVERITY_INFO);
       // FacesContext fc = FacesContext.getCurrentInstance(); fc.addMessage(null, fm);
        
    }
   //Method to retrive day of week and month like thursday-march
     public  String DayOfWeek(Date dt)
     {
                Calendar cal = Calendar.getInstance();
                cal.setTime(dt);
               // int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_WEEK);
                int dayofmonth=cal.get(Calendar.DAY_OF_MONTH);
                String DayofWeek="";
                String MonthofYear="";
                if(day==1)
                    DayofWeek="Sun";
              else  if(day==2)
                    DayofWeek= "Mon";
           else  if(day==3)
                 DayofWeek= "Tue";
           else  if(day==4)
                 DayofWeek= "Wed";
          else   if(day==5)
                 DayofWeek= "Thurs";
          else   if(day==6)
                 DayofWeek="Fri";
        else     if(day==7)
                 DayofWeek= "Sat";
             //------------Month-------
             if(month==0)
                 MonthofYear="Jan";
           else  if(month==1)
                 MonthofYear="Feb";
             else  if(month==2)
                   MonthofYear="March";
             else  if(month==3)
                   MonthofYear="Apr";
             else  if(month==4)
                   MonthofYear="May";
             else  if(month==5)
                   MonthofYear="Jun";
             else  if(month==6)
                   MonthofYear="Jul";
               else  if(month==7)
                     MonthofYear="Aug";
               else  if(month==8)
                     MonthofYear="Sept";
               else  if(month==9)
                     MonthofYear="Oct";
               else  if(month==10)
                     MonthofYear="Nov";
             else  if(month==11)
                   MonthofYear="Dec";
             return DayofWeek+" "+MonthofYear+" "+String.valueOf(dayofmonth);
           
         }
     // Method to add Days  for the the date inserted in choose week start day to get the other six days
    public  Date addDays(Date date, int days)
       {
           Calendar cal = Calendar.getInstance();
           cal.setTime(date);
           cal.add(Calendar.DATE, days); //minus number would decrement the days
           return cal.getTime();
       }
    public void setPb2(RichPanelBox pb2) {
        this.pb2 = pb2;
    }

    public RichPanelBox getPb2() {
        return pb2;
    }

    public void setT1(RichTable t1) {
        this.t1 = t1;
    }

    public RichTable getT1() {
        return t1;
    }


    public void setT2(RichTable t2) {
        this.t2 = t2;
    }

    public RichTable getT2() {
        return t2;
    }

    public void setIt1(RichInputText it1) {
        this.it1 = it1;
    }

    public RichInputText getIt1() {
        return it1;
    }

    public void setSoc1(RichSelectOneChoice soc1) {
        this.soc1 = soc1;
    }

    public RichSelectOneChoice getSoc1() {
        return soc1;
    }

    public void setSi1(UISelectItems si1) {
        this.si1 = si1;
    }

    public UISelectItems getSi1() {
        return si1;
    }

    public void setSoc2(RichSelectOneChoice soc2) {
        this.soc2 = soc2;
    }

    public RichSelectOneChoice getSoc2() {
        return soc2;
    }

    public void setSi2(UISelectItems si2) {
        this.si2 = si2;
    }

    public UISelectItems getSi2() {
        return si2;
    }


    public void setPgl4(RichPanelGridLayout pgl4) {
        this.pgl4 = pgl4;
    }

    public RichPanelGridLayout getPgl4() {
        return pgl4;
    }

    public void setGr6(RichGridRow gr6) {
        this.gr6 = gr6;
    }

    public RichGridRow getGr6() {
        return gr6;
    }

    public void setGc7(RichGridCell gc7) {
        this.gc7 = gc7;
    }

    public RichGridCell getGc7() {
        return gc7;
    }

    public void setGc8(RichGridCell gc8) {
        this.gc8 = gc8;
    }

    public RichGridCell getGc8() {
        return gc8;
    }

    public void setGr7(RichGridRow gr7) {
        this.gr7 = gr7;
    }

    public RichGridRow getGr7() {
        return gr7;
    }

    public void setGc9(RichGridCell gc9) {
        this.gc9 = gc9;
    }

    public RichGridCell getGc9() {
        return gc9;
    }

    public void setGc10(RichGridCell gc10) {
        this.gc10 = gc10;
    }

    public RichGridCell getGc10() {
        return gc10;
    }

    public void setIt2(RichInputText it2) {
        this.it2 = it2;
    }

    public RichInputText getIt2() {
        return it2;
    }

  

    public void setSoc3(RichSelectOneChoice soc3) {
        this.soc3 = soc3;
    }

    public RichSelectOneChoice getSoc3() {
        return soc3;
    }

    public void setSi3(UISelectItems si3) {
        this.si3 = si3;
    }

    public UISelectItems getSi3() {
        return si3;
    }

    public void setPgl5(RichPanelGridLayout pgl5) {
        this.pgl5 = pgl5;
    }

    public RichPanelGridLayout getPgl5() {
        return pgl5;
    }

    public void setGr8(RichGridRow gr8) {
        this.gr8 = gr8;
    }

    public RichGridRow getGr8() {
        return gr8;
    }

    public void setGc11(RichGridCell gc11) {
        this.gc11 = gc11;
    }

    public RichGridCell getGc11() {
        return gc11;
    }

    public void setGc12(RichGridCell gc12) {
        this.gc12 = gc12;
    }

    public RichGridCell getGc12() {
        return gc12;
    }

    public void setGc13(RichGridCell gc13) {
        this.gc13 = gc13;
    }

    public RichGridCell getGc13() {
        return gc13;
    }

    public void setPh1(RichPanelHeader ph1) {
        this.ph1 = ph1;
    }

    public RichPanelHeader getPh1() {
        return ph1;
    }

    public void setPgl6(RichPanelGroupLayout pgl6) {
        this.pgl6 = pgl6;
    }

    public RichPanelGroupLayout getPgl6() {
        return pgl6;
    }
    
    public void changefirstday(AttributeChangeEvent attributeChangeEvent)
    {
           this.setFirstDay(this.DayOfWeek(this.getWeekStartDate())); 
       
        }
}

package com.mbc.oracle.scheduler.view.beans;

import com.mbc.oracle.scheduler.model.am.SchedulerAMImpl;
import com.mbc.oracle.scheduler.model.vo.AsAssignmentEmpsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceBulletinsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsPublicSpaceBulletinsVORowImpl;

import com.mbc.oracle.scheduler.model.vo.AsSchdeulerStaffTableVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdeulerStaffTableVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdlrStaffBulletinsHistVoImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdlrStaffBulletinsHistVoRowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdulerCurrentShiftworkrgroupvoImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchdulerCurrentShiftworkrgroupvoRowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerSectionsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffBulletinsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffBulletinsVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffEmailsmsVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffHistoryVoImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffHistoryVoRowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffShiftTableVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffShiftTableVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffVOImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulerStaffVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSchedulersVORowImpl;
import com.mbc.oracle.scheduler.model.vo.AsSendEmailSMSVoImpl;
import com.mbc.oracle.scheduler.model.vo.AspublicSpaceLeavesVOImpl;
import com.mbc.oracle.scheduler.model.vo.AspublicSpaceLeavesVORowImpl;
import com.mbc.oracle.scheduler.view.util.ADFUtils;
import com.mbc.oracle.scheduler.view.util.DateConversion;
import com.mbc.oracle.scheduler.view.util.JSFUtils;
import com.mbc.oracle.scheduler.view.util.SchedulerUtil;


import groovy.util.slurpersupport.ReplacementNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichCalendar;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.LaunchPopupEvent;
import oracle.adf.view.rich.render.ClientEvent;
import oracle.adf.view.rich.util.InstanceStyles;
import oracle.adfinternal.view.faces.model.binding.FacesCtrlLOVBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;
import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.TransactionEvent;
import oracle.jbo.server.ViewObjectImpl;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class SchedularBean {

    private String schdlrAssignIntersectionMsg;
    private String schdlrLeaveIntersectionMsg;
    private String filterEmpStr;
    private HashMap<Number, String> calFilterEmpMap;
    private String validateReport;
    private String validatedetails ;
    private RichCalendar schdlrCalendar;
    private RichPopup schlrPop;
    private RichPopup schlrPopNewsRoom;
    private RichPopup viewhistoryPopNewsRoom;
    private RichPopup schlrPopPresenter;
    private RichPopup valPop;
    private RichPopup RepeatA;
    private RichPopup DeleteA;
    private  String cutCopyA;
    private  String cutCopyB; 
    private String cutCopyC ;
    private ArrayList CutCList  =new ArrayList<Object>();
    private ArrayList CutAList  =new ArrayList<Object>();
    private ArrayList CutABCBulletins=new ArrayList<Number>();
   private Number CutSchedulerStaffIdPresenter=new Number(0);
   private Number  CutSchedulerStaffIdNewsRoom =new Number(0);
   private String columnWidth ;
   private Number  currentPersonId;
   //public Number  HistorySchedulerStaffId ;
    public void setRepeatB(RichPopup RepeatB) {
        this.RepeatB = RepeatB;
    }

    public RichPopup getRepeatB() {
        return RepeatB;
    }

    public void setDeleteB(RichPopup DeleteB) {
        this.DeleteB = DeleteB;
    }

    public RichPopup getDeleteB() {
        return DeleteB;
    }
    private RichPopup RepeatB;
    private RichPopup DeleteB;
   private RichPopup RepeatC;
    private RichPopup DeleteC;
    public java.util.Date DeleteDateStartA ;
    public java.util.Date DeleteDateEndA ;
    public java.util.Date DeleteDateMaxA ;
    public java.util.Date DeleteDateStartB ;
    public java.util.Date DeleteDateEndB ;
    public java.util.Date DeleteDateMaxB;
    public java.util.Date DeleteDateStartC ;
    public java.util.Date DeleteDateEndC ;
    public java.util.Date DeleteDateMaxC;
    public java.util.Date RepeatDateStartA ;
    public java.util.Date RepeatDateEndA ;
    public java.util.Date RepeatDateMaxA ;
    public java.util.Date RepeatDateStartB ;
    public java.util.Date RepeatDateEndB ;
    public java.util.Date RepeatDateMaxB ;
    public java.util.Date RepeatDateStartC ;
    public java.util.Date RepeatDateEndC ;
    public java.util.Date RepeatDateMaxC ;
   private RichPopup templatepopup;

    private Date calSelectedDate;
    private RichPopup apprvalPop;
    private RichPopup translPop;
    private RichPopup apprvStatusPop;
    private RichPopup validatetPop;
    private RichPopup transApprvPop;
    private RichPopup delSchedulerPop;
    private String templateDate ;

    public void setP1(RichPopup p1) {
        this.p1 = p1;
    }

    public RichPopup getP1() {
        return p1;
    }

    public void setP2(RichPopup p2) {
        this.p2 = p2;
    }

    public RichPopup getP2() {
        return p2;
    }
    private RichPopup p1;
    private RichPopup p2;
    private RichPopup p7;
    private HashMap calStyles = new HashMap<Set<String>, InstanceStyles>();
    private MyDateCustomizer holidays = new MyDateCustomizer(); 
    private String radioBtnValue="NA";
    public java.util.Date WeekStartDate ;
    public RichPanelCollection pc1;
    public String FirstDay ;
     public String SecondDay ;
     public String ThirdDay ;
     public String FourthDay ;
    public String FifthDay ;
     public String SixDay ;
     public String SevenDay; 
     public String dsi8 ;
    public String dsi9 ;
    public String dsi10 ;
    public String dsi11 ;
    public String dsi12 ;
    public String dsi13 ;
    public String dsi14 ;
    public String dsi15 ;
    public String dsi16 ;
    public String dsi17 ;
    public String dsi18 ;
    public String dsi19 ;
    public String dsi20 ;
    public String dsi21 ;
    public String dsi22 ;
    public String dsi23 ;
    public String dsi24 ;
    public String dsi25 ;
    public String dsi26 ;
    public String dsi27 ;
    public String dsi28 ;
    public String dsi29 ;
    public String dsi30 ;
    public String dsi31 ;
    public java.util.Date dateFirstDay ;
     public java.util.Date dateSecondDay ;
     public java.util.Date dateThirdDay ;
     public java.util.Date dateFourthDay ;
    public java.util.Date dateFifthDay ;
     public java.util.Date dateSixDay ;
     public java.util.Date dateSevenDay; 
     public java.util.Date datedsi8 ;
    public java.util.Date datedsi9 ;
    public java.util.Date datedsi10 ;
    public java.util.Date datedsi11 ;
    public java.util.Date datedsi12 ;
    public java.util.Date datedsi13 ;
    public java.util.Date datedsi14 ;
    public java.util.Date datedsi15 ;
    public java.util.Date datedsi16 ;
    public java.util.Date datedsi17 ;
    public java.util.Date datedsi18 ;
    public java.util.Date datedsi19 ;
    public java.util.Date datedsi20 ;
    public java.util.Date datedsi21 ;
    public java.util.Date datedsi22 ;
    public java.util.Date datedsi23 ;
    public java.util.Date datedsi24 ;
    public java.util.Date datedsi25 ;
    public java.util.Date datedsi26 ;
    public java.util.Date datedsi27 ;
    public java.util.Date datedsi28 ;
    public java.util.Date datedsi29 ;
    public java.util.Date datedsi30 ;
    public java.util.Date datedsi31 ;
    public Boolean showhidetable=false ;
    public java.util.Date SchedulerDateflag ;
    public java.util.Date ScheduelerStartDateflag;
    private RichTable t3;
    private RichTreeTable tt3;
    public String h1;
    public String h2;
    public String h3;
    public String h4;
    public String h5;
    public String h6;
    public String h7;
    public String h8;
    public String h9;
    public String h10;
    public String h11;
    public String h12;
    public String h13;
    public String h14;
    public String h15;
    public String h16;
    public String h17;
    public String h18;
    public String h19;
    public String h20;
    public String h21;
    public String h22;
    public String h23;
    public String h24;
    public String h25;
    public String h26;
    public String h27;
    public String h28;
    public String h29;
    public String h30;
    public String h31;
    public String SaveUpdate_text ;
    public String SaveUpdateNotify_text ;
    public String scolor1;
     public String scolor2; 
    public String scolor3; 
    public String scolor4;
    public String scolor5;
    public String scolor6;
    public String scolor7;
    public String scolor8;
    public String scolor9; 
    public String scolor10;
    public String scolor11;
    public String scolor12;
    public String scolor13;
    public String scolor14;
    public String scolor15;
    public String scolor16;
    public String scolor17;
    public String scolor18;
    public String scolor19;
     public String scolor20; 
    public String scolor21; 
    public String scolor22;
    public String scolor23;
    public String scolor24;
    public String scolor25;
    public String scolor26;
    public String scolor27; 
    public String scolor28;
    public String scolor29;
    public String scolor30;
    public String scolor31;
    public Number CopyId ;
    public String CopyValue="UNDEFINED";
 
    public Number CopyShiftId ;
    public String saveUpdate_SchedulerType ;
    public Number CopySchedulerStaffIdPresenter =new Number(0);
    public Number CopySchedulerStaffIdNewsRoom =new Number(0);
 

    public void setDeleteDateStartA(java.util.Date DeleteDateStartA) {
        this.DeleteDateStartA = DeleteDateStartA;
    }

    public java.util.Date getDeleteDateStartA() {
        return DeleteDateStartA;
    }

    public void setDeleteDateEndA(java.util.Date DeleteDateEndA) {
        this.DeleteDateEndA = DeleteDateEndA;
    }

    public java.util.Date getDeleteDateEndA() {
        return DeleteDateEndA;
    }

    public void setDeleteDateMaxA(java.util.Date DeleteDateMaxA) {
        this.DeleteDateMaxA = DeleteDateMaxA;
    }

    public java.util.Date getDeleteDateMaxA() {
        return DeleteDateMaxA;
    }
 
    public void setSaveUpdate_SchedulerType(String saveUpdate_SchedulerType) {
        this.saveUpdate_SchedulerType = saveUpdate_SchedulerType;
    }

    public String getSaveUpdate_SchedulerType() {
        return saveUpdate_SchedulerType;
    }
    
    public void setCopySchedulerStaffIdPresenter(Number CopySchedulerStaffIdPresenter) {
        this.CopySchedulerStaffIdPresenter = CopySchedulerStaffIdPresenter;
    }

    public Number getCopySchedulerStaffIdPresenter() {
        return CopySchedulerStaffIdPresenter;
    }

    public void setCopySchedulerStaffIdNewsRoom(Number CopySchedulerStaffIdNewsRoom) {
        this.CopySchedulerStaffIdNewsRoom = CopySchedulerStaffIdNewsRoom;
    }

    public Number getCopySchedulerStaffIdNewsRoom() {
        return CopySchedulerStaffIdNewsRoom;
    }
 
   
    public void setCopyId(Number CopyId) {
        this.CopyId = CopyId;
    }

    public Number getCopyId() {
        return CopyId;
    }

    public void setCopyValue(String CopyValue) {
        this.CopyValue = CopyValue;
    }

    public String getCopyValue() {
        return CopyValue;
    }
 
    public void setScolor1(String scolor1) {
        this.scolor1 = scolor1;
    }

    public String getScolor1() {
        return scolor1;
    }

    public void setScolor2(String scolor2) {
        this.scolor2 = scolor2;
    }

    public String getScolor2() {
        return scolor2;
    }

    public void setScolor3(String scolor3) {
        this.scolor3 = scolor3;
    }

    public String getScolor3() {
        return scolor3;
    }

    public void setScolor4(String scolor4) {
        this.scolor4 = scolor4;
    }

    public String getScolor4() {
        return scolor4;
    }

    public void setScolor5(String scolor5) {
        this.scolor5 = scolor5;
    }

    public String getScolor5() {
        return scolor5;
    }

    public void setScolor6(String scolor6) {
        this.scolor6 = scolor6;
    }

    public String getScolor6() {
        return scolor6;
    }

    public void setScolor7(String scolor7) {
        this.scolor7 = scolor7;
    }

    public String getScolor7() {
        return scolor7;
    }

    public void setScolor8(String scolor8) {
        this.scolor8 = scolor8;
    }

    public String getScolor8() {
        return scolor8;
    }

    public void setScolor9(String scolor9) {
        this.scolor9 = scolor9;
    }

    public String getScolor9() {
        return scolor9;
    }

    public void setScolor10(String scolor10) {
        this.scolor10 = scolor10;
    }

    public String getScolor10() {
        return scolor10;
    }

    public void setScolor11(String scolor11) {
        this.scolor11 = scolor11;
    }

    public String getScolor11() {
        return scolor11;
    }

    public void setScolor12(String scolor12) {
        this.scolor12 = scolor12;
    }

    public String getScolor12() {
        return scolor12;
    }

    public void setScolor13(String scolor13) {
        this.scolor13 = scolor13;
    }

    public String getScolor13() {
        return scolor13;
    }

    public void setScolor14(String scolor14) {
        this.scolor14 = scolor14;
    }

    public String getScolor14() {
        return scolor14;
    }

    public void setScolor15(String scolor15) {
        this.scolor15 = scolor15;
    }

    public String getScolor15() {
        return scolor15;
    }

    public void setScolor16(String scolor16) {
        this.scolor16 = scolor16;
    }

    public String getScolor16() {
        return scolor16;
    }

    public void setScolor17(String scolor17) {
        this.scolor17 = scolor17;
    }

    public String getScolor17() {
        return scolor17;
    }

    public void setScolor18(String scolor18) {
        this.scolor18 = scolor18;
    }

    public String getScolor18() {
        return scolor18;
    }

    public void setScolor19(String scolor19) {
        this.scolor19 = scolor19;
    }

    public String getScolor19() {
        return scolor19;
    }

    public void setScolor20(String scolor20) {
        this.scolor20 = scolor20;
    }

    public String getScolor20() {
        return scolor20;
    }
    public void setSchlrPopPresenter(RichPopup schlrPopPresenter) {
        this.schlrPopPresenter = schlrPopPresenter;
    }

    public RichPopup getSchlrPopPresenter() {
        return schlrPopPresenter;
    }
    public void setScolor21(String scolor21) {
        this.scolor21 = scolor21;
    }

    public String getScolor21() {
        return scolor21;
    }

    public void setScolor22(String scolor22) {
        this.scolor22 = scolor22;
    }

    public String getScolor22() {
        return scolor22;
    }

    public void setScolor23(String scolor23) {
        this.scolor23 = scolor23;
    }

    public String getScolor23() {
        return scolor23;
    }

    public void setScolor24(String scolor24) {
        this.scolor24 = scolor24;
    }

    public String getScolor24() {
        return scolor24;
    }

    public void setScolor25(String scolor25) {
        this.scolor25 = scolor25;
    }

    public String getScolor25() {
        return scolor25;
    }

    public void setScolor26(String scolor26) {
        this.scolor26 = scolor26;
    }

    public String getScolor26() {
        return scolor26;
    }

    public void setScolor27(String scolor27) {
        this.scolor27 = scolor27;
    }

    public String getScolor27() {
        return scolor27;
    }

    public void setScolor28(String scolor28) {
        this.scolor28 = scolor28;
    }

    public String getScolor28() {
        return scolor28;
    }

    public void setScolor29(String scolor29) {
        this.scolor29 = scolor29;
    }

    public String getScolor29() {
        return scolor29;
    }

    public void setScolor30(String scolor30) {
        this.scolor30 = scolor30;
    }

    public String getScolor30() {
        return scolor30;
    }

    public void setScolor31(String scolor31) {
        this.scolor31 = scolor31;
    }

    public String getScolor31() {
        return scolor31;
    }

    public void setSaveUpdate_text(String SaveUpdate_text) {
        this.SaveUpdate_text = SaveUpdate_text;
    }

    public String getSaveUpdate_text() {
        return SaveUpdate_text;
    }
    public void setH1(String h1) {
        this.h1 = h1;
    }

    public String getH1() {
     return    this.IsValid(this.getDateFirstDay());
      //  return h1;
    }
    public void setH2(String h2) {
        this.h2 = h2;
    }

    public String getH2() {
        return    this.IsValid(this.getDateSecondDay());
    }

    public void setH3(String h3) {
        this.h3 = h3;
    }

    public String getH3() {
        return    this.IsValid(this.getDateThirdDay());
    }

    public void setH4(String h4) {
        this.h4 = h4;
    }

    public String getH4() {
        return    this.IsValid(this.getDateFourthDay());
    }

    public void setH5(String h5) {
        this.h5 = h5;
    }

    public String getH5() {
        return    this.IsValid(this.getDateFifthDay());
    }

    public void setH6(String h6) {
        this.h6 = h6;
    }

    public String getH6() {
        return    this.IsValid(this.getDateSixDay());
    }

    public void setH7(String h7) {
        this.h7 = h7;
    }

    public String getH7() {
        return    this.IsValid(this.getDateSevenDay());
    }

    public void setH8(String h8) {
        this.h8 = h8;
    }

    public String getH8() {
        return    this.IsValid(this.getDatedsi8());
        
    }

    public void setH9(String h9) {
        this.h9 = h9;
    }

    public String getH9() {
        return    this.IsValid(this.getDatedsi9());
    }

    public void setH10(String h10) {
        this.h10 = h10;
    }

    public String getH10() {
        return    this.IsValid(this.getDatedsi10());
    }

    public void setH11(String h11) {
        this.h11 = h11;
    }

    public String getH11() {
        return    this.IsValid(this.getDatedsi11());
    }

    public void setH12(String h12) {
        this.h12 = h12;
    }

    public String getH12() {
        return    this.IsValid(this.getDatedsi12());
    }

    public void setH13(String h13) {
        this.h13 = h13;
    }

    public String getH13() {
        return    this.IsValid(this.getDatedsi13());
    }

    public void setH14(String h14) {
        this.h14 = h14;
    }

    public String getH14() {
        return    this.IsValid(this.getDatedsi14());
    }

    public void setH15(String h15) {
        this.h15 = h15;
    }

    public String getH15() {
        return    this.IsValid(this.getDatedsi15());
    }

    public void setH16(String h16) {
        this.h16 = h16;
    }

    public String getH16() {
        return    this.IsValid(this.getDatedsi16());
    }

    public void setH17(String h17) {
        this.h17 = h17;
    }

    public String getH17() {
        return    this.IsValid(this.getDatedsi17());
    }

    public void setH18(String h18) {
        this.h18 = h18;
    }

    public String getH18() {
        return    this.IsValid(this.getDatedsi18());
    }

    public void setH19(String h19) {
        this.h19 = h19;
    }

    public String getH19() {
        return    this.IsValid(this.getDatedsi19());
    }

    public void setH20(String h20) {
        this.h20 = h20;
    }

    public String getH20() {
        return    this.IsValid(this.getDatedsi20());
    }

    public void setH21(String h21) {
        this.h21 = h21;
    }

    public String getH21() {
        return    this.IsValid(this.getDatedsi21());
    }

    public void setH22(String h22) {
        this.h22 = h22;
    }

    public String getH22() {
        return    this.IsValid(this.getDatedsi22());
    }

    public void setH23(String h23) {
        this.h23 = h23;
    }

    public String getH23() {
        return    this.IsValid(this.getDatedsi23());
    }

    public void setH24(String h24) {
        this.h24 = h24;
    }

    public String getH24() {
        return    this.IsValid(this.getDatedsi24());
    }

    public void setH25(String h25) {
        this.h25 = h25;
    }

    public String getH25() {
        return    this.IsValid(this.getDatedsi25());
    }

    public void setH26(String h26) {
        this.h26 = h26;
    }

    public String getH26() {
        return    this.IsValid(this.getDatedsi26());
    }

    public void setH27(String h27) {
        this.h27 = h27;
    }

    public String getH27() {
        return    this.IsValid(this.getDatedsi27());
    }

    public void setH28(String h28) {
        this.h28 = h28;
    }

    public String getH28() {
        return    this.IsValid(this.getDatedsi28());
    }

    public void setH29(String h29) {
        this.h29 = h29;
    }

    public String getH29() {
        return    this.IsValid(this.getDatedsi29());
    }

    public void setH30(String h30) {
        this.h30 = h30;
    }

    public String getH30() {
        return    this.IsValid(this.getDatedsi30());
    }

    public void setH31(String h31) {
        this.h31 = h31;
    }

    public String getH31() {
        return    this.IsValid(this.getDatedsi31());
    }
    public void setSchedulerDateflag(java.util.Date SchedulerDateflag) {
        this.SchedulerDateflag = SchedulerDateflag;
    }

    public java.util.Date getSchedulerDateflag() {
        return SchedulerDateflag;
    }

    public void setScheduelerStartDateflag(java.util.Date ScheduelerStartDateflag) {
        this.ScheduelerStartDateflag = ScheduelerStartDateflag;
    }

    public java.util.Date getScheduelerStartDateflag() {
        return ScheduelerStartDateflag;
    }
    
    public void setDateFirstDay(java.util.Date dateFirstDay) {
        this.dateFirstDay = dateFirstDay;
    }

    public java.util.Date getDateFirstDay() {
      return  this.addDays(this.getWeekStartDate(),0);
       //return dateFirstDay;
    }

    public void setDateSecondDay(java.util.Date dateSecondDay) {
        this.dateSecondDay = dateSecondDay;
    }

    public java.util.Date getDateSecondDay() {
           return  this.addDays(this.getWeekStartDate(),1);
            //dateSecondDay;
    }

    public void setDateThirdDay(java.util.Date dateThirdDay) {
        this.dateThirdDay = dateThirdDay;
    }

    public java.util.Date getDateThirdDay() {
        return  this.addDays(this.getWeekStartDate(),2);
        //return dateThirdDay;
    }

    public void setDateFourthDay(java.util.Date dateFourthDay) {
        this.dateFourthDay = dateFourthDay;
    }

    public java.util.Date getDateFourthDay() {
      //  return dateFourthDay;
      return  this.addDays(this.getWeekStartDate(),3);
    }

    public void setDateFifthDay(java.util.Date dateFifthDay) {
        this.dateFifthDay = dateFifthDay;
    }

    public java.util.Date getDateFifthDay() {
//        return dateFifthDay;
           return  this.addDays(this.getWeekStartDate(),4);
    }

    public void setDateSixDay(java.util.Date dateSixDay) {
        this.dateSixDay = dateSixDay;
    }

    public java.util.Date getDateSixDay() {
        return  this.addDays(this.getWeekStartDate(),5);
    }

    public void setDateSevenDay(java.util.Date dateSevenDay) {
        this.dateSevenDay = dateSevenDay;
    }

    public java.util.Date getDateSevenDay() {
        return  this.addDays(this.getWeekStartDate(),6);
    }

    public void setDatedsi8(java.util.Date datedsi8) {
        this.datedsi8 = datedsi8;
    }

    public java.util.Date getDatedsi8() {
        return  this.addDays(this.getWeekStartDate(),7);
    }

    public void setDatedsi9(java.util.Date datedsi9) {
        this.datedsi9 = datedsi9;
    }

    public java.util.Date getDatedsi9() {
        return  this.addDays(this.getWeekStartDate(),8);
    }

    public void setDatedsi10(java.util.Date datedsi10) {
        this.datedsi10 = datedsi10;
    }

    public java.util.Date getDatedsi10() {
        return  this.addDays(this.getWeekStartDate(),9);
    }

    public void setDatedsi11(java.util.Date datedsi11) {
        this.datedsi11 = datedsi11;
    }

    public java.util.Date getDatedsi11() {
        return  this.addDays(this.getWeekStartDate(),10);
    }

    public void setDatedsi12(java.util.Date datedsi12) {
        this.datedsi12 = datedsi12;
    }

    public java.util.Date getDatedsi12() {
        return  this.addDays(this.getWeekStartDate(),11);
    }

    public void setDatedsi13(java.util.Date datedsi13) {
        this.datedsi13 = datedsi13;
    }

    public java.util.Date getDatedsi13() {
        return  this.addDays(this.getWeekStartDate(),12);
    }

    public void setDatedsi14(java.util.Date datedsi14) {
        this.datedsi14 = datedsi14;
    }

    public java.util.Date getDatedsi14() {
        return  this.addDays(this.getWeekStartDate(),13);
    }

    public void setDatedsi15(java.util.Date datedsi15) {
        this.datedsi15 = datedsi15;
    }

    public java.util.Date getDatedsi15() {
        return  this.addDays(this.getWeekStartDate(),14);
    }

    public void setDatedsi16(java.util.Date datedsi16) {
        this.datedsi16 = datedsi16;
    }

    public java.util.Date getDatedsi16() {
        return  this.addDays(this.getWeekStartDate(),15);
    }

    public void setDatedsi17(java.util.Date datedsi17) {
        this.datedsi17 = datedsi17;
    }

    public java.util.Date getDatedsi17() {
        return  this.addDays(this.getWeekStartDate(),16);
    }

    public void setDatedsi18(java.util.Date datedsi18) {
        this.datedsi18 = datedsi18;
    }

    public java.util.Date getDatedsi18() {
        return  this.addDays(this.getWeekStartDate(),17);
    }

    public void setDatedsi19(java.util.Date datedsi19) {
        this.datedsi19 = datedsi19;
    }

    public java.util.Date getDatedsi19() {
        return  this.addDays(this.getWeekStartDate(),18);
    }

    public void setDatedsi20(java.util.Date datedsi20) {
        this.datedsi20 = datedsi20;
    }

    public java.util.Date getDatedsi20() {
        return  this.addDays(this.getWeekStartDate(),19);
    }

    public void setDatedsi21(java.util.Date datedsi21) {
        this.datedsi21 = datedsi21;
    }

    public java.util.Date getDatedsi21() {
        return  this.addDays(this.getWeekStartDate(),20);
    }

    public void setDatedsi22(java.util.Date datedsi22) {
        this.datedsi22 = datedsi22;
    }

    public java.util.Date getDatedsi22() {
        return  this.addDays(this.getWeekStartDate(),21);
    }

    public void setDatedsi23(java.util.Date datedsi23) {
        this.datedsi23 = datedsi23;
    }

    public java.util.Date getDatedsi23() {
        return  this.addDays(this.getWeekStartDate(),22);
    }

    public void setDatedsi24(java.util.Date datedsi24) {
        this.datedsi24 = datedsi24;
    }

    public java.util.Date getDatedsi24() {
        return  this.addDays(this.getWeekStartDate(),23);
    }

    public void setDatedsi25(java.util.Date datedsi25) {
        this.datedsi25 = datedsi25;
    }

    public java.util.Date getDatedsi25() {
        return  this.addDays(this.getWeekStartDate(),24);
    }

    public void setDatedsi26(java.util.Date datedsi26) {
        this.datedsi26 = datedsi26;
    }

    public java.util.Date getDatedsi26() {
        return  this.addDays(this.getWeekStartDate(),25);
    }

    public void setDatedsi27(java.util.Date datedsi27) {
        this.datedsi27 = datedsi27;
    }

    public java.util.Date getDatedsi27() {
        return  this.addDays(this.getWeekStartDate(),26);
    }

    public void setDatedsi28(java.util.Date datedsi28) {
        this.datedsi28 = datedsi28;
    }

    public java.util.Date getDatedsi28() {
        return  this.addDays(this.getWeekStartDate(),27);
    }

    public void setDatedsi29(java.util.Date datedsi29) {
        this.datedsi29 = datedsi29;
    }

    public java.util.Date getDatedsi29() {
        return  this.addDays(this.getWeekStartDate(),28);
    }

    public void setDatedsi30(java.util.Date datedsi30) {
        this.datedsi30 = datedsi30;
    }

    public java.util.Date getDatedsi30() {
        return  this.addDays(this.getWeekStartDate(),29);
    }

    public void setDatedsi31(java.util.Date datedsi31) {
        this.datedsi31 = datedsi31;
    }

    public java.util.Date getDatedsi31() {
        return  this.addDays(this.getWeekStartDate(),30);
    }
  
    public void setShowhidetable(Boolean showhidetable) {
        this.showhidetable = showhidetable;
    }

    public Boolean getShowhidetable() {
        return showhidetable;
    }


    public void setValidateReport(String validateReport) {
        this.validateReport = validateReport;
    }

    public String getValidateReport() {
        return validateReport;
    }

    public void setCalFilterEmpMap(HashMap<Number, String> calFilterEmpMap) {
        this.calFilterEmpMap = calFilterEmpMap;
       // schdlrCalendar.se
    }

    public HashMap<Number, String> getCalFilterEmpMap() {
        if (calFilterEmpMap == null)
            calFilterEmpMap = new HashMap();
        return calFilterEmpMap;
    }

    public void setSchdlrAssignIntersectionMsg(String schdlrAssignIntersectionMsg) {
        this.schdlrAssignIntersectionMsg = schdlrAssignIntersectionMsg;
    }

    public String getSchdlrAssignIntersectionMsg() {
        return schdlrAssignIntersectionMsg;
    }

    public void setFilterEmpStr(String filterEmpStr) {
        this.filterEmpStr = filterEmpStr;
    }

    public String getFilterEmpStr() {
        return filterEmpStr;
    }

    public void setCalSelectedDate(Date calSelectedDate) {
        this.calSelectedDate = calSelectedDate;
    }

    public Date getCalSelectedDate() {
        return calSelectedDate;
    }


    public SchedularBean() {
    }


    /*public void onChangeSchdlrShiftChoice(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        AdfFacesContext.getCurrentInstance().addPartialTarget(schdlrShiftTable);
        FacesContext.getCurrentInstance().renderResponse();
    }*/

    public void initCalendarParamsWorklist()
    { 
        
        this.setRadioBtnValue("Week");
        
        }
    public void initCalendarParams() {
        // Add event code here...
        Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
        Date StartDate =
            (Date) asSchedulersRow.getAttribute("StartDate"); //(Date) ADFUtils.getBoundAttributeValue("StartDate");
           this.setSchedulerDateflag(DateConversion.getUtilDateFromJboDate((Date)asSchedulersRow.getAttribute("EndDate")));

        Date endDate =
            (Date) asSchedulersRow.getAttribute("EndDate"); //(Date) ADFUtils.getBoundAttributeValue("EndDate");
           this.setScheduelerStartDateflag(DateConversion.getUtilDateFromJboDate((Date)asSchedulersRow.getAttribute("StartDate")));
            this.setRadioBtnValue("Week");


    }
//    public void initCalendarParamsNews() {
//        // Add event code here...
//        Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
//        Date StartDate =
//            (Date) asSchedulersRow.getAttribute("StartDate"); //(Date) ADFUtils.getBoundAttributeValue("StartDate");
//           this.setSchedulerDateflag(DateConversion.getUtilDateFromJboDate((Date)asSchedulersRow.getAttribute("EndDate")));
//
//        Date endDate =
//            (Date) asSchedulersRow.getAttribute("EndDate"); //(Date) ADFUtils.getBoundAttributeValue("EndDate");
//           this.setScheduelerStartDateflag(DateConversion.getUtilDateFromJboDate((Date)asSchedulersRow.getAttribute("StartDate")));
//            this.setRadioBtnValue("Week");
//
//
//    }


   
    public void setT3(RichTable t3) {
        this.t3 = t3;
    }

    public RichTable getT3() {
        return t3;
    }
    public void setSchdlrCalendar(RichCalendar schdlrCalendar) {
        this.schdlrCalendar = schdlrCalendar;
    }

    public RichCalendar getSchdlrCalendar() {
        return schdlrCalendar;
    }

    public void setSchlrPop(RichPopup schlrPop) {
        this.schlrPop = schlrPop;
    }

    public RichPopup getSchlrPop() {
        return schlrPop;
    }


public String CreateSchedule()

{
    
    this.setSaveUpdate_SchedulerType("NEW");
    return "createSchedular";
    
    }

    public String EditSchedule()

    {
        
//            String  SchedulerType= "";
//                 
//                       Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
//             
//                      
//                         SchedulerType=asSchedulersRow.getAttribute("SchedulerType").toString();
//                   FacesMessage Message = new FacesMessage(SchedulerType);   
//                         Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                         FacesContext fc = FacesContext.getCurrentInstance();   
//                         fc.addMessage(null, Message); 
        
        
        this.setSaveUpdate_SchedulerType("UPDATE");
      // return null; 
        return "editSchedular";
        
        }
    public void onClickSaveSchdlrPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number schedulerStaffId = (Number) ADFUtils.getBoundAttributeValue("SchedulerStaffId");
        Boolean recurringFlag = (Boolean) ADFUtils.getBoundAttributeValue("RecurringFlag");
        ADFUtils.findOperation("Commit").execute();
         this.bindEmployeesShifts();
//        FacesMessage Message = new FacesMessage(recurringFlag.toString());   
//              Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//              FacesContext fc = FacesContext.getCurrentInstance();   
//              fc.addMessage(null, Message); 
//        if (recurringFlag) {
//            String repeatStatus = this.callRepeatPattern(schedulerStaffId);
//            if ("SUCCESS".equals(repeatStatus)) {
//                ADFUtils.findIterator("AsSchedulerStaffVOIterator").getViewObject().executeQuery();
//                SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been applied successfully.",
//                                              FacesMessage.SEVERITY_INFO);
//            } else {
//                SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been failed.", FacesMessage.SEVERITY_ERROR);
//            }
//        }
//        
        this.getSchlrPop().hide();
    }

    public void onClickCancelSchdlrPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        if(this.getSaveUpdate_text().equals("Save"))
        {
        ADFUtils.findOperation("DeleteSchdlrStaff").execute();
            ADFUtils.findOperation("Commit").execute();
        }
//        else if(this.getSaveUpdate_text().equals("Update"))
//        {
//        ADFUtils.findOperation("Rollback").execute();
//        }
        this.getSchlrPop().hide();
    }

    private void showSchdlrPopup() {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getSchlrPop().show(hints);
    }
    private void showvalPopup() {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getValPop().show(hints);
    }





  

  

 

    private String callRepeatPattern(Number schedulerStaffId,Date scdate,Date enddate) {
        OperationBinding oper = ADFUtils.findOperation("repeatPattern");
        oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
        oper.getParamsMap().put("schedulerdate", scdate);
        oper.getParamsMap().put("schedulerenddate", enddate);
        oper.execute();
        String retValue = (String) oper.getResult();
        return retValue;
    }
    private String callRepeatPatternNewsRoom(Number schedulerStaffId,Date scdate,Date enddate,Number fkshift,Number fkshiftattrbuteid) {
        OperationBinding oper = ADFUtils.findOperation("repeatPatternNewsRoom");
        oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
        oper.getParamsMap().put("schedulerdate", scdate);
        oper.getParamsMap().put("schedulerenddate", enddate);
        oper.getParamsMap().put("fkshiftid", fkshift);
        oper.getParamsMap().put("fkshiftattrbuteid", fkshiftattrbuteid);        

        oper.execute();
        String retValue = (String) oper.getResult();
        return retValue;
    }
    public void onClickSubmitforApprovalBtn(ActionEvent actionEvent) {
        // Add event code here...
        DCIteratorBinding iter = ADFUtils.findIterator("AsApprovalTypesVOIterator");
        Key key = new Key(new Object[] { "SCHEDULAR" });
        iter.setCurrentRowWithKey(key.toStringFormat(true));

        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getApprvalPop().show(hints);
    }

    public void setApprvalPop(RichPopup apprvalPop) {
        this.apprvalPop = apprvalPop;
    }

    public RichPopup getApprvalPop() {
        return apprvalPop;
    }

    public String isSchdlrExistsOnDate(Number schedulerId, Number personId, Date schdlrStartDate) {
        OperationBinding oper = ADFUtils.findOperation("schdlrExistsOnDate");
        oper.getParamsMap().put("schedulerId", schedulerId);
        oper.getParamsMap().put("personId", personId);
        oper.getParamsMap().put("schdlrStartDate", schdlrStartDate);
        String result = (String) oper.execute();
        return result;
    }

    private HashMap getSchdlrAssignIntesection(Number schedulerId, Number personId, Date schdlrStartDate) {
        OperationBinding oper = ADFUtils.findOperation("getSchdlrAssignIntesection");
        oper.getParamsMap().put("schedulerId", schedulerId);
        oper.getParamsMap().put("personId", personId);
        oper.getParamsMap().put("schdlrStartDate", schdlrStartDate);
        HashMap result = (HashMap) oper.execute();
        return result;
    }

    private HashMap getSchdlrLeaveIntesection(Number schedulerId, Number personId, Date schdlrStartDate) {
        OperationBinding oper = ADFUtils.findOperation("getSchdlrLeaveIntesection");
        oper.getParamsMap().put("schedulerId", schedulerId);
        oper.getParamsMap().put("personId", personId);
        oper.getParamsMap().put("schdlrStartDate", schdlrStartDate);
        HashMap result = (HashMap) oper.execute();
        return result;
    }

    public void onClickDeleteAllCalCntxtPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number schedulerStaffId = (Number) ADFUtils.getBoundAttributeValue("SchedulerStaffId");
        OperationBinding oper = ADFUtils.findOperation("deleteAllSchdlr");
        oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
        oper.execute();
    }

    public void onClickCancelCalCntxPopBtn(ActionEvent actionEvent) {
        // Add event code here...
       // this.createTransRecordShowPop("CANCEL");
    }

    public void onClickReplaceCalCntxPopBtn(ActionEvent actionEvent) {
        // Add event code here...
      //  this.createTransRecordShowPop("REPLACE");
    }

    public void onClickChangeTimingCalCntxPopBtn(ActionEvent actionEvent) {
        // Add event code here...
       // this.createTransRecordShowPop("CHANGE");
    }

    private void createTransRecordShowPop(String transType, Number SchedulerStaffId ,Date scheulesdate,Number PersonId) {
  
      Date schedularStartDate =scheulesdate;
       ADFUtils.findOperation("CreateInsertTrans").execute();
      ADFUtils.setBoundAttributeValue("SchedulerStaffIdTrans", SchedulerStaffId );
        ADFUtils.setBoundAttributeValue("TransTypeAttr", transType);
        Number personId = (Number) ADFUtils.evaluateEL("#{sessionScope.userHelper.personId}");
        ADFUtils.setBoundAttributeValue("RequesterId", personId);
        ADFUtils.setBoundAttributeValue("EffectiveStartDate", schedularStartDate);
        ADFUtils.setBoundAttributeValue("EffectiveEndDate", schedularStartDate);

        DCIteratorBinding iter = ADFUtils.findIterator("AsApprovalTypesVOIterator");
        Key key = null;
        if ("CHANGE".equals(transType)) {
            key = new Key(new Object[] { "CHANGE" });

            ADFUtils.setBoundAttributeValue("StartTimeHoursTrans",
                                            (Number) ADFUtils.getBoundAttributeValue("StartTimeHours"));
            ADFUtils.setBoundAttributeValue("StartTimeMinutesTrans",
                                            (Number) ADFUtils.getBoundAttributeValue("StartTimeMinutes"));
            ADFUtils.setBoundAttributeValue("EndTimeHoursTrans",
                                            (Number) ADFUtils.getBoundAttributeValue("EndTimeHours"));
            ADFUtils.setBoundAttributeValue("EndTimeMinutesTrans",
                                            (Number) ADFUtils.getBoundAttributeValue("EndTimeMinutes"));
        } else if ("REPLACE".equals(transType)) {
            key = new Key(new Object[] { "REPLACE" });
        } else if ("CANCEL".equals(transType)) {
            key = new Key(new Object[] { "CANCEL" });
        }
        iter.setCurrentRowWithKey(key.toStringFormat(true));

//        Number oldPersonId = PersonId;
//            //(Number) ADFUtils.getBoundAttributeValue("PersonId");
//       Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
//    HashMap schdlrAssignIntesectionMap =
//          this.getSchdlrAssignIntesection(schedulerId, oldPersonId, schedularStartDate);
//        if ("Y".equals(schdlrAssignIntesectionMap.get("STATUS"))) {
//            this.setSchdlrAssignIntersectionMsg((String) schdlrAssignIntesectionMap.get("STATUS_MESSAGE"));
//        } else {
//            this.setSchdlrAssignIntersectionMsg(null);
//        }
//  FacesMessage Message = new FacesMessage(SchedulerStaffId.toString());   
//      Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//      FacesContext fc = FacesContext.getCurrentInstance();   
//      fc.addMessage(null, Message); 
       
       //SchedulerStaffId
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getTranslPop().show(hints);
    }

    public void setTranslPop(RichPopup translPop) {
        this.translPop = translPop;
    }

    public RichPopup getTranslPop() {
        return translPop;
    }

    public void onClickSaveTransPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("Commit").execute();
        Number schedulerTransHdrId = (Number) ADFUtils.getBoundAttributeValue("SchedulerTransHdrId");
        OperationBinding oper = ADFUtils.findOperation("submitSchdlrTrans");
        oper.getParamsMap().put("schedulerTransHdrId", schedulerTransHdrId);
        oper.execute();

        ADFUtils.findIterator("AsSchedulerTransHdrsVOIterator").getCurrentRow().refresh(Row.REFRESH_WITH_DB_FORGET_CHANGES);

        this.getTranslPop().hide();
    }

    public void onClickCancelTransPopBtn(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("DeleteTrans").execute();
        ADFUtils.findOperation("Commit").execute();
        this.getTranslPop().hide();
    }

    public void onClickConfirmSubmitApprovalBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
        Number personId = (Number) ADFUtils.evaluateEL("#{sessionScope.userHelper.personId}");

        OperationBinding oper = ADFUtils.findOperation("submitScheduler");
        oper.getParamsMap().put("schedulerId", schedulerId);
        oper.getParamsMap().put("personId", personId);
        oper.execute();

        SchedulerUtil.showPageMessage("Approval", "Schedules has been submitted for approval.",
                                      FacesMessage.SEVERITY_INFO);
        this.getApprvalPop().hide();
    }

    public void onClickCancelSubmitApprovalBtn(ActionEvent actionEvent) {
        // Add event code here...
        this.getApprvalPop().hide();
    }

    public void onClickApprovalStatusBtn(ActionEvent actionEvent) {
        // Add event code here...
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getApprvStatusPop().show(hints);
    }

    public void setApprvStatusPop(RichPopup apprvStatusPop) {
        this.apprvStatusPop = apprvStatusPop;
    }

    public RichPopup getApprvStatusPop() {
        return apprvStatusPop;
    }



    public void runJavaScriptCode(String javascriptCode) {
        FacesContext facesCtx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service = Service.getRenderKitService(facesCtx, ExtendedRenderKitService.class);
        service.addScript(facesCtx, javascriptCode);
    }



    public void onClickValidateBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");

        OperationBinding validateSchedulerOper = ADFUtils.findOperation("validateScheduler");
        validateSchedulerOper.getParamsMap().put("schedulerId", schedulerId);
        String validateReportResult = (String) validateSchedulerOper.execute();

        this.setValidateReport(validateReportResult.replaceAll("\n", "<br/>"));

        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getValidatetPop().show(hints);
    }

    public void setValidatetPop(RichPopup validatetPop) {
        this.validatetPop = validatetPop;
    }

    public RichPopup getValidatetPop() {
        return validatetPop;
    }

    public void setTransApprvPop(RichPopup transApprvPop) {
        this.transApprvPop = transApprvPop;
    }

    public RichPopup getTransApprvPop() {
        return transApprvPop;
    }

    public void onClickShowTransApprovalCycleBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number schedulerTransHdrId = (Number) ADFUtils.getBoundAttributeValue("SchedulerTransHdrId");
        System.out.println("schedulerTransHdrId=" + schedulerTransHdrId);

        ViewObjectImpl transActionHistorySubVO =
            (ViewObjectImpl) ADFUtils.findIterator("TransActionHistorySubVOIterator").getViewObject();
        transActionHistorySubVO.setNamedWhereClauseParam("TransTypepar", "TRANS");
        transActionHistorySubVO.executeQuery();

        for (Row row : transActionHistorySubVO.getAllRowsInRange()) {
            System.out.println("xxx row=" + row.getAttribute("TransId") + " , " + row.getAttribute("PersonId"));
        }

        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getTransApprvPop().show(hints);
    }

    public void onClickOkTransApprovalCycleBtn(ActionEvent actionEvent) {
        // Add event code here...
        this.getTransApprvPop().hide();
    }

    public void onClickConfirmDeleteSchedulerBtn(ActionEvent actionEvent) {
        // Add event code here...
        Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
        OperationBinding oper = ADFUtils.findOperation("deleteScheduler");
        oper.getParamsMap().put("schedulerId", schedulerId);
        oper.execute();

        ADFUtils.findIterator("AsSchedulersVOIterator").getViewObject().executeQuery();

        this.getDelSchedulerPop().hide();
    }

    public void onClickCancelDeleteSchedulerBtn(ActionEvent actionEvent) {
        // Add event code here...
        this.getDelSchedulerPop().hide();
    }

    public void setDelSchedulerPop(RichPopup delSchedulerPop) {
        this.delSchedulerPop = delSchedulerPop;
    }

    public RichPopup getDelSchedulerPop() {
        return delSchedulerPop;
    }

    public void setSchdlrLeaveIntersectionMsg(String schdlrLeaveIntersectionMsg) {
        this.schdlrLeaveIntersectionMsg = schdlrLeaveIntersectionMsg;
    }

    public String getSchdlrLeaveIntersectionMsg() {
        return schdlrLeaveIntersectionMsg;
    }


    // Added By Heba 24-7-2017//
    //for filterization of list of value of replaced by in assignment 
    public  void onLovLaunch(LaunchPopupEvent launchPopupEvent) {
     
        Number wkId = getGurrentShiftAttbiuteRowData()[2];
        
        BindingContext bctx = BindingContext.getCurrent();
        BindingContainer bindings = bctx.getCurrentBindingsEntry();
        
       Number[]currentrow=getGurrentShiftAttbiuteRowData();
           
            String workgrouname=this.getCurrentSelectedShiftWorkgroupName(currentrow[1],currentrow[2])[1];
                
                //getGurrentShiftAttbiuteRowData()[2].toString();
           // SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            FacesCtrlLOVBinding lov = (FacesCtrlLOVBinding)bindings.get("Replacedby");
            lov.getListIterBinding().getViewObject().setWhereClause(null);
            lov.getListIterBinding().getViewObject().setWhereClauseParams(null);
            String whereclause="";
            String workgroupclause="WK_CATEGORY_ID=".concat(ADFUtils.getBoundAttributeValue("FkWkCategoryId").toString());
            whereclause=workgroupclause;
            lov.getListIterBinding().getViewObject().setWhereClause(whereclause);
            String orderclause="DECODE(WORK_GROUP_NAME,'".concat(workgrouname).concat("',1,2)");
            System.out.print(orderclause);
            lov.getListIterBinding().getViewObject().setOrderByClause(orderclause);
                //("DECODE(WORK_GROUP_NAME,'ERP Testing',1,2)");
            
    }
    
    
    public  void onLovLaunchEmployee(LaunchPopupEvent launchPopupEvent) {
     
     
        Number wkId = getGurrentShiftAttbiuteRowData()[2];
      
     BindingContext bctx = BindingContext.getCurrent();
     BindingContainer bindings = bctx.getCurrentBindingsEntry();
  

           
            String workgroupid=getGurrentShiftAttbiuteRowData()[2].toString();
           // SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            FacesCtrlLOVBinding lov = (FacesCtrlLOVBinding)bindings.get("EmployeeName");
            lov.getListIterBinding().getViewObject().setWhereClause(null);
            lov.getListIterBinding().getViewObject().setWhereClauseParams(null);
            String whereclause="";
            String workgroupclause="WORK_GROUP_ID=".concat(workgroupid);
            whereclause=workgroupclause;
            lov.getListIterBinding().getViewObject().setWhereClause(whereclause);
       
//       
//           
//       
//        
//        
             
    }

    public void setcalStyles(HashMap calStyles) {
        this.calStyles = calStyles;
    }

   

  
    public void onClickValidate1(ActionEvent actionEvent) {
        // Add event code here...
  
     this.validationofday(this.getDateFirstDay());
    
    }
    public void onClickValidate2(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDateSecondDay());
    
    }
    public void onClickValidate3(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDateThirdDay());
    
    }
    public void onClickValidate4(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDateFourthDay());
    
    }
    public void onClickValidate5(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDateFifthDay());
    
    }
    public void onClickValidate6(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDateSixDay());
    
    }
    public void onClickValidate7(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDateSevenDay());
    
    }
    public void onClickValidate8(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi8());
    
    }
    public void onClickValidate9(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi9());
    
    }
    public void onClickValidate10(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi10());
    
    }
    public void onClickValidate11(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi11());
    
    }
    public void onClickValidate12(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi12());
    
    }
    public void onClickValidate13(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi13());
    
    }
    public void onClickValidate14(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi14());
    
    }
    public void onClickValidate15(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi15());
    
    }
    public void onClickValidate16(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi16());
    
    }
    public void onClickValidate17(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi17());
    
    }
    public void onClickValidate18(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi18());
    
    }
    public void onClickValidate19(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi19());
    
    }
    public void onClickValidate20(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi20());
    
    }
    
    public void onClickValidate21(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi21());
    
    }
    public void onClickValidate22(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi22());
    
    }
    public void onClickValidate23(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi23());
    
    }
    public void onClickValidate24(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi24());
    
    }
    public void onClickValidate25(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi25());
    
    }
    public void onClickValidate26(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi26());
    
    }
    public void onClickValidate27(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi27());
    
    }
    public void onClickValidate28(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi28());
    
    }
    public void onClickValidate29(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi29());
    
    }
    public void onClickValidate30(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi30());
    
    }
    public void onClickValidate31(ActionEvent actionEvent) {
        // Add event code here...
    
     this.validationofday(this.getDatedsi31());
    
    }
  public void validationofday(java.util.Date day)
  {
      
      
          Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
          OperationBinding validateSchedulerOper = ADFUtils.findOperation("getScheduleValidateDetail");
          validateSchedulerOper.getParamsMap().put("activeday", day);
          validateSchedulerOper.getParamsMap().put("SchedulerId", schedulerId);
          String Result = (String) validateSchedulerOper.execute();
          this.setValidatedetails(Result);
          this.showvalPopup();
      
      
      }
    public void setHolidays(MyDateCustomizer holidays) {
        this.holidays = holidays;
    }

    public MyDateCustomizer getHolidays() {
        return holidays;
    }


    public void setValPop(RichPopup valPop) {
        this.valPop = valPop;
    }

    public RichPopup getValPop() {
        return valPop;
    }

    public void setValidatedetails(String validatedetails) {
        this.validatedetails = validatedetails;
    }

    public String getValidatedetails() {
        return validatedetails;
    }


    public void setRadioBtnValue(String radioBtnValue) {
        this.radioBtnValue = radioBtnValue;
    }

    public String getRadioBtnValue() {
        return radioBtnValue;
    }

    public void setWeekStartDate(java.util.Date WeekStartDate) {
        this.WeekStartDate = WeekStartDate;
    }

    public java.util.Date getWeekStartDate() {
        if(WeekStartDate!= null && (!WeekStartDate.equals("")))
         return WeekStartDate;
            
         else 
        {
               if(this.getRadioBtnValue().equals("Month"))
                   
               {
                       Row asSchedulersRow = ADFUtils.findIterator("AsSchedulerMonthVO1Iterator").getCurrentRow();
                       Date StartDate =(Date) asSchedulersRow.getAttribute("Datemonthstart"); 
                return  new java.util.Date(StartDate.dateValue().getTime());
                }
               
                       //new java.util.Date(getScheduleFirstweekDay().dateValue().getTime());}
            else 
                {return  new java.util.Date(getScheduleFirstweekDay().dateValue().getTime());}
                
            }

    }
    public void setFirstDay(String FirstDay) {
        this.FirstDay = FirstDay;
    }

    public String getFirstDay() {
        return   this.DayOfWeek(this.getWeekStartDate());
       // return FirstDay;
    }

    public void setSecondDay(String SecondDay) {
        this.SecondDay = SecondDay;
    }

    public String getSecondDay() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 1));
        //return SecondDay;
    }

    public void setThirdDay(String ThirdDay) {
        
        this.ThirdDay = ThirdDay;
    }

    public String getThirdDay() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 2));
       // return ThirdDay;
    }

    public void setFourthDay(String FourthDay) {
        this.FourthDay = FourthDay;
    }

    public String getFourthDay() {
       // return FourthDay;
       return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 3));
    }

    public void setFifthDay(String FifthDay) {
        this.FifthDay = FifthDay;
    }

    public String getFifthDay() {
       // return FifthDay;
       return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 4));
    }

    public void setSixDay(String SixDay) {
        this.SixDay = SixDay;
    }

    public String getSixDay() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 5));
       // return SixDay;
    }

    public void setSevenDay(String SevenDay) {
        this.SevenDay = SevenDay;
    }

    public String getSevenDay() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 6));
       // return SevenDay;
    }

    public void setDsi8(String dsi8) {
        this.dsi8 = dsi8;
    }

    public String getDsi8() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 7));
       // return dsi8;
    }

    public void setDsi9(String dsi9) {
        this.dsi9 = dsi9;
    }

    public String getDsi9() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 8));
       // return dsi9;
    }

    public void setDsi10(String dsi10) {
        this.dsi10 = dsi10;
    }

    public String getDsi10() {
       
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 9));
       // return dsi10;
    }

    public void setDsi11(String dsi11) {
        this.dsi11 = dsi11;
    }

    public String getDsi11() {
      
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 10));
      //  return dsi11;
    }

    public void setDsi12(String dsi12) {
        this.dsi12 = dsi12;
    }

    public String getDsi12() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 11));
      //  return dsi12;
    }

    public void setDsi13(String dsi13) {
        this.dsi13 = dsi13;
    }

    public String getDsi13() {
       
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 12));
       // return dsi13;
    }

    public void setDsi14(String dsi14) {
        this.dsi14 = dsi14;
    }

    public String getDsi14() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 13));
      //  return dsi14;
    }

    public void setDsi15(String dsi15) {
        this.dsi15 = dsi15;
    }

    public String getDsi15() {
       
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 14));
       // return dsi15;
    }

    public void setDsi16(String dsi16) {
        this.dsi16 = dsi16;
    }

    public String getDsi16() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 15));
       // return dsi16;
    }

    public void setDsi17(String dsi17) {
        this.dsi17 = dsi17;
    }

    public String getDsi17() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 16));
       // return dsi17;
    }

    public void setDsi18(String dsi18) {
        this.dsi18 = dsi18;
    }

    public String getDsi18() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 17));
       // return dsi18;
    }

    public void setDsi19(String dsi19) {
        this.dsi19 = dsi19;
    }

    public String getDsi19() {
       
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 18));
       // return dsi19;
    }

    public void setDsi20(String dsi20) {
        this.dsi20 = dsi20;
    }

    public String getDsi20() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 19));
      //  return dsi20;
    }

    public void setDsi21(String dsi21) {
        this.dsi21 = dsi21;
    }

    public String getDsi21() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 20));
       
       // return dsi21;
    }

    public void setDsi22(String dsi22) {
        this.dsi22 = dsi22;
    }

    public String getDsi22() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 21));
      //  return dsi22;
    }

    public void setDsi23(String dsi23) {
        this.dsi23 = dsi23;
    }

    public String getDsi23() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 22));
      //  return dsi23;
    }

    public void setDsi24(String dsi24) {
        this.dsi24 = dsi24;
    }

    public String getDsi24() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 23));
       // return dsi24;
    }

    public void setDsi25(String dsi25) {
        this.dsi25 = dsi25;
    }

    public String getDsi25() {
       
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 24));
       // return dsi25;
    }

    public void setDsi26(String dsi26) {
        this.dsi26 = dsi26;
    }

    public String getDsi26() {
       
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 25));
       // return dsi26;
    }

    public void setDsi27(String dsi27) {
        this.dsi27 = dsi27;
    }

    public String getDsi27() {
      
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 26));
      //  return dsi27;
    }

    public void setDsi28(String dsi28) {
        this.dsi28 = dsi28;
    }

    public String getDsi28() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 27));
        //return dsi28;
    }

    public void setDsi29(String dsi29) {
        this.dsi29 = dsi29;
    }

    public String getDsi29() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 28));
       // return dsi29;
    }

    public void setDsi30(String dsi30) {
        this.dsi30 = dsi30;
    }

    public String getDsi30() {
       
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 29));
       // return dsi30;
    }

    public void setDsi31(String dsi31) {
        this.dsi31 = dsi31;
    }

    public String getDsi31() {
        return   this.DayOfWeek(this.addDays(this.getWeekStartDate(), 30));
       // return dsi31;
    }
  

    public void setPc1(RichPanelCollection pc1) {
        this.pc1 = pc1;
    }

    public RichPanelCollection getPc1() {
        return pc1;
    }
    // Added By Heba  11-9-2017 //

       
      // Events Start //  
     //   public void SelectMonthChanging(ValueChangeEvent valueChangeEvent) {
            // Drop Down of months
//                  valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
//                   AdfFacesContext.getCurrentInstance().addPartialTarget(pc1);
//               FacesContext.getCurrentInstance().renderResponse();
//            Row asSchedulersRow = ADFUtils.findIterator("AsSchedulerMonthVO1Iterator").getCurrentRow();
//            Date StartDate =(Date) asSchedulersRow.getAttribute("Datemonthstart"); 
//            this.setWeekStartDate(new java.util.Date(StartDate.dateValue().getTime()));
//            exceuteSections();
            
            
        
       // }
    public void SelectWeekMonthChanging(ValueChangeEvent valueChangeEvent) {
        // Radio Button Change Event 
                      valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                       AdfFacesContext.getCurrentInstance().addPartialTarget(pc1);
                   FacesContext.getCurrentInstance().renderResponse();
     
         // if(this.getRadioBtnValue().equals("Month"))
         // {

           // set current month depen on current weekstartdate
          // setCurrentMonthRow();
        exceuteSections();
//        Row asSchedulersRow = ADFUtils.findIterator("AsSchedulerMonthVO1Iterator").getCurrentRow();
//        Date StartDate =(Date) asSchedulersRow.getAttribute("Datemonthstart"); 
//        this.setWeekStartDate(new java.util.Date(StartDate.dateValue().getTime()));
          
        
        
        //  }
//          else 
//          {
//                  Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
//                  Date StartDate =(Date) asSchedulersRow.getAttribute("StartDate"); 
//                  this.setWeekStartDate(new java.util.Date(StartDate.dateValue().getTime()));
//              
//              }
        
    }
     public void setCurrentMonthRow()
     {
             BindingContext bindingctx = BindingContext.getCurrent();

             BindingContainer binding =bindingctx.getCurrentBindingsEntry();
                 OperationBinding opt =binding.getOperationBinding("setCurrentRowWithKeyValue");
  Number currkey=new Number(132018);
  //CurrentMonth()
             opt.getParamsMap().put("rowKey",currkey);

             opt.execute();
         
         }

        public String SearchSchedule_action() {
            this.setShowhidetable(true);
          bindEmployeesShifts();
            // Add event code here...
            //this.getScheduleFirstweekDay();
    //        FacesMessage fm = new FacesMessage(getScheduleFirstweekDay().toString());
    //                  fm.setSeverity(FacesMessage.SEVERITY_INFO);
    //                  FacesContext fctx = FacesContext.getCurrentInstance();
    //                  fctx.addMessage(null, fm);

            return null;
        }
     
        public String Cancel_action() {
            // Add event code here...
            this.setWeekStartDate(null);
            this.setShowhidetable(false);
            return "cancel";
        }
        //// Events End ///
        // Custom Methods  Start  //
        private Date getScheduleFirstweekDay() {
            
        Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
        Date StartDate =(Date) asSchedulersRow.getAttribute("StartDate");
         BindingContainer bc = BindingContext.getCurrent().getCurrentBindingsEntry();
         OperationBinding oper = bc.getOperationBinding("getStartWeekDaySchedule");
        oper.getParamsMap().put("SchedulerStartDate", StartDate);
        Date result = (Date) oper.execute();
        return result;
        }

        public  String DayOfWeek(java.util.Date dt)
        {
                   Calendar cal = Calendar.getInstance();
                   cal.setTime(dt);
                  // int year = cal.get(Calendar.YEAR);
                   int month = cal.get(Calendar.MONTH);
                   int day = cal.get(Calendar.DAY_OF_WEEK);
                   int dayofmonth=cal.get(Calendar.DAY_OF_MONTH);
                   String DayofWeek="";
                   String MonthofYear="";
                   if(day==1)
                       DayofWeek="Sun";
                 else  if(day==2)
                       DayofWeek= "Mon";
              else  if(day==3)
                    DayofWeek= "Tue";
              else  if(day==4)
                    DayofWeek= "Wed";
             else   if(day==5)
                    DayofWeek= "Thurs";
             else   if(day==6)
                    DayofWeek="Fri";
           else     if(day==7)
                    DayofWeek= "Sat";
                //------------Month-------
                if(month==0)
                    MonthofYear="Jan";
              else  if(month==1)
                    MonthofYear="Feb";
                else  if(month==2)
                      MonthofYear="March";
                else  if(month==3)
                      MonthofYear="Apr";
                else  if(month==4)
                      MonthofYear="May";
                else  if(month==5)
                      MonthofYear="Jun";
                else  if(month==6)
                      MonthofYear="Jul";
                  else  if(month==7)
                        MonthofYear="Aug";
                  else  if(month==8)
                        MonthofYear="Sept";
                  else  if(month==9)
                        MonthofYear="Oct";
                  else  if(month==10)
                        MonthofYear="Nov";
                else  if(month==11)
                      MonthofYear="Dec";
                return DayofWeek+" "+MonthofYear+" "+String.valueOf(dayofmonth);
              
            }
        // Method to add Days  for the the date inserted in choose week start day to get the other six days
        public  java.util.Date addDays(java.util.Date date, int days)
          {
              Calendar cal = Calendar.getInstance();
              cal.setTime(date);
              cal.add(Calendar.DATE, days); //minus number would decrement the days
              return cal.getTime();
          }
      public SchedulerAMImpl getApplicationModule()
      {
          
              FacesContext fctx = FacesContext.getCurrentInstance(); 
              BindingContext bindingContext = BindingContext.getCurrent();
              DCDataControl dc = bindingContext.findDataControl("SchedulerAMDataControl");
              SchedulerAMImpl am = (SchedulerAMImpl)dc.getDataProvider();
            return am;
          }
        public String getShiftDay(Number SchdeulerId ,Number PersonId,java.util.Date day)
        {
                SchedulerAMImpl sc=this.getApplicationModule();
                
            AsSchedulerStaffShiftTableVOImpl vo=sc.getAsSchedulerStaffShiftTableVO();
                vo.setWhereClauseParams(null);
                vo.setWhereClause(null);
                DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                vo.setWhereClause("person_id=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
                vo.setWhereClauseParams(new Object[] { PersonId, df.format(day) ,SchdeulerId});
                vo.executeQuery();
                if (vo.first() != null) {
               
                    AsSchedulerStaffShiftTableVORowImpl r=(AsSchedulerStaffShiftTableVORowImpl)vo.first();
                     return r.getShiftName();
                }
     
                /// Added By Heba 5-December-2017  to view leave type if the employee hase this day as leave 
               else 
                {String   LeaveType=this.getLeaveType(PersonId,day);
                if(!LeaveType.equals("NO Leave"))
                    return LeaveType;
                        //LeaveType;
                    
                }
            return "000000000000000";
            }
        
    public String getLeaveType(Number PersonId,java.util.Date day) {
        StringBuilder st = new StringBuilder();
       
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
         
         String   WeekDay = df.format(day);
      

        SchedulerAMImpl sc=this.getApplicationModule();
        AspublicSpaceLeavesVOImpl ps = sc.getAspublicSpaceLeavesVO1();
        ps.setNamedWhereClauseParam("pdate", WeekDay);
        ps.setNamedWhereClauseParam("pperson_id", PersonId);
        ps.executeQuery();
        if (ps.first() != null) {
               
            AspublicSpaceLeavesVORowImpl row = (AspublicSpaceLeavesVORowImpl) ps.first();
                          
            return "(".concat(row.getName()).concat(")");
        } else
            return "NO Leave";


    }
        
    public String getShiftColor(Number SchdeulerId ,Number PersonId,java.util.Date day)
    {
            SchedulerAMImpl sc=this.getApplicationModule();
            String color= "transparent";
        AsSchedulerStaffShiftTableVOImpl vo=sc.getAsSchedulerStaffShiftTableVO();
            vo.setWhereClauseParams(null);
            vo.setWhereClause(null);
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            vo.setWhereClause("person_id=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
            vo.setWhereClauseParams(new Object[] { PersonId, df.format(day) ,SchdeulerId});
            vo.executeQuery();
            if (vo.first() != null) {
              
                AsSchedulerStaffShiftTableVORowImpl r=(AsSchedulerStaffShiftTableVORowImpl)vo.first();
               
                 color=r.getStatusColor();
               
                     //r.getStatusColor();
            }
            else 
             {
                String   LeaveType=this.getLeaveType(PersonId,day);
             if(!LeaveType.equals("NO Leave"))
                 color="#ffb134";
                     //LeaveType;
                 
             }
               
                
              
        return color;
        }
        public void bindEmployeesShifts()
        {
               //get Paramerts used to get employee shift in each day 
            // ScheduleId from scheduler iterator
            // personid form schedulerstaff table iterator
            //start date from weekstart date varaible
            Number SchedulerID=new Number() ;
            Number  PersonId ;
                Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
              try {
               
                  SchedulerID=    new oracle.jbo.domain.Number(Integer.valueOf(asSchedulersRow.getAttribute("SchedulerId").toString()));
              } catch (SQLException e) {
              }
              ///first loop through schedulerstaff table iterator
              SchedulerAMImpl sc=this.getApplicationModule();
              
              AsSchdeulerStaffTableVOImpl staff=sc.getAsSchdeulerStaffTableVO();
             // DCIteratorBinding  staff=ADFUtils.findIterator("AsSchdeulerStaffTableVOIterator");
              //while (staff.hasn)
             RowSetIterator siter =  staff.createRowSetIterator(null);
             siter.reset();
             while(siter.hasNext())
             {
                 // looping through all employess row of schedulerstaff table 
                 Row r = siter.next();
                  try {
                      PersonId = new oracle.jbo.domain.Number(Integer.valueOf(r.getAttribute("PersonId").toString()));
                     // set shift name for day1
                      // if radion buttonvalue is week loop with 7 only
                      //if radion buttonvalue is month loop with month daynum
                     // if(this.getRadioBtnValue().equals("Month")){
                    // r.setAttribute("Dsn1",this.getShiftDay(SchedulerID, PersonId, this.getWeekStartDate()));
                          r.setAttribute("Dsn1",this.getShiftDay(SchedulerID, PersonId, this.getWeekStartDate()));
                          r.setAttribute("Dsn2",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),1)));
                          r.setAttribute("Dsn3",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),2)));
                          r.setAttribute("Dsn4",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),3)));
                          r.setAttribute("Dsn5",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),4)));
                          r.setAttribute("Dsn6",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),5)));
                          r.setAttribute("Dsn7",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),6)));
                          r.setAttribute("Dsn8",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),7)));
                          r.setAttribute("Dsn9",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),8)));
                          r.setAttribute("Dsn10",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),9)));
                          r.setAttribute("Dsn11",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),10)));
                          r.setAttribute("Dsn12",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),11)));
                          r.setAttribute("Dsn13",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),12)));
                          r.setAttribute("Dsn14",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),13)));
                          r.setAttribute("Dsn15",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),14)));
                          r.setAttribute("Dsn16",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),15)));
                          r.setAttribute("Dsn17",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),16)));
                          r.setAttribute("Dsn18",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),17)));
                          r.setAttribute("Dsn19",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),18)));
                          r.setAttribute("Dsn20",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),19)));
                          r.setAttribute("Dsn21",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),20)));
                          r.setAttribute("Dsn22",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),21)));
                          r.setAttribute("Dsn23",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),22)));
                          r.setAttribute("Dsn24",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),23)));
                          r.setAttribute("Dsn25",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),24)));
                          r.setAttribute("Dsn26",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),25)));
                          r.setAttribute("Dsn27",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),26)));
                          r.setAttribute("Dsn28",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),27)));
                          r.setAttribute("Dsn29",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),28)));
                          r.setAttribute("Dsn30",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),29)));
                          r.setAttribute("Dsn31",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(),30)));
                         
                          r.setAttribute("Dsc1",this.getShiftColor(SchedulerID, PersonId, this.getWeekStartDate()));
                          r.setAttribute("Dsc2",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 1)));
                          r.setAttribute("Dsc3",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 2)));
                          r.setAttribute("Dsc4",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 3)));
                          r.setAttribute("Dsc5",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 4)));
                          r.setAttribute("Dsc6",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 5)));
                          r.setAttribute("Dsc7",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 6)));
                          
                          r.setAttribute("Dsc8",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 7)));
                          r.setAttribute("Dsc9",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 8)));
                          r.setAttribute("Dsc10",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 9)));
                          r.setAttribute("Dsc11",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 10)));
                          r.setAttribute("Dsc12",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 11)));
                          r.setAttribute("Dsc13",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 12)));
                          r.setAttribute("Dsc14",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 13)));
                          
                          r.setAttribute("Dsc15",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 14)));
                          r.setAttribute("Dsc16",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 15)));
                          r.setAttribute("Dsc17",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 16)));
                          r.setAttribute("Dsc18",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 17)));
                          r.setAttribute("Dsc19",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 18)));
                          r.setAttribute("Dsc20",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 19)));
                          r.setAttribute("Dsc21",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 20)));
                          
                          r.setAttribute("Dsc22",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 21)));
                          r.setAttribute("Dsc23",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 22)));
                          r.setAttribute("Dsc24",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 23)));
                          r.setAttribute("Dsc25",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 24)));
                          r.setAttribute("Dsc26",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 25)));
                          r.setAttribute("Dsc27",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 26)));
                          r.setAttribute("Dsc28",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 27)));
                          
                          r.setAttribute("Dsc29",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 28)));
                          r.setAttribute("Dsc30",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 29)));
                          r.setAttribute("Dsc31",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 30)));

                          
                    
                   //   }
//                      else
//                      {
//                           
//                              r.setAttribute("Dsn1",this.getShiftDay(SchedulerID, PersonId, this.getWeekStartDate()));
//                              r.setAttribute("Dsn2",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 1)));
//                              r.setAttribute("Dsn3",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 2)));
//                              r.setAttribute("Dsn4",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 3)));
//                              r.setAttribute("Dsn5",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 4)));
//                              r.setAttribute("Dsn6",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 5)));
//                              r.setAttribute("Dsn7",this.getShiftDay(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 6)));
//                              r.setAttribute("Dsc1",this.getShiftColor(SchedulerID, PersonId, this.getWeekStartDate()));
//                              r.setAttribute("Dsc2",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 1)));
//                              r.setAttribute("Dsc3",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 2)));
//                              r.setAttribute("Dsc4",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 3)));
//                              r.setAttribute("Dsc5",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 4)));
//                              r.setAttribute("Dsc6",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 5)));
//                              r.setAttribute("Dsc7",this.getShiftColor(SchedulerID, PersonId, this.addDays(this.getWeekStartDate(), 6)));
//                             // this.setScolor1(this.getShiftColor(SchedulerID, PersonId, this.getWeekStartDate()));
//                         
//                          }
                  } 
             
             catch (SQLException e) {
                  }


              }
             
             
          }
         ///Custom Methods End //
   ///---------------Context Menu Section ---------------/////
        
   public void AddSchedulerStaff(ClientEvent ce){
       
       this.setSaveUpdate_text("Save");
       this.setSaveUpdateNotify_text("Save & Notify");
       java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
       RichTable table = this.getT3();
       Object oldKey = table.getRowKey();
       
       try {
            int rowKeyIndex =
            ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
               
           AddSelectedTableRows(columnDate, rowKeyIndex);  
       
       }
       catch(Exception e){
         e.printStackTrace();
           FacesMessage Message = new FacesMessage(e.getMessage());   
                 Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                 FacesContext fc = FacesContext.getCurrentInstance();   
                 fc.addMessage(null, Message);  
       }
       finally {
          table.setRowKey(oldKey);
       }  
   }
   
    public void PasteSchedulerStaff(ClientEvent ce){
        
      //  this.setSaveUpdate_text("Save");
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
                
            PasteSelectedTableRows(columnDate, rowKeyIndex);  
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
    }
    private void PasteSelectedTableRows(java.util.Date columndate, int rowKeyIndex){
    
        if(this.getCopyValue().equals("UNDEFINED"))
         {
             
                         FacesMessage Message2 = new FacesMessage(this.getCopyShiftId().toString());   
                               Message2.setSeverity(FacesMessage.SEVERITY_WARN);   
                               FacesContext fc2 = FacesContext.getCurrentInstance();   
                               fc2.addMessage(null, Message2);  
            
             }
        else
         {
       RichTable table = this.getT3();
       table.setRowIndex(rowKeyIndex);
       JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
      AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();          
       Number vPersonId = rowToCopyFrom.getPersonId();
      String  vEmpName=rowToCopyFrom.getEmployeeName();
      String vEmpNumber=rowToCopyFrom.getEmployeeNumber();
      Number vWorkGroupId=rowToCopyFrom.getWorkGroupId();
    String vWorkGroupName=rowToCopyFrom.getWorkGroupName();
         ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
         //to get startdate  which is column passed in client listener
         ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
         ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
         ADFUtils.setBoundAttributeValue("PersonId", vPersonId);
         ADFUtils.setBoundAttributeValue("EmployeeNumber", vEmpNumber);
         ADFUtils.setBoundAttributeValue("EmployeeName", vEmpName);
         ADFUtils.setBoundAttributeValue("WorkGroupId", vWorkGroupId);
         ADFUtils.setBoundAttributeValue("WorkGroupName", vWorkGroupName);
             ADFUtils.setBoundAttributeValue("ShiftId",this.getCopyShiftId());
           //  ADFUtils.setBoundAttributeValue("WorkGroupId",this.getCopyWkId());
             ADFUtils.setBoundAttributeValue("RecurringFlag","N");
             ADFUtils.setBoundAttributeValue("BulletinFlag","N");
         //this.showSchdlrPopup();
         ADFUtils.findOperation("Commit").execute();  
         AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
       adffctx.addPartialTarget(table);
        this.bindEmployeesShifts();
         }  
    }
   private void AddSelectedTableRows(java.util.Date columndate, int rowKeyIndex){
 
      RichTable table = this.getT3();
      table.setRowIndex(rowKeyIndex);
      JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
     AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();          
      Number vPersonId = rowToCopyFrom.getPersonId();
     String  vEmpName=rowToCopyFrom.getEmployeeName();
     String vEmpNumber=rowToCopyFrom.getEmployeeNumber();
     Number vWorkGroupId=rowToCopyFrom.getWorkGroupId();
   String vWorkGroupName=rowToCopyFrom.getWorkGroupName();
        ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
        //to get startdate  which is column passed in client listener
        ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
        ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
        ADFUtils.setBoundAttributeValue("PersonId", vPersonId);
        ADFUtils.setBoundAttributeValue("EmployeeNumber", vEmpNumber);
        ADFUtils.setBoundAttributeValue("EmployeeName", vEmpName);
        ADFUtils.setBoundAttributeValue("WorkGroupId", vWorkGroupId);
        ADFUtils.setBoundAttributeValue("WorkGroupName", vWorkGroupName);
        this.showSchdlrPopup();

        AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
      adffctx.addPartialTarget(table);
    }
   public void DeleteScheduleShift(ClientEvent ce){
      
//               ADFUtils.findOperation("DeleteSchdltStaff").execute();
//        ADFUtils.findOperation("Commit").execute();
//        this.bindEmployeesShifts();
               Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
            Number vPersonId = rowToCopyFrom.getPersonId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
            schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
         
                //(Number) sessionscope.get("AAPersonId");
            OperationBinding oper = ADFUtils.findOperation("deleteSchdlr");
            oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
            oper.getParamsMap().put("Type", "Delete");
            oper.getParamsMap().put("PersonID", this.getLoggedPersonId());
            oper.execute();
            this.bindEmployeesShifts();
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
       
   }
    public void DeleteAllScheduleShift(ClientEvent ce){
        
        
        
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");
                DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
               this.setDeleteDateStartB(columnDate);
                Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
                       
                java.util.Date enddate=DateConversion.getUtilDateFromJboDate((oracle.jbo.domain.Date)currwk.getAttribute("EndDate"));
                this.setDeleteDateEndB(enddate);
                this.setDeleteDateMaxB(enddate);
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getDeleteB().show(hints);
//        Number schedulerStaffId = new Number();
//        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
//        RichTable table = this.getT3();
//        Object oldKey = table.getRowKey();
//        
//        try {
//             int rowKeyIndex =
//             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
//             table.setRowIndex(rowKeyIndex);
//            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
//            AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
//            Number vPersonId = rowToCopyFrom.getPersonId();
//            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//            String Schedulesdate=df.format(columnDate);
//            schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
//            OperationBinding oper = ADFUtils.findOperation("deleteAllSchdlr");
//            oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
//            oper.execute();
//            this.bindEmployeesShifts();
//        
//        }
//        catch(Exception e){
//          e.printStackTrace();
//            FacesMessage Message = new FacesMessage(e.getMessage());   
//                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                  FacesContext fc = FacesContext.getCurrentInstance();   
//                  fc.addMessage(null, Message);  
//        }
//        finally {
//           table.setRowKey(oldKey);
//        }  
//       
//       
       
       
       
         
    }
   
      
    public void RepeatPAttern(ClientEvent ce){
       
        
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        this.setRepeatDateStartB(columnDate);
        Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
               
        java.util.Date enddate=DateConversion.getUtilDateFromJboDate((oracle.jbo.domain.Date)currwk.getAttribute("EndDate"));
        this.setRepeatDateEndB(enddate);
        this.setRepeatDateMaxB(enddate);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getRepeatB().show(hints);
//        Number schedulerStaffId = new Number();
//        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
//        RichTable table = this.getT3();
//        Object oldKey = table.getRowKey();
//        
//        try {
//             int rowKeyIndex =
//             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
//             table.setRowIndex(rowKeyIndex);
//            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
//            AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
//            Number vPersonId = rowToCopyFrom.getPersonId();
//            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//            String Schedulesdate=df.format(columnDate);
//            schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
//            String repeatStatus = this.callRepeatPattern(schedulerStaffId);
//            if ("SUCCESS".equals(repeatStatus)) {
//                ADFUtils.findIterator("AsSchedulerStaffVOIterator").getViewObject().executeQuery();
//                SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been applied successfully.",
//                                              FacesMessage.SEVERITY_INFO);
//            } else {
//                SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been failed.", FacesMessage.SEVERITY_ERROR);
//            }
//            this.bindEmployeesShifts();
//        
//        }
//        catch(Exception e){
//          e.printStackTrace();
//            FacesMessage Message = new FacesMessage(e.getMessage());   
//                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                  FacesContext fc = FacesContext.getCurrentInstance();   
//                  fc.addMessage(null, Message);  
//        }
//        finally {
//           table.setRowKey(oldKey);
//        }  
//        
        
        
        
       
       
       
     
     
    }

    public void EditSchedule(ClientEvent ce){
       
        this.setSaveUpdate_text("Update");
        this.setSaveUpdateNotify_text("Update & Notify");
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
            Number vPersonId = rowToCopyFrom.getPersonId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
          schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
//            FacesMessage Message = new FacesMessage(schedulerStaffId.toString());   
//                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                  FacesContext fc = FacesContext.getCurrentInstance();   
//                  fc.addMessage(null, Message);  
            this.getCurrrentSchedulerStaffRow(schedulerStaffId);
           this.showSchdlrPopup();
           
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
        
        
        
        
       
       
       
     
     
    }
    public void CopySchedule(ClientEvent ce){
       
       // this.setSaveUpdate_text("Update");
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
            Number vPersonId = rowToCopyFrom.getPersonId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
          schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
//                FacesMessage Message = new FacesMessage(schedulerStaffId.toString());
//                      Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                      FacesContext fc = FacesContext.getCurrentInstance();
//                      fc.addMessage(null, Message);
            this.getCurrrentSchedulerStaffRowCopyNewsResource(schedulerStaffId);
          // this.showSchdlrPopup();
           
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
        
        
        
        
       
       
       
     
     
    }
    public void CancelSchedule(ClientEvent ce){
       
        
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
            Number vPersonId = rowToCopyFrom.getPersonId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
            schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
            this.getCurrrentSchedulerStaffRow(schedulerStaffId);
           
            this.createTransRecordShowPop("CANCEL",schedulerStaffId, new oracle.jbo.domain.Date(DateConversion.getSqlDateFromUtilDate(columnDate)),vPersonId);
           
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage("Error");   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
        
        
        
        
       
       
       
     
     
    }
    public void ReplaceSchedule(ClientEvent ce){
       
        
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
            Number vPersonId = rowToCopyFrom.getPersonId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
            schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
            this.getCurrrentSchedulerStaffRow(schedulerStaffId);
           
            this.createTransRecordShowPop("REPLACE",schedulerStaffId, new oracle.jbo.domain.Date(DateConversion.getSqlDateFromUtilDate(columnDate)),vPersonId);
           
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage("Error");   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
        
        
        
        
       
       
       
     
     
    }
    public void ChangeTimingSchedule(ClientEvent ce){
       
        
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
            Number vPersonId = rowToCopyFrom.getPersonId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
            schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
            this.getCurrrentSchedulerStaffRow(schedulerStaffId);
           
            this.createTransRecordShowPop("CHANGE",schedulerStaffId, new oracle.jbo.domain.Date(DateConversion.getSqlDateFromUtilDate(columnDate)),vPersonId);
           
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage("Error");   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
        
        
        
        
       
       
       
     
     
    }
    
   
  public Number getCurrentSchedulerStaffId(Number PersonId,String SchdeulerStaffDate)
  {
       Number Scstaffid=new Number();
          Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
          SchedulerAMImpl sc=this.getApplicationModule();
          
          AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffCurrentVO();
          staff.setWhereClauseParams(null);
          staff.setWhereClause(null);
          staff.setWhereClause("person_id=:1 and schedular_start_date=:2 and PARENT_SCHEDULER_STAFF_ID is null and SCHEDULER_ID=:3 ");
          staff.setWhereClauseParams(new Object[] { PersonId, SchdeulerStaffDate,schedulerId });
          staff.executeQuery();
          if (staff.first() != null) {
               AsSchedulerStaffVORowImpl row = ( AsSchedulerStaffVORowImpl) staff.first();
         
              Scstaffid=row.getSchedulerStaffId();
         
          }
              ;
      return Scstaffid;
      
      }
  
    public Number getCurrentSchedulerStaffIdNewsRoom(Number FkShiftAttrId,String SchdeulerStaffDate)
    {
         Number Scstaffid=new Number();
            Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
            SchedulerAMImpl sc=this.getApplicationModule();
            
            AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffCurrentVO();
            staff.setWhereClauseParams(null);
            staff.setWhereClause(null);
            staff.setWhereClause("FK_SCHDLR_SHIFT_ATTRIBUTE_ID=:1 and schedular_start_date=:2 and PARENT_SCHEDULER_STAFF_ID is null and SCHEDULER_ID=:3 ");
            staff.setWhereClauseParams(new Object[] { FkShiftAttrId, SchdeulerStaffDate,schedulerId });
            staff.executeQuery();
            if (staff.first() != null) {
                 AsSchedulerStaffVORowImpl row = ( AsSchedulerStaffVORowImpl) staff.first();
           
                Scstaffid=row.getSchedulerStaffId();
           
            }
                ;
        return Scstaffid;
        
        }
  
  public void getCurrrentSchedulerStaffRow(Number SchedulerStaffID)
  {
          Number Scstaffid=new Number();
             SchedulerAMImpl sc=this.getApplicationModule();
             
             AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
             staff.setWhereClauseParams(null);
             staff.setWhereClause(null);
             staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
             staff.setWhereClauseParams(new Object[] { SchedulerStaffID });
             staff.executeQuery();
    

      }
//    public void getCurrrentSchedulerStaffRowCopy(Number SchedulerStaffID)
//    {
//            Number Scstaffid=new Number();
//               SchedulerAMImpl sc=this.getApplicationModule();
//               
//               AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
//               staff.setWhereClauseParams(null);
//               staff.setWhereClause(null);
//               staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
//               staff.setWhereClauseParams(new Object[] { SchedulerStaffID });
//               staff.executeQuery();
//             this.setCopyId(SchedulerStaffID);
//            AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();
//          this.setCopyValue(r.getEmployeeName());
//          this.setCopyPersonId(r.getPersonId());
//          this.setCopyWkId(r.getWorkGroupId());
//         
//        }
    public void getCurrrentSchedulerStaffRowCopyNewsResource(Number SchedulerStaffID)
    {
            Number Scstaffid=new Number();
               SchedulerAMImpl sc=this.getApplicationModule();
               
               AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
               staff.setWhereClauseParams(null);
               staff.setWhereClause(null);
               staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
               staff.setWhereClauseParams(new Object[] { SchedulerStaffID });
               staff.executeQuery();
             this.setCopyId(SchedulerStaffID);
            AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();
          this.setCopyValue(r.getShiftId().toString());
            this.setCopyShiftId(r.getShiftId());
     
        }

    public String IsValid(java.util.Date date) {
        // TODO Implement this method
        int result=0;
        
        OperationBinding oper = ADFUtils.findOperation("getStartEndScheduleDates");
        oper.execute();
        HashMap SchedulerDuration =(HashMap)oper.getResult();
        oracle.jbo.domain.Date schStartdate=(oracle.jbo.domain.Date)SchedulerDuration.get("SchedulerStartDate");
        oracle.jbo.domain.Date schEnddate=(oracle.jbo.domain.Date)SchedulerDuration.get("SchedulerEndDate");
       
        
        if(this.addDays(date,1).after(new java.util.Date(schStartdate.dateValue().getTime()))&& this.addDays(date,-1).before(new java.util.Date(schEnddate.dateValue().getTime())))
        {
                DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                 String StartDate=df.format(date);
                 OperationBinding oper1 = ADFUtils.findOperation("validateSchedulerDay");
                oper1.getParamsMap().put("p_Date",StartDate);
                oper1.execute();
                result=  Integer.valueOf(oper1.getResult().toString());
              //  String HeaderMessage=String.valueOf(result)+" " +"Shifts invalid";
              
                  if(result>0)
                  {
                 return "N";
                  }
                  else 
                  return "Y";
                
          
          
            }
      else
     
        return "UN";
        


        
     
    
      
      
       
    }
    //  
        /////--------End Context Menu Section ---------------//
    //------------------ News Room Schedule ------------------//
  String ScheduleShiftCurrentId ;
  String ShiftCurrentId ;
  String ShiftCurrentName ;
  String WorkgroupCurrentId ;
  /// News Room Schedule ////
//  public void rowDisclosureListener(RowDisclosureEvent rowDisclosureEvent) {
//      
//      
//  this.bindEmployeesShiftsNewsRoom();
//      this.bindEmployeesShiftsNewsRoom();
////      FacesMessage Message = new FacesMessage("Test1");   
////                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
////                    FacesContext fc = FacesContext.getCurrentInstance();
////                    fc.addMessage(null, Message);
//      RichTreeTable table=this.getTt3();
//               AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
//       adffctx.addPartialTarget(table);
//      
//  }
    public String createShiftAttributeRow()
    {

            OperationBinding oper1 = ADFUtils.findOperation("createShiftAttributeRow");
            oper1.getParamsMap().put("SchedulerShiftId",ScheduleShiftCurrentId);
            oper1.getParamsMap().put("ShiftId",ShiftCurrentId);
            oper1.getParamsMap().put("ShiftName",ShiftCurrentName);
            oper1.getParamsMap().put("wkId",WorkgroupCurrentId);
            oper1.execute();
            // Fix issue of loase binded data after add new row as the view objects exceuted 
           // bindEmployeesShiftsNewsRoom();
//                FacesMessage Message = new FacesMessage(ScheduleShiftCurrentId);   
//              Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//              FacesContext fc = FacesContext.getCurrentInstance();   
//              fc.addMessage(null, Message); 
           return null;
        }
    public String SearchScheduleNewRoom_action() {
        this.setShowhidetable(true);
       exceuteShiftattribute();
     
             
                return null;
    }
    public String SearchScheduleNewRoombutton_action() {
        this.setShowhidetable(true);
       exceuteShiftattribute();
        SchedulerAMImpl sc=this.getApplicationModule();
        sc.getAsSchedulerShiftsVO().executeQuery();
        RichTreeTable table = this.getTt3();
        AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
        adffctx.addPartialTarget(table);
             
                return null;
    }
    public String NextScheduleNewRoombutton_action() {
    
        this.setShowhidetable(true);
        this.setWeekStartDate(this.addDays(this.getWeekStartDate(), 7));
        exceuteShiftattribute();
         SchedulerAMImpl sc=this.getApplicationModule();
         sc.getAsSchedulerShiftsVO().executeQuery();
         RichTreeTable table = this.getTt3();
         AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
         adffctx.addPartialTarget(table);
       
                return null;
    }
    
    
    public String PreviousScheduleNewRoombutton_action() {
    
        this.setShowhidetable(true);
        this.setWeekStartDate(this.addDays(this.getWeekStartDate(), -7));
        exceuteShiftattribute();
         SchedulerAMImpl sc=this.getApplicationModule();
         sc.getAsSchedulerShiftsVO().executeQuery();
         RichTreeTable table = this.getTt3();
         AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
         adffctx.addPartialTarget(table);
       
                return null;
    }
    
    
    
    public void exceuteShiftattribute()
    {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
             String StartDate=df.format(this.getWeekStartDate());
            JSFUtils.storeOnSession("ShiftAttributeStartDate", StartDate);

        
        
        }
    public String getShiftDayNewsRoom(Number SchdeulerId ,Number SchdlrShiftAttributeId,java.util.Date day)
    {
            SchedulerAMImpl sc=this.getApplicationModule();
            
        AsSchedulerStaffShiftTableVOImpl vo=sc.getAsSchedulerStaffShiftTableVO();
            vo.setWhereClauseParams(null);
            vo.setWhereClause(null);
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
           // and schedular_start_date=:2
            vo.setWhereClause("FK_SCHDLR_SHIFT_ATTRIBUTE_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
            vo.setWhereClauseParams(new Object[] {SchdlrShiftAttributeId,df.format(day),SchdeulerId});
            vo.executeQuery();
            if (vo.first() != null) {
           
                AsSchedulerStaffShiftTableVORowImpl r=(AsSchedulerStaffShiftTableVORowImpl)vo.first();
                 return r.getEmployeeName();
            }
            //000000000000000
        return " ";
        }
    public String getShiftColorNewsRoom(Number SchdeulerId ,Number SchdlrShiftAttributeId,java.util.Date day)
    {
        SchedulerAMImpl sc=this.getApplicationModule();
        String color="transparent";
    AsSchedulerStaffShiftTableVOImpl vo=sc.getAsSchedulerStaffShiftTableVO();
        vo.setWhereClauseParams(null);
        vo.setWhereClause(null);
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        vo.setWhereClause("FK_SCHDLR_SHIFT_ATTRIBUTE_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
        vo.setWhereClauseParams(new Object[] { SchdlrShiftAttributeId, df.format(day) ,SchdeulerId});
        vo.executeQuery();
        if (vo.first() != null) {
          
            AsSchedulerStaffShiftTableVORowImpl r=(AsSchedulerStaffShiftTableVORowImpl)vo.first();
           
             color=r.getStatusColor();
           
                 //r.getStatusColor();
        }
    return color;
    }
    public void EditScheduleNewsRoom(ClientEvent ce){
       
        this.setSaveUpdate_text("Update");
        this.setSaveUpdateNotify_text("Update & Notify");
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTreeTable table = this.getTt3();
        Object oldKey = table.getRowKey();
        
       try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            Number[]currentrow=  getGurrentShiftAttbiuteRowData();
            Number vFkShiftAttributeId=currentrow[0];

            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
          schedulerStaffId=this.getCurrentSchedulerStaffIdNewsRoom(vFkShiftAttributeId, Schedulesdate);
  
            this.getCurrrentSchedulerStaffRow(schedulerStaffId);
           // added by Heba at 21-4-2019
           
           Row asSchedulersRow = ADFUtils.findIterator("AsSchedulerStaffVOIterator").getCurrentRow();
            currentPersonId=new Number(0);
             currentPersonId=  (Number)ADFUtils.getBoundAttributeValue("PersonId");

           
           // end Added by Heba at 21-4-2019
          // this.showSchdlrPopup();
          RichPopup.PopupHints hints = new RichPopup.PopupHints();
          this.getSchlrPopNewsRoom().show(hints);
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
           this.hideEditNewsRoomPopup();
        }  
        
        
        
        
       
       
       
     
     
    }
    public String viewenter_Schedule()
    {
            String  SchedulerType= "";
          
                Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
      
               
                  SchedulerType=asSchedulersRow.getAttribute("SchedulerType").toString();
//            FacesMessage Message = new FacesMessage(SchedulerType);   
//                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                  FacesContext fc = FacesContext.getCurrentInstance();   
//                  fc.addMessage(null, Message); 
           if(SchedulerType.equals("Layout_A"))
           return "LayoutA";
               else  if(SchedulerType.equals("Layout_B"))
               return  "LayoutB";
           
           else return "LayoutC";
                    //"SchedulerEntryPresenter";
      // return null;
        
        }
    public String cancelSchdeuleEntryNews()
    {
        
        this.setShowhidetable(false);
            ADFUtils.findOperation("Rollback").execute();
        return "gotoSchedulerHome";
        }
   
    public void onClickCancelSchdlrPopNewsRoomBtn(ActionEvent actionEvent) {
        // Add event code here...
        if(this.getSaveUpdate_text().equals("Save"))
        {
        ADFUtils.findOperation("DeleteSchdlrStaff").execute();
            ADFUtils.findOperation("Commit").execute();
            this.getSchlrPopNewsRoom().hide();
        }
            else if(this.getSaveUpdate_text().equals("Update"))
            {
            

        BindingContext bc=BindingContext.getCurrent();  
       BindingContainer binding=bc.getCurrentBindingsEntry();  
        DCBindingContainer bindingsImpl = (DCBindingContainer) binding;  
        DCIteratorBinding dciter = bindingsImpl.findIteratorBinding("AsSchedulerStaffVOIterator");  
        ViewObject vo=dciter.getViewObject();  
        
        Row row=vo.getCurrentRow();  
        if(row!=null)  
        {  
            row.refresh(Row.REFRESH_UNDO_CHANGES|Row.REFRESH_WITH_DB_FORGET_CHANGES);  
              
            }  
        vo.executeQuery();  
        
                       this.getSchlrPopNewsRoom().hide();
      
            }
      
    }
    
    
    
    
    public void onClickSaveSchdlrPopNewsRoomBtn(ActionEvent actionEvent) {
        // Add event code here...
    
    String Leaves =    check_employee_replace_leave();
        if(!Leaves.equals("No Leaves")) {
         
       FacesMessage Message = new FacesMessage(Leaves);
       Message.setSeverity(FacesMessage.SEVERITY_WARN);
      FacesContext fc = FacesContext.getCurrentInstance();
      fc.addMessage(null, Message);
      // tracking History
      if(("Save").equals(this.SaveUpdate_text))
      {TrackHistory(new Number(0),"Add","N");}
            if(("Update").equals(this.SaveUpdate_text))
            {TrackHistory(new Number(0),"Update","N");}
            ADFUtils.findOperation("Commit").execute();
            SchedulerAMImpl sc=this.getApplicationModule();
            sc.getAsSchedulerShiftsVO().executeQuery();
            
            RichTreeTable table = this.getTt3();
            AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
            adffctx.addPartialTarget(table);
            this.getSchlrPopNewsRoom().hide();
            
        }
        
      else
       {
            
           // tracking History
           if(("Save").equals(this.SaveUpdate_text))
           {TrackHistory(new Number(0),"Add","N");}
                 if(("Update").equals(this.SaveUpdate_text))
                 {TrackHistory(new Number(0),"Update","N");}
         ADFUtils.findOperation("Commit").execute();
        SchedulerAMImpl sc=this.getApplicationModule();
        sc.getAsSchedulerShiftsVO().executeQuery();
      
      RichTreeTable table = this.getTt3();
      AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
      adffctx.addPartialTarget(table);
      this.getSchlrPopNewsRoom().hide();
       }
    }
    public void onClickSaveandNotifySchdlrPopNewsRoomBtn(ActionEvent actionEvent) {
        // Add event code here...
    
        String Leaves =    check_employee_replace_leave();
            if(!Leaves.equals("No Leaves")) {
             
           FacesMessage Message = new FacesMessage(Leaves);
           Message.setSeverity(FacesMessage.SEVERITY_WARN);
          FacesContext fc = FacesContext.getCurrentInstance();
          fc.addMessage(null, Message);
                // tracking History
                if(("Save & Notify").equals(this.SaveUpdateNotify_text))
                {TrackHistory(new Number(0),"Add & Notify","N");}
                      if(("Update & Notify").equals(this.SaveUpdateNotify_text))
                      {TrackHistory(new Number(0),"Update & Notify","N");}
                      // End Tracking History
                ADFUtils.findOperation("Commit").execute();
                /// After commit need to send notification to the user (Email-Sms)
                sendUpdateNotification();
                sendUpdateSmsNotification();
                SchedulerAMImpl sc=this.getApplicationModule();
                sc.getAsSchedulerShiftsVO().executeQuery();
                   // Refresh Tree Table//
                RichTreeTable table = this.getTt3();
                AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
                adffctx.addPartialTarget(table);
                this.getSchlrPopNewsRoom().hide();
            }
            else 
            {
     
                if(("Save & Notify").equals(this.SaveUpdateNotify_text))
                {TrackHistory(new Number(0),"Add & Notify","N");}
                      if(("Update & Notify").equals(this.SaveUpdateNotify_text))
                      {TrackHistory(new Number(0),"Update & Notify","N");}
        ADFUtils.findOperation("Commit").execute();
        /// After commit need to send notification to the user
        sendUpdateNotification();
       sendUpdateSmsNotification();
        SchedulerAMImpl sc=this.getApplicationModule();
        sc.getAsSchedulerShiftsVO().executeQuery();
           // Refresh Tree Table//
      RichTreeTable table = this.getTt3();
      AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
      adffctx.addPartialTarget(table);
       this.getSchlrPopNewsRoom().hide();
            }
    }
    
    public void onClickSendOutLook(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("Commit");
        Number Schedulerd= (oracle.jbo.domain.Number)ADFUtils.getBoundAttributeValue("SchedulerId");
        Number SchedulerStaffId= (oracle.jbo.domain.Number)ADFUtils.getBoundAttributeValue("SchedulerStaffId");
                  OperationBinding oper = ADFUtils.findOperation("sendOutLookcalendar");
                    oper.getParamsMap().put("schedulerStaffId", SchedulerStaffId);
           oper.getParamsMap().put("schedulerId", Schedulerd);
               String result=     oper.execute().toString(); 
        if(result.equals("0"))
           {
            
            
            FacesMessage Message = new FacesMessage("Calendar Event Sent Successfully .....");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
           
           
           
           
            }
        
        else 
        {
            
                FacesMessage Message = new FacesMessage("Calendar Event Sending  Failed .....");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            
            
            }
        
        
        
     
//        ADFUtils.findOperation("Commit").execute();
//        /// After commit need to send notification to the user
//        sendUpdateNotification();
//        SchedulerAMImpl sc=this.getApplicationModule();
//        sc.getAsSchedulerShiftsVO().executeQuery();
//           // Refresh Tree Table//
//      RichTreeTable table = this.getTt3();
//      AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
//      adffctx.addPartialTarget(table);
 //        this.getSchlrPopNewsRoom().hide();
    }
    
    
    public void sendUpdateNotification()
    {
        
        
       Number SchedulerStaffId= (oracle.jbo.domain.Number)ADFUtils.getBoundAttributeValue("SchedulerStaffId");
            OperationBinding oper = ADFUtils.findOperation("sendUpdateNotification");
              oper.getParamsMap().put("schedulerStaffId", SchedulerStaffId);
              oper.execute(); 
        }
    
    public void sendUpdateSmsNotification()
    {
        
           Number PersonId= (oracle.jbo.domain.Number)ADFUtils.getBoundAttributeValue("PersonId");
           Object ReplaceId=(oracle.jbo.domain.Number)ADFUtils.getBoundAttributeValue("FkReplacedById");
           Number ReplacemntId=new Number(0);
           if (ReplaceId!=null)
           {ReplacemntId=(oracle.jbo.domain.Number)ReplaceId;}
            OperationBinding oper = ADFUtils.findOperation("GetEmployeeContanctInfo");
                //("GetEmployeePhoneNumber");
              oper.getParamsMap().put("PersonId", PersonId.toString());
            oper.getParamsMap().put("ReplaceId", ReplacemntId.toString());
              oper.execute(); 
            //              //

            String[]empphones=new String[2] ;
            if (oper.getErrors().isEmpty())
            {
                
              
            empphones = (String[]) oper.getResult();
                 System.out.println("phone1 :"+empphones[0]);
                 System.out.println("phone2 :"+empphones[1]);
                 System.out.println("personid :"+PersonId.toString());
                System.out.println("ReplaceId :"+ReplacemntId.toString());
          String empphonenumber= empphones[0];
         String replacephonenumber= empphones[1];
             
            Object smsnotfication= ADFUtils.getBoundAttributeValue("SmsNotifiction");
      
            String resultemp="";
            String resultreplace="";
     
            if(smsnotfication !=null)
            {
                    if(oper.getResult().toString()!="0")
                   // {
                    System.out.println("Sms Notification :"+smsnotfication.toString());
                        empphonenumber=oper.getResult().toString();
                if(empphones[0]!=null)
                {
                        System.out.println("personid :"+PersonId.toString());
                        System.out.println("Phone number :"+empphones[0]);
              resultemp=   this.sendSMSAction(empphones[0],smsnotfication.toString()); 
                      insertsmsstatus(PersonId,empphones[0],resultemp) ;
                    }
                if(replacephonenumber!=null)
                {
                        System.out.println("Replace :"+replacephonenumber);
                  resultreplace=this.sendSMSAction(replacephonenumber,smsnotfication.toString());  
                 insertsmsstatus(ReplacemntId,replacephonenumber,resultreplace) ;
                    }
                
                  //  }
                }
//            else 
//            {
//                // sms text is empty 
//                
//                }
            
            
            }
        }

    
    //---- Context Menu Section NewsRoom -----
         
    public void AddSchedulerStaffNewsroom(ClientEvent ce){
        
        this.setSaveUpdate_text("Save");
        this.setSaveUpdateNotify_text("Save & Notify");
      java.util.Date columnDate = (java.util.Date)ce.getParameters().get("column"); 
       RichTreeTable table = this.getTt3();
        Object oldKey = table.getRowKey();
        currentPersonId=new Number(0);
        
       try {
            int rowKeyIndex =0;
            // ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
        AddSelectedTableRowsNewsRoom(columnDate, rowKeyIndex);  
        
      }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
           this.hideAddNewsRoomPopup();
        }  
    }
    

    private void AddSelectedTableRowsNewsRoom(java.util.Date columndate, int rowKeyIndex){
    
       RichTreeTable table = this.getTt3();

     Number[]currentrow=  getGurrentShiftAttbiuteRowData();
         ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
         //to get startdate  which is column passed in client listener
         ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
         ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
         ADFUtils.setBoundAttributeValue("FkSchdlrShiftAttributeId",currentrow[0]);
        ADFUtils.setBoundAttributeValue("ShiftId",currentrow[1]);
       ADFUtils.setBoundAttributeValue("WorkGroupId",currentrow[2]);
        ADFUtils.setBoundAttributeValue("Shiftworkgroup",this.getCurrentSelectedShiftWorkgroupName(currentrow[1],currentrow[2])[0]);
      
       
      //System.out.println(currentrow[2]);
     
         RichPopup.PopupHints hints = new RichPopup.PopupHints();
         this.getSchlrPopNewsRoom().show(hints);
         //this.showSchdlrPopup();

         AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
       adffctx.addPartialTarget(table);
     }
    private void PasteSelectedTableRowsNewsRoom(java.util.Date columndate, int rowKeyIndex,String CutCopy){
  
  

  
    if(CutCopy.equals("Copy"))
    {
    if(CopySchedulerStaffIdNewsRoom.intValue()==0)
    {
        
                    FacesMessage Message2 = new FacesMessage("No Employee Copied ");   
                          Message2.setSeverity(FacesMessage.SEVERITY_WARN);   
                          FacesContext fc2 = FacesContext.getCurrentInstance();   
                          fc2.addMessage(null, Message2);  
        
        }
    else
    {
        
        RichTreeTable table = this.getTt3();

          Number[]currentrow=  getGurrentShiftAttbiuteRowData();
       
        
        SchedulerAMImpl sc=this.getApplicationModule();
         AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
        staff.setWhereClauseParams(null);
        staff.setWhereClause(null);
        staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
        staff.setWhereClauseParams(new Object[] { CopySchedulerStaffIdNewsRoom });
        staff.executeQuery();
        AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();
        
        String has_leave=check_employee_replace_leave_db(r.getPersonId(),new Number(0),DateConversion.getJboDateFromUtilDate(columndate));
      Object   fkreplacedbyId=r.getFkReplacedById();
        Object fkwkreplacedbyid=r.getFkWkReplacedId();
        Object   replacetype=r.getReplaceType();
        if(r.getWorkGroupId().compareTo(currentrow[2])==0)
        {
           
           if(has_leave.equals("No Leaves"))
         {  


        try
        {
        
         ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
         //to get startdate  which is column passed in client listener
         ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
         ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
         ADFUtils.setBoundAttributeValue("FkSchdlrShiftAttributeId",currentrow[0]);
        ADFUtils.setBoundAttributeValue("ShiftId",currentrow[1]);
        if(fkreplacedbyId!=null)
        { ADFUtils.setBoundAttributeValue("FkReplacedById" ,(Number) fkreplacedbyId);}
            if(replacetype!=null)
            { ADFUtils.setBoundAttributeValue("ReplaceType" ,(String) replacetype);}
            if(fkwkreplacedbyid!=null)
           { ADFUtils.setBoundAttributeValue("FkWkReplacedId" ,(Number) fkwkreplacedbyid);}
         ADFUtils.setBoundAttributeValue("PersonId",r.getPersonId());
         ADFUtils.setBoundAttributeValue("WorkGroupId",r.getWorkGroupId());
            ADFUtils.setBoundAttributeValue("Notes",r.getNotes());
         ADFUtils.setBoundAttributeValue("RecurringFlag","N");
         ADFUtils.setBoundAttributeValue("BulletinFlag","N");
            ADFUtils.setBoundAttributeValue("StartTimeHours",r.getStartTimeHours());
            ADFUtils.setBoundAttributeValue("StartTimeMinutes",r.getStartTimeMinutes());
            ADFUtils.setBoundAttributeValue("EndTimeHours",r.getEndTimeHours());
            ADFUtils.setBoundAttributeValue("EndTimeMinutes",r.getEndTimeMinutes());
            ADFUtils.setBoundAttributeValue("EmailNotification",r.getEmailNotification());
            ADFUtils.setBoundAttributeValue("SmsNotifiction",r.getSmsNotifiction());
        }
        catch (NullPointerException ex)
         { 
                ADFUtils.setBoundAttributeValue("Notes",r.getNotes());
            
            }
                                        
         ADFUtils.findOperation("Commit").execute(); 
     ///// Copy ofBulletins -----
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String Schedulesdate=df.format(columndate);
       Number toSchedulerStaffIdNewsRoom=this.getCurrentSchedulerStaffIdNewsRoom(currentrow[0], Schedulesdate);
         CopyPasteBulletins(CopySchedulerStaffIdNewsRoom,toSchedulerStaffIdNewsRoom);
                 
            
                 /////--------End Copy of Bulletins ----
         
         // Tracking history ///
        
         this.pasteTrackingSchedulerStaff(r, columndate, currentrow[1], currentrow[0],toSchedulerStaffIdNewsRoom,this.getLoggedPersonId());
         
         //// End Tracking History ////
            
                 ADFUtils.findOperation("Commit").execute(); 
          sc.getAsSchedulerShiftsVO().executeQuery();
        AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
       adffctx.addPartialTarget(table);
       }
   else 
       {
               FacesMessage Message = new FacesMessage(has_leave);
               Message.setSeverity(FacesMessage.SEVERITY_WARN);
               FacesContext fc = FacesContext.getCurrentInstance();
               fc.addMessage(null, Message); 
               
               try
               {
               
                ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
                //to get startdate  which is column passed in client listener
                ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
                ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
                ADFUtils.setBoundAttributeValue("FkSchdlrShiftAttributeId",currentrow[0]);
               ADFUtils.setBoundAttributeValue("ShiftId",currentrow[1]);
                   if(fkreplacedbyId!=null)
                   { ADFUtils.setBoundAttributeValue("FkReplacedById" ,(Number) fkreplacedbyId);}
                   if(replacetype!=null)
                   { ADFUtils.setBoundAttributeValue("ReplaceType" ,(String) replacetype);}
                   if(fkwkreplacedbyid!=null)
                   { ADFUtils.setBoundAttributeValue("FkWkReplacedId" ,(Number) fkwkreplacedbyid);}
                ADFUtils.setBoundAttributeValue("PersonId",r.getPersonId());
                ADFUtils.setBoundAttributeValue("WorkGroupId",r.getWorkGroupId());
                   ADFUtils.setBoundAttributeValue("Notes",r.getNotes());
                ADFUtils.setBoundAttributeValue("RecurringFlag","N");
                ADFUtils.setBoundAttributeValue("BulletinFlag","N");
                   ADFUtils.setBoundAttributeValue("StartTimeHours",r.getStartTimeHours());
                   ADFUtils.setBoundAttributeValue("StartTimeMinutes",r.getStartTimeMinutes());
                   ADFUtils.setBoundAttributeValue("EndTimeHours",r.getEndTimeHours());
                   ADFUtils.setBoundAttributeValue("EndTimeMinutes",r.getEndTimeMinutes());
                   ADFUtils.setBoundAttributeValue("EmailNotification",r.getEmailNotification());
                   ADFUtils.setBoundAttributeValue("SmsNotifiction",r.getSmsNotifiction());
               }
               catch (NullPointerException ex)
                { 
                       ADFUtils.setBoundAttributeValue("Notes",r.getNotes());
                   
                   }
                                               
                ADFUtils.findOperation("Commit").execute(); 
               ///// Copy ofBulletins -----
               DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
               String Schedulesdate=df.format(columndate);
               Number toSchedulerStaffIdNewsRoom=this.getCurrentSchedulerStaffIdNewsRoom(currentrow[0], Schedulesdate);
                CopyPasteBulletins(CopySchedulerStaffIdNewsRoom,toSchedulerStaffIdNewsRoom);
                        
                   
                        /////--------End Copy of Bulletins ----
               ////Tracking History/////
                   
                        this.pasteTrackingSchedulerStaff(r, columndate, currentrow[1], currentrow[0],toSchedulerStaffIdNewsRoom,this.getLoggedPersonId());
               /// End Tracking History ////
                        ADFUtils.findOperation("Commit").execute(); 
                 sc.getAsSchedulerShiftsVO().executeQuery();
               AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
               adffctx.addPartialTarget(table);
               
           }
 
        } // if workgroup match 
        else 
        {
                /// validate same work group of cut/paste  or copy /paste 
                     ///get paste workgroup id 
                
                FacesMessage Message = new FacesMessage("Copied Workgroup Doesn't Match Paste Workgroup");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            
            
            
            
            }
    } // if copy value exist
    
    } // if copy 
    
    
    
    
    else     if(CutCopy.equals("Cut"))
    {
    if(CutSchedulerStaffIdNewsRoom.intValue()==0)
    {
        
                    FacesMessage Message2 = new FacesMessage("No Employee Cut ");   
                          Message2.setSeverity(FacesMessage.SEVERITY_WARN);   
                          FacesContext fc2 = FacesContext.getCurrentInstance();   
                          fc2.addMessage(null, Message2);  
        
        }
    else
    {
       RichTreeTable table = this.getTt3();
         Number[]currentrow=  getGurrentShiftAttbiuteRowData();
        SchedulerAMImpl sc=this.getApplicationModule();
        Number prid=(Number)CutAList.get(0);
       Number cutwk=(Number)CutAList.get(1);
        String has_leave=check_employee_replace_leave_db(prid,new Number(0),DateConversion.getJboDateFromUtilDate(columndate));
        if(cutwk.compareTo(currentrow[2])==0)
        {
        
            if(has_leave.equals("No Leaves"))
            {
        try
        {
        
         ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
         //to get startdate  which is column passed in client listener
         ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
         ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
         ADFUtils.setBoundAttributeValue("FkSchdlrShiftAttributeId",currentrow[0]);
        ADFUtils.setBoundAttributeValue("ShiftId",currentrow[1]);
        
         ADFUtils.setBoundAttributeValue("PersonId",CutAList.get(0));
         ADFUtils.setBoundAttributeValue("WorkGroupId",CutAList.get(1));
            ADFUtils.setBoundAttributeValue("Notes",CutAList.get(2));
            if(CutAList.get(3)!="0")
            { ADFUtils.setBoundAttributeValue("FkReplacedById",CutAList.get(3));} 
            if(CutAList.get(4)!="0")
            { ADFUtils.setBoundAttributeValue("ReplaceType",CutAList.get(4));} 
            if(CutAList.get(5)!="0")
            { ADFUtils.setBoundAttributeValue("FkWkReplacedId" ,CutAList.get(5));}
         ADFUtils.setBoundAttributeValue("RecurringFlag","N");
         ADFUtils.setBoundAttributeValue("BulletinFlag","N");
            ADFUtils.setBoundAttributeValue("StartTimeHours",CutAList.get(6));
            ADFUtils.setBoundAttributeValue("StartTimeMinutes",CutAList.get(7));
            ADFUtils.setBoundAttributeValue("EndTimeHours",CutAList.get(8));
            ADFUtils.setBoundAttributeValue("EndTimeMinutes",CutAList.get(9));
            ADFUtils.setBoundAttributeValue("EmailNotification",CutAList.get(10));
            ADFUtils.setBoundAttributeValue("SmsNotifiction",CutAList.get(11));
        }
        catch (NullPointerException ex)
         { 
              //  ADFUtils.setBoundAttributeValue("Notes",r.getNotes());
            
            }
                                        
         ADFUtils.findOperation("Commit").execute(); 
        
        ///// Paste ofBulletins -----
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String Schedulesdate=df.format(columndate);
       Number toSchedulerStaffIdNewsRoom=this.getCurrentSchedulerStaffIdNewsRoom(currentrow[0], Schedulesdate);
      CutPasteBulletins(toSchedulerStaffIdNewsRoom);
            // Tracking history ///
            AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
            staff.setWhereClauseParams(null);
            staff.setWhereClause(null);
            staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
            staff.setWhereClauseParams(new Object[] { toSchedulerStaffIdNewsRoom});
            staff.executeQuery();
            AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();
            this.pasteTrackingCutSchedulerStaff(r,this.getLoggedPersonId());
            
            //// End Tracking History ////
           ADFUtils.findOperation("Commit").execute(); 
          sc.getAsSchedulerShiftsVO().executeQuery();
        AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
       adffctx.addPartialTarget(table);
  
        }
        else 
            {
                    FacesMessage Message = new FacesMessage(has_leave);
                    Message.setSeverity(FacesMessage.SEVERITY_WARN);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message); 
                
                    try
                    {
                    
                     ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
                     //to get startdate  which is column passed in client listener
                     ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
                     ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
                     ADFUtils.setBoundAttributeValue("FkSchdlrShiftAttributeId",currentrow[0]);
                    ADFUtils.setBoundAttributeValue("ShiftId",currentrow[1]);
                    
                     ADFUtils.setBoundAttributeValue("PersonId",CutAList.get(0));
                     ADFUtils.setBoundAttributeValue("WorkGroupId",CutAList.get(1));
                        ADFUtils.setBoundAttributeValue("Notes",CutAList.get(2));
                        if(CutAList.get(3)!="0")
                        { ADFUtils.setBoundAttributeValue("FkReplacedById",CutAList.get(3));}
                        if(CutAList.get(4)!="0")
                        { ADFUtils.setBoundAttributeValue("ReplaceType",CutAList.get(4));} 
                        if(CutAList.get(5)!="0")
                        { ADFUtils.setBoundAttributeValue("FkWkReplacedId" ,CutAList.get(5));}
                     ADFUtils.setBoundAttributeValue("RecurringFlag","N");
                     ADFUtils.setBoundAttributeValue("BulletinFlag","N");
                        ADFUtils.setBoundAttributeValue("StartTimeHours",CutAList.get(6));
                        ADFUtils.setBoundAttributeValue("StartTimeMinutes",CutAList.get(7));
                        ADFUtils.setBoundAttributeValue("EndTimeHours",CutAList.get(8));
                        ADFUtils.setBoundAttributeValue("EndTimeMinutes",CutAList.get(9));
                        ADFUtils.setBoundAttributeValue("EmailNotification",CutAList.get(10));
                        ADFUtils.setBoundAttributeValue("SmsNotifiction",CutAList.get(11));
                    }
                    catch (NullPointerException ex)
                     { 
                          //  ADFUtils.setBoundAttributeValue("Notes",r.getNotes());
                        
                        }
                                                    
                     ADFUtils.findOperation("Commit").execute(); 
                    
                    ///// Paste ofBulletins -----
                    DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                    String Schedulesdate=df.format(columndate);
                    Number toSchedulerStaffIdNewsRoom=this.getCurrentSchedulerStaffIdNewsRoom(currentrow[0], Schedulesdate);
                    CutPasteBulletins(toSchedulerStaffIdNewsRoom);
                
                
                    // Tracking history ///
                    AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
                    staff.setWhereClauseParams(null);
                    staff.setWhereClause(null);
                    staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
                    staff.setWhereClauseParams(new Object[] { toSchedulerStaffIdNewsRoom});
                    staff.executeQuery();
                    AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();
                    this.pasteTrackingCutSchedulerStaff(r,this.getLoggedPersonId());
                    //// End Tracking History ////
                       ADFUtils.findOperation("Commit").execute(); 
                      sc.getAsSchedulerShiftsVO().executeQuery();
                    AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
                    adffctx.addPartialTarget(table);
                
                }  ///validate leaves
  
  
  
    }  //validate workgroup
        
    else 
    {
            /// validate same work group of cut/paste  or copy /paste 
                 ///get paste workgroup id 
            
            FacesMessage Message = new FacesMessage("Cut Workgroup Doesn't Match Paste Workgroup");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        
        
        
        
        }
    } //cut value exist close 
    } // cut close 
    
    } // method close
    public Number[] getGurrentShiftAttbiuteRowData()
    {
        
      Number[] shiftattr=new Number[3] ;
            RichTreeTable treeTable = this.getTt3();
                //get all selected row keys
                RowKeySet rks = treeTable.getSelectedRowKeys();        
                Iterator keys = rks.iterator();
               if(rks.getSize()>0)
            {
                while(keys.hasNext()){
                
                   List key = (List) keys.next();
                  treeTable.setRowKey(key);
                 JUCtrlHierNodeBinding node = (JUCtrlHierNodeBinding) treeTable.getRowData();
                  Row rw = node.getRow();
                    Number ShiftattrId=(oracle.jbo.domain.Number)rw.getAttribute("ShiftAttributeId");
                    Number ShiftId=(oracle.jbo.domain.Number)rw.getAttribute("FkShiftId");
                    Number fkwkId=(oracle.jbo.domain.Number)rw.getAttribute("FkWorkGroupId");
                    
                    
                    shiftattr[0]=ShiftattrId;
                    shiftattr[1]=ShiftId;
                    shiftattr[2]=fkwkId;
              
                }
            }
      
   
      return shiftattr;
        
        }
    
    public Number[] getCurrentPersonRowData()
    {
        
      Number[] personattr=new Number[2] ;
            RichTable Table = this.getT3();
                //get all selected row keys
                RowKeySet rks = Table.getSelectedRowKeys();        
                Iterator keys = rks.iterator();
               if(rks.getSize()>0)
            {
                while(keys.hasNext()){
                
                   List key = (List) keys.next();
                  Table.setRowKey(key);
                 JUCtrlHierNodeBinding node = (JUCtrlHierNodeBinding) Table.getRowData();
                  Row rw = node.getRow();
                    Number personId=(oracle.jbo.domain.Number)rw.getAttribute("PersonId");
                    Number wkId=(oracle.jbo.domain.Number)rw.getAttribute("WorkGroupId");
                    personattr[0]=personId;
                    personattr[1]=wkId;
              
                }
            }
      
    
      return personattr;
        
        }
    
    public void DeleteScheduleShifAttribute(ClientEvent ce){
       
    //        FacesMessage Message2 = new FacesMessage("Test");
    //              Message2.setSeverity(FacesMessage.SEVERITY_INFO);
    //              FacesContext fc2 = FacesContext.getCurrentInstance();
    //              fc2.addMessage(null, Message2);
               // Number schedulerStaffId = new Number();
        int id=       ((Double)ce.getParameters().get("column")).intValue();
         Number ShiftattributeId =  new oracle.jbo.domain.Number(id);    
         RichTreeTable table = this.getTt3();
         Object oldKey = table.getRowKey();
         
         try {
              int rowKeyIndex =
              ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
              table.setRowIndex(rowKeyIndex);
             
//                     FacesMessage Message2 = new FacesMessage(ShiftattributeId);
//                           Message2.setSeverity(FacesMessage.SEVERITY_INFO);
//                           FacesContext fc2 = FacesContext.getCurrentInstance();
//                           fc2.addMessage(null, Message2);

           OperationBinding oper = ADFUtils.findOperation("deleteShiftAttibute");
             oper.getParamsMap().put("ShiftAttributeId", ShiftattributeId);
             oper.execute();
             if(Integer.valueOf(oper.getResult().toString())==0)
             {
                 
          
                     
                         FacesMessage Message2 = new FacesMessage("Shift Section Deleted Successfully ");
                     Message2.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc2 = FacesContext.getCurrentInstance();
                     fc2.addMessage(null, Message2);
                      SchedulerAMImpl sc=this.getApplicationModule();
                     sc.getAsSchedulerShiftsVO().executeQuery();
                     // sc.getAsShiftAttributesVO2().executeQuery();
                      // Refresh Tree Table//
                         AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
                         adffctx.addPartialTarget(table);
                     
                     
                        
                  
                 
                 
                 
             
                 }
             else 
             {
                 
                                     FacesMessage Message2 = new FacesMessage(" Shift Sections have Assigned Employees ,Shift Section Can't be Deleted ");
                                                Message2.setSeverity(FacesMessage.SEVERITY_WARN);
                                                FacesContext fc2 = FacesContext.getCurrentInstance();
                                                fc2.addMessage(null, Message2); 
                 
                 }
      
     
         
         }
         catch(Exception e){
           e.printStackTrace();
             FacesMessage Message = new FacesMessage(e.getMessage());   
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                   FacesContext fc = FacesContext.getCurrentInstance();   
                   fc.addMessage(null, Message);  
         }
         finally {
            table.setRowKey(oldKey);
         }  
        
    }
    
    
    
    
    public void DeleteScheduleShiftNewsRoom(ClientEvent ce){
       
//        FacesMessage Message2 = new FacesMessage("Test");   
//              Message2.setSeverity(FacesMessage.SEVERITY_INFO);   
//              FacesContext fc2 = FacesContext.getCurrentInstance();   
//              fc2.addMessage(null, Message2);  
                Number schedulerStaffId = new Number();
         java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
         RichTreeTable table = this.getTt3();
         Object oldKey = table.getRowKey();
         
         try {
              int rowKeyIndex =
              ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
              table.setRowIndex(rowKeyIndex);
             Number[]currentrow=  getGurrentShiftAttbiuteRowData();
//             JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
//             AsSchdeulerStaffTableVORowImpl rowToCopyFrom = (AsSchdeulerStaffTableVORowImpl) rowBinding.getRow();
//             Number vPersonId = rowToCopyFrom.getPersonId();
             DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
             String Schedulesdate=df.format(columnDate);
             Number vFkShiftAttributeId=currentrow[0];
             schedulerStaffId=this.getCurrentSchedulerStaffIdNewsRoom(vFkShiftAttributeId, Schedulesdate);
          
             OperationBinding oper = ADFUtils.findOperation("deleteSchdlr");
             oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
             oper.getParamsMap().put("Type", "Delete");
             oper.getParamsMap().put("PersonID", this.getLoggedPersonId());
             oper.execute();
             SchedulerAMImpl sc=this.getApplicationModule();
             sc.getAsSchedulerShiftsVO().executeQuery();
            // sc.getAsShiftAttributesVO2().executeQuery();
             // Refresh Tree Table//
        
             AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
             adffctx.addPartialTarget(table);
         
         }
         catch(Exception e){
           e.printStackTrace();
             FacesMessage Message = new FacesMessage(e.getMessage());   
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                   FacesContext fc = FacesContext.getCurrentInstance();   
                   fc.addMessage(null, Message);  
         }
         finally {
            table.setRowKey(oldKey);
            this.hideEditNewsRoomPopup();
         }  
        
    }
     public void DeleteAllScheduleShiftNewsRoom(ClientEvent ce){
         Number schedulerStaffId = new Number();
         java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");  
         
         this.setDeleteDateStartA(columnDate);
              Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
                     
              java.util.Date enddate=DateConversion.getUtilDateFromJboDate((oracle.jbo.domain.Date)currwk.getAttribute("EndDate"));
              this.setDeleteDateEndA(enddate);
              this.setDeleteDateMaxA(enddate);
              RichPopup.PopupHints hints = new RichPopup.PopupHints();
              this.getDeleteA().show(hints);
         
         
         RichTreeTable table = this.getTt3();
         Object oldKey = table.getRowKey();
         int rowKeyIndex =((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
         this.hideEditNewsRoomPopup();
//         try {
//              int rowKeyIndex =
//              ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
//              table.setRowIndex(rowKeyIndex);
//
//                  Number[]currentrow=  getGurrentShiftAttbiuteRowData();
//            Number vFkShiftAttributeId=currentrow[0];
//             DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//             String Schedulesdate=df.format(columnDate);
//             schedulerStaffId=this.getCurrentSchedulerStaffIdNewsRoom(vFkShiftAttributeId, Schedulesdate);
//             OperationBinding oper = ADFUtils.findOperation("deleteAllSchdlrNewsRoom");
//             oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
//             oper.execute();
//             SchedulerAMImpl sc=this.getApplicationModule();
//             sc.getAsShiftAttributesVO2().executeQuery();
//             // Refresh Tree Table//
//             
//             AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
//             adffctx.addPartialTarget(table);
//           
//         
//         }
//         catch(Exception e){
//           e.printStackTrace();
//             FacesMessage Message = new FacesMessage(e.getMessage());   
//                   Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                   FacesContext fc = FacesContext.getCurrentInstance();   
//                   fc.addMessage(null, Message);  
//         }
//         finally {
//            table.setRowKey(oldKey);
//             this.hideEditNewsRoomPopup();
//         }  
        
        
        
        
        
          
     }
    public void DeleteRangeScheduleShiftNewsRoom(ActionEvent actionEvent){
        Number schedulerStaffId = new Number();
       // java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");  
        
       // this.setDeleteDateStartA(columnDate);
//             Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
//                    
//             java.util.Date enddate=DateConversion.getUtilDateFromJboDate((oracle.jbo.domain.Date)currwk.getAttribute("EndDate"));
//             this.setDeleteDateEndA(enddate);
//             this.setDeleteDateMaxA(enddate);
//             RichPopup.PopupHints hints = new RichPopup.PopupHints();
//             this.getDeleteA().show(hints);
//        
//        
      RichTreeTable table = this.getTt3();
//        Object oldKey = table.getRowKey();
//        int rowKeyIndex =((Double)ce.getParameters().get("rowKeyIndex")).intValue();
//            table.setRowIndex(rowKeyIndex);
      //  this.hideEditNewsRoomPopup();
         
      java.util.Date columnDate=this.getDeleteDateStartA();
       java.util.Date columnEndDate=this.getDeleteDateEndA();
    
                 Number[]currentrow=  getGurrentShiftAttbiuteRowData();
                Number vFkShiftAttributeId=currentrow[0];
                 DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                 String Schedulesdate=df.format(columnDate);
                 schedulerStaffId=this.getCurrentSchedulerStaffIdNewsRoom(vFkShiftAttributeId, Schedulesdate);
                 OperationBinding oper = ADFUtils.findOperation("deleteAllSchdlrNewsRoom");
      //  String repeatStatus = this.callRepeatPatternNewsRoom(schedulerStaffId,DateConversion.getJboDateFromUtilDate(columnDate),DateConversion.getJboDateFromUtilDate(columnEndDate),vfkshiftid,vFkShiftAttributeId);
                 oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
        oper.getParamsMap().put("schedulerdate", DateConversion.getJboDateFromUtilDate(columnDate));
        oper.getParamsMap().put("schedulerenddate",DateConversion.getJboDateFromUtilDate(columnEndDate));
                 oper.execute();
                 SchedulerAMImpl sc=this.getApplicationModule();
                 sc.getAsSchedulerShiftsVO().executeQuery();
                 //sc.getAsShiftAttributesVO2().executeQuery();
                 // Refresh Tree Table//
    
                 AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
                 adffctx.addPartialTarget(table);
                 this.getDeleteA().hide();
        this.hideEditNewsRoomPopup();
    
    
         
       
       
       
       
       
         
    }
       
     public void RepeatPAtternNewsRoom(ClientEvent ce){
        
         //Set Start and end date of Repeat 
         Number schedulerStaffId = new Number();
         java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");
         this.setRepeatDateStartA(columnDate);
         Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
                
         java.util.Date enddate=DateConversion.getUtilDateFromJboDate((oracle.jbo.domain.Date)currwk.getAttribute("EndDate"));
         this.setRepeatDateEndA(enddate);
         this.setRepeatDateMaxA(enddate);
         RichPopup.PopupHints hints = new RichPopup.PopupHints();
         this.getRepeatA().show(hints);
    
         
         
         RichTreeTable table = this.getTt3();
         Object oldKey = table.getRowKey();
          int rowKeyIndex = ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
               table.setRowIndex(rowKeyIndex);

         
       this.hideEditNewsRoomPopup();
     }
     
     
     public String PublishNewRoomScheduler()
     {
         
             Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
            // String workgroupid=currwk.getAttribute("WorkGroupId").toString();
         currwk.setAttribute("SchedularStatus", "PUBLISHED");
         ADFUtils.findOperation("Commit").execute();  
           
             sendemailPublish();
            sendemployeeSmsPublish();
             
FacesMessage Message = new FacesMessage("Schedule Published . Emails and SMS sent Successfully,Please Check SMS-Email monitoring for more details .");
                                                                               Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                                                               FacesContext fc = FacesContext.getCurrentInstance();
                                                                               fc.addMessage(null, Message);
                     
             return "";
         
         }
     
     
    public void CopyScheduleNewsRoom(ClientEvent ce){
       this.setCutCopyA("Copy");
       // this.setSaveUpdate_text("Update");
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTreeTable table = this.getTt3();
        Object oldKey = table.getRowKey();

        int rowKeyIndex = ((Double) ce.getParameters().get("rowKeyIndex")).intValue();
        table.setRowIndex(rowKeyIndex);
        Number[] currentrow = getGurrentShiftAttbiuteRowData();
        Number vFkShiftAttributeId = currentrow[0];

        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        String Schedulesdate = df.format(columnDate);
        CopySchedulerStaffIdNewsRoom = this.getCurrentSchedulerStaffIdNewsRoom(vFkShiftAttributeId, Schedulesdate);

        //  this.getCurrrentSchedulerStaffRowCopy(schedulerStaffId);
        //          FacesMessage Message = new FacesMessage(this.getCopyId()+"/"+this.getCopyValue());
        //               Message.setSeverity(FacesMessage.SEVERITY_INFO);
        //               FacesContext fc = FacesContext.getCurrentInstance();
        //               fc.addMessage(null, Message);
        table.setRowKey(oldKey);
        this.hideEditNewsRoomPopup();


    }
    public void PasteScheduleNewsRoom(ClientEvent ce){
    try
    {
        if(this.getCutCopyA().equals("Copy"))
        {
            java.util.Date columnDate = (java.util.Date)ce.getParameters().get("column"); 
       RichTreeTable table = this.getTt3();
        Object oldKey = table.getRowKey();
        
       try {
            int rowKeyIndex =((Double)ce.getParameters().get("rowKeyIndex")).intValue();
            // 
        PasteSelectedTableRowsNewsRoom(columnDate, rowKeyIndex,"Copy");  
        
      }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
           this.hideAddNewsRoomPopup();
        }  
     
        
        
        
        
                                                        
    }
       else         if(this.getCutCopyA().equals("Cut"))
        {
            java.util.Date columnDate = (java.util.Date)ce.getParameters().get("column"); 
       RichTreeTable table = this.getTt3();
        Object oldKey = table.getRowKey();
        
       try {
            int rowKeyIndex =((Double)ce.getParameters().get("rowKeyIndex")).intValue();
            // 
        PasteSelectedTableRowsNewsRoom(columnDate, rowKeyIndex,"Cut");  
        
      }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
           this.hideAddNewsRoomPopup();
        }  
     
        
        
        
        
                                                        
    } 
    }
    catch (NullPointerException ex)
    {
        
          
              FacesMessage Message = new FacesMessage("No Data to Paste");   
                    Message.setSeverity(FacesMessage.SEVERITY_WARN);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
        
        
        }
    }
    /////------- End Context Menu Section News Room -----//
 
    /////------ End News Room Schedule------//
    public void setScheduleShiftCurrentId(String ScheduleShiftCurrentId) {
        this.ScheduleShiftCurrentId = ScheduleShiftCurrentId;
    }

    public String getScheduleShiftCurrentId() {
        return ScheduleShiftCurrentId;
    }

    public void setShiftCurrentId(String ShiftCurrentId) {
        this.ShiftCurrentId = ShiftCurrentId;
    }

    public String getShiftCurrentId() {
        return ShiftCurrentId;
    }

    public void setSchlrPopNewsRoom(RichPopup schlrPopNewsRoom) {
        this.schlrPopNewsRoom = schlrPopNewsRoom;
    }

    public RichPopup getSchlrPopNewsRoom() {
        return schlrPopNewsRoom;
    }

    public void setTt3(RichTreeTable tt3) {
        this.tt3 = tt3;
    }

    public RichTreeTable getTt3() {
        return tt3;
    }


    public void setShiftCurrentName(String ShiftCurrentName) {
        this.ShiftCurrentName = ShiftCurrentName;
    }

    public String getShiftCurrentName() {
        return ShiftCurrentName;
    }

    public void setCopyShiftId(Number CopyShiftId) {
        this.CopyShiftId = CopyShiftId;
    }

    public Number getCopyShiftId() {
        return CopyShiftId;
    }
//    public String sendSMS()
//    {
//        
                      //URLConnection myURLConnection = null;
//                      URL myURL = null;
//                      BufferedReader reader = null;
//                      String mainUrl = "https://www.smsglobal.com/http-api.php?action=sendsms&user=bk6honho&password=samJfPVC&";
//                       String vFrom ="MBCTEST";
//                      String vto="971551051547";
//                   String vtext=" Dear Mahmoud ,Kindly have a look to your Rota for the changes";
//                      //Prepare parameter string
//                      StringBuilder sbPostData = new StringBuilder(mainUrl);
//                      sbPostData.append("from=" + vFrom);
//                       sbPostData.append("&to=" + vto);
//                      sbPostData.append("&text=" +vtext);
//                      mainUrl = sbPostData.toString();
//                      System.out.println("URL to Send SMS-" + mainUrl);
//                      try {
//                          //prepare connection
//                          myURL = new URL(mainUrl);
//                          myURLConnection = myURL.openConnection();
//                          myURLConnection.connect();
//                          reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
//                          //reading response
//                          String response;
//                          while ((response = reader.readLine()) != null) {
//                              //print response
//                              System.out.println(response);
//                          }
//                          System.out.println(response);
//                          //finally close connection
//                          reader.close();
//
//
//                      } catch (IOException e)
//                      
//                      {
//                          e.printStackTrace();
//                      } 
//    /////Second//////
//    URLConnection myURLConnection2 = null;
//    URL myURL2 = null;
//    BufferedReader reader2 = null;
//    String mainUrl2 = "https://www.smsglobal.com/http-api.php?action=sendsms&user=bk6honho&password=samJfPVC&";
//     String vFrom2 ="MBCTEST";
//    String vto2="971502458630";;
//    String vtext2="Heba Kindly have a look to your Rota for the changes";
//    //Prepare parameter string
//    StringBuilder sbPostData2 = new StringBuilder(mainUrl2);
//    sbPostData2.append("from=" + vFrom2);
//     sbPostData2.append("&to=" + vto2);
//    sbPostData2.append("&text=" +vtext2);
//    mainUrl2 = sbPostData2.toString();
//    System.out.println("URL to Send SMS-" + mainUrl2);
//    try {
//        //prepare connection
//        myURL2 = new URL(mainUrl2);
//        myURLConnection2 = myURL2.openConnection();
//        myURLConnection2.connect();
//        reader2 = new BufferedReader(new InputStreamReader(myURLConnection2.getInputStream()));
//        //reading response
//        String response2;
//        while ((response2 = reader2.readLine()) != null) {
//            //print response
//            System.out.println(response2);
//        }
//        System.out.println(response2);
//        //finally close connection
//        reader2.close();
//
//
//    } catch (IOException e)
//    
//    {
//        e.printStackTrace();
//    } 
//   ///// third  //////
// 
//   URLConnection myURLConnection3 = null;
//   URL myURL3 = null;
//   BufferedReader reader3 = null;
//   String mainUrl3 = "https://www.smsglobal.com/http-api.php?action=sendsms&user=bk6honho&password=samJfPVC&";
//    String vFrom3 ="MBCTEST";
//   String vto3="971501044103";;
//   String vtext3=" Dear Emad Kindly have a look to your Rota for the changes";
//   //Prepare parameter string
//   StringBuilder sbPostData3 = new StringBuilder(mainUrl3);
//   sbPostData3.append("from=" + vFrom3);
//    sbPostData3.append("&to=" + vto3);
//   sbPostData3.append("&text=" +vtext3);
//   mainUrl3 = sbPostData3.toString();
//   System.out.println("URL to Send SMS-" + mainUrl3);
//   try {
//       //prepare connection
//       myURL3 = new URL(mainUrl3);
//       myURLConnection3 = myURL3.openConnection();
//       myURLConnection3.connect();
//       reader3 = new BufferedReader(new InputStreamReader(myURLConnection3.getInputStream()));
//       //reading response
//       String response3;
//       while ((response3 = reader3.readLine()) != null) {
//           //print response
//           System.out.println(response3);
//       }
//       System.out.println(response3);
//       //finally close connection
//       reader3.close();
//
//
//   } catch (IOException e)
//   
//   {
//       e.printStackTrace();
//   } 
//
//        return "";
//        }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    //--------------------Presenter Scheduler Layout --------//
    public String cancelSchdeuleEntryPresenter()
    {
      //  this.setShowhidetable(false);
        
        return "gotoSchedulerHome";
        }
    
    public String createSectionRow()
    {

            OperationBinding oper1 = ADFUtils.findOperation("createSectionRow");
           
            oper1.execute();
        
           return null;
        }
 
    public  Number CurrentMonth()
    {
        
            Number monthkey=new Number();
               Calendar cal = Calendar.getInstance();
            cal.setTime(this.getWeekStartDate());
              // int year = cal.get(Calendar.YEAR);
               int month = cal.get(Calendar.MONTH);
              // int day = cal.get(Calendar.DAY_OF_WEEK);
               int year=cal.get(Calendar.YEAR);
              // int dayofmonth=cal.get(Calendar.DAY_OF_MONTH);
              // String DayofWeek="";
               String MonthofYear="";
          
            //------------Month-------
            if(month==0)
                MonthofYear="Jan";
          else  if(month==1)
                MonthofYear="Feb";
            else  if(month==2)
                  MonthofYear="March";
            else  if(month==3)
                  MonthofYear="Apr";
            else  if(month==4)
                  MonthofYear="May";
            else  if(month==5)
                  MonthofYear="Jun";
            else  if(month==6)
                  MonthofYear="Jul";
              else  if(month==7)
                    MonthofYear="Aug";
              else  if(month==8)
                    MonthofYear="Sept";
              else  if(month==9)
                    MonthofYear="Oct";
              else  if(month==10)
                    MonthofYear="Nov";
            else  if(month==11)
                  MonthofYear="Dec";

  month=month+1;
        try {
            monthkey = new Number(String.valueOf("1"+month+year));
        } catch (SQLException e) {
        }
        
        
            FacesMessage Message = new FacesMessage(this.getWeekStartDate().toString());   
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                        FacesContext fc = FacesContext.getCurrentInstance();   
                        fc.addMessage(null, Message);
        return monthkey;
                //MonthofYear+"-"+String.valueOf(year);
          
        }
    public void exceuteSections()
    {
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
             String StartDate=df.format(this.getWeekStartDate());
             //String EndDate=df.format(this.addDays(this.getWeekStartDate(), 6));
              OperationBinding oper = ADFUtils.findOperation("SchedulePresenterSearch");
              oper.getParamsMap().put("pstart_Date",StartDate);
              oper.execute();
              
        }
    public String SearchSchedulePresenter_action() {
       // this.setShowhidetable(true);
        exceuteSections();
        //this.bindPresentersShifts();
       // OperationBinding oper = ADFUtils.findOperation("SchedulePresenterSearch");
                               
                return null;
    }
    public String NextSchedulePresenter_action() {
   
       
        this.setWeekStartDate(this.addDays(this.getWeekStartDate(), 7));
        exceuteSections();
       
                return null;
    }
    
    public String PreviousSchedulePresenter_action() {
    
       
        this.setWeekStartDate(this.addDays(this.getWeekStartDate(), -7));
        exceuteSections();
      
                return null;
    }
    public String PublishPresenterScheduler()
    {
        
            Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
           // String workgroupid=currwk.getAttribute("WorkGroupId").toString();
        currwk.setAttribute("SchedularStatus", "PUBLISHED");
            ADFUtils.findOperation("Commit").execute();  
            return "";
        
        }
    public void bindPresentersShifts()
    {
       
          exceuteSections();
//   SchedulerAMImpl sc=this.getApplicationModule();
//   sc.getAsSchedulerSectionsVO2().executeQuery();
//        
//          // Refresh Tree Table//
//          RichTable table = this.getT3();
//          AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
//          adffctx.addPartialTarget(table);
        
        
  
         
      }
    
    public String getEmployeeShiftDay(Number SchdeulerId ,Number SectionId,java.util.Date day)
    {
            SchedulerAMImpl sc=this.getApplicationModule();
            
        AsSchedulerStaffVOImpl vo=sc.getAsSchedulerStaffVO1();
            vo.setWhereClauseParams(null);
            vo.setWhereClause(null);
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            vo.setWhereClause("FK_SCHEDULER_SECTION_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
            vo.setWhereClauseParams(new Object[] { SectionId, df.format(day) ,SchdeulerId});
            vo.executeQuery();
            if (vo.first() != null) {
           
                AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)vo.first();
//                if(r.getDefaultshift().equals("S0930E1830"))
//                    
              return r.getEmployeeName();
//                
//                else 
//                    
//                    return r.getEmployeeName()+","+r.getDefaultshift();
            }
     
           
           else 
          
        return " ";
        }
    
    public String getEmployeeShiftColor(Number SchdeulerId ,Number SectionId,java.util.Date day)
    {
            SchedulerAMImpl sc=this.getApplicationModule();
            String color= "transparent";
            
            
            AsSchedulerStaffVOImpl vo=sc.getAsSchedulerStaffVO1();
            vo.setWhereClauseParams(null);
            vo.setWhereClause(null);
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            vo.setWhereClause("FK_SCHEDULER_SECTION_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
            vo.setWhereClauseParams(new Object[] { SectionId, df.format(day) ,SchdeulerId});
            vo.executeQuery();
            if (vo.first() != null) {
            
                AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)vo.first();
                color=r.getStatusColor();
            }
        
               
                
              
        return color;
        }
    
    public String getShiftBulletinNotesPresenter(Number SchdeulerId ,Number SectionId,java.util.Date day)
    {
       

        
       SchedulerAMImpl sc=this.getApplicationModule();
      // String color= "transparent";
       
       
       AsSchedulerStaffVOImpl vo=sc.getAsSchedulerStaffVO1();
       vo.setWhereClauseParams(null);
       vo.setWhereClause(null);
       DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
       vo.setWhereClause("FK_SCHEDULER_SECTION_ID=:1 and schedular_start_date=:2  and SCHEDULER_ID=:3");
       vo.setWhereClauseParams(new Object[] { SectionId, df.format(day) ,SchdeulerId});
       vo.executeQuery();
            if (vo.first() != null) {
            
                            AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)vo.first();
                           // First Get Bulletins
                            String SchedulerBulletin=getNesRoomShiftBulletins(r.getSchedulerStaffId());
                          
                           String Note=r.getNotes();
        
                            if(SchedulerBulletin.equals("None"))
                            {
                                if(Note.equals("N"))
                                 return "";
                                else
                                    return Note;
                            }
                            else
            
                            {
                                if(Note.equals("N"))
                                    
                                  return SchedulerBulletin;
                            else
                              
                                  return SchedulerBulletin.concat(" | ").concat(r.getNotes());
            
            
                            }
                        }
       
                   else 
           return "";
        
        
        
        
        
        } 
    
    public String getNesRoomShiftBulletins(Number Scedular_staff_id )
    
    {
         StringBuilder  Bulletins=new StringBuilder();
            SchedulerAMImpl sc=this.getApplicationModule(); 
            AsPublicSpaceBulletinsVOImpl b = sc.getAsPublicSpaceBulletinsVO1();
            b.setWhereClauseParams(null);
            b.setWhereClause(null);
            b.setNamedWhereClauseParam("p_schedular_staff_id", Scedular_staff_id);
             b.executeQuery();
         
            int count =b.getRowCount();
            if(count>0)
            {
            Bulletins.append("(");
            for(int i=1;i<=count;i++)
            {
                    AsPublicSpaceBulletinsVORowImpl r = (AsPublicSpaceBulletinsVORowImpl)b.next();
             
                                        Bulletins.append(r.getBulletinName());
                                   
                                  if(i!=count)
                                  {
                                       Bulletins.append("/");
                                       Bulletins.append(""); 
                                  }
              
            }
            
            Bulletins.append(")");
            }
            else 
                Bulletins.append("None");
          
           return Bulletins.toString();
        
        }
    
    
    public void AddSchedulerStaffPresenter(ClientEvent ce){
        
        this.setSaveUpdate_text("Save");
        this.setSaveUpdateNotify_text("Save & Notify");
      java.util.Date columnDate = (java.util.Date)ce.getParameters().get("column"); 
       RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
      try {
          int rowKeyIndex =
          ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             
        
        AddSelectedTableRowsPresenter(columnDate,rowKeyIndex);  
        
      }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
    }
    

    private void AddSelectedTableRowsPresenter(java.util.Date columndate,int rowKeyIndex){
    
       RichTable table = this.getT3();
       
         table.setRowIndex(rowKeyIndex);
         JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
         AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
         Number CurrentSection = rowToCopyFrom.getSectionId();
  
         ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
         //to get startdate  which is column passed in client listener
         ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
         ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
       ADFUtils.setBoundAttributeValue("FkSchedulerSectionId",CurrentSection);
       // ADFUtils.setBoundAttributeValue("ShiftId",currentrow[1]);
     
         RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getSchlrPopPresenter().show(hints);
         //this.showSchdlrPopup();

         AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
       adffctx.addPartialTarget(table);
     }

    public void onClickCancelSchdlrPopPresenterBtn(ActionEvent actionEvent) {
        // Add event code here...
        if(this.getSaveUpdate_text().equals("Save"))
        {
        ADFUtils.findOperation("DeleteSchdlrStaff").execute();
            ADFUtils.findOperation("Commit").execute();

        this.getSchlrPopPresenter().hide();
     
    }
        else this.getSchlrPopPresenter().hide();
        
    }
    
    
    public void onClickSaveSchdlrPopPresenterBtn(ActionEvent actionEvent) {
        // Add event code here...
        SchedulerAMImpl sc=this.getApplicationModule();
        AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
        staff.executeQuery();
          commit_action();
        
     //  ADFUtils.findOperation("Commit").execute();
       // this.setShowhidetable(true);
       this.bindPresentersShifts();
        commit_action();
        this.bindPresentersShifts();
        this.getSchlrPopPresenter().hide();
       // commit_action();
    }
    public void EditSchedulePresenter(ClientEvent ce){
       
        this.setSaveUpdate_text("Update");
        this.setSaveUpdateNotify_text("Update & Notify");
 
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
            Number vSectionId = rowToCopyFrom.getSectionId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
          schedulerStaffId=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
   
            this.getCurrrentSchedulerStaffRow(schedulerStaffId);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getSchlrPopPresenter().show(hints);
           
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
    
    }
    
    public Number getCurrentSchedulerStaffIdPresenter(Number SectionId,String SchdeulerStaffDate)
    {
         Number Scstaffid=new Number();
            Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
            SchedulerAMImpl sc=this.getApplicationModule();
            
            AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffCurrentVO();
            staff.setWhereClauseParams(null);
            staff.setWhereClause(null);
            staff.setWhereClause("FK_SCHEDULER_SECTION_ID=:1 and schedular_start_date=:2 and PARENT_SCHEDULER_STAFF_ID is null and SCHEDULER_ID=:3 ");
            staff.setWhereClauseParams(new Object[] { SectionId, SchdeulerStaffDate,schedulerId });
            staff.executeQuery();
            if (staff.first() != null) {
                 AsSchedulerStaffVORowImpl row = ( AsSchedulerStaffVORowImpl) staff.first();
           
                Scstaffid=row.getSchedulerStaffId();
           
            }
               
        return Scstaffid;
        
        }
    public void CopySchedulePresenter(ClientEvent ce){
       
       // this.setSaveUpdate_text("Update");
        this.setCutCopyC("Copy");
        Number schedulerStaffId = new Number();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
            Number vSectionId = rowToCopyFrom.getSectionId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
            
          CopySchedulerStaffIdPresenter=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
    //                FacesMessage Message = new FacesMessage(schedulerStaffId.toString());
    //                      Message.setSeverity(FacesMessage.SEVERITY_INFO);
    //                      FacesContext fc = FacesContext.getCurrentInstance();
    //                      fc.addMessage(null, Message);
    
          //  this.getCurrrentSchedulerPresenterRowCopy(schedulerStaffId);
          // this.showSchdlrPopup();
           
        
       }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
     
    }
//    public void getCurrrentSchedulerPresenterRowCopy(Number SchedulerStaffID)
//    {
//            Number Scstaffid=new Number();
//               SchedulerAMImpl sc=this.getApplicationModule();
//               
//               AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
//               staff.setWhereClauseParams(null);
//               staff.setWhereClause(null);
//               staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
//               staff.setWhereClauseParams(new Object[] { SchedulerStaffID });
//               staff.executeQuery();
//             this.setCopyId(SchedulerStaffID);
//            AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();
//          this.setCopyValue2(r.getEmployeeName());
//          this.setCopyPersonId(r.getPersonId());
//          this.setCopyWkId(r.getWorkGroupId());
//          try
//          {
//            this.setCopyValue(r.getShiftId().toString());
//             this.setCopyShiftId(r.getShiftId());
//    }
//          catch (NullPointerException ex)
//          {
//                  this.setCopyValue(null);
//                  this.setCopyShiftId(null);
//              }
//        }

    public void PasteSchedulerStaffPresenter(ClientEvent ce){
        
      //  this.setSaveUpdate_text("Save");
        try
        {
        if(this.getCutCopyC().equals("Copy"))
        {
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
       
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
                
            PasteSelectedTableRowsPresenter(columnDate, rowKeyIndex,"Copy");  
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
        
        }
      else   if(this.getCutCopyC().equals("Cut"))
        {
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
                
            PasteSelectedTableRowsPresenter(columnDate, rowKeyIndex,"Cut");  
        
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
        }  
        
        }
            
        }
        catch (NullPointerException ex )
        {
            
                FacesMessage Message = new FacesMessage("No Data to Paste");   
                      Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                      FacesContext fc = FacesContext.getCurrentInstance();   
                      fc.addMessage(null, Message); 
            
            
            }
    }
    
    private void PasteSelectedTableRowsPresenter(java.util.Date columndate, int rowKeyIndex,String CutCopy){
    
    
    if(CutCopy.equals("Copy"))
    {
    
        if(CopySchedulerStaffIdPresenter.intValue()==0)
         {
             
                         FacesMessage Message2 = new FacesMessage("No Values Copied");   
                               Message2.setSeverity(FacesMessage.SEVERITY_WARN);   
                               FacesContext fc2 = FacesContext.getCurrentInstance();   
                               fc2.addMessage(null, Message2);  
            
             }
        else
         {
       RichTable table = this.getT3();
       table.setRowIndex(rowKeyIndex);
       JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
             AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
             Number vSectionId = rowToCopyFrom.getSectionId();
             DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
             String Schedulesdate=df.format(columndate);

//------------------------------------------------------------------//
           
                SchedulerAMImpl sc=this.getApplicationModule();
                 AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
                staff.setWhereClauseParams(null);
                staff.setWhereClause(null);
                staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
                staff.setWhereClauseParams(new Object[] { CopySchedulerStaffIdPresenter });
                staff.executeQuery();
            
             AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();

             try
             {
                 ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
                 //to get startdate  which is column passed in client listener
                 ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
                 ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
                 ADFUtils.setBoundAttributeValue("FkSchedulerSectionId", vSectionId);
                     ADFUtils.setBoundAttributeValue("PersonId", r.getPersonId());
                     ADFUtils.setBoundAttributeValue("EmployeeName", r.getEmployeeName());
                 ADFUtils.setBoundAttributeValue("WorkGroupId", r.getWorkGroupId());
                 ADFUtils.setBoundAttributeValue("Notes", r.getNotes());
                  ADFUtils.setBoundAttributeValue("ShiftId",r.getShiftId());
                   ADFUtils.setBoundAttributeValue("RecurringFlag","N");
                     ADFUtils.setBoundAttributeValue("BulletinFlag","N"); 
                 
            
             }
             catch (NullPointerException ex)
             {
                 
                   ADFUtils.setBoundAttributeValue("ShiftId",r.getShiftId());
                   ADFUtils.setBoundAttributeValue("Notes", r.getNotes());
               }

             ADFUtils.findOperation("Commit").execute(); 
   ///// Copy ofBulletins -----
      Number toSchedulerStaffIdPresenter=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
      CopyPasteBulletins(CopySchedulerStaffIdPresenter,toSchedulerStaffIdPresenter);
         
      ADFUtils.findOperation("Commit").execute(); 
      this.bindPresentersShifts();
         }  
    }
      else  if(CutCopy.equals("Cut"))
    {
    
        if(CutSchedulerStaffIdPresenter.intValue()==0)
         {
             
                         FacesMessage Message2 = new FacesMessage("No Values Cut");   
                               Message2.setSeverity(FacesMessage.SEVERITY_WARN);   
                               FacesContext fc2 = FacesContext.getCurrentInstance();   
                               fc2.addMessage(null, Message2);  
            
             }
        else
         {
          RichTable table = this.getT3();
          table.setRowIndex(rowKeyIndex);
           JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
           AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
             Number vSectionId = rowToCopyFrom.getSectionId();
             DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
             String Schedulesdate=df.format(columndate);

//------------------------------------------------------------------//
           
//                SchedulerAMImpl sc=this.getApplicationModule();
//                 AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
//                staff.setWhereClauseParams(null);
//                staff.setWhereClause(null);
//                staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
//                staff.setWhereClauseParams(new Object[] { CopySchedulerStaffIdPresenter });
//                staff.executeQuery();
//            
//             AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();

             try
             {
                 ADFUtils.findOperation("CreateInsertSchdlrStaff").execute();
                 //to get startdate  which is column passed in client listener
                 ADFUtils.setBoundAttributeValue("SchedularStartDate", columndate);
                 ADFUtils.setBoundAttributeValue("SchedularEndDate", columndate);
                 ADFUtils.setBoundAttributeValue("FkSchedulerSectionId", vSectionId);
                     ADFUtils.setBoundAttributeValue("PersonId", CutCList.get(0));
                   //  ADFUtils.setBoundAttributeValue("EmployeeName", r.getEmployeeName());
                   ADFUtils.setBoundAttributeValue("ShiftId",CutCList.get(1));
                 ADFUtils.setBoundAttributeValue("WorkGroupId", CutCList.get(2));
                 ADFUtils.setBoundAttributeValue("Notes", CutCList.get(3));
                
                 ADFUtils.setBoundAttributeValue("RecurringFlag","N");
                     ADFUtils.setBoundAttributeValue("BulletinFlag","N"); 
                 
            
             }
             catch (NullPointerException ex)
             {
                 
//                   ADFUtils.setBoundAttributeValue("ShiftId",r.getShiftId());
//                   ADFUtils.setBoundAttributeValue("Notes", r.getNotes());
               }

             ADFUtils.findOperation("Commit").execute(); 
   ///// Copy ofBulletins -----
      Number toSchedulerStaffIdPresenter=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
      CutPasteBulletins(toSchedulerStaffIdPresenter);
       ADFUtils.findOperation("Commit").execute(); 
      this.bindPresentersShifts();
         }  
    }  
        
        
        
    }
    
    public void CutPasteBulletins( Number to_Scheduler_staff_id)
    {
        
    
          
            int count=CutABCBulletins.size();
           
            if (count>0) {
            
                    for(int i=0;i<count;i++)
                
                {
                      
                        OperationBinding oper = ADFUtils.findOperation("createBulletinRow");
                                  oper.getParamsMap().put("fkscheduler_staff_id", to_Scheduler_staff_id);
                        oper.getParamsMap().put("fkBulletinID", CutABCBulletins.get(i));
  
                                  oper.execute();   
                      
                    }
                 
            }
            
    
        }
    public void CopyPasteBulletins(Number from_Scheduler_staff_id, Number to_Scheduler_staff_id)
    {
        
    
            SchedulerAMImpl sc=this.getApplicationModule();
            AsSchedulerStaffBulletinsVOImpl vo = sc.getAsSchedulerStaffBulletinsVO1();
            vo.setWhereClauseParams(null);
            vo.setWhereClause(null);
             vo.setWhereClause("SCHEDULER_STAFF_ID=:1 ");
            vo.setWhereClauseParams(new Object[] { from_Scheduler_staff_id});
             
            vo.executeQuery();
            int count=vo.getRowCount();
           
            if (count>0) {
            
                    for(int i=1;i<=count;i++)
                
                {
                        AsSchedulerStaffBulletinsVORowImpl r=( AsSchedulerStaffBulletinsVORowImpl)vo.next();  
                        OperationBinding oper = ADFUtils.findOperation("createBulletinRow");
                                  oper.getParamsMap().put("fkscheduler_staff_id", to_Scheduler_staff_id);
                        oper.getParamsMap().put("fkBulletinID", r.getBulletinId());
//                                             FacesMessage Message2 = new FacesMessage(r.getBulletinId().toString());
//                                                   Message2.setSeverity(FacesMessage.SEVERITY_INFO);
//                                                   FacesContext fc2 = FacesContext.getCurrentInstance();
//                                                   fc2.addMessage(null, Message2);
                                  oper.execute();   
                      
                    }
                 
            }
            
    
        }
  
    public void DeleteScheduleSection(ClientEvent ce){

        int id=       ((Double)ce.getParameters().get("column")).intValue();
         Number SectionId =  new oracle.jbo.domain.Number(id);    
         RichTable table = this.getT3();
         Object oldKey = table.getRowKey();
         
         try {
              int rowKeyIndex =
              ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
              table.setRowIndex(rowKeyIndex);
             
    //                     FacesMessage Message2 = new FacesMessage(ShiftattributeId);
    //                           Message2.setSeverity(FacesMessage.SEVERITY_INFO);
    //                           FacesContext fc2 = FacesContext.getCurrentInstance();
    //                           fc2.addMessage(null, Message2);

           OperationBinding oper = ADFUtils.findOperation("deleteSchedulerSection");
             oper.getParamsMap().put("SectionId", SectionId);
             oper.execute();
             if(Integer.valueOf(oper.getResult().toString())==0)
             {
                 
          
                     
                         FacesMessage Message2 = new FacesMessage("Section Deleted Successfully ");
                     Message2.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc2 = FacesContext.getCurrentInstance();
                     fc2.addMessage(null, Message2);
                 //   this.bindPresentersShifts();
                    
                     
                     
                        
                  
                 
                 
                 
             
                 }
             else 
             {
                 
                                     FacesMessage Message2 = new FacesMessage("  Sections have Assigned Employees and Shift, Section Can't be Deleted ");
                                                Message2.setSeverity(FacesMessage.SEVERITY_WARN);
                                                FacesContext fc2 = FacesContext.getCurrentInstance();
                                                fc2.addMessage(null, Message2); 
                 
                 }
      
     
         
         }
         catch(Exception e){
           e.printStackTrace();
             FacesMessage Message = new FacesMessage(e.getMessage());   
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                   FacesContext fc = FacesContext.getCurrentInstance();   
                   fc.addMessage(null, Message);  
         }
         finally {
            table.setRowKey(oldKey);
         }  
        
    }
    public void DeleteScheduleShiftPresenter(ClientEvent ce){
       
    //               ADFUtils.findOperation("DeleteSchdltStaff").execute();
    //        ADFUtils.findOperation("Commit").execute();
    //        this.bindEmployeesShifts();
                Number schedulerStaffId = new Number();
         java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
         RichTable table = this.getT3();
         Object oldKey = table.getRowKey();
         
         try {
              int rowKeyIndex =
              ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
              table.setRowIndex(rowKeyIndex);
             JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
             AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
             Number vSectionId = rowToCopyFrom.getSectionId();
             DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
             String Schedulesdate=df.format(columnDate);
             schedulerStaffId=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
  
            OperationBinding oper = ADFUtils.findOperation("deleteSchdlr");
            oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
            oper.getParamsMap().put("Type", "Delete");
            oper.getParamsMap().put("PersonID", this.getLoggedPersonId());
             oper.execute();
             this.bindPresentersShifts();
         
        }
         catch(Exception e){
           e.printStackTrace();
             FacesMessage Message = new FacesMessage(e.getMessage());   
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                   FacesContext fc = FacesContext.getCurrentInstance();   
                   fc.addMessage(null, Message);  
         }
         finally {
            table.setRowKey(oldKey);
         }  
        
    }
    
    public void DeleteAllScheduleShiftPresenter(ClientEvent ce){
        
        
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");
                DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
               this.setDeleteDateStartC(columnDate);
                Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
                       
                java.util.Date enddate=DateConversion.getUtilDateFromJboDate((oracle.jbo.domain.Date)currwk.getAttribute("EndDate"));
                this.setDeleteDateEndC(enddate);
                this.setDeleteDateMaxC(enddate);
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getDeleteC().show(hints);
                
 
       
    }
    private String callRepeatPatternPresenter(Number schedulerStaffId,Date scdate,Date senddate,Number fksectionId) {
        OperationBinding oper = ADFUtils.findOperation("repeatPatternPresenter");
        oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
        oper.getParamsMap().put("schedulerdate", scdate);
        oper.getParamsMap().put("schedulerenddate", senddate);
        oper.getParamsMap().put("fksectionId", fksectionId);        

        oper.execute();
        
//        FacesMessage Message = new FacesMessage(schedulerStaffId.toString());   
//              Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//              FacesContext fc = FacesContext.getCurrentInstance();   
//              fc.addMessage(null, Message);  
        String retValue = (String) oper.getResult();
        return retValue;
    }
    public void RepeatPAtternPresenter(ClientEvent ce){
       
        
      
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
       this.setRepeatDateStartC(columnDate);
        Row currwk = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
               
        java.util.Date enddate=DateConversion.getUtilDateFromJboDate((oracle.jbo.domain.Date)currwk.getAttribute("EndDate"));
        this.setRepeatDateEndC(enddate);
        this.setRepeatDateMaxC(enddate);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getRepeatC().show(hints);
        
        
        
//        RichTable table = this.getT3();
//        Object oldKey = table.getRowKey();
//        int rowKeyIndex =  ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
//        table.setRowIndex(rowKeyIndex);
//        JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
//        AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
//        Number vSectionId = rowToCopyFrom.getSectionId();
//    
//            schedulerStaffId=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
//    
//       String repeatStatus = this.callRepeatPatternPresenter(schedulerStaffId,DateConversion.getJboDateFromUtilDate(columnDate),vSectionId);
//            if ("SUCCESS".equals(repeatStatus)) {
//               
//           this.bindPresentersShifts();
//                          
//                SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been applied successfully.",
//                                              FacesMessage.SEVERITY_INFO);
//            }
//            else if("OVERLAP".equals(repeatStatus))
//            {
//                    SchedulerUtil.showPageMessage("Warning", "Repeat Pattern can't be applied as there are employees in the next date range ,Please use Copy /Paste Functionality",
//                                                  FacesMessage.SEVERITY_WARN); 
//                
//                }
//            else {
//                SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been failed.", FacesMessage.SEVERITY_ERROR);
//            }
    
        
    
    }

 public void hideAddNewsRoomPopup()
 {

        // RichPopup.PopupHints hints = new RichPopup.PopupHints();
         this.getP1().hide();
     
     
     }
    public void hideEditNewsRoomPopup()
    {

           // RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getP2().hide();
        
        
        }

  
    
     public void onClickCloseRepeatPopup(ActionEvent actionEvent) 

    {

           // RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getRepeatA().hide();
        
        
        }
    public void onClickCloseDeletePopup(ActionEvent actionEvent) 

    {

          // RichPopup.PopupHints hints = new RichPopup.PopupHints();
           this.getDeleteA().hide();
       
       
       }
    public void onClickCloseRepeatPopupC(ActionEvent actionEvent) 

    {

    
           this.getRepeatC().hide();
       
       
       }
    public void onClickCloseDeletePopupC(ActionEvent actionEvent) 

    {

        
           this.getDeleteC().hide();
       
       
       }
    
    public void onClickCloseRepeatPopupB(ActionEvent actionEvent) 

    {

    
           this.getRepeatB().hide();
       
       
       }
    public void onClickCloseDeletePopupB(ActionEvent actionEvent) 

    {

        
           this.getDeleteB().hide();
       
       
       }
    public void onClickRepeatPatternNewsRoom(ActionEvent actionEvent) 

    {

               
                  Number[]currentrow=  getGurrentShiftAttbiuteRowData();
                Number vFkShiftAttributeId=currentrow[0];
                 Number vfkshiftid=currentrow[1];
                 DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        java.util.Date columnDate=this.getRepeatDateStartA();
        java.util.Date columnEndDate=this.getRepeatDateEndA();
                 String Schedulesdate=df.format(columnDate);
            //     String Scheduleendate=df.format(columnEndDate);
                Number  schedulerStaffId=this.getCurrentSchedulerStaffIdNewsRoom(vFkShiftAttributeId, Schedulesdate);
           
            String repeatStatus = this.callRepeatPatternNewsRoom(schedulerStaffId,DateConversion.getJboDateFromUtilDate(columnDate),DateConversion.getJboDateFromUtilDate(columnEndDate),vfkshiftid,vFkShiftAttributeId);
        if ("FAIL".equals(repeatStatus)) {
           SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been failed.", FacesMessage.SEVERITY_ERROR);
        }
        else 
        {
                                   SchedulerAMImpl sc=this.getApplicationModule();
                                   sc.getAsSchedulerShiftsVO().executeQuery();
                                   //sc.getAsShiftAttributesVO2().executeQuery();
                                    RichTreeTable table = this.getTt3();
                                   
                                   AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
                                   adffctx.addPartialTarget(table);
           if(repeatStatus.contains("dates where another employees Exist"))
           
           {
                    SchedulerUtil.showPageMessage("Saving", repeatStatus,
                                                                   FacesMessage.SEVERITY_ERROR); 
               
                }
            else 
            {
                   SchedulerUtil.showPageMessage("Saving", repeatStatus,
                                                                   FacesMessage.SEVERITY_INFO);            
               }
           }

    System.out.print("Status:"+repeatStatus);


//                 if ("SUCCESS".equals(repeatStatus)) {
//                    
//                   
//                    SchedulerAMImpl sc=this.getApplicationModule();
//                    sc.getAsSchedulerShiftsVO().executeQuery();
//                    //sc.getAsShiftAttributesVO2().executeQuery();
//                     RichTreeTable table = this.getTt3();
//                    
//                    AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
//                    adffctx.addPartialTarget(table);
//                     
//                     SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been applied successfully.",
//                                                   FacesMessage.SEVERITY_INFO);
//                 }
//                 else if ("LEAVES".equals(repeatStatus))
//                     {
//
//                                        
//                                       
//                                        SchedulerAMImpl sc=this.getApplicationModule();
//                                        sc.getAsSchedulerShiftsVO().executeQuery();
//                                        //sc.getAsShiftAttributesVO2().executeQuery();
//                                         RichTreeTable table = this.getTt3();
//                                        
//                                        AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
//                                        adffctx.addPartialTarget(table);
//                                         
//                                         SchedulerUtil.showPageMessage("Saved with Warning", " Employee has Leaves/Pending Leaves ,Repeat Pattern has been applied successfully.",
//                                                                       FacesMessage.SEVERITY_WARN);
//                                     }
//                 else if("OVERLAP".equals(repeatStatus))
//                 {
//                         SchedulerUtil.showPageMessage("Warning", "Repeat Pattern can't be applied , employees in the next date range check please ",
//                                                       FacesMessage.SEVERITY_WARN); 
//                     
//                     }
//                 else {
//                     SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been failed.", FacesMessage.SEVERITY_ERROR);
//                 }
                 this.getRepeatA().hide();
                 
        
        
    }

    public void onClickRepeatPatternC(ActionEvent actionEvent) 

    {
        RichTable table = this.getT3();
        DCBindingContainer bc = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = bc.findIteratorBinding("AsSchedulerSectionsVO2Iterator");
        Row r = iterator.getCurrentRow();
       

                Number vSectionId = (Number)r.getAttribute("SectionId");
                    //rowToCopyFrom.getSectionId();
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                java.util.Date columnDate=this.getRepeatDateStartC();
                java.util.Date columnEndDate=this.getRepeatDateEndC();
                         String Schedulesdate=df.format(columnDate);
           
            
            Number  schedulerStaffId=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
            
               String repeatStatus = this.callRepeatPatternPresenter(schedulerStaffId,DateConversion.getJboDateFromUtilDate(columnDate),DateConversion.getJboDateFromUtilDate(columnEndDate),vSectionId);
                    if ("SUCCESS".equals(repeatStatus)) {
                       
                   this.bindPresentersShifts();
                                  
                        SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been applied successfully.",
                                                      FacesMessage.SEVERITY_INFO);
                    }
                    else if("OVERLAP".equals(repeatStatus))
                    {
                            SchedulerUtil.showPageMessage("Warning", "Repeat Pattern can't be applied as there are employees in the next date range ,Please use Copy /Paste Functionality",
                                                          FacesMessage.SEVERITY_WARN); 
                        
                        }
                    else {
                        SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been failed.", FacesMessage.SEVERITY_ERROR);
                    }
            
        
        
        
        this.getRepeatC().hide();
                 
        
        
    }

    public void onClickDeleteC(ActionEvent actionEvent) 

    {
        RichTable table = this.getT3();
        DCBindingContainer bc = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = bc.findIteratorBinding("AsSchedulerSectionsVO2Iterator");
        Row r = iterator.getCurrentRow();
       Number vSectionId = (Number)r.getAttribute("SectionId");
            
             DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                java.util.Date columnDate=this.getDeleteDateStartC();
                java.util.Date columnEndDate=this.getDeleteDateEndC();
                         String Schedulesdate=df.format(columnDate);
                
            
            Number  schedulerStaffId=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
         
                    OperationBinding oper = ADFUtils.findOperation("deleteAllSchdlrPresenter");
                    oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
          oper.getParamsMap().put("schedulerdate", DateConversion.getJboDateFromUtilDate(columnDate));
        oper.getParamsMap().put("schedulerenddate",DateConversion.getJboDateFromUtilDate(columnEndDate));
                    oper.execute();
                   this.bindPresentersShifts();
                   this.getDeleteC().hide();
                
    
        
        
        
    }

    public void onClickRepeatPatternB(ActionEvent actionEvent) 

    {
  
        
        
       // RichTable table = this.getT3();
        DCBindingContainer bc = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = bc.findIteratorBinding("AsSchdeulerStaffTableVOIterator");
        Row r = iterator.getCurrentRow();

                Number vPersonId = (Number)r.getAttribute("PersonId");
                    //rowToCopyFrom.getSectionId();
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                java.util.Date columnDate=this.getRepeatDateStartB();
                java.util.Date columnEndDate=this.getRepeatDateEndB();
                         String Schedulesdate=df.format(columnDate);
           
            
         
      Number  schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
               //            String repeatStatus = this.callRepeatPattern(schedulerStaffId); 
               String repeatStatus = this.callRepeatPattern(schedulerStaffId,DateConversion.getJboDateFromUtilDate(columnDate),DateConversion.getJboDateFromUtilDate(columnEndDate));
                    if ("SUCCESS".equals(repeatStatus)) {
                       
                        this.bindEmployeesShifts();
                                  
                        SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been applied successfully.",
                                                      FacesMessage.SEVERITY_INFO);
                    }
                    else if("OVERLAP".equals(repeatStatus))
                    {
                            SchedulerUtil.showPageMessage("Warning", "Repeat Pattern can't be applied as there are employees in the next date range ,Please use Copy /Paste Functionality",
                                                          FacesMessage.SEVERITY_WARN); 
                        
                        }
                    else {
                        SchedulerUtil.showPageMessage("Saving", "Repeat Pattern has been failed.", FacesMessage.SEVERITY_ERROR);
                    }
            
        
        
        
        this.getRepeatB().hide();
                 
        
        
    }
    
    public void onClickDeleteB(ActionEvent actionEvent) 

    {
        RichTable table = this.getT3();
      
        DCBindingContainer bc = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = bc.findIteratorBinding("AsSchdeulerStaffTableVOIterator");
        Row r = iterator.getCurrentRow();

                Number vPersonId = (Number)r.getAttribute("PersonId");
                    //rowToCopyFrom.getSectionId();
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
                java.util.Date columnDate=this.getDeleteDateStartB();
                java.util.Date columnEndDate=this.getDeleteDateEndB();
                         String Schedulesdate=df.format(columnDate);
           
            
         
        Number  schedulerStaffId=this.getCurrentSchedulerStaffId(vPersonId, Schedulesdate);
            
           
                
                    OperationBinding oper = ADFUtils.findOperation("deleteAllSchdlr");
                    oper.getParamsMap().put("schedulerStaffId", schedulerStaffId);
          oper.getParamsMap().put("schedulerdate", DateConversion.getJboDateFromUtilDate(columnDate));
        oper.getParamsMap().put("schedulerenddate",DateConversion.getJboDateFromUtilDate(columnEndDate));
                    oper.execute();
                   this.bindEmployeesShifts();
                   this.getDeleteB().hide();
                
    
        
        
        
    }

    public void setRepeatDateStartA(java.util.Date RepeatDateStartA) {
        this.RepeatDateStartA = RepeatDateStartA;
    }

    public java.util.Date getRepeatDateStartA() {
        return RepeatDateStartA;
    }

    public void setRepeatDateEndA(java.util.Date RepeatDateEndA) {
        this.RepeatDateEndA = RepeatDateEndA;
    }

    public java.util.Date getRepeatDateEndA() {
        return RepeatDateEndA;
    }

    public void setRepeatDateMaxA(java.util.Date RepeatDateMaxA) {
        this.RepeatDateMaxA = RepeatDateMaxA;
    }

    public java.util.Date getRepeatDateMaxA() {
        return RepeatDateMaxA;
    }

 



    public void setRepeatDateStartB(java.util.Date RepeatDateStartB) {
        this.RepeatDateStartB = RepeatDateStartB;
    }

    public java.util.Date getRepeatDateStartB() {
        return RepeatDateStartB;
    }

    public void setRepeatDateEndB(java.util.Date RepeatDateEndB) {
        this.RepeatDateEndB = RepeatDateEndB;
    }

    public java.util.Date getRepeatDateEndB() {
        return RepeatDateEndB;
    }

    public void setRepeatDateMaxB(java.util.Date RepeatDateMaxB) {
        this.RepeatDateMaxB = RepeatDateMaxB;
    }

    public java.util.Date getRepeatDateMaxB() {
        return RepeatDateMaxB;
    }

    public void setRepeatDateStartC(java.util.Date RepeatDateStartC) {
        this.RepeatDateStartC = RepeatDateStartC;
    }

    public java.util.Date getRepeatDateStartC() {
        return RepeatDateStartC;
    }

    public void setRepeatDateEndC(java.util.Date RepeatDateEndC) {
        this.RepeatDateEndC = RepeatDateEndC;
    }

    public java.util.Date getRepeatDateEndC() {
        return RepeatDateEndC;
    }

    public void setRepeatDateMaxC(java.util.Date RepeatDateMaxC) {
        this.RepeatDateMaxC = RepeatDateMaxC;
    }

    public java.util.Date getRepeatDateMaxC() {
        return RepeatDateMaxC;
    }

    public void setRepeatA(RichPopup RepeatA) {
        this.RepeatA = RepeatA;
    }

    public RichPopup getRepeatA() {
        return RepeatA;
    }

    public void setDeleteA(RichPopup DeleteA) {
        this.DeleteA = DeleteA;
    }

    public RichPopup getDeleteA() {
        return DeleteA;
    }
    public void loadTemplate(ActionEvent actionEvent) {
        // Add event code here...
        Row  currenttemp=ADFUtils.findIterator("AsTemplatesVO1Iterator").getViewObject().getCurrentRow();
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
        java.util.Date columnDate=new java.util.Date();
        try {
           columnDate = df.parse(this.getTemplateDate());
        } catch (ParseException e) {
        }
        Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
       
       OperationBinding oper = ADFUtils.findOperation("loadTemplatePresenter");
        oper.getParamsMap().put("TemplateId", currenttemp.getAttribute("TemplateId"));
        oper.getParamsMap().put("schedulerdate", DateConversion.getJboDateFromUtilDate(columnDate));
        oper.getParamsMap().put("schedulerId",(Number)asSchedulersRow.getAttribute("SchedulerId"));
        oper.getParamsMap().put("SchedulerTitle",asSchedulersRow.getAttribute("SchedulerTitle").toString());
        oper.execute();
  
        if (oper.getErrors().isEmpty()) {
            this.getTemplatepopup().hide();

//            if(Integer.valueOf(oper.getResult().toString())==0)
//            {
//                FacesMessage Message = new FacesMessage("There is Section Order Gap ");
//               // ("No Need to Create New Section Rows")
//                    //(oper.getResult().toString());
//                    //(currenttemp.getAttribute("TemplateId").toString());   
//                     Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                     FacesContext fc = FacesContext.getCurrentInstance();   
//                     fc.addMessage(null, Message); 
//            }  
//            else 
//            {
                    FacesMessage Message = new FacesMessage("Template Applied Succeesfully");
                        //(oper.getResult().toString());
                        //("New Section Rows Need to Be Created");
                        //(oper.getResult().toString());
                        //(currenttemp.getAttribute("TemplateId").toString());   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message); 
                
              //  }
            
        }
   
 
        
      
    
    }
    
    public void openTemplate(ClientEvent ce){
       
//        this.setSaveUpdate_text("Update");
//        Number schedulerStaffId = new Number();
        // beforre open Template poup retive the tempaltes associated with same workgruops of current scheduler 
       ViewObject template= ADFUtils.findIterator("AsTemplatesVO1Iterator").getViewObject();
        template.setWhereClause(null);
         template.setWhereClauseParams(null);
        template.setWhereClause("FK_WORK_GROUP_ID in (select work_group_id  from  as_scheduler_shifts  where SCHEDULER_ID =:1)");
        Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
        Number SchedulerId=(Number)asSchedulersRow.getAttribute("SchedulerId");
        template.setWhereClauseParams(new Object[] { SchedulerId });
        template.executeQuery();
        
        
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column"); 
        DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
    //   String Schedulesdate=df.format(columnDate);
       this.setTemplateDate(df.format(columnDate));
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getTemplatepopup().show(hints);
        
        
//        RichTable table = this.getT3();
//        Object oldKey = table.getRowKey();
        
//        try {
//             int rowKeyIndex =
//             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
//             table.setRowIndex(rowKeyIndex);
//            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
//            AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
//            Number vSectionId = rowToCopyFrom.getSectionId();
//            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
//            String Schedulesdate=df.format(columnDate);
//          schedulerStaffId=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
//    
//            this.getCurrrentSchedulerStaffRow(schedulerStaffId);
//        RichPopup.PopupHints hints = new RichPopup.PopupHints();
//        this.getSchlrPopPresenter().show(hints);
//           
//        
//        }
//        catch(Exception e){
//          e.printStackTrace();
//            FacesMessage Message = new FacesMessage(e.getMessage());   
//                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                  FacesContext fc = FacesContext.getCurrentInstance();   
//                  fc.addMessage(null, Message);  
//        }
//        finally {
//           table.setRowKey(oldKey);
//        }  
    //
    }
    
    
    
    public void CutSchedulePresenter(ClientEvent ce){
       
       // this.setSaveUpdate_text("Update");
        
        this.setCutCopyC("Cut");
      
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTable table = this.getT3();
        Object oldKey = table.getRowKey();
        
        try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
            AsSchedulerSectionsVORowImpl rowToCopyFrom = (AsSchedulerSectionsVORowImpl) rowBinding.getRow();
            Number vSectionId = rowToCopyFrom.getSectionId();
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
            
          CutSchedulerStaffIdPresenter=this.getCurrentSchedulerStaffIdPresenter(vSectionId, Schedulesdate);
                 
                 // from schedulerstaffid get the scheduler staff data and chekc if there is bulletin get scheduler staff bulletin
                 SchedulerAMImpl sc=this.getApplicationModule();
                               AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
                              staff.setWhereClauseParams(null);
                              staff.setWhereClause(null);
                              staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
                              staff.setWhereClauseParams(new Object[] { CutSchedulerStaffIdPresenter });
                              staff.executeQuery();
                               AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();
       /// Fill Cut Array list with data of Scheduler Staff
            CutCList.clear();
            CutCList.add(r.getPersonId());
           CutCList.add(r.getShiftId());
            CutCList.add(r.getWorkGroupId());
            CutCList.add(r.getNotes());
            // check if scheduler staff has bulletin  if yes fill the list of bulletins with bulletins needed 
            
            CutBulletins(CutSchedulerStaffIdPresenter);

            // Delete Current Scheduler Staff and Refrsh the view 
            
      
            OperationBinding oper = ADFUtils.findOperation("deleteSchdlr");
            oper.getParamsMap().put("schedulerStaffId", CutSchedulerStaffIdPresenter);
          
       
           oper.getParamsMap().put("Type", "Cut");
           oper.getParamsMap().put("PersonID", this.getLoggedPersonId());
            oper.execute();
            this.bindPresentersShifts();
            
            
        
       }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
          table.setRowKey(oldKey);
          
        }  
     
    }

    public void CutScheduleNewsRoom(ClientEvent ce){
       this.setCutCopyA("Cut");
        SchedulerAMImpl sc=this.getApplicationModule();
        java.util.Date columnDate = (java.util.Date) ce.getParameters().get("column");    
        RichTreeTable table = this.getTt3();
        Object oldKey = table.getRowKey();
        
       try {
             int rowKeyIndex =
             ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
             table.setRowIndex(rowKeyIndex);
            Number[]currentrow=  getGurrentShiftAttbiuteRowData();
            Number vFkShiftAttributeId=currentrow[0];
    
            DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
            String Schedulesdate=df.format(columnDate);
          CutSchedulerStaffIdNewsRoom=this.getCurrentSchedulerStaffIdNewsRoom(vFkShiftAttributeId, Schedulesdate);
           // from schedulerstaffid get the scheduler staff data and chekc if there is bulletin get scheduler staff bulletin
        
                  AsSchedulerStaffVOImpl staff=sc.getAsSchedulerStaffVO();
                  staff.setWhereClauseParams(null);
                  staff.setWhereClause(null);
                  staff.setWhereClause("SCHEDULER_STAFF_ID=:1");
                  staff.setWhereClauseParams(new Object[] { CutSchedulerStaffIdNewsRoom });
                  staff.executeQuery();
                  AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)sc.getAsSchedulerStaffVO().first();

           /// Fill Cut Array list with data of Scheduler Staff
         
           CutAList.clear();
           CutAList.add(r.getPersonId());
           CutAList.add(r.getWorkGroupId());
           CutAList.add(r.getNotes());
           Object fkreplacedby=r.getFkReplacedById();
         Object fkwkreplacedby=r.getFkWkReplacedId();
           Object ShiftAttributeName =r.getShiftAttributeName();
           Object replacetpye=r.getReplaceType();
           if (fkreplacedby!=null)
           { CutAList.add(r.getFkReplacedById());  } 
           else {CutAList.add("0");}
            if (replacetpye!=null)
            { CutAList.add(r.getReplaceType());  } 
            else {CutAList.add("0");}
            if (fkwkreplacedby!=null)
            { CutAList.add(r.getFkWkReplacedId());  } 
            else {CutAList.add("0");}
            CutAList.add(r.getStartTimeHours());
            CutAList.add(r.getStartTimeMinutes());
            CutAList.add(r.getEndTimeHours());
            CutAList.add(r.getEndTimeMinutes());
            CutAList.add(r.getEmailNotification());
            CutAList.add(r.getSmsNotifiction());
           // For Tracking History
           CutAList.add(columnDate);
           CutAList.add(r.getShiftId());
           CutAList.add(r.getShiftName());
           CutAList.add(r.getFkSchdlrShiftAttributeId());
           if(ShiftAttributeName!=null)
           { CutAList.add(r.getShiftAttributeName());}
           else
           {CutAList.add("0");}
           ////end columns of Tracking History //
           // check if scheduler staff has bulletin  if yes fill the list of bulletins with bulletins needed
           
           CutBulletins(CutSchedulerStaffIdNewsRoom);
           
            OperationBinding oper = ADFUtils.findOperation("deleteSchdlr");
            oper.getParamsMap().put("schedulerStaffId", CutSchedulerStaffIdNewsRoom);
          
          
            oper.getParamsMap().put("Type", "Cut");
            oper.getParamsMap().put("PersonID", this.getLoggedPersonId());
            oper.execute();
//           
//            FacesMessage Message = new FacesMessage( CutSchedulerStaffIdNewsRoom.toString());   
//                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                  FacesContext fc = FacesContext.getCurrentInstance();   
//                  fc.addMessage(null, Message);  
            sc.getAsSchedulerShiftsVO().executeQuery();
            AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
            adffctx.addPartialTarget(table);
           
           
           
        }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage("Error");   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
           this.hideEditNewsRoomPopup();
        
            // Delete Current Scheduler Staff and Refrsh the view 
               } 
     
        
       
        
      
    }

    public void CutBulletins(Number from_Scheduler_staff_id)
    {
        
    
            SchedulerAMImpl sc=this.getApplicationModule();
            AsSchedulerStaffBulletinsVOImpl vo = sc.getAsSchedulerStaffBulletinsVO1();
            vo.setWhereClauseParams(null);
            vo.setWhereClause(null);
             vo.setWhereClause("SCHEDULER_STAFF_ID=:1 ");
            vo.setWhereClauseParams(new Object[] { from_Scheduler_staff_id});
             
            vo.executeQuery();
            int count=vo.getRowCount();
           
            if (count>0) {
                CutABCBulletins.clear();
                    for(int i=1;i<=count;i++)
                
                {
                        AsSchedulerStaffBulletinsVORowImpl r=( AsSchedulerStaffBulletinsVORowImpl)vo.next();  

                        CutABCBulletins.add(r.getBulletinId())  ;       
                    }
                 
            }
            else 
            
            CutABCBulletins.clear();
    
        }
    
    public void createSectionRowOrder(ClientEvent ce){

        int id=       ((Double)ce.getParameters().get("column")).intValue();
         Number SectionId =  new oracle.jbo.domain.Number(id);    
         RichTable table = this.getT3();
         Object oldKey = table.getRowKey();
         
         try {
              int rowKeyIndex =
              ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
              table.setRowIndex(rowKeyIndex);


             OperationBinding oper1 = ADFUtils.findOperation("createSectionRowOrder");
                oper1.execute();
             
          
          
      
     
         
         }
         catch(Exception e){
           e.printStackTrace();
             FacesMessage Message = new FacesMessage(e.getMessage());   
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                   FacesContext fc = FacesContext.getCurrentInstance();   
                   fc.addMessage(null, Message);  
         }
         finally {
            table.setRowKey(oldKey);
         }  
        
    }
    
    
    
    
    public void setTemplatepopup(RichPopup templatepopup) {
        this.templatepopup = templatepopup;
    }

    public RichPopup getTemplatepopup() {
        return templatepopup;
    }

    public void setTemplateDate(String templateDate) {
        this.templateDate = templateDate;
    }

    public String getTemplateDate() {
        return templateDate;
    }

    public void setRepeatC(RichPopup RepeatC) {
        this.RepeatC = RepeatC;
    }

    public RichPopup getRepeatC() {
        return RepeatC;
    }

    public void setDeleteC(RichPopup DeleteC) {
        this.DeleteC = DeleteC;
    }

    public RichPopup getDeleteC() {
        return DeleteC;
    }

    public void setDeleteDateStartB(java.util.Date DeleteDateStartB) {
        this.DeleteDateStartB = DeleteDateStartB;
    }

    public java.util.Date getDeleteDateStartB() {
        return DeleteDateStartB;
    }

    public void setDeleteDateEndB(java.util.Date DeleteDateEndB) {
        this.DeleteDateEndB = DeleteDateEndB;
    }

    public java.util.Date getDeleteDateEndB() {
        return DeleteDateEndB;
    }

    public void setDeleteDateMaxB(java.util.Date DeleteDateMaxB) {
        this.DeleteDateMaxB = DeleteDateMaxB;
    }

    public java.util.Date getDeleteDateMaxB() {
        return DeleteDateMaxB;
    }

    public void setDeleteDateStartC(java.util.Date DeleteDateStartC) {
        this.DeleteDateStartC = DeleteDateStartC;
    }

    public java.util.Date getDeleteDateStartC() {
        return DeleteDateStartC;
    }

    public void setDeleteDateEndC(java.util.Date DeleteDateEndC) {
        this.DeleteDateEndC = DeleteDateEndC;
    }

    public java.util.Date getDeleteDateEndC() {
        return DeleteDateEndC;
    }

    public void setDeleteDateMaxC(java.util.Date DeleteDateMaxC) {
        this.DeleteDateMaxC = DeleteDateMaxC;
    }

    public java.util.Date getDeleteDateMaxC() {
        return DeleteDateMaxC;
    }

    public void setCutCopyA(String cutCopyA) {
        this.cutCopyA = cutCopyA;
    }

    public String getCutCopyA() {
        return cutCopyA;
    }

    public void setCutCopyB(String cutCopyB) {
        this.cutCopyB = cutCopyB;
    }

    public String getCutCopyB() {
        return cutCopyB;
    }

    public void setCutCopyC(String cutCopyC) {
        this.cutCopyC = cutCopyC;
    }

    public String getCutCopyC() {
        return cutCopyC;
    }

    public void setCutSchedulerStaffIdPresenter(Number CutSchedulerStaffIdPresenter) {
        this.CutSchedulerStaffIdPresenter = CutSchedulerStaffIdPresenter;
    }

    public Number getCutSchedulerStaffIdPresenter() {
        return CutSchedulerStaffIdPresenter;
    }
    
    //--------------- Send Sms /Email //
    public String commit_action() {
        BindingContainer bindings = getBindings();
     OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        
     
         
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }
    public String sendemployeeSmsPublish() {
        // Add event code here...
        // First Commit Changes//
         commit_action();
      SchedulerAMImpl am =  this.getApplicationModule();
      DCIteratorBinding iter = ADFUtils.findIterator("AsSchedulersVOIterator");
      AsSchedulersVORowImpl currRow = (AsSchedulersVORowImpl) iter.getCurrentRow();
       int count= currRow.getAsSchedulerStaffEmailsmsVO().getRowCount();
       Number PersonID ;
       String mobileNum="";
        String     rmsg="";
        StringBuilder msg=new StringBuilder();
        String smsmessage="";
        
        msg.append("Scheduler ");
            msg.append((String) ADFUtils.getBoundAttributeValue("SchedulerTitle"));
            msg.append(" has been published. for more details  check AA Scheduler Application . ");
        smsmessage =msg.toString();
        RowSetIterator rs = (RowSetIterator) currRow.getAsSchedulerStaffEmailsmsVO();
        rs.reset();
         Row rr=null;
        while(rs.hasNext()) {
        rr= rs.next();
       
            
            PersonID = (Number) rr.getAttribute("PersonId");
            mobileNum=rr.getAttribute("PhoneNumber").toString();
           rmsg= sendSMSAction(mobileNum,smsmessage);
           insertsmsstatus(PersonID,mobileNum,rmsg) ;  
        }
        rs.closeRowSetIterator();
        
        
        /// Send to Editors //
        RowSetIterator rs2 = am.getAsEditorRoleVO1();
        rs2.reset();
         Row rr2=null;
        while(rs2.hasNext()) {
        rr2= rs2.next();
        
            
            PersonID = (Number) rr2.getAttribute("PersonId");
            if (rr2.getAttribute("PhoneNumber") !=null)
            {mobileNum=rr2.getAttribute("PhoneNumber").toString();}
           // else 
          //  { mobileNum="971509353481";}
           rmsg= sendSMSAction(mobileNum,smsmessage);
           insertsmsstatus(PersonID,mobileNum,rmsg) ;  
        }
        rs2.closeRowSetIterator();
        
        
        
        //// End Send to Editors ///
 
// int x=currRow.getAsSchedulerStaffEmailsmsVO().getRowCount();
//currRow.getPhoneNumber()+currRow.getEmployeeName()+currRow.getEmailAddress()
//       FacesMessage Message = new FacesMessage("Sms sent Successfully,Please Check SMS-Email monitoring for more details .");   
//       Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//       FacesContext fc = FacesContext.getCurrentInstance();   
//       fc.addMessage(null, Message);
        
        
        
        
        return null;
    }
    public String sendemployeeSms() {
        // Add event code here...
        // First Commit Changes//
         commit_action();
      SchedulerAMImpl am =  this.getApplicationModule();
      DCIteratorBinding iter = ADFUtils.findIterator("AsSchedulersVOIterator");
      AsSchedulersVORowImpl currRow = (AsSchedulersVORowImpl) iter.getCurrentRow();
       int count= currRow.getAsSchedulerStaffEmailsmsVO().getRowCount();
       Number PersonID ;
       String mobileNum="";
        String     rmsg="";
        StringBuilder msg=new StringBuilder();
        String smsmessage="";
        
        msg.append("Scheduler ");
            msg.append((String) ADFUtils.getBoundAttributeValue("SchedulerTitle"));
            msg.append(" has been published. for more details, check AA Scheduler Application . ");
        smsmessage =msg.toString();
        RowSetIterator rs = (RowSetIterator) currRow.getAsSchedulerStaffEmailsmsVO();
        rs.reset();
         Row rr=null;
        while(rs.hasNext()) {
        rr= rs.next();
       
            
            PersonID = (Number) rr.getAttribute("PersonId");
            mobileNum=rr.getAttribute("PhoneNumber").toString();
           rmsg= sendSMSAction(mobileNum,smsmessage);
           insertsmsstatus(PersonID,mobileNum,rmsg) ;  
        }
        rs.closeRowSetIterator();
        
        /// Send to Editors //
        RowSetIterator rs2 = am.getAsEditorRoleVO1();
        rs2.reset();
         Row rr2=null;
        while(rs2.hasNext()) {
        rr2= rs2.next();
        
            
            PersonID = (Number) rr2.getAttribute("PersonId");
            if (rr2.getAttribute("PhoneNumber") !=null)
            {mobileNum=rr2.getAttribute("PhoneNumber").toString();}
          //  else 
          //  { mobileNum="971509353481";}
           rmsg= sendSMSAction(mobileNum,smsmessage);
           insertsmsstatus(PersonID,mobileNum,rmsg) ;  
        }
        rs2.closeRowSetIterator();
        
        
        
        //// End Send to Editors ///
    
     int x=currRow.getAsSchedulerStaffEmailsmsVO().getRowCount();
   // currRow.getPhoneNumber()+currRow.getEmployeeName()+currRow.getEmailAddress()
           FacesMessage Message = new FacesMessage("Sms sent Successfully,Please Check SMS-Email monitoring for more details .");
           Message.setSeverity(FacesMessage.SEVERITY_INFO);
           FacesContext fc = FacesContext.getCurrentInstance();
           fc.addMessage(null, Message);
        
        
        
        
        return null;
    }
    public String getSmsUrl()
    {
            OperationBinding oper = ADFUtils.findOperation("getSmsUrl");
          
            oper.execute();
            String retValue = (String) oper.getResult();
            return retValue;
        
        }
    public String sendSMSAction(String mobilenum,String messagetext) {
       
             String mobile = mobilenum;
             String senderid = "MEN";
             String message = messagetext;

             URLConnection myURLConnection = null;
             URL myURL = null;
             BufferedReader reader = null;
          String encoded_message = URLEncoder.encode(message);
             //Send SMS API
             String mainUrl =getSmsUrl();
                 //"https://www.smsglobal.com/http-api.php?action=sendsms&user=bk6honho&password=samJfPVC&";
             //Prepare parameter string
             StringBuilder sbPostData = new StringBuilder(mainUrl);
             sbPostData.append("from=" + senderid);
             sbPostData.append("&to=" + mobile);
             sbPostData.append("&text=" + encoded_message);
              mainUrl = sbPostData.toString();
             System.out.println("URL to Send SMS-" + mainUrl);
             String messagestatus="";
             try {
                 //prepare connection
                 myURL = new URL(mainUrl);
                 myURLConnection = myURL.openConnection();
                 myURLConnection.connect();
                 reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
                 //reading response
                 String response;
                 while ((response = reader.readLine()) != null) {
                     //print response
                     messagestatus=response;
                     System.out.println(response);
                 }
                 System.out.println(response);
                 //finally close connection
                 reader.close();


             } catch (IOException e) {
                 e.printStackTrace();
             }
             
             
             

         
             
             return messagestatus ;
       
     }
    public  void insertsmsstatus(Number vpersonid,String vphone,String vstatusmessage)
    {
            OperationBinding oper = ADFUtils.findOperation("insertSmsMessageStatus");
            oper.getParamsMap().put("personid", vpersonid);
            oper.getParamsMap().put("phone", vphone);
            oper.getParamsMap().put("statusmessage", vstatusmessage);
            oper.execute();
            String retValue = (String) oper.getResult();
        
        
        
        }
    
    
    
    public String sendemail() {
        // Add event code here...
        
       // commit_action();
        SchedulerAMImpl am =  this.getApplicationModule();
        DCIteratorBinding iter = ADFUtils.findIterator("AsSchedulersVOIterator");
        AsSchedulersVORowImpl currRow = (AsSchedulersVORowImpl) iter.getCurrentRow();
        int count= currRow.getAsSchedulerStaffEmailsmsVO().getRowCount();
        String employeename ;
        String emailaddress="";
        String     rmsg="";
        StringBuilder msg=new StringBuilder();
        String emailmessage="";
  
        msg.append("Scheduler ");
           msg.append((String) ADFUtils.getBoundAttributeValue("SchedulerTitle"));
           msg.append(" has been published. for more details, check AA Scheduler Application . ");
        emailmessage =msg.toString();
        RowSetIterator rs = (RowSetIterator) currRow.getAsSchedulerStaffEmailsmsVO();
        rs.reset();
        Row rr=null;
        while(rs.hasNext()) {
        rr= rs.next();
                employeename = rr.getAttribute("EmployeeName").toString();
           emailaddress=rr.getAttribute("EmailAddress").toString();
            insertemail(emailaddress,employeename,emailmessage);
        }
        rs.closeRowSetIterator();
        
        // Second Send to Editors ///
    
        RowSetIterator rs2 = am.getAsEditorRoleVO1();
        rs2.reset();
        Row rr2=null;
        while(rs2.hasNext()) {
        rr2= rs2.next();
                employeename = rr2.getAttribute("EmployeeName").toString();
           emailaddress=rr2.getAttribute("EmailAddress").toString();
            insertemail(emailaddress,employeename,emailmessage);
        }
        rs2.closeRowSetIterator();
        
        
        
        // End Send to Editors //






                    FacesMessage Message = new FacesMessage("Emails sent Successfully,Please Check SMS-Email monitoring for more details .");
                                                                  Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                                                  FacesContext fc = FacesContext.getCurrentInstance();
                                                                  fc.addMessage(null, Message);
        
        
        return null;
    }

    public String sendemailPublish() {
        // Add event code here...
        
       // commit_action();
        SchedulerAMImpl am =  this.getApplicationModule();
        DCIteratorBinding iter = ADFUtils.findIterator("AsSchedulersVOIterator");
        AsSchedulersVORowImpl currRow = (AsSchedulersVORowImpl) iter.getCurrentRow();
        int count= currRow.getAsSchedulerStaffEmailsmsVO().getRowCount();
        String employeename ;
        String emailaddress="";
        String     rmsg="";
        StringBuilder msg=new StringBuilder();
        String emailmessage="";
        RowSetIterator rs = (RowSetIterator) currRow.getAsSchedulerStaffEmailsmsVO();
        msg.append("Scheduler");
        msg.append((String) ADFUtils.getBoundAttributeValue("SchedulerTitle"));
           msg.append(" has been published. for more details check AA Scheduler Application . ");
        emailmessage =msg.toString();
        rs.reset();
        Row rr=null;
        while(rs.hasNext()) {
        rr= rs.next();
        
           
           employeename = rr.getAttribute("EmployeeName").toString();
           emailaddress=rr.getAttribute("EmailAddress").toString();
            insertemail(emailaddress,employeename,emailmessage);
        }
        rs.closeRowSetIterator();
        
        
        
        // Second Send to Editors ///
        
        RowSetIterator rs2 = am.getAsEditorRoleVO1();
        rs2.reset();
        Row rr2=null;
        while(rs2.hasNext()) {
        rr2= rs2.next();
                employeename = rr2.getAttribute("EmployeeName").toString();
           emailaddress=rr2.getAttribute("EmailAddress").toString();
            insertemail(emailaddress,employeename,emailmessage);
        }
        rs2.closeRowSetIterator();
        
        
        
        // End Send to Editors //

    //                    FacesMessage Message = new FacesMessage("Emails sent Successfully,Please Check SMS-Email monitoring for more details .");
    //                                                                  Message.setSeverity(FacesMessage.SEVERITY_INFO);
    //                                                                  FacesContext fc = FacesContext.getCurrentInstance();
    //                                                                  fc.addMessage(null, Message);
    //
        
        return null;
    }
    
    
    public  void insertemail(String vemail,String vemployeename,String vmessage)
    {
            OperationBinding oper = ADFUtils.findOperation("sendemail");
            oper.getParamsMap().put("toemail", vemail);
            oper.getParamsMap().put("employeename", vemployeename);
            oper.getParamsMap().put("emailmessage", vmessage);
            oper.execute();
            String retValue = (String) oper.getResult();
        
        
        
        }
    
    public void onClickSaveSchdlrBtn(ActionEvent actionEvent) {
        // Add event code here...
      
              
        ///Add Shiftworkgroups for selected workgroup category
        ADFUtils.findOperation("Commit").execute();
        BindingContainer bindings = getBindings();
        Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
             Number scId =(Number)asSchedulersRow.getAttribute("SchedulerId");
        Number fkcategoryid =(Number)asSchedulersRow.getAttribute("FkWkCategoryId");   

        OperationBinding operationBinding = bindings.getOperationBinding("createSchedulerShiftWorkGroup");
        operationBinding.getParamsMap().put("schedulerId", scId);
        operationBinding.getParamsMap().put("fkCategoryId", fkcategoryid);
        String result =(String) operationBinding.execute();
     
        if (!operationBinding.getErrors().isEmpty()) {
           
        }
        else
        {
            

            
                if(result.equals("0"))
                {
                    
                 FacesMessage fm=new FacesMessage("Changes Saved Successfully");
                 fm.setSeverity(FacesMessage.SEVERITY_INFO);
                 FacesContext fc = FacesContext.getCurrentInstance(); 
                 fc.addMessage(null, fm);
                }
            else
                
                {
                    
                 FacesMessage fm=new FacesMessage("one or more Shifts containing Data in Schedule Detail ,please remove it ");
                 fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                 FacesContext fc = FacesContext.getCurrentInstance(); 
                 fc.addMessage(null, fm);
                }
            
            
            }
        
        Refresh_SchulerShifts();
      
    }
    // Added By Heba Jan-2018----
    public String[] getCurrentSelectedShiftWorkgroupName(Number FkShiftId,Number FkwkId)
    {
        
        String [] ShiftWorkgroup=new String [2];
         //Number Scstaffid=new Number();
          //  Number schedulerId = (Number) ADFUtils.getBoundAttributeValue("SchedulerId");
            SchedulerAMImpl sc=this.getApplicationModule();
            AsSchdulerCurrentShiftworkrgroupvoImpl staff=sc.getAsSchdulerCurrentShiftworkrgroupvo1();
            staff.setWhereClauseParams(null);
            staff.setWhereClause(null);
            staff.setWhereClause("SHIFT_ID=:1 and WORK_GROUP_ID=:2");
            staff.setWhereClauseParams(new Object[] { FkShiftId, FkwkId });
            staff.executeQuery();
            if (staff.first() != null) {
                 AsSchdulerCurrentShiftworkrgroupvoRowImpl row = (AsSchdulerCurrentShiftworkrgroupvoRowImpl) staff.first();
                ShiftWorkgroup[0]=row.getShiftName()+"-"+row.getWorkGroupName();
                ShiftWorkgroup[1]=row.getWorkGroupName();
            }
              
        return ShiftWorkgroup;
        
        }

    public void setSaveUpdateNotify_text(String SaveUpdateNotify_text) {
        this.SaveUpdateNotify_text = SaveUpdateNotify_text;
    }

    public String getSaveUpdateNotify_text() {
        return SaveUpdateNotify_text;
    }

    public void setWorkgroupCurrentId(String WorkgroupCurrentId) {
        this.WorkgroupCurrentId = WorkgroupCurrentId;
    }

    public String getWorkgroupCurrentId() {
        return WorkgroupCurrentId;
    }
    
    public String check_employee_replace_leave ()
    
    {       
    Row asSchedulersRow = ADFUtils.findIterator("AsSchedulerStaffVOIterator").getCurrentRow();
    
        
    Date StartDate =   (Date) asSchedulersRow.getAttribute("SchedularStartDate");
    Number  vPersonId=  (Number)ADFUtils.getBoundAttributeValue("PersonId");
    String  vReplaceType=  ADFUtils.getBoundAttributeValue("ReplaceType").toString();
    Number  vFkReplacedById=new Number(0);
     if(!vReplaceType.equals("No Replacement"))
     {
              
     Object obj=ADFUtils.getBoundAttributeValue("FkReplacedById");
     if(obj!=null)
      vFkReplacedById=  (Number)obj;
     }
     
      
     //return "No Leaves";
     
//     FacesMessage Message = new FacesMessage("old person id :"+currentPersonId+ "new person id "+vPersonId);
//     Message.setSeverity(FacesMessage.SEVERITY_ERROR);
//     FacesContext fc = FacesContext.getCurrentInstance();
//     fc.addMessage(null, Message);
     int vcurrentPersonId=currentPersonId.intValue();
     int vvPersonId =vPersonId.intValue();
     if( vcurrentPersonId==vvPersonId)
   
        return check_employee_replace_leave_db(new Number(0),vFkReplacedById,StartDate);
     else 
            return check_employee_replace_leave_db(vPersonId,vFkReplacedById,StartDate);
     
                
        }
    
    
    private String check_employee_replace_leave_db(Number vpersonId, Number vreplceid ,Date vstdate) {
        OperationBinding oper = ADFUtils.findOperation("check_leave_pending_validation");
        oper.getParamsMap().put("personId", vpersonId);
        oper.getParamsMap().put("replceid", vreplceid);
        oper.getParamsMap().put("vstartdate", vstdate);
      

        oper.execute();
        String retValue = (String) oper.getResult();
        return retValue;
            //"No Leaves";
        // commmented as requested to diable leave validation in test instance //
            //retValue;
    }


    public String add_new_scheduler_shift() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }

        return null;
    }

    public String Refresh_SchulerShifts() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Execute");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }
    public String Refresh_Schuler() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Execute");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }

    public void ChangeSchedluerShiftStatus(ActionEvent actionEvent) {
        BindingContainer bindings = getBindings();
        Row asSchedulersRow = ADFUtils.findIterator("AsAllSchedulerShiftsVo1Iterator").getCurrentRow();
             String st =(String)asSchedulersRow.getAttribute("Status");
   if (st.equals("Displayed"))  
   
       asSchedulersRow.setAttribute("Status","Hidden");
           
           else 
        asSchedulersRow.setAttribute("Status","Displayed");
      
                    ADFUtils.findOperation("Commit").execute();
                 FacesMessage fm=new FacesMessage("Status Changed Successfully");
                 fm.setSeverity(FacesMessage.SEVERITY_INFO);
                 FacesContext fc = FacesContext.getCurrentInstance(); 
                 fc.addMessage(null, fm);
        
        
        Refresh_SchulerShifts();
        
    }
    public void ChangeSchedluertatus(ActionEvent actionEvent) {
        BindingContainer bindings = getBindings();
        Row asSchedulersRow = ADFUtils.findIterator("AsSchedulersVOIterator").getCurrentRow();
             String st =(String)asSchedulersRow.getAttribute("Status");
    if (st.equals("Displayed"))
    
       asSchedulersRow.setAttribute("Status","Hidden");
           
           else 
        asSchedulersRow.setAttribute("Status","Displayed");
      
                    ADFUtils.findOperation("Commit").execute();
                 FacesMessage fm=new FacesMessage("Status Changed Successfully");
                 fm.setSeverity(FacesMessage.SEVERITY_INFO);
                 FacesContext fc = FacesContext.getCurrentInstance(); 
                 fc.addMessage(null, fm);
        
        
        Refresh_Schuler();
        
    }
    public void showSchedulers()
    {
        
            OperationBinding oper = ADFUtils.findOperation("showHideScheduler");
             oper.getParamsMap().put("Status", "Displayed");

             oper.execute();
            
             if (oper.getErrors().isEmpty())
             {
                     Refresh_Schuler();
                     FacesMessage fm=new FacesMessage("Status Changed Successfully");
                     fm.setSeverity(FacesMessage.SEVERITY_INFO);
                     FacesContext fc = FacesContext.getCurrentInstance(); 
                     fc.addMessage(null, fm);
                 
                 }
             
             else
             
             {
                 
                     FacesMessage fm=new FacesMessage("Error:Contact System Adminstrator");
                     fm.setSeverity(FacesMessage.SEVERITY_INFO);
                     FacesContext fc = FacesContext.getCurrentInstance(); 
                     fc.addMessage(null, fm);
                 }
        
        }
    public void hideSchedulers()
    {
        
            OperationBinding oper = ADFUtils.findOperation("showHideScheduler");
             oper.getParamsMap().put("Status", "Hidden");

             oper.execute();
            
             if (oper.getErrors().isEmpty())
             {
                     Refresh_Schuler();
                     FacesMessage fm=new FacesMessage("Status Changed Successfully");
                     fm.setSeverity(FacesMessage.SEVERITY_INFO);
                     FacesContext fc = FacesContext.getCurrentInstance(); 
                     fc.addMessage(null, fm);
                 
                 }
             
             else
             
             {
                 
                     FacesMessage fm=new FacesMessage("Error:Contact System Adminstrator");
                     fm.setSeverity(FacesMessage.SEVERITY_INFO);
                     FacesContext fc = FacesContext.getCurrentInstance(); 
                     fc.addMessage(null, fm);
                 }
        
        }
    
    
    
    //// View History  (Scheduler Tracking ) Heba /July 2019
    public void ViewHistoryNewsroom(ClientEvent ce){
        
//        this.setSaveUpdate_text("Save");
//        this.setSaveUpdateNotify_text("Save & Notify");
      java.util.Date columnDate = (java.util.Date)ce.getParameters().get("column"); 
       RichTreeTable table = this.getTt3();
        Object oldKey = table.getRowKey();
        currentPersonId=new Number(0);
        
       try {
            int rowKeyIndex =0;
            // ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
       ViewHistoryTableNewsRoom(columnDate);  
        
      }
        catch(Exception e){
          e.printStackTrace();
            FacesMessage Message = new FacesMessage(e.getMessage());   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                  fc.addMessage(null, Message);  
        }
        finally {
           table.setRowKey(oldKey);
            this.getViewhistoryPopNewsRoom().hide();
        }  
    }
    private void ViewHistoryTableNewsRoom(java.util.Date columndate){
    
       RichTreeTable table = this.getTt3();
    
           DateFormat df = new SimpleDateFormat("d-MMM-yyyy");
         String StartDate=df.format(columndate);
         Number[] currentrow = getGurrentShiftAttbiuteRowData();
         Number vFkShiftAttributeId = currentrow[0];
       OperationBinding oper = ADFUtils.findOperation("ViewTrackHistory");
       oper.getParamsMap().put("pstart_Date",StartDate);
       oper.getParamsMap().put("ShiftattributeId",vFkShiftAttributeId);
         //this.getEmpFullName());
       oper.execute();
     
         RichPopup.PopupHints hints = new RichPopup.PopupHints();
         this.getViewhistoryPopNewsRoom().show(hints);
         

         AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
       adffctx.addPartialTarget(table);
     }

    public void setViewhistoryPopNewsRoom(RichPopup viewhistoryPopNewsRoom) {
        this.viewhistoryPopNewsRoom = viewhistoryPopNewsRoom;
    }

    public RichPopup getViewhistoryPopNewsRoom() {
        return viewhistoryPopNewsRoom;
    }
    
    public void pasteTrackingSchedulerStaff(AsSchedulerStaffVORowImpl r, java.util.Date columndate , Number shiftId,Number FkSchdlrShiftAttributeId,Number SchedulerStaffId,Number PersonId) {
       
     // AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)this.getAsSchedulerStaffVO().first();
        if(SchedulerStaffId.intValue()!=0)
        {
    AsSchedulerStaffHistoryVoImpl vo=     this.getApplicationModule().getAsSchedulerStaffHistoryVo1();
    AsSchedulerStaffHistoryVoRowImpl h = (AsSchedulerStaffHistoryVoRowImpl) vo.createRow();
    h.setSchedulerId(r.getSchedulerId());
    h.setSchedulerStaffId(SchedulerStaffId);
    h.setPersonId(r.getPersonId());
    h.setSchedularStartDate(new oracle.jbo.domain.Date(DateConversion.getSqlDateFromUtilDate(columndate)));
    h.setSchedularEndDate(new oracle.jbo.domain.Date(DateConversion.getSqlDateFromUtilDate(columndate)));
    h.setStartTimeHours(r.getStartTimeHours());
    h.setEndTimeHours(r.getEndTimeHours());
    h.setStartTimeMinutes(r.getStartTimeMinutes());
    h.setEndTimeMinutes(r.getEndTimeMinutes());
    //we get created by from session 
    h.setCreatedBy(PersonId);
        //creation date setted on entity level through wizard //
        // h.setLastUpdateDate(r.getLastUpdateDate());
        // h.setCreationDate(r.getCreationDate());
  //  h.setLastUpdatedBy(r.getLastUpdatedBy());
   // h.setLastUpdateLogin(r.getLastUpdateLogin());
    h.setRecurringFlag(r.getRecurringFlag());
    h.setBulletinFlag(r.getBulletinFlag());
    h.setParentSchedulerStaffId(r.getParentSchedulerStaffId());
    h.setWorkGroupId(r.getWorkGroupId());
    h.setShiftId(shiftId);
    h.setNotes(r.getNotes());
    h.setFkSchdlrShiftAttributeId(FkSchdlrShiftAttributeId);
    h.setFkSchedulerSectionId(r.getFkSchedulerSectionId());
    h.setFkReplacedById(r.getFkReplacedById());
    h.setReplaceType(r.getReplaceType());
    h.setEmailNotification(r.getEmailNotification());
    h.setSmsNotifiction(r.getSmsNotifiction());
    h.setFkWkReplacedId(r.getFkWkReplacedId() );
    /// Source colums
    h.setSourceDate(r.getSchedularStartDate());
    h.setSourceShiftId(r.getShiftId());
    h.setSourceShift(r.getShiftName());
    h.setSourceShiftAttributeId(r.getFkSchdlrShiftAttributeId());
     h.setSourceShiftAttribute(r.getShiftAttributeName());
     // End Source columns
    h.setActionType("Paste");
        ///Tracking history Paste of Copy //
        //int count=CutABCBulletins.size();
        
        if( r.getAsSchedulerStaffBulletinsVO().getRowCount()>0) {
        
                      AsSchdlrStaffBulletinsHistVoImpl bulletinvo=    this.getApplicationModule().getAsSchdlrStaffBulletinsHistVo1();

             RowSet rs = (RowSet) r.getAttribute("AsSchedulerStaffBulletinsVO");
        
              //Iterate over child viewObject rows for corresponding master record
              while (rs.hasNext()) {
              Row rb = rs.next();
                  AsSchdlrStaffBulletinsHistVoRowImpl bulletin= (AsSchdlrStaffBulletinsHistVoRowImpl) bulletinvo.createRow();
                  bulletin.setCreatedBy(PersonId);
                  bulletin.setSchedulerStaffHistoryId(h.getHistoryId());
                  bulletin.setSchedulerStaffId(h.getSchedulerStaffId());
                  bulletin.setBulletinId((Number)rb.getAttribute("BulletinId"));
                  bulletin.setSchedulerStaffBulletinId((Number)rb.getAttribute("SchedulerStaffBulletinId"));
            //  System.out.println(" Bulletin : " + rb.getAttribute("BulletinId") + " Scheduler Staff id  " +
             // rb.getAttribute("SchedulerStaffId"));
             
        }
    
        }
        
        
        }

    }
    
    
    
   
        public void pasteTrackingCutSchedulerStaff(AsSchedulerStaffVORowImpl r,Number PersonId) {
           
           
           if(r.getSchedulerStaffId().intValue()!=0)
           {
     // AsSchedulerStaffVORowImpl r=(AsSchedulerStaffVORowImpl)this.getAsSchedulerStaffVO().first();
    AsSchedulerStaffHistoryVoImpl vo=     this.getApplicationModule().getAsSchedulerStaffHistoryVo1();
    AsSchedulerStaffHistoryVoRowImpl h = (AsSchedulerStaffHistoryVoRowImpl) vo.createRow();
    h.setSchedulerId(r.getSchedulerId());
    h.setSchedulerStaffId(r.getSchedulerStaffId());
    h.setPersonId(r.getPersonId());
    h.setSchedularStartDate(r.getSchedularStartDate());
    h.setSchedularEndDate(r.getSchedularStartDate());
    h.setStartTimeHours(r.getStartTimeHours());
    h.setEndTimeHours(r.getEndTimeHours());
    h.setStartTimeMinutes(r.getStartTimeMinutes());
    h.setEndTimeMinutes(r.getEndTimeMinutes());
    //we get created by from session 
    h.setCreatedBy(PersonId);
       h.setRecurringFlag(r.getRecurringFlag());
    h.setBulletinFlag(r.getBulletinFlag());
    h.setParentSchedulerStaffId(r.getParentSchedulerStaffId());
    h.setWorkGroupId(r.getWorkGroupId());
    h.setShiftId(r.getShiftId());
    h.setNotes(r.getNotes());
    h.setFkSchdlrShiftAttributeId(r.getFkSchdlrShiftAttributeId());
    h.setFkSchedulerSectionId(r.getFkSchedulerSectionId());
    h.setFkReplacedById(r.getFkReplacedById());
    h.setReplaceType(r.getReplaceType());
    h.setEmailNotification(r.getEmailNotification());
    h.setSmsNotifiction(r.getSmsNotifiction());
    h.setFkWkReplacedId(r.getFkWkReplacedId() );
    /// Source colums
  
    java.util.Date sourcecolumndate= (java.util.Date)CutAList.get(12);
      h.setSourceDate(new oracle.jbo.domain.Date(DateConversion.getSqlDateFromUtilDate(sourcecolumndate)));
    h.setSourceShiftId((oracle.jbo.domain.Number)CutAList.get(13));
    h.setSourceShift((String)CutAList.get(14));
    h.setSourceShiftAttributeId((oracle.jbo.domain.Number)CutAList.get(15));
     h.setSourceShiftAttribute((String)CutAList.get(16));
     // End Source columns
    h.setActionType("Paste");
    
    ///Tracking history Paste of Cut //
    //int count=CutABCBulletins.size();
    
    if( r.getAsSchedulerStaffBulletinsVO().getRowCount()>0) {
 
                  AsSchdlrStaffBulletinsHistVoImpl bulletinvo=    this.getApplicationModule().getAsSchdlrStaffBulletinsHistVo1();

         RowSet rs = (RowSet) r.getAttribute("AsSchedulerStaffBulletinsVO");
  
          //Iterate over child viewObject rows for corresponding master record
          while (rs.hasNext()) {
          Row rb = rs.next();
              AsSchdlrStaffBulletinsHistVoRowImpl bulletin= (AsSchdlrStaffBulletinsHistVoRowImpl) bulletinvo.createRow();
              bulletin.setCreatedBy(PersonId);
              bulletin.setSchedulerStaffHistoryId(h.getHistoryId());
              bulletin.setSchedulerStaffId(h.getSchedulerStaffId());
              bulletin.setBulletinId((Number)rb.getAttribute("BulletinId"));
              bulletin.setSchedulerStaffBulletinId((Number)rb.getAttribute("SchedulerStaffBulletinId"));
      
         
    }
    
    }
           }

    }
    public void TrackHistory(Number SchedulerStaffId,String ActionType ,String Source )
    {
        
        
            OperationBinding oper1 = ADFUtils.findOperation("createTrackingSchedulerStaff");
            oper1.getParamsMap().put("SchedulerStaffId",SchedulerStaffId);
            oper1.getParamsMap().put("ActionType",ActionType);
            oper1.getParamsMap().put("Source",Source);
           // oper1.getParamsMap().put("wkId",WorkgroupCurrentId);
            oper1.execute();
        
   
        
        }
    
    public Number getLoggedPersonId()
    {
        
            Map sessionscope = ADFContext.getCurrent().getSessionScope();
            Number personId = (Number) sessionscope.get("AAPersonId"); 
          return personId;
        
        }
    
//    private class StaffBulletin
//    {
//        
//        
//        
//        
//        
//        
//        
//        }


    public void setP7(RichPopup p7) {
        this.p7 = p7;
    }

    public RichPopup getP7() {
        return p7;
    }
}

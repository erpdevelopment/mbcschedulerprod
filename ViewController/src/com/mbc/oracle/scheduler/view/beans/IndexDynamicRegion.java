package com.mbc.oracle.scheduler.view.beans;

import java.io.Serializable;

import oracle.adf.controller.TaskFlowId;

public class IndexDynamicRegion implements Serializable {
    private String homeTaskFlowId = "/WEB-INF/home/home-BTF.xml#home-BTF";
    private String profileTaskFlowId = "/WEB-INF/user/profile-BTF.xml#profile-BTF";
    private String taskFlowId = "/WEB-INF/home/home-BTF.xml#home-BTF";

    public IndexDynamicRegion() {
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String home() {
        // Add event code here...
        this.setDynamicTaskFlowId(homeTaskFlowId);
        return null;
    }

    public String statistics() {
        // Add event code here...
        //this.setDynamicTaskFlowId(homeTaskFlowId);
        return null;
    }

    public String schedulers() {
        // Add event code here...
        //this.setDynamicTaskFlowId(homeTaskFlowId);
        return null;
    }

    public String administration() {
        // Add event code here...
        //this.setDynamicTaskFlowId(homeTaskFlowId);
        return null;
    }

    public String profile() {
        // Add event code here...
        this.setDynamicTaskFlowId(profileTaskFlowId);
        return null;
    }
}

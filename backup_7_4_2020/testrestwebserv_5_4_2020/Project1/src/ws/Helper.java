package ws;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

import java.net.URLDecoder;

import java.sql.*;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;


public class Helper {
    public Helper() {
        super();
    }

    private static Context ctx = null;
    private static DataSource ds = null;
   



    public static Connection getOracleConnection() throws Exception {
        Connection conn = null;
        try {
         
            if (ctx == null) {
                ctx = new InitialContext();
            }
            if (ds == null) {
                ds = (DataSource)ctx.lookup("jdbc/mobWS");  
                //jdbc/mobWPatch
                //jdbc/mobWS
               // mobWUPG2
                //mobWSD2
            }
            conn = ds.getConnection();

        } catch (SQLException sqle) {
            // TODO: Add catch code
            sqle.printStackTrace();
        } catch (NamingException ne) {
            // TODO: Add catch code
            ne.printStackTrace();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        return conn;
        
        
    }
    
    public  static Response  StringtoReponse(String jsonString) {
       String message = "\"message\":\"2\"";
        Response response = null;
        ResponseBuilder builder = Response.ok("[{\"value\":\""+jsonString+"\","+message +"}]");
        response = builder.build();
        return response;
            
        


    }
    
    public  static Response  StringtoReponse(String jsonString ,String messageN) {
       String message = "\"message\":\""+messageN+"\"";
        Response response = null;
        ResponseBuilder builder = Response.ok("[{\"value\":\""+jsonString+"\","+message +"}]");
        response = builder.build();
        return response;
            
        


    }
    
    public  static Response  userDtaReponse(String personId,String businessGroupid, String userId,String assignmentId, String notificationNO ,String messageN) {
       String message = "\"message\":\""+messageN+"\"";
        Response response = null;
        String responseString=null;
        responseString="[{";
        responseString=responseString+"\"person_id\":\""+personId+"\",";
        responseString=responseString+"\"business_group_id\":\""+businessGroupid+"\",";
        responseString=responseString+"\"user_id\":\""+userId+"\",";
        responseString=responseString+"\"assignment_id\":\""+assignmentId+"\",";
        responseString=responseString+"\"ntf_no\":\""+notificationNO+"\",";
        responseString=responseString+message;
        responseString=responseString+"}]";
        System.out.println(responseString);
        ResponseBuilder builder = Response.ok(responseString);
        response = builder.build();
        return response;
            
        


    }
    
    public static void main(String[] args) {
       String sqlStemt="select    \n" + 
            " emplist.person_id ,emplist.Full_name ,   \n" + 
            "  (select mbc_mobile_pkg.BASE64ENCODE(  image )  from  per_images where parent_id= emplist.person_id) img,emplist.person_id,\n" + 
            "AS_SCHDLR_PKG.PUBLICSPACESHIFT1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),emplist.work_group_id ,null,?) Shift ,   \n" + 
            "                                  \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACEBULLETIN1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),emplist.work_group_id,null,?) Bulletins ,   \n" + 
            "                                 \n" + 
            " AS_SCHDLR_PKG.PUBLICSPACEASSIGNMENTS(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'))Assginments   \n" + 
            "                       \n" + 
            " ,AS_SCHDLR_PKG.PUBLICSPACELEAVES(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD')) Leaves ,   \n" + 
            "   \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACENOTES1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),emplist.work_group_id,null,?) Notes ,  \n" + 
            "\n" + 
            "                   \n" + 
            " AS_SCHDLR_PKG.PUBLICSPACEREPLACEMENT1(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),emplist.work_group_id,null,?) ReplacedBy ,   \n" + 
            "  \n" + 
            "AS_SCHDLR_PKG.PUBLICSPACEREPLACEMENTDATA(emplist.person_id,TO_DATE (datestart , 'YYYY-MM-DD'),?,emplist.work_group_id ) Relacing \n" + 
            "\n" + 
            "   \n" + 
            "from    \n" + 
            "  \n" + 
            "   \n" + 
            "   \n" + 
            "(select  distinct    papf.PERSON_ID,papf.FULL_NAME ,wk.work_group_id  , ? datestart from    \n" + 
            "PER_ALL_PEOPLE_F PAPF ,AS_WORK_GROUPS_HDR wk  ,AS_WORK_GROUPS_LINES wkl       \n" + 
            " where  wk.work_group_id=wkl.work_group_id    and wkl.person_id=papf.person_id         \n" + 
            "  AND TO_DATE (? , 'YYYY-MM-DD') BETWEEN PAPF.EFFECTIVE_START_DATE    AND PAPF.EFFECTIVE_END_DATE    \n" + 
            "  AND wkl.work_group_id =?\n" + 
            "                                         \n" + 
            " AND (wk.fk_organization_id=?)      ) emplist    \n" + 
            "\n" + 
            "    \n" + 
            " order by decode (Shift , null , 0 , 1) desc, Full_name";
       System.out.println(sqlStemt);
//       System.out.println(StringtoReponse("ddd").toString());
//       String x="button_name :FORWARD, button_color :Gray, button_icon :arrow-round-forward,is_sub_action:1; button_name :REJECT, button_color :Red, button_icon :close,is_sub_action:1; button_name :REASSIGN, button_color :Gray, button_icon :send,is_sub_action:1; button_name :MORE_INFORMATION_REQUEST, button_color :Gray, button_icon :information,is_sub_action:1";
//       String y [] =x.split(";") ; 
//       String m []= y[0].split(",");
//       String k[]=m[0].split(":");
//       System.out.println(y[0]);
//       System.out.println(m[0]);
//       System.out.println(k[0]);
//       System.out.println(k[1]);
   }
    
    
    public static void logActivity (Connection connection , String personId , String userId,String ActivityName , String descriptin , String userName )
    {
           
            CallableStatement cs=null;
          
            String sqlStemt="begin " +
            "APPS.MBC_MOBILE_PKG.LOG_MOBILE_ACTIVITY ( ?, ?, ?, ?, ? , ? , ? );" +
            "end;";
            try {
        
            cs = connection.prepareCall(sqlStemt);
                cs.setString(1, "1");
                cs.setString(2, personId);
                cs.setString(3, userId);
                cs.setString(4, ActivityName);
                cs.setString(5, "WS");
                cs.setString(6, descriptin);
                cs.setString(7, userName);
               
            cs.executeUpdate();
                
            }
            catch ( Exception e)
            {
               e.printStackTrace();
                }
        
            finally {
                        try {

                            if ((cs != null) && !(cs.isClosed()))
                                cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
        
        }
    
    
    public static void logActivity (Connection connection , String ActivityName , String descriptin  )
    {
           
           
           
            CallableStatement cs=null;
          
            String sqlStemt="begin " +
            "APPS.MBC_MOBILE_PKG.LOG_MOBILE_ACTIVITY (  ?, ? , ? , ? );" +
            "end;";
            try {
        
            cs = connection.prepareCall(sqlStemt);
                cs.setString(1, "1");
                cs.setString(2, ActivityName);
                cs.setString(3, "WS");
                cs.setString(4, descriptin);
               
            cs.executeUpdate();
                
            }
            catch ( Exception e)
            {
               e.printStackTrace();
                }
        
            finally {
                        try {

                            if ((cs != null) && !(cs.isClosed()))
                                cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
        
        }
    
    
    
    public static String checkSession (Connection connection , String wsid , String signature , String compareValue )
    {
           
            CallableStatement cs=null;
            String result;
            String sqlStemt="begin " +
            "?:=APPS.MBC_MOBILE_PKG.check_session ( ?, ? ,?);" +
            "end;";
            try {
        
            cs = connection.prepareCall(sqlStemt);
                cs.registerOutParameter(1,Types.VARCHAR);
                cs.setString(2, wsid);
                cs.setString(3, signature);
                cs.setString(4, compareValue);
                
               
            cs.executeUpdate();
                result=cs.getString(1);
                
            }
            catch ( Exception e)
            {
               e.printStackTrace();
               result= e.getMessage();
                }
        
            finally {
                        try {

                            if ((cs != null) && !(cs.isClosed()))
                                cs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
        return result;
        }
    
    public Response getData (SecurityContext sc,
                                String wsid,
                                String signature,
                                String name,
                                String sign,
                                String sqlStemt,
                                String [] parameters)
    {
        
        
            if (!sc.isUserInRole("Authenticated"))  
            throw new SecurityException("User is unauthorized.");
            
            if (wsid==null || signature==null )  
            return Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
            
            wsid=URLDecoder.decode(wsid);
            signature=URLDecoder.decode(signature);
            Connection connection = null;
            PreparedStatement statement = null;
            Response response = null;
            ResultSet rs=null;
   
            try {
         
                connection = Helper.getOracleConnection();
                String result=Helper.checkSession(connection, wsid, signature ,sign);
                if (!"OK".equals(result))
                throw new SecurityException("Unauthorized Access."); 
                
                Helper.logActivity(connection, name, name);
            statement = connection.prepareStatement(sqlStemt);
              //  statement.setString(1, wsid); 
               

               
             rs = statement.executeQuery();
          
               
               List<Map<String, Object>> m = new ArrayList<Map<String, Object>>();
                m = ConvertRStoMap.getEntitiesFromResultSet(rs);
                          
                System.out.println("size is "+m.size());
                StringWriter outPut = new StringWriter();
                ObjectMapper map = new ObjectMapper();
                map.writeValue(outPut, m);
                ResponseBuilder builder = Response.ok(outPut.toString());
                response = builder.build();
                if (m.size() == 0) {
                 
                    
                }

            }
            catch(SecurityException s)
            {
                s.printStackTrace();
                response = Response.status(401).entity("Unauthorized Access.").type("text/plain").build();
                }
            catch(Exception e)
            {
                e.printStackTrace();
                    response = Helper.StringtoReponse(e.getMessage().replace("\n", "").replace("\r", ""),"2");
                }
            finally {
                        try {
                          
                            if ((connection != null) && !(connection.isClosed()))
                                connection.close();
                            if ((statement != null) && !(statement.isClosed()))
                                          statement.close();
                            if ((rs != null) && !(rs.isClosed()))
                            rs.close();
                        } catch (SQLException sqle) {
                            // TODO: Add catch code
                            sqle.printStackTrace();
                        }
                    }
                    return response;
        }

}

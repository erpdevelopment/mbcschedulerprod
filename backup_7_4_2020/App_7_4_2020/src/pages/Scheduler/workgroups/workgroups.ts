import  {NavController, NavParams } from 'ionic-angular';
import { AppModule } from '../../../app/app.module';
import { Navbar } from 'ionic-angular';
import { ViewChild, Component } from '@angular/core';
import {SchedulerProvider} from '../../../providers/scheduler/scheduler'
import { LoadingController } from 'ionic-angular';
import {AlertController } from 'ionic-angular';
import {MyTeamPage} from '../../Scheduler/myteam/myteam';
/**
 * Generated class for the WorkgroupsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-workgroups',
  templateUrl: 'workgroups.html',
})
export class WorkgroupsPage {

   // businessGroup     :string ;
  // assignmentId      :string ;
  MywkList;
  MywkListAll;
  fromR=0;
  v_step=12;
  toR=this.v_step;

  @ViewChild('pageTopx') pageTopx;
  
 // seg:string ="myspace";
 // @ViewChild(Navbar) Navbar: Navbar;
 searchDate :string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public SchedulerData:SchedulerProvider,
    public loadingController: LoadingController ,  public AlertController :AlertController) {
  
 


    
    

  }

  ionViewDidLoad() {
   // this.setBackButtonAction();
   this.getMywkList();
    console.log('ionViewDidLoad MyWorkGroupPage');
  
  }

  getMywkList()
  {
    
    
   

    try{

      this.SchedulerData.getworkgroupsList()
     // this.payrollData.getPayslipList(this.businessGroup,this.personId)
      
      .then(data => {
        
      this.MywkListAll= data;
      if (this.MywkListAll)
      {
      this.MywkList=this.MywkListAll.slice(this.fromR,this.toR);
      console.log("DataLoaded");
    }
      });
      console.log("Test After Loading");
     
    }
    catch (error)
    {
    
      
      AppModule.showMessage(this.AlertController,error);
    }
  }



  async doRefresh(refresher) {
    this.toR=this.v_step;
    await this.getMywkList();
    refresher.complete();

    }



    doInfinite(infiniteScroll) {


      // this.fromR=this.fromR+this.v_step;
       this.toR=this.toR+this.v_step;
   
   
       try{
   
         
           
         if(this.MywkListAll)
         this.MywkList=(this.MywkListAll.slice(0 ,this.toR));
         
         infiniteScroll.complete();
     }
       catch (err)
       {
         
       }
       console.log(' infiniteScroll has ended');
     }


     async wkTapped(event, item)
     {
 
      if (item==null)
      return;
 
       
 
 
     
      try
      {  
    AppModule.sAAwkId=item.work_group_id ;
        console.log ('workgroupid'+AppModule.sAAwkId);
        this.navCtrl.push(MyTeamPage);
      }
      catch (err)
      {
       
        AppModule.showMessage(this.AlertController,"error");
      }
    
     
     
     }
     
     
   
     
   


}
